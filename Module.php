<?php
namespace Lur;
use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    public function onBootstrap(MvcEvent $event){
        $app = $event->getApplication();
        $sm  = $app->getServiceManager();

        $aConfig = $sm->get('config');

        if (defined('APPLICATION_MODEL') && strtolower(APPLICATION_MODEL) !== "console") {
            $sClass1 = '\\'.LAF_NAMESPACE.'\YcheukfReportExt\\Report\\Dictionary\\Id2label';
            $sClass2 = '\\'.LAF_NAMESPACE.'\YcheukfReportExt\\Report\\Dictionary\\Metric';

            if (class_exists('\YcheukfReport\Lib\ALYS\ALYSFunction')) {
                \YcheukfReport\Lib\ALYS\ALYSFunction::setClassMapper('Id2label', $sClass1);
                \YcheukfReport\Lib\ALYS\ALYSFunction::setClassMapper('Metric', $sClass2);
                # code...
            }
        }
        
        // \Application\Model\Common::initReportDictionary($sm);
    }
    /**
     * 配置Server
     * @return multitype:multitype:NULL  |\Publish\Form\Login
     */
    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'Lur\Service\Adrserver' => 'Lur\Service\Adrserver',
                'Lur\Service\Proxyip' => 'Lur\Service\Proxyip',
                'Lur\Service\Common' => 'Lur\Service\Common',
                'Lur\Service\Across' => 'Lur\Service\Across',
                'Lur\Service\Location' => 'Lur\Service\Location',
                'Lur\Service\Report' => 'Lur\Service\Report',
                'Lur\Service\Record' => 'Lur\Service\Record',
                'Lur\Service\Addtional' => 'Lur\Service\Addtional',
                'Lur\Service\UvcodeSpider' => 'Lur\Service\UvcodeSpider',
                'lf_admincontent' => 'Lur\Service\AdminContent',
            ),
        );
    }
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
