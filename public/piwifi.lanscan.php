<?php
/**
 * 局域网扫描PI的方式
 */
include_once(__DIR__."/../../../bin/script/simplerestapi.inc.php");


// 让adr pi 报告wifi信息, 并等待返回
function callAdrWifiInfo($sCommandType, $aCommandParams=array())
{
    global $oGlobalFramework;
    $bGetDeviceMsgFlag = false;
    $oGlobalFramework->sm->get('\Lur\Service\Common')->sendAdrCommand($_GET['i'], $sCommandType, $aCommandParams);
    // 等待设备信息返回
    $bBreakFlag = false;
    $bWhileRetryTimes = 20;
    while (1) {
        if ($bWhileRetryTimes-- < 1)$bBreakFlag = true;
        sleep(5);

        $aAdrInfo = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrSerInfoById($_GET['i']);;
        if (isset($aAdrInfo['wifi']) && isset($aAdrInfo['wifi']['pi3wifi']) && count($aAdrInfo['wifi']['pi3wifi'])) {
            $bGetDeviceMsgFlag = true;
            $bBreakFlag = true;
        }
        
        if($bBreakFlag)break;
    }
    return array($bGetDeviceMsgFlag, $aAdrInfo);
}

function genToken($i)
{
    return "i=".$i."&ei=".\YcheukfCommon\Lib\Functions::encryptBySlat($i);
}

try {

    // var_dump($aAdrRemoteIps);
    $sAction = isset($_GET['action']) ? $_GET['action'] : "default";
    switch ($sAction) {
        case 'scan':
            $aDeviceList = array(   );
            $aAdrRemoteIps = $oGlobalFramework->sm->get('\Lur\Service\Addtional')->getAllAdrRemoteIp();
            $sClientIps = \YcheukfCommon\Lib\Functions::getRemoteClientIp();
            if (isset($aAdrRemoteIps[$sClientIps]) && count($aAdrRemoteIps[$sClientIps])) {
                foreach ($aAdrRemoteIps[$sClientIps] as $nAdrId) {
                    $aAdrInfo = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrSerInfoById($nAdrId);;
                    $aDeviceList[$aAdrInfo['id']] = array(
                        'label' => $aAdrInfo['id'].",".$aAdrInfo['label'],
                        'token' => genToken($aAdrInfo['id']),
                        'machineuptime' => $aAdrInfo['machineuptime'],
                    );

                }
            }
            if (count($aDeviceList)) {
                header('HTTP/1.1 200 OK');
                echo json_encode($aDeviceList);
            }else{
                header('HTTP/1.1 500 Internal Server Error');
                echo "";
            }
            exit;
            break;
        
        default:
            # code...
            break;
    }

} catch (Exception $e) {
    echoMsg($e->getMessage());
    exit;
    
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>ADRPI - 局域网内设备扫描结果</title>

    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
     <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
     <script src="http://apps.bdimg.com/libs/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</head>
<body>
<div class="page-header">
  <h1>ADRPI - 局域网内设备扫描结果<small></small></h1>
    <p>
</div>

<div class="panel panel-default panelbody">
    <div class="pilist">
        <p>
        <p>
        <center style="font-size: 30pt;">正在扫描附近的ADRPI... 请稍等</center>
        <p>
        <p>
        <p>

    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">要找的设备没有出现的上面, 请按照以下步骤排查</h3>
    </div>
    <div class="panel-body">
        <ul>
            <li> 确保ADRPI的电源已经接通
            <li> 确保用一根网线连接了ADRPI 与 <目标WIFI>的路由器 (<目标WIFI>即要给ADRPI设置的WIFI)
            <li> 确保您的手机正在使用<目标WIFI> 
            <li> 以上条件都满足的情况下, 再 <button type="button" class="btn btn-lg btn-success retry">重试</button>    
    </div>
</div>
</body>
</html>
<script type="text/javascript">


    $(".retry").click(function(event) {
        window.location.reload()
    });

    jQuery.ajax({
      url: location.href+'?action=scan',
      type: 'POST',
      dataType: 'json',
      data: {param1: 'value1'},
      complete: function(xhr, textStatus) {
        //called when complete
      },
      success: function(data, textStatus, xhr) {
        $('div.pilist').html("<h3>扫描到的设备列表<h3/><ul></ul>");
        $.map(data, function(item, index) {
            $('div.pilist ul').append('<li><big style="font-size:25pt"><a href="piwifi.php?'+item.token+'">'+item.label+"  (开机时间:"+item.machineuptime+')</a></big></li>')
        });

      },
      error: function(xhr, textStatus, errorThrown) {
        $('div.pilist').html("<center style=\"font-size: 30pt;\">没有找到任何ADRPI<center/>");
      }
    });
    
</script>