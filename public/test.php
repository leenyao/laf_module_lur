<?php
/**
 * 检查 adr_trolley_release 是否有新版本
 */



include_once("/app/module/Lur/public/adrapi/simplerestapi2.inc.php");
$oSimpleRestApi = new SimpleRestApi2();



$aList = $oSimpleRestApi->sm->get('\Lur\Service\Addtional')->getAliveAdrTrolleyVpnList();
var_dump($aList);

exit;
try{
// var_dump($oSimpleRestApi->redis->info());

	// 判断token合法性
	if (!isset($_GET['token_t']) || !isset($_GET['token_v'])) {
		throw new Exception("miss token params", 1);
	}
	$bFlag = \YcheukfCommon\Lib\Functions::chkLyDynamicToken($_GET['token_t'],  $_GET['token_v'], "ADRTROLLEY");
	if ($bFlag !== true) {
		throw new Exception("error token", 1);
	}
	$sVersion = isset($_GET['version']) ? $_GET['version'] : "release";

	if ($sVersion === "release") {
		$sDir = "/app/data/adr_trolley_release/package/";
	}else{
		$sDir = "/app/data/adr_trolley_release/package-$sVersion/";
	}




	// 当前最新版本
	$sDownloadFile = file_get_contents('/app/data/adr_trolley_release/adr-trolley.lastest.package');

// var_dump($sDownloadFile);

	if (!empty($sDownloadFile)) {
		$sAction = isset($_GET['act']) ? $_GET['act'] : "chkmd5";
		// $sAction = isset($_GET['md5flag']) && $_GET['md5flag']==1 ? "chkmd5" : "download";
		switch ($sAction) {
			case 'chkmd5':
				$oSimpleRestApi->httpresponse($sDownloadFile, 200, false);
				break;
			case 'ask4update'://x秒内只允许升级一个
				if(isset($_GET['token_suid']) && !empty($_GET['token_suid'])){

					$sClientIps = \YcheukfCommon\Lib\Functions::getRemoteClientIp();
					$sCacheKey = 'LC_501/'.$sClientIps;

			        if (!$oSimpleRestApi->redis->exists($sCacheKey)) {
			        	$oSimpleRestApi->redis->watch($sCacheKey);
			        	$bRet = $oSimpleRestApi->redis->multi()->setEx($sCacheKey, 5, $_GET['token_suid'])->exec();
			        	if ($bRet) {
			        		$aLogs = array(
			        			"server" => $_SERVER,
			        			"query" => $_GET,
			        		);
			        		file_put_contents("/tmp/last_trolley_setup_info.log", print_r($aLogs, 1));
							$oSimpleRestApi->httpresponse(md5_file($sDir.$sDownloadFile), 200, false);
			        	}
			        }else{
			        	$sCurrentSuid = $oSimpleRestApi->redis->get($sCacheKey);
			        	if ($sCurrentSuid === $_GET['token_suid']) {
							$oSimpleRestApi->httpresponse(md5_file($sDir.$sDownloadFile), 200, false);
			        	}
			        }

				}
				$oSimpleRestApi->httpresponse(0, 500, false);
				break;
			case 'download':

					ob_clean();
					header('Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0');
					header('Pragma: no-cache');
					header('Pragma: public');
					header('Content-Description: File Transfer');
					header('Content-Disposition: attachment; filename="'.($sDownloadFile).'"');
					header('Content-Type: text/comma-separated-values');
					header('Content-Transfer-Encoding: binary');
					header('Content-Length: ' . filesize($sDir.$sDownloadFile));
					readfile($sDir.$sDownloadFile);
				break;
			
			default:
				throw new Exception("error action:".$sAction, 1);
				break;
		}
	}else{
		$oSimpleRestApi->httpresponse("no suitable file", 500, false);

	}

}catch(\Exception $e){

	$oSimpleRestApi->httpresponse($e->getMessage(), 500, false);

}


