<?php
include_once(__DIR__."/../../../../../bin/script/config_inc.php");
set_time_limit(0);


//first param
$nParamIndex = 1;
$sTargetData = null;
if (isset($_SERVER['argv'][$nParamIndex])) {
    $sTargetData = $_SERVER['argv'][$nParamIndex];
}else{
    $sTargetData = "xx";
}
$dirpath = BASE_INDEX_PATH.'/../data/adrresult/'.$sTargetData;

$nFileCount = 0;
$aResult = array();
$aCounter = array();
$aServerids = array();
$sDate = $sCodeId = null;
if($handle = opendir($dirpath)) { //注意这里要加一个@，不然会有warning错误提示：）
    while(($file = readdir($handle)) !== false) {
        if($file != ".." && $file != ".") { //排除根目录；
            if(is_dir($dirpath."/".$file)) { //如果是子文件夹，就进行递归
            } else { //不然就将文件的名字存入数组；

                $aServerids[$file] = $file;
                $filename = $dirpath."/".$file;
                $fhandle = fopen($filename, "r") or exit("Unable to open file!");
                $index = 0;
                while(!feof($fhandle))
                {
                    $linedata1 = $linedata = fgets($fhandle);
                    // $linedata = str_replace('"', "", $linedata);
                    // $linedata = trim($linedata);
                    $jsondata = json_decode($linedata1, 1);
                    if (is_null($jsondata) ) {
                      continue;
                    }
                    // print_r($jsondata);

                    if (is_null($sDate)) {
                        $sDate = $jsondata['info']['date'];
                    }
                    if (is_null($sCodeId)) {
                        $sCodeId = $jsondata['info']['codeid'];
                    }
                    $nTotal = 0;

                    if (isset($jsondata['data'])) {
                        foreach ($jsondata['data'] as $metric => $row) {
                            if (!isset($aCounter[$metric])) {
                                $aCounter[$metric] = [
                                    'imp' =>0,
                                    'te-firstQuartile' =>0,
                                    'te-midpoint' =>0,
                                    'te-thirdQuartile' =>0,
                                    'te-complete' =>0,
                                ];
                            }
                            foreach ($row as $metric2 => $count1) {
                                $aCounter[$metric][$metric2] += $count1;
                            }
                            // $aResult[$sTe]['ad1'] += $row['first'];
                            // $aResult[$sTe]['ad2+'] += $row['other'];
                        }

                        // foreach ($jsondata['data'] as $sTe => $row) {
                        //     if (!isset($aResult[$sTe])) {
                        //         $aResult[$sTe]['ad1'] = 0;
                        //         $aResult[$sTe]['ad2+'] = 0;
                        //     }
                        //     $aResult[$sTe]['ad1'] += $row['first'];
                        //     $aResult[$sTe]['ad2+'] += $row['other'];
                        // }

                        // if (isset($jsondata['data']['imp']['other'])
                        //     && isset($jsondata['data']['te-firstQuartile']['other'])
                        // ) {
                        //     if ($jsondata['data']['te-firstQuartile']['other'] > $jsondata['data']['imp']['other']) {
                        //         echo "\n===bad data:".$file."===\n";
                        //         print_r($jsondata['data']);

                        //     }
                        // }
                    }

                }
                fclose($fhandle);
                $nFileCount++;
            }

        }
    }
    closedir($handle);
}
// ksort($aServerids);

// echo "\n\n======\n";

// $nTypeTmp = 6;
// list($bFalg, $aReportData) = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrReportDataForCounterTable( array($sCodeId), $sDate, $sDate, $nTypeTmp);
// print_r($aReportData);


$aResult = $aCounter;
/**
 * #######closure function start#######
 */
$fPrintFunc = function($key=null) use ($aResult)
{

    $aTarget = [
        'ad1' => [
            '1/4' => 84.32,
            '2/4' => 77.45,
            '3/4' => 73.15,
            '4/4' => 71.10,
        ],
        'ad2+' => [
            '1/4' => 96.77,
            '2/4' => 95.95,
            '3/4' => 94.53,
            '4/4' => 94.84,
        ],
    ];
    echo "\n\n===".$key."===\n";

    if (isset($aResult['imp'])) {
        echo "imp =>".$aResult['imp'][$key]. ", 100\n";
    }
    if (isset($aResult['te-firstQuartile'])) {
        $t1 = (round($aResult['te-firstQuartile'][$key]/$aResult['imp'][$key],4)*100);
        $t2 = $aTarget[$key]['1/4'];
        echo "1/4 =>".$aResult['te-firstQuartile'][$key]. ", ".$t1."/".$t2.", gap:".round($t1-$t2, 2)."\n";
    }
    if (isset($aResult['te-midpoint'])) {
        $t1 = (round($aResult['te-midpoint'][$key]/$aResult['imp'][$key],4)*100);
        $t2 = $aTarget[$key]['2/4'];
        echo "2/4 =>".$aResult['te-midpoint'][$key]. ", ".$t1."/".$t2.", gap:". round($t1-$t2, 2)."\n";
    }
    if (isset($aResult['te-thirdQuartile'])) {
        $t1 = (round($aResult['te-thirdQuartile'][$key]/$aResult['imp'][$key],4)*100);
        $t2 = $aTarget[$key]['3/4'];
        echo "3/4 =>".$aResult['te-thirdQuartile'][$key]. ", ".$t1."/".$t2.", gap:".round($t1-$t2, 2)."\n";
    }
    if (isset($aResult['te-complete'])) {
        $t1 = (round($aResult['te-complete'][$key]/$aResult['imp'][$key],4)*100);
        $t2 = $aTarget[$key]['4/4'];
        echo "4/4 =>".$aResult['te-complete'][$key]. ", ".$t1."/".$t2.", gap:". round($t1-$t2, 2)."\n";
    }
    echo "\n\n";
};


echo "\n[result-s]\n";
// $fPrintFunc();
// $fPrintFunc('ad2+');
//unset($fClosureFunc);
print_r($aResult);

$standars = [
    '15|pad|i1' => [83.19, 76.86, 73.45, 71.68], 
    '15|pad|i2' => [98.34, 97.55, 96.99, 96.71], 
    '15|phone|i1'=> [81.83, 75.26, 71.67, 70],
    '15|phone|i2'=> [97.09, 96.27, 95.5, 95.26],
    '15|pc|i1' => [83.94, 80.58, 78.25, 76.66],
    '15|pc|i2'=> [98.87, 97.99, 97.04, 96.42],
    '15|ott|i1'=> [90.31, 86.38, 83.3, 80.65], 
    '15|ott|i2'=> [98.39, 96.87, 96.19, 95.24],
];
$names = [
    '15|pad|i1' => "pad-前贴-15s-1", 
    '15|pad|i2' => "pad-前贴-15s-2+", 
    '15|phone|i1'=> "phone-前贴-15s-1", 
    '15|phone|i2'=> "phone-前贴-15s-2+", 
    '15|pc|i1' => "pc-前贴-15s-1", 
    '15|pc|i2'=> "pc-前贴-15s-2+", 
    '15|ott|i1'=> "ott-前贴-15s-1", 
    '15|ott|i2'=> "ott-前贴-15s-2+", 
];
$titles = ['firstQuartile', 'midpoint', 'thirdQuartile', 'complete'];
$aCsv = [];
$aCsv[] = join(",", array_merge(["广告位"], $titles));
foreach($aResult as $m1 => $r1){
    if(in_array($m1, ['15|pad|i1', '15|pad|i2', '15|phone|i1', '15|phone|i2', '15|pc|i1', '15|pc|i2', '15|ott|i1', '15|ott|i2'])){
        $line = [$names[$m1]];
        $standar = $standars[$m1];
        $line = array_merge($line, $standar);
        foreach($titles as $index1=>$title){
        // print_r($r1['te-'.$title]);   
            $per = 0;
            if(!empty($r1['imp'])){
                $per = 100*round($r1['te-'.$title]/$r1['imp'], 4);
            }
            $diff = round($per-$standar[$index1],1);
            $line[] = $per."(".$diff.")";
        }
        // print_r($line);
        $aCsv[] = join(",", $line);
    }
}

echo "\n\n".(join("\n", $aCsv))."\n\n";

echo "datapath=".$dirpath."\n";
echo "nFileCount=".$nFileCount."\n";
echo "info:".print_r($jsondata['info'], 1);
echo "\n[result-e]\n";




