<?php
include_once(__DIR__."/../../../../../bin/script/config_inc.php");
set_time_limit(0);


//first param
$nParamIndex = 1;
$sTargetData = null;
if (isset($_SERVER['argv'][$nParamIndex])) {
    $sTargetData = $_SERVER['argv'][$nParamIndex];
}else{
    $sTargetData = "xx";
}
$dirpath = BASE_INDEX_PATH.'/../data/adrresult/'.$sTargetData;

$nFileCount = 0;
$aResult = array();
$aServerids = array();
$sDate = $sCodeId = null;
if($handle = opendir($dirpath)) { //注意这里要加一个@，不然会有warning错误提示：）
    while(($file = readdir($handle)) !== false) {
        if($file != ".." && $file != ".") { //排除根目录；
            if(is_dir($dirpath."/".$file)) { //如果是子文件夹，就进行递归
            } else { //不然就将文件的名字存入数组；

                $aServerids[$file] = $file;
                $filename = $dirpath."/".$file;
                $fhandle = fopen($filename, "r") or exit("Unable to open file!");
                $index = 0;
                while(!feof($fhandle))
                {
                    $linedata1 = $linedata = fgets($fhandle);
                    // $linedata = str_replace('"', "", $linedata);
                    // $linedata = trim($linedata);

                    $jsondata = json_decode($linedata1, 1);

                    if (is_null($jsondata)) {
                      continue;
                    }

                    if (is_null($sDate)) {
                        $sDate = $jsondata['info']['date'];
                    }
                    if (is_null($sCodeId)) {
                        $sCodeId = $jsondata['info']['codeid'];
                    }
                    $nTotal = 0;

                    if (isset($jsondata['data'])) {
                        // print_r($file. "=>" .count($jsondata['data'])."\n");

                        foreach ($jsondata['data'] as $datecodeid => $row1) {
                            list($dodate, $codeid) = explode("-", $datecodeid);
                            foreach ($row1 as  $row2) {
                                if (!isset($aResult[$datecodeid])) {
                                    $aResult[$datecodeid] = [];
                                }

                                $row2 = trim($row2);
                                if (empty($row2)) {
                                    continue;
                                }
                                // if (preg_match('/^a=/', $row2)) {
                                    @list($count, $uuid) = explode(" ", $row2);
                                    // $uuid = $row2;
                                    if (!isset($aResult[$datecodeid][$uuid])) {
                                        $aResult[$datecodeid][$uuid] = 0;
                                    }
                                    if (is_numeric($count) && !empty($uuid)){
                                        $aResult[$datecodeid][$uuid] += $count;
                                    }
                                    
                                // }else {
                                // }
                            }
                        }
                    }

                }
                fclose($fhandle);
                $nFileCount++;
            }

        }
    }
    closedir($handle);
}

arsort($aResult);

// print_r($aResult);
$aFreq = [];
foreach ($aResult as $datecodeid => $aResult2) {
    foreach ($aResult2 as $uuid => $freq) {

        if (!isset($aFreq[$datecodeid])) {
            $aFreq[$datecodeid] = [];
        }
        if (!isset($aFreq[$datecodeid][$freq])) {
            $aFreq[$datecodeid][$freq] = 0;
        }
        $aFreq[$datecodeid][$freq] += 1;
    }
}

$aFreq2 = [];
foreach ($aFreq as $datecodeid => $aFreq22) {
    foreach ($aFreq22 as $freq => $count) {

        if (!isset($aFreq2[$datecodeid])) {
            $aFreq2[$datecodeid] = [];
        }
        if ($freq>=4) {
            $freq = "4";
        }
        if (!isset($aFreq2[$datecodeid][$freq])) {
            $aFreq2[$datecodeid][$freq] = 0;
        }
        $aFreq2[$datecodeid][$freq] += $count;
    }
}


$aFreq3 = [];
foreach ($aFreq2 as $datecodeid => $aFreq22) {
    foreach ($aFreq22 as $freq => $count) {

        if (!isset($aFreq3[$datecodeid])) {
            $aFreq3[$datecodeid] = [];
        }
        $aFreq3[$datecodeid][$freq] = [$count, (100*round($count/array_sum($aFreq22),4))];
    }
    ksort($aFreq3[$datecodeid]);

}
ksort($aFreq3);


$aPrint = [["date-codeid"]];
$init =  false;
foreach ($aFreq3 as $datecodeid => $aFreq22) {
    $row5 = [];
    $row5[] = $datecodeid;
    foreach ($aFreq22 as $freq => $row4) {
        if($freq == 0)continue;
        if ($init == false){
            $aPrint[0][] = 'freq-'.$freq;
            $aPrint[0][] = 'freq-'.$freq.'-rate';
        }
        $row5[] = $row4[0];
        $row5[] = $row4[1];
    }
    $init = true;

    $aPrint[] = $row5;
}


print_r($aPrint);
foreach($aPrint as $linerow){
    echo "\n".join(", ", $linerow);
}

// $nTypeTmp = 6;
// list($bFalg, $aReportData) = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrReportDataForCounterTable( array($sCodeId), $sDate, $sDate, $nTypeTmp);
// print_r($aReportData);
echo "\n \n ";
echo "datapath=".$dirpath."\n";
echo "nFileCount=".$nFileCount."\n";
echo "info:".print_r($jsondata['info'], 1);




