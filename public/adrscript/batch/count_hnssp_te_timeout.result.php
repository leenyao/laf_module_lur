<?php
include_once(__DIR__."/../../../../../bin/script/config_inc.php");
set_time_limit(0);


//first param
$nParamIndex = 1;
$sTargetData = null;
if (isset($_SERVER['argv'][$nParamIndex])) {
    $sTargetData = $_SERVER['argv'][$nParamIndex];
}else{
    $sTargetData = "xx";
}
$dirpath = BASE_INDEX_PATH.'/../data/adrresult/'.$sTargetData;



$nFileCount = 0;
$jsondata = [];
$aResult = array();
$aServerids = array();
$sDate = $sCodeId = null;
$nTotal = 0;
if($handle = opendir($dirpath)) { //注意这里要加一个@，不然会有warning错误提示：）
    while(($file = readdir($handle)) !== false) {
        if($file != ".." && $file != ".") { //排除根目录；
            if(is_dir($dirpath."/".$file)) { //如果是子文件夹，就进行递归
            } else { //不然就将文件的名字存入数组；

                $aServerids[$file] = $file;
                $filename = $dirpath."/".$file;
                $fhandle = fopen($filename, "r") or exit("Unable to open file!");
                $index = 0;
                while(!feof($fhandle))
                {
                    $linedata1 = $linedata = fgets($fhandle);
                    // $linedata = str_replace('"', "", $linedata);
                    // $linedata = trim($linedata);
                    $jsondata = json_decode($linedata1, 1);
                    if (is_null($jsondata)) {
                      continue;
                    }
                    // print_r($jsondata);

                    if (is_null($sDate)) {
                        $sDate = $jsondata['info']['date'];
                    }
                    if (is_null($sCodeId)) {
                        $sCodeId = $jsondata['info']['codeid'];
                    }

                    if (isset($jsondata['data']) && count($jsondata['data'])) {
                        // echo "\n=======".$file."=========\n";
                        $nTotal += count($jsondata['data']);
                        $jsondata['data']['server'] = $file;
                        // print_r($jsondata['data']);
                    }

                }
                fclose($fhandle);
                $nFileCount++;
            }

        }
    }
    closedir($handle);
}

$aDayCount = $oGlobalFramework->sm->get('\Lur\Service\Report')->getSspHunanDataByDateCode($jsondata['info']['date'], $jsondata['info']['date'], 3);

// print_r($aDayCount);

echo "\n[result-s]\n";

echo "nTotal-ot=".$nTotal."\n";
echo "nTotal-all=".$aDayCount[0]['impcount']."\n";
echo "percent=".(round($nTotal/$aDayCount[0]['impcount'],4)*100)."%\n";

echo "datapath=".$dirpath."\n";
echo "nFileCount=".$nFileCount."\n";
if (isset($jsondata['info'])) {
    echo "info:".print_r($jsondata['info'], 1);
}

echo "\n[result-e]\n";




