<?php
include_once(__DIR__."/../../../../../bin/script/config_inc.php");
set_time_limit(0);


//first param
$nParamIndex = 1;
$sTargetData = null;
if (isset($_SERVER['argv'][$nParamIndex])) {
    $sTargetData = $_SERVER['argv'][$nParamIndex];
}else{
    $sTargetData = "xx";
}
$dirpath = BASE_INDEX_PATH.'/../data/adrresult/'.$sTargetData;

echo "datapath:".$dirpath."\n\n";

// exec("ls -l --sort=size ".$dirpath, $aOutput);
// print_r($aOutput);




$nFileCount = 0;
$aResult = array();
$aServerids = array();
if($handle = opendir($dirpath)) { //注意这里要加一个@，不然会有warning错误提示：）
    while(($file = readdir($handle)) !== false) {
        if($file != ".." && $file != ".") { //排除根目录；
            if(is_dir($dirpath."/".$file)) { //如果是子文件夹，就进行递归
            } else { //不然就将文件的名字存入数组；

                $aServerids[$file] = $file;
                $filename = $dirpath."/".$file;
                $fhandle = fopen($filename, "r") or exit("Unable to open file!");
                $index = 0;
                while(!feof($fhandle))
                {
                    $linedata1 = $linedata = fgets($fhandle);
                    $linedata = trim($linedata);
                    if (empty($linedata)) {
                      continue;
                    }
                    $dataary = json_decode($linedata, 1);
                    if (is_array($dataary) && count($dataary)) {
                        foreach ($dataary as $host => $count) {
                            if (!isset($aResult[$host])) {
                                $aResult[$host] = 0;
                            }
                            $aResult[$host] += $count;
                        }
                    }

                }
                fclose($fhandle);
                $nFileCount++;
            }

        }
    }
    closedir($handle);
}
arsort($aResult);

$print = [];
$oPdo = \Application\Model\Common::getPDOObejct($oGlobalFramework->sm, "db_slave"); //db_master|db_slave
if (count($aResult)) {
    foreach ($aResult as $host => $count) {
        if ($count<100) {
            continue;
        }
        $sSql = "select * from `laf_lur`.`vpn_hosts` where `host` = :host";
        $oSth = $oPdo->prepare($sSql);
        $oSth->execute(array(
            ":host" => $host,
        ));
        $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
        if (is_array($aResultTmp) && count($aResultTmp)) {
            $print[] = <<<__EOF
供应商:{$aResultTmp[0]['vpnstore']}
拨号域名:{$aResultTmp[0]['host']}
拨号地点:{$aResultTmp[0]['memo']}-{$aResultTmp[0]['city']}
超时次数:{$count}
__EOF;
        }
    }
}
echo "========\n".join("\n\n========\n", $print);

echo "nFileCount:".$nFileCount."\n\n";
