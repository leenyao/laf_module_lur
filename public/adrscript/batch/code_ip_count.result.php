<?php
include_once(__DIR__."/../../../../../bin/script/config_inc.php");
set_time_limit(0);


//first param
$nParamIndex = 1;
$sTargetData = null;
if (isset($_SERVER['argv'][$nParamIndex])) {
    $sTargetData = $_SERVER['argv'][$nParamIndex];
}else{
    $sTargetData = "xx";
}
$dirpath = BASE_INDEX_PATH.'/../data/adrresult/'.$sTargetData;


// exec("ls -l --sort=size ".$dirpath, $aOutput);
// print_r($aOutput);

$givtip_file = __DIR__."/givt-ip";

$aIacIpMatch = [];
$nIacIpPvTotal = 0;
$nFileCount = 0;
$aResult = array();
$aServerids = array();
$topPvIps = [
  '125.78.13.241',
  '36.133.99.110',
  '117.24.174.246',
"123.185.4.37",];
$aTopIp2Count = [];
if($handle = opendir($dirpath)) { //注意这里要加一个@，不然会有warning错误提示：）
    while(($file = readdir($handle)) !== false) {
        if($file != ".." && $file != ".") { //排除根目录；
            if(is_dir($dirpath."/".$file)) { //如果是子文件夹，就进行递归
            } else { //不然就将文件的名字存入数组；

                $aServerids[$file] = $file;
                $filename = $dirpath."/".$file;
                $fhandle = fopen($filename, "r") or exit("Unable to open file!");
                $index = 0;
                    // var_dump($filename);

                while(!feof($fhandle))
                {
                    $linedata1 = $linedata = fgets($fhandle);
                    $linedata = str_replace('"', "", $linedata);
                    $linedata = trim($linedata);
                    if (empty($linedata)) {
                      continue;
                    }
                    if(!preg_match("/\s*\d+\s*\d+\s*/", $linedata)){
                        continue;
                    }

                    $codeid = "";
                    // @list($codeid, $nCount, $sIp) = explode(" ", $linedata);
                    @list($codeid, $nCount, $sIp) = explode(" ", $linedata);


                    // if (in_array($sIp, $topPvIps)) {
                        $key2 = $sIp."|".$codeid;
                        if (!isset($aTopIp2Count[$key2])) {
                            $aTopIp2Count[$key2] = 0;
                        }
                        $aTopIp2Count[$key2] += $nCount;
                    // }


                    if (!is_numeric($nCount) || !filter_var($sIp, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) continue;


                    if (!isset($aResult[$sIp])) {
                        $aIacIpMatch[$sIp] = 0;
                        $isIacMatch = $oGlobalFramework->sm->get('\Lur\Service\Common')->isIacCityMatch($sIp, 17) ? 1 : 0;
                        if ($isIacMatch) {
                            $aIacIpMatch[$sIp] = 1;
                            $nIacIpPvTotal += $nCount;
                        }
                    	$aResult[$sIp] = 0;
                    }
                    $aResult[$sIp] += $nCount;

                }
                fclose($fhandle);
                $nFileCount++;
            }

        }
    }
    closedir($handle);
}
arsort($aResult);
// print_r($aResult);
// print_r($aIacIpMatch);


// $givtip_file;
$givt_content = file_get_contents($givtip_file);
    // var_dump($givt_content);

$aIpFreq = [];
$aIpFreq["lt500"] = 0;
$aIpFreq["lt300"] = 0;
$aBlackIpCount = 0;
$nIndex = 0;
$aBlackList = [];



$aIacCitys = [];
foreach ($aResult as $ip => $count) {
    $nIndex++;

    $iacinfo = $oGlobalFramework->sm->get('\Lur\Service\Common')->getIacConfigByIp2($ip);
    if (isset($iacinfo['data']) && isset($iacinfo['data']['region'])) {
        if (!isset($aIacCitys[$iacinfo['data']['region']])) {
            $aIacCitys[$iacinfo['data']['region']] = 0;
        }
        $aIacCitys[$iacinfo['data']['region']] += $count;
    }


    // if ($nIndex <= 10) {//最大的10个
    if ($nIndex >= count($aResult)-30) {//最少的10个
        echo $ip ."=>". $count."\n";
    }

    $isBlackIp = $oGlobalFramework->sm->get('\Lur\Service\Common')->isBlackIp($ip);
    if ($isBlackIp) {
        $aBlackList[] = $ip;
        $aBlackIpCount += $count;
    }

    // if ($ip == '1.180.205.164') {
    $shell = 'grep \''.$ip.'\' '.$givtip_file.' | wc -l';
    exec($shell, $output);
    if ($output && count($output) && intval($output[0])>0) {
        // var_dump($shell);
        // var_dump($output);
    }

    unset($output);
    // }

    if (intval($count) >=300) {
        if (!isset($aIpFreq["lt300"])) {
            $aIpFreq["lt300"] = 0;
        }
        $aIpFreq["lt300"] += 1;

    }
    if (intval($count) >=500) {
        if (!isset($aIpFreq["lt500"])) {
            $aIpFreq["lt500"] = 0;
        }
        $aIpFreq["lt500"] += 1;
    }
}

arsort($aTopIp2Count);
echo "\n\n==aTopIp2Count==\n";
$index = 0;
foreach ($aTopIp2Count  as $sip =>$count) {
    if($index++ >= 20)continue;
    echo $sip." => ".$count."\n";
}
// print_r($aTopIp2Count);

echo "\n\n==blackip pv, blackip count, blackip-pv-percent==\n";
echo $aBlackIpCount.",".count($aBlackList).",".round(100*$aBlackIpCount/array_sum($aResult))."\n";

echo "\n\n==ipcount, 300+, 500+==\n";
echo count($aResult).",".$aIpFreq["lt300"]."(".round(100*$aIpFreq["lt300"]/count($aResult),4)."),".$aIpFreq["lt500"]."(".round(100*$aIpFreq["lt500"]/count($aResult),4).")\n";


echo "\n\n==pv,ip,pv/ip,iacip-percent,iacip-pv-percent==\n";
echo array_sum($aResult)
    .",".count($aResult)
    .",".round(array_sum($aResult)/count($aResult), 2)
    .",".round(100*array_sum($aIacIpMatch)/count($aIacIpMatch), 2)
    .",".round(100*$nIacIpPvTotal/array_sum($aResult), 2)
    ."\n\n";

echo "\n\n==iaccity==\n";
arsort($aIacCitys);
print_r($aIacCitys);

    

echo "sum:".array_sum($aResult)."\n\n";
echo "datapath:".$dirpath."\n\n";
echo "nFileCount:".$nFileCount."\n\n";

// print_r($aResult);