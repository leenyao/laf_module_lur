#!/bin/bash

tmpfile=/tmp/cache
datadir=/app/logs-101
mkdir -p ${tmpfile}

utag_file()
{
    day=$1
    id=$2
    if [ -f "${datadir}/backup-${day}.tar" ];then
        tar xf ${datadir}/backup-${day}.tar -C ${tmpfile} 2>/dev/null
        bzip2 -d ${tmpfile}/*${id}*.bz2 2>/dev/null
    else
        echo "none"
    fi
}
tag=`utag_file $1 $2`
day=$1
id=$2
if [ "${tag}" != "none" ];then
    find ${tmpfile} -name "*${day}-${id}*" ! -name "*bz2"|xargs cat |awk -F, '{print $1}'|awk -F: '{print $2}'|tr -d "\""|sed /^$/d|sort|uniq -c|awk -v id=$id '{print id","$2","$3","$1}'
else
    echo "not found file ${datadir}/backup-${day}.tar"
fi