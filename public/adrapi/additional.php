<?php
/**
 * 补量 服务
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug=1;

try{
	$oSra->init();
    $oSra->chkToken();
    // $oSra->log($oSra->instants['postdata']);


    /**
     * #######closure function start#######
     */
    $fGetAction = function() use ($oSra)
    {
	    $sAction = isset($oSra->instants['postdata']['action']) ? $oSra->instants['postdata']['action'] : "get";
	    $sAction = in_array($sAction, array("get", "send")) ? $sAction : "get";
	    return $sAction;
    };
    $sAction = $fGetAction();

    $sActionFunc = "_do_".strtolower($sAction);
    $aReturn = $sActionFunc($oSra);
    // $oSra->log($aReturn);
    $oSra->httpresponse($aReturn, 200, true);


}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}

function _do_get($oSra)
{

	$aPostData = $oSra->instants['postdata'];

	// 直连pi3 ,屏蔽
	if (in_array($oSra->instants['adr_id'], [9,10,11,17,22,23,24,26,31,33,37,38,39,41,43,44,45,46,47,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94])) {
	    return [];
	}
	$aConfig = $oSra->config;
	$aIgnorecoids =isset($aPostData['ignorecoids']) ? $aPostData['ignorecoids'] : array();
	$sVpnremoteip =isset($aPostData['sVpnremoteip']) ? $aPostData['sVpnremoteip'] : "";
	$sLocalremoteip =isset($aPostData['sLocalremoteip']) ? $aPostData['sLocalremoteip'] : "";
	$sPostVpnAcc =isset($aPostData['sVpnAcc']) ? $aPostData['sVpnAcc'] : "";
	$sPostAdrtype =isset($aPostData['adrtype']) ? $aPostData['adrtype'] : "";

	$bVpnStat = LAF_CALLVPNSTAT_BAD_602;
	$oAddtionalRedis = $oSra->addtionalredis;


	$aReturn = array();
	if (isset($_GET['tradeno'])) {

			// 存在历史记录
	    $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_TRADENO_GET_303."/".date("Ymd");
	    $aCacheTmp = $oAddtionalRedis->hGet($sTradeNoCacheKey, $_GET['tradeno']);
	    if ($aCacheTmp) {
	        $aJson = json_decode($aCacheTmp, 1);
	        $aReturn = $aJson[0];
	    }else{


	        if ($_SERVER['REMOTE_ADDR']=="172.17.0.1") {//测试用
	            $sAdrServerIp = "222.67.215.54";//上海IP, 1
	        }elseif($oSra->isProxyAdr($oSra->instants['adr_id'])){
	        	$sAdrServerIp = $sVpnremoteip;
	        }else{
	            $sAdrServerIp = \YcheukfCommon\Lib\Functions::getRemoteClientIp();
	        }
	        $sCurrentVpnAcc = $oSra->getVpnData($oSra->instants['adr_id']);

	        // 判断是否直连模式
	        // 未分配VPN
	        if ($sPostAdrtype==LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021 && empty($sCurrentVpnAcc) && empty($sPostVpnAcc)) {
	            // 存在本地IP 且 本地IP==请求IP
	            
	            $bVpnStat = LAF_CALLVPNSTAT_DERECT_603;
	            return [];
	        }

	        // 上传的VPN IP 与 lur实际读到的IP 一致 (避免adr vpn 中途失效的情况)
	        // if (!empty($sVpnremoteip) && $sVpnremoteip==$sAdrServerIp) {
	        // 预设vpn信息 与 上传的 vpn信息一致


	        // if ($oSra->instants['adr_id']==343) {
	        //     exec("echo 'sVpnremoteip=".$sVpnremoteip."' >> /tmp/feng.".$oSra->instants['adr_id']);
	        //     exec("echo 'sCurrentVpnAcc=".$sCurrentVpnAcc."' >> /tmp/feng.".$oSra->instants['adr_id']);
	        //     exec("echo 'sPostVpnAcc=".$sPostVpnAcc."' >> /tmp/feng.".$oSra->instants['adr_id']);
	            

    // $oSra->log([$sCurrentVpnAcc, $sPostVpnAcc, $sCurrentVpnAcc]);

	        // }
	        if (
	        	!empty($sCurrentVpnAcc) 
	        	&& !empty($sPostVpnAcc) 
	        	&& $sPostVpnAcc==$sCurrentVpnAcc
	        	&& !empty($sVpnremoteip)
	        ){
	            $sAdrServerIp = $sVpnremoteip;
	            $bVpnStat = LAF_CALLVPNSTAT_OK_601;
	        }

	        // 存储上报上来的 vpnacc, 以供重播使用
	        if (!empty($sPostVpnAcc)) {
	            \Sra2Redis::saveCache($oSra->redis, LAF_CACHE_VPNLOCALACC_116.$oSra->instants['adr_id'], trim($sPostVpnAcc), 300);
	        }
	        
	        $aReturn = $oSra->getJobsByType($bVpnStat,$oSra->instants['adr_id'], $oSra->getAdrServerType($oSra->instants['adr_id']), $sAdrServerIp, $aIgnorecoids);

	        if (count($aReturn)) {
	            $aLogData = json_encode(array($aReturn, date("Y-m-d H:i:s"), $_GET));
	            $oAddtionalRedis->hSet($sTradeNoCacheKey, $_GET['tradeno'], $aLogData);
	            $oAddtionalRedis->set(LAF_CACHE_ADDTIONAL_LASTRECORD_GET_PRE_308.$oSra->instants['adr_id'], $aLogData);

	            // 记录历史交易
	            $aLogData = json_encode(array($aReturn, date("Y-m-d H:i:s")));
	            $sHisTradeCacheKey = LAF_CACHE_ADDTIONAL_GETLOG_PRE_312.date("Ymd")."/".$oSra->instants['adr_id'];
	            $oAddtionalRedis->rpush($sHisTradeCacheKey, $aLogData);
	            if ($oAddtionalRedis->ttl($sHisTradeCacheKey)<3600) {
	                $oAddtionalRedis->setTimeout($sHisTradeCacheKey, 86400*1.5);
	            }
	        }
	    }

	    if ($oAddtionalRedis->ttl($sTradeNoCacheKey)<3600) {
	        $oAddtionalRedis->setTimeout($sTradeNoCacheKey, 86400);
	    }

	}
	return $aReturn;
}


function _do_send($oSra)
{
	$aPostData = $oSra->instants['postdata'];

    // 直连pi3 ,屏蔽
    if (in_array($oSra->instants['adr_id'], [9,10,11,17,22,23,24,26,31,33,37,38,39,41,43,44,45,46,47,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94])) {
        return [];
    }
    // 最后时刻不再请求任务, 避免跨天
    // $oTimeDiff = (\YcheukfCommon\Lib\Functions::getTimeSecondDiff(date("H:i:s"), "23:59:59"));
    // if ($oTimeDiff && $oTimeDiff->interval<5) {

    //     return true;
    // }


    $oAddtionalRedis = $oSra->addtionalredis;
    
    if (count($aPostData) && isset($aPostData['data'])  && isset($_GET['tradeno'])  && count($aPostData['data'])) {

        $sCurrentDateTime = date("Y-m-d H:i:s");
        $sCurrentDate = isset($aPostData['date']) ? $aPostData['date'] :  date("Ymd");


        $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_TRADENO_SEND_302."/".$sCurrentDate;
        $sHisTradeCacheKey = LAF_CACHE_ADDTIONAL_SENDLOG_PRE_311.$sCurrentDate."/".$oSra->instants['adr_id'];
        
        if (!$oAddtionalRedis->hExists($sTradeNoCacheKey, $_GET['tradeno'])) {

            $sCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".$sCurrentDate;
            foreach ($aPostData['data'] as $sJobkey => $nCount) {
                list($p1, $p2, $p3, $p4) = explode("-", $sJobkey);
                $sJobkey = $p1."-".$p2."-9999-".$p4;

                // truck
                if ($oSra->getAdrServerType($oSra->instants['adr_id']) == LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009) {
                    $oAddtionalRedis->hIncrBy($sCacheKey, $sJobkey, $nCount);

                    $oSra->incTruckSendHourCounter($oSra->instants['adr_id'], $sCurrentDate, $nCount);


                }else{// 非truck
                    $bGetInitFlag = $oSra->getGetInitFlag($oSra->instants['adr_id'], $p1);
                    if ($bGetInitFlag) { // 先有get
                        # code...
                      $oAddtionalRedis->hIncrBy($sCacheKey, $sJobkey, $nCount);
                    }else{
                        // 跨天的提交, 都归到昨天的池子

                        $sCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".date("Ymd", strtotime("-1 day"));
                        $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_TRADENO_SEND_302."/".date("Ymd", strtotime("-1 day"));
                        $sHisTradeCacheKey = LAF_CACHE_ADDTIONAL_SENDLOG_PRE_311.date("Ymd", strtotime("-1 day"))."/".$oSra->instants['adr_id'];

                        $oAddtionalRedis->hIncrBy($sCacheKey, $sJobkey, $nCount);
                        // exec("echo '".date("Ymd H:i:s").",".$oSra->instants['adr_id'].','.$sJobkey.','.$nCount."' >> /tmp/error.addtional.send.".date("Ymd"));
                    }
                }


            }
            if ($oAddtionalRedis->ttl($sCacheKey)<3600) {
                $oAddtionalRedis->setTimeout($sCacheKey, 86400*3);
            }

            // 记录某ADR最后一次交易, 按trade no
            $aLogData = json_encode(array($aPostData['data'], $sCurrentDateTime, $_GET));
            $oAddtionalRedis->hSet($sTradeNoCacheKey, $_GET['tradeno'], $aLogData);
            $oAddtionalRedis->set(LAF_CACHE_ADDTIONAL_LASTRECORD_SEND_PRE_307.$oSra->instants['adr_id'], $aLogData);

            // 记录某ADR最后一次交易, 按 时间顺序
            $aLogData = json_encode(array($aPostData['data'], $sCurrentDateTime));
            $oAddtionalRedis->rpush($sHisTradeCacheKey, $aLogData);
            if ($oAddtionalRedis->ttl($sHisTradeCacheKey)<3600) {
                $oAddtionalRedis->setTimeout($sHisTradeCacheKey, 86400*1.5);
            }
            
        }

        if ($oAddtionalRedis->ttl($sTradeNoCacheKey)<3600) {
            $oAddtionalRedis->setTimeout($sTradeNoCacheKey, 86400*3);
        }

    }
    return 1;
}