<?php
/**
 * adr配置
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug = 1;
try{
    $oSra->init();

    $sCacheKey = "restapi_adrconfig";

    $cachedata = \Sra2Redis::getCache($oSra->redis, $sCacheKey);

    $aReturn = [];
    if ($cachedata && is_array($cachedata) && count($cachedata)) {
        $aReturn = $cachedata;
    }



    $oSra->httpresponse($aReturn, 200, true);

}catch(\Exception $e){
    $oSra->response_error($e->getMessage(), $e->getCode());

}