<?php
/**
 * 补量 服务
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug=1;

try{
	$oSra->init();
    $oSra->chkToken();

	if (!isset($_GET["ip"])) {
		throw new \Exception("Error GET:".json_encode($_GET), 1);
	}
    $ip=$_GET["ip"];

    if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
		throw new \Exception("BAD IP:".json_encode($_GET), 1);
    }

    $aReturn = $oSra->isVpnIpCalledTime($ip, date("Ymd"));
    $oSra->httpresponse($aReturn, 200, true);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}

