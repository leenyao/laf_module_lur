<?php
/**
 * 状态上报 服务
 */
include_once(__DIR__."/simplerestapi2.inc.php");
$oRsa = new SimpleRestApi2();
$oRsa->bDebug =1;
try{
    $oRsa->init();

    // 存储uv 日志
    // $oRsa->log(($oRsa->instants['postdata']));

    // 将没有取过code的url加入列表
    $fSaveUvUrlFunc = function($data) use ($oRsa)
    {
        $urls = [$data['url'], $data['url2']];
        // $oRsa->log($urls, 'fSaveUvUrlFunc');
        foreach ($urls as $url) {
            if (!empty($url) && strlen($url)>5) {

                $cachekey = LAF_CACHE_UVPOOL_UNMATCHURL_326.date("Ymd");

                $s = $url;
                $s = preg_replace('/\[adrid=\d+\]/i', '', $s);   
                $s = preg_replace('/\[adrsplitid=\w+\]/i', '', $s);   
                $s = preg_replace('/\[adrcookie\]/i', '', $s);   

        // $oRsa->log($cachekey, 'fSaveUvUrlFunc');
        // $oRsa->log($s, 'fSaveUvUrlFunc');
                
                $oRsa->addtionalredis->sAdd($cachekey, $s);
                if ($oRsa->addtionalredis->ttl($cachekey) < 86400) {
                    $oRsa->addtionalredis->setTimeout($cachekey,86400*3); 
                }
            }
        }
    };


    if (count($oRsa->instants['postdata'])) {
        foreach ($oRsa->instants['postdata'] as $row) {
            if (isset($row['ctid'])
                && is_numeric($row['ctid'])
            ) {
                $cachekey = LAF_CACHE_UVPOOLBYCT_327.date("Ymd").'/'.$row['ctid'];
                $oRsa->addtionalredis->lPush($cachekey, json_encode($row));

                if ($oRsa->addtionalredis->ttl($cachekey) < 86400) {
                    $oRsa->addtionalredis->setTimeout($cachekey,86400*3); 
                }

            }
            $fSaveUvUrlFunc($row);
            //unset($fSaveUvUrlFunc);
        }
    }

    $aReturn = [];


    $oRsa->httpresponse($aReturn, 200, true);

}catch(\Exception $e){

    $oRsa->response_error($e->getMessage(), $e->getCode());

}