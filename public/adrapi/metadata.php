<?php
/**
 * 统计
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug=1;

try{
    $oSra->init();
    $oSra->chkToken();

    $m1026 = \Sra2Db::getMLabels($oSra->pdo_slave, 1026);

    $oSra->httpresponse($m1026, 200);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}

