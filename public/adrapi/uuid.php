<?php
/**
 * UUID 服务
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSimpleRestApi = new SimpleRestApi2();
try{
    $oSimpleRestApi->chkToken();
    

    $oUuid = new \Lur\Service\Uuid();
    $oUuid->setReids($oSimpleRestApi->uuidredis);
    $nAdrId = isset($_GET['i']) ? $_GET['i'] : null;
    if (is_null($nAdrId)) {
        throw new \Exception("unavailbe:".$nAdrId, 1);
    }
    $sIp = isset($_GET['ip']) ? $_GET['ip'] : null;
    // print_r($_REQUEST);

    switch ($_GET['act']) {
        case 'genpool': //取自增池子, 需要加锁 
            $aReturn = $oUuid->getGenPool($nAdrId, $sIp);
            if (!is_array($aReturn)) {
                throw new \Exception("the ip is locked:".$sIp, 2);
            }
            $oSimpleRestApi->httpresponse($aReturn, 200, true);
            break;
        case 'set_genpool': //更新自增池
            $oUuid->updGenPool($nAdrId, $_POST['data']);
            $oSimpleRestApi->httpresponse(1, 200, false);
            break;
        
        default:
            throw new \Exception("no such act:".$sMsg, 1);
            break;
    }


}catch(\Exception $e){

    $oSimpleRestApi->response_error($e->getMessage(), $e->getCode());

}