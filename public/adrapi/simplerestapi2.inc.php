<?php

set_time_limit(0);
chdir(('/app'));

spl_autoload_register(function($class)
{
    if ($class == 'YcheukfCommon\Lib\Functions') {
        $path = '/app/module/ycheukf/Common/src/Common/Lib/Functions.php';
        include_once $path;
    }
    if ($class == 'YcheukfCommon\Lib\RedisMq') {
        $path = '/app/module/ycheukf/Common/src/Common/Lib/RedisMq.php';
        include_once $path;
    }
    if ($class == 'Lur\Service\Uuid') {
        $path = '/app/module/Lur/src/Lur/Service/Uuid.php';
        include_once $path;
    }
});

require '/app/init_autoloader.php';
include_once __DIR__.'/library.inc.php';
include_once __DIR__."/redis.inc.php";
include_once '/app/module/Lur/config/module.config.php';

class SimpleRestApi2 
{
    use Sra2LibraryTrait;
    var $instants=[];
    var $bDebug=0;
    function __construct()
    {
        $this->instants['adr_id'] = isset($_GET['i']) ? $_GET['i'] : null;
    }

    public function init(){
        $this->chkToken();
        $this->_init_post();
        $this->_init_adr();
    }


    function _init_adr() {
        $this->instants['adr_type'] = $this->getAdrServerType($this->instants['adr_id']);
        $this->add2AliveList($this->instants['adr_id']);

    }


    function _init_post() {
        $postdata = file_get_contents("php://input"); 
        $postdata = trim($postdata);
        if (!empty($postdata)) {
            $aPostData = json_decode($postdata, 1);
            if(is_null($aPostData)){
                throw new \Exception("error postdata:".$postdata, 1);
            }
            $this->instants['postdata'] = $aPostData;
        }
    }


    public function chkToken()
    {
        if ($this->bDebug==1) {
            return true;
        }

        $nAdrId = isset($_GET['token_i']) ? $_GET['token_i'] : (isset($_GET['i'])?$_GET['i']:null);

        if (
            is_null($nAdrId)
            || !isset($_GET['ei'])
            || !isset($_GET['token_v'])
            || !isset($_GET['token_t'])
        ) {
            throw new \Exception("miss token params", 1);
        }

        $this->instants['adr_id'] = $nAdrId;
        $sEncrytId = \YcheukfCommon\Lib\Functions::encryptBySlat($nAdrId);
        $bFlag = \YcheukfCommon\Lib\Functions::chkLyDynamicToken($_GET['token_t'],  $_GET['token_v']);
        // $this->log([$sEncrytId, $nAdrId, $bFlag]);

        if (!$bFlag || $sEncrytId!=$_GET['ei']) {
            throw new \Exception("token verification failed", 1);
        }
        $nCurrentTime = time();
        if (($_GET['token_t']>($nCurrentTime+600) || $_GET['token_t']<($nCurrentTime-600))) {//时间和服务器对不上报错
            throw new \Exception("timespan check faild; token_t(".$_GET['token_t'].") - servertimespan(".$nCurrentTime.") = ".($nCurrentTime-$_GET['token_t']), 1);
        }
        return true;
    }


    function response_error($sMsg="", $nCode=1){
        if ($nCode == 1) {
            exec(" echo '[".date("Y-m-d H:i:s")."]\tadrapierror\t".(is_array($sMsg) ? json_encode($sMsg) : addslashes($sMsg))."\t".$_SERVER['REQUEST_URI']."' >> /app/php_errors.log");
        }
        $this->httpjsonresponse($sMsg, 500, false);
    }

    function log($sMsg="", $keys='log'){
        if ($this->bDebug == 1) {
            exec(" echo '[".date("Y-m-d H:i:s")."]\t".$keys."\t".(is_array($sMsg) ? json_encode($sMsg) : addslashes($sMsg))."\t".$_SERVER['REQUEST_URI']."' >> /app/data/simplerestapi2.log");
        }
    }
    function httpjsonresponse($sMsg="", $nCode=1, $xData=array()){
        $this->httpresponse(array(
            "code" => $nCode,
            "msg" => $sMsg,
            "data" => $xData,
        ));
    }


    function httpresponse($sMsg='', $nStatus=\Zend\Http\PhpEnvironment\Response::STATUS_CODE_200, $bJsonFlag=true)
    {

        $response = new \Zend\Http\PhpEnvironment\Response();
        $response->setStatusCode($nStatus);
        $response->getHeaders()->addHeaders(array(
                'Access-Control-Allow-Origin' => '*'
            )
        );

        if ($bJsonFlag) {
            $response->getHeaders()->addHeaders(array(
                    'Content-type' => 'application/json'
                )
            );
            $sMsg = json_encode($sMsg);

        }
        $response->setContent($sMsg);

        $response->send();
        exit;
    }

    public function initRedis($host, $port, $db)
    {
        try{
            // var_dump(1);
            // $this->log($host);
            $oReturn = new \YcheukfCommon\Lib\RedisMq();

            $isok = $oReturn->connect($host, $port);
            $oReturn->select($db);
            if (!$isok) {
                throw new \RedisException('connect client error');
            }
        }catch(\RedisException $e){
            throw new \Exception('connect client error:'.$host.":".$port);
        }
        return $oReturn;
    }

    static function initPdo($sType='db_master'){
        $aPdoConfig = require '/app/config/autoload/local.php';

        $oGlobalPDO =  @new \PDO($aPdoConfig[$sType]['dsn'], $aPdoConfig[$sType]['username'], $aPdoConfig[$sType]['password'], array(\PDO::ATTR_PERSISTENT => true));
        @$oGlobalPDO->query($aPdoConfig[$sType]['driver_options'][\PDO::MYSQL_ATTR_INIT_COMMAND]);
        return $oGlobalPDO;
    }
    function __get($name='')
    {


        if (isset($this->instants[$name])) {
            return $this->instants[$name];
        }
        switch ($name) {
            // case 'pdo':
            //     $this->instants[$name] = self::initPdo();
            // break;
            case 'pdo_slave':
                $this->instants[$name] = self::initPdo('db_slave');
            break;
            case 'redis':
                $aRedisConfig = require '/app/config/autoload/local.php';
                $this->instants[$name] = $this->initRedis($aRedisConfig['cache_config']['params']['servers'][0][0], $aRedisConfig['cache_config']['params']['servers'][0][1], $aRedisConfig['cache_config']['params']['servers'][0][2]);
                break;
            case 'uuidredis':
            case 'addtionalredis':
                $aRedisConfig = require '/app/config/autoload/local.php';
                $this->instants[$name] = $this->initRedis($aRedisConfig['addtionalredis']['host'], $aRedisConfig['addtionalredis']['port'], $aRedisConfig['addtionalredis']['db']);
                break;

            case 'configs':
            case 'config':
                $configs = require '/app/config/autoload/local.php';
                
                $this->instants[$name] = $configs;
                break;

            case 'cncode':
                $configs = require '/app/config/autoload/cncode.global.php';
                return $configs;

            default:
                throw new \Exception("no such var:".$name, 1);
                break;
        }
        return $this->instants[$name];
    }

}




