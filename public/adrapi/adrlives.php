<?php
/**
 * 状态上报 服务
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug = 1;
try{
    $oSra->init();
    
    // $aAdrSummary = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrSummary();
    $aReturn = [];
    $aReturn["addtional_adrcityid"] = [];

    // $oSra->log($oSra->postdata);

    $oSra->httpresponse($aReturn, 200, true);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}