<?php
/**
 * 检查 adr_trolley_release 是否有新版本
 */



include_once(__DIR__."/simplerestapi2.inc.php");
$oSimpleRestApi = new SimpleRestApi2();
try{
// var_dump($oSimpleRestApi->redis->info());

	// 判断token合法性
	if (!isset($_GET['token_t']) || !isset($_GET['token_v'])) {
		throw new Exception("miss token params", 1);
	}
	$bFlag = \YcheukfCommon\Lib\Functions::chkLyDynamicToken($_GET['token_t'],  $_GET['token_v'], "leenyao");
	if ($bFlag !== true) {
		throw new Exception("error token", 1);
	}
	$sVersion = isset($_GET['version']) ? $_GET['version'] : "release";
	$sDir = "/app/module/Lur/src/Lur/Script/gensdkconfig/release/";


	/**
	 * #######closure function start#######
	 */
	$fGetLastestPackage = function($sDir=null) 
	{

	    $sLastestFile = "";
	    $aFiles = scandir($sDir, 1);
	    arsort($aFiles);
	    $aAll = [];
	    $bLastest = false;
	    foreach ($aFiles as $sFile) {
	        if (in_array($sFile, array('.', '..'))) {
	            continue;
	        }
	        if (preg_match("/^\d+$/i", $sFile)) {
		        if ($bLastest==false) {
		            $sLastestFile = $sFile;
		            $bLastest = true;
		        }else{
		        }
		        $aAll[$sFile] = md5_file($sDir.$sFile);
	        }
	    }
	    return $aAll;
	};
	$aAllPackages = $fGetLastestPackage($sDir);


// var_dump($sDownloadFile);

	if (count($aAllPackages)) {
		$sAction = isset($_GET['act']) ? $_GET['act'] : "all";
		// $sAction = isset($_GET['md5flag']) && $_GET['md5flag']==1 ? "chkmd5" : "download";
		switch ($sAction) {
			case 'all':
				$oSimpleRestApi->httpresponse(json_encode($aAllPackages), 200, false);
				break;
			case 'download':
					$sDownloadFile = $_GET['version'];
					if (!file_exists($sDir.$sDownloadFile)) {
						throw new \Exception("no such version:".$_GET['version'], 1);
					}
					ob_clean();
					header('Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0');
					header('Pragma: no-cache');
					header('Pragma: public');
					header('Content-Description: File Transfer');
					header('Content-Disposition: attachment; filename="'.($sDownloadFile).'"');
					header('Content-Type: text/comma-separated-values');
					header('Content-Transfer-Encoding: binary');
					header('Content-Length: ' . filesize($sDir.$sDownloadFile));
					readfile($sDir.$sDownloadFile);
				break;
			
			default:
				throw new \Exception("error action:".$sAction, 1);
				break;
		}
	}else{
		throw new \Exception("no suitable file", 1);
	}

}catch(\Exception $e){

    $oSimpleRestApi->response_error($e->getMessage(), $e->getCode());

}
