<?php
/**
 * 给下游供应商的plan 分配
 */

include_once(__DIR__."/../simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug=1;

try{

		// 曾经分配过给下游的
		function isEverDone($ophis)
		{
			$return = 0;
			foreach ($ophis as $raw) {
				$row = json_decode($raw, 1);
				if (intval($row['percent'])>0 || (isset($row['grep_configs']) && count($row['grep_configs']))) {
					$return = 1;
				}
			}
			return $return;
		}

    $oSra->init();
    $oSra->chkToken();

    $return = [];
    $lists = $oSra->redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_LIST_206);

    if (is_array($lists) && count($lists)) {
    	foreach ($lists as $plan => $json_s) {
				$ophis = $oSra->redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_OPHIS_210.$plan);
				// var_dump($ophis);
				if ($ophis && is_array($ophis) && count($ophis)) {
					krsort($ophis);
					$last = json_decode(current($ophis), 1);
					$return[$plan] = [];
					if (isEverDone($ophis)) {
						$return[$plan]['percent'] = intval($last['percent']);
						
						if (isset($last['grep_configs']) && count($last['grep_configs'])) {
							$return[$plan]['grep_configs'] = $last['grep_configs'];
						}
						if (isset($last['status'])) {
							$return[$plan]['status'] = $last['status'];
						}
					}
					if (empty($return[$plan])) {
						unset($return[$plan]);
					}
				}
    		
    	}
    }


    $oSra->httpresponse($return, 200);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}

