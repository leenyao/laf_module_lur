<?php
// 该API放在149上供下游使用
$debug = 1;
defined('LAF_WEBHOST') || define('LAF_WEBHOST', "xxxx.com");


function stopallplan($content_json){
    $return = $content_json;
    $ajson = json_decode($content_json, true);
    foreach ($ajson['data'] as $i => $row){
        $row['status'] = 1;
        $row['sign'] = md5($row['plan']);
        $ajson['data'][$i] = $row;
    }
    if (time() > strtotime("2021-03-31 23:35:00")){
    // if (time() > strtotime("2021-03-30 10:15:00")){
        file_put_contents(__DIR__."/debug", json_encode($ajson));
        $return = json_encode($ajson);
    }
    return $return;
}

try {
    
    $cachefile = "./mg.cache";
    $cachedata = getcache($cachefile);
    if ($cachedata) {
        header('content-type:application/json');
        // echo "cache\n";
        echo stopallplan($cachedata);
        exit;
    }

    $url = 'http://cx.da.mgtv.com/planList?cxid=6778_ry_0&key=a3703010714a4416a16c4141d3ac9c9f';

    $content = file_get_contents($url);
    $content_json = json_decode($content, 1);


    $url_match = 'http://'.LAF_WEBHOST.'/module/lur/adrapi/mg.supply/mg.supply.php';
    $content_match = file_get_contents($url_match);
    $content_match_json = json_decode($content_match, 1);

    // var_dump($content_match_json);
    // var_dump(count($content_json['data']));
    $return = [];
    if (count($content_json)) {
        foreach ($content_json['data'] as $key => $row) {
            if(isset($content_match_json[$row['plan']])){
    // var_dump($row['plan']);
    // var_dump($row['sign']);
    // var_dump($content_match_json[$row['plan']]);
                // unset($content_json['data'][$key]);
                $row['sign'] = md5($row['sign'].json_encode($content_match_json[$row['plan']]));
                $content_json['data'][$key] = $row;
            }else{
                unset($content_json['data'][$key]);
            }
        }
    }
    if (isset($content_json['data'])) {
        $content_json['data'] = array_values($content_json['data']);
    }
    // var_dump(count($content_json['data']));

    header('content-type:application/json');

    echo stopallplan(json_encode($content_json));

    file_put_contents($cachefile, json_encode($content_json));
} catch (\Exception $e) {
    header('content-type:application/json');
    echo json_encode(["status"=>1, "msg"=>$e->getMessage(), "data"=>[]]);
    // echo $e->getMessage();
}

function getcache($file)
{
    if (file_exists($file)) {
        $t1 = filemtime($file);
        if (time()-$t1 <120) {
            return file_get_contents($file);
        }
    }
    return false;
}