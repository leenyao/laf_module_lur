<?php
// 该API放在149上供下游使用
$debug = 0;
defined('LAF_WEBHOST') || define('LAF_WEBHOST', "xxxx.com");


try {

    if ($debug) {
        $plan = "1030020";
    }else{
        $plan = $_GET['plan'];
    }
    $cachefile = "./mg.plan.cache.".$plan;
    $cachedata = getcache($cachefile);
    if ($cachedata) {
        header('content-type:application/json');
        // echo "cache\n";
        echo $cachedata;
        exit;
    }

    $url = 'http://cx.da.mgtv.com/planDetail?cxid=6778_ry_0&key=a3703010714a4416a16c4141d3ac9c9f&plan='.$plan;

    $url_match = 'http://'.LAF_WEBHOST.'/module/lur/adrapi/mg.supply/mg.supply.php';
    $content_match = file_get_contents($url_match);
    $content_match_json = json_decode($content_match, 1);

    if(!isset($content_match_json[$plan])){
        throw new \Exception('no such plan:'.$plan);
        // unset($content_json['data'][$key]);
    }

    // $percent = intval($content_match_json[$plan]);
    // var_dump($content_match_json[$plan]);

    $content = file_get_contents($url);
    $content_tmp = json_decode($content, 1);
    if (isset($content_tmp['data'])
        && isset($content_tmp['data']['task'])
        && count($content_tmp['data']['task'])
    ) {
        foreach ($content_tmp['data']['task'] as $k => $row) {
            
            foreach ($row['list'] as $k2 => $row2) {
                // $row['list'][$k2]['num'] = intval($row2['num']*($percent/100));
                $row['list'][$k2]['num'] = getNumByGrep(intval($row2['num']), $content_match_json[$plan]['percent'], 0, $row['date'], $row2['plt'], (isset($content_match_json[$plan]['grep_configs'])?$content_match_json[$plan]['grep_configs']:[]));
            }
            $content_tmp['data']['task'][$k] = $row;
        }
        $content = json_encode($content_tmp);
    }



    header('content-type:application/json');
    echo $content;
    file_put_contents($cachefile, $content);
} catch (\Exception $e) {
    header('content-type:application/json');
    echo json_encode(["status"=>1, "msg"=>$e->getMessage(), "data"=>[]]);
}


function getNumByGrep($num, $defaultpercent, $stop, $date, $ua, $grep_configs){
    $ua = strtolower($ua);
    if ($stop) {
        return intval($num);
    }
    if (empty($grep_configs)) {
        return intval($num*$defaultpercent/100);
    }else{
        foreach ($grep_configs as $row) {
            $matcheds = [];
            $matcheds['d'] = 1; //日期判断
            $matcheds['u'] = 1; //ua判断

            if (
                isset($row['d']) 
                && count($row['d'])
                && (strtotime($date) < strtotime($row['d'][0]) ||  strtotime($date) > strtotime($row['d'][1]))
            ) {
                $matcheds['d'] = 0;
            }

            if (
                isset($row['u']) 
                && count($row['u'])
                && !in_array($ua, $row['u'])
            ) {
                $matcheds['u'] = 0;
            }
            // var_dump($row, $matcheds);
            if (array_sum($matcheds) == 2) {//满足条件
                return intval($num*($row['p'])/100);
            }
        }
        // 没有命中任何条件, 走默认
        return intval($num*$defaultpercent/100);
    }
}

function getcache($file)
{
    if (file_exists($file)) {
        $t1 = filemtime($file);
        if (time()-$t1 <120) {
            return file_get_contents($file);
        }
    }
    return false;
}