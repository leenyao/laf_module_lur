<?php
/**
 * 给下游供应商的notice 接口
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug=1;

try{
    $oSra->init();
    $oSra->chkToken();
    $return = 1;

    $plan = $_GET['plan'];
    $type = $_GET['type'];

    if (in_array($type, ['published'])) {


    	$lists = $oSra->redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_LIST_206);

    	if (is_array($lists) && isset($lists[$plan])) {
	    	$oSra->redis->lPush(LAF_CACHE_HNSCHEDULEAPI_NOTICES_211.$plan.$type, date("Y-m-d H:i:s"));
	    	$return = ['code'=>1, 'msg'=>'ok', 'data'=>[]];
		    $oSra->httpresponse($return, 200);
    	}
    	throw new \Exception('bad params');

    	
    }else{
    	throw new \Exception('server error');
    }



}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}

