<?php
/**
 * 状态上报 服务
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug = 1;
try{
    $oSra->init();
    $bIsBlackIp = $oSra->updBlackIpCounter($oSra->instants['adr_id'],$oSra->instants['postdata']);
    $oSra->saveLastAdrStats($oSra->instants['postdata']);

    $aReturn = [];
    $aReturn["addtional_adrcityid"] = $oSra->getCityidByAdrid($oSra->instants['adr_id']);
    $aReturn["adrserver_type"] =  $oSra->getAdrServerType($oSra->instants['adr_id']);
    $aReturn["is_blackip"] =  $bIsBlackIp===true?1:0;
    $sVPNData = $oSra->getVpnData($oSra->instants['adr_id']);
    $aReturn["vpnacc"] = ($sVPNData) && !empty($sVPNData) ? $sVPNData : "";

    $aReturn["province2city"] =  $oSra->getprovince2city();

    $aReturn["proxyconf"] =  $oSra->getProxyconf();
    
    // 是有需要跑的量, 有的话再启用ip vpn/proxy
    $aReturn["addrunable"] =  $oSra->isAddtionalRunable($oSra->instants['adr_id']);

    $adrHost = $oSra->getAdrId2Host($oSra->instants['adr_id']);
    $tmp = $oSra->getShellScript($adrHost);
    if (!empty($tmp)) {
        $aReturn["shellscript"] = $tmp;
    }

    // $oSra->log($oSra->postdata);

    $oSra->httpresponse($aReturn, 200, true);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}