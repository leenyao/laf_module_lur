<?php
/**
 * 补量 服务
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug=0;

try{
	$oSra->init();
    // $oSra->chkToken();

	if (!isset($_GET["ip"])) {
		throw new \Exception("Error GET:".json_encode($_GET), 1);
	}
    $ip=$_GET["ip"];

    if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
		throw new \Exception("BAD IP:".json_encode($_GET), 1);
    }

    $bIsBlackIp = $oSra->isBlackIp($ip);

    $oSra->httpresponse($bIsBlackIp?1:0, 200, true);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}

