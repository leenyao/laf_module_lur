<?php

class  Sra2Db 
{

    static function getMLabel($pdo, $type, $resid){

        $sSql = "select label from system_metadata where type=:type and resid=:resid ";
        $sth = $pdo->prepare($sSql);
        $sth->execute([
            ":type" => $type,
            ":resid" => $resid,
        ]);
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        return $result['label'] ?:null;

    }
    static function getMLabels($pdo, $type){

        $sSql = "select label from system_metadata where type=:type and status=1 ";
        $sth = $pdo->prepare($sSql);
        $sth->execute([
            ":type" => $type,
        ]);
        $results = $sth->fetchAll(\PDO::FETCH_ASSOC);


        $return = [];
        if ($results && count($results)) {
            foreach ($results as $row) {
                $return[] = $row['label'];
            }
            
        }

        return $return;

    }

}

