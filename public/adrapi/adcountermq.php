<?php
/**
 * 统计
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
// $oSra->bDebug=1;

try{
	$oSra->init();
    $oSra->chkToken();

    if (!isset($oSra->instants['postdata'])) {
    	throw new \Exception("Error Processing Request", 1);
    	
    }

    $aPostData = $oSra->instants['postdata'];
    $oSra->log($aPostData);
    // 计数器进入哪个队列
    $aMqTypeMaps = array("adschedule_counter", "b_adcounter_ssp_mango");// 默认adr, ssp芒果
    $nMqType = isset($aPostData['type']) && in_array($aPostData['type'], $aMqTypeMaps)? $aPostData['type'] : $aMqTypeMaps[0];
    if (count($aPostData) && isset($aPostData['data'])  && isset($aPostData['adrserverid']) && count($aPostData['data'])) {
        foreach ($aPostData['data'] as $sDate => $aRowTmp) {
            foreach ($aRowTmp as $sId => $nCount) {
                $aTmp = array(
                    $aPostData['adrserverid'],
                    $sDate,
                    $sId,
                    $nCount,
                );
                $sKeyString = join("/", $aTmp);
                // var_dump($sKeyString);
                sendMQ($oSra->redis, $sKeyString, $nMqType);
            }
        }
    }
    $oSra->httpresponse(1, 200);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}

function sendMQ($oRedis, $sMessage, $sQueue){
    $sQueueName = "/Lur/queue/".$sQueue;
    $bFlag = $oRedis->zAdd($sQueueName, 0, json_encode($sMessage));;
}

