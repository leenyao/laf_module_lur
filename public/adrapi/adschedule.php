<?php
/**
 * 状态上报 服务
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug = 0;
try{
    $oSra->init();
    $aReturn = [];
    $bReqUrl = true;


    $nCodeId = (isset($_GET['id']) && !empty($_GET['id']) ) ? $_GET['id'] : 0;

    // if ($nCodeId == 'code') {
    //     throw new \Exception('badbad code:'.$nCodeId);
    // }
    if (!empty($nCodeId)) {

        $sCacheKey = LAF_CACHE_RESAPI_AD_SINGLE_132.$nCodeId;

        $aCacheData = \Sra2Redis::getCache($oSra->redis, $sCacheKey);
        if ($aCacheData && $aCacheData[0]) {

            $adscheduel = $aCacheData[0];
            $nTotal = $aCacheData[1];
            $sCacheTime = isset($aCacheData[2]) ? $aCacheData[2] : "";

            $adscheduel = __trigger_fmtReturnValue($adscheduel, $oSra->redis);
            $aReturn = json_encode([
                'dataset'=>$adscheduel, 
                'md5'=>md5(json_encode($adscheduel)), 
                'count'=>$nTotal, 
                'laf-cachekey'=>$sCacheKey, 
                'laf-cacheflag'=>true, 
                'laf-cachetime'=>$sCacheTime
            ]);
            // $oSra->log($aReturn, 'aCacheData aReturn');
            $bReqUrl = false;
        }

    }

    if ($bReqUrl && isset($_SERVER['QUERY_STRING'])) {
        if (isset($_GET['id']) && !is_numeric($_GET['id']) ) {
            throw new \Exception('bad code id:'.json_encode($_SERVER));
        }
        // 并发20
        if (isset($oSra->instants['adr_id'])) {
            $cachekeylock = "";

            $json_s = ($oSra->redis->get(LAF_CACHE_ONLINE_ADRTRUCKS_316));
            $json_arr = json_decode($json_s, 1);
            $bHit = false;

            // 是否truck
            if (isset($json_arr[$oSra->instants['adr_id']])) {
                $bHit = true;
            }else{
                $cachekeylock =  "adlock_".($oSra->instants['adr_id']%50);
                $bLockFlag = $oSra->redis->setNx($cachekeylock, 1);
                if ($bLockFlag) {
                    $bHit = true;
                }

            // $oSra->log($bLockFlag, 'cachekeylock');

            }


            if ($bHit) {
                $url = 'http://127.0.0.1/restapi.php/adschedule?'.$_SERVER['QUERY_STRING'];
                $aReturn = file_get_contents($url);
                if (!empty($cachekeylock)) {
                    $oSra->redis->del($cachekeylock);
                }
            }else{
                throw new \Exception('lock by other adr');
            }
        }
        // }else{

        // }
    }


    $oSra->httpresponse($aReturn, 200, false);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}


 function __trigger_fmtReturnValue($a='', $oCacheObject)
{
    $nYestodayDateTimeWaterLine = strtotime("-1 day", time());//只加载三天数据
    $nDateTimeWaterLine = strtotime("+2 day", time());//只加载三天数据
    // var_dump(date("Y-m-d", $nDateTimeWaterLine));
    $aReturn = array();
    if (count($a)) {
        foreach ($a as $k1 => $a1) {

            if ($a1['id'] == 87949) {
                // \Application\Model\Common::onlinedebug('useiac', json_encode($a1));
            }
            // 将指定使用
            if (isset($a1['useiac']) && $a1['useiac']==1) { // 测试
                $oCacheObject->sAdd(LAF_CACHE_USEIACIP_ADSET_130, $a1['id']);
            }else{
                $oCacheObject->sRem(LAF_CACHE_USEIACIP_ADSET_130, $a1['id']);
            }


            $aNewA1 = $a1;
            $Adschedule = array();
            foreach ($a1['schedule'] as $k2 => $a2) {
                $a2[0] = preg_replace("/\s+/i", " ", trim($a2[0]));
                if (preg_match("/,/i", $a2[0])) {
                }else{
                    $a3 = explode(" ", $a2[0]);
                    if (strtotime($a3[0]) > $nDateTimeWaterLine) {
                        continue;
                    }
                    if (strtotime($a3[0]) < $nYestodayDateTimeWaterLine) {
                        continue;
                    }
                }

                $Adschedule[$k2] = $a2;
            }
            $aNewA1['schedule'] = $Adschedule;
            $aReturn[$k1] = $aNewA1;
        }
    }
    return $aReturn;
}