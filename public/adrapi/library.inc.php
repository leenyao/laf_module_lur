<?php

include_once(__DIR__."/redis.inc.php");
include_once(__DIR__."/db.inc.php");
trait Sra2LibraryTrait 
{



    public function getJobsByType($bVpnStat, $nAdrServerId, $sAdrServerType, $sAdrServerIp, $aIgnorecoids=array())
    {

        $aConfig = $this->config;
        $sAddtionalDbCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".date("Ymd");
        $nCurrentHour = date("G");

        

        $aReturn = array();
        // if($nAdrServerId == 42)


        // 所允许的ID, 湖南测试
        $allowlids = [];
        // $allowlids = [169646,169647,169648,169649,169650,169651,169652,169653,169654,169655];
        switch (intval($sAdrServerType)) {

            case LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYPROXY_1027: // TROLLEY模式, 优先取城市

                // $ahunanids = [169646,169647,169648,169649,169650,169651,169652,169653,169654,169655];
                // if($nAdrServerId==1){
                //     $allowlids = [166575, 167838, 166575, 167585];
                // }

                $nMaxJobsPerTime = $nJobsPerTime = 100;//最多x条代码

                // $sCityIdIac = $this->getIacCityId($sAdrServerIp);

                // if($nAdrServerId == 42){
                    // $this->log([$bVpnStat], 'sAdrServerType');

                // }

                $sCityIdIac = $this->addtionalredis->get(LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306.$nAdrServerId);
                list($aMapCity, $aMapProvince, $aMapCountry, $nMapedCityId, $provinceid) = $this->getJobsByCityId($sCityIdIac, $aIgnorecoids, $nAdrServerId);



                // 加载城市
                list($aReturn, $nJobsPerTime) = $this->_fmtReturn($aMapCity, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, false, false, $allowlids);

                // if (in_array($nAdrServerId, [283])){
                    // $this->bDebug = 0;

                    if (!empty($provinceid)){
                        // 加载省份
                        list($aReturn, $nJobsPerTime) = $this->_fmtReturn($aMapProvince, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, $provinceid, false, $allowlids);

                    }
                    // $this->log([$aMapCountry, $aMapProvince, $aMapCity, $aReturn, $sCityIdIac,$nMapedCityId, $provinceid], 'sCityIdIac');
                // }

                // 把带宽都留给主要的代码, 加载x条全国代码
                list($aReturn, $nJobsPerTime) = $this->_fmtReturn($aMapCountry, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, false, false, $allowlids);
                

            break;

            case LAF_METADATATYPE_ADRSERVERTYPE_TROLLEY_1018: // TROLLEY模式, 优先取城市
            default:
                $nMaxJobsPerTime = $nJobsPerTime = 100;//最多x条代码

                $sCityIdIac = $this->getIacCityId($sAdrServerIp);

                // if($nAdrServerId == 386){
                //     $this->log([$sCityIdIac], 'sCityIdIac2');

                // }
                if ($bVpnStat === LAF_CALLVPNSTAT_OK_601) {//vpn 拨号成功状态, 则从现有的 adrid-vpncityidid中获取任务
                    $sCityIdIac = $this->addtionalredis->get(LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306.$nAdrServerId);
                }


                list($aMapCity, $aMapProvince, $aMapCountry, $nMapedCityId, $provinceid) = $this->getJobsByCityId($sCityIdIac, $aIgnorecoids, $nAdrServerId);


                // 加载城市
                list($aReturn, $nJobsPerTime) = $this->_fmtReturn($aMapCity, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, $sCityIdIac, $sAdrServerIp, $allowlids);

                // if (in_array($nAdrServerId, [283])){
                    // $this->bDebug = 0;

                    if (!empty($provinceid)){
                        // 加载省份
                        list($aReturn, $nJobsPerTime) = $this->_fmtReturn($aMapProvince, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, $provinceid, $sAdrServerIp, $allowlids);

                    }
                    // $this->log([$aMapCountry, $aMapProvince, $aMapCity, $aReturn, $sCityIdIac,$nMapedCityId, $provinceid], 'sCityIdIac');
                // }

                // 把带宽都留给主要的代码, 加载x条全国代码
                list($aReturn, $nJobsPerTime) = $this->_fmtReturn($aMapCountry, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, false, false, $allowlids);
                

                // 更新ADRID CITYID 关系
                $this->updAdridCityidMap($nMapedCityId, $nAdrServerId, $sAdrServerIp);

            break;
        }

        // 更新满载指标
        // $this->updLoadPercent($nMapedCityId, $nLoadPercentmolecular, $nLoadPercentDenominator);

        return $aReturn;
    }



    /**
     * 将关联的显示和点击一同返回 
     * 
    */
    function addClickRelation2List($aMapData)
    {
        $aMapDataReturn = $aMapData;
        $aShow2Click = \Sra2Redis::getCache($this->redis, LAF_CACHE_SHOWCLICK_RELATION_126);

        // 存在关联数组
        if ($aShow2Click && is_array($aShow2Click) && count($aShow2Click) && count($aMapData)) {
            $aClick2Show =  array_flip($aShow2Click);

            // 如果仅有点击, 去掉
            foreach ($aMapData as $sJobKey => $v) {
                $aSplitKey = explode('-', $sJobKey);
                $nCodeId = $aSplitKey[0];
                if (isset($aClick2Show[$nCodeId])) {
                    unset($aMapDataReturn[$sJobKey]);
                }
            }

            // 如果有显示且有点击, 组合返回
            foreach ($aMapDataReturn as $sJobKey => $v) {
                $aSplitKey = explode('-', $sJobKey);
                $nShowCodeId = $aSplitKey[0];
                if (isset($aShow2Click[$nShowCodeId])) { // 存在关联点击代码
                    $nClickCodeId = $aShow2Click[$nShowCodeId];


                    foreach ($aMapData as $sJobKey2 => $v2) {
                        $aSplitKey2 = explode('-', $sJobKey2);
                        $nCodeId = $aSplitKey2[0];
                        if ($nCodeId == $nClickCodeId) { // 存在点击代码补量
                            $aMapDataReturn[$sJobKey2] = $v2;
                        }
                    }
                }
                // print_r($aSplitKey);
            }
        }
        return $aMapDataReturn;
    }

    // 为truck 计算每小时上报的数
    public function incTruckSendHourCounter($nAdrServerId, $sCurrentDate, $nCount)
    {
        
        $sCounterStr = LAF_CACHE_ADRTRUCK_SENDCOUNTER_318.$nAdrServerId."/".$sCurrentDate;
        $this->addtionalredis->hIncrBy($sCounterStr, date("H"), $nCount);
    }

    public function getGetInitFlag($nAdrServerId, $nCodeId)
    {
        $sCacheKey = LAF_CACHE_ADDTIONAL_DATEINIT_315.date("Ymd");

        $oObject = $this->redis;
        return $oObject->sIsMember($sCacheKey, "$nAdrServerId,$nCodeId");
    }

    public function setGetInitFlag($nAdrServerId, $nCodeId)
    {
        $sCacheKey = LAF_CACHE_ADDTIONAL_DATEINIT_315.date("Ymd");

        $oObject = $this->redis;
        $oObject->sAdd($sCacheKey, "$nAdrServerId,$nCodeId");

        if ($oObject->ttl($sCacheKey)<86400) {
            $oObject->setTimeout($sCacheKey, 86400*5);
        }
    }

    // 添加到vpn的ip漂移列表
    public function add2VpnIpFloat($sCityIdIac, $nVpnCityId, $nCodeId, $sAdrServerIp){

        $sCacheKey = LAF_CACHE_VPNIP_FLOATHIS_319.date("Ymd");

        $oObject = $this->redis;
        $oObject->sAdd($sCacheKey, json_encode([$sCityIdIac, $nVpnCityId, $nCodeId, $sAdrServerIp]));

        if ($oObject->ttl($sCacheKey)<86400) {
            $oObject->setTimeout($sCacheKey, 86400*4);
        }
    }


    public function _isSspSourceType($codeid, $sourcetype){

        $sCacheKey2 = LAF_CACHE_RESAPI_AD_SINGLE_132.$codeid;
        $aCacheData2 = \Sra2Redis::getCache($this->redis, $sCacheKey2);
        if ($aCacheData2
            && isset($aCacheData2[0])
            && isset($aCacheData2[0][0])
            && isset($aCacheData2[0][0]['sourcetype'])
            && ($aCacheData2[0][0]['sourcetype']==$sourcetype)
        ) {
            return true;
        }
        return false;
    }

    // 属于 百孚思/郡州 
    public function _isSspMzBrandJunzhou($codeid){

        $sCacheKey2 = LAF_CACHE_RESAPI_AD_SINGLE_132.$codeid;
        $aCacheData2 = \Sra2Redis::getCache($this->redis, $sCacheKey2);
        if ($aCacheData2
            && isset($aCacheData2[0])
            && isset($aCacheData2[0][0])
            && isset($aCacheData2[0][0]['user_id'])
            && ($aCacheData2[0][0]['user_id']==2019)
        ) {
            return true;
        }
        return false;
    }


    public function _fmtReturn($aMapCity, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, $sCityIdIac=false, $sAdrServerIp=false, $allowlids=[])
    {

        $aMapCity = $this->addClickRelation2List($aMapCity);
        $oReids = $this->redis;





        if (count($aMapCity)) {

            // 是否有优先级
            $ispiority = false;
            foreach ($aMapCity as $sJobkey => $nCountTmp) {
                list($p1, $p2, $p3, $p4) = explode("-", $sJobkey);
                if (count($allowlids) && in_array($p1, $allowlids) && $nCountTmp>50){
                    $ispiority = true;
                    break;
                }
            }

                // if($nAdrServerId == 42){
                    // $this->log([$bVpnStat], 'sAdrServerType');

                // }
            foreach ($aMapCity as $sJobkey => $nCountTmp) {
                $bReturn = false;

                    list($p1, $p2, $p3, $p4) = explode("-", $sJobkey);


                    // $this->log([$allowlids, $p1, $sJobkey, $nAdrServerId], 'sAdrServerType2');

                    // 存在优先级, 则独占资源
                    if ($ispiority && count($allowlids) && !in_array($p1, $allowlids)){
                        continue;
                    }

                    if ($this->_isSspSourceType($p1, LAF_ADSOURCETYPE_SSP_HUNAN)) {
                        $nMaxTimePerJob = 5; //每一条代码在一次请求中最多可以取多少次
                        $nMaxCountPerTime = 30;// 一次任务最多包含的请求次数
                    }elseif ($this->_isSspSourceType($p1, LAF_ADSOURCETYPE_SSP_SOHU)) {
                        $nMaxTimePerJob = 3; //每一条代码在一次请求中最多可以取多少次
                        $nMaxCountPerTime = 50;// 一次任务最多包含的请求次数
                    }elseif ($this->_isSspMzBrandJunzhou($p1)) {
                        $nMaxTimePerJob = 2; //每一条代码在一次请求中最多可以取多少次
                        $nMaxCountPerTime = 120;// 一次任务最多包含的请求次数
                    }else{
                        $nMaxTimePerJob = 3; //每一条代码在一次请求中最多可以取多少次
                        $nMaxCountPerTime = 50;// 一次任务最多包含的请求次数

                    }



                    for ($i=0; $i < $nMaxTimePerJob; $i++) { 
                        if ($nJobsPerTime < 1) {
                            $bReturn = true;
                            break;
                        }

                        // 若该代码指定城市
                        
                        if ($p4!=LAF_CACHE_ADDTIONAL_ALLTICIES_9999
                            && $sCityIdIac
                            && $sAdrServerIp
                        ) {
                            // iac城市与vpn城市 不同
                            if ($sCityIdIac != $p4) {
                                $this->add2VpnIpFloat($sCityIdIac, $p4, $p1, $sAdrServerIp);
                                // 改代码使用严格的iac 模式
                                if ($oReids->sIsMember(LAF_CACHE_USEIACIP_ADSET_130, $p1)) {
                                    continue;
                                }
                                // \Application\Model\Common::onlinedebug($sJobkey."|".$nAdrServerId, $sCityIdIac);
                            }
                        }

                        $nCountTmp = $this->addtionalredis->hGet($sAddtionalDbCacheKey, $sJobkey);
                        if (is_numeric($nCountTmp) && intval($nCountTmp)>0) {//库存未用完
                            $nCountTmp = intval($nCountTmp);
                            

                            $sJobkey2 = $p1."-".$p2."-".$p3."-".$p4;
                        // exec("echo ".$nAdrServerId.','.$sJobkey2." >> /tmp/feng");
                            $this->setGetInitFlag($nAdrServerId, $p1);

                            $nCountTmp2 = $nCountTmp<$nMaxCountPerTime ? $nCountTmp : $nMaxCountPerTime;
                            $aReturn[$sJobkey2] = $nCountTmp2;
                            $this->addtionalredis->hIncrBy($sAddtionalDbCacheKey, $sJobkey, intval("-".$nCountTmp2));
                            $nJobsPerTime--;
                            $p3--;
                        }

                    }
                    if ($bReturn) {
                        break;
                    }

            }
        }
        return array($aReturn, $nJobsPerTime);
    }

    public function updAdridCityidMap($nMapedCityId, $nAdrServerId, $sAdrServerIp)
    {

        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306.$nAdrServerId;
        $this->addtionalredis->set($sCacheKey, $nMapedCityId);
        if ($this->addtionalredis->ttl($sCacheKey)<3600) {
            $this->addtionalredis->setTimeout($sCacheKey, 86400);
        }

        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2IP_PRE_309.$nAdrServerId;
        $this->addtionalredis->set($sCacheKey, $sAdrServerIp);
        if ($this->addtionalredis->ttl($sCacheKey)<3600) {
            $this->addtionalredis->setTimeout($sCacheKey, 86400);
        }
    }

    public function getRemoteIpByAdrid($nAdrServerId=null)
    {
        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2IP_PRE_309.$nAdrServerId;
        $sReturn = $this->addtionalredis->get($sCacheKey);
        return $sReturn ? $sReturn : false;

    }
    public function getAllAdrRemoteIp()
    {
        $sCacheTmp = LAF_CACHE_ADDTIONAL_ADRSERVER2IP_PRE_309;
        $aReturn = array();
        $aKeys = $this->addtionalredis->keys($sCacheTmp."*");
        if (count($aKeys)) {
            foreach ($aKeys as $sCacheTmp2) {
                $sIp = $this->addtionalredis->get($sCacheTmp2);
                if (!isset($aReturn[$sIp])) {
                    $aReturn[$sIp] = array( );
                }
                $aReturn[$sIp][] = str_replace(LAF_CACHE_ADDTIONAL_ADRSERVER2IP_PRE_309, "", $sCacheTmp2);
            }
        }

        return $aReturn;

    }


    function getRemoteLurClientIp(){
        return \Sra2Redis::getCache($this->redis, LC_REMOTE_LURIP_836);
    }

    function getAddtionalRedis(){
        return $this->addtionalredis;
    }


    public function getJobCountById($id, $sDate=null)
    {
        
        // 补量账户数据

        $nIndex2 = 0;
        $aJobs = $this->getAllJobs(array(), $sDate);
        $nTotal = 0;
        $nCodeCount = 0;
        $aReturn = array();
        if (count($aJobs)) {
            foreach ($aJobs as $nCityId => $aTmp2) {
              foreach ($aTmp2 as $sJobkey => $nCount) {
                list($sJobId) =explode('-', $sJobkey);
                if ($sJobId == $id) {
                    $aReturn[$sJobkey] = $nCount;
                }
              }
            }
        }
        return $aReturn;
    }

    /**
     * 获取全部进行中的任务, 并且乱序
     */
    public function getAllJobs($aIgnorecoids=array(), $sDate=null){

        $aReturn = array();
        $sDate = is_null($sDate) ? date("Ymd") : $sDate;
        $sAddtionalDbCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".$sDate;
        $aAllJobs = $this->addtionalredis->hGetAll($sAddtionalDbCacheKey);
        if (count($aAllJobs)) {
            // var_dump($aIgnorecoids);
            foreach ($aAllJobs as $key => $nCountTmp) {
                if (intval($nCountTmp) < 10) {
                    continue;
                }
                $aTmp = explode("-", $key);

                if (count($aIgnorecoids) && in_array($aTmp[0], $aIgnorecoids)) {
                    continue;
                }
                $aTmp[3] = $this->_getMapCityId($aTmp[3]);
                $sNewKey = join("-", $aTmp);
                // 按城市id分组
                if (!isset($aReturn[$aTmp[3]])) {
                    $aReturn[$aTmp[3]] = array();
                }
                $aReturn[$aTmp[3]][$sNewKey] = $nCountTmp;
            }
        }
        return $aReturn;
    }


    // 处理曾经错乱的ID
    public function _getMapCityId($nId)
    {
        $aMap = array(
            34=>27,
            220=>50,
            225=>62,
            196=>168,
            232=>143,
            224=>81,
            235=>183,
        );
        return isset($aMap[$nId]) ? $aMap[$nId] : $nId;
    }




    /**
     * 获取全部进行中的任务, 并且乱序
     */
    public function getAllCityCounter($sDate=null){

        $aReturn = array();
        $aAllJobs = $this->getAllJobs(array(), $sDate);
        if (count($aAllJobs)) {
            foreach ($aAllJobs as $nCityId  => $aTmp) {
                $aReturn[$nCityId] = array_sum($aTmp);
            }
        }
        arsort($aReturn);
        return $aReturn;
    }    
    /**
     * 获取全国与随机其他城市
     */
    public function getJobsByCountry($aIgnorecoids=array()){
        $aMapRandomCity = $aMapCountry = array();
        $aAllJobs = $this->getAllJobs($aIgnorecoids);
        if (isset($aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999])) {
            $aMapCountry = $aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999];
            unset($aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999]);
        }

        if (count($aAllJobs)) {
            $nTotal = 30;
            foreach ($aAllJobs as $nMapedCityId => $aTmp) {
                foreach ($aTmp as $key => $nCountTmp) {
                    if ($nTotal < 1) {
                        continue;
                    }
                    $aMapRandomCity[$key] = $nCountTmp;
                    $nTotal--;
                }
            }
        }

        $aMapCountry = \YcheukfCommon\Lib\Functions::shuffle_assoc($aMapCountry);
        $aMapRandomCity = \YcheukfCommon\Lib\Functions::shuffle_assoc($aMapRandomCity);

        return array(
            $aMapRandomCity,
            $aMapCountry,
        );
    }


    public function getIntByIp($sIp='')
     {
         
         $s = explode('.', trim($sIp));
         $d = $s[0]*0x1000000+$s[1]*0x10000+$s[2]*0x100+$s[3];
         return $d;
     }

     // 根据IP取iac的信息
     function getIacCityCode($sIp='', $bDebug=false)
    {
       $oRedis = $this->redis;

        $d = $this->getIntByIp($sIp);
        $sCacheCityScore = 'iacmapping_score';

        $value = $oRedis->zRevRangeByScore($sCacheCityScore, $d,0, ['limit'=>[0, 1]]);

        if (is_array($value) && count($value)) {
            $tmp1 = json_decode($value[0], 1);
            return $tmp1[0];
        }
        return 0;
    }


    
    public function isVpnIpCalledTime($ip, $sDate){
        $sHisCacheKey2 = LAF_CACHE_VPNIPS_HISSET_140.$sDate;
        return intval($this->redis->hGet($sHisCacheKey2, $ip));

    }

    // 新方法, 从缓存中取
    public function getIacConfigByIp($ip, $bForceFlag=false){
       $oRedis = $this->redis;
       $sCacheCityHash = 'iacmapping';

        $citycode = $this->getIacCityCode($ip);


        // 同名映射
        $samemaps = [
            1347 => 1305, //大理.大理白族自治州
            1147 => 221,//红河.红河哈尼族彝族自治州
            1297 => 268,//西双版纳.西双版纳傣族自治州
        ];
             
           


        $iacinfo = $oRedis->hGet($sCacheCityHash, $citycode);
        if ($iacinfo) {
            $iacinfo_a = json_decode($iacinfo, 1);
            if (is_array($iacinfo_a) && count($iacinfo_a)) {
                return array(
                    "errorMsg"=>"null",
                    "data"=>array('region' => $iacinfo_a[2],
                        'code' => $citycode,
                        'mapresid' => isset($samemaps[$iacinfo_a[3]]) ? $samemaps[$iacinfo_a[3]] : $iacinfo_a[3],
                        "ip"=>$ip 
                    ),"success"=>true);
            }
        }

        return array("errorMsg"=>"unavailable ip","data"=>array(),"success"=>false);

    }

    public function getIacCityId($sAdrServerIp){

        $sCityIdIac = false;
        $aIpConfig = $this->getIacConfigByIp($sAdrServerIp);
        
        if (is_array($aIpConfig)
         && isset($aIpConfig['data'])
         && isset($aIpConfig['data']['mapresid'])
         && !empty($aIpConfig['data']['mapresid'])
        ) {// 找到iac关系
            $sCityIdIac = $aIpConfig['data']['mapresid'];
        }

        return $sCityIdIac;
    }
    /**
     * 获取全国与某指定城市
     */
    public function getJobsByCityId($nMapedCityId, $aIgnorecoids, $nAdrServerId=0)
    {
        $aMapCity = $aMapCountry = $aMapProvince = array();
        $aAllJobs = $this->getAllJobs($aIgnorecoids);

        $provinceid = 0;
        // $nMapedCityId = LAF_CACHE_ADDTIONAL_ALLTICIES_9999;
        if (is_numeric($nMapedCityId)) {//vpn 拨号成功状态, 则从现有的 adrid-vpncityidid中获取任务
            // vpn 中适配好的 adrid-vpncityid
            $province2city = $this->getprovince2city();
            foreach($province2city as $pid2 => $row1){
                if (isset($row1[$nMapedCityId])){
                    $provinceid = $pid2;
                }
            }
        }

        if (!empty($provinceid) && isset($aAllJobs[$provinceid])) {
            $aMapProvince = $aAllJobs[$provinceid];
        }
        if (isset($aAllJobs[$nMapedCityId])) {
            $aMapCity = $aAllJobs[$nMapedCityId];
        }
        if (isset($aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999])) {
            $aMapCountry = $aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999];
            $aMapCountry = \YcheukfCommon\Lib\Functions::shuffle_assoc($aMapCountry);
        }
        if (count($aMapCity)) {
            $aMapCity = \YcheukfCommon\Lib\Functions::shuffle_assoc($aMapCity);
        }

        return array(
            $aMapCity,
            $aMapProvince, 
            $aMapCountry,
            $nMapedCityId,
            $provinceid
        );

    }



    /**
     * 所有活着的ADR机器
     */
    public function getAliveList(){
        $oObject = $this->redis;
        $aReturn = $oObject->sMembers(LAF_CACHE_ADRSERVERS_ALIVE_106);
        return $aReturn ? $aReturn : array();
    }

    /**
     * adr机器是否或者
     */
    public function isAdrAlive($sId){
        $oObject = $this->redis;
        return $oObject->sIsMember(LAF_CACHE_ADRSERVERS_ALIVE_106, $sId);
    }


    public function remove4AliveList($sId='')
    {
        
        $oObject = $this->redis;

        if ($oObject->sIsMember(LAF_CACHE_ADRSERVERS_ALIVE_106, $sId)) {
            $oObject->sRem(LAF_CACHE_ADRSERVERS_ALIVE_106, $sId);
            // echoMsg('['.__CLASS__.'] remove4AliveList :'.$sId);
        }

    }



    public function getAdrId2Host($adrid){
        $return = \Sra2Redis::getCache($this->redis, LAF_CACHE_ADRID2HOST_135);
        return $return && isset($return[$adrid]) ? $return[$adrid] : null;
    }

    public function getDaylyShellScript()
    {
        $adrid = $this->instants['adr_id'];
        $daylyconfig = require '/app/module/Lur/config/adr.shell.script.dayli.php';
        $cachekey = LAF_BATCHSHELL_DAYLY_704."/".date("Ymd");

        $index = 1;
        foreach ($daylyconfig as $key1 => $shell) {

            list($crontime, $duration) = explode(",", $key1);
            $shellkey = $cachekey."/".$adrid."/".$shell;
            $lockkey = LAF_BATCHSHELL_DAYLY_LOCK_705.$adrid;

            if ($this->redis->exists($lockkey)) {
                return $this->redis->get($lockkey);
            }
            $shellkeymd5 = md5($shellkey);
            // 时间已经到
            if (time() > strtotime(date("Y-m-d")." ".$crontime)) {
                // 尚未执行
                if (!$this->redis->hExists($cachekey, $shellkeymd5)) {
                    $this->redis->hSet($cachekey, $shellkeymd5, $shellkey);

                    $shell2 = "t=".date("Ymd").$index.";".$shell;
                    $this->redis->setEx($lockkey, $duration*60, $shell2);
                    return $shell2;
                }
            }
            $index++;
        }
        $this->redis->setTimeout($cachekey, 10*86400);

        // 程序没有任何跳出,说明已经没有需要执行的脚本, 则可执行自定义的脚本
        return null;

    }
    public function getShellScript($adrHost=null)
    {

        $daylyScript = $this->getDaylyShellScript();
        if ($daylyScript===null) {
            $sShellscript = require '/app/module/Lur/config/adr.shell.script.php';
        }else{
            $sShellscript = $daylyScript;
        }

        // 测试用
        // if (in_array($this->instants['adr_id'], [42])){
        //     return $sShellscript;
        // }
        
        $return = '';
        if ($sShellscript 
            && !is_null($sShellscript)
            && !is_null($adrHost)
        ) {
            preg_match_all('/t=(\w+);.*/', $sShellscript, $matches);
            if (isset($matches[1]) && isset($matches[1][0])) {
                $taskid = $matches[1][0];
                $this->redis->set(LAF_CACHE_CURRENT_SHELLID_137, $taskid);

                $sShellKeyLock = LAF_CACHE_ADRSHELLLIST_136."/".$taskid;
                $sShellKeySucc = LAF_CACHE_ADRSHELLLIST_OK_138."/".$taskid;
                
                // 若已经成功提交了结果, 则跳过
                if (!$this->redis->hExists($sShellKeySucc, $this->instants['adr_id'])) {

                    // 是否当前机器已经有被某adrid锁定, 若无则自己锁定
                    if (!$this->redis->hExists($sShellKeyLock, $adrHost)) {


                        $this->redis->hSet($sShellKeyLock, $adrHost, json_encode([
                            "t" => date("Y-m-d H:i:s"),
                            "id" => $this->instants['adr_id'],
                        ]));
                        $this->redis->setTimeout($sShellKeyLock,100*3600);
                        $return = $sShellscript;

                    }else{
                        
                        $tmp = $this->redis->hGet($sShellKeyLock, $adrHost);
                        $json = json_decode($tmp, 1);
                        // 超时没有执行, 重新执行
                        if (time()-strtotime($json['t']) > 120) {
                            $return = "rand=".time().";".$sShellscript;
                            $this->redis->hSet($sShellKeyLock, $adrHost, json_encode([
                                "t" => date("Y-m-d H:i:s"),
                                "id" => $this->instants['adr_id'],
                            ]));
                        }
                    }
                }
            }
        }
        return $return;
    }
    
    public function getVpnData($nAdrServerId)
    {
        $sReturn = "";
        $sCacheKey = LAF_CACHE_VPNACCPRE_107.$nAdrServerId;
        $sReturn = \Sra2Redis::getCache($this->redis, $sCacheKey);
        if (!$sReturn) {
            $sReturn = "";
        }

        return $sReturn;
    }
    public function getCityidByAdrid($nAdrServerId){

        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306.$nAdrServerId;
        $sReturn = $this->addtionalredis->get($sCacheKey);
        return $sReturn ? $sReturn : false;
    }
    public function saveLastAdrStats($aData)
    {
        
        if (is_array($aData) 
            && isset($aData['host']) 
            && isset($aData['type']) 
            && preg_match("/^\d+$/i", $aData['host']) 
            && preg_match("/^\d+$/i", $aData['type'])
        ) {

            // $aConfig = $this->serviceManager->get('config');

            $aTypes = [$aData['type']];
            if ($aData['type'] == 2) {
                // 1状态包含了2状态
                $aTypes[] = 1;
            }

            $nExpiredTime =87000*3;
            foreach ($aTypes as $nType) {
                $sCacheKey = LAF_CACHE_ADRLASTSTATS_121.$nType."/".$aData['host'];
        // $this->log($sCacheKey, 'sCacheKey');

                \Sra2Redis::saveCache($this->redis, $sCacheKey, $aData, $nExpiredTime);
            }

            if (isset($aData['dockerhost'])
                && !empty($aData['dockerhost'])
             ) {
                // 
                $this->redis->hSet(LAF_CACHE_ADRID2DOCKERHOST_141, $aData['host'], $aData['dockerhost']);
            }
        }
    }

    public function updBlackIpCounter($nAdrServerId,$aPostData){
        // ADRKEY_COMMON_CLIENT_801
        $tmp = is_array($aPostData) && isset($aPostData['data']) ? $aPostData['data'] : null;
        // $this->log($tmp, 'tmp');

        if ($tmp) {
            $tmp1 = json_decode($tmp, 1);
            $sAdrServerIp = isset($tmp1['ADRKEY_COMMON_CLIENT_801']) ? $tmp1['ADRKEY_COMMON_CLIENT_801'] : null;
            $sDate = date("Ymd");

            // exec("echo '".json_encode($sAdrServerIp)."' > /tmp/feng2");

            $sHisCacheKey2 = LAF_CACHE_VPNIPS_HIS_125.$sDate;

            if (isset($tmp1['addtional_adrcityid']) && !empty($tmp1['addtional_adrcityid'])) {
                $nCityId = $tmp1['addtional_adrcityid'];
            }else{
                $nCityId = $this->getCityidByAdrid($nAdrServerId);
            }
            
            if ($nCityId && !preg_match('/180\.97\.81\./', $sAdrServerIp)) {


                $cachekey5 = LAF_CACHE_VPNCITY2_HIS_146.$sDate;
                $this->redis->sAdd($cachekey5, $nCityId);
                $this->redis->setTimeout($cachekey5, 10*86400);

                $cachekey4 = LAF_CACHE_VPNCITY2IP_HIS_145.$sDate."/".$nCityId;
                $this->redis->hIncrBy($cachekey4, $sAdrServerIp, 1);
                $this->redis->setTimeout($cachekey4, 10*86400);
            }



            // 该机器未曾拨过号, 则给ip拨号次数+1
            if (!$this->redis->hExists($sHisCacheKey2, $sAdrServerIp.",".$nAdrServerId)) {

                $sHisCacheKey3 = LAF_CACHE_VPNIPS_HISSET_140.$sDate;
                $this->redis->hIncrBy($sHisCacheKey3, $sAdrServerIp, 1);
                $this->redis->setTimeout($sHisCacheKey3, 20*86400);

            }

            $this->redis->hIncrBy($sHisCacheKey2, $sAdrServerIp.",".$nAdrServerId, 1);
            $this->redis->setTimeout($sHisCacheKey2, 20*86400);
            



            
            $isBlackIp = $this->isBlackIp($sAdrServerIp);
            if ($isBlackIp) {
                // $this->log([$nAdrServerId, $sAdrServerIp], 'isBlackIp');

                $sCounterKey = LAF_CACHE_BLACKIPS_COUNTER_123.$sDate;
                $this->redis->hIncrBy($sCounterKey, $sAdrServerIp, 1);

                $sHisCacheKey = LAF_CACHE_BLACKIPS_HIS_124.$sDate;
                $this->redis->hIncrBy($sHisCacheKey, $sAdrServerIp.",".$nAdrServerId, 1);

                $this->redis->setTimeout($sCounterKey, 20*86400);
                return true;
            }
        }
        return false;
    }
    public function isBlackIp($sAdrServerIp){
        return $this->redis->sIsMember(LAF_CACHE_BLACKIPS_122, $sAdrServerIp);
    }

    
    public function add2AliveList($nAdrServerId){

        if (!$this->redis->sIsMember(LAF_CACHE_ADRSERVERS_ALIVE_106, $nAdrServerId)) {
            $this->redis->sAdd(LAF_CACHE_ADRSERVERS_ALIVE_106, $nAdrServerId);
        }
        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRALIVE_PRE_310.$nAdrServerId;
        // $this->log($sCacheKey);
        \Sra2Redis::saveCache($this->redis, $sCacheKey, 1, $this->config['adr_server_heartbeat_interaltime']);


    }

    /*判断是否有量需要跑, 有的话再拨号*/
    public function isAddtionalRunable($nAdrServerId){
        $cachekey = LAF_CACHE_ADDTIONAL_ADRRUNABLE_353.$nAdrServerId;
        if ($this->redis->exists($cachekey))return $this->redis->get($cachekey);
        $flag = false;
        $sCityIdIac = $this->getCityidByAdrid($nAdrServerId);

        list($aMapCity, $aMapProvince, $aMapCountry, $nMapedCityId, $provinceid) = $this->getJobsByCityId($sCityIdIac, [], $nAdrServerId);

        // 
        // if ((array_sum($aMapCity)>1000 ||array_sum($aMapProvince)>1000) && (array_sum($aMapCity) + array_sum($aMapProvince)+ array_sum($aMapCountry)) > 10000) {

        // ||array_sum($aMapProvince)>1000
        if ((array_sum($aMapCity)>1000 ) ) {
            $flag = true;
        }
        if ($nAdrServerId == 1) {
            // $this->log([$sCityIdIac, $nAdrServerId, $aMapCity, $aMapProvince, $aMapCountry, array_sum($aMapCity), array_sum($aMapProvince), array_sum($aMapCountry)], 'aMapCity');
        }
        $this->redis->setEx($cachekey, 10, $flag?1:0);
        return $flag?1:0;
    }
    
    public function getProxyconf(){

        
        // $account = ([
        //     "url" => "http://d.jghttp.alicloudecs.com/getip?num=3&type=2&pro=510000&city=510600&yys=0&port=11&pack=39718&ts=1&ys=1&cs=1&lb=1&sb=0&pb=4&mr=2&regions=",
        //     "p" => "jiguangdaili",
        //     "clid" => 89,
        // ]);
        $adrid = $this->instants['adr_id'];
        $account = [];
        if($this->redis->hExists(LC_PROXY_BANDADR_833, $adrid)){
            $account_json = $this->redis->hGet(LC_PROXYADR_ID2CONFIG_831, $adrid);
            $account2 = json_decode($account_json, 1);
            if (isset($account2['p']) 
                && in_array($account2['p'], ['jiguang', 'zhima'])
                && intval(date("H"))<9
            ) {// 有些供应商不需要在凌晨低峰时跑
                $account = [];
            }else{
                $account = $account2;
            }

        }



        // $account = ([
        //     "url" => "http://http.tiqu.letecs.com/getip3?num=1&type=2&pro=310000&city=0&yys=0&port=1&pack=163514&ts=1&ys=1&cs=1&lb=1&sb=0&pb=4&mr=2%C2%AEions=&tl=1",
        //     "whiteurl" => "https://wapi.http.linkudp.com/index/index/save_white?neek=441636&appkey=d04646926af624e94fd48cc505e5138b&white=",
        //     "p" => "jiguangdaili",
        //     "clid" => 1,
        // ]);
        

        return $account;
    }

    public function getprovince2city(){
        
        $sReturn = \Sra2Redis::getCache($this->redis, LAF_CACHE_PROVINCEID2CITYID_148);
        return $sReturn;
    }

    function isProxyAdr($adrid){
        return $this->redis->sIsMember(LC_PROXYADRLIST_830, $adrid);
    }

    public function getAdrServerType($nAdrServerId, $bUpdateFlag=0){

        if ($nAdrServerId == LAF_METADATATYPE_ADRSERVERTYPE_LOCALDEBUGADR){
            return LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYPROXY_1027;
        }

        if($this->isProxyAdr($nAdrServerId)){
            return LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYPROXY_1027;
        }

        $sCacheKey = LAF_CACHE_ADRID2TYPEID_117.$nAdrServerId;

        $sReturn = \Sra2Redis::getCache($this->redis, $sCacheKey);


        // $this->log($sReturn, "getAdrServerType");

        if (!$sReturn || $bUpdateFlag) {

            $sReturn = LAF_METADATATYPE_ADRSERVERTYPE_TROLLEY_1018;
            $sLabelTmp = \Sra2Db::getMLabel($this->pdo_slave, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021, $nAdrServerId);
            if ($sLabelTmp && !empty($sLabelTmp)) {
                $sReturn = LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021;
            }
            $sLabelTmp = \Sra2Db::getMLabel($this->pdo_slave, LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009, $nAdrServerId);

            if ($sLabelTmp && !empty($sLabelTmp)) {
                $sReturn = LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009;
            }

            \Sra2Redis::saveCache($this->redis, $sCacheKey, $sReturn, rand(86400,86400*3));
        }
        return $sReturn;
    }

}
