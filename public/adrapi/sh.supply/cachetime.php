<?php
// 该API放在149上供下游使用
$debug = 1;


try {
    
	include_once(__DIR__."/../simplerestapi2.inc.php");
	$oSra = new SimpleRestApi2();

    $cachekey5 = LAF_CACHE_DISTRICBUTIONAPI_APICACHE_816.'sohu';
    $dtime = $oSra->redis->get($cachekey5);
    $result = ['t'=>$dtime];
    header('content-type:application/json');
    echo json_encode($result);
    // if ($cachedata) {
    //     header('content-type:application/json');
    //     // echo "cache\n";
    //     echo $cachedata;
    //     exit;
    // }
}catch (\Exception $e) {
    header('content-type:application/json');
    echo json_encode(["status"=>1, "msg"=>$e->getMessage(), "data"=>[]]);
    // echo $e->getMessage();
}

function getcache($file)
{
    if (file_exists($file)) {
        $t1 = filemtime($file);
        if (time()-$t1 <120) {
            return file_get_contents($file);
        }
    }
    return false;
}