<?php
// 该API放在149上供下游使用
$debug = 1;

$plan = $_GET['plan'];
try {
    
	include_once(__DIR__."/../simplerestapi2.inc.php");
	$oSra = new SimpleRestApi2();
	$cachekey4 = LAF_CACHE_DISTRICBUTIONAPI_LASTDETAIL_814.'shdown';

	$cncode2label = $oSra->cncode;
    $sSql = "select code, label, mapresid from `laf_lur`.`location_iaccode` group by code, label";
    $oSth = $oSra->pdo_slave->prepare($sSql);
    $oSth->execute([]);
    $locationrow = $oSth->fetchAll(\PDO::FETCH_ASSOC);

    // var_dump($locationrow);
    function replaceAdrTag($s){
        $s = preg_replace('/\[adrid=\d+\]/i', '', $s);   
        $s = preg_replace('/\[adrsplitid=\w+\]/i', '', $s);   
        return $s;
    }

	function fmt_vurlparams($ua, $iline, $row6, $row4, $titleindexs){
		$return = $row6;



		$aParams = [
            "appid" => 'tv',//
            "adoriginal" => 'sohu',//
            "c" => 'tv',//
            "ext" => '2097151',//
            "exten" => '1,2,3,4,5,6,7,8',//
            "extern" => '1,2,3,4,5,6,7,8',//
            "sver" => '8.2.0',//
            "build" => '8002000',//
		];
		$aParams['identifyid'] = $row4[$titleindexs['cxid']];



		// [adrid=12329]
		$asspid = explode(",", replaceAdrTag( $row4[$titleindexs['sspid']]));
        $aParams['pt'] = trim($asspid[0]); // 投放的平台

		switch($aParams['pt']){
			default:
			case 'oad':
			break;
			// case 'oad':
			case 'overfly':

                $poscode = [17001,17002,17003,17004,17005,17006];
                $aParams['poscode']= ''.join('|', $poscode);

                $expired = [];
                $start = count($poscode)/($iline+1);
                foreach($poscode as $i=>$code1){
                    if($i>=$start && count($expired)<3){
                        $expired[] = $code1;
                    }
                }
                $aParams['expired']= ''.join('|', $expired);

			break;
			case 'open':
			break;
		}

        if (isset($asspid[2])) { //vrs 分类
            $aSplitVc = explode("#", $asspid[2]);
            $aParams['vc']= ''.$aSplitVc[0];
        }
        if (isset($asspid[3]) && !empty($asspid[3])) { //指定渠道参数
            $aParams['al'] = ''.$asspid[3];
        }



		switch($asspid[0]){
			default:
			case 'oad':
			break;
			// case 'oad':
			case 'overfly':

                $poscode = [17001,17002,17003,17004,17005,17006];
                $aParams['poscode']= ''.join('|', $poscode);

                $expired = [];
                $start = count($poscode)/($iline+1);
                foreach($poscode as $i=>$code1){
                    if($i>=$start && count($expired)<3){
                        $expired[] = $code1;
                    }
                }
                $aParams['expired'] = ''.join('|', $expired);

			break;
			case 'open':
			break;
		}

	    switch(intval($ua)){
	    	case 35:
	    		$return['vurl'] = "http://agn.aty.snmsohu.aisee.tv/m?prot=vast&prot1=vast";
	    		$return['plt'] = "OTT";
                $aParams['plat']= 'ott1';

                $aParams['site'] = 1;
                $aParams['poid'] = 1;
                // $aParams['sver'] = 1;
                $aParams['playstyle'] = 1;
                $aParams['islocaltv'] = 1;
                $aParams['sdkVersion'] = 'tv7.7.02';
                $aParams['wt'] = 'wifi';
                $aParams['appid'] = 'ott';
                $aParams['protv'] = 3;
                $aParams['offline'] = 0;
                $aParams['license'] = 0;
                $aParams['density'] = 2;
                $aParams['ext'] = 65536;
                // $aParams['pt'] = 'oad';
                $aParams['c'] = 'tv';
                $aParams['localAareaCode'] = '';
                $aParams['partner'] = 9999;
                $aParams['adoriginal'] = 'sohu';
                $aParams['poscode']="op_ott_1";
	    	break;
	    	case 1:
	    		$return['vurl'] = "https://v.aty.sohu.com/v?prot=vast";
	    		$return['plt'] = "PC";
                $aParams['plat']= 'pc';
                $aParams['c']='tv';
                $aParams['type']='vrs';
                $aParams['out']=0;
                $aParams['autoplay']=1;
                $aParams['fee']=0;
                $aParams['isHplayer']=1;
                $aParams['ak']="Pc";
                $aParams['du']="";
                $aParams['pagerefer']="";
                $aParams['pt']="";
                $aParams['pageUrl']=("https://tv.sohu.com");

	    	break;
	    	case 2:
	    		$return['vurl'] = "http://agn.aty.sohu.com/m?prot=vast&prot1=vast";
	    		$return['plt'] = "PHONE-ANDROID";
                $aParams['plat']= '6';

                if (strtolower($aParams['pt']) == 'open') {
                    $aParams['poscode']="op_aphone_1";
                }
                $aParams['sdkVersion'] = 'tv7.7.02';

	    	break;
	    	case 3:
	    		$return['vurl'] = "http://agn.aty.sohu.com/m?prot=vast&prot1=vast";
	    		$return['plt'] = "PHONE-IOS";
                $aParams['plat']= '3';

                if (strtolower($aParams['pt']) == 'open') {
                    $aParams['poscode']="op_iphone_1";
                }
                $aParams['sdkVersion'] = '12.5.1';
	    	break;
	    	case 16:
	    		$return['vurl'] = "http://agn.aty.sohu.com/m?prot=vast&prot1=vast";
	    		$return['plt'] = "PAD-ANDROID";
                $aParams['plat']= '0';
                if (strtolower($aParams['pt']) == 'open') {
                    $aParams['poscode']="op_aphone_1";
                }
                $aParams['sdkVersion'] = 'tv7.7.02';
	    	break;
	    	case 17:
	    		$return['vurl'] = "http://agn.aty.sohu.com/m?prot=vast&prot1=vast";
	    		$return['plt'] = "PAD-IOS";
                $aParams['plat']= '1';
                if (strtolower($aParams['pt']) == 'open') {
                    $aParams['poscode']="op_iphone_1";
                }
                $aParams['sdkVersion'] = '12.5.1';
	    	break;
	    }



		// var_dump($aParams, $row6, $row4, $titleindexs);
		// exit;
		$aTmp = [];
        foreach ($aParams as $pname => $pvalue) {
             $aTmp[] = $pname.'='.$pvalue;
        }
        $return['vurl'] = $return['vurl']."&".join("&", $aTmp);
		return $return;
	}


	function getcitybyid($cncode2label, $locationrow, $resid){
		// var_dump($resid);
		$return = ['1156000000', '中国大陆'];
	    foreach ($locationrow as $aRowTmp) {
	    	if ($aRowTmp['mapresid'] == $resid){
		        foreach ($cncode2label as $cncode => $cnlabel) {
			        if ($cnlabel == $aRowTmp['label'] || $cnlabel == $aRowTmp['label']."市" || $cnlabel == $aRowTmp['label']."省" ) {
						$return = [strval($cncode), $cnlabel];
			        }
		        }
	    	}
	    }
	    return $return;
	}
	$avids = [
		1 => explode("\n", file_get_contents((__DIR__ ."/vid.1"))),
		2 => explode("\n", file_get_contents((__DIR__ ."/vid.2"))),
		3 => explode("\n", file_get_contents((__DIR__ ."/vid.3"))),
	];
	function getvids($pvids, $i){
		global $avids, $plan;
		$base = intval(str_replace('-', '', $plan))*($i+count($pvids));
		// var_dump($plan, $i, $base);
		// var_dump($avids);
		// var_dump($pvids);

		$return = [];

		$num = 50;
		foreach($pvids as $pvid){
			$start = $base%count($avids[$pvid]);
			for($ii=$start; $ii<=$start+$num; $ii++){
				if(isset($avids[$pvid][$ii])){
					$return[] = $avids[$pvid][$ii];
				}
			}
			
		}
		return $return;
		// var_dump($return);

	}





	$result = [];
    $sql = "select id,memo,created,planno from (select id,memo,created,planno,pplanno from system_batch_jobs where m158_id=10 and pplanno<>'' and planno<>'' and op_user_id=2003 and planno='".$plan."' order by id desc) t group by t.pplanno";
    // echo $sql;
    $sth = $oSra->pdo_slave->prepare($sql);
    $sth->execute();
    $aTmp = $sth->fetch(\PDO::FETCH_ASSOC);
    if(is_array($aTmp) && count($aTmp)){


    	// 父记录
	    $sql = "select id,memo,created,planno,pplanno,in_file from system_batch_jobs where m158_id=10 and pplanno='' and planno<>'' and op_user_id=2003 and planno='".$plan."' order by id desc limit 1";
	    // echo $sql;
	    $sth = $oSra->pdo_slave->prepare($sql);
	    $sth->execute();
	    $frow = $sth->fetch(\PDO::FETCH_ASSOC);
	    // var_dump($frow);


    	// 分给下游的配置
		// $cachekey1 = LAF_CACHE_DISTRICBUTIONAPI_OPHIS_810."shmain/".$plan;
		// $ops = $oSra->redis->hGetAll($cachekey1);
	 //    var_dump($cachekey1, $ops);
	    // var_dump($cachekey4);


		$detailhis = json_decode($oSra->redis->hGet($cachekey4, $plan), 1);
		// var_dump($detailhis);
	    $titleindexs = [];
	    $dateindexs = [];
	    foreach($detailhis[0] as $i => $title){
    		$titleindexs[$title] = $i;
    		if(preg_match('/^\d{4}-\d{2}-\d{2}/', $title)){
	    		$dateindexs[$title] = $i;
    		}
	    }
	    ksort($dateindexs);

    	$result['sign'] = md5($aTmp['planno'].$aTmp['id']);

    	$result['uidurl'] = "";
    	$result['clr'] = 0;
    	$result['f-ip'] = 20;
    	$result['f-uuid'] = 5;
    	$result['task'] = [];


    	// uidurl 用73的地址跳转
    	// $detailhis[1][$titleindexs['useapiuuid']] = 2;
    	switch(intval($detailhis[1][$titleindexs['useapiuuid']])){
    		case 1:
	    		$result['uidurl'] = 'http://180.97.80.73:8990/equipmentInfo/get';
    		break;
    		case 2:
	    		$result['uidurl'] = 'http://180.97.80.73:8990/equipmentInfo/female/get';
    		break;
    		case 3:
	    		$result['uidurl'] = 'http://180.97.80.73:8990/equipmentInfo/male/get';
    		break;
    	}


    	if(is_numeric($detailhis[1][$titleindexs['cookie-frequence']])){
			$result['f-uuid'] = intval($detailhis[1][$titleindexs['cookie-frequence']]);
    	}

    	if(is_numeric($detailhis[1][$titleindexs['ssp-ctr']])){
			$result['clr'] = floatval($detailhis[1][$titleindexs['ssp-ctr']]);
    	}

    	$row2 = [];
    	foreach($dateindexs as $date1 => $i){
    		$date1 = trim($date1);
    		$hours = 0;
    		$houre = 23;
    		if(preg_match('/\s+/', $date1)){
    			list($date2, $hour2) = explode(" ", $date1);
	    		if(preg_match('/-/', $hour2)){
	    			list($hours, $houre) = explode("-", $hour2); 
	    		}else{
	    			$hours = $houre = intval($hour2);
	    		}
	    		$hours = intval($hours);
	    		$houre = intval($houre);

    		}else{
    			$date2 = $date1;
    		}
    		$row3 = [
    			'date' => $date2,
    			'hour' => ['s'=>$hours, 'e'=>$houre],
    			'rows' => [],
    		];
    		$row5 = [];
		    foreach($detailhis as $iline => $row4){
		    	if($iline==0 || !isset($titleindexs['userid']) || empty($row4[$titleindexs['userid']]) || empty($row4[$titleindexs['sspid']])){continue;
				}
			    if(is_numeric($row4[$i]) && intval($row4[$i])<1){
			    	continue;
			    }
			    // 被禁止
		    	if(is_numeric($row4[$titleindexs['show_activity']]) && $row4[$titleindexs['show_activity']] > 1){
		    		continue;
				}


			    $row6 = [
			    	'vurl' => '',
			    	'vidmap' => [],
			    	'p' => [
			    		'vid' => '',
			    		'imei' => '',
			    		'imenc' => '',
			    		'AndroidID' => '',
			    		'idfa' => '',
			    		'mac' => '',
			    		'manufacturer' => '',
			    		'pn' => '',
			    		'me' => '',
			    		'bssid' => '',
			    	],
			    	'plt' => 'PC',
			    	'city' => ['1156000000', '中国大陆'],
			    	'count' => 0,

			    ];
			    if(is_numeric($row4[$i])){
			    	$row6['count'] = $row4[$i];
			    }
			    if(!empty($row4[$titleindexs['cities']])){
			    	if(is_numeric($row4[$titleindexs['cities']]))
				    	$cities = [$row4[$titleindexs['cities']]];
				    else{
				    	$cities = json_decode($row4[$titleindexs['cities']], 1);
				    }
			    	$row6['city'] = getcitybyid($cncode2label, $locationrow, $cities[0]);
			    }
			    if(!empty($row4[$titleindexs['vid']])){
			    	if(is_numeric($row4[$titleindexs['vid']]))
				    	$pvids = [$row4[$titleindexs['vid']]];
				    else{
				    	$pvids = explode(",", $row4[$titleindexs['vid']]);
				    }
				    if(is_array($pvids) && count($pvids)){
						$vids = getvids($pvids, ($i*$iline));
				    	$row6['vidmap'] = $vids;
				    }
			    }
			    if(!empty($row4[$titleindexs['UA']])){
			    	if(is_numeric($row4[$titleindexs['UA']]))
				    	$uas = [$row4[$titleindexs['UA']]];
				    else{
				    	$uas = json_decode($row4[$titleindexs['UA']], 1);
				    }

			    }
				// $row6['code'] = $row4[$titleindexs['sspid']];
				$row6 = fmt_vurlparams($uas[0], $iline, $row6, $row4, $titleindexs);




	    		$row5[] = $row6;
		    }
		    $row3['rows'] = $row5;

		    if(count($row3['rows']))
    			$row2[] = $row3;
    	}
    	$result['task'] = $row2;

	    // var_dump($detailhis);
	    // print_r($result);
	    // var_dump($dateindexs);

    }else{
    	$result = [];
    }
    header('content-type:application/json');
    echo json_encode($result);

}catch (\Exception $e) {
    header('content-type:application/json');
    echo json_encode(["status"=>1, "msg"=>$e->getMessage(), "data"=>[]]);
    // echo $e->getMessage();
}

function getcache($file)
{
    if (file_exists($file)) {
        $t1 = filemtime($file);
        if (time()-$t1 <120) {
            return file_get_contents($file);
        }
    }
    return false;
}

function getdetail(){

	$detail = $this->getCsvFile($id);
	$lastop =  $this->serviceManager->get('Lur\Service\Common')->getLastHisOp2('shmain', $plan);
	list($pplanno, $csvcontent, $detail) = $this->_ftmDetail($plan, $type, $detail, $lastop['percent'], $lastop['grep_configs'], $lastop['datereplace']);

}