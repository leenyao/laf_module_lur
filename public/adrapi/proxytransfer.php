<?php
/**
 * 适配不同代理的服务
 */

include_once(__DIR__."/simplerestapi2.inc.php");
$oSra = new SimpleRestApi2();
$oSra->bDebug=1;

try{
	$oSra->init();
    // $oSra->chkToken();
    $adrid = $oSra->instants['adr_id'];
    $proxyconfigs = $oSra->redis->hGet(LC_PROXYADR_ID2CONFIG_831, $adrid);
    $proxyconfig = json_decode($proxyconfigs, 1);
    $localremoteip = $oSra->getRemoteLurClientIp();
    $data = [];
    // var_dump($proxyconfig);

  //   if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
		// throw new \Exception("BAD IP:".json_encode($_GET), 1);
  //   }




    if (is_array($proxyconfig) && isset($proxyconfig['p']) && isset($proxyconfig['url'])) {

        $fWhiteUrl = function($localremoteip, $redis) use ($proxyconfig, $oSra)
        {
            if (isset($proxyconfig["whiteurl"]) && strlen($proxyconfig["whiteurl"])>0) {
                $whiteurl = str_replace('[IP]', $localremoteip, $proxyconfig["whiteurl"]);

                $whiteurlmd5 = md5($proxyconfig["whiteurl"])."/".$localremoteip;
                $cachekey4 = LC_PROXY_WHITELIST_837.date("Ymd");

                if (!$redis->hExists($cachekey4, $whiteurlmd5)){
                    $wdata = file_get_contents($whiteurl);
                    $redis->hSet($cachekey4, $whiteurlmd5, $whiteurl);
                    $redis->setTimeout($cachekey4, 86400*5);
                }
            }
        };
        $fWhiteUrl($localremoteip, $oSra->redis);


        switch(strtolower($proxyconfig['p'])){
            case 'jiguang':
            case 'zhima':
            case 'zhima2':
                $urlcontent = @file_get_contents($proxyconfig['url']);
                $acclist = json_decode($urlcontent, 1);
                if($acclist['code'] == 0 && count($acclist['data'])){

                    foreach($acclist['data'] as $row){

                        $ttl =120;
                        if (isset($row['expire_time'])) {
                            $ttl = strtotime($row['expire_time'])-time() ;
                        }
                        $data[] = [
                            'ip' => $row['ip'],
                            'port' => $row['port'],
                            'ttl' => $ttl,

                        ];
                    }
                }else{
                    if (isset($acclist['code'])) {
                        throw new \Exception($acclist['code'], 1);
                    }else{
                        throw new \Exception(9999, 1);
                    }
                }
            break;
            case 'jinglin':
                $urlcontent = @file_get_contents($proxyconfig['url']);
                $acclist = json_decode($urlcontent, 1);
                if(isset($acclist['code']) && $acclist['code']==0 && count($acclist['data'])){
                    foreach($acclist['data'] as $row){
                        $ttl =120;
                        if (isset($row['ExpireTime'])) {
                            $ttl = strtotime($row['ExpireTime'])-time() ;
                        }
                        list($ip1, $port1) = explode(":", $row['IP']);
                        $data[] = [
                            'ip' => $ip1,
                            'port' => $port1,
                            'ttl' => $ttl,
                            'pa' => isset($proxyconfig['pa'])?$proxyconfig['pa']:'',
                            'pp' => isset($proxyconfig['pp'])?$proxyconfig['pp']:'',
                        ];
                    }
                }else{
                    if (isset($acclist['code']) && !in_array($acclist['code'] , [200, 500])) {
                        throw new \Exception($urlcontent, 1);
                    }else{
                        throw new \Exception(9999, 1);
                    }
                }
            break;
            case 'xingsuyun':
                $urlcontent = @file_get_contents($proxyconfig['url']);
                $acclist = json_decode($urlcontent, 1);
                if(isset($acclist['code']) && $acclist['code']==200 && count($acclist['result'])){

                    foreach($acclist['result'] as $row){
                        list($proxyip, $t1, $t2, $t3, $entts) = explode(",", $row);
                        list($ipt, $portt) = explode(":", $proxyip);
                        $ttl  = $entts-time();
                        $data[] = [
                            'ip' => $ipt,
                            'port' => $portt,
                            'ttl' => $ttl,

                        ];
                    }
                }else{
                    if (isset($acclist['code']) && !in_array($acclist['code'] , [200, 500])) {
                        throw new \Exception($acclist['code'], 1);
                    }else{
                        throw new \Exception(9999, 1);
                    }
                }
            break;


            case 'songziliuliang':
            case 'songzi':
                $urlcontent = @file_get_contents($proxyconfig['url']);
                $acclist = json_decode($urlcontent, 1);
                if($acclist['Code'] == 0 && count($acclist['Data'])){

                    foreach($acclist['Data'] as $row){
                        $ttl =120;
                        if (isset($row['expire_time'])) {
                            $ttl = strtotime($row['expire_time'])-time();
                        }
                        $data[] = [
                            'ip' => $row['Ip'],
                            'port' => $row['Port'],
                            'ttl' => $ttl,
                        ];
                    }
                }else{


                    if (isset($acclist['Code']) && in_array($acclist['Code'], [304, 301])) {
                        // 304 => 无ip
                    }else{
                         $oSra->log($urlcontent, 'urlcontent');
                    }

                }
            break;

            default:
                throw new \Exception("no such adapter:".$proxyconfig['p'], 1);
            break;
        }
        // code...
    }
    $oSra->log([$proxyconfig, $adrid, $data], 'httpresponse');

    $oSra->httpresponse(($data), 200, true);

}catch(\Exception $e){

    $oSra->response_error($e->getMessage(), $e->getCode());

}

