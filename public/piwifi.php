<?php
include_once(__DIR__."/../../../bin/script/simplerestapi.inc.php");


// 让adr pi 报告wifi信息, 并等待返回
function callAdrWifiInfo($sCommandType, $aCommandParams=array())
{
    global $oGlobalFramework;
    $bGetDeviceMsgFlag = false;
    $oGlobalFramework->sm->get('\Lur\Service\Common')->sendAdrCommand($_GET['i'], $sCommandType, $aCommandParams);
    // 等待设备信息返回
    $bBreakFlag = false;
    $bWhileRetryTimes = 20;
    while (1) {
        if ($bWhileRetryTimes-- < 1)$bBreakFlag = true;
        sleep(5);

        $aAdrInfo = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrSerInfoById($_GET['i']);;
        if (isset($aAdrInfo['wifi']) && isset($aAdrInfo['wifi']['pi3wifi']) && count($aAdrInfo['wifi']['pi3wifi'])) {
            $bGetDeviceMsgFlag = true;
            $bBreakFlag = true;
        }
        
        if($bBreakFlag)break;
    }
    return array($bGetDeviceMsgFlag, $aAdrInfo);
}


try {
    
    if (\YcheukfCommon\Lib\Functions::encryptBySlat($_GET['i']) !== $_GET['ei']) {
        throw new Exception("token check faild", 1);
        
    }

    $aAdrInfo = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrSerInfoById($_GET['i']);;

    if ($aAdrInfo && isset($aAdrInfo['model']) && "TROLLEY-DIRECT" == $aAdrInfo['model']) {

        // echoMsg("正在收集信息");

        // list($bGetDeviceMsgFlag, $aAdrInfoTmp) = callAdrWifiInfo("report_wifi2lur");
        // if ($bGetDeviceMsgFlag) {
        //     $aAdrInfo = $aAdrInfoTmp;
        // }
        // 让adr pi 报告wifi信息
        // $oGlobalFramework->sm->get('\Lur\Service\Common')->sendAdrCommand($_GET['i'], "report_wifi2lur");
        // var_dump($aAdrInfo);
        # code...
    }else{
        throw new Exception("adr is not in pi3 model:".$_GET['i'], 1);
    }

} catch (Exception $e) {
    echoMsg($e->getMessage());
    exit;
    
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>ADRPI - 网络配置</title>

    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
     <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
     <script src="http://apps.bdimg.com/libs/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</head>
<body>
<div class="page-header">
  <h1>ADRPI - 网络配置<small><a href="piwifi.lanscan.php">扫描附近的ADRPI</a></small></h1>
    <p>
</div>

<div class="panel panel-default">
      <?php
        $sWifiInfo = "未连接";
        $sEthInfo = "未连接";
        $sDeviceNotice = "<font color=red>设备没有在正常工作</font>";
        if (count($aAdrInfo)) {
            if (isset($aAdrInfo['wifi']) && isset($aAdrInfo['wifi']['pi3wifi']) && count($aAdrInfo['wifi']['pi3wifi'])) {
                preg_match_all("/(wlan0\s+wifi.*)/", $aAdrInfo['wifi']['pi3wifi']['networkdevice'], $aMatches);
                if (isset($aMatches[1][0]) && count(($aMatches[1][0]))) {
                    $sWifiString = preg_replace("/\s+/", " ", $aMatches[1][0]);
                    $aSplit = explode(" ", $sWifiString);
                    $aSplitWifiConfing = explode(":", $aAdrInfo['wifi']['pi3wifi']['config']);

                    $sWarningMsg = "";
                    if ($aSplitWifiConfing[0] != $aSplit[3]) {
                        $sWarningMsg = "; 但与配置的 ".$aSplitWifiConfing[0]."不符!";
                    }
                    if ($aSplit[2] == 'connected') {
                        $sWifiInfo = "已连接上 ".$aSplit[3].$sWarningMsg;
                        $sDeviceNotice = "<font color=green> 正常 </font> ";

                    }
                    // var_dump($sWifiString);
                }


                preg_match_all("/(eth0\s+ethernet.*)/", $aAdrInfo['wifi']['pi3wifi']['networkdevice'], $aMatches);
                if (isset($aMatches[1][0]) && count(($aMatches[1][0]))) {
                    $sWifiString = preg_replace("/\s+/", " ", $aMatches[1][0]);
                    $aSplit = explode(" ", $sWifiString);
                    if ($aSplit[2] == 'connected') {
                        $sEthInfo = "已连接";
                        $sDeviceNotice = "<font color=green> 正常 </font> ";

                    }
                }
            }
        ?>
    <div class="panel-heading">
        <h3 class="panel-title">设备信息 - <?php echo $sDeviceNotice;?></h3>
    </div>
    <div class="panel-body">

        <div class="table-responsive">
            <table class="table  table-bordered table-hover ">
                <tbody>
                    <tr>
                        <td>名称</td>
                        <td><?php echo $aAdrInfo['label']?></td>
                    </tr>
                    <tr>
                        <td>IP / 城市</td>
                        <td><?php echo $aAdrInfo['current_ip']?> / <?php echo $aAdrInfo['current_citylabel']?></td>
                    </tr>
                    <tr>
                        <td>最后一次活动时间</td>
                        <td><?php echo $aAdrInfo['created_time']?></td>
                    </tr>
                    <tr>
                        <td>WIFI</td>
                        <td><?php echo $sWifiInfo?></td>
                    </tr>
                    <tr>
                        <td>有线网络</td>
                        <td><?php echo $sEthInfo?></td>
                    </tr>
                    

                </tbody>
            </table>
        </div>

        <?php
          }else{
          ?>
          <div class="panel panel-default">
              <div class="panel-body">
                  该设备还没有联网信息, 请稍后刷新重试
              </div>
          </div>
          <?php
            }
            ?>
    </div>
</div>
<center>
<button type="button" class="btn btn-lg btn-success showsetupmsg">↓↓↓↓↓↓↓↓设置WIFI (若使用了有线网络, 也可不设置) ↓↓↓↓↓↓↓↓↓<small></small></button>
</center>
<div class="panel panel-info setupmsg-info" style="display: none;">
    <div class="panel-heading">
        <h3 class="panel-title">设置WIFI信息流程说明</h3>
    </div>
    <div class="panel-body">
        <ul>
            <li>
                第一步: 用一根网线将设备连接上有线网络
            </li>
            <li>
                第二步: 给设备通电(设备通电后会直接开机)
            </li>
            <li>
                第三步: 约等5秒后, 点击下方按钮开始设置WIFI(与设备通讯时速度较慢, 请耐心等待)
            </li>
            <li>
                第四步(可选): 设备WIFI设置成功后, 可断开网线使用
            </li>
        </ul>
        <center>
            <button type="button" class="btn btn-lg btn-warning setupwifi">+++++++++点击开始+++++++++<small></small></button>
            </center>
            <p>
            <p>
            <p>
    </div>
</div>

</body>
</html>
<script type="text/javascript">

    $(".showsetupmsg").click(function(){
        $(".setupmsg-info").toggle()
    })
    $(".setupwifi").click(function(){
        window.location.href = "piwifi/setupwifi.php?i=<?php echo $_GET['i']?>&ei=<?php echo $_GET['ei']?>";
    })
</script>