<?php
/**
 * 下载linux-chrome 
 */
include_once(__DIR__."/../../../bin/script/simplerestapi.inc.php");
$oSimpleRestApi = new SimpleRestApi();
try{

	// 判断token合法性
	if (!isset($_GET['token_t']) || !isset($_GET['token_v'])) {
		throw new Exception("miss token params", 1);
	}
	$bFlag = \YcheukfCommon\Lib\Functions::chkLyDynamicToken($_GET['token_t'],  $_GET['token_v'], "ADRTROLLEY");
	if ($bFlag !== true) {
		throw new Exception("error token", 1);
	}

	$sDownloadFile = BASE_INDEX_PATH."/../data/linux_chrome/541059-chrome-linux.zip";


	ob_clean();
	header('Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0');
	header('Pragma: no-cache');
	header('Pragma: public');
	header('Content-Description: File Transfer');
	header('Content-Disposition: attachment; filename="chrome-linux.zip"');
	header('Content-Type: text/comma-separated-values');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($sDownloadFile));
	readfile($sDownloadFile);

}catch(\Exception $e){

	$oSimpleRestApi->httpresponse($e->getMessage(), 500, false);

}


