<?php
/**
 * 下载 uv 用的node
 uv_packges.php
 */
include_once("/app/module/Lur/public/adrapi/simplerestapi2.inc.php");
$oSimpleRestApi = new SimpleRestApi2();


try{

	// 判断token合法性
	// if (!isset($_GET['token_t']) || !isset($_GET['token_v'])) {
	// 	throw new Exception("miss token params", 1);
	// }
	// $bFlag = \YcheukfCommon\Lib\Functions::chkLyDynamicToken($_GET['token_t'],  $_GET['token_v'], "ADRTROLLEY");
	// if ($bFlag !== true) {
	// 	throw new Exception("error token", 1);
	// }

	$sDownloadFile = "/app/data/uv_packges/node_modules.tar.gz";

	ob_clean();
	header('Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0');
	header('Pragma: no-cache');
	header('Pragma: public');
	header('Content-Description: File Transfer');
	header('Content-Disposition: attachment; filename="node_modules.tar.gz"');
	header('Content-Type: text/comma-separated-values');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($sDownloadFile));
	readfile($sDownloadFile);

}catch(\Exception $e){

	$oSimpleRestApi->httpresponse($e->getMessage(), 500, false);

}


