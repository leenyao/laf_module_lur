<?php
/**
 * 检查 adr_trolley_release 是否有新版本
 */
include_once(__DIR__."/../../../bin/script/simplerestapi.inc.php");
$sFilepath = __DIR__."/../src/Lur/Script/setupadr/setup_trolley.php";

ob_clean();
header('Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0');
header('Pragma: no-cache');
header('Pragma: public');
header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="setup_trolley.php"');
header('Content-Type: text/comma-separated-values');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($sFilepath));
readfile($sFilepath);