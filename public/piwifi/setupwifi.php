<?php
include_once(__DIR__."/../../../../bin/script/simplerestapi.inc.php");

// 让adr pi 报告wifi信息, 并等待返回
function callAdrWifiInfo($sCommandType, $aCommandParams=array())
{
    global $oGlobalFramework;
    $bGetDeviceMsgFlag = false;
    $oGlobalFramework->sm->get('\Lur\Service\Common')->sendAdrCommand($_GET['i'], $sCommandType, $aCommandParams);
    // 等待设备信息返回
    $bBreakFlag = false;
    $bWhileRetryTimes = 20;
    while (1) {
        if ($bWhileRetryTimes-- < 1)$bBreakFlag = true;
        sleep(5);

        $aAdrInfo = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrSerInfoById($_GET['i']);;
        if (isset($aAdrInfo['wifi']) && isset($aAdrInfo['wifi']['pi3wifi']) && count($aAdrInfo['wifi']['pi3wifi'])) {
            $bGetDeviceMsgFlag = true;
            $bBreakFlag = true;
        }
        
        if($bBreakFlag)break;
    }
    return array($bGetDeviceMsgFlag, $aAdrInfo);
}

$sAction = isset($_GET['action']) ? $_GET['action'] : "show";

try {
    $bGetDeviceMsgFlag = true;

    if (\YcheukfCommon\Lib\Functions::encryptBySlat($_GET['i']) !== $_GET['ei']) {
        throw new Exception("token check faild", 1);
        
    }

    if ($sAction == 'submit') {
        
        // $bGetDeviceMsgFlag = true;
        // $aAdrInfoTmp = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrSerInfoById($_GET['i']);;
        list($bGetDeviceMsgFlag, $aAdrInfoTmp) = callAdrWifiInfo("adrpi_updatewifi", $_POST);
// $_POST['wifi_name'] = "fenghome2";
        header('HTTP/1.1 200 OK');
        $sFaildMsg = "faild to get device wifi info";
        if ($bGetDeviceMsgFlag) {
                // var_dump($aAdrInfoTmp['wifi']['pi3wifi']);
            if (isset($aAdrInfoTmp['wifi']['pi3wifi']['wlan0stats']) && !empty($aAdrInfoTmp['wifi']['pi3wifi']['wlan0stats'])) {
                if (preg_match("/GENERAL\.DEVICE.*wlan0/", $aAdrInfoTmp['wifi']['pi3wifi']['wlan0stats'])
                    && preg_match("/GENERAL\.STATE.*connected/", $aAdrInfoTmp['wifi']['pi3wifi']['wlan0stats'])
                    && preg_match("/{$_POST['wifi_name']}/", $aAdrInfoTmp['wifi']['pi3wifi']['wlan0stats'])
                ) {
                // var_dump($aAdrInfoTmp['wifi']['pi3wifi']['wlan0stats']);
                    
                    echo '["WIFI CONNECT SUCCESS"]';                
                    exit;
                }else{
                // var_dump($aAdrInfoTmp['wifi']['pi3wifi']['wlan0stats']);

                    $sFaildMsg = "WIFI CONNECT FAILD";
                }
            }
        }

        header("HTTP/1.1 500 Internal Server Error");
        echo $sFaildMsg;
        exit;
    }
    

    $aAdrInfo = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrSerInfoById($_GET['i']);
    $aAdrPi3Config = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrPi3Config($_GET['i']);



    if ($aAdrInfo && isset($aAdrInfo['model']) && "TROLLEY-DIRECT" == $aAdrInfo['model']) {


        list($bGetDeviceMsgFlag, $aAdrInfoTmp) = callAdrWifiInfo("report_wifi2lur");
        if ($bGetDeviceMsgFlag) {
            $aAdrInfo = $aAdrInfoTmp;
        }


    }else{
        throw new Exception("adr is not in pi3 model:".$_GET['i'], 1);
    }

} catch (Exception $e) {
    echoMsg($e->getMessage());
    exit;
    
}


function getPi3ConfigValue($aAdrPi3Config, $sField)
{
    return $aAdrPi3Config && count($aAdrPi3Config) && isset($aAdrPi3Config[$sField]) ? $aAdrPi3Config[$sField] : "";
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>ADRPI - WIFI设置</title>

    <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
     <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
     <script src="http://apps.bdimg.com/libs/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</head>
<body>
<div class="page-header">
  <h1>ADRPI - WIFI设置<small></small></h1>
    <p>
</div>
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">wifi 列表, 若您的WIFI不在列表中, 请手动填写, 不支持中文</h3>
    </div>
    <div class="panel-body">
        <?php

        if (!$bGetDeviceMsgFlag) {
            echo <<<__EOF
            与设备通讯失败. 请刷新重试, 若重试后仍然不成功, 请检查设备的电源与网络是否正常工作
            <button type="button" class="btn btn-lg btn-default refresh">点击刷新</button>
__EOF;
        }else{
            $sWifiListHtml = "<ul>";
            if (!empty($aAdrInfo['wifi']['pi3wifi']['wifilist'])) {
                $sWifiList = $aAdrInfo['wifi']['pi3wifi']['wifilist'];
                $aWifiList = explode("\n", $sWifiList);
                if (count($aWifiList)) {
                    foreach ($aWifiList as $sRowTmp) {
                        $sRowTmp = str_replace("*", "", $sRowTmp);
                        $sRowTmp = trim($sRowTmp);
                        if (empty($sRowTmp) || preg_match("/SSID\s+MODE/i", $sRowTmp)) {
                            continue;
                        }
                        $sRowTmp = preg_replace("/\s+/", " ", $sRowTmp);
                        $aWifiSet = explode(" ", $sRowTmp);
                        $sWifiListHtml .= '<li><div class="radio">
                            <label>
                                <input type="radio" name="wifioptions" class="wifioptions"  value="'.$aWifiSet[0].'" >
                                '.$aWifiSet[0].'
                            </label>
                        </div>';


                    }
                }
            }
            $sWifiListHtml .= "</ul>";
            $sWifiListHtml .= '
            <form action="" method="POST" id="wifiform" role="form">
                <legend>提交WIFI信息</legend>
            
                <div class="form-group">

                    <label for="wifi_name" class="col-sm-2 control-label">WIFI名称:</label>
                    <input type="text" name="wifi_name" id="wifi_name" class="form-control" value="'.getPi3ConfigValue($aAdrPi3Config, 'var3').'" required="required" pattern="" title="" placeholder="从上面列表选择你的wifi, 或者自己填写">

                    <label for="wifi_psw" class="col-sm-2 control-label">WIFI密码:</label>
                        <input type="text" name="wifi_psw" id="wifi_psw"  class="form-control" value="'.getPi3ConfigValue($aAdrPi3Config, 'var4').'" required="required" pattern="" title="" placeholder="WIFI密码">

                    <label for="device_name" class="col-sm-2 control-label">设备名称:</label>
                    <input type="text" name="device_name" id="device_name"  class="form-control" value="'.getPi3ConfigValue($aAdrPi3Config, 'var1').'" required="required" pattern="" title="" placeholder="例如 张三家的PI3">

                    <label for="device_contact" class="col-sm-2 control-label">联系手机:</label>
                    <input type="text" name="device_contact" id="device_contact"  class="form-control" value="'.getPi3ConfigValue($aAdrPi3Config, 'var2').'" required="required" pattern="" title="" placeholder="我们只会在设备故障需要重启的时候联系你">
                    <p>
                    <center>
                    <button type="button" class="btn btn-lg btn-success submit">提交WIFI信息</button>
                    <span class="label label-info wifimsg" style="display:none">正在设置WIFI信息, 大约需要1-2分钟, 请稍候</span>
                    <div class="alert alert-info alertfaild" style="display:none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>WIFI设置失败</strong>请检查您的WIFI设置是否正确
                    </div>
                    </center>
                </div>
            </form>

                ';
            
                
            

            echo $sWifiListHtml;

        }

        ?>
    </div>
</div>

</body>
</html>
<script type="text/javascript">
    $(".wifioptions").click(function(event) {
        console.log($(this).val())
        $('#wifi_name').val($(this).val());
    });
    $(".refresh").click(function(event) {
        window.location.reload()
    });

    $(".submit").click(function(event) {
        $(this).toggle();
        $(".wifimsg").toggle();

        $.ajax({
            url: 'http://<?php echo LAF_WEBHOST;?>/module/lur/piwifi/setupwifi.php?action=submit&i=<?php echo $_GET['i']?>&ei=<?php echo $_GET['ei']?>',
            type: 'POST',
            dataType: 'json',
            data: $('#wifiform').serializeArray(),
        })
        .done(function() {
            $('body').html("<h2>WIFI设置成功, 正在为您跳转..</h2>");
            setTimeout(function() {
                window.location.href='http://<?php echo LAF_WEBHOST;?>/module/lur/piwifi.php?i=<?php echo $_GET['i']?>&ei=<?php echo $_GET['ei']?>'
            }, 30);

        })
        .fail(function() {

            console.log("error");
            $(".submit").toggle();
            $(".wifimsg").toggle();
            $(".alertfaild").show();

        })
        // .always(function() {

        //     console.log("complete");
        //     $(".submit").toggle();
        //     $(".wifimsg").toggle();
        // });
        
        // .serializeArray()
    });


</script>