<?php
/**
 * 本API已废弃, 功能移动至iproxy中
 * 
 */

set_time_limit(0);
ini_set("auto_detect_line_endings", "1");
include_once(__DIR__."/../../../config/autoload/defined.global.php");

chdir(LAF_BASEPATH);
include(LAF_BASEPATH."/init_autoloader.php");

function httpresponse($sMsg='', $nStatus=\Zend\Http\PhpEnvironment\Response::STATUS_CODE_200)
{

    $response = new \Zend\Http\PhpEnvironment\Response();
    $response->setStatusCode($nStatus);
    $response->setContent($sMsg);
    $response->getHeaders()->addHeaders(array(
            'Access-Control-Allow-Origin' => '*'
        )
    );
    $response->send();
    exit;
}

$aCacheConfig = file_exists(LAF_BASEPATH."/config/autoload/cache.local.php") ? require LAF_BASEPATH."/config/autoload/cache.local.php" : require LAF_BASEPATH."/config/autoload/cache.global.php";

$aLurConfig = require LAF_BASEPATH."/module/Lur/config/module.config.php";

if (!isset($_GET['scity']) || empty($_GET['scity'])) {
    httpresponse('emtyp scity', \Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
}

if($aCacheConfig['cache_config']['type'] == 'redis'){
    try{
        $oRedis = new \Redis();
        $oRedis->connect($aCacheConfig['cache_config']['params']['servers'][0][0], $aCacheConfig['cache_config']['params']['servers'][0][1]);
        $oRedis->select($aCacheConfig['cache_config']['params']['servers'][0][2]);
    }catch(\RedisException $e){
        httpresponse($e->getMessage(), \Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
        exit;
    }
}else{
    httpresponse("can not connent to redis", \Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
}


$sCacheKey = "proxy_commonconfig";
$aProxyConfig = $oRedis->get($sCacheKey);
if($aProxyConfig == false){
    sleep(3);
    $aProxyConfig = $oRedis->get($sCacheKey);
    if($aProxyConfig == false){
        return httpresponse("can not get ".$sCacheKey,500);
    }
}
list($aProxyIpconfig, $aCityName2Id) = json_decode($aProxyConfig, 1);

if (!isset($aCityName2Id[$_GET['scity']])) {
    return httpresponse("match city faild:".$_GET['scity'],500);
}

$nCityId = $aCityName2Id[$_GET['scity']];
$sReturn = "";
foreach ($aProxyIpconfig as $sKey => $value) {
    $sCacheKey = "iproxy_".$sKey."/".$nCityId;
    $sTmp = $oRedis->get($sCacheKey);
    if ($sTmp == false) {
        continue;
    }
    $sTmp = json_decode($sTmp, 1);
    if (!is_null($sTmp)) {
        $sReturn .= "\n###key=".$sKey."\n".$sTmp;
        # code...
    }
}

//  unique
$aTmp = explode("\n", $sReturn);
$aTmp = array_unique($aTmp);

return httpresponse(join("\n", $aTmp), 200);




