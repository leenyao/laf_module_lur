<?php
/**
 * 接收adr shell结果
 * 
 */
include_once(__DIR__."/../../../bin/script/simplerestapi.inc.php");
$oSimpleRestApi = new SimpleRestApi();
try{

    if ($_FILES["file"]["error"] > 0)
    {
        print_r($_FILES);
        echo "Error file: " . $_FILES["file"]["error"] . "<br />";
        throw new \Exception("Error Processing Request", 1);
        
    }
    if (empty($_GET['adrid'])) {
        echo "Error adrid: " . $_GET['adrid'] . "<br />";
        throw new \Exception("Error Processing Request", 1);
    }
    $path = BASE_INDEX_PATH.'/../data/adrresult/'.$_GET['no'];
    $filepath = $path."/".$_GET['adrid'];
    exec('mkdir -p '.$path);
    exec('rm -f '.$filepath);

    $oReids = \Application\Model\Common::getCacheObject($oGlobalFramework->sm);

    // _GET['adrid']

    exec('echo "-,'.$_GET['no'].'" > '.BASE_INDEX_PATH.'/../data/lastadrresult');


    $taskid = $oReids->get(LAF_CACHE_CURRENT_SHELLID_137);
    $sShellKeySucc = LAF_CACHE_ADRSHELLLIST_OK_138."/".$taskid;
    $sShellKeyLock = LAF_CACHE_ADRSHELLLIST_136."/".$taskid;

    // 加入成功列表
    $oReids->hSet($sShellKeySucc, $_GET['adrid'], json_encode([
        "t" => date("Y-m-d H:i:s"),
        "no" => $_GET['no'],
    ]));
    $oReids->setTimeout($sShellKeySucc,100*3600);
    $oReids->set(LAF_CACHE_CURRENT_SHELLID_LASTACTIVE_139, time());


    // 去掉锁
    $adrHost = $oGlobalFramework->sm->get('\Lur\Service\Common')->getAdrId2Host($_GET['adrid']);
    $oReids->hDel($sShellKeyLock, $adrHost);

    

    move_uploaded_file($_FILES["file"]["tmp_name"],
    $filepath);
    echo 1;
}catch(\Exception $e){

    $oSimpleRestApi->httpresponse($e->getMessage(), 500, false);

}