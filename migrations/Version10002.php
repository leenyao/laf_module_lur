<?php

namespace YcheukfMigration\Migration;

use YcheukfMigration\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version10002 extends AbstractMigration
{
    public static $description = "加载LUR";

    public function up(MetadataInterface $schema)
    {
        $aDbConfig = $this->serviceManager->get('config');
        $aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($aDbConfig['db_master']['dsn']);
        $sDbName = $aDsn['dbname']; 
        $sDbHost = $aDsn['host']; 
        $sDbPort = $aDsn['port']; 
        $sDbUsername = $aDbConfig['db_master']['username'];
        $sDbPassword = $aDbConfig['db_master']['password'];
        $aTables = $schema->getTableNames();

        system("mysql -u".$sDbUsername." -p".$sDbPassword." -h".$sDbHost."  --port=".$sDbPort."  --default-character-set=utf8 ".$sDbName." < ".__DIR__."/Version10002.sql");

        $pdo = new \PDO($aDbConfig['db_master']['dsn'], $sDbUsername, $sDbPassword, array (\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES'utf8';"));

        $sSql = "select user_id,display_name    from system_users";
        $sth = $pdo->prepare($sSql);
        $sth->execute();
        $oPDOStatement = $sth->fetchAll();

        foreach($oPDOStatement as $result) {

            // var_dump($result);
            $sSql = "insert into b_adcustomer (id, name, user_id) values(?, ?, ?)";

            $sth = $pdo->prepare($sSql);
            $sth->execute(array($result['user_id'], $result['display_name'], 1044));


        }
    }


    public function down(MetadataInterface $schema)
    {
    }
}
