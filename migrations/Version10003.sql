
-- 判断索引是否存在, 存在则删除
set @table_name := 'b_adschedule';
set @index_name := 'user_id';
set @exist := (select count(*) from information_schema.statistics where table_name = @table_name and index_name = @index_name and table_schema = database());
set @sql_drop := concat('drop index  ', @index_name, " on ",  @table_name);
set @sqlstmt := if( @exist > 0, @sql_drop, 'select "no index drop"');
PREPARE stmt FROM @sqlstmt;
EXECUTE stmt;

set @index_name := 'abc';
set @exist := (select count(*) from information_schema.statistics where table_name = @table_name and index_name = @index_name and table_schema = database());
set @sql_drop := concat('drop index  ', @index_name, " on ",  @table_name);
set @sqlstmt := if( @exist > 0, @sql_drop, 'select "no index drop"');
PREPARE stmt FROM @sqlstmt;
EXECUTE stmt;


set @index_name := 'spliturl';
set @exist := (select count(*) from information_schema.statistics where table_name = @table_name and index_name = @index_name and table_schema = database());
set @sql_drop := concat('drop index  ', @index_name, " on ",  @table_name);
set @sqlstmt := if( @exist > 0, @sql_drop, 'select "no index drop"');
PREPARE stmt FROM @sqlstmt;
EXECUTE stmt;

set @index_name := 'index1';
set @exist := (select count(*) from information_schema.statistics where table_name = @table_name and index_name = @index_name and table_schema = database());
set @sql_drop := concat('drop index  ', @index_name, " on ",  @table_name);
set @sqlstmt := if( @exist > 0, @sql_drop, 'select "no index drop"');
PREPARE stmt FROM @sqlstmt;
EXECUTE stmt;

set @index_name := 'index2';
set @exist := (select count(*) from information_schema.statistics where table_name = @table_name and index_name = @index_name and table_schema = database());
set @sql_drop := concat('drop index  ', @index_name, " on ",  @table_name);
set @sqlstmt := if( @exist > 0, @sql_drop, 'select "no index drop"');
PREPARE stmt FROM @sqlstmt;
EXECUTE stmt;


ALTER TABLE `laf_lur`.`b_adschedule` ADD INDEX `index1`  (`op_user_id`, `m102_id`, `modified`,`id`);
ALTER TABLE `laf_lur`.`b_adschedule` ADD INDEX `index2`  (`m102_id`, `modified`,`id`);




set @table_name := 'system_metadata';
set @index_name := 'type';
set @exist := (select count(*) from information_schema.statistics where table_name = @table_name and index_name = @index_name and table_schema = database());
set @sql_drop := concat('drop index  ', @index_name, " on ",  @table_name);
set @sqlstmt := if( @exist > 0, @sql_drop, 'select "no index drop"');
PREPARE stmt FROM @sqlstmt;
EXECUTE stmt;
set @table_name := 'system_metadata';
set @index_name := 'index1';
set @exist := (select count(*) from information_schema.statistics where table_name = @table_name and index_name = @index_name and table_schema = database());
set @sql_drop := concat('drop index  ', @index_name, " on ",  @table_name);
set @sqlstmt := if( @exist > 0, @sql_drop, 'select "no index drop"');
PREPARE stmt FROM @sqlstmt;
EXECUTE stmt;
ALTER TABLE `laf_lur`.`system_metadata` ADD INDEX `index1`  (`type`, `resid`, `status`);






DROP TRIGGER IF EXISTS `ti_b_adschedule`;
DELIMITER ;;
CREATE TRIGGER `ti_b_adschedule` AFTER INSERT ON `b_adschedule` FOR EACH ROW BEGIN
    INSERT INTO system_trigger_log SET type = 'b_adschedule', optype = 'i', resId = NEW.id, memo=concat(
        'schedule:', NEW.`schedule`
        , ' ,stable_cookie_percent:', NEW.`stable_cookie_percent`
        ,' ,split_index:', NEW.`split_index`
        ,' ,m102_id:',NEW.`m102_id`
        ,' ,m1014_id:',NEW.`m1014_id`
        , ' ,cookie_code_id:',NEW.`cookie_code_id`
        , ' ,user_id:',NEW.`user_id`
        , ' ,op_user_id:',NEW.`op_user_id`
      ), crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `tu_b_adschedule`;
DELIMITER ;;
CREATE TRIGGER `tu_b_adschedule` AFTER UPDATE ON `b_adschedule` FOR EACH ROW BEGIN
    IF 
            OLD.id != NEW.id  or  OLD.modified != NEW.modified 
    THEN
        INSERT INTO system_trigger_log SET type = 'b_adschedule', optype = 'u', resId = NEW.id, memo=concat(
        'schedule:', NEW.`schedule`
        , ' ,stable_cookie_percent:', NEW.`stable_cookie_percent`
        ,' ,split_index:', NEW.`split_index`
        ,' ,m102_id:',NEW.`m102_id`
        ,' ,m1014_id:',NEW.`m1014_id`
        , ' ,cookie_code_id:',NEW.`cookie_code_id`
        , ' ,user_id:',NEW.`user_id`
        , ' ,op_user_id:',NEW.`op_user_id`
        ),  crt_timestamp = CURRENT_TIMESTAMP();
    END IF;
  END
;;
DELIMITER ;
