

DROP TABLE IF EXISTS `b_adcustomer`;
CREATE TABLE `b_adcustomer` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id. 0即所有人可读到',
  `name` varchar(250) DEFAULT NULL COMMENT '客户名称',
  `m101_id` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  unique key `name` (`user_id`, `name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户列表';


ALTER TABLE `laf_lur`.`b_adschedule` CHANGE COLUMN `user_id` `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '客户. b_adcustomer.id', ADD COLUMN `op_user_id` int NOT NULL DEFAULT 0 COMMENT '操作用户的id' AFTER `modified`;


-- 将旧数据移动到rayee账号下
update laf_lur.b_adschedule set op_user_id=1044;
update laf_lur.system_batch_jobs set op_user_id=1044;

-- 降rayee密码改为 ry1234
-- update  `laf_lur`.`system_users` set password='29c4828490b36f63c59760053e46c0d6' set where user_id=1044;
-- update  `laf_lur`.`oauth_users` set password='29c4828490b36f63c59760053e46c0d6' set where id=1044;

-- 修改 admin 密码为 psw2
-- update  `laf_lur`.`system_users` set password='8458d7f272ee14a31ea286525b79520e' set where username='admin';
-- update  `laf_lur`.`oauth_users` set password='8458d7f272ee14a31ea286525b79520e' set where username='admin';



-- ruiyiadmin
INSERT INTO `oauth_users` VALUES ('1044', 'ruiyiadmin', '4aaf3fc7e3da233a6ff330304aa86629', '1', '', '', '2014-06-25 15:31:42', '3', 0, '2000-01-01 01:01:01', '2000-01-01 01:01:01');
INSERT INTO system_users (`id`,`user_id`,`role_id`, `username`,`email`,`phone`,`display_name`) VALUES('1044', '1044', 8, 'ruiyiadmin', 'fengruzhuo@163.com', '13800138000', 'ruiyiadmin'); 
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (6, 1044, 8);

-- mzbrand
INSERT INTO `oauth_users` VALUES ('2000', 'mzbrand', '4aaf3fc7e3da233a6ff330304aa86629', '1', '', '', '2014-06-25 15:31:42', '3', 0, '2000-01-01 01:01:01', '2000-01-01 01:01:01');
INSERT INTO system_users (`id`,`user_id`,`role_id`, `username`,`email`,`phone`,`display_name`) VALUES('2000', '2000', 8, 'mzbrand', '51380949@qq.com', '13800138000', 'mzbrand'); 
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (6, 2000, 8);

INSERT into `b_adcustomer` (`id`, `user_id`, `name`)VALUES(2015,1044, '测试客户');
