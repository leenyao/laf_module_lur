

DROP TABLE IF EXISTS `b_addata`;
CREATE TABLE `b_addata` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `m1005_id` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '对应表名',
  `datapath` varchar(250) DEFAULT NULL COMMENT '数据源地址',
  `memo` text  COMMENT '描述',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表数据';



DROP TABLE IF EXISTS `b_adrstats`;
CREATE TABLE `b_adrstats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(250) DEFAULT NULL COMMENT 'adr的主机',
  `data` longtext  COMMENT 'json stats data',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型 1=>普通状态;2=>完整状态',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created_time` (`created_time`),
  KEY `host` (`host`,`type`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='adr状态数据';

DROP TABLE IF EXISTS `b_usstats`;
CREATE TABLE `b_usstats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data` longtext  COMMENT 'json stats data',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='us状态数据';



DROP TABLE IF EXISTS `b_adschedule`;
CREATE TABLE `b_adschedule` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cookie_code_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '所使用cookie的 code id',
  `cookie_domain` varchar(200) DEFAULT NULL COMMENT 'cookie domain',
  `m1023_id` varchar(200) NOT NULL DEFAULT '0' COMMENT 'ta库id',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `code` text COMMENT '代码',
  `uv2ndurl` text COMMENT 'uv二跳代码',
  `stable_cookie_percent` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'stable cookie的百分比',
  `domaincookie_delete_percent` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'domain cookie的删除概率',
  `code_label` varchar(250) DEFAULT '' COMMENT '代码名称',
  `sspctr` varchar(250) DEFAULT '' COMMENT 'ssp的ctr',
  `sspmzid` text COMMENT 'ssp的媒资id',
  
  
  `split_fid` int(11) NOT NULL DEFAULT '0',
  `split_index` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'url被切割的序号',
  `referer` text COMMENT '来源',
  `m102_id` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;2=>禁止;3=>删除',
  `m1014_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT '广告类型,1=>普通,2=>sdk',
  `schedule` longtext COMMENT 'ad schedle',
  `uv2schedule` longtext COMMENT 'ad uv2 schedle',
  `uv2_percent` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '二跳概率',
  `schedule_md5` varchar(200) DEFAULT NULL COMMENT 'schedule md5',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `abc` (`user_id`,`split_index`,`m102_id`),
  KEY `spliturl` (`split_index`,`m102_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='排期广告';


DROP TABLE IF EXISTS `b_adcounter`;
CREATE TABLE `b_adcounter` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `adrserver_id` int(8) unsigned NOT NULL DEFAULT '0' COMMENT 'adr的主机id',
  `date` date NOT NULL DEFAULT '1970-01-01' COMMENT '日期',
  `code_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT ' code id',
  `count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`adrserver_id`,`date`,`code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告代码计数器';


DROP TABLE IF EXISTS `b_adcounter_ssp`;
CREATE TABLE `b_adcounter_ssp` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `adrserver_id` int(8) unsigned NOT NULL DEFAULT '0' COMMENT 'adr的主机id',
  `date` date NOT NULL DEFAULT '1970-01-01' COMMENT '日期',
  `code_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT ' code id',
  `city_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT ' city_id',
  `code_type` varchar(250) DEFAULT '' COMMENT '访问类型',
  `ssp_type` varchar(250) DEFAULT 'hunan' COMMENT '访问类型',
  `count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告代码计数器-SSP';
ALTER TABLE `laf_lur`.`b_adcounter_ssp` ADD COLUMN `code_cid` bigint(20) UNSIGNED COMMENT '订单id' AFTER `date`;
ALTER TABLE `laf_lur`.`b_adcounter_ssp` ADD INDEX  (`code_cid`) comment '';
ALTER TABLE `laf_lur`.`b_adcounter_ssp` ADD INDEX  (`date`,`code_type`,`ssp_type`,`code_cid`,`count`) comment '';

ALTER TABLE `laf_lur`.`b_adcounter_ssp` ADD COLUMN `code_lid` bigint(20) UNSIGNED COMMENT 'lur的代码id' AFTER `code_cid`;


DROP TABLE IF EXISTS `b_report_a`;
CREATE TABLE `b_report_a` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `date` date NOT NULL DEFAULT '1970-01-01',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `d1` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 1',
  `d2` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `m1` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m2` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m3` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `crt_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`user_id`, `date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `b_report_b`;
CREATE TABLE `b_report_b` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `date` date NOT NULL DEFAULT '1970-01-01',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `d1` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 1',
  `d2` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `m1` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m2` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `m3` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `crt_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`user_id`, `date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `b_report_c`;
CREATE TABLE `b_report_c` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `date` date NOT NULL DEFAULT '1970-01-01',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `md5key` varchar(200) NOT NULL DEFAULT '0' COMMENT '',
  `dc1` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 1',
  `dc2` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `dc3` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `dc4` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `dc5` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `mc1` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `mc2` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `mc3` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `mc4` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `md5key` (`md5key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `b_report_d`;
CREATE TABLE `b_report_d` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `date` date NOT NULL DEFAULT '1970-01-01',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `md5key` varchar(200) NOT NULL DEFAULT '0' COMMENT '',
  `dd1` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 1',
  `dd2` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `dd3` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `dd4` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `dd5` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `md1` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `md2` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `md3` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `md4` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `md5key` (`md5key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `b_report_e`;
CREATE TABLE `b_report_e` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `date` date NOT NULL DEFAULT '1970-01-01',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `md5key` varchar(200) NOT NULL DEFAULT '0' COMMENT '',
  `de1` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 1',
  `de2` text  NULL COMMENT 'dimon 2',
  `de3` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `de4` text  NULL COMMENT 'dimon 2',
  `de5` varchar(200) NOT NULL DEFAULT '0' COMMENT 'dimon 2',
  `me1` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `me2` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `me3` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `me4` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `md5key` (`md5key`),
  KEY `datede1` (`date`,`de1`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `b_schedule3rd_log`;
CREATE TABLE `b_schedule3rd_log` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `atime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '进队列时间',
  `ptime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '处理时间',
  `cid` int(8) unsigned NOT NULL DEFAULT 0 COMMENT '客户id',
  `p_in` text  NULL COMMENT '进队列的参数',
  `p_in2` text  NULL COMMENT '处理后的参数',
  `p_result` text  NULL COMMENT '处理后的结果',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `atime` (`atime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



DROP VIEW IF EXISTS `system_relation_1007`;
CREATE VIEW `system_relation_1007` AS select `system_relation`.`cid` AS `cid`,`system_metadata`.`label` AS `clabel`,`system_relation`.`fid` AS `fid` from (`system_relation` join `system_metadata` on((`system_relation`.`cid` = `system_metadata`.`resid`))) where ((`system_relation`.`type` = 1007) and (`system_metadata`.`type` = 1007));
DROP VIEW IF EXISTS `system_relation_1008`;
CREATE VIEW `system_relation_1008` AS select `system_relation`.`cid` AS `cid`,`system_metadata`.`label` AS `clabel`,`system_relation`.`fid` AS `fid` from (`system_relation` join `system_metadata` on((`system_relation`.`cid` = `system_metadata`.`resid`))) where ((`system_relation`.`type` = 1008) and (`system_metadata`.`type` = 1008));
DROP VIEW IF EXISTS `system_relation_1009`;
CREATE VIEW `system_relation_1009` AS select `system_relation`.`cid` AS `cid`,`system_metadata`.`label` AS `clabel`,`system_relation`.`fid` AS `fid` from (`system_relation` join `system_metadata` on((`system_relation`.`cid` = `system_metadata`.`resid`))) where ((`system_relation`.`type` = 1009) and (`system_metadata`.`type` = 1009));
DROP VIEW IF EXISTS `system_relation_1015`;
CREATE VIEW `system_relation_1015` AS select `system_relation`.`cid` AS `cid`,`system_metadata`.`label` AS `clabel`,`system_relation`.`fid` AS `fid` from (`system_relation` join `system_metadata` on((`system_relation`.`cid` = `system_metadata`.`resid`))) where ((`system_relation`.`type` = 1015) and (`system_metadata`.`type` = 1015));



DROP TRIGGER IF EXISTS `ti_b_adschedule`;
DELIMITER ;;
CREATE TRIGGER `ti_b_adschedule` AFTER INSERT ON `b_adschedule` FOR EACH ROW BEGIN
    INSERT INTO system_trigger_log SET type = 'b_adschedule', optype = 'i', resId = NEW.id, memo=concat('schedule:', NEW.`schedule`, ' ,stable_cookie_percent:', NEW.`stable_cookie_percent`,' ,split_index:', NEW.`split_index`,' ,m102_id:',NEW.`m102_id`,' ,m1014_id:',NEW.`m1014_id`, ' ,cookie_code_id:',NEW.`cookie_code_id`), crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `tu_b_adschedule`;
DELIMITER ;;
CREATE TRIGGER `tu_b_adschedule` AFTER UPDATE ON `b_adschedule` FOR EACH ROW BEGIN
    IF 
            OLD.id != NEW.id  or  OLD.modified != NEW.modified 
    THEN
        INSERT INTO system_trigger_log SET type = 'b_adschedule', optype = 'u', resId = NEW.id, memo=concat('schedule:', NEW.`schedule`, ' ,stable_cookie_percent:', NEW.`stable_cookie_percent`,' ,split_index:', NEW.`split_index`,' ,m102_id:',NEW.`m102_id`,' ,m1014_id:',NEW.`m1014_id`, ' ,cookie_code_id:',NEW.`cookie_code_id`),  crt_timestamp = CURRENT_TIMESTAMP();
    END IF;
  END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `td_b_adschedule`;
DELIMITER ;;
CREATE TRIGGER `td_b_adschedule` AFTER DELETE ON `b_adschedule` FOR EACH ROW BEGIN
        INSERT INTO system_trigger_log SET type = 'b_adschedule', optype = 'd', resId = OLD.id,  crt_timestamp = CURRENT_TIMESTAMP();
  END
;;
DELIMITER ;


-- Records of rbac_permission
-- ----------------------------
-- lv1 1-100
INSERT INTO `rbac_permission` VALUES ('1', '', '', '1', '2013-11-24 15:24:40', '0', 'menu_report');

-- lv2 101-1000
INSERT INTO `rbac_permission` VALUES ('101', '', '', '1', '2013-11-24 15:24:40', '1', 'menu_report');

-- lv3 1001+
INSERT INTO `rbac_permission` VALUES (null, 'zfcadmin/report/reporta', '', '1', '2013-11-24 15:42:19', '101', 'view');

INSERT INTO rbac_role_permission VALUES (null, 8, (select id from `rbac_permission` where perm_name='zfcadmin/report/reporta'), '2015-11-05 11:11:11');


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1005, 1, 'reporta');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1005, 2, 'reportb');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1005, 3, 'reportc');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1005, 4, 'reportd');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1005, 5, 'reporte');

INSERT INTO `b_addata` (`user_id`, `m1005_id`, `datapath`) VALUES ('2', '1', '/module/lur/demo/report_a.csv');
INSERT INTO `b_addata` (`user_id`, `m1005_id`, `datapath`) VALUES ('2', '2', '/module/lur/demo/report_b.csv');
INSERT INTO `b_addata` (`user_id`, `m1005_id`, `datapath`) VALUES ('2', '4', '/module/lur/demo/report_d.csv');
INSERT INTO `b_addata` (`user_id`, `m1005_id`, `datapath`) VALUES ('2', '5', '/module/lur/demo/report_e.csv');


-- adr log status
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1006, 101,'成功');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1006, 104,'访问超时');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1006, 105,'返回错误');


-- adr city mapping
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 1,'上海');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 14,'杭州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 15,'北京');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 16,'广州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 17,'深圳');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 18,'天津');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 19,'南京');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 20,'重庆');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 21,'青岛');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 22,'大连');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 23,'成都');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 24,'武汉');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 25,'西安');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 26,'长沙');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 27,'郑州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 28,'东莞');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 29,'太原');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 30,'昆明');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 31,'沈阳');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 32,'宁波');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 33,'无锡');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 34,'郑州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 35,'哈尔滨');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 36,'南昌');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 37,'佛山');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 38,'南宁');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 39,'嘉兴');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 40,'乌鲁木齐');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 41,'扬州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 42,'台州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 43,'玉林');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 44,'温州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 45,'合肥');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 46,'芜湖');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 47,'苏州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 48,'济南');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 49,'厦门');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 50,'石家庄');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 51,'福州');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1007, 52,'长春');


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1008, 1,'pc');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1008, 2,'android');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1008, 3,'ios');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1008, 4,'pad');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1008, 5,'xiaomi');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1008, 6,'coolpad');

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1009, 1,'bf-211.152.39.66');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1009, 2,'bf-203.166.162.30');

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1018, 1,'bf-211.152.39.66');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1018, 2,'bf-203.166.162.30');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1018, 3,'adr-trolley-vpn');

INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1021, 3,'adr-trolley-vpn');


-- ignore ad hostname
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 1,'vcm.chinavivaki.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 2,'cm.e.qq.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 3,'cm.ipinyou.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 4,'cc.xtgreat.com/admaster.gif');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 5,'m.reachmax.cn/ad');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 6,'mapping.yoyi.com.cn');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 7,'idm.bce.baidu.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 8,'cm.g.doubleclick.net');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 9,'cm.jd.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 10,'cm.zintow.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 11,'cms.tanx.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 12,'map.dxpmedia.com/cm');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 13,'www.admaster.com.cn');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 14,'its.fugetech.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 15,'pass.tmall.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1010, 16,'login.taobao.com');

-- ipproxy url
-- INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1011, 1,'http://dev.kuaidaili.com/api/getproxy/?orderid=935261268871827&num=30&area=[cityurlencode]&sort=1');
-- INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1011, 2,'http://ervx.daili666.com/ip/?tid=558369258572339&num=30&sortby=time&area=[cityurlencode]');
-- INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1011, 4,'http://ipproxy.leenyao.com/api/ip?token=13962a512b88f0801b368b54dddc2320&l=[cityurlencode]');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1011, 5,'http://ipproxy.leenyao.com/api/ip?token=13962a512b88f0801b368b54dddc2320&l=[cityurlencode]&v=internal');

-- INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1011, 6,'http://ip.baizhongsou.com/?u=qq2505821155&p=b63b5ef58ac7fb67&sl=100&dq=[cityurlencode]');


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1012, 1,'fengruzhuo@163.com');


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1013, 1,'m.reachmax.cn/ad.gif');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1013, 2,'v.admaster.com.cn');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1013, 3,'cm.ipinyou.com');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1013, 4,'origin.allyes.com/pixel');


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1014, 1,'WEB');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1014, 2,'SDK');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1014, 3,'UV');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1014, 4,'VPC');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1014, 5,'SSP-HUNAN');


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1015, 1, '{"ios":{"mn":"乐视视频","mp":"com.letv.iphone.client", "appver":["3.1.5.3","3.1.5.4"]},"android":{"mn":"乐视视频","mp":"com.letv.android.client"}}');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1015, 2, '{"ios":{"mn":"优酷","mp":"com.youku.YouKu","appver":["20160509"]},"android":{"mn":"优酷","mp":"com.youku.phone"}}');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1015, 3, '{"ios":{"mn":"Tudou","mp":"com.tudou.tudouiphone","appver":["16050318"]},"android":{"mn":"土豆视频","mp":"com.tudou.android"}}');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1015, 4, '{"ios":{"mn":"SOHUVideo","mp":"com.sohu.iPhoneVideo","appver":["5.6.0.1"]},"android":{"mn":"搜狐视频","mp":"com.sohu.sohuvideo"}}');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1015, 5, '{"ios":{"mn":"live4iphone","mp":"com.tencent.live4iphone", "appver":["12120"]},"android":{"mn":"腾讯视频","mp":"com.tencent.qqlive"}}');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1015, 6, '{"ios":{"mn":"爱奇艺","mp":"com.qiyi.iphone","appver":["20160505143200"]},"android":{"mn":"爱奇艺","mp":"com.qiyi.video"}}');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1015, 7, '{"android":{"mn":"视频","mp":"com.ktcp.tvvideo"}}');

-- 运营邮箱
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1016, 1,'51380949@qq.com');



INSERT INTO `oauth_users` VALUES ('1001', 'adr_luhu', 'adr_luhu', '1', '', '', '2014-06-25 15:31:42', '3', 0, '2000-01-01 01:01:01', '2000-01-01 01:01:01');
INSERT INTO `oauth_users` VALUES ('1002', 'adr_bsd', 'adr_bsd', '1', '', '', '2014-06-25 15:31:42', '3', 0, '2000-01-01 01:01:01', '2000-01-01 01:01:01');

INSERT INTO system_users (`id`,`user_id`,`username`,`email`,`phone`,`display_name`) VALUES('1001', '1001', 'adr_luhu', 'admin@loclahost.com', '', '路虎'); 
INSERT INTO system_users (`id`,`user_id`,`username`,`email`,`phone`,`display_name`) VALUES('1002', '1002', 'adr_bsd', 'admin@loclahost.com', '', '波士顿'); 


-- city
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1007', '1', '15');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1007', '1', '16');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1007', '2', '1');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1007', '2', '16');

-- ua
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1008', '1', '1');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1008', '1', '2');

-- server
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1009', '1', '1');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1009', '1', '2');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1009', '2', '1');

-- app info
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1015', '1', '1');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1015', '2', '2');
INSERT INTO `system_relation` (`type`, `fid`, `cid`) VALUES ('1015', '2', '3');



INSERT into system_batch_jobs set memo='双列', user_id=1, m158_id=1, type=1, in_file='/../module/Lur/config/sample-double-code.csv', out_text='';
INSERT into system_batch_jobs set memo='单列', user_id=1, m158_id=1, type=1, in_file='/../module/Lur/config/sample-single-code.csv', out_text='';
INSERT into system_batch_jobs set memo='UV', user_id=1, m158_id=1, type=1, in_file='/../module/Lur/config/sample-uv-code.csv', out_text='';
INSERT into system_batch_jobs set memo='VPC', user_id=1, m158_id=1, type=1, in_file='/../module/Lur/config/sample-vpc-code.csv', out_text='';
INSERT into system_batch_jobs set memo='SSP-HUNAN', user_id=1, m158_id=1, type=1, in_file='/../module/Lur/config/sample-ssphunan-code.csv', out_text='';




DROP TABLE IF EXISTS `b_schedule3rd`;
CREATE TABLE `b_schedule3rd` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
  `token` varchar(250) DEFAULT NULL COMMENT 'token',
  `json_data` longtext  COMMENT 'json 数据',
  `memo` text  COMMENT '描述',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='第三方排期';
INSERT INTO `b_schedule3rd` (`id`, `user_id`, `token`, `json_data`, `status`, `created_time`, `modified`) VALUES ('99', '0', 'a607ab1b7e863dfff894ada6b2af56db', '{\"adverName\":\"ap+ipad\",\"adverPlan\":{\"2016-07-07\":23,\"2016-07-05\":21,\"2016-07-06\":22,\"2016-07-04\":20},\"clickMonitors\":[\"http://ark.letv.com/k?_oid=2241&_rtname=click&_rt=2&_u=apdianji1212\"],\"completeMonitors\":[],\"coopAdverCode\":\"67\",\"directStrategy\":{\"isAreaNegative\":0,\"putAreaId\":[1156000000],\"putHourPeriodId\":[1,2,3,4,5,6,7],\"clickRate\":33.33,\"platform\":\"aphone\"},\"endDate\":\"2016-07-07 23:59:59\",\"frequencyPeriod\":0,\"landingPage\":\"\",\"materialDuration\":0,\"materialURL\":\"\",\"maxFrequency\":0,\"minFrequency\":0,\"startDate\":\"2016-07-04 00:00:00\",\"viewMonitors\":[\"http://ark.letv.com/k?_oid=2241&_rtname=Impression&_rt=1&_u=apbaogaung1313\",\"http://ark.letv.com/k?_oid=2241&_rtname=Impression&_rt=1&_u=apbaogaung1212\"],\"memo\":\"频控1\"}', '1', '2016-07-04 18:00:27', '2016-07-04 18:00:27');
INSERT INTO `b_schedule3rd` (`id`, `user_id`, `token`, `json_data`, `status`, `created_time`, `modified`) VALUES ('100', '0', 'a607ab1b7e863dfff894ada6b2af56db', '{\"adverName\":\"ip+ipad\",\"adverPlan\":{\"2016-07-05\":2,\"2016-07-06\":3},\"clickMonitors\":[\"http://ark.letv.com/k?_oid=2242&_rtname=click&_rt=2&_u=ipdianji2323\"],\"completeMonitors\":[],\"coopAdverCode\":\"68\",\"directStrategy\":{\"isAreaNegative\":0,\"putAreaId\":[1156000000],\"putHourPeriodId\":[1,2,5,6,7],\"clickRate\":33.33,\"platform\":\"iphone\"},\"endDate\":\"2016-07-06 23:59:59\",\"frequencyPeriod\":0,\"landingPage\":\"\",\"materialDuration\":0,\"materialURL\":\"\",\"maxFrequency\":0,\"minFrequency\":0,\"startDate\":\"2016-07-05 00:00:00\",\"viewMonitors\":[\"http://ark.letv.com/k?_oid=2242&_rtname=Impression&_rt=1&_u=ipbaoguang2323\"],\"memo\":\"频控2\"}', '1', '2016-07-04 18:00:27', '2016-07-04 18:00:27');
INSERT INTO `b_schedule3rd` (`id`, `user_id`, `token`, `json_data`, `status`, `created_time`, `modified`) VALUES ('101', '0', 'a607ab1b7e863dfff894ada6b2af56db', '{\"adverName\":\"ap+ipad\",\"adverPlan\":{\"2016-07-05\":1},\"clickMonitors\":[\"http://ark.letv.com/k?_oid=2241&_rtname=click&_rt=2&_u=2\"],\"completeMonitors\":[],\"coopAdverCode\":\"67\",\"directStrategy\":{\"isAreaNegative\":0,\"putAreaId\":[1156000000],\"putHourPeriodId\":[1,2,3,4,5,6,7],\"clickRate\":33.33,\"platform\":\"ipad\"},\"endDate\":\"2016-07-05 23:59:59\",\"frequencyPeriod\":0,\"landingPage\":\"\",\"materialDuration\":0,\"materialURL\":\"\",\"maxFrequency\":0,\"minFrequency\":0,\"startDate\":\"2016-07-05 00:00:00\",\"viewMonitors\":[\"http://ark.letv.com/k?_oid=2241&_rtname=Impression&_rt=1&_u=2\"],\"memo\":\"频控1\"}', '1', '2016-07-04 18:35:19', '2016-07-04 18:35:19');
INSERT INTO `b_schedule3rd` (`id`, `user_id`, `token`, `json_data`, `status`, `created_time`, `modified`) VALUES ('102', '0', 'a607ab1b7e863dfff894ada6b2af56db', '{\"adverName\":\"ip+ipad\",\"adverPlan\":{\"2016-07-06\":1,\"2016-07-05\":1},\"clickMonitors\":[\"http://ark.letv.com/k?_oid=2242&_rtname=click&_rt=2&_u=111\"],\"completeMonitors\":[],\"coopAdverCode\":\"68\",\"directStrategy\":{\"isAreaNegative\":0,\"putAreaId\":[1156000000],\"putHourPeriodId\":[1,2,5,6,7],\"clickRate\":33.33,\"platform\":\"iphone\"},\"endDate\":\"2016-07-06 23:59:59\",\"frequencyPeriod\":0,\"landingPage\":\"\",\"materialDuration\":0,\"materialURL\":\"\",\"maxFrequency\":0,\"minFrequency\":0,\"startDate\":\"2016-07-05 00:00:00\",\"viewMonitors\":[\"http://ark.letv.com/k?_oid=2242&_rtname=Impression&_rt=1&_u=111\"],\"memo\":\"频控2\"}', '1', '2016-07-04 18:35:20', '2016-07-04 18:35:20');




DROP TABLE IF EXISTS `b_adrpi3config`;
CREATE TABLE `b_adrpi3config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(250) DEFAULT NULL COMMENT 'adr的主机',
  `var1` varchar(250) DEFAULT NULL COMMENT '名称',
  `var2` varchar(250) DEFAULT NULL COMMENT '电话号码',
  `var3` varchar(250) DEFAULT NULL COMMENT 'ssid',
  `var4` varchar(250) DEFAULT NULL COMMENT '密码',

  `data` longtext  COMMENT 'json stats data',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
  `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `host` (`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='adr-pi信息';


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1023, 1, 'qq/mail/25-45');
INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1023, 2, 'mango/mail/25-45');

