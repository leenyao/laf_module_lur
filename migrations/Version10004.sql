

DROP TABLE IF EXISTS `b_iacip_list`;
CREATE TABLE `b_iacip_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `start` bigint(20) DEFAULT NULL COMMENT 'IP开始段',
  `end` bigint(20) DEFAULT NULL COMMENT 'IP结束段',
  `startip` varchar(100) DEFAULT NULL COMMENT 'IP开始',
  `endip` varchar(100) DEFAULT NULL COMMENT 'IP结束',
  `citycode` bigint(20) DEFAULT '0' COMMENT '城市代码',
  `citylabel` varchar(250) DEFAULT '' COMMENT '城市名称',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  unique key `iprange` (`start`, `end`),
  key `citycode` (`citycode`),
  key `citylabel` (`citylabel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='iac ip列表';


ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `sspid` varchar(200) COMMENT 'ssp 排期id, 目前只有芒果用' AFTER `op_user_id`;

ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `ssp_7_zid` varchar(200) COMMENT 'letv 的广告位id' AFTER `sspid`;
ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `ssp_7_pid` varchar(200) COMMENT 'letv 的平台id' AFTER `sspid`;
ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `ssp_7_dealid` varchar(200) COMMENT 'letv 的dealid' AFTER `sspid`;
ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `ssp_7_oid` varchar(200) COMMENT 'letv 的oid' AFTER `sspid`;


ALTER TABLE `laf_lur`.`b_adcounter` ADD INDEX  USING BTREE (`date`, `code_id`) comment '';


CREATE TABLE `b_sdkip_counter` (
  `ip` varchar(255) NOT NULL DEFAULT '' COMMENT 'ip',
  `platform` varchar(255) NOT NULL DEFAULT '' COMMENT '平台id',
  `data` text COMMENT '内容',
  `city_id` varchar(255) NOT NULL DEFAULT '' COMMENT '城市id',
  `city_label` varchar(255) NOT NULL DEFAULT '' COMMENT '城市名称',
  `md5key` varchar(255) NOT NULL DEFAULT '' COMMENT 'ip与platform的联合id',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `maxindex` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`md5key`),
  KEY `city_id` (`city_id`),
  KEY `city_label` (`city_label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `hunan_cxids` tinyint COMMENT 'hunan ssp cxids' AFTER `sspid`;
ALTER TABLE `laf_lur`.`b_adschedule` CHANGE COLUMN `hunan_cxids` `hunan_cxids` tinytext DEFAULT NULL COMMENT 'hunan ssp cxids';
ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `useiac` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否必须使用与iac一致的地域数据' AFTER `ssp_7_zid`;

ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `rtbdata` longtext AFTER `useiac`;


ALTER TABLE `laf_lur`.`vpn_accounts` ADD COLUMN `platform` varchar(255) DEFAULT 1 COMMENT '1=>ad,2=>uv' AFTER `id`;


ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `sh_vid` longtext COMMENT 'sohu vids' AFTER `rtbdata`;
ALTER TABLE `laf_lur`.`b_adschedule` ADD COLUMN `sh_useapiuuid` longtext COMMENT 'sohu sh_useapiuuid' AFTER `rtbdata`;


INSERT INTO `system_metadata` (`type`, `resid`, `label`) VALUES (1025, 1,'美剧');
