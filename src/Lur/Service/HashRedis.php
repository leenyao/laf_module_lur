<?php

namespace Lur\Service;



class HashRedis
{
    static $aConfig;
    static $oRedis;

    /**
     * 本静态方法用于替换所有redis类操作, 在此处实现redis 配置的选择
     * 
     */
    public static function __callStatic($sFunc , $aVars){


        try{
            /**
             * #######closure function start#######
             */
            $fClosureFunc = function($oRedis) use ($sFunc, $aVars)
            {

                $aNewVars = array();
                $nVarCount = extract($aVars, EXTR_PREFIX_ALL, "p");
                for ($i=0; $i < $nVarCount ; $i++) { 
                    $aNewVars[] = "\$p_".$i;
                }
                $sVarsTmp = join(", ", $aNewVars);
                $sRedisFunction = "\$mReturn = \$oRedis->\$sFunc($sVarsTmp);";

                eval($sRedisFunction);
                return $mReturn;
            };


            $aReturnTmp = null;
            switch ($sFunc) {
                case 'info'://info函数遍历
                    $aConfig = self::getConfig();
                    foreach ($aConfig['configkeys'] as $sRedisConfigKey) {
                        $aReturnTmp[$sRedisConfigKey] = $fClosureFunc(self::getRedis($sRedisConfigKey));
                    }
                    break;
                case 'keys'://info函数遍历
                    $aConfig = self::getConfig();
                    foreach ($aConfig['configkeys'] as $sRedisConfigKey) {
                        $aReturnTmp[$sRedisConfigKey] = $fClosureFunc(self::getRedis($sRedisConfigKey));
                    }
                    break;
                default:


                    $oRedis = self::getObjectByCacheKey($aVars);
                    $aReturnTmp = $fClosureFunc($oRedis);
                    break;
            }

            return $aReturnTmp;
        }catch (\Exception $e){
            throw new \Exception("NO SUCH REDIS METHOD:".$e->getMessage(), 1);
            
        }
        // $oRedis->$sFunc();
    }

    /**
     * #######closure function start#######
     * 通过需要操作的 rediskey
     * 循环所有的参数, 找到 redis的key值, 并返回 根键
     * 
     */
    static function _getBaseCacheKeyByVars($aVars){
        $sReturn = null;
        if (count($aVars)) {
            foreach ($aVars as $sKey) {
                if (
                        preg_match("/^ADRKEY_COOKIE_/", $sKey)
                        || preg_match("/^ARK_/", $sKey)
                        || preg_match("/^ADRKEY_COOKIEDB_/", $sKey)
                    ) {
                    $sKey = str_replace("-","/", $sKey);

                    $aTmp = explode("/", $sKey);
                    $sReturn = $aTmp[0];
                    $sReturn = str_replace("*", "", $sReturn);
                    break;
                }
            }
        }

        return $sReturn;
    }

    // 获取哈希后的redis句柄
    static function getHashedObject($sCacheKey){

        $aConfig = self::getConfig();
        $dbs = $aConfig['configkeys'];

        if (count($dbs)){
            $iIndex = self::md52Int($sCacheKey);
            $dbkey = $dbs[$iIndex%count($dbs)];
            return $dbkey;
        }
        return null;
    }
    // md5转10进制
    static function md52Int($s, $nLenth=10) {
        return substr(base_convert( md5( $s ), 16, 10 ), 0, $nLenth);
    }

    static function getObjectByCacheKey($aVars=[])
    {
        $sRedisConfigKey = self::getHashedObject($aVars[0]);

        // var_dump($aVars, $sRedisConfigKey);
        $oReturn = self::getRedis($sRedisConfigKey);
        return $oReturn;
    }


    static function getRedis($sRedisConfigKey){

        try{
            if (!isset(self::$oRedis)) {
                self::$oRedis = array();
            }
            if (!isset(self::$oRedis[$sRedisConfigKey])) {
                $aConfig = self::getConfig();

                if (!isset($aConfig[$sRedisConfigKey])) {
                    throw new \Exception($sRedisConfigKey);

                }
                self::$oRedis[$sRedisConfigKey] = new \Redis();
                // var_dump($aConfig[$sRedisConfigKey]);
                self::$oRedis[$sRedisConfigKey]->connect($aConfig[$sRedisConfigKey]['host'], $aConfig[$sRedisConfigKey]['port']);
                self::$oRedis[$sRedisConfigKey]->select($aConfig[$sRedisConfigKey]['db']);
            }
            return self::$oRedis[$sRedisConfigKey];

        }catch(\RedisException $e){
            throw $e;
        }


    }


    static function getConfig()
    {
        if (!self::$aConfig) {
            $localconfigs = require "/app/config/autoload/local.php";
            self::$aConfig = $localconfigs['adrhashredis'];
        }

        return self::$aConfig;
    }


    
}

