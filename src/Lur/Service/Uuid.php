<?php
/**
 * 补量类
 */
namespace Lur\Service;



class Uuid 
{
    var $redis;

    public function __construct()
    {

    }

    public function setReids($o){
        $this->redis = $o;

    }
    public function getReids(){
        return $this->redis;
    }

    public function getIpCpart($sIp){
        return $sIp;
        list($p1, $p2, $p3) = explode('.', $sIp);
        return $p1.".".$p2.".".$p3;
    }

    public function getGenPoolKey($sIp='', $sPlatform)
    {
        return LAF_CACHE_UUID_GENPOOL_313.$sPlatform."/".$this->getIpCpart($sIp);
    }

    public function getGenPoolSeyKey($sVersion)
    {
        return $sVersion;
    }

    /**
     * 锁定adr 与 ip关系
     */
    public function lockAdr2Ip($nAdrId, $sIp){
        $ipCpart = $this->getIpCpart($sIp);

        $aAdr2Ip = $this->redis->hGetAll(LAF_CACHE_UUID_ADR2IP_314);
        $aAliveIds = $this->getAliveList();

        // 剔除下线的ADR
        if (count($aAdr2Ip)) {
            foreach ($aAdr2Ip as $id1 => $s1) {
                if (!in_array($id1, $aAliveIds)) { // 剔除掉线的ADR

                    // unset($aAdr2Ip[$id1]);
                    // $this->redis->hDel(LAF_CACHE_UUID_ADR2IP_314, $id1);

                }
            }
        }


        // 之前已经锁定
        if (isset($aAdr2Ip[$nAdrId]) && $aAdr2Ip[$nAdrId]==$ipCpart) return true;

// var_dump($ipCpart);
// var_dump($aAdr2Ip);
// var_dump(in_array($ipCpart, $aAdr2Ip));


        // 被其他ADR锁定
        if (in_array($ipCpart, $aAdr2Ip)) return false;

        // 加锁
        $this->redis->hSet(LAF_CACHE_UUID_ADR2IP_314, $nAdrId, $ipCpart);
        return true;

    }


    /**
     * 所有活着的ADR机器
     */
    public function getAliveList(){
        $aReturn = $this->redis->sMembers(LAF_CACHE_ADRSERVERS_ALIVE_106);
        return $aReturn ? $aReturn : array();
    }


    public function fmtMd5Key($sIp='', $nPlatform='')
    {
        return md5($sIp."/".intval($nPlatform));
    }
    public function loadGenPoolFromTable($sIp='', $nPlatform='')
    {
        $sMd5Key = $this->fmtMd5Key($sIp, $nPlatform);
        $oPdo = \SimpleRestApi2::initPdo('db_slave'); //db_master|db_slave
        $sSql = "select * from b_sdkip_counter where md5key=:md5key";
        

        $oSth = $oPdo->prepare($sSql);
        $oSth->execute(array(
            ":md5key" => $sMd5Key,
        ));
        $aResultTmp = $oSth->fetch(\PDO::FETCH_ASSOC);
        $sData = is_array($aResultTmp)&& count($aResultTmp) ? $aResultTmp['data'] : false;

        $aReturn = [];
        if ($sData) {
            $aData = json_decode($sData, 1);
            if (is_array($aData) && count($aData)) {
                $aReturn = $aData;
                krsort($aReturn);
            }
        }
        return $aReturn;

    }

    public function getGenPool($nAdrId, $sIp){

        $bLock = $this->lockAdr2Ip($nAdrId, $sIp);

        if ($bLock) {
            $aPlatForm = [1,2,3,4,5,6];
            $aReturn = array();
            foreach ($aPlatForm as $sPlatform) {
                $sCacheKey = $this->getGenPoolKey($sIp, $sPlatform);

                $a = [];
                if ($this->redis->exists($sCacheKey)) {
                    $a = $this->redis->hGetAll($sCacheKey);
                    krsort($a);
                    // $aReturn[$sPlatform] = 2;
                }else{
                    // if ($sIp=='180.97.81.112') {
                        $a = $this->loadGenPoolFromTable($sIp, $sPlatform);

                    // }

                }
                if (count($a)) {
                    $aReturn[$sPlatform] = current($a);
                }

            }
            return $aReturn;
        }else{
            return false;
        }

    }

    // 更新id位移池子, 只更新大的
    public function updGenPool($nAdrId, $sData){
        $aList = json_decode($sData, 1);
        if (is_array($aList) && count($aList)) {
            foreach ($aList as $s) {
                list($sIp, $sPlatform, $sVersion, $nCount) = explode('/', $s);
                $sCacheKey = $this->getGenPoolKey($sIp, $sPlatform);
                $sVersion = $this->getGenPoolSeyKey($sVersion);


                $nCurrentCount = 0;
                if ($this->redis->exists($sCacheKey)) {
                    $nCurrentCount = $this->redis->hGet($sCacheKey, $sVersion);
                    // \YcheukfCommon\Lib\Functions::onlinedebug('updGenPool-cache', $sCacheKey);
                }else{
                    $aFromDb = $this->loadGenPoolFromTable($sIp, $sPlatform);
                    // \YcheukfCommon\Lib\Functions::onlinedebug('updGenPool-db', $aFromDb);

                    if (count($aFromDb) ) {
                        foreach ($aFromDb as $k1 => $v1) {
                            $this->redis->hSet($sCacheKey, $k1, $v1);
                        }
                        $this->redis->setTimeout($sCacheKey,5*3600);
                        if (isset($aFromDb[$sVersion])) {
                            $nCurrentCount = $aFromDb[$sVersion];
                        }
                    }
                    // \YcheukfCommon\Lib\Functions::onlinedebug('updGenPool-cc', $nCount." vs ".$nCurrentCount);

                }


                // 只有比当前池子大的位移才会被更新, 防止覆盖
                if (intval($nCount) > intval($nCurrentCount)) {
                    $this->redis->hSet($sCacheKey, $sVersion, $nCount);
                    $this->redis->setTimeout($sCacheKey,5*3600);
                }
            }
        }
        return true;
        
    }
}

