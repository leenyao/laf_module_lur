<?php

namespace Lur\Service;



use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Across implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {

    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function getTaglist()
    {
        $aConfig = $this->serviceManager->get('config');
        $aReturn = [];
        foreach ($aConfig['adr_across']['map']['taglist'] as $id => $row) {
            $row['adtype'] = $aConfig['adr_across']['map']['adtype'][$row['at']];

            $row['p'] = isset($row['p']) && !empty($row['p']) ? $row['p'] : '';
            $p_label = isset($row['p']) && !empty($row['p']) ? $aConfig['adr_across']['map']['labelDick'][$row['p']] : '全平台';

            $row['p_label'] = $p_label;
            $aReturn[$id] = $row;
        }
        return $aReturn;
    }


// {"tagid":51939, "pfid":2, "adtypeid":3103, "size":"750*650"}
    public function getRtbDataByTagId($tagid)
    {
        $aConfig = $this->serviceManager->get('config');

        $aTag = $aConfig['adr_across']['map']['taglist'][$tagid];
        $aAdType = $aConfig['adr_across']['map']['adtype'][$row['at']];


        $return = [
            "p_cpm" => $aTag['p_cpm'],
            "p_cpc" => $aTag['p_cpc'],
            "tagid" => $tagid,
            "adtypeid" => $aTag['at'],
            "size" => $aTag['s'],
            "pfid" => isset($aTag['p']) ? $aTag['p'] : '',
        ];
        return $return;
    }

}

