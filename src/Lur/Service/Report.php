<?php

namespace Lur\Service;



use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Report extends Common
{

    public function clickPercentReportRealTime($nSspType, $sToday){
        $sToday = date("Ymd", strtotime($sToday));
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);

        $sCacheKey = LAF_CACHE_COUNTER_SSP_CLICKRATE_410.$sToday."/".$nSspType;
        $aClickRate = $oReids->hGetAll($sCacheKey);
        $aReturn = $aClickRate ? $aClickRate : [];
        ksort($aReturn);
        return $aReturn;
    }

    public function clickPercentReport($nSspType, $sDate1, $eDate1, $bCache=false){
        $aReturn = [];

        $sCacheKey = $nSspType."/".$sDate1."/".$eDate1;

        if ($bCache==true) {
            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->serviceManager, LAF_MD5KV_SSP_CLICKRATE_109, $sCacheKey);
            if ($aMd5KeyData && count($aMd5KeyData)) {
                return json_decode($aMd5KeyData['data'], 1);
            }
        }

        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
        $sSql = "select sum(count) total,code_type,date,code_cid from b_adcounter_ssp where code_type in ('imp', 'c-t') and ssp_type='hunan' and date between :sdate and :edate  group by code_cid, date, code_type";

        $sth = $oPdo->prepare($sSql);

        $sth->execute(array(
            ":sdate" => $sDate1,
            ":edate" => $eDate1,
        ));
        $oPDOStatement = $sth->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($oPDOStatement as $aRow) {
            $sKey = $aRow['date'].$aRow['code_cid'];
            if ($aRow['code_type'] == 'imp') {
                $aRow['imp_count'] = $aRow['total'];
            }elseif ($aRow['code_type'] == 'c-t') {
                $aRow['ct_count'] = $aRow['total'];
            }
            if (!isset($aReturn[$sKey])) {
                $aReturn[$sKey] = [];
            }
            $aReturn[$sKey] = array_merge($aReturn[$sKey], $aRow);

        }
        krsort($aReturn);
            // var_dump($aReturn)`;
        
        \YcheukfCommon\Lib\Functions::saveLafCacheData($this->serviceManager, LAF_MD5KV_SSP_CLICKRATE_109, $sCacheKey, json_encode($aReturn));

        return $aReturn;
    }
    function getSspHunanDataByDateCode($sDate1=null, $eDate1=null, $nType=1, $sspType='hunan')
    {
        $sDate1 = date("Y-m-d", strtotime($sDate1));
        $eDate1 = date("Y-m-d", strtotime($eDate1));
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
        $sspType = (strtolower($sspType)=='shanghai') ? 'sohu':$sspType;

        $oAuthService = ($this->serviceManager->get('zfcuser_auth_service'));
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($this->serviceManager, $oAuthService->getIdentity());


        if ((PHP_SAPI === 'cli') || in_array("superadmin", $aRoles)) {
            $sGrantUserWhere = '';
        }else{
            $nOpUserId = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId();
            $sGrantUserWhere = ' and code_lid in (select id from b_adschedule where op_user_id='.$nOpUserId.')';
        }


        switch ($nType) {
            case 3: //day, imp, c

                $sSql = 'select t1.date,t1.impcount, t2.ccount from 
                    (select date,sum(count) as impcount from `laf_lur`.`b_adcounter_ssp` where code_type="imp" and ssp_type=:ssp_type and date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date) t1 left join (select date,sum(count) as ccount from `laf_lur`.`b_adcounter_ssp` where ssp_type=:ssp_type and code_type="c-t" and  date between (:sdate) and (:edate)   '.$sGrantUserWhere.' group by date ) t2 on  t1.date=t2.date  group by t1.date  order by t1.date desc';
                break;
            case 8: //day, sspid, imp, c

                $sSql = 'select t3.date,t4.hunan_cxids,t4.sspid,t4.id,t4.code_label,t3.impcount,t3.ccount  from (select t1.date,t1.code_lid,t1.impcount, t2.ccount from (select date,code_lid,sum(count) as impcount from `laf_lur`.`b_adcounter_ssp` where code_type="imp" and ssp_type=:ssp_type and  date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date,code_lid) t1 left join (select date,code_lid,sum(count) as ccount from `laf_lur`.`b_adcounter_ssp` where ssp_type=:ssp_type and code_type="c-t" and  date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date,code_lid ) t2 on  t1.date=t2.date and t1.code_lid=t2.code_lid  group by t1.date,t1.code_lid) t3 left join laf_lur.b_adschedule t4 on t3.code_lid=t4.id  group by t3.date,t4.hunan_cxids,t4.sspid,t4.id order by t3.date,t4.hunan_cxids,t4.sspid,t4.id';

                break;
            case 9: //day, sspid, imp, c

                $sSql = 'select t3.date,t4.hunan_cxids,t4.sspid,sum(t3.impcount) as impcount,sum(t3.ccount) as ccount  from (select t1.date,t1.code_lid,t1.impcount, t2.ccount from (select date,code_lid,sum(count) as impcount from `laf_lur`.`b_adcounter_ssp` where code_type="imp" and ssp_type=:ssp_type and date between (:sdate) and (:edate)  '.$sGrantUserWhere.' group by date,code_lid) t1 left join (select date,code_lid,sum(count) as ccount from `laf_lur`.`b_adcounter_ssp` where ssp_type=:ssp_type and code_type="c-t" and  date between (:sdate) and (:edate)  '.$sGrantUserWhere.' group by date,code_lid ) t2 on  t1.date=t2.date and t1.code_lid=t2.code_lid  group by t1.date,t1.code_lid) t3 left join laf_lur.b_adschedule t4 on t3.code_lid=t4.id group by t3.date,t4.hunan_cxids,t4.sspid  order by t3.date,t4.hunan_cxids,t4.sspid';

                break;
            case 7: // day, imp, c with ids


                // pc
                $ids = '69505, 72525, 72526, 72527, 67245, 72890, 72891, 72892, 72893, 72894, 72895, 72896, 72897, 72898, 72899, 72900, 72901, 72902, 72903, 72904, 72905';
                
                // ios, android
                $ids = '67247, 69506, 72530, 72531, 72532, 72906, 72907, 72908, 72909, 72910, 72911, 72912, 72913, 72914, 72915, 72916, 72918, 72919, 72920, 72921, 72923, 72924, 72925, 72926, 72927, 72928, 72931, 72932, 72933, 72935, 72936, 72937, 72938, 72941, 72942, 72943, 72944, 72945, 72947, 72948, 72949, 72950, 72951';
                
                // pad
                $ids = '67249,69507';

                
                $sSql = 'select t1.date,t1.impcount, t2.ccount from (select date,sum(count) as impcount from `laf_lur`.`b_adcounter_ssp` where code_type="imp" and ssp_type=:ssp_type  and  date between (:sdate) and (:edate) and code_lid in ('.$ids.')   group by date) t1 left join (select date,sum(count) as ccount from `laf_lur`.`b_adcounter_ssp` where ssp_type=:ssp_type and code_type="c-t" and  date between (:sdate) and (:edate) and code_lid in ('.$ids.')   group by date ) t2 on  t1.date=t2.date   group by t1.date  order by t1.date desc';
                break;
            case 4: // day, codeid, imp, c
            $sSql = 'select t1.date,t1.code_id,t1.impcount, t2.ccount from (select code_id,date,sum(count) as impcount from `laf_lur`.`b_adcounter_ssp` where code_type="imp" and ssp_type=:ssp_type  and date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date,code_id) t1 left join (select date,code_id,sum(count) as ccount from `laf_lur`.`b_adcounter_ssp` where ssp_type=:ssp_type and code_type="c-t" and  date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date,code_id ) t2 on  t1.date=t2.date and  t1.code_id=t2.code_id  group by t1.date,t1.code_id  order by t1.date desc, t1.code_id  asc';
              break;
            case 2: // date, city, imp, c

                $sSql = 'select t1.date,t1.city_id,t1.impcount, t2.ccount from (select city_id,date,sum(count) as impcount from `laf_lur`.`b_adcounter_ssp` where code_type="imp" and ssp_type=:ssp_type  and date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date,city_id) t1 left join (select date,city_id,sum(count) as ccount from `laf_lur`.`b_adcounter_ssp` where ssp_type=:ssp_type and  code_type="c-t" and  date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date,city_id ) t2 on  t1.date=t2.date and  t1.city_id=t2.city_id  group by t1.date,t1.city_id  order by t1.date desc,t1.city_id asc';
                break;
            
            default: // date, code, city, imp, c
              $sSql = 'select t1.date,t1.code_id,t1.city_id,t1.impcount, t2.ccount from (select code_id,city_id,date,sum(count) as impcount from `laf_lur`.`b_adcounter_ssp` where code_type="imp" and ssp_type=:ssp_type  and date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date,code_id,city_id) t1 left join (select date,code_id,city_id,sum(count) as ccount from `laf_lur`.`b_adcounter_ssp` where ssp_type=:ssp_type and code_type="c-t" and  date between (:sdate) and (:edate) '.$sGrantUserWhere.' group by date,code_id,city_id ) t2 on  t1.date=t2.date and  t1.code_id=t2.code_id and  t1.city_id=t2.city_id  group by t1.date,t1.code_id  order by t1.date desc, t1.code_id asc,t1.city_id asc';
                break;
        }
        $sth = $oPdo->prepare($sSql);


        $sth->execute(array(
            ":sdate" => $sDate1,
            ":edate" => $eDate1,
            ":ssp_type" => $sspType,
        ));

        // var_dump($sSql);
        // var_dump($sspType);
        $oPDOStatement = $sth->fetchAll(\PDO::FETCH_ASSOC);
// print_r($oPDOStatement);
        // $aReturn = array();
        // foreach($oPDOStatement as $result) {

        // }
        return $oPDOStatement;
    }

    

    function getSspHunanData($sDate1=null, $eDate1=null)
    {

        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
        if (is_null($sDate1)) {
            $sDate1 = date("Y-m-d", strtotime("-7 day"));
        }
        if (is_null($eDate1)) {
            $eDate1 = date("Y-m-d");
        }

        $debug_t1 = microtime(true);

        $aCacheRepoData = $this->serviceManager->get('\Lur\Service\Common')->getSspReportDataFromCache($sDate1, $eDate1);

        $debug_t2 = microtime(true);
        // print_r($aDateCode2Count);
        echo 'exceute: '.round($debug_t2-$debug_t1,3).'s';
        exit;
        $aReturn = $aTmp2 = $aTmp3 = $aTmp4 = $aTmp5 = $aDateCode2Count = array();
        $aTmp = array();
        // $sSql = 'select t1.date,t1.adrserver_id, t1.impcount, t2.ccount from (select adrserver_id,date,sum(count) as impcount from `laf_lur`.`b_adcounter_ssp` where code_type="imp" and date between (:sdate) and (:edate) group by date,adrserver_id) t1 left join (select date,adrserver_id, sum(count) as ccount from `laf_lur`.`b_adcounter_ssp` where code_type="c" and  date between (:sdate) and (:edate) group by date,adrserver_id ) t2 on  t1.date=t2.date and t1.adrserver_id=t2.adrserver_id group by t1.date,t1.adrserver_id  order by t1.date';
        // $sth = $oPdo->prepare($sSql);

        // $sth->execute(array(
        //     ":sdate" => $sDate1,
        //     ":edate" => $eDate1,
        // ));
        // $oPDOStatement = $sth->fetchAll(\PDO::FETCH_ASSOC);

        foreach($aCacheRepoData as  $sDateTmp=>$sRowTmp2) {
            foreach($sRowTmp2 as  $sKey2=>$sRowTmp3) {
                
                $aRowTmp3 = json_decode($sRowTmp3, 1);
                list($result['adrserver_id']) = explode("|", $sKey2);


                // var_dump($result);
                //分服务器
                $sServerLable = $aRowTmp3['sl'];
                $result['total'] = intval($aRowTmp3['c']);
                $result['date'] = $sDateTmp;


                if (!isset($aTmp[$sServerLable])) {
                    $aTmp[$sServerLable] = array();
                }
                if (!isset($aTmp[$sServerLable][$result['date']])) {
                    $aTmp[$sServerLable][$result['date']] = 0;
                }
                $aTmp[$sServerLable][$result['date']] += $result['total'];


                $aTmp2[$sServerLable] = 0;
                $aTmp3[$result['date']] = 0;


            }
        }


        // var_dump($aTmp);
        // var_dump($aTmp2);
        // var_dump($aTmp3);

        // $aReturn['legend'] = array_merge(array("总量")  ,   array_keys($aTmp2));



        /**
         * #######closure function start#######
         */
        $fClosureFunc = function($axAxisData, $aLegned, $axAxisData2) 
        {
            $aReturn2 = array();
            $aReturn2['xAxisData'] = array_keys($axAxisData);
            sort($aReturn2['xAxisData']);
            $aReturn2['legend'] = array_keys($aLegned);
            sort($aReturn2['legend']);
            foreach ($aReturn2['legend'] as $sTmp1) {
                foreach ($aReturn2['xAxisData'] as $sDate) {
                    $axAxisData2[$sTmp1][$sDate] = isset($axAxisData2[$sTmp1][$sDate]) ? $axAxisData2[$sTmp1][$sDate] : 0;
                    ksort($axAxisData2[$sTmp1]);
                }
            }

            $aReturn2['series'] = array();


            foreach ($aReturn2['legend'] as $sTmp2) {

                $aReturn2['series'][] = array(
                    "name" => $sTmp2,
                    "type" => "bar",
                    "stack" => "one",
                    "data" => array_values($axAxisData2[$sTmp2]),
                    'itemStyle' => array(),
                    'label' => array(
                        "normal" => array(
                            // 'show' => true,
                            'position' => "insideRight",
                    )),
                );
            }
                // var_dump($aReturn2['xAxisData']);
                // var_dump($aReturn2);
                // var_dump($aTmp);

            return $aReturn2;
        };

        $aReturn = array();
        $aReturn = $fClosureFunc($aTmp3, $aTmp2, $aTmp);

        unset($fClosureFunc);
        // var_dump($aReturn);


        return $aReturn;

    }
}