<?php
namespace Lur\Service;



use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class UvcodeSpider implements ServiceManagerAwareInterface
{
    protected $serviceManager;
    var $debug=true;
    var $thirdScheduleRedis=null;
    var $analysites = array(
        'baidu',
        'ga'
    );
    var $document_config=array();
    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function getUvcodeByUrl($url='')
    {
        $this->document_config['url'] = $url;
        $return = array();
        if ($this->debug) {
            $urlcontent = $this->_democontent();
        }else{
            $urlcontent = file_get_contents($url);
        }
        $this->fetchDocumentConfig($urlcontent);
        foreach ($this->analysites as $site) {
            $func = '_fetch_'.$site;
            $tmp = $this->$func($urlcontent);
            if ($tmp) {
                $return[$site] = $tmp;
            }
        }
        return $return;
    }

    public function getDocumentConfig(){
        return $this->document_config;
    }

    public function fetchDocumentConfig($urlcontent){

        // $urlcontent = \YcheukfCommon\Lib\Functions::iconv2SpecialCharset($urlcontent, );
            
        var_dump('<title>1上海棱耀信息技术2</title>');
        preg_match_all('/<title>(.*)<\/title>/is', $urlcontent, $matches);
        if (is_array($matches[1]) && isset($matches[1][0])) {
            var_dump(\YcheukfCommon\Lib\Functions::iconv2Utf8($matches[1][0]));
            // $return = $matches[1][0];
            $this->document_config['title'] = $matches[1][0];
        }
    }


    public function _fetch_baidu($urlcontent)
    {
        $return = null;
        preg_match_all('/\s+(var _hmt =.*\(function\(\).*hm\.baidu\.com\/hm\.js\?.*\(\)\;\s+)+<\/script>/is', $urlcontent, $matches);
        if (is_array($matches[1]) && isset($matches[1][0])) {
            // var_dump($matches);
            $return = $matches[1][0];
        }
        // $return = $urlcontent;
        return $return;
    }
    
    public function _fetch_ga($urlcontent)
    {
        $return = null;
        preg_match_all('/\s+(\(function\(i,s,o,g,r,a,m\).*google-analytics\.com\/analytics\.js.*\'send\', \'pageview\'\);\s+)+/is', $urlcontent, $matches);
        if (is_array($matches[1]) && isset($matches[1][0])) {
            // var_dump($matches);
            $return = $matches[1][0];
        }
        // $return = $urlcontent;
        return $return;
    }

    public function _democontent()
    {
        return <<<__EOF

        <!DOCTYPE html>
        <html lang="en">

        <head>

            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="互联网各行业应用及网站的开发及营销，提供业务解决方案的同时给予优质的服务。">
            <meta name="keywords" content="技术，广告监测，APP，微信，网站建设，网络布线，棱耀，棱耀信息，棱耀科技" />
            <meta name="author" content="weier">

            <title>上海棱耀信息技术</title>

            <!-- Bootstrap Core CSS -->
            <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

            <!-- Custom Fonts -->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

            <!-- Plugin CSS -->
            <link rel="stylesheet" href="css/animate.min.css" type="text/css">

            <!-- Custom CSS -->
            <link rel="stylesheet" href="css/creative.css" type="text/css">

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
        <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "//hm.baidu.com/hm.js?f3462db4e8fcfa5d2a5cbcf4d0e02d1f";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        </script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-96182334-1', 'auto');
          ga('send', 'pageview');

        </script>
        </head>

        <body id="page-top">

            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand page-scroll" href="#page-top">棱耀科技</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a class="page-scroll" href="#about">关于我们</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#services">产品服务</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#portfolio">案例展示</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#contact">联系我们</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>

            <header>
                <div class="header-content">
                    <div class="header-content-inner">
                        <h1>互联网技术服务专家</h1>
                        <hr>
                        <p>我们致力于互联网各行业应用及网站的开发及营销，提供业务解决方案的同时给予优质的服务。</p>
                        <a href="#about" class="btn btn-primary btn-xl page-scroll">了解更多</a>
                    </div>
                </div>
            </header>

            <section class="bg-primary" id="about">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 text-center">
                            <h2 class="section-heading">您的需求！我的服务！</h2>
                            <hr class="light">
                            <p class="text-faded">我们可为您提供互联网应用的一切技术解决方案，包括：网站建设，O2O平台，微信营销，广告投放，广告监测，网站分析以及机房搭建等一系列的技术及服务。</p>
                            <a href="#" class="btn btn-default btn-xl">联系我们</a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="services">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading">产品服务</h2>
                            <hr class="primary">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-diamond wow bounceIn text-primary"></i>
                                <h3>高效建站</h3>
                                <p class="text-muted">跨屏建站，PC＋Mobile＋WeChat，一站式平台开发。</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-paper-plane wow bounceIn text-primary" data-wow-delay=".1s"></i>
                                <h3>监测分析</h3>
                                <p class="text-muted">互联网广告第三方监测及分析，提供跨媒体跨终端广告一站式监测分析。</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary" data-wow-delay=".2s"></i>
                                <h3>网站优化</h3>
                                <p class="text-muted">流量来源细致解读，受众用户精准分析，页面属性完美剖析。</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-heart wow bounceIn text-primary" data-wow-delay=".3s"></i>
                                <h3>网络搭建</h3>
                                <p class="text-muted">PDS布线系统，统一布线设计、安装施工和集中维护。</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="no-padding" id="portfolio">
                <div class="container-fluid">
                    <div class="row no-gutter">
                        <div class="col-lg-4 col-sm-6">
                            <a href="#" class="portfolio-box">
                                <img src="img/portfolio/1.jpg" class="img-responsive" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-category text-faded">
                                            案例
                                        </div>
                                        <div class="project-name">
                                            母婴O2O
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="#" class="portfolio-box">
                                <img src="img/portfolio/2.jpg" class="img-responsive" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-category text-faded">
                                            案例
                                        </div>
                                        <div class="project-name">
                                            本地化餐饮外卖平台
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="#" class="portfolio-box">
                                <img src="img/portfolio/3.jpg" class="img-responsive" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-category text-faded">
                                            案例
                                        </div>
                                        <div class="project-name">
                                            摄影美化APP
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="#" class="portfolio-box">
                                <img src="img/portfolio/4.jpg" class="img-responsive" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-category text-faded">
                                            案例
                                        </div>
                                        <div class="project-name">
                                            传统文化电商平台
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="#" class="portfolio-box">
                                <img src="img/portfolio/5.jpg" class="img-responsive" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-category text-faded">
                                            案例
                                        </div>
                                        <div class="project-name">
                                            活动MiniSite
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <a href="#" class="portfolio-box">
                                <img src="img/portfolio/6.jpg" class="img-responsive" alt="">
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-category text-faded">
                                            案例
                                        </div>
                                        <div class="project-name">
                                            办公自动化工具
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            <aside class="bg-dark">
                <div class="container text-center">
                    <div class="call-to-action">
                        <h2>以人为本，技术驱动</h2>
                        <a href="#" class="btn btn-default btn-xl wow tada">联系我们</a>
                    </div>
                </div>
            </aside>

            <section id="contact">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 text-center">
                            <h2 class="section-heading">联系我们</h2>
                            <hr class="primary">
                            <p>上海棱耀信息技术有限公司（以下简称棱耀），是互联网技术服务供应商，棱耀以软件开发与互联网运营为主营业务，总部位于上海，创立于2015年。公司致力于为客户提供互联网应用服务、企业应用开发与维护、响应式跨屏网站服务、互联网营销服务、网络布线设计管理、软件开发等的一站式服务，满足客户对技术支持的需求，同时协助客户的业务发展，帮助客户提高管理效率及营销效果。公司立志成为顶尖应用软件和互联网服务提供商。</p>
                        </div>
                        <div class="col-lg-4 col-lg-offset-2 text-center">
                            <i class="fa fa-qq fa-3x wow bounceIn"></i>
                            <p>3295485860</p>
                        </div>
                        <div class="col-lg-4 text-center">
                            <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                        </div>
                    </div>
                </div>
            </section>

            <!-- jQuery -->
            <script src="js/jquery.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="js/bootstrap.min.js"></script>

            <!-- Plugin JavaScript -->
            <script src="js/jquery.easing.min.js"></script>
            <script src="js/jquery.fittext.js"></script>
            <script src="js/wow.min.js"></script>

            <!-- Custom Theme JavaScript -->
            <script src="js/creative.js"></script>

        </body>

        </html>

__EOF;
    }
}

