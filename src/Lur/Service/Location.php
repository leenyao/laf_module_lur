<?php

namespace Lur\Service;



use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Location implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {

    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }


    // 获取平台中的所有城市列表, 并格式化他们的名字
    function getDbCities(){

        $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager);
        // select * from `laf_lur`.`location_city`
        $aCities = array();
        $aProvinceId2Lable = array();
        $aCitieName2ProvinceId = array();

        $sSql = "select id,name from `laf_lur`.`location_province`";
        $oQuery = $oPdo->query($sSql);
        while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){
            $aResult['name'] = preg_replace("/市$/i", "", $aResult['name']);
            $aResult['name'] = preg_replace("/省$/i", "", $aResult['name']);
            $aResult['name'] = preg_replace("/自治区$/i", "", $aResult['name']);
            $aResult['name'] = preg_replace("/壮族/i", "", $aResult['name']);
            $aResult['name'] = preg_replace("/回族/i", "", $aResult['name']);
            $aResult['name'] = preg_replace("/维吾尔/i", "", $aResult['name']);
            $aResult['name'] = preg_replace("/内蒙古/i", "内蒙", $aResult['name']);


            $aResult['name'] = preg_replace("/混播/i", "全国混拨", $aResult['name']);
            $aResult['name'] = preg_replace("/混拨/i", "全国混拨", $aResult['name']);
            $aResult['name'] = preg_replace("/全国/i", "全国混拨", $aResult['name']);
            $aResult['name'] = preg_replace("/混合/i", "全国混拨", $aResult['name']);
            

            $aProvinceId2Lable[$aResult['id']] = $aResult['name'];
        }

        // $sSql = "select name,province_id from `laf_lur`.`location_district`";
        // $oQuery = $oPdo->query($sSql);
        // while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){

        //     $sCityName = $aResult['name'];
        //     $sCityName = trim($sCityName);
        //     if (preg_match("/^.*市$/i", $sCityName)) {
        //         $sCityName = preg_replace("/^(.*)市$/i", '\1', $sCityName);
        //     }elseif(preg_match("/^.*县$/i", $sCityName)){
        //         $sCityName = preg_replace("/^(.*)县$/i", '\1', $sCityName);
        //     }
        //     $sCityName = trim($sCityName);
        //     $aCities[] = $sCityName;
        //     $aCitieName2ProvinceId[$sCityName] = $aResult['province_id'];
        //  }
         $aCities[] = '全国混拨';

         $sSql = "select name,province_id from `laf_lur`.`location_city` order by province_id, city_index asc";
         $oQuery = $oPdo->query($sSql);
         while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){

            $sCityName = $aResult['name'];
            $sCityName = trim($sCityName);
            if (preg_match("/^.*市$/i", $sCityName)) {
                $sCityName = preg_replace("/^(.*)市$/i", '\1', $sCityName);
            }elseif(preg_match("/^.*县$/i", $sCityName)){
                $sCityName = preg_replace("/^(.*)县$/i", '\1', $sCityName);
            }
            $sCityName = trim($sCityName);
            // if (preg_match("/忻州/i", $sCityName)) {
            //  var_dump($sCityName, $aResult['province_id']);
            //  # code...
            // }
             $aCities[] = $sCityName;
            $aCitieName2ProvinceId[$sCityName] = $aResult['province_id'];
          }
            /**
             * #######closure function start#######
             */
            $fUpdCitySort = function($aCities=null) 
            {
                $aReturn = array();
                foreach ($aCities as $k=>$sCity) {
                    if (in_array($sCity, ['马鞍山', '鞍山'])) {
                        unset($aCities[$k]);
                    }
                }
                $aReturn = array_merge(['马鞍山', '鞍山'], $aCities);
                return $aReturn;
            };
            $aCities = $fUpdCitySort($aCities);
            //unset($fUpdCitySort);
        return $aCities;
    }

}

