<?php

namespace Lur\Service;



use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Adrserver implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {

    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    // 为truck 计算每小时上报的数
    public function incTruckSendHourCounter($nAdrServerId, $sCurrentDate, $nCount)
    {
        
        $sCounterStr = LAF_CACHE_ADRTRUCK_SENDCOUNTER_318.$nAdrServerId."/".$sCurrentDate;
        $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->hIncrBy($sCounterStr, date("H"), $nCount);
    }

    // 为truck 计算每小时上报的数
    public function getTruckSendCounterInfo($nAdrServerId, $sCurrentDate)
    {
        $sCounterStr = LAF_CACHE_ADRTRUCK_SENDCOUNTER_318.$nAdrServerId."/".$sCurrentDate;
        return $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->hGetAll($sCounterStr);
    }

    
}

