<?php

namespace Lur\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class AdminContent extends \Application\Service\AdminContent
{


    public function preSave($aForm, $sSource, $nCid, $nId){
        $aForm = parent::preSave($aForm, $sSource, $nCid, $nId);
//        $sCurrentTime = date("Y-m-d H:i:s");
       switch($sSource){
           case 'adcustomer'://若为用户, 则先增加 oauthuser
                if (is_null($nId)) {
                    $aForm['user_id'] = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId();
                }
           break;
           case 'adschedule'://若为用户, 则先增加 oauthuser
                $pslManager = new \Pdp\PublicSuffixListManager();
                $parser = new \Pdp\Parser($pslManager->getList());
                // $host = 'http://www.admaster.com.cn/?adrid=0]';
                $url = $parser->parseUrl($aForm['code']);
                $aForm['cookie_domain'] = \YcheukfCommon\Lib\Functions::replaceAdrTag($url->host->registerableDomain);

                $aForm['schedule_md5'] = md5($aForm['schedule']);

                if (is_null($nId) && $this->serviceManager->has('zfcuser_auth_service')) {
                   $aForm['op_user_id'] = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId();
                }


           break;
       }
//                if(!empty($aForm['username'])){
//                    $aUserData = is_null($nId) ? array() : \Application\Model\Common::getResourceById($this->serviceManager, $sSource, $nId);
//                    $aTmpRecord = array(
//                        'username' => $aForm['username'],
//    //                    'password' => md5($aForm['password']),
//                        'from_type' => 1,
//                        'email' => $aForm['email'],
//                        'phone' => $aForm['phone'],
//                        'modified' => $sCurrentTime,
//                    );
//                    if(isset($aForm['password']) && !empty($aForm['password']))
//                        $aTmpRecord['password'] = md5($aForm['password']);
//                    if(isset($aUserData['user_id']) && !empty($aUserData['user_id']))
//                        $aTmpRecord['id'] = $aUserData['user_id'];
//    //                var_dump($aTmpRecord);
//                    $sOauthUserId = \YcheukfCommon\Lib\Functions::saveResource($this->serviceManager, 'oauthuser', array($aTmpRecord));
//    //                var_dump($sOauthUserId);
//                    $aForm['id'] = $aForm['user_id'] = $sOauthUserId;
//                    if(!empty($aForm['password']))
//                        $aForm['password'] = md5($aForm['password']);
//                    else
//                        unset($aForm['password']);
//                    
//                    if(is_null($nId)){
//                        $aForm['user_registered'] = $sCurrentTime;
//                    }
//                }
//            break;
//            case 'metadata'://若为用户的编辑
//                $sMaxId = \Application\Model\Common::getMetadataMaxResId($this->serviceManager, $nCid);
//                if(is_null($nId)){
//                    $aForm['resid'] = $sMaxId + 1;
//                    $aForm['type'] = $nCid;
//                }
//            break;
//            case 'userbander'://绑定用户
//                if(isset($aForm['user_id']) && isset($aForm['bander_user_id'])){
//                    $aTmpUserId = json_decode($aForm['user_id'], 1);
//                    $aTmpBanderUserId = json_decode($aForm['bander_user_id'], 1);
//                    $aForm['user_id'] = $aTmpUserId[0]['value'];
//                    $aForm['bander_user_id'] = $aTmpBanderUserId[0]['value'];
//
//
//                    $aParams = array(
//                        'op'=> 'delete',
//                        'resource'=> $sSource,
//                        'resource_type'=> 'common',
//                        'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
//                        'params' => array(
//                            'where' => array('user_id'=>$aForm['user_id'], 'bander_user_id'=>$aForm['bander_user_id']),
//                        ),
//                    );
//                    $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
//                }
//            break;
//            case 'help':
//                $tmp_pic = array();
//                if(!empty($aForm['picture']))
//                    $tmp_pic[] = $aForm['picture'];
//                if(!empty($aForm['picture_1']))
//                    $tmp_pic[] = $aForm['picture_1'];
//                if(!empty($aForm['picture_2']))
//                    $tmp_pic[] = $aForm['picture_2'];
//                if(!empty($tmp_pic))
//                    $aForm['picture'] = implode(',', $tmp_pic);
//            break;
//        }
        return $aForm;
    }
   public function afterSave($aForm, $sSource, $sSaveId, $nCid=null, $nFid=null){
        parent::afterSave($aForm, $sSource, $sSaveId, $nCid, $nFid);
        switch($sSource){
            case "metadata":
                //更新缓存ip配置
                if (is_null($nCid)) {
                    $aMetadataTmp = \Application\Model\Common::getResourceById($this->serviceManager, $sSource, $sSaveId);
                    $nCid = intval($aMetadataTmp['type']);
                }
                if (in_array($nCid, array(1007, 1011))) {
                    $this->serviceManager->get('\Lur\Service\Common')->updateCache(LAF_CACHE_PROXYCOMMONCONFIG);
                }
            break;
            case "addata":
                $aData = is_null($sSaveId) ? array() : \Application\Model\Common::getResourceById($this->serviceManager, $sSource, $sSaveId);

                $aConfig = $this->serviceManager->get('Config');
                $sMemo = "";
                $aCsvData = array();
                $bImport = true;
                if(file_exists(BASE_INDEX_PATH.$aData['datapath'])){
                    $sReportTable = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, 1005, $aData['m1005_id']);
                    $fHandle = fopen(BASE_INDEX_PATH.$aData['datapath'], 'r');
                    $i=0;
                    $aUser2Id = $aUserCheck = $aDate = array();
                    while ($data = fgetcsv($fHandle)) { //每次读取CSV里面的一行内容
                        if($i++==0)continue;
                        $data[0] = isset($data[0]) ? trim($data[0]) : "";
                        $data[1] = isset($data[1]) ? trim($data[1]) : "";
                        $data[2] = isset($data[2]) ? trim($data[2]) : "";
                        $data[3] = isset($data[3]) ? trim($data[3]) : "";
                        $data[4] = isset($data[4]) ? trim($data[4]) : "";
                        $data[5] = isset($data[5]) ? trim($data[5]) : "";
                        $data[6] = isset($data[6]) ? trim($data[6]) : "";
                        $data[7] = isset($data[7]) ? trim($data[7]) : "";
                        $data[8] = isset($data[8]) ? trim($data[8]) : "";
                        $data[9] = isset($data[9]) ? trim($data[9]) : "";
                        $sUserName = $data[$aConfig['report_field_mapper'][$sReportTable]['user_id']];
                        if(!isset($aUserCheck[$sUserName])){
                            $aUserCheck[$sUserName] = 1;
                            $aUserData = \Application\Model\Common::getResourceById($this->serviceManager, 'user', $sUserName, 'username');
                            if(!count($aUserData)){
                                $bImport = false;
                                $sMemo .= "导入失败. 没有该用户数据:".$sUserName;
                                break;
                            }else{
                                $aUser2Id[$sUserName] = $aUserData['id'];
                            }
                        }
                        $data[$aConfig['report_field_mapper'][$sReportTable]['user_id']] = $aUser2Id[$sUserName];
                        $aDate[$aUser2Id[$sUserName]][] = $data[1];
                        $aCsvData[] = $data;
                     }
                    fclose($fHandle);
                }else{
                    $bImport = false;
                    $sMemo = "失败.没有读到相应的上传文件";
                }
                if($bImport){
                    if(count($aDate)){
                        //清理覆盖数据
                        foreach($aDate as $sUserId => $aTmp){
                            for($i=0; $i<count($aTmp); $i++){
                                $aParams = array(
                                    'op'=> 'delete',
                                    'resource'=> $sReportTable,
                                    'resource_type'=> 'common',
                                    'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
                                    'params' => array(
                                        'where' => array('user_id'=>$sUserId, 'date'=>$aTmp[$i]),
                                    ),
                                );
                                $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
                            }
                        }
                        foreach($aCsvData as $aTmp){
                            $aInsert = array();
                            foreach($aConfig['report_field_mapper'][$sReportTable] as $k2=>$v2){
                                $aInsert[$k2] = $aTmp[$v2];
                            }
//                            var_dump($sReportTable);
//                            var_dump($aInsert);
                            $aParams = array(
                                'op'=> 'save',
                                'resource'=> $sReportTable,
                                'resource_type'=> 'common',
                                'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
                                'params' => array(
                                    'dataset' => array (
                                        $aInsert
                                    ),
                                ),
                            );
                            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
                        }
                    }
                    $sMemo = "导入数据成功";
                }

                $aParams = array(
                    'op'=> 'save',
                    'resource'=> $sSource,
                    'resource_type'=> 'common',
                    'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
                    'params' => array(
                        'dataset' => array (
                            array( 'memo' => $sMemo),
                        ),
                        'where' => array('id'=>$sSaveId),
                    ),
                );
                $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
            break;

        }
    }

}
