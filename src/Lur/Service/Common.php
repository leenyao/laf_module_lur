<?php

namespace Lur\Service;



use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Common implements ServiceManagerAwareInterface
{
    protected $serviceManager;
    var $thirdScheduleRedis=null;
    var $aAdr2VpnAcc=null;
    var $aAdr2VpnHost=null;
    var $aAllVpnAccs=null;
    var $aSchedule=array();
    var $aSspData=null;
    var $aSspData2=null;
    var $aAdrSummary=null;

    public function __construct()
    {

    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
    *短信SOS
    */
    public function smsSos($m_content)
    {
        $aConfig = $this->serviceManager->get('config');


        if (isset($aConfig['debugFlag']) && $aConfig['debugFlag']) {
            return true;
        }

        
        foreach ($aConfig['smsmonitorphones'] as $name => $phone) {
            $m_content = str_replace('[LY-SOS]', '', $m_content);
            $m_content = substr($m_content, 0,20);
            \YcheukfCommon\Lib\Functions::sendSMSByAliyun($this->serviceManager,$phone,'SMS_139226757', ['m_content'=>$m_content, 'm_time'=>date("Y-m-d H:i:s")]);
        }
        
    }



    public function updateCache($sCacheKey)
    {

    	switch ($sCacheKey) {
    		case LAF_CACHE_PROXYCOMMONCONFIG:
		    	$aM1007 = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->serviceManager, 1007);
		    	$aProxyIpconfig = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->serviceManager, 1011);
		    	$aCityName2Id = array_flip($aM1007);
		    	$bFlag = \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, $sCacheKey, array($aProxyIpconfig, $aCityName2Id), 7*3600);
    			break;
    		
            case LAF_CACHE_IACCITY2IP:

                $aM1008 = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1007);
                $aCacheData = $this->getIacCity2IpList();
                $bFlag = \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, $sCacheKey, $aCacheData, 24*3600);
            break;
    		default:
    			# code...
    			break;
    	}
    }

    /**
     * 获取整个平台的 今天/昨天 的小时数据
    */
    public function getLatestDateHourReport()
    {
        $sDate1 = date("Ymd", strtotime("-1 day"));
        $sDate2 = date("Ymd");
        $aHourDatas = [];
        $aKeys = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager)->keys(LAF_CACHE_ADRLASTSTATS_121."1/*");
        if (count($aKeys)) {
            foreach ($aKeys as $sCacheTmp2) {
                $aResult = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager,$sCacheTmp2);
                
                $aTmp7 = json_decode($aResult['data'], 1);

                if (isset($aTmp7['dhc_'.$sDate1])) {
                    for ($i=0; $i < 24; $i++) { 
                        $sHourTmp = strval($i<10 ? "0".$i : $i);
                        $nTotalTmp1 = isset($aTmp7['dhc_'.$sDate1][$sHourTmp]) ? $aTmp7['dhc_'.$sDate1][$sHourTmp] : 0;
                        $nTotalTmp2 = isset($aTmp7['dhc_'.$sDate2][$sHourTmp]) ? $aTmp7['dhc_'.$sDate2][$sHourTmp] : 0;

                        if (!isset($aHourDatas[$sDate1][$sHourTmp])) {
                            $aHourDatas[$sDate1][$sHourTmp] = 0;
                        }
                        if (!isset($aHourDatas[$sDate2][$sHourTmp])) {
                            $aHourDatas[$sDate2][$sHourTmp] = 0;
                        }
                        $aHourDatas[$sDate1][$sHourTmp] += $nTotalTmp1;
                        $aHourDatas[$sDate2][$sHourTmp] += $nTotalTmp2;
                    }
                }
            }
        }
        return $aHourDatas;

    }

    public function getIacCity2IpListByCityId($nCityId)
    {

        $sCityLabel = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, 1007, $nCityId);
        $sCityLabel2 = $sCityLabel."市";

        $aParams = array(
            'op'=> 'get',
            'resource'=> 'b_iacip_list',
            'resource_type'=> 'common',
            'params' => array(
                'select' => array (
                   "limit"=> 100,
                   "where" => array('$or' => array(array("citylabel"=>$sCityLabel2), array("citylabel"=>$sCityLabel))),
                ),
            ),
        );
        $aCacheData = array();
        $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
        if (count($aResult['data'])) {
            foreach ($aResult['data']['dataset'] as $aRow) {
        
                $aCacheData[] = $aRow['startip'].",".$aRow['endip'];
            }
        }

        return $aCacheData;

    }
    public function getIacCity2IpList()
    {
        $aM1008 = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1007);
        $aCacheData = array();

        foreach ($aM1008 as $nCityId => $sCityLabel) {
            $sCityLabel2 = $sCityLabel."市";

            $aParams = array(
                'op'=> 'get',
                'resource'=> 'b_iacip_list',
                'resource_type'=> 'common',
                'params' => array(
                    'select' => array (
                       "limit"=> 50,
                       "where" => array('$or' => array(array("citylabel"=>$sCityLabel2), array("citylabel"=>$sCityLabel))),
                    ),
                ),
            );
            $aResult = \YcheukfCommon\Lib\Functions::getDataFromApi($this->serviceManager, $aParams, 'function');
            if (count($aResult['data'])) {
                foreach ($aResult['data']['dataset'] as $aRow) {
            
                    if(!isset($aCacheData[$nCityId])){
                        $aCacheData[$nCityId] = array();
                    }
                    $aCacheData[$nCityId][] = $aRow['startip'].",".$aRow['endip'];
                }
            }
        }
        return $aCacheData;
    }

    public function updateScheduleCache()
    {
        $sCurrentDateTime = date("Y-m-d H:i:s");
        \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, "lur_adschedule_lastupdate_time", $sCurrentDateTime, null);
        \YcheukfCommon\Lib\Functions::sendMQ($this->serviceManager, $sCurrentDateTime, "adschedule_update");

        
        $redis = \Application\Model\Common::getCacheObject($this->serviceManager);
        $t = date('Y-m-d H:i:s');
        $redis->set(LAF_CACHE_DISTRICBUTIONAPI_APICACHE_816.'sohu', $t);

    }


    public function getCookieDomain($value='')
    {
        
        $pslManager = new \Pdp\PublicSuffixListManager();
        $parser = new \Pdp\Parser($pslManager->getList());
        // $host = 'http://www.admaster.com.cn/?adrid=0]';
        $url = $parser->parseUrl($value);

        return \YcheukfCommon\Lib\Functions::replaceAdrTag($url->host->registerableDomain);
    }


    public function getAdrServerLastStats($nAdrId, $nType=1)
    {
        
        $sLastStatsIdCacheKey = LAF_CACHE_ADRLASTSTATS_121.$nType."/".$nAdrId;
        $sLastStats = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, $sLastStatsIdCacheKey);
        if ($sLastStats) {
            return json_decode($sLastStats['data'], 1);
        }
        return false;
    }



    public function getScheduleFinishedInfo($sId, $sDate, $nFinish, $nSspFinish=0, $sReturnType='text'){
        list($aScheduleEntity, $nScheduleTotal) = $this->getScheduleCountByDate($sId, $sDate);

        $nSspFinish = 0;
        $nUvFinish = 0;
        $aConfig = $this->serviceManager->get('config');

        if (isset($aScheduleEntity['m1014_id']) 
            // && in_array($aScheduleEntity['m1014_id'], $aConfig['ssp_platforms'])
        ) {
            $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);

            $aAdrSerVerAll = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEY_1018);
            foreach ($aAdrSerVerAll as $nServerId=>$s) {
                $sKey2 = $nServerId."/".$aScheduleEntity['id']."/".$aScheduleEntity['user_id']."/".$aScheduleEntity['op_user_id'];
                $sDate = date("Ymd", strtotime($sDate));
                $stmp3 = $oReids->hGet(LAF_CACHE_COUNTER_DAYSET4SHOW_408.$sDate, $sKey2);
                // var_dump($oReids->hExists(LAF_CACHE_COUNTER_DAYSET4SHOW_408.$sDate, $sKey2));
                // var_dump($stmp3);
                $stmp3_json = json_decode($stmp3, 1);


                $nUvAdCount = $this->getUvAdCount($sDate, $aScheduleEntity['id'], $nServerId);
                // var_dump($nUvAdCount);

                $nUvFinish += $nUvAdCount;
                if (is_array($stmp3_json)) {
                    $nSspFinish += intval($stmp3_json['c_sad']);

                }
            }
        }

        $nPlan1 = ($nScheduleTotal && $nScheduleTotal>0) ? round($nFinish/$nScheduleTotal, 2) : 0;
        $nPlan2 = ($nScheduleTotal && $nScheduleTotal>0) ? round($nSspFinish/$nScheduleTotal, 2) : 0;
        $nPlan3 = ($nScheduleTotal && $nScheduleTotal>0) ? round($nUvFinish/$nScheduleTotal, 2) : 0;

        if ($sReturnType=='array') {
            return [
                [
                    'label' => $nSspFinish."/".$nScheduleTotal."(".($nPlan2*100)."%)",
                    'rate' => $nPlan2*100,
                ],
                [
                    'label' => $nFinish."/".$nScheduleTotal."(".($nPlan1*100)."%)",
                    'rate' => $nPlan1*100,
                ],
                [
                    'label' => $nUvFinish."/".$nScheduleTotal."(".($nPlan3*100)."%)",
                    'rate' => $nPlan3*100,
                ]
            ];
        }else{
            $sSspColor1 = empty($nPlan2)||$nPlan2*100>70?'':'red';
            return "<font color={$sSspColor1}>".$nSspFinish."/".$nScheduleTotal."(".($nPlan2*100)."%), "
            .$nFinish."/".$nScheduleTotal."(".($nPlan1*100)."%), "
            .$nUvFinish."/".$nScheduleTotal."(".($nPlan3*100)."%) </font>"

            ;

        }

    }


    public function getSchedule($id='')
    {
        if(isset($this->aSchedule[$id]))return $this->aSchedule[$id];
        $this->aSchedule[$id] = \YcheukfCommon\Lib\Functions::getResourceById($this->serviceManager, 'adschedule', $id);
        return $this->aSchedule[$id];
    }
    
    /**
     * 获取某投放某天的计划量
     */
    public function getScheduleCountByDate($sId, $sDate)
    {
        $aEntity = $this->getSchedule($sId);
        $nReturn = 0;
        if (count($aEntity)) {

            $aScheduleData = json_decode($aEntity['schedule'], 1);
            foreach ($aScheduleData as $k2 => $a2) {
                $a2[0] = preg_replace("/\s+/i", " ", trim($a2[0]));

                $a3 = explode(" ", $a2[0]);

                if (preg_match("/,/i", $a3[0])) {
                    list($sStartDate, $sEndDate) = explode(",", $a3[0]);
                    $nTimeStart = strtotime($sStartDate." 00:00:00");
                    $nTimeEnd = strtotime($sEndDate." 23:59:59");
                }else{
                    $nTimeStart = strtotime($a3[0]." 00:00:00");
                    $nTimeEnd = strtotime($a3[0]." 23:59:59");
                }



                if (strtotime($sDate." 00.00.01") >= $nTimeStart && strtotime($sDate." 00.00.01") <= $nTimeEnd) {
                    $nReturn = $a2[3];
                }

            }
        }
        return array($aEntity, $nReturn);
    }


    /**
     */
    function getAdrReportData($sUrl, $sUserId, $aUrlids, $sDate, $eDate){

        $aPostField = array(
            "uid" => strval($sUserId),
            "urlid" => strval(implode(",", $aUrlids)),
            "sdate" => strval($sDate),
            "edate" => strval($eDate),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            $sUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     json_encode($aPostField)); 

        $result=curl_exec ($ch);

        if(!curl_errno($ch)){ 
            $info = curl_getinfo($ch); 

            if(in_array($info['http_code'], array(200))){
                $aJson = json_decode($result, 1);
                if (!is_null($aJson) || count($aJson) < 1) {
                    return array(true, $aJson);
                }else{
                    return array(false, '['.__CLASS__.'] wrong json format:'.$result."\n");
                }
            }else{
                return array(false, '['.__CLASS__.'] wrong http_code:'.$info['http_code']."\n");
            }
            curl_close($ch);  
        }else {
            curl_close($ch);  
            return array(false, '['.__CLASS__.'] :'.curl_error($ch)."\n");
        } 

    }   


    public function getAdrInfoCurrent3HourCount($aAdrServerInfoTmp, $sKey='dayhourcounter')
    {
        
        $aDayHourCounter = isset($aAdrServerInfoTmp[$sKey]) ? $aAdrServerInfoTmp[$sKey] : array();
        $sDayHourCounterHtml = "";
        if (count($aDayHourCounter)) {
          $nHour = date("H");
          $nTmp1 = isset($aDayHourCounter[$nHour]) ? $aDayHourCounter[$nHour] : 0;
          $nHour = intval(date("H"))-1;
          $nHour = $nHour<=9 ? "0".$nHour:$nHour;
          $nTmp2 = isset($aDayHourCounter[$nHour]) ? $aDayHourCounter[$nHour] : 0;
          $nHour = intval(date("H"))-2;
          $nHour = $nHour<9 ? "0".$nHour:$nHour;
          $nTmp3 = isset($aDayHourCounter[$nHour]) ? $aDayHourCounter[$nHour] : 0;
          $sDayHourCounterHtml = $nTmp1." / ".$nTmp2." / ".$nTmp3;
        }
        return $sDayHourCounterHtml;
    }

    /**
     * 获取date-counter的排名
     */
    public function getCityIdsByCounterRanking($sDate=null, $bUseCache=true)
    {
        $sDate = is_null($sDate) ? date("Y-m-d") : $sDate;

        $aReturn = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_COUNTER_DATE_CITYIDS_RANKING_405);
        if ($bUseCache && $aReturn) {
        // if (false) {
        }else{
            $aCounterRanking = $this->getCounterRanking($sDate, false);
            if (count($aCounterRanking)) {
                $aIds = array_keys($aCounterRanking);
                $aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_LOCATION);


                $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave
                $sSql = "select fid, cid from system_relation where type=1007 && fid in (".join(",", $aIds).")";
                $oSth = $oPdo->prepare($sSql);
                $oSth->execute(array(
                ));
                $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);

                $aReturn = array();
                foreach ($aResultTmp as $aRowTmp) {
                    $aReturn[$aRowTmp['fid']] = array(
                        "id" => $aRowTmp['cid'],
                        "label" => $aCityId2Label[$aRowTmp['cid']],
                    );
                }

                // var_dump($sSql);
                // var_dump($aReturn);
                // exit;
                \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, LAF_CACHE_COUNTER_DATE_CITYIDS_RANKING_405, $aReturn, 300);
            }

        }
        return $aReturn;
    }
    /**
     * 获取date-counter的排名
     */
    public function getCounterRanking($sDate=null, $bUseCache=true)
    {
        $sDate = is_null($sDate) ? date("Y-m-d") : $sDate;

        $aCounterRanking = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_COUNTER_DATERANKING_404);

        if ($aCounterRanking && $bUseCache) {
        }else{
            $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave
            $sSql = "select sum(count) as total,code_id from b_adcounter where date=:date group by date,code_id order by sum(count) desc limit 30";

            $oSth = $oPdo->prepare($sSql);
            $oSth->execute(array(
                ":date" => date("Y-m-d", strtotime($sDate)),
            ));
            $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
            $aCounterRanking = array();
            if (count($aResultTmp)) {
                foreach ($aResultTmp as $aRowTmp) {
                    $aCounterRanking[$aRowTmp['code_id']] = $aRowTmp['total'];
                }
            }
            \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, LAF_CACHE_COUNTER_DATERANKING_404, $aCounterRanking, 300);
        }
        return $aCounterRanking;
    }

    
    /**
     * 将缓存中的SSP计数器刷进DB
     */
    public function flushCacheCounterSsp2Db()
    {
        $nReturn = 0;


        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        $aKeys = $oReids->keys(LAF_CACHE_COUNTERS_SSP_DAYSET_406."*");
        if ($aKeys && count($aKeys)) {
            $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager));

            foreach ($aKeys as $sKeyTmp) {
                $sDate = str_replace(LAF_CACHE_COUNTERS_SSP_DAYSET_406, "", $sKeyTmp);

                $aDaySetData = $oReids->hGetAll($sKeyTmp);
                if (is_array($aDaySetData) && count($aDaySetData)) {
                    foreach ($aDaySetData as $sKeyTmp2 => $nCount) {
                        list($nServerId2, $mCodeId) = explode("/", $sKeyTmp2);
                        $aSplit1 = explode("|", $mCodeId);
                        $ssp_type = isset($aSplit1[3])?strtolower($aSplit1[3]) : 'hunan';
                        $ssp_campaign = isset($aSplit1[4])?strtolower($aSplit1[4]) : '';
                        $lurid = isset($aSplit1[5])?strtolower($aSplit1[5]) : '';


                        $aSqlParams = array(
                            // ":id" => $aRMembers['id'],
                            ":adrserver_id" => $nServerId2,
                            ":date" => $sDate,
                            ":code_id" => isset($aSplit1[0]) ? $aSplit1[0] : 0,
                            ":code_type" => isset($aSplit1[1]) ? $aSplit1[1] : 0,
                            ":city_id" => isset($aSplit1[2]) ? $aSplit1[2] : 0,
                            ":ssp_type" => $ssp_type,
                            ":count" => $nCount,
                        );
                        if (!empty($ssp_campaign)) {
                            $aSqlParams[":code_cid"] = $ssp_campaign;
                        }
                        if (!empty($lurid)) {
                            $aSqlParams[":code_lid"] = $lurid;
                        }


                        $aFields = array();
                        $aFields2 = array();
                        foreach ($aSqlParams as $key => $value) {
                            $aFields[] = "`".str_replace(":", "", $key)."`";
                            $aFields2[] = $key;
                        }


                        $sSql = "REPLACE INTO b_adcounter_ssp (".join(",", $aFields).") VALUES (".join(",", $aFields2).")";
                        $oSth = $oPdo->prepare($sSql);
                        $bFalg = $oSth->execute($aSqlParams);

                    }
                    $nReturn += count($aDaySetData);
                }
                $oReids->del($sKeyTmp);
            }
        }
        
        return $nReturn;
    }


// 数据量太大, 待优化
    public function getSspReportDataFromCache($sDate, $eDate){
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        $aDates = $this->getDateFromRange($sDate, $eDate);
        $aReturn = array();
        if ($aDates) {
            foreach ($aDates as $sDateTmp) {

              $aDaaTmp = $oReids->hGetAll(LAF_CACHE_COUNTER_SSP_DAYSET4SHOW_409.$sDateTmp);
              if ($aDaaTmp) {
                  $aReturn[$sDateTmp]=$aDaaTmp;
              }else{
                  $aReturn[$sDateTmp]=array();
              }
            }
        }
        return $aReturn;
    }

    public function getSspDataByDate2($sDate, $sspplatform){
        $sDate = date("Ymd", strtotime($sDate));
        if (is_null($this->aSspData2)) {
            $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
            $aDaaTmp = $oReids->hGetAll(LAF_CACHE_COUNTER_SSP_DAYSET4SHOW_409.$sDate);

            if (count($aDaaTmp)) {
                foreach ($aDaaTmp as $sKey => $sRow) {
                    list($nServerId, $sspkey) = explode('/', $sKey);
                    @list($p1,$code_type, $nCityId, $sspplatform, $sspid, $nCodeId2) = explode('|', $sspkey);

                    if ($sspplatform != 'hunan') {
                        continue;
                    }
                    $sKeyTmp3 = $sspplatform.'-'.$sspid;

                    if (!isset($this->aSspData2[$sKeyTmp3])) {
                        $this->aSspData2[$sKeyTmp3] = [];
                    }
                    $a4Merge = [];
                    if (in_array($code_type, ['imp', 'c-t'])) {


                        if (!isset($this->aSspData2[$sKeyTmp3][$code_type])) {
                            $this->aSspData2[$sKeyTmp3][$code_type] = 0;
                        }


                        $aJson = json_decode($sRow, 1);
                        if ($aJson) {
                            $this->aSspData2[$sKeyTmp3][$code_type] += $aJson['c'];
                        }

                    }


                }
            }
        }
        return $this->aSspData2;
    }


    public function isInBlackIpList($sAdrServerIp){
        $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);

        return $oRedis->sIsMember(LAF_CACHE_BLACKIPS_122, $sAdrServerIp) ? 1 : 0;
    }
    public function getBlackIpInfo(){
        $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);
        return [
            'version' => $oRedis->get(LAF_CACHE_BLACKIP_VERSION_133),
            'count' => $oRedis->sCard(LAF_CACHE_BLACKIPS_122)
        ];
    }

    public function getIacIpInfo(){
        $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);
        return [
            'version' => $oRedis->get(LAF_CACHE_IACIP_VERSION_134),
            'iacmapping_count' => $oRedis->hLen('iacmapping'),
            'iacmapping_score_count' => $oRedis->zCard('iacmapping_score'),


            // 'count' => $oRedis->sCard(LAF_CACHE_BLACKIPS_122)
        ];
    }

    public function getCity2IpHis($sDate=null){
        $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);
        $sDate = is_null($sDate) ? date("Ymd") : $sDate;
        $sHisCacheKey2 = LAF_CACHE_VPNCITY2_HIS_146.$sDate;
        // $sHisCacheKey2 = LAF_CACHE_VPNCITY2IP_HIS_145.$sDate;
        $return = [];
        // $return2 = [];
        $cities = $oRedis->sMembers($sHisCacheKey2);
        foreach ($cities as $cityid) {
            $sHisCacheKey3 = LAF_CACHE_VPNCITY2IP_HIS_145.$sDate."/".$cityid;
            $rows = $oRedis->hGetAll($sHisCacheKey3);
            $return[$cityid] = count($rows);
            // $return2[$cityid] = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_LOCATION, $cityid);

        }

        return [$return];
    }


    public function getVpnIpHis($sDate=null){
        $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);
        $sDate = is_null($sDate) ? date("Ymd") : $sDate;
        $sHisCacheKey2 = LAF_CACHE_VPNIPS_HIS_125.$sDate;
        $aHis4Adrid = $oRedis->hGetAll($sHisCacheKey2);
        $aIp2AdrCount = [];
        $aIp2Iac = [];
        $nBlackIpCounter = 0;

        foreach ($aHis4Adrid as $k =>$row) {
            list($ip, $nAdrId) = explode(",", $k);

            $aIacIpInfo = $this->serviceManager->get('\Lur\Service\Common')->getIacConfigByIp($ip);
            $aIp2Iac[$ip] = $aIacIpInfo;

            if (!isset($aIp2AdrCount[$ip])) {
                $aIp2AdrCount[$ip] = [];
                if ($this->isInBlackIpList($ip)) {
                    $nBlackIpCounter++;
                }
            }
            if (!isset($aIp2AdrCount[$ip][$nAdrId])) {
                $aIp2AdrCount[$ip][$nAdrId] = $row;
            }
        }
        return [$aIp2AdrCount, $nBlackIpCounter, $aIp2Iac];
    }

    public function updBlackIpCounter($nAdrServerId,$aPostData){
        // ADRKEY_COMMON_CLIENT_801
        $tmp = is_array($aPostData) && isset($aPostData['data']) ? $aPostData['data'] : null;
        if ($tmp) {
            $tmp1 = json_decode($tmp, 1);
            $sAdrServerIp = isset($tmp1['ADRKEY_COMMON_CLIENT_801']) ? $tmp1['ADRKEY_COMMON_CLIENT_801'] : null;
            $sDate = date("Ymd");
            $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);

            // exec("echo '".json_encode($sAdrServerIp)."' > /tmp/feng2");

            $sHisCacheKey2 = LAF_CACHE_VPNIPS_HIS_125.$sDate;
            $oRedis->hIncrBy($sHisCacheKey2, $sAdrServerIp.",".$nAdrServerId, 1);
            $oRedis->setTimeout($sHisCacheKey2, 20*86400);
            


            if ($oRedis->sIsMember(LAF_CACHE_BLACKIPS_122, $sAdrServerIp)) {
                $sCounterKey = LAF_CACHE_BLACKIPS_COUNTER_123.$sDate;
                $oRedis->hIncrBy($sCounterKey, $sAdrServerIp, 1);

                $sHisCacheKey = LAF_CACHE_BLACKIPS_HIS_124.$sDate;
                $oRedis->hIncrBy($sHisCacheKey, $sAdrServerIp.",".$nAdrServerId, 1);

                $oRedis->setTimeout($sCounterKey, 20*86400);
                return true;
            }
        }
        return false;
    }
    public function getSspDataByDate($sDate){
        if (is_null($this->aSspData) || !isset($this->aSspData[$sDate])) {
            $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
            $aDaaTmp = $oReids->hGetAll(LAF_CACHE_COUNTER_SSP_DAYSET4SHOW_409.$sDate);

            $aTmp7 = [];
            if (count($aDaaTmp)) {
                foreach ($aDaaTmp as $sKey => $sRow) {
                    list($nServerId, $sspkey) = explode('/', $sKey);
                    @list($p1,$code_type, $p3, $p4, $p5, $nCodeId2) = explode('|', $sspkey);
                    $sKeyTmp3 = $nCodeId2."_".$nServerId;

                    if (!isset($aTmp7[$sKeyTmp3])) {
                        $aTmp7[$sKeyTmp3] = 0;
                    }


                    $aJson = json_decode($sRow, 1);
                    if ($code_type== 'imp' && isset($aJson['c']) && is_numeric($aJson['c'])) {
                        $aTmp7[$sKeyTmp3] += $aJson['c'];
                    }
                }
            }
            $this->aSspData[$sDate] = $aTmp7;
        }
        return $this->aSspData[$sDate];
    }

    public function getUvAdCount($sDate, $nCodeId, $nServerId){
        $oRedis = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);

        $sCacheKey2 = LAF_CACHE_COUNTER_DAYSET4SHOW_UV_411.$sDate;
        $sHashKey = $nServerId."/".str_replace("uv", "", $nCodeId);

        $return = $oRedis->hGet($sCacheKey2, $sHashKey);
        return $return ? $return : 0;

    }

    
    public function getSspDataBySspId($sDate, $nCodeId, $nServerId){
        $sReturn = 0;
        $aDaaTmp = $this->getSspDataByDate($sDate);
        $sKey = $nCodeId."_".$nServerId;
        return isset($aDaaTmp[$sKey]) ? $aDaaTmp[$sKey] : 0;
    }

    public function getReportDataFromCache($sDate){
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        $aDates = $this->getDateFromRange($sDate, $sDate);
        $aReturn = array();
        if ($aDates) {
            foreach ($aDates as $sDateTmp) {

              $aDaaTmp = $oReids->hGetAll(LAF_CACHE_COUNTER_DAYSET4SHOW_408.$sDateTmp);
              // var_dump(count($aDaaTmp));
              if ($aDaaTmp) {
                  $aReturn=$aDaaTmp;
              }else{
                  $aReturn=array();
              }
            }
        }

        // var_dump($aReturn);
        return $aReturn;
    }

    /**
     * 获取指定日期段内每一天的日期
     * @param  Date  $startdate 开始日期
     * @param  Date  $enddate   结束日期
     * @return Array
     */
    function getDateFromRange($startdate, $enddate){

        $stimestamp = strtotime($startdate);
        $etimestamp = strtotime($enddate);

        // 计算日期段内有多少天
        $days = ($etimestamp-$stimestamp)/86400+1;

        // 保存每天日期
        $date = array();

        for($i=0; $i<$days; $i++){
            $date[] = date('Ymd', $stimestamp+(86400*$i));
        }

        return $date;
    }

    /**
     * 将缓存中的计数器刷进DB
     */
    public function flushCacheCounter2Db()
    {
        $nReturn = 0;

        $bFlushFlag = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_COUNTER_FLUSH2DBFLAG2_403);
        // var_dump($bFlushFlag);
        if ($bFlushFlag !== 1) {
            // x秒内只允许刷一次
            \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, LAF_CACHE_COUNTER_FLUSH2DBFLAG2_403, 1, 120);

            $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
            $aKeys = $oReids->keys(LAF_CACHE_COUNTER_DAYSET_401."*");
            if ($aKeys && count($aKeys)) {
                $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager));

                foreach ($aKeys as $sKeyTmp) {
                    $sDate = str_replace(LAF_CACHE_COUNTER_DAYSET_401, "", $sKeyTmp);
                    $aDaySetData = $oReids->hGetAll($sKeyTmp);
                    // print_r($aDaySetData);
                    if (is_array($aDaySetData) && count($aDaySetData)) {
                        foreach ($aDaySetData as $sKeyTmp2 => $nCount) {
                            list($nServerId2, $nCodeId) = explode("/", $sKeyTmp2);

                            $sSql = "REPLACE INTO b_adcounter (`adrserver_id`, `date`, `code_id`, `count`) VALUES (:adrserver_id, :date, :code_id, :count)";
                            $sth = $oPdo->prepare($sSql);
                            $sth->execute(array(
                                ":adrserver_id" => $nServerId2,
                                ":date" => date("Y-m-d", strtotime($sDate)),
                                ":code_id"=> $nCodeId,
                                ":count"=>$nCount,
                            ));
                        }
                        $nReturn += count($aDaySetData);
                    }
                    $oReids->del($sKeyTmp);
                }
            }
        }
        
        return $nReturn;
    }

    /**
     *
     * @return array 
     *
     * 
{
  "date:2017-03-22": {
    "uid:1039": {
      "codeid:23745": 617
    }
  }
}
     */
    function getAdrReportDataForCounterTable($aUrlids, $sDate, $eDate, $nType=1){
        $aReturn = array();
        $aReturn2 = array();
        $aReturn3 = array();
        $aReturn6 = array();
        $aScheduleDatas = array();
        $aSspDatas = array();
        $aConfig = $this->serviceManager->get('config');
        $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave
        $this->serviceManager->get('\Lur\Service\Common')->flushCacheCounter2Db();

        foreach ($aUrlids as $id1) {
            $aScheduleDatas[$id1] = $this->getSchedule($id1);
        }


       $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);
       $aAdrid2DockerHost = $oRedis->hGetAll(LAF_CACHE_ADRID2DOCKERHOST_141);


        if (in_array($nType, array(3,4,5,7))) {
            $sSql = "select sum(count) sum,date, code_id, adrserver_id from b_adcounter where code_id in (".join(",", $aUrlids).") and date between '".$sDate."' and '".$eDate."' group by date, code_id, adrserver_id";
        }else{
            $sSql = "select sum(count) sum,date, code_id, adrserver_id from b_adcounter where code_id in (".join(",", $aUrlids).") and date between '".$sDate."' and '".$eDate."' group by date, code_id";

        }
        $sth = $oPdo->prepare($sSql);
        $sth->execute([]);
        $aResult = $sth->fetchAll(\PDO::FETCH_ASSOC);



        /*
        $aParams = array(
            'op'=> 'get',
            'resource'=> 'b_adcounter',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->serviceManager),
            'params' => array(
                'select' => array (
                    "columns" => array('date', 'code_id', 'adrserver_id', 'sum'=>array('$expression'=>'sum(count)')),
                    "where" => array(
                        'code_id' => array('$in' => $aUrlids),
                        'date' => array('$lte' => $eDate, '$gte' => $sDate),
                    ),
                    "group" => array("date", "code_id"),
                ),
            ),
        );
        if (in_array($nType, array(3,4,5,7))) {
            $aParams['params']['select']['group'] = array("date", "code_id", "adrserver_id");
        }
        $aResult = \Application\Model\Common::getResourceList2($this->serviceManager, $aParams);
        */
        

        if ($aResult && count($aResult)>0) {
            foreach ($aResult as $aRow) {
                if (!isset($aScheduleDatas[$aRow['code_id']])) {
                    continue;
                }
                $aScheduleData = $aScheduleDatas[$aRow['code_id']];

                if (!isset($aReturn[$aRow['date']])) {
                    $aReturn6[$aRow['date']] = $aReturn[$aRow['date']] = array();
                    $aReturn2[$aRow['date']] = array();
                    $aReturn3[$aRow['date']] = array();
                }
                if (!isset($aReturn3[$aRow['date']][$aRow['code_id']])) {
                    $aReturn3[$aRow['date']][$aRow['code_id']] = array();
                }
                if (!isset($aReturn[$aRow['date']][$aScheduleData['user_id']])) {
                    $aReturn6[$aRow['date']][$aScheduleData['user_id']] = $aReturn[$aRow['date']][$aScheduleData['user_id']] = array();
                }
                $aReturn[$aRow['date']][$aScheduleData['user_id']][$aRow['code_id']] = intval($aRow['sum']);
                $aReturn2[$aRow['date']][$aRow['code_id']] = intval($aRow['sum']);
                $aReturn3[$aRow['date']][$aRow['code_id']][$aRow['adrserver_id']] = intval($aRow['sum']);

                if ($nType==6) {

                    $aReturn6[$aRow['date']][$aScheduleData['user_id']][$aRow['code_id']] = $this->getScheduleFinishedInfo($aRow['code_id'], $aRow['date'], intval($aRow['sum']));
                }

                if (in_array($aScheduleData['m1014_id'], $aConfig['ssp_platforms'])) {

                    $sSql = "select sum(count) total,code_lid,adrserver_id from b_adcounter_ssp where date=:date and code_lid=:code_lid and adrserver_id=:adrserver_id and code_type='imp'  group by code_lid,adrserver_id";

                    $sth = $oPdo->prepare($sSql);
                    $sth->execute(array(
                        ":date" => $aRow['date'],
                        ":code_lid" => $aRow['code_id'],
                        ":adrserver_id" => $aRow['adrserver_id'],
                    ));
                    $aTmp = $sth->fetch(\PDO::FETCH_ASSOC);
                    if (!isset($aSspDatas[$aRow['date']])) {
                        $aSspDatas[$aRow['date']] = [];
                    }
                    if (!isset($aSspDatas[$aRow['date']][$aRow['code_id']])) {
                        $aSspDatas[$aRow['date']][$aRow['code_id']] = [];
                    }
                    $aSspDatas[$aRow['date']][$aRow['code_id']][$aRow['adrserver_id']] = $aTmp['total'];
                    // var_dump($aTmp);

                }
            }
        }
        if ($nType === 1) {
        }elseif ($nType === 3) {
            return array(true, $aReturn3);
        }elseif ($nType === 4) {

            $aReturn4 = $aMetadataList = array();
            $aMetadataList[LAF_METADATATYPE_ADRSERVER] = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_ADRSERVER);
            foreach ($aReturn3 as $k1 => $aTmp4) {
                foreach ($aTmp4 as $k2 => $aTmp5) {
                    $aReturn4[$k1.'_'.$k2] = array();
                    foreach ($aTmp5 as $k3 => $aTmp6) {
                        $aReturn4[$k1.'_'.$k2][$k3."_".$aMetadataList[LAF_METADATATYPE_ADRSERVER][$k3]] = $aTmp6;
                    }
                }
                arsort($aReturn4[$k1.'_'.$k2]);
            }


            return array(true, $aReturn4);
        }elseif ($nType === 7) {

            $aReturn4 = $aMetadataList = array();
            $aMetadataList[LAF_METADATATYPE_ADRSERVER] = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_ADRSERVER);


            $aConfig = $this->serviceManager->get('config');



            // var_dump($aSspDatas);


            // var_dump($aMetadataList);
            $a4sort = [];
            foreach ($aReturn3 as $k1 => $aTmp4) {
                foreach ($aTmp4 as $k2 => $aTmp5) {
                    $aReturn4[$k1.'_'.$k2] = array();
                    foreach ($aTmp5 as $k3 => $aTmp6) {

                        $sLabelTmp2 = $aMetadataList[LAF_METADATATYPE_ADRSERVER][$k3];
                        foreach ($aConfig['aAdrRegistrationNameMapping'] as $sKey1 => $sValue1) {
                            $sLabelTmp2 = str_replace($sKey1, $sValue1, $sLabelTmp2);
                        }

                        $nSspTmp = 0;
                        if (isset($aSspDatas[$k1])
                            && isset($aSspDatas[$k1][$k2])
                            && isset($aSspDatas[$k1][$k2][$k3])) {
                            $nSspTmp = $aSspDatas[$k1][$k2][$k3];
                        }
                        // var_dump($aSspDatas[$k1][$k2][$k3]);
                        // 
                        $datahid = substr($sLabelTmp2, 0, (strpos($sLabelTmp2, '/')));


                        $nUvAdCount = $this->getUvAdCount(date("Ymd", strtotime($k1)), $k2, $k3);

                        $dockerHost = isset($aAdrid2DockerHost[$k3]) ? $aAdrid2DockerHost[$k3]: '0';
                        $dockerHost = str_replace(".lyops.com", "", $dockerHost);
                        

                        $k3Label = "<a href='javascript:void(0)' class='copy_adr_attach' data-hid='".strtolower($datahid)."' data-aid='".$k3."' data-dhost='".$dockerHost."' data-cid='".$aRow['code_id']."'>COPY</a>,".$k3;
                        $aReturn4[$k1.'_'.$k2][$k3Label."_".$sLabelTmp2] = $nSspTmp.",".$aTmp6.",".$nUvAdCount;
                        $a4sort[$k1.'_'.$k2][$k3Label."_".$sLabelTmp2] = $aTmp6;
                    }
                }
                krsort($aReturn4[$k1.'_'.$k2]);
            }

// var_dump($aReturn4);

            return array(true, $aReturn4);
        }elseif ($nType === 5) {

            $aReturn4 = $aMetadataList = array();
            foreach ($aReturn3 as $k1 => $aTmp4) {
                foreach ($aTmp4 as $k2 => $aTmp5) {
                    if (!isset($aReturn4[$k1])) {
                        $aReturn4[$k1] = array();
                    }
                    foreach ($aTmp5 as $k3 => $aTmp6) {


                        $nSspTmp = $aTmp6;
                        if (isset($aSspDatas[$k1])
                            && isset($aSspDatas[$k1][$k2])
                            && isset($aSspDatas[$k1][$k2][$k3])) {
                            $nSspTmp = $aSspDatas[$k1][$k2][$k3];
                        }
                        $aReturn4[$k1][$k3] = $nSspTmp.",".$aTmp6;
                    }
                }
                arsort($aReturn4[$k1]);
            }

            return array(true, $aReturn4);
        }elseif ($nType === 6) {
            krsort($aReturn6);

            return array(true, $aReturn6);
        }else{
            return array(true, $aReturn2);

        }
    }

    /**
     * 获取us 的redis配置
     * 
     */
    public function getUsRedisConf(){
        $aTmp3 = $this->getUsLastData();
        $aReidsConfs = array_keys($aTmp3);
        $aReidsConfs = array_filter($aReidsConfs, function($s){
            return (preg_match("/^[\w.-_]+:\d+$/i", $s));
        });
        return $aReidsConfs;
    }

    /**
     * 获取us 的配置
     * 
     */
    public function getUsConf(){
        $aTmp3 = $this->getUsLastData();
        return isset($aTmp3['config']) ? $aTmp3['config'] : array();
    }

    public function getAllVpnAccs($platform='ad')
    {
        if (isset($this->aAllVpnAccs[$platform])) {
            return $this->aAllVpnAccs[$platform];
        }
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
        $aVpnAccounts = array();
        $aVpnAccountsStore2Acc = array();
        $sSql = "select * from `vpn_accounts` where status=1 and platform=:platform";
        $sth = $oPdo->prepare($sSql);
        $sth->execute([
            ':platform' => $platform,
        ]);
        $aTmp = $sth->fetchAll(\PDO::FETCH_ASSOC);
        $aReturn = array();
        foreach ($aTmp as $row) {
            $aReturn[$row['id']] = $row;
        }
        $this->aAllVpnAccs[$platform] = $aReturn;
        return $aReturn;
    }
    public function getAllAdrServer(){
        $aAdrSerVerProxy = $aReturn = array();
        $aAdrSerVerVpn = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021);
        $aAdrSerVerTruck = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009);
        $aAdrSerVerAll = $aAdrSerVerDirect = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEY_1018);

        $aConfig = $this->serviceManager->get('config');
        // var_dump($aConfig['aAdrRegistrationNameMapping']);
        foreach ($aAdrSerVerDirect as $key => $value) {


            foreach ($aConfig['aAdrRegistrationNameMapping'] as $sKey1 => $sValue1) {
                $aAdrSerVerDirect[$key] = str_replace($sKey1, $sValue1, $aAdrSerVerDirect[$key]);
            }

            // $aAdrSerVerDirect[$key] = isset($aConfig['aAdrRegistrationNameMapping'][$value]) ? $aConfig['aAdrRegistrationNameMapping'][$value]."/".$key : $value;
        }
        


        $aReturn['serverinfo'] = $aAdrSerVerDirect;

        if (count($aAdrSerVerVpn)) {
            foreach ($aAdrSerVerVpn as $key => $value) {
                unset($aAdrSerVerDirect[$key]);
                if ($this->isProxyAdr($key)) {
                    $aAdrSerVerProxy[$key] = $value;
                    unset($aAdrSerVerVpn[$key]);
                }
            }
        }
        if (count($aAdrSerVerTruck)) {
            foreach ($aAdrSerVerTruck as $key => $value) {
                unset($aAdrSerVerDirect[$key]);
            }
        }
        
        $aReturn['aServerType2Id'] = array(
            "ALL" => array_keys($aAdrSerVerAll),
            "TROLLEY-DIRECT" => array_keys($aAdrSerVerDirect),
            "TROLLEY-PROXY" => array_keys($aAdrSerVerProxy),
            "TROLLEY-VPN" => array_keys($aAdrSerVerVpn),
            "TRUCK" => array_keys($aAdrSerVerTruck),
        );

        return $aReturn;
    }

    public function getAdrId2Host($adrid, $bForceFlag=false){

        


        $return = [];
        $sCacheData = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_ADRID2HOST_135);

        if ($sCacheData && $bForceFlag===false) {
            $return = $sCacheData;

        }else{
            $adrs = $this->getAllAdrServer();
            foreach ($adrs['serverinfo'] as $id => $label) {
                $return[$id] = substr($label, 0, strpos($label, "/"));
            }

            \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, LAF_CACHE_ADRID2HOST_135, $return, rand(600,1200));

        }


        return isset($return[$adrid]) ? $return[$adrid] : null;

    }


    public function getAdrSummary($bForceFlag=false)
    {
        if (!is_null($this->aAdrSummary)) {
            return $this->aAdrSummary;
        }

        $aReturn = array();
        $sCacheData = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_ADRSUMMARY_112);
        if ($sCacheData && $bForceFlag===false) {
            $aReturn = $sCacheData;
        }else{
            $aAliveMachines = $this->serviceManager->get("\Lur\Service\Addtional")->getAliveList();
            $aAdrServerInfo = array();
            $aReturn["aAllAdrServer"] = $this->getAllAdrServer();
            $aReturn["aAliveMachines"] = ($aAliveMachines);
            $aReturn["aAdrServerInfo"] = array();
            $aReturn["aActiveTruck"] = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_ONLINE_ADRTRUCK_ACTIVE_317);

            $aReturn["aLiveAdrServerType2Id"] = array(
                "TROLLEY-VPN" => array(),
                "TROLLEY-DIRECT" => array(),
                "TROLLEY-PROXY" => array(),
                "TRUCK" => array(),
            );
            foreach ($aReturn["aAllAdrServer"]['aServerType2Id']['ALL'] as $nAdrId) {
                $aAdrServerInfo[$nAdrId] = $this->getAdrSerInfoById($nAdrId);
                $aReturn["aAdrServerInfo"][$nAdrId] = $aAdrServerInfo[$nAdrId];
                if (in_array($nAdrId, $aAliveMachines)) {
                    $aReturn["aLiveAdrServerType2Id"][$aAdrServerInfo[$nAdrId]['model']][] = $nAdrId;
                }
            }

            $aAllVpnAccs = $this->getAllVpnAccs();
            $aUsingVpnAcc = $this->getAdr2VpnAcc();
            $aReturn["aUsingVpnAcc"] = ($aUsingVpnAcc);
            $aReturn["aAllVpnAccs"] = ($aAllVpnAccs);


            \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, LAF_CACHE_ADRSUMMARY_112, $aReturn, 60);

        }


        if (is_array($aReturn["aAdrServerInfo"]) && count($aReturn["aAdrServerInfo"])) {
            foreach ($aReturn["aAdrServerInfo"] as $nAdrServerId => $aAdrServerInfo) {
                $aAdrServerInfo['current_ip'] = $this->serviceManager->get("\Lur\Service\Addtional")->getRemoteIpByAdrid($nAdrServerId);
                $nCityId = $this->serviceManager->get("\Lur\Service\Addtional")->getCityidByAdrid($nAdrServerId);
                $sCityLabel = $nCityId==9999 ? "全国" : \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_LOCATION, $nCityId);
                $aAdrServerInfo['current_cityid'] = $nCityId;
                $aAdrServerInfo['current_citylabel'] = $sCityLabel;
                $aReturn["aAdrServerInfo"][$nAdrServerId] = $aAdrServerInfo;
            }
        }

        $this->aAdrSummary = $aReturn;

        return $aReturn;
    }


    public function getCitysByCodeId($nId='', $bid=true)
    {
        $aReturn = $aCities = [];
        $aM1007 = \Application\Model\Common::getRelationList($this->serviceManager,1007, $nId);
        
        $sCities = ["全国"];
        if (count($aM1007)) {
            $aCities = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1007, $aM1007);

            $sCities = [];
            foreach ($aCities as $keyTmp2 => $sValTmp2) {
                $sCities[] = $bid ? $keyTmp2."/".$sValTmp2 : $sValTmp2;
            }
        }
        return [
            'data'=>$aCities,
            'label'=>join("; ", $sCities),
        ];
    }

    public function getUsLastData()
    {
        $sSql = "select * from `b_usstats` order by id desc limit 1 ";
        $aConfig = $this->serviceManager->get('config');
        $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave');
        $oQuery = $oPdo->query($sSql);
        $aUsStats = array();
        $aM1009 = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1009);
        while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){
            $aUsStats = json_decode($aResult['data'], 1);
         }
         return $aUsStats;
    }

    function getThirdScheduleRedis(){
        if (is_null($this->thirdScheduleRedis)) {
            $aConfig = $this->serviceManager->get("config");
            $oRedis = new \Redis();
            $oRedis->connect($aConfig['3rdschedule']['redismq']["host"], $aConfig['3rdschedule']['redismq']["port"]);
            $oRedis->select($aConfig['3rdschedule']['redismq']["db"]);
            $this->thirdScheduleRedis = $oRedis;
        }
        return $this->thirdScheduleRedis;
    }

    function getAddtionalRedis(){
        if (is_null($this->thirdScheduleRedis)) {

            $aConfig = $this->serviceManager->get("config");
            $oRedis = new \Redis();
            $oRedis->connect($aConfig['addtionalredis']["host"], $aConfig['addtionalredis']["port"]);
            $oRedis->select($aConfig['addtionalredis']["db"]);
            $this->thirdScheduleRedis = $oRedis;
        }
        return $this->thirdScheduleRedis;
    }

    public function getIproxyAllCity()
    {
        $aReturn = array(
            "third" => array(),
            "self" => array(),
        );
        $aConfig = $this->serviceManager->get("config");
        $sContent = \Application\Model\Common::myFileGetContents($aConfig['ipproxy_url']);
        $aTmp2 = json_decode($sContent, 1);
        if (!is_null($aTmp2)) {
            $aReturn['third'] = $aTmp2['external'];
            $aTmp4 = array();
            if (count($aTmp2['internal'])) {
                foreach ($aTmp2['internal'] as $aRow1) {
                    if (!isset($aTmp4[$aRow1['location']])) {
                        $aTmp4[$aRow1['location']] = array();
                    }
                    $aTmp4[$aRow1['location']][] = $aRow1['ip'];
                }
            }
            $aReturn['self'] = $aTmp4;

        }

        return $aReturn;
        
    }


    public function _sspTypeId2Lable($nId){

        $aMetadataList = array();
        $aMetadataList[1014] = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1014);
        return str_replace('ssp-', "", strtolower($aMetadataList[1014][$nId]));
    }
    public function getViewSspPlatformLabels(){
        $aReturn = [];

        $aMetadataList = array();
        $aMetadataList[1014] = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1014);
        $aViewSspPlatform = $this->getViewSspPlatform();
        foreach ($aViewSspPlatform as $nId) {
            $aReturn[$nId] = $aMetadataList[1014][$nId];
        }
        return $aReturn;
    }


    public function getViewSspPlatform()
    {
        
        $nOpUserId = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId();
       $aConfig = $this->serviceManager->get('config');
        if ($nOpUserId == 1) {
            $aConfig['userid2ssp_platforms'][$nOpUserId] =  $aConfig['ssp_platforms'];
        }
        return $aConfig['userid2ssp_platforms'][$nOpUserId];
    }


    /**
     * 顶端通栏右边的菜单
     */
    public function getTopMenuRightHtml($aParams=array()){
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($this->serviceManager, $this->serviceManager->get('zfcuser_auth_service')->getIdentity());
        $sLastUpdateTime = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, 'lur_adschedule_lastupdate_time');

        $sUrl1 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'clearapicache'));
        $sUrl2 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'viewschedule'));
        $sUrl3 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'viewadrservers'));
        $sUrl4 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'viewlackingcities'))."?d=".date("Y-m-d");
        $sUrl5 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'viewmappinglist'));
        $sUrl6 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'admin', 'action'=>'dashboard'));
        $sUrl8 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'sspschedule'));
        $sUrl9 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'admin', 'action'=>'vpnacc'));



        $sUserId = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId();
        $aConfig = $this->serviceManager->get('config');
        $pSt = $this->_sspTypeId2Lable(isset($aConfig['userid2ssp_platforms'][$sUserId]) ? $aConfig['userid2ssp_platforms'][$sUserId][0] : LAF_ADSOURCETYPE_SSP_HUNAN);

        $sUrl7 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'sspreport'))."?sdate=".date("Y-m-d", strtotime("-3 day"))."&edate=".date("Y-m-d")."&type=3&st=".$pSt;
        $sUrl7_2 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'sspreport'))."?sdate=".date("Y-m-d", strtotime("-3 day"))."&edate=".date("Y-m-d")."&type=9&st=".$pSt;
        $sUrl7_3 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'sspreport'))."?sdate=".date("Y-m-d", strtotime("-3 day"))."&edate=".date("Y-m-d")."&type=8&st=".$pSt;

        $sUrl10 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'viewacrossmapping'));
        $sUrl11 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'viewbadcode'));

        $sUrl12 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'admin', 'action'=>'mgplan'));

        $sUrl13 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'pvforecast'));

        $sUrl15 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'distribution', 'action'=>'shmainplan'));


        // $sUrl7 = $this->serviceManager->get('zendviewrendererphprenderer')->url('lur/default', array('controller'=>'index', 'action'=>'sspreport'))."?sdate=".date("Y-m-d", strtotime("-7 day"))."&edate=".date("Y-m-d")."&type=1";


        $sUserName = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getUsername();

        $sReturn = $sUserName." |";
        if ($aRoles[0] == 'superadmin') {
            $sReturn .= '<a target="_blank" href="'.$sUrl1.'">更新缓存['.$sLastUpdateTime.']</a> | ';
            $sReturn .= '<a target="_blank" href="'.$sUrl13.'">城市可跑量</a> | ';
            // $sReturn .= '<a target="_blank" href="'.$sUrl12.'">MG排期API</a> | ';

            $sReturn .= '<a target="_blank" href="'.$sUrl2.'">查看排期</a> | ';
            $sReturn .= '<a target="_blank" href="'.$sUrl3.'">adr状态</a> | ';
            // $sReturn .= '<a target="_blank" href="'.$sUrl9.'">vpn状态</a> | ';
            $sReturn .= '<a target="_blank" href="'.$sUrl8.'">ssp排期</a> | ';
            // $sReturn .= '<a target="_blank" href="'.$sUrl7.'">ssp报表(按天)</a> | ';
            $sReturn .= 'ssp报表(<a target="_blank" href="'.$sUrl7.'">按天</a> / <a target="_blank" href="'.$sUrl7_2.'">按plan</a> / <a target="_blank" href="'.$sUrl7_3.'">按plan+code</a> )| ';

            $sReturn .= '<a target="_blank" href="'.$sUrl5.'">mapping表</a> | ';
            // $sReturn .= '<a target="_blank" href="'.$sUrl10.'">across-m</a> | ';

            $sReturn .= '<a target="_blank" href="'.$sUrl4.'">缺量城市</a> | ';
            $sReturn .= '<a target="_blank" href="'.$sUrl6.'">运营数据</a> | ';
            $sReturn .= '<a target="_blank" href="'.$sUrl15.'">SSPPLAN</a> | ';

            
        }else if ('user-member') {
            $sReturn .= '<a target="_blank" href="'.$sUrl1.'">更新缓存['.$sLastUpdateTime.']</a> | ';
            $sReturn .= '<a target="_blank" href="'.$sUrl13.'">城市可跑量</a> | ';


            if (in_array($sUserId, [1, 1044, 2002])) {
                $sReturn .= '<a target="_blank" href="'.$sUrl12.'">MG排期API</a> | ';
            }
            if (in_array($sUserId, [1, 2003])) {
                $sReturn .= '<a target="_blank" href="'.$sUrl15.'">SSPPLAN</a> | ';
            }


            $sReturn .= '<a target="_blank" href="'.$sUrl5.'">mapping表</a> | ';
            $sReturn .= '<a target="_blank" href="'.$sUrl11.'">问题代码</a> | ';

            $aViewSspPlatform = $this->getViewSspPlatform();
            if (is_array($aViewSspPlatform) && count($aViewSspPlatform)) {
                $sReturn .= '<a target="_blank" href="'.$sUrl8.'">ssp排期</a> | ';
                $sReturn .= 'ssp报表(<a target="_blank" href="'.$sUrl7.'">按天</a> / <a target="_blank" href="'.$sUrl7_2.'">按plan</a> / <a target="_blank" href="'.$sUrl7_3.'">按plan+code</a> )| ';
            }

            // 

            // 1044

        }
        return $sReturn;

    }


    public function getIntByIp($sIp='')
     {
         
         $s = explode('.', trim($sIp));
         if (count($s)>=4) {
             $d = $s[0]*0x1000000+$s[1]*0x10000+$s[2]*0x100+$s[3];
             return $d;
         }
         return $sIp;
     }

     // 根据IP取iac的信息
     function getIacCityId($sIp='', $bDebug=false)
    {
       $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);

        $d = $this->getIntByIp($sIp);
        $sCacheCityScore = 'iacmapping_score';

        $value = $oRedis->zRevRangeByScore($sCacheCityScore, $d,0, ['limit'=>[0, 1]]);

        if (is_array($value) && count($value)) {
            $tmp1 = json_decode($value[0], 1);
            return $tmp1[0];
        }
        return 0;
    }



    public function province2cities(){

        $aM1007 = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1007);

        $aprovices = [];
        $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager);
        $sSql = "select t1.id ctid, t1.name ctname,t1.province_id,t2.name provincename from `laf_lur`.`location_city` t1 left join location_province t2 on t1. province_id=t2.id order by t1.province_id, t1.city_index asc";
        $oQuery = $oPdo->query($sSql);
        while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){
            // $k1 = $aResult['province_id']."/".$aResult['provincename'];
            $k1 = $aResult['provincename'];
            if (!isset($aprovices[$k1])) {
                $aprovices[$k1] = [];
            }
            $aprovices[$k1][$aResult['ctid']] = $aResult['ctname'];

        }


        krsort($aM1007);
        $aprovicesRes = [];
        foreach ($aM1007 as $ctresid => $label2) {
            foreach ($aprovices as $k1 => $row2) {
                foreach ($row2 as $k2 => $label3) {
                    if (preg_match("/^".$label2."/", $label3)) {
                        if (!isset($aprovicesRes[$k1])) {
                            $aprovicesRes[$k1] = [];
                        }
                        $ctresid = intval($ctresid);
                        $aprovicesRes[$k1][$ctresid] = $label2;
                        break;
                    }
                }
            }
        }
        return $aprovicesRes;
    }
    /**
     * 将IAC的记录载进缓存
     * @return [type] [description]
     */
    public function loadIacMapping($bReloadCache=0){

       $sCacheCityHash = 'iacmapping';
       $sCacheCityScore = 'iacmapping_score';
       $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);
       $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave

       if ($bReloadCache==1) {
            $oRedis->del($sCacheCityHash);
            $oRedis->del($sCacheCityScore);
       }


       if (!$oRedis->exists($sCacheCityScore)) {
           $sSql = "select start,end,citycode,citylabel from `b_iacip_list` order by end asc ";
           $oSth = $oPdo->prepare($sSql);
           $oSth->execute(array(
           ));
           $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
           foreach ($aResultTmp as $aRowTmp) {

               $oRedis->zAdd($sCacheCityScore, $aRowTmp['start'], json_encode([$aRowTmp['citycode'],$aRowTmp['citylabel'], $aRowTmp['start']]));
           }
       }

       if (!$oRedis->exists($sCacheCityHash)) {
           
           $sSql = "select a.code,a.fcode,b.citylabel as label,a.mapresid from `location_iaccode` a left join b_iacip_list b on a.code=b.citycode group by a.code order by a.code asc ";
           $oSth = $oPdo->prepare($sSql);
           $oSth->execute(array(
           ));
           $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
           foreach ($aResultTmp as $aRowTmp) {
               $oRedis->hSet($sCacheCityHash, $aRowTmp['code'], json_encode([$aRowTmp['code'],$aRowTmp['fcode'],$aRowTmp['label'],$aRowTmp['mapresid']]));
           }
       }

    }

    public function isBlackIp($ip){
       $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);

        return $oRedis->sIsMember(LAF_CACHE_BLACKIPS_122, $ip) ? 1:0;

    }


    public function isIacCityMatch($ip, $cityid){

        $aIacIpInfo = $this->getIacConfigByIp2($ip);
        if ($aIacIpInfo
            && isset($aIacIpInfo['data'])
            && isset($aIacIpInfo['data']['mapresid'])
            && is_numeric($aIacIpInfo['data']['mapresid'])
            && ($cityid == $aIacIpInfo['data']['mapresid'])
        ) {
            return true;
        }
        return false;
    }


    // 新方法, 从缓存中取
    public function getIacConfigByIp2($ip, $bForceFlag=false){
       $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);
       $sCacheCityHash = 'iacmapping';

        $citycode = $this->getIacCityId($ip);


        // 同名映射
        $samemaps = [
            1347 => 1305, //大理.大理白族自治州
            1147 => 221,//红河.红河哈尼族彝族自治州
            1297 => 268,//西双版纳.西双版纳傣族自治州
        ];
             

        $iacinfo = $oRedis->hGet($sCacheCityHash, $citycode);
        if ($iacinfo) {
            $iacinfo_a = json_decode($iacinfo, 1);
            if (is_array($iacinfo_a) && count($iacinfo_a)) {
                if ($bForceFlag) {
                    if (!isset($iacinfo_a[3]) || empty($iacinfo_a[3])) {
                        $this->saveIacMapping($citycode);
                    }
                }


                return array(
                    "errorMsg"=>"null",
                    "data"=>array('region' => $iacinfo_a[2],
                        'code' => $citycode,
                        'mapresid' => isset($samemaps[$iacinfo_a[3]]) ? $samemaps[$iacinfo_a[3]] : $iacinfo_a[3],
                        "ip"=>$ip 
                    ),"success"=>true);
            }
        }

        return array("errorMsg"=>"unavailable ip","data"=>array(),"success"=>false);

    }

    public function getIacConfigByIp($ip, $bForceFlag=false)
    {
        return $this->getIacConfigByIp2($ip, $bForceFlag);
        /*
        if (!\YcheukfCommon\Lib\Functions::isIpv4Address($ip)) {
            return array("errorMsg"=>"unavailable ip","data"=>array(),"success"=>false);
        }
        //$ip="111.11.1.1";
        $aTmp = $s = explode('.', trim($ip));
        unset($aTmp[3]);


        $sCacheKey = LAF_CACHE_IACIPPRE.join(".",$aTmp);
        $aCacheData = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, $sCacheKey);

        if(!is_null($aCacheData) && !empty($aCacheData)){//找到缓存
            $aReturn = $aCacheData;
            $aReturn['data']['ip'] = $ip;
            $aReturn['cacheflag'] = 1;

        }else{
            $d = $s[0]*0x1000000+$s[1]*0x10000+$s[2]*0x100+$s[3];
            // $sql = 'select b_iacip_list.citylabel, location_iaccode.code, location_iaccode.mapresid from b_iacip_list inner join location_iaccode on b_iacip_list.citycode=location_iaccode.code where b_iacip_list.start<='.$d.' and b_iacip_list.end>='.$d.' ';
            $sql = 'select b.citylabel, location_iaccode.code, location_iaccode.mapresid from location_iaccode inner join (select citycode, citylabel from b_iacip_list where  end>='.$d.' order by end limit 1) b on b.citycode=location_iaccode.code';

            // echo $sql;
            
            $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
            $result = $oPdo->query($sql)->fetch(\PDO::FETCH_ASSOC);  

            if ($bForceFlag) {
                if (!isset($result['mapresid']) || empty($result['mapresid'])) {
                    $result["mapresid"] = $this->saveIacMapping($result['code']);
                }
            }

            $aReturn = array("errorMsg"=>"null","data"=>array('region' => $result['citylabel'],'code' => $result['code'],'mapresid' => $result['mapresid'],"ip"=>$ip ),"success"=>true);

            \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, $sCacheKey, $aReturn, rand(86400*5,86400*10));
            $aReturn['cacheflag'] = 0;
        }
        return $aReturn;
        */
    }
    // 通用型
    function getLastHisOp2($type, $plan)
    {
        $last = [];
        $redis = \Application\Model\Common::getCacheObject($this->serviceManager);
        $ophis = $redis->hGetAll(LAF_CACHE_DISTRICBUTIONAPI_OPHIS_810.$type."/".$plan);
        // var_dump($ophis);
        if ($ophis && is_array($ophis) && count($ophis)) {
            krsort($ophis);
            $last = json_decode(current($ophis), 1);
            if (!isset($last['grep_configs'])) {
                $last['grep_configs'] = '';
            }
            if (!isset($last['percent'])) {
                $last['percent'] = 0;
            }
        }
        return $last;
    }

    function getLastHisOp($plan)
    {
        $last = [];
        $redis = \Application\Model\Common::getCacheObject($this->serviceManager);
        $ophis = $redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_OPHIS_210.$plan);
        // var_dump($ophis);
        if ($ophis && is_array($ophis) && count($ophis)) {
            krsort($ophis);
            $last = json_decode(current($ophis), 1);
        }
        if (!isset($last['grep_configs'])) {
            $last['grep_configs'] = '';
        }
        if (!isset($last['percent'])) {
            $last['percent'] = 0;
        }
        return $last;
    }

    public function getIacProvince()
    {

        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager));

        $sql = "select code,label,mapresid from location_iaccode where fcode=0 and flabel='中国'";
        $aTmp = $oPdo->query($sql)->fetchAll(\PDO::FETCH_ASSOC); 

        $aReturn = array();
        foreach ($aTmp as $aRow) {
            $aReturn[$aRow['mapresid']] = $aRow;
        }
        return $aReturn;
    }

    public function saveIacMapping($nId)
    {

        $nReturn = 0;
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager));

        $sql = "select code,label,mapresid from location_iaccode where code=".$nId;
        $aLocationCode = $oPdo->query($sql)->fetch(\PDO::FETCH_ASSOC); 
        if (count($aLocationCode) && isset($aLocationCode['mapresid']) && !is_numeric($aLocationCode['mapresid'])) {
            $sCityTmp = $aLocationCode['label'];
            if (preg_match("/^.*市$/i", $aLocationCode['label'])) {
                $sCityTmp = preg_replace("/^(.*)市$/i", '\1', $aLocationCode['label']);
            }

            $matched = false;
            $aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_LOCATION);
            foreach ($aCityId2Label as $cityid => $label){
                if($label == $sCityTmp){
                    $matched = true;
                    $nReturn = $cityid;
                    break;
                }
            }
            if ($matched == false){
                $sMaxId = \Application\Model\Common::getMetadataMaxResId($this->serviceManager, LAF_METADATATYPE_LOCATION);
                $nReturn = $sMaxId+1;
                \YcheukfCommon\Lib\Functions::saveMetadata($this->serviceManager, LAF_METADATATYPE_LOCATION, $nReturn, $sCityTmp);
            }

            // 强制转换了吉林省
            if ($nId == '156021000000')$nReturn=244;

            $sql = "update location_iaccode set mapresid=".$nReturn." where code=".$nId;
            $aLocationCode = $oPdo->query($sql); 
        }
        return $nReturn;
    }

    public function getAllVpnCities()
    {
        $aAllAccs = $this->getAllVpnAccs();
        $aStore2AccCounts = [];
        foreach ($aAllAccs as $row) {
            if (!isset($aStore2AccCounts[$row['vpnstore']])) {
                $aStore2AccCounts[$row['vpnstore']] = 0;
            }
            $aStore2AccCounts[$row['vpnstore']] += 1;
        }

        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"));
        $sSql = "select * from `vpn_hosts` where status=1 ";
        $sth = $oPdo->prepare($sSql);
        $sth->execute();
        $aTmp = $sth->fetchAll(\PDO::FETCH_ASSOC);
        $aReturn = array();
        foreach ($aTmp as $aRow) {
            if (!isset($aReturn[$aRow['city']])) {
                $aReturn[$aRow['city']] = array();
            }
            $aReturn[$aRow['city']][$aRow['vpnstore']] = $aRow;
            $aReturn[$aRow['city']][$aRow['vpnstore']]['acccount'] = isset( $aStore2AccCounts[$aRow['vpnstore']])? $aStore2AccCounts[$aRow['vpnstore']] : 0;
        }
        ksort($aReturn);
        return $aReturn;
    }

    public function getAllRunninngCityIds(){

        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
        $sSql = "select distinct(b.cid) as nCityId from b_adschedule  a inner join system_relation b on a.id=b.fid  where  a.m102_id=1 and b.type=1007 ";
        $sth = $oPdo->prepare($sSql);
        $sth->execute();
        $aTmp = $sth->fetchAll(\PDO::FETCH_ASSOC);
        $aReturn = array();
        foreach ($aTmp as $aRow) {
            $aReturn[] = intval($aRow['nCityId']);
        }
        // asort($aReturn);
        return $aReturn;
    }



    public function getScheduleByCodeId($nCodeId)
    {

        $sReturn = "";
        $sUrl = \Application\Model\Common::fmtUrlWithDynamicToken('http://127.0.0.1/restapi.php/adschedule?md5flag=0&updatecacheflag=0&fengtest=2&id='.$nCodeId, 42);
        $sReturn = file_get_contents($sUrl);
        return $sReturn;
    }

    public function getScheduleByAdrId($nAdrId, $bUpdateFlag=1, $bMd5Flag=0)
    {

        $sReturn = "";
        $sUrl = \Application\Model\Common::fmtUrlWithDynamicToken('http://127.0.0.1/restapi.php/adschedule?md5flag='.$bMd5Flag.'&updatecacheflag='.$bUpdateFlag.'&server_id='.$nAdrId, $nAdrId);
        // var_dump($sUrl);
        // exit;
        /****code 重复请求API N次, 直到成功 *****/
        // 最多重试次数
        $nRetryTimes = 3;

        while ($nRetryTimes-- > 0) {
          $bBreakFlag = false;

          // 替换以下逻辑代码, 一旦满足条件则跳出循环
            $sReturn = file_get_contents($sUrl);

          if($sReturn){
            $bBreakFlag = true; // 满足条件
          }
          

          if ($bBreakFlag) { //跳出循环
            break;
          }else{
            sleep(1);
          }
        }
        /***************************************/

        return $sReturn;
    }


    public function saveAdr2VpnAcc($nAdrServerId, $nId){

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        $oReids->hSet(LAF_CACHE_ADR2VPNACCID_127, $nAdrServerId, $nId);
    }

    public function saveAdr2VpnHost($nAdrServerId, $nId){

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        $oReids->hSet(LAF_CACHE_ADR2VPNHOSTID_128, $nAdrServerId, $nId);
    }

    public function delAdr2VpnAcc($nAdrServerId=null){

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        if (is_null($nAdrServerId)) {
            return $oReids->del(LAF_CACHE_ADR2VPNACCID_127);
        }else{
            return $oReids->hDel(LAF_CACHE_ADR2VPNACCID_127, $nAdrServerId);
        }
    }

    public function delAdr2VpnHost($nAdrServerId=null){

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        if (is_null($nAdrServerId)) {
            return $oReids->del(LAF_CACHE_ADR2VPNHOSTID_128);
        }else{
            return $oReids->hDel(LAF_CACHE_ADR2VPNHOSTID_128, $nAdrServerId);
        }
    }

    public function getAdr2VpnAcc($nAdrServerId=null){

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        if (is_null($nAdrServerId)) {
            return $oReids->hGetAll(LAF_CACHE_ADR2VPNACCID_127);
        }else{
            return $oReids->hGet(LAF_CACHE_ADR2VPNACCID_127, $nAdrServerId);
        }
    }

    public function getAdr2VpnHost($nAdrServerId=null){

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        if (is_null($nAdrServerId)) {
            return $oReids->hGetAll(LAF_CACHE_ADR2VPNHOSTID_128);
        }else{
            return $oReids->hGet(LAF_CACHE_ADR2VPNHOSTID_128, $nAdrServerId);
        }
    }

    public function getVpnDataFromDb($nAdrServerId)
    {
        $aReturn = array();
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
        $nAccId = $this->getAdr2VpnAcc($nAdrServerId);
        $nHostId = $this->getAdr2VpnHost($nAdrServerId);
        $aAllVpnAccs = $this->getAllVpnAccs();

        if (
            $nAccId 
            && is_numeric($nAccId) 
            && is_numeric($nHostId) 
            && $aAllVpnAccs
            && isset($aAllVpnAccs[$nAccId])) {
            $sSql = "select * from `vpn_hosts` where id =".$nHostId;
            $sth = $oPdo->prepare($sSql);
            $sth->execute();
            $tmp2 = $sth->fetch(\PDO::FETCH_ASSOC);

            if (is_array($tmp2)&&count($tmp2)) {
                $aReturn['host'] = $tmp2;
            }else{
                $sVpnhost = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_VPNHOST_147);
                if (is_array($sVpnhost) && isset($sVpnhost[$nHostId])) {
                    $aReturn['host'] = $sVpnhost[$nHostId];
                }else{
                    return [];
                }

            }


            $aReturn['account'] = $aAllVpnAccs[$nAccId];
        }
        return $aReturn;
    }

    public function getAdrServerType($nAdrServerId, $bUpdateFlag=0){

        // return \YcheukfCommon\Lib\AdrApi::callfunc(__FUNCTION__, func_get_args());

        // YcheukfCommon\Lib\AdrApi::call(__FUNCTION__);

        $sCacheKey = LAF_CACHE_ADRID2TYPEID_117.$nAdrServerId;
        $sReturn = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, $sCacheKey);

        if (!$sReturn || $bUpdateFlag) {

            $sReturn = LAF_METADATATYPE_ADRSERVERTYPE_TROLLEY_1018;
            $sLabelTmp = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021, $nAdrServerId);
            if ($sLabelTmp && !empty($sLabelTmp)) {
                $sReturn = LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021;
            }
            $sLabelTmp = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009, $nAdrServerId);
            if ($sLabelTmp && !empty($sLabelTmp)) {
                $sReturn = LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009;
            }
            \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, $sCacheKey, $sReturn, 86400);
        }
        return $sReturn;
    }
    
    public function getShellScript($nAdrServerId){
        $aLastData = \YcheukfCommon\Lib\Functions::getResourceById($this->serviceManager, 'apishell', $nAdrServerId, 'host');
        $sReturn = "";
        if (count($aLastData) && $aLastData['status']==1) {
            if ($aLastData['ssh2_tunnel'] == 1) {
                $nPort = $aLastData['host']%10000 + 10000;
                $sReturn = 'ps -ef|grep "ssh -fNR"|grep -v grep|cut -c 9-15|xargs kill -9 2>/dev/null; ssh -fNR '.$nPort.':localhost:22 ruzhuo.feng@180.97.80.75 -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no';
            }else{
                $sReturn = $aLastData['data'];
            }
            $sReturn = trim($sReturn);
            if (!empty($sReturn)) {
                $sReturn = "echo ".$aLastData['id'].";".$sReturn;
            }
        }

        // nAdrServerId
//         $sReturn = <<<__EOF
//         pwd
//         pwd
// __EOF;
        return $sReturn;
    }

    
    public function getVpnData($nAdrServerId, $bUpdateFlag=0)
    {
        $sReturn = "";
        $sCacheKey = LAF_CACHE_VPNACCPRE_107.$nAdrServerId;
        $sReturn = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, $sCacheKey);
        if (!$sReturn || $bUpdateFlag) {

            $aVpnRecord = $this->getVpnDataFromDb($nAdrServerId);
            if (count($aVpnRecord)) {
                $sReturn = $aVpnRecord['host']['host']." ".$aVpnRecord['account']['account']." ".$aVpnRecord['account']['password']." ".$aVpnRecord['account']['vpnstore']." ".$aVpnRecord['host']['city']." ".$aVpnRecord['host']['province'];
                \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, $sCacheKey, $sReturn, rand(3600,7200));
            }else{
            }
        }

        return $sReturn;
    }


    public function getAdrPi3Config($host)
    {
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));

        $sSql = "select * from b_adrpi3config where host=:host";
        $sth = $oPdo->prepare($sSql);
        $sth->execute(array(
            ":host" => $host,
        ));
        return $sth->fetch(\PDO::FETCH_ASSOC);

        
    }

    /**
     * 给adr机器发送命令
     * @param  [type] $nAdrServerId [description]
     * @param  [type] $sCommandType        [description]
     * @return [type]               [description]
     */
    public function sendAdrCommand($nAdrServerId, $sCommandType, $aCommandParams=array()){
        $sCommand = "";
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager));

        switch ($sCommandType) {
            // 上报adrpi3的wifi信息
            case 'report_wifi2lur':
            case 'adrpi_reportwifi2lur':
                $sSql = "update system_apishell set status=2 where host=:host";
                $sth = $oPdo->prepare($sSql);
                $sth->execute(array(
                    ":host" => $nAdrServerId,
                ));

                $sSql = "delete from b_adrstats where type=3 and host=:host";
                $sth = $oPdo->prepare($sSql);
                $sth->execute(array(
                    ":host" => $nAdrServerId,
                ));

                $sCommand = "php /app/bin/pi3/report_wifi2lur.php;";

            break;
            // 更新adrpi的wifi信息, 并上报
            case 'adrpi_updatewifi':
                $sSql = "update system_apishell set status=2 where host=:host";
                $sth = $oPdo->prepare($sSql);
                $sth->execute(array(
                    ":host" => $nAdrServerId,
                ));

                $sSql = "delete from b_adrstats where type=3 and host=:host";
                $sth = $oPdo->prepare($sSql);
                $sth->execute(array(
                    ":host" => $nAdrServerId,
                ));


                $sSql = "replace into `b_adrpi3config` (host, var1, var2, var3, var4) VALUES (:host, :var1, :var2, :var3, :var4);";
                $sth = $oPdo->prepare($sSql);
                $sth->execute(array(
                    ":host" => $nAdrServerId,
                    ":var1" => $aCommandParams['device_name'],
                    ":var2" => $aCommandParams['device_contact'],
                    ":var3" => $aCommandParams['wifi_name'],
                    ":var4" => $aCommandParams['wifi_psw'],
                ));

                $sSql = "update `system_metadata` set label=:label where type=".LAF_METADATATYPE_ADRSERVER." and resid=:resid";
                $sth = $oPdo->prepare($sSql);
                $sth->execute(array(
                    ":label" => $aCommandParams['device_name'],
                    ":resid" => $nAdrServerId,
                ));

                
                $sCommand = "echo '".$aCommandParams['wifi_name'].":".$aCommandParams['wifi_psw']."'>/app/bin/pi3/wifipass.config;php /app/bin/pi3/wifi_connect.php 1;php /app/bin/pi3/report_wifi2lur.php;";
            break;
            default:
                break;
        }

        if (!empty($sCommand)) {
            $sSql = "insert into system_apishell set host=:host, data=:data, ssh2_tunnel=0";

            $sth = $oPdo->prepare($sSql);
            $sth->execute(array(
                ":host" => $nAdrServerId,
                ":data" => $sCommand,
            ));
        }


    }

    public function getAdrSerCopyScriptId($nAdrServerId, $codeid=0){


        $oRedis = \Application\Model\Common::getCacheObject($this->serviceManager);
        $aAdrid2DockerHost = $oRedis->hGetAll(LAF_CACHE_ADRID2DOCKERHOST_141);
        $dockerHost = isset($aAdrid2DockerHost[$nAdrServerId]) ? $aAdrid2DockerHost[$nAdrServerId]: '0';
        $dockerHost = str_replace(".lyops.com", "", $dockerHost);
        

        $aAdrSummary = $this->getAdrSummary();
        $aAdrServerInfoTmp = $aAdrSummary['aAdrServerInfo'][$nAdrServerId];

        $datahid = substr($aAdrServerInfoTmp['label'], 0, (strpos($aAdrServerInfoTmp['label'], '/')));
        return "<a href='javascript:void(0)' class='copy_adr_attach'  data-hid='".strtolower($datahid)."'  data-dhost='".$dockerHost."' data-aid='".$nAdrServerId."' data-cid='".$codeid."'>COPY</a>, ".$aAdrServerInfoTmp['label'];
    }


    function isProxyAdr($adrid){
        $redis = \Application\Model\Common::getCacheObject($this->serviceManager);
        return $redis->sIsMember(LC_PROXYADRLIST_830, $adrid);
    }
    public function getProxyconf($adrid){
        $redis = \Application\Model\Common::getCacheObject($this->serviceManager);
        $account_json = $redis->hGet(LC_PROXYADR_ID2CONFIG_831, $adrid);
        return json_decode($account_json, 1);
    }

    public function getAdrSerInfoById($nAdrServerId)
    {
        // if (isset($this->aAdrServerInfoData[$nAdrServerId])) {
        //     return $this->aAdrServerInfoData[$nAdrServerId];
        // }
        $aReturn = array();
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
        $sLabel2 = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009, $nAdrServerId);
        $sLabel1021 = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021, $nAdrServerId);
        


        $aConfig = $this->serviceManager->get('config');


        $sVpnLabel2 = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_ADRSERVER, $nAdrServerId);

        foreach ($aConfig['aAdrRegistrationNameMapping'] as $sKey1 => $sValue1) {
            $sVpnLabel2 = str_replace($sKey1, $sValue1, $sVpnLabel2);
        }

        $nAdrServerType = $sLabel2 ? "TRUCK" : "TROLLEY-DIRECT";
        $nAdrServerType = $sLabel1021 ? "TROLLEY-VPN" : $nAdrServerType;
        $nAdrServerType = $this->isProxyAdr($nAdrServerId) ? "TROLLEY-PROXY" : $nAdrServerType;





        $aReturn['id'] = $nAdrServerId;
        $aReturn['model'] = $nAdrServerType;
        $aReturn['proxyocnfig'] = $this->getProxyconf($nAdrServerId);
        $aReturn['label'] = $sLabel2 ? $sLabel2 : $sVpnLabel2;


        $aStatsData = $this->serviceManager->get('\Lur\Service\Common')->getAdrServerLastStats($nAdrServerId);

        $aReturn['daycounter'] = 0;
        $aReturn['daycounter_yesterday'] = 0;
        $aReturn['daycounter_yesterday2'] = 0;
        $aReturn['dayhourcounter'] = [];

        if ($aStatsData) {//该机器最近没有上报状态
            // $aReturn['created_time'] = $aStats1['created_time'];
            $aReturn['daycounter'] = isset($aStatsData["dc_".date("Ymd")]) ? $aStatsData["dc_".date("Ymd")] : 0;
            $sYestoday = date("Ymd", strtotime("-1 day"));
            $aReturn['daycounter_yesterday'] = isset($aStatsData["dc_".$sYestoday]) ? $aStatsData["dc_".$sYestoday] : 0;
            $sYestoday2 = date("Ymd", strtotime("-2 day"));
            $aReturn['daycounter_yesterday2'] = isset($aStatsData["dc_".$sYestoday2]) ? $aStatsData["dc_".$sYestoday2] : 0;

            $aReturn['dayhourcounter'] = isset($aStatsData["dhc_".date("Ymd")]) ? $aStatsData["dhc_".date("Ymd")] : array();
        }

        $aReturn['tds_counter'] = $this->serviceManager->get('\Lur\Service\Adrserver')->getTruckSendCounterInfo($nAdrServerId, date("Ymd"));       
        $aReturn['tds_counter_yesterday'] = $this->serviceManager->get('\Lur\Service\Adrserver')->getTruckSendCounterInfo($nAdrServerId, date("Ymd", strtotime("-1 day")));       
        $aReturn['tds_counter_yesterday2'] = $this->serviceManager->get('\Lur\Service\Adrserver')->getTruckSendCounterInfo($nAdrServerId, date("Ymd", strtotime("-2 day")));        
        



        // $sSql = "select * from `b_adrstats` where id =(select  max(id) from `b_adrstats` where status=1 and type=1 and host=".$nAdrServerId." group by host)";



        $aStatsData = $this->serviceManager->get('\Lur\Service\Common')->getAdrServerLastStats($nAdrServerId, 2);
        // $sLastStatsIdCacheKey = LAF_CACHE_ADRLASTSTATS_118."2/".$nAdrServerId;
        // $sLastStatsId = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, $sLastStatsIdCacheKey);
        if ($aStatsData) {//该机器最近没有上报状态

            $aReturn['version'] = isset($aStatsData['ENV']['software']) && isset($aStatsData['ENV']['software']['ADRTROLLEYVERSION']) ? $aStatsData['ENV']['software']['ADRTROLLEYVERSION'] : "";
            $aReturn['containerhostname'] = $aStatsData['ENV']['system']['hostname'];
            $aReturn['localmachine_ip'] = isset($aStatsData['ENV']['system']['localmachine_ip']) ? trim($aStatsData['ENV']['system']['localmachine_ip']) : "";

            if (isset($aStatsData['ENV']['system']['uptime'])) {
                $sUptime = trim($aStatsData['ENV']['system']['uptime']);
                list($sTmp1, $sTmp2) = explode(" ", $sUptime);
                $aReturn['machineuptime'] =  date("Y-m-d H:i:s", strtotime("- ".intval($sTmp1)." seconds"));
            }else{
                $aReturn['machineuptime'] =  "";
            }
        }



        // $aReturn['current_ip'] = isset($aStatsData['ENV']['system']['client_ip']) ? trim($aStatsData['ENV']['system']['client_ip']) : $this->serviceManager->get("\Lur\Service\Addtional")->getRemoteIpByAdrid($nAdrServerId);
        $aReturn['current_ip'] = $this->serviceManager->get("\Lur\Service\Addtional")->getRemoteIpByAdrid($nAdrServerId);

        $nCityId = $this->serviceManager->get("\Lur\Service\Addtional")->getCityidByAdrid($nAdrServerId);


        $sCityLabel = $nCityId==9999 ? "全国" : \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_LOCATION, $nCityId);
        $aReturn['current_cityid'] = $nCityId;
        $aReturn['current_citylabel'] = $sCityLabel;

        $aVpnInfo = $this->getVpnDataFromDb($nAdrServerId);

        if (count($aVpnInfo)) {
            $aReturn['vpn'] = array(
                "vpnstore" => $aVpnInfo['host']['vpnstore'],
                "account" => $aVpnInfo['account']['account'],
                "accountid" => $aVpnInfo['account']['id'],
                "hostid" => $aVpnInfo['host']['id'],
                "password" => $aVpnInfo['account']['password'],
                "host" => $aVpnInfo['host']['host'],
                "city" => $aVpnInfo['host']['city'],
                "modified" => $aVpnInfo['host']['modified'],
            );
            if ($nAdrServerType === "TROLLEY-VPN") {
                if (isset($aReturn['localmachine_ip']) && isset($aReturn['localmachine_ip']) && $aReturn['localmachine_ip']!==$aReturn['current_ip']) {
                    $aReturn['vpnresult'] = "VPN OK";
                }else{
                    $aReturn['vpnresult'] = "VPN FAILD";
                }
            }
        }


        $aReturn['wifi'] = array();



        $aStatsData = $this->serviceManager->get('\Lur\Service\Common')->getAdrServerLastStats($nAdrServerId, 3);


        if ($aStatsData){
            $aReturn['wifi'] = $aStatsData;
        }

        // $this->aAdrServerInfoData[$nAdrServerId] = $aReturn;
        // print_r($aStatsData['ENV']);
        return $aReturn;

        //  
        //  // var_dump($sSql);


    }

    public function getIgnoreVpnHostIds()
    {
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        $aIgnoreIdKeys = $oReids->keys(LAF_CACHE_IGNOREVPNHOST_PRE_110."*");
        $aReturn = $aRelation = $aHostIds = $aAccountIds = array();
        if ($aIgnoreIdKeys && count($aIgnoreIdKeys)) {
            foreach ($aIgnoreIdKeys as $sValueTmp) {
                $aTmp5 = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, $sValueTmp);
                if ($aTmp5) {
                    $aRelation[$aTmp5['accid']."/".$aTmp5['hostid']] = $aTmp5;
                    $aHostIds[$aTmp5['hostid']] = $aTmp5['hostid'];
                    $aAccountIds[$aTmp5['accid']] = $aTmp5['accid'];
                }
            }
        }
        $aReturn = array(
            "aRelation" => $aRelation,
            "aHostIds" => $aHostIds,
            "aAccountIds" => $aAccountIds,
        );
        return $aReturn;
    }

    public function getIgnoreVpnHostDatas()
    {
        $aReturn = array();
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, 'db_slave'));
        $aIds = $this->getIgnoreVpnHostIds();
        if (count($aIds) && count($aIds['aRelation']) && count($aIds['aHostIds'])) {
            $aId2Host = array();
            $sSql = "select * from `vpn_hosts` where id in (0".(count($aIds['aHostIds'])?",".join(",", $aIds['aHostIds']):"").")";
            $sth = $oPdo->prepare($sSql);
            $sth->execute();
            $oPDOStatement = $sth->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($oPDOStatement as $aTmp) {
                $aId2Host[$aTmp['id']] = $aTmp;
            }

            $aId2Account = array();
            $sSql = "select * from `vpn_accounts` where id in (0".(count($aIds['aAccountIds'])?",".join(",", $aIds['aAccountIds']):"").")";
            $sth = $oPdo->prepare($sSql);
            $sth->execute();
            $oPDOStatement = $sth->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($oPDOStatement as $aTmp) {
                $aId2Account[$aTmp['id']] = $aTmp;
            }

// print_r($aIds);
// print_r($aId2Host);
            foreach ($aIds['aRelation'] as $aRow2) {
                $aReturn[] = array(
                    "aHost" => $aId2Host[$aRow2['hostid']],
                    "aAccount" => $aId2Account[$aRow2['accid']],
                );
            }

        }
        // var_dump($aReturn);
        return $aReturn;
    }

    public function getVpnIp2City($sDate)
    {
        $sCacheKey = LAF_CACHE_VPNIP_FLOATHIS_319.$sDate;
        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        $aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_LOCATION);

        $aReturn = [];
        $a1 = $oObject->sMembers($sCacheKey);
        if (count($a1)) {
            foreach ($a1 as $k=>$sRow) {
                list($nIacCityId, $nVpnCityId, $codeId, $sIp) = json_decode($sRow, 1);
                $aReturn[$sIp] = [
                    'vpncity' => $nVpnCityId."/".$aCityId2Label[$nVpnCityId],
                    'iaccity' =>  $nIacCityId."/".$aCityId2Label[$nIacCityId],
                ];
            }
        }
        return $aReturn;
    }
    
    public function getVpnCityStats($sDate)
    {
        $sCacheKey = LAF_CACHE_VPNIP_FLOATHIS_319.$sDate;
        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        $aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_LOCATION);

        $aReturn = [];
        $a1 = $oObject->sMembers($sCacheKey);
        $aIpTotals = [];
        if (count($a1)) {
            foreach ($a1 as $k=>$sRow) {
                list($nIacCityId, $nVpnCityId, $codeId, $sIp) = json_decode($sRow, 1);
                $nIacCityId = $nIacCityId==1428 ? 193 :$nIacCityId;
                $keyindex = $nVpnCityId."-".$nIacCityId;
                if (!isset($aReturn[$keyindex])) {
                    $aReturn[$keyindex] = [];
                }
                $aReturn[$keyindex]['iaccity'] = $nIacCityId."/".$aCityId2Label[$nIacCityId];
                $aReturn[$keyindex]['vpncity'] = $nVpnCityId."/".$aCityId2Label[$nVpnCityId];
                $aReturn[$keyindex]['codes'][] = $codeId;
                $aReturn[$keyindex]['codes'] = array_unique($aReturn[$keyindex]['codes']);
                $aReturn[$keyindex]['ips'][] = $sIp;
                $aReturn[$keyindex]['ips'] = array_unique($aReturn[$keyindex]['ips']);
                $aIpTotals[$keyindex] = count($aReturn[$keyindex]['ips']);
            }
        }
        ksort($aReturn);
        arsort($aIpTotals);
        return [$aReturn, $aIpTotals];
    }

}

