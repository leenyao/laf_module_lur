<?php
/**
 * 补量类
 */
namespace Lur\Service;



use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Addtional implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {

    }
    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function getJobsByType($bVpnStat, $nAdrServerId, $sAdrServerType, $sAdrServerIp, $aIgnorecoids=array())
    {

        $aConfig = $this->serviceManager->get("config");
        $sAddtionalDbCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".date("Ymd");
        $nCurrentHour = date("G");


        
        $aCityLoadPercent = $this->getAllCitiesLoadPercent();
        

        $aReturn = array();
        switch (intval($sAdrServerType)) {
            case LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009: // 服务器模式, 优先取全国, 不用记录负载, 在TROLLEY机器存在的情况下, 不强多TROLLEY的资源

                // truck 介入的阈值
                $nTruckInGuideline = 30000;

                $nMapedCityId = LAF_CACHE_ADDTIONAL_ALLTICIES_9999;
                // 一次任务最多包含的请求次数
                $nMaxCountPerTime = isset($nMaxCountPerTime) ? $nMaxCountPerTime : 500;
                // 一次最多返回的任务条数
                $nJobsPerTime = 10;

                list($aMapCity, $aMapCountry) = $this->getJobsByCountry($aIgnorecoids);
                //优先跑全国, 但若存在TROLLEY机器, 则不请求全国的量, 留给TROLLEY机器请求
                if (count($aMapCountry)) {
                     /**
                     * 屏蔽, 不需要23点的逻辑了 
                    foreach ($aMapCountry as $sKeyTmp => $nCountTmp) {
                        if ($nCurrentHour<23 && $nCountTmp < $nTruckInGuideline) {//太少则留给trolley机器申请, 只有太多来不及跑才会去申请全国的量
                            continue;
                        }
                        if ($nJobsPerTime < 1) {
                            break;
                        }
                        $nCountTmp = $nCountTmp<$nMaxCountPerTime ? $nCountTmp : $nMaxCountPerTime;
                        $aReturn[$sKeyTmp] = $nCountTmp;
                        $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->hIncrBy($sAddtionalDbCacheKey, $sKeyTmp, intval("-".$nCountTmp));
                        $nJobsPerTime--;
                    }
                     */

                }

                //其次跑城市, 若存在目标城市的拨号中TROLLEY机器, 则不申请该机器
                if (count($aMapCity)) {
                    // 每次请求最多返回的任务条数
                    $aVpnCity2Count = $this->getVpnCity2Count();
                    $aLocation = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_LOCATION);


                    foreach ($aMapCity as $nCityIdTmp2 => $nCountTmp) {
                        if ($nJobsPerTime < 1) {//上限已满
                            break;
                        }
                        if (isset($aCityLoadPercent[$nCityIdTmp2])) {//存在目标城市TROLLEY拨号中机器
                            continue;
                        }
                        if (isset($aLocation[$nCityIdTmp2])) {
                            /**
                             * 该城市有VPN库存
                             * && 库存>0
                             * 
                             */
                            if (
                                isset($aVpnCity2Count[$aLocation[$nCityIdTmp2]]) 
                                && count($aVpnCity2Count[$aLocation[$nCityIdTmp2]])
                            ) {
                                // 未达到警戒线, 不要申请, 留给trolley(作废逻辑)
                                if ($nCurrentHour<53 && $nCountTmp < $nTruckInGuideline) {
                                    continue;
                                }
                            }
                        }
                        $nCountTmp = $nCountTmp<$nMaxCountPerTime ? $nCountTmp : $nMaxCountPerTime;
                        $aReturn[$nCityIdTmp2] = $nCountTmp;
                        $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->hIncrBy($sAddtionalDbCacheKey, $nCityIdTmp2, intval("-".$nCountTmp));
                        $nJobsPerTime--;
                    }
                }



                break;
            
            case LAF_METADATATYPE_ADRSERVERTYPE_TROLLEY_1018: // TROLLEY模式, 优先取城市
            default:
                $nMaxJobsPerTime = $nJobsPerTime = 100;//最多x条代码

                list($aMapCity, $aMapCountry, $nMapedCityId) = $this->getJobsByIp($bVpnStat, $sAdrServerIp, $aIgnorecoids, $nAdrServerId);

                $sCityIdIac = $this->getIacCityId($sAdrServerIp);

                // 加载城市
                list($aReturn, $nJobsPerTime) = $this->_fmtReturn($aMapCity, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, $sCityIdIac, $sAdrServerIp);


                // 把带宽都留给主要的代码, 加载x条全国代码
                list($aReturn, $nJobsPerTime) = $this->_fmtReturn($aMapCountry, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId);
                


            break;
        }

        // 更新ADRID CITYID 关系
        $this->updAdridCityidMap($nMapedCityId, $nAdrServerId, $sAdrServerIp);

        // 更新满载指标
        // $this->updLoadPercent($nMapedCityId, $nLoadPercentmolecular, $nLoadPercentDenominator);

        return $aReturn;
    }



    /**
     * 将关联的显示和点击一同返回 
     * 
    */
    function addClickRelation2List($aMapData)
    {
        $aMapDataReturn = $aMapData;
        $aShow2Click = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_SHOWCLICK_RELATION_126);

        // 存在关联数组
        if ($aShow2Click && is_array($aShow2Click) && count($aShow2Click) && count($aMapData)) {
            $aClick2Show =  array_flip($aShow2Click);

            // 如果仅有点击, 去掉
            foreach ($aMapData as $sJobKey => $v) {
                $aSplitKey = explode('-', $sJobKey);
                $nCodeId = $aSplitKey[0];
                if (isset($aClick2Show[$nCodeId])) {
                    unset($aMapDataReturn[$sJobKey]);
                }
            }

            // 如果有显示且有点击, 组合返回
            foreach ($aMapDataReturn as $sJobKey => $v) {
                $aSplitKey = explode('-', $sJobKey);
                $nShowCodeId = $aSplitKey[0];
                if (isset($aShow2Click[$nShowCodeId])) { // 存在关联点击代码
                    $nClickCodeId = $aShow2Click[$nShowCodeId];


                    foreach ($aMapData as $sJobKey2 => $v2) {
                        $aSplitKey2 = explode('-', $sJobKey2);
                        $nCodeId = $aSplitKey2[0];
                        if ($nCodeId == $nClickCodeId) { // 存在点击代码补量
                            $aMapDataReturn[$sJobKey2] = $v2;
                        }
                    }
                }
                // print_r($aSplitKey);
            }
        }
        return $aMapDataReturn;
    }


    public function getGetInitFlag($nAdrServerId, $nCodeId)
    {
        $sCacheKey = LAF_CACHE_ADDTIONAL_DATEINIT_315.date("Ymd");

        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        return $oObject->sIsMember($sCacheKey, "$nAdrServerId,$nCodeId");
    }

    public function setGetInitFlag($nAdrServerId, $nCodeId)
    {
        $sCacheKey = LAF_CACHE_ADDTIONAL_DATEINIT_315.date("Ymd");

        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        $oObject->sAdd($sCacheKey, "$nAdrServerId,$nCodeId");

        if ($oObject->ttl($sCacheKey)<86400) {
            $oObject->setTimeout($sCacheKey, 86400*5);
        }
    }

    // 添加到vpn的ip漂移列表
    public function add2VpnIpFloat($sCityIdIac, $nVpnCityId, $nCodeId, $sAdrServerIp){

        $sCacheKey = LAF_CACHE_VPNIP_FLOATHIS_319.date("Ymd");

        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        $oObject->sAdd($sCacheKey, json_encode([$sCityIdIac, $nVpnCityId, $nCodeId, $sAdrServerIp]));

        if ($oObject->ttl($sCacheKey)<86400) {
            $oObject->setTimeout($sCacheKey, 86400*4);
        }
    }


    public function _fmtReturn($aMapCity, $aReturn, $nJobsPerTime, $sAddtionalDbCacheKey, $nAdrServerId, $sCityIdIac=false, $sAdrServerIp=false)
    {

        $aMapCity = $this->addClickRelation2List($aMapCity);
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);

        $nMaxTimePerJob = 3; //每一条代码在一次请求中最多可以取多少次
        $nMaxCountPerTime = 150;// 一次任务最多包含的请求次数
        if (count($aMapCity)) {
            foreach ($aMapCity as $sJobkey => $nCountTmp) {
                $bReturn = false;

                    list($p1, $p2, $p3, $p4) = explode("-", $sJobkey);
                    for ($i=0; $i < $nMaxTimePerJob; $i++) { 
                        if ($nJobsPerTime < 1) {
                            $bReturn = true;
                            break;
                        }

                        // 若该代码指定城市
                        
                        if ($p4!=LAF_CACHE_ADDTIONAL_ALLTICIES_9999
                            && $sCityIdIac
                        ) {
                            // iac城市与vpn城市 不同
                            if ($sCityIdIac != $p4) {
                                $this->add2VpnIpFloat($sCityIdIac, $p4, $p1, $sAdrServerIp);
                                // 改代码使用严格的iac 模式
                                if ($oReids->sIsMember(LAF_CACHE_USEIACIP_ADSET_130, $p1)) {
                                    continue;
                                }
                                // \Application\Model\Common::onlinedebug($sJobkey."|".$nAdrServerId, $sCityIdIac);
                            }
                        }

                        $nCountTmp = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->hGet($sAddtionalDbCacheKey, $sJobkey);
                        if (is_numeric($nCountTmp) && intval($nCountTmp)>0) {//库存未用完
                            $nCountTmp = intval($nCountTmp);
                            

                            $sJobkey2 = $p1."-".$p2."-".$p3."-".$p4;
                        // exec("echo ".$nAdrServerId.','.$sJobkey2." >> /tmp/feng");
                            $this->setGetInitFlag($nAdrServerId, $p1);

                            $nCountTmp2 = $nCountTmp<$nMaxCountPerTime ? $nCountTmp : $nMaxCountPerTime;
                            $aReturn[$sJobkey2] = $nCountTmp2;
                            $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->hIncrBy($sAddtionalDbCacheKey, $sJobkey, intval("-".$nCountTmp2));
                            $nJobsPerTime--;
                            $p3--;
                        }

                    }
                    if ($bReturn) {
                        break;
                    }

            }
        }
        return array($aReturn, $nJobsPerTime);
    }

    public function updAdridCityidMap($nMapedCityId, $nAdrServerId, $sAdrServerIp)
    {

        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306.$nAdrServerId;
        $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->set($sCacheKey, $nMapedCityId);
        if ($this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->ttl($sCacheKey)<3600) {
            $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->setTimeout($sCacheKey, 86400);
        }

        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2IP_PRE_309.$nAdrServerId;
        $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->set($sCacheKey, $sAdrServerIp);
        if ($this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->ttl($sCacheKey)<3600) {
            $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->setTimeout($sCacheKey, 86400);
        }
    }

    public function getCityidByAdrid($nAdrServerId=null)
    {
        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306.$nAdrServerId;
        $sReturn = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->get($sCacheKey);
        return $sReturn ? $sReturn : false;

    }
    public function getRemoteIpByAdrid($nAdrServerId=null)
    {
        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2IP_PRE_309.$nAdrServerId;
        $sReturn = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->get($sCacheKey);
        return $sReturn ? $sReturn : false;

    }
    public function getAllAdrRemoteIp()
    {
        $sCacheTmp = LAF_CACHE_ADDTIONAL_ADRSERVER2IP_PRE_309;
        $aReturn = array();
        $aKeys = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->keys($sCacheTmp."*");
        if (count($aKeys)) {
            foreach ($aKeys as $sCacheTmp2) {
                $sIp = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->get($sCacheTmp2);
                if (!isset($aReturn[$sIp])) {
                    $aReturn[$sIp] = array( );
                }
                $aReturn[$sIp][] = str_replace(LAF_CACHE_ADDTIONAL_ADRSERVER2IP_PRE_309, "", $sCacheTmp2);
            }
        }

        return $aReturn;

    }

    /**
     * 更新满载指标, 废弃
     */
    public function updLoadPercent($nMapedCityId, $nLoadPercentmolecular, $nLoadPercentDenominator)
    {
        if (!empty($nMapedCityId)) {
            $aTmp = array(
                $nLoadPercentmolecular,
                $nLoadPercentDenominator,
                date("Y-m-d H:i:s"),
            );
            $sCacheKey = LAF_CACHE_ADDTIONAL_LOAD_PRE_304.$nMapedCityId;
            $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->set($sCacheKey, json_encode($aTmp));
            if ($this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->ttl($sCacheKey)<3600) {
                $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->setTimeout($sCacheKey, 86400);
            }

            $sHistoryCacheKey = LAF_CACHE_ADDTIONAL_LOADHISTORY_PRE_305.$nMapedCityId;
            $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->rPush($sHistoryCacheKey, json_encode($aTmp));
            if ($this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->lLen($sHistoryCacheKey) > 500 ) {
                $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->lPop($sHistoryCacheKey);
            }
            if ($this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->ttl($sHistoryCacheKey)<3600) {
                $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->setTimeout($sHistoryCacheKey, 86400);
            }

            
        }
    }

    /**
     * 获取所有trolley机器的city维度负载指标 city=>count
     */
    public function getAllCitiesLoadPercent(){
        $aReturn = array();
        $aAdrServers = $this->getAliveList();
        if (count($aAdrServers)) {
            foreach ($aAdrServers as $nAdrServerId) {
                $nCityId = $this->getCityidByAdrid($nAdrServerId);
                if ($nCityId) {
                    // $sCacheKey = LAF_CACHE_ADDTIONAL_LOAD_PRE_304.$nCityId;
                    // $sTmp = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->get($sCacheKey);
                    // $aJson = json_decode($sTmp, 1);
                    if (!isset($aReturn[$nCityId])) {
                        $aReturn[$nCityId] = 0;
                    }
                    $aReturn[$nCityId] = $aReturn[$nCityId] + 1;
                }

            }
            // 不要TRUCK模式机器
            if (isset($aReturn[LAF_CACHE_ADDTIONAL_ALLTICIES_9999])) {
                unset($aReturn[LAF_CACHE_ADDTIONAL_ALLTICIES_9999]);
            }
        }
        return $aReturn;
    }

    function getAddtionalRedis(){
        return $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis();
    }


    public function getJobCountById($id, $sDate=null)
    {
        
        // 补量账户数据

        $nIndex2 = 0;
        $aJobs = $this->getAllJobs(array(), $sDate);
        $nTotal = 0;
        $nCodeCount = 0;
        $aReturn = array();
        if (count($aJobs)) {
            foreach ($aJobs as $nCityId => $aTmp2) {
              foreach ($aTmp2 as $sJobkey => $nCount) {
                list($sJobId) =explode('-', $sJobkey);
                if ($sJobId == $id) {
                    $aReturn[$sJobkey] = $nCount;
                }
              }
            }
        }
        return $aReturn;
    }

    public function getLackingCityCount($sType="id", $sDate=null){
        $sDate = is_null($sDate) ? date("Ymd") : $sDate;
        $aJobs = $this->getAllJobs([], $sDate);
        $aReturn = [];

        $aM1007 = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1007);
        $aM1007[9999] = '全国';


        foreach ($aJobs as $nCityId => $aTmp2) {
            $nTotal = 0;
            foreach ($aTmp2 as $sJobkey => $nCount) {
                $nTotal += $nCount;
            }
            $sKey = $sType=='id' ? $nCityId : $aM1007[$nCityId];
            $aReturn[$sKey] = $nTotal;
        }
        return $aReturn;
    }

    public function iscitband($sp, $city){
        list($sp2cities, $citiesbanded) = $this->sp2cities();
        if (isset($sp2cities[$sp]) && isset($sp2cities[$sp][$city]) && $sp2cities[$sp][$city]>0) {
            return 1;
        }
        return 0;
    }

    // 缺量且没有被供应商绑定的城市
    public function citiesnoband(){

        $aLocation = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_LOCATION);
        $aLackingCity2Count = $this->getAllCityCounter();

        list($sp2cities, $citiesbanded) = $this->sp2cities();
        $citiesnoband = [];
        $toplacking = [];
        if (count($aLackingCity2Count)) {

            foreach($aLackingCity2Count as $cityid=>$v){
                if (isset($aLocation[$cityid])) {
                    $citylabel = $aLocation[$cityid];
                    if (!in_array($citylabel, $citiesbanded)) {
                        $citiesnoband[$cityid] = $citylabel;
                    }else{
                        $toplacking[$cityid] = $citylabel;
                    }
                }
            }
        }
        return [$citiesnoband, $toplacking];
    }

    // 获取当前供应商与城市关系
    public function sp2cities(){

        $rdata = [];
        $oCommon = $this->serviceManager->get('\Lur\Service\Common');
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);


        $adrids = $this->getAliveList();
        $bandcities = [];

        foreach($adrids as $adrid){
            $vpndata = $oCommon->getVpnData($adrid);
            $vpndata = trim($vpndata);

            if (strlen($vpndata)) {
                list($host, $account, $password, $vpnstore, $city, $province) = explode(" ", $vpndata);
                if (!isset($rdata[$vpnstore])) {
                    $rdata[$vpnstore] = [];
                }
                if (!isset($rdata[$vpnstore][$city])) {
                    $rdata[$vpnstore][$city] = 0;
                }
                $rdata[$vpnstore][$city]++;
                $bandcities[$city] = $city;
            }
        }


        $proxyconfigs = $oCacheObject->hGetAll(LC_PROXYADR_ID2CONFIG_831);
        foreach($proxyconfigs as $jsons){
            $json = json_decode($jsons, 1);
            $vpnstore = $json['p'];
            $city = $json['clabel'];
            if (!isset($rdata[$vpnstore])) {
                $rdata[$vpnstore] = [];
            }
            if (!isset($rdata[$vpnstore][$city])) {
                $rdata[$vpnstore][$city] = 0;
            }
            $rdata[$vpnstore][$city]++;
            $bandcities[$city] = $city;

        }

        return [$rdata, $bandcities];
    }

    /**
     * 获取全部进行中的任务, 并且乱序
     */
    public function getAllJobs($aIgnorecoids=array(), $sDate=null){

        $aReturn = array();
        $sDate = is_null($sDate) ? date("Ymd") : $sDate;
        $sAddtionalDbCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".$sDate;
        $aAllJobs = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->hGetAll($sAddtionalDbCacheKey);
        if (count($aAllJobs)) {
            // var_dump($aIgnorecoids);
            foreach ($aAllJobs as $key => $nCountTmp) {
                if (intval($nCountTmp) < 10) {
                    continue;
                }
                $aTmp = explode("-", $key);

                if (count($aIgnorecoids) && in_array($aTmp[0], $aIgnorecoids)) {
                    continue;
                }
                $aTmp[3] = $this->_getMapCityId($aTmp[3]);
                $sNewKey = join("-", $aTmp);
                // 按城市id分组
                if (!isset($aReturn[$aTmp[3]])) {
                    $aReturn[$aTmp[3]] = array();
                }
                $aReturn[$aTmp[3]][$sNewKey] = $nCountTmp;
            }
        }
        return $aReturn;
    }


    // 处理曾经错乱的ID
    public function _getMapCityId($nId)
    {
        $aMap = array(
            34=>27,
            220=>50,
            225=>62,
            196=>168,
            232=>143,
            224=>81,
            235=>183,
        );
        return isset($aMap[$nId]) ? $aMap[$nId] : $nId;
    }


    public function getVpnCity2Count($aCityLabels=array())
    {
        $sCacheKey = LAF_CACHE_VPNCITY2COUNT_115.'/'.md5(json_encode($aCityLabels));
        $aReturn = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, $sCacheKey);
        if ($aReturn) {
        }else{
            $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager));

            // 所有的vpn城市
            $aVpnHosts = $aVpnHostsLable2Row = array();
            if (count($aCityLabels)) {
                $sSql = "select a.city,count(*) as count from (select city, vpnstore from `vpn_hosts` where vpn_hosts.status=1 and vpn_hosts.city in ('".join("','", $aCityLabels)."') group by city,vpnstore) a left join vpn_accounts b on a.vpnstore=b.vpnstore and b.status=1 group by a.city";

                // $sSql = "select city,count(*) as count from `vpn_hosts` where status=1 and city in ('".join("','", $aCityLabels)."') group by city";
            }else{
                $sSql = "select a.city,count(*) as count from (select city, vpnstore from `vpn_hosts` where vpn_hosts.status=1  group by city,vpnstore) a left join vpn_accounts b on a.vpnstore=b.vpnstore and b.status=1 group by a.city";
            }
            $sth = $oPdo->prepare($sSql);
            $sth->execute();
            $aTmp = $sth->fetchAll(\PDO::FETCH_ASSOC);
            $aReturn = array();
            foreach ($aTmp as $aRow) {
                $aReturn[$aRow['city']] = $aRow;
            }
            \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, $sCacheKey, $aReturn, 86400);

        }
        return $aReturn;
        // print_r($aReturn);
    }

    public function getCurrentVpnRelation($bUpdCacheFlag=false)
    {
        $sCacheKey = LAF_CACHE_LACKINGCITY_CURRENTRELATION_109;
        $oAddtionalRedis = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis();

        $sCacheValue = false;
        if ($bUpdCacheFlag===false) {
            $sCacheValue = $oAddtionalRedis->get($sCacheKey);
        }

        if ($sCacheValue) {
            $aReturn = json_decode($sCacheValue, 1);
        }else{
            $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager));
            \YcheukfCommon\Lib\Functions::setUseLafCacheFlag(false);

            // 使用中VPN账号
            $aUsingVpnAcc = $this->serviceManager->get('\Lur\Service\Common')->getAdr2VpnAcc();
            // print_r($aUsingVpnAcc);

            // 使用中VPN城市
            $aUsingVpnCity = $this->serviceManager->get('\Lur\Service\Common')->getAdr2VpnHost();

            // print_r($aUsingVpnCity);
            
            // print_r($aUsingVpnCity);

            $aUsingVpnCity2AdrId = array();
            foreach ($aUsingVpnCity as $nAdrServerIdTmp => $nCityIdTmp) {
                if (empty($nAdrServerIdTmp)) {
                    continue;
                }
                if (!isset($aUsingVpnCity2AdrId[$nCityIdTmp])) {
                    $aUsingVpnCity2AdrId[$nCityIdTmp] = array();
                }
                array_push($aUsingVpnCity2AdrId[$nCityIdTmp], $nAdrServerIdTmp);
            }


            // 所有的vpn账号
            $aVpnAccounts = array();
            $sSql = "select * from `vpn_accounts` where status=1 and id in(0,".join(",", array_values($aUsingVpnAcc)).")";
            $sth = $oPdo->prepare($sSql);
            $sth->execute();
            $aTmp = $sth->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($aTmp as $aRow) {
                $aVpnAccounts[$aRow['id']] = $aRow;
            }


            // VPN账号数量上限
            $aVpnAccountsMaxCount = count($aVpnAccounts);


            // 所有的vpn城市
            $aVpnHosts = $aVpnHostsLable2Row = array();
            $sSql = "select * from `vpn_hosts` where status=1 and id in(0,".join(",", array_values($aUsingVpnCity)).")";
            $sth = $oPdo->prepare($sSql);
            $sth->execute();
            $aTmp = $sth->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($aTmp as $aRow) {
                $aVpnHosts[$aRow['id']] = $aRow;
                $aVpnHostsLable2Row[$aRow['city']][$aRow['id']] = $aRow;
            }

            $aReturn = array();
            foreach ($aUsingVpnCity2AdrId as $nCityIdTmp => $aRowTmp) {
                $sCityLabel = $aVpnHosts[$nCityIdTmp]['city'];
                if (!isset($aReturn[$sCityLabel])) {
                    $aReturn[$sCityLabel] = array();
                }
                foreach ($aRowTmp as $nAdrServerIdTmp) {

                    // var_dump($nAdrServerIdTmp."=>".$aUsingVpnAcc[$nAdrServerIdTmp]);
                    $aReturn[$sCityLabel][$nAdrServerIdTmp] = array(
                        "vpnstore" => $aVpnHosts[$aUsingVpnCity[$nAdrServerIdTmp]]['vpnstore'],
                        "host" => $aVpnHosts[$aUsingVpnCity[$nAdrServerIdTmp]]['host'],

                        "account" => $aVpnAccounts[$aUsingVpnAcc[$nAdrServerIdTmp]]['account'],
                    );
                }
            }
            $oAddtionalRedis->setEx($sCacheKey, 3600, json_encode($aReturn));
        }
        return $aReturn;
        // var_dump($aUsingVpnCity2AdrId, $aUsingVpnAcc, $aUsingVpnCity, $aVpnAccounts, $aVpnHosts, $aReturn);
    }


    /**
     * 获取全部进行中的任务, 并且乱序
     */
    public function getAllCityCounter($sDate=null){

        $aReturn = array();
        $aAllJobs = $this->getAllJobs(array(), $sDate);
        if (count($aAllJobs)) {
            foreach ($aAllJobs as $nCityId  => $aTmp) {
                $aReturn[$nCityId] = array_sum($aTmp);
            }
        }
        arsort($aReturn);
        return $aReturn;
    }    
    /**
     * 获取全国与随机其他城市
     */
    public function getJobsByCountry($aIgnorecoids=array()){
        $aMapRandomCity = $aMapCountry = array();
        $aAllJobs = $this->getAllJobs($aIgnorecoids);
        if (isset($aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999])) {
            $aMapCountry = $aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999];
            unset($aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999]);
        }

        if (count($aAllJobs)) {
            $nTotal = 30;
            foreach ($aAllJobs as $nMapedCityId => $aTmp) {
                foreach ($aTmp as $key => $nCountTmp) {
                    if ($nTotal < 1) {
                        continue;
                    }
                    $aMapRandomCity[$key] = $nCountTmp;
                    $nTotal--;
                }
            }
        }

        $aMapCountry = \YcheukfCommon\Lib\Functions::shuffle_assoc($aMapCountry);
        $aMapRandomCity = \YcheukfCommon\Lib\Functions::shuffle_assoc($aMapRandomCity);

        return array(
            $aMapRandomCity,
            $aMapCountry,
        );
    }


    public function getIacCityId($sAdrServerIp){

        $sCityIdIac = false;
        $aIpConfig = $this->serviceManager->get('\Lur\Service\Common')->getIacConfigByIp($sAdrServerIp);
        
        if (is_array($aIpConfig)
         && isset($aIpConfig['data'])
         && isset($aIpConfig['data']['mapresid'])
         && !empty($aIpConfig['data']['mapresid'])
        ) {// 找到iac关系
            $sCityIdIac = $aIpConfig['data']['mapresid'];
        }

        return $sCityIdIac;
    }
    /**
     * 获取全国与某指定城市
     */
    public function getJobsByIp($bVpnStat, $sAdrServerIp, $aIgnorecoids, $nAdrServerId=0)
    {
        $aMapCity = $aMapCountry = array();
        $aAllJobs = $this->getAllJobs($aIgnorecoids);

        $nMapedCityId = LAF_CACHE_ADDTIONAL_ALLTICIES_9999;
        if ($bVpnStat === LAF_CALLVPNSTAT_OK_601) {//vpn 拨号成功状态, 则从现有的 adrid-vpncityidid中获取任务
            // vpn 中适配好的 adrid-vpncityid
            $nMapedCityId = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_ADRSERVERID2CITYID_113.$nAdrServerId);
        }elseif ($bVpnStat === LAF_CALLVPNSTAT_DERECT_603) { // 直连模式, 从iac中匹配关系
            // \Application\Model\Common::onlinedebug("LAF_CALLVPNSTAT_DERECT_603", $bVpnStat);

            $aIpConfig = $this->serviceManager->get('\Lur\Service\Common')->getIacConfigByIp($sAdrServerIp, true);
            
            if (is_array($aIpConfig)
             && isset($aIpConfig['data'])
             && isset($aIpConfig['data']['mapresid'])
             && !empty($aIpConfig['data']['mapresid'])
            ) {// 找到iac关系
                $nMapedCityId = $aIpConfig['data']['mapresid'];
            }else{
            }
            // \Application\Model\Common::onlinedebug("nMapedCityId", [$nAdrServerId, $nMapedCityId]);
            
        }else{// 拨号失败, 只允许访问全国

        }

        if (isset($aAllJobs[$nMapedCityId])) {

            $aMapCity = $aAllJobs[$nMapedCityId];
        }
        if (isset($aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999])) {
            $aMapCountry = $aAllJobs[LAF_CACHE_ADDTIONAL_ALLTICIES_9999];
            $aMapCountry = \YcheukfCommon\Lib\Functions::shuffle_assoc($aMapCountry);
        }
        if (count($aMapCity)) {
            $aMapCity = \YcheukfCommon\Lib\Functions::shuffle_assoc($aMapCity);
        }

        return array(
            $aMapCity,
            $aMapCountry,
            $nMapedCityId
        );

    }


    /**
     * 所有活着的ADR-TROLLEY-VPN机器
     */
    public function getAliveAdrTrolleyVpnList()
    {
        $aReturn = array();
        $aAliveMachines = $this->getAliveList();
        // 所有用于 adr-trolley-vpn 的id
        $aAdrTrolleyVpns = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021);

        if (count($aAdrTrolleyVpns)) {
            $aConfig = $this->serviceManager->get('config');
            foreach ($aAdrTrolleyVpns as $nId => $sTmp) {
                if (isset($aConfig['adridAliveNoCallVpn']) && in_array($nId, $aConfig['adridAliveNoCallVpn'])) {
                    continue;
                }
                if($this->isProxyAdr($nId))continue;
                if (in_array($nId, $aAliveMachines)) {
                    $aReturn[] = $nId;
                }
            }
        }
        return $aReturn;
    }


    function getBindedProxyConfig(){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        return $oCacheObject->hGetAll(LC_PROXYADR_ID2CONFIG_831);
    }

    function getProxyAdrs(){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        $ids = $oCacheObject->sMembers(LC_PROXYADRLIST_830);
        // id逆序, 优先让机器好的跑proxy
        if (is_array($ids) && count($ids)) {
            arsort($ids);
        }
        return $ids;
    }
    function isProxyAdr($adrid){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        return $oCacheObject->sIsMember(LC_PROXYADRLIST_830, $adrid);
    }

    /**
     * 所有活着的ADR-TROLLEY-VPN机器, 乱序
     */
    public function getAliveAdrTrolleyVpnList2()
    {
        $aReturn = \Application\Model\Common::shuffle_assoc($this->getAliveAdrTrolleyVpnList());
        return $aReturn;
    }

    /**
     * 所有活着的ADR机器
     */
    public function getAliveList(){
        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        $aReturn = $oObject->sMembers(LAF_CACHE_ADRSERVERS_ALIVE_106);
        return $aReturn ? $aReturn : array();
    }

    /**
     * adr机器是否或者
     */
    public function isAdrAlive($sId){
        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        return $oObject->sIsMember(LAF_CACHE_ADRSERVERS_ALIVE_106, $sId);
    }


    public function remove4AliveList($sId='')
    {
        
        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);

        if ($oObject->sIsMember(LAF_CACHE_ADRSERVERS_ALIVE_106, $sId)) {
            $oObject->sRem(LAF_CACHE_ADRSERVERS_ALIVE_106, $sId);
            // echoMsg('['.__CLASS__.'] remove4AliveList :'.$sId);
        }

    }

    public function add2AliveList($sId='')
    {
        // \YcheukfCommon\Lib\AdrApi::callfunc(__FUNCTION__, func_get_args());
        // return true;
        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        if (!$oObject->sIsMember(LAF_CACHE_ADRSERVERS_ALIVE_106, $sId)) {
            $oObject->sAdd(LAF_CACHE_ADRSERVERS_ALIVE_106, $sId);
            // echoMsg('['.__CLASS__.'] add2AliveList :'.$sId);
        }


        $aConfig = $this->serviceManager->get('config');
        \YcheukfCommon\Lib\Functions::saveCache($this->serviceManager, LAF_CACHE_ADDTIONAL_ADRALIVE_PRE_310.$sId, 1, $aConfig['adr_server_heartbeat_interaltime']);


    }

    public function getVpnFaildHistoryByAccId($sId=''){
        $sCacheKey = LAF_CACHE_VPNFAILD_HISTORY_120.date("Ymd").'/'.$sId;
        $aReturn = array();

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
        if ($oReids->exists($sCacheKey)) {
            return $oReids->lRange($sCacheKey, 0, -1);
        }
        return array();

    }

    function getFaildTooManyAcc(){
        $sCacheKey = LAF_CACHE_VPNFAILD_HISTORY_120.date("Ymd")."/";

       $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
       $aKeys = $oReids->keys($sCacheKey."*");
       $sCurrentHour = date("YmdH");
       $sPreHour = date("YmdH", strtotime("-1 hour"));
       // $sPreHour = $sCurrentHour;
       $aReturn = array();
       if (count($aKeys)) {
            foreach ($aKeys as $sKey) {
                $nAccId = str_replace($sCacheKey, '', $sKey);
                if ($this->isVpnAccAtive($nAccId) == false) {
                    continue;
                }
                // if ($oReids->llen($sKey) < 10) continue;

                $aHis = $this->getVpnFaildHistoryByAccId($nAccId);
                $nLastestHourTotal  = 0;
                foreach ($aHis as $sTmp) {
                    $aRow = json_decode($sTmp, 1);
                    if (in_array(date("YmdH", strtotime($aRow['t'])), array($sCurrentHour, $sPreHour))) {
                        $nLastestHourTotal++;
                    }
                }
                // if ($nLastestHourTotal > 3) {
                    $aReturn[$nAccId] = $nLastestHourTotal;
                // }

                # code...
            }
                // var_dump($aReturn);

       }
       return $aReturn;


    }

    function isVpnAccAtive($id){

        $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave
        $sSql = "select * from `vpn_accounts` where status=1 and id=:id";
        
        $oSth = $oPdo->prepare($sSql);
        $oSth->execute(array(
            ":id" => $id,
        ));
        $aResultTmp = $oSth->fetch(\PDO::FETCH_ASSOC);
        if ($aResultTmp) {
            return true;
        }
        return false;

    }

    function updVpnFaildHistory($sAdrVpnAcc, $aVpnHost, $nServerId){
        $sCacheKey = LAF_CACHE_VPNFAILD_HISTORY_120.date("Ymd").'/'.$sAdrVpnAcc['id'];

       $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);
       $aTmp = array(
        't'=>date("Y-m-d H:i:s"),
        'a'=>$sAdrVpnAcc,
        'h'=>$aVpnHost,
        's'=>$nServerId,
       );
        $oReids->rPush($sCacheKey, json_encode($aTmp));

        $oReids->setTimeout($sCacheKey, 3*86400);
    }

}

