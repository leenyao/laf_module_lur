<?php

namespace Lur\Service;


require_once "/app/module/Lur/config/hashredisdef.php";










use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Proxyip implements ServiceManagerAwareInterface
{
    protected $serviceManager;

    public function __construct()
    {

    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $locator
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }
    

    // 每天的IP池子, proxy+vpn
    public function getAdrIdPool($date)
    {
        // 
        $return = HashRedis::sMembers(ADRKEY_IPHIS_ADRPOOL_369.$date);
        return $return;

    }

    // proxy 的平台
    public function getPlats($date)
    {
        // 
        $return = HashRedis::sMembers(ADRKEY_PROXYCOUNTER_PLATS_366.$date);
        return $return;

    }

    public function getGoodIpsByPlat($plat, $date){
        $cachekey = ADRKEY_PROXYCOUNTER_GOODS_364.$date."/".$plat;
        return HashRedis::hGetAll($cachekey);
        
    }
    public function getBadIpsByPlat($plat, $date){
        $cachekey = ADRKEY_PROXYCOUNTER_BADS_365.$date."/".$plat;
        return HashRedis::hGetAll($cachekey);
        
    }
    public function getBlackIpsByPlat($plat, $date){
        $cachekey = ADRKEY_PROXYCOUNTER_BLACKIP_367.$date."/".$plat;
        return HashRedis::hGetAll($cachekey);
        
    }


    public function getSummary($date, $types=['vpn', 'proxy']){

        $rowtype = [];
        $rowall = [
            'count_all' => 0,
            'count_black' => 0,
            'percent' => 0,
        ];
        foreach($types as $type){
            $cachekey_all = ADRKEY_IPHIS_IPS_372.$date."/".$type;
            $cachekey_black = ADRKEY_IPHIS_IPS_BLACK_373.$date."/".$type;
            $count_all = HashRedis::sCard($cachekey_all);
            $count_black = HashRedis::sCard($cachekey_black);
            $rowtype[$type] = [
                'count_all' =>  $count_all,
                'count_black' =>  $count_black,
                'percent' =>  empty($count_all)?0:100*round($count_black/$count_all, 4),
            ];
            $rowall['count_all'] += $count_all;
            $rowall['count_black'] += $count_black;

        }
        $rowall['percent'] = empty($rowall['count_all'])?0:100*round($rowall['count_black']/$rowall['count_all'], 4);

        return [$rowall, $rowtype];
    }

    public function getCityStatsByPlat($plat, $date){

        $cachekey = ADRKEY_IPHIS_CITY_374.$date;
        $cities = HashRedis::sMembers($cachekey);
        $data = [];
        if (is_array($cities) &&  count($cities)) {
            foreach($cities as $city){

                $cachekey2 = ADRKEY_IPHIS_CITY2IPS_371.$date."/".$plat."/".$city;
                if (HashRedis::exists($cachekey2)) {
                    $data[$city] = HashRedis::sCard($cachekey2);

                }
            }
        }

        return $data;

    }



    public function getCodesByPlat($plat, $date){

        $data = [];
        $oObject = \Application\Model\Common::getCacheObject($this->serviceManager);
        $adrids = $oObject->sMembers(LAF_CACHE_ADRSERVERS_ALIVE_106);
        foreach($adrids as $adrid){
            $cachekey2 = ADRKEY_PROXYCOUNTER_APICODE_368.$date."/".$plat."/".$adrid;

            if (HashRedis::exists($cachekey2)) {
                $data[$adrid] = HashRedis::hGetAll($cachekey2);

            }
        }
        return $data;
    }

    // proxy 的表现
    public function getIpstatsByPlat($plat, $date)
    {
        $good = $this->getGoodIpsByPlat($plat, $date);
        $bad = $this->getBadIpsByPlat($plat, $date);
        $black = $this->getBlackIpsByPlat($plat, $date);
        $count_total = count($good)+count($bad);
        $sum_total = array_sum($good)+array_sum($bad);

        $row = [
            "count_total" => $count_total, 
            "sum_total" => $sum_total, 
            "good_count" => count($good), 
            "bad_count" => count($bad), 
            "black_count" => count($black), 
            "good_sum" => array_sum($good), 
            "bad_sum" => array_sum($bad), 
            "black_sum" => array_sum($black), 

        ];

        $row['good_percent'] = empty($row['count_total'])?0:100*round($row['good_count']/$row['count_total'], 4);
        $row['bad_percent'] = empty($row['count_total'])?0:100*round($row['bad_count']/$row['count_total'], 4);
        $row['black_percent'] = empty($row['count_total'])?0:100*round($row['black_count']/$row['count_total'], 4);

        $row['freq'] = empty($row['sum_total'])?0:round($row['sum_total']/$row['count_total'], 4);
        return $row;

    }
    
}

