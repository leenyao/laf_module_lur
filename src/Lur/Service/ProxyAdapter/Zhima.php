<?php

class Zhima{
    // 一个账号X个ADR共用的数量
    const ACCGROUPSTEP=2;

    static function keepcities(){
        // return ["宁波", "北京", "上海", "广州", "深圳"];
        return ["河南省", "四川省"];

	}
    static function cities(){
        // $cachekey = LC_PROXY_CITYLIST_838.__CLASS__;
        // $cities = ["北京", "温州", "徐州", "郑州", "昆明", "青岛", "沈阳", "重庆", "合肥", "金华","舟山","宁波", "上海", "贵阳"];
        // $cities = ["杭州", "北京", "郑州", "昆明", "青岛", "沈阳", "重庆", "合肥", "金华","舟山","宁波", "上海", "贵阳", "温州", "徐州", "郑州", "昆明", "青岛"];
        // $cities = ["杭州", "厦门", "武汉" , "合肥"];
        $cities = ["北京", "成都", "昆明", "杭州", "金华","舟山","宁波","温州", "徐州", "郑州", "石家庄"];

        return $cities;
    }
    // 处理不同的code, 判断是否需要更新
    static function monitorcodes($rowcode){
        $ok = LC_PROXY_MONITORCODE_FALSE;
        if (count($rowcode)) {
            foreach($rowcode as $code=>$count){
                switch(strtoupper($code)){
                    case "115": //无该地区ip
                        if($count > 50){
                            $ok = LC_PROXY_MONITORCODE_NEEDUPDATE;
                        }
                    break;
                    case "121": //您的该套餐已经过期了！  
                    case "116": //该套餐今天已经消耗完毕
                            $ok = LC_PROXY_MONITORCODE_EXPIRED;
                    break;
                    case "114": //账户金额消耗完毕
                    break;
                }
            }
        }
        return $ok;
    }

    // 绑定配置
	static function band( $city, $acconf, $locationconf){
        $data = [
            // https://wapi.http.linkudp.com/index/index/save_white?neek=483006&appkey=a5b729ccad4675a97c0956c3f6eb572a&white=
            "whiteurl" => "https://wapi.http.linkudp.com/index/index/save_white?neek=483006&appkey=a5b729ccad4675a97c0956c3f6eb572a&white=[IP]",

            // 包月套餐
            "url" => "http://webapi.http.zhimacangku.com/getip?num=5&type=2&pro=&city=&yys=0&port=1&pack=[pack]&ts=1&ys=0&cs=0&lb=1&sb=0&pb=45&mr=2&regions=&username=chukou01&spec=1",

            // 按次套餐
            // "url" => "http://webapi.http.zhimacangku.com/getip?num=5&type=2&pro=&city=&yys=0&port=1&time=1&ts=0&ys=0&cs=0&lb=1&sb=0&pb=45&mr=2&regions=&username=chukou01&spec=1",
            // "url" => "http://webapi.http.zhimacangku.com/getip?num=20&type=2&pro=330000&city=330109&yys=0&port=1&time=1&ts=1&ys=1&cs=1&lb=1&sb=0&pb=4&mr=3&regions=",

            // 包月套餐
            // "url" => "http://webapi.http.zhimacangku.com/getip?num=5&type=2&pro=&city=&yys=0&port=1&time=1&ts=0&ys=0&cs=0&lb=1&sb=0&pb=45&mr=2&regions=&username=chukou01&spec=1",

        ];
// http://webapi.http.zhimacangku.com/getip?num=1&type=2&pro=330000&city=330109&yys=0&port=1&time=1&ts=1&ys=1&cs=1&lb=1&sb=0&pb=4&mr=2&regions=
//pack= 169986
        $data['url'] = str_replace('&pro=&', "&pro=".$locationconf[0]."&", $data['url']);
        $data['url'] = str_replace('&city=&', "&city=".$locationconf[1]."&", $data['url']);

        if (isset($acconf['replacement']) && count($acconf['replacement'])) {
            foreach($acconf['replacement'] as $rk => $rv){
                $data['whiteurl'] = str_replace($rk, $rv, $data['whiteurl']);
                $data['url'] = str_replace($rk, $rv, $data['url']);

            }
        }
        return $data;

	}

    // 初始化地域
    static function initLocation($platcache, $aCities, $redis){


        // $url = 'http://webapi.jghttp.alicloudecs.com/index/api/get_all_area_map';
        $url = 'http://wapi.http.linkudp.com/index/api/get_all_area_map';
        $i = 0;
        $ageo2id = [];
        $aprovince2id = [];
        $aresut = [];
        $contents = file_get_contents($url);
        $a1 = json_decode($contents, 1);
        if (is_array($a1) && isset($a1['code']) && $a1['code']==1 && isset($a1['ret_data']) && count($a1['ret_data'])){
            // print_r($a1);
            foreach($a1['ret_data'] as $row1){
                if(isset($row1['date']) && count($row1['date'])){
                    foreach($row1['date'] as $row2){
                        $key1 = $row1['name'].'-'.$row2['name'];
                        $keyprovince = $row1['name'];
                        if(!isset($aprovince2id[$keyprovince])){
                            $aprovince2id[$keyprovince] = [$row1['id'], 0];
                        }
                        if(!isset($ageo2id[$key1])){
                            $ageo2id[$key1] = [$row1['id'], $row2['id']];
                        }
                    }
                }
            }
        }
        foreach($aprovince2id as $keyprovince => $row1){
            $redis->hSet($platcache, $keyprovince, json_encode($row1)); 
        }

        foreach($ageo2id as $location => $row1){
            foreach ($aCities as $sCityName) {
                $sCityName = trim($sCityName);
                if (empty($sCityName)  || strlen($sCityName)<=3) {
                    continue;
                }
                    // echo "@@@".$sCityName."\n";
                if (!isset($aresut[$sCityName]) && preg_match("/$sCityName/i", $location)) {
                    $redis->hSet($platcache, $sCityName, json_encode($row1)); 
                }

            }

        }
    }
}