<?php

class Vps91{
	
    // 一个账号X个ADR共用的数量
    const ACCGROUPSTEP=1;

    static function keepcities(){
        return ["上海", "南京"];
    }
    static function cities(){
        return ["上海",'南京','合肥','厦门','昆明','南宁','成都','长沙','天津','济南'];
    }
    // 处理不同的code, 判断是否需要更新
    static function monitorcodes($rowcode){
        $ok = false;
        if (count($rowcode)) {
            foreach($rowcode as $code=>$count){
                switch(strtoupper($code)){
                    case "9999": //无该地区ip
                        if($count > 50){
                            $ok = true;
                        }
                    break;
                }
            }
        }
        return $ok;
    }

    // 绑定配置
	static function band( $city, $acconf, $locationconf){

        $data = [
            "whiteurl" => "",
            "url" => "http://httpapi.91vps.com.cn/api/getips?num=5&proxyType=http&dataType=json&province=&city=&duplicate=24&username=H5XPBS6Z&po=A16278731627046173&auth=ip",

        ];
// 19, 351, 湖南长沙

        $data['url'] = str_replace('&province=&', "&province=".$locationconf[0]."&", $data['url']);
        $data['url'] = str_replace('&city=&', "&city=".$locationconf[1]."&", $data['url']);
        return $data;

	}

    // 初始化地域
    static function initLocation($platcache, $aCities, $redis){
        $mapss = file_get_contents(__DIR__."/vps91.json");
        $datas = json_decode(trim($mapss), 1);

        $aCities = [];
        foreach($datas['data'] as $prow){
            foreach($prow['citys'] as $crow){
                $aCities[$crow['name']] = [
                    0 => $prow['province'],
                    1 => $crow['city'],
                ];

            }

        }
        // $aCities = [
        //     '长沙' => [19, 351],
        //     '合肥' => [19, 351],
        // ];

        foreach ($aCities as $sCityName => $data) {
            $redis->hSet($platcache, $sCityName, json_encode($data)); 
        }
// , , 湖南长沙

    }
}