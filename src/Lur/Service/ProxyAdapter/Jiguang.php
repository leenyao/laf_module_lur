<?php

class Jiguang{
	
    // 一个账号X个ADR共用的数量
    const ACCGROUPSTEP=1;

    // 常驻的城市
    static function keepcities(){
        // return ["北京", "湖南省","河南省"];
        return ["北京"];
    }
    
    static function cities(){
                // $cachekey = LC_PROXY_CITYLIST_838.__CLASS__;
        // $cities = ["北京", "温州", "徐州", "郑州", "昆明", "青岛", "沈阳", "重庆", "合肥", "金华","舟山","宁波", "上海", "贵阳"];
        $cities = ["北京", "杭州", "徐州", "郑州", "昆明","台州","嘉兴","宁波", "金华", "湖州"];

        // var_dump($cachekey);
        return $cities;

        // return ["重庆","上海",'杭州',"绍兴","金华","衢州","丽水","湖州","舟山","温州","台州","嘉兴","宁波"];
    }
    // 处理不同的code, 判断是否需要更新
    static function monitorcodes($rowcode){
        $ok = false;
        if (count($rowcode)) {
            foreach($rowcode as $code=>$count){
                switch(strtoupper($code)){
                    case "115": //无该地区ip
                        if($count > 50){
                            $ok = true;
                        }
                    break;
                    case "121": //该套餐今天已经消耗完毕
                    break;
                }
            }
        }
        return $ok;
    }

    // 绑定配置
	static function band( $city, $acconf, $locationconf){

        $data = [
            "whiteurl" => "http://webapi.jghttp.alicloudecs.com/index/index/save_white?neek=37619&appkey=d4ac972b82717c2f2176654fb6087934&white=[IP]",
            "url" => "http://d.jghttp.alicloudecs.com/getip?num=3&type=2&pro=&city=&yys=0&port=11&pack=[pack]&ts=1&ys=0&cs=0&lb=1&sb=0&pb=45&mr=1&regions=",

        ];


        $data['url'] = str_replace('&pro=&', "&pro=".$locationconf[0]."&", $data['url']);
        $data['url'] = str_replace('&city=&', "&city=".$locationconf[1]."&", $data['url']);

        if (isset($acconf['replacement']) && count($acconf['replacement'])) {
            foreach($acconf['replacement'] as $rk => $rv){
                $data['whiteurl'] = str_replace($rk, $rv, $data['whiteurl']);
                $data['url'] = str_replace($rk, $rv, $data['url']);
            }
        }

        return $data;

	}

    // 初始化地域
    static function initLocation($platcache, $aCities, $redis){

        $url = 'http://webapi.jghttp.alicloudecs.com/index/api/get_all_area_map';
        $i = 0;
        $ageo2id = [];
        $aresut = [];
        $aprovince2id = [];
        $contents = file_get_contents($url);
        $a1 = json_decode($contents, 1);
        if (is_array($a1) && isset($a1['code']) && $a1['code']==1 && isset($a1['ret_data']) && count($a1['ret_data'])){
            // print_r($a1);
            foreach($a1['ret_data'] as $row1){
                if(isset($row1['date']) && count($row1['date'])){
                    foreach($row1['date'] as $row2){
                        $key1 = $row1['name'].'-'.$row2['name'];
                        $keyprovince = $row1['name'];
                        if(!isset($aprovince2id[$keyprovince])){
                            $aprovince2id[$keyprovince] = [$row1['id'], 0];
                        }
                        if(!isset($ageo2id[$key1])){
                            $ageo2id[$key1] = [$row1['id'], $row2['id']];
                        }
                    }
                }
            }
        }
        foreach($aprovince2id as $keyprovince => $row1){
            $redis->hSet($platcache, $keyprovince, json_encode($row1)); 
        }

        foreach($ageo2id as $location => $row1){
            foreach ($aCities as $sCityName) {
                $sCityName = trim($sCityName);
                if (empty($sCityName)  || strlen($sCityName)<=3) {
                    continue;
                }
                    // echo "@@@".$sCityName."\n";
                if (!isset($aresut[$sCityName]) && preg_match("/$sCityName/i", $location)) {
                    $redis->hSet($platcache, $sCityName, json_encode($row1)); 
                }

            }

        }
    }
}