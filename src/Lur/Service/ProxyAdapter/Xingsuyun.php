<?php
// 星速云http://xingsuyun.clbiz66.cn/
class Xingsuyun{
    // 一个账号X个ADR共用的数量
    const ACCGROUPSTEP=2;

    static function keepcities(){
        // return ["宁波", "北京", "上海", "广州", "深圳"];
        return ["青岛", "成都", "重庆", "宁波", "汕头"];

	}
    static function cities(){

        return self::keepcities();
    }
    // 处理不同的code, 判断是否需要更新
    static function monitorcodes($rowcode){
        $ok = LC_PROXY_MONITORCODE_FALSE;
        if (count($rowcode)) {
            // foreach($rowcode as $code=>$count){
            //     switch(strtoupper($code)){
            //         case "115": //无该地区ip
            //             if($count > 50){
            //                 $ok = LC_PROXY_MONITORCODE_NEEDUPDATE;
            //             }
            //         break;
            //         case "121": //您的该套餐已经过期了！  
            //         case "116": //该套餐今天已经消耗完毕
            //                 $ok = LC_PROXY_MONITORCODE_EXPIRED;
            //         break;
            //         case "114": //账户金额消耗完毕
            //         break;
            //     }
            // }
        }
        return $ok;
    }

    // 绑定配置
	static function band( $city, $acconf, $locationconf){
        $data = [
            "whiteurl" => "",
            // 包月套餐
            "url" => "http://user.xingsudaili.com:25434/jeecg-boot/extractIp/s?uid=[uid]&orderNumber=[orderNumber]&number=10&wt=json&randomFlag=false&detailFlag=true&useTime=80&region=[region]",
        ];
// http://webapi.http.zhimacangku.com/getip?num=1&type=2&pro=330000&city=330109&yys=0&port=1&time=1&ts=1&ys=1&cs=1&lb=1&sb=0&pb=4&mr=2&regions=
//pack= 169986
        $data['url'] = str_replace('[region]', urlencode($locationconf[1]."市"), $data['url']);
        // var_dump($acconf);
        if (isset($acconf['replacement']) && count($acconf['replacement'])) {
            foreach($acconf['replacement'] as $rk => $rv){
                $data['whiteurl'] = str_replace($rk, $rv, $data['whiteurl']);
                $data['url'] = str_replace($rk, $rv, $data['url']);

            }
        }
        return $data;

	}

    // 初始化地域
    static function initLocation($platcache, $aCities, $redis){

        $aCities = self::keepcities();
        foreach($aCities as $sCityName){        
            $row1 = [$sCityName,$sCityName];
            $redis->hSet($platcache, $sCityName, json_encode($row1));
        }
    }
}