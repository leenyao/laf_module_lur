<?php

class Jinglin{
	
    // 一个账号X个ADR共用的数量
    const ACCGROUPSTEP=1;

    // 常驻的城市
    static function keepcities(){
        return ["北京"];
    }
    
    static function cities(){

        // var_dump($cachekey);
        return self::keepcities();

        // return ["重庆","上海",'杭州',"绍兴","金华","衢州","丽水","湖州","舟山","温州","台州","嘉兴","宁波"];
    }
    // 处理不同的code, 判断是否需要更新
    static function monitorcodes($rowcode){
        $ok = false;
        // if (count($rowcode)) {
        //     foreach($rowcode as $code=>$count){
        //         switch(strtoupper($code)){
        //             case "115": //无该地区ip
        //                 if($count > 50){
        //                     $ok = true;
        //                 }
        //             break;
        //             case "121": //该套餐今天已经消耗完毕
        //             break;
        //         }
        //     }
        // }
        return $ok;
    }

    // 绑定配置
	static function band( $city, $acconf, $locationconf){

        $data = [
            "whiteurl" => "http://www.jinglingdaili.com/Users-whiteIpAddNew.html?appid=[appid]&appkey=[appkey]&type=dt&whiteip=[IP]&index=[i]",
            "url" => "http://ip.ipjldl.com/index.php/api/entry?method=proxyServer.generate_api_url&packid=[packid]&fa=0&fetch_key=&groupid=0&qty=5&time=1&pro=[pro]&city=[city]&port=1&format=json&ss=5&css=&ipport=1&et=1&dt=0&specialTxt=3&specialJson=&usertype=2",

        ];


        $data['url'] = str_replace('[pro]', urlencode($locationconf[0]), $data['url']);
        $data['url'] = str_replace('[city]', urlencode($locationconf[1]), $data['url']);
        $data['whiteurl'] = str_replace('[i]', ($acconf['id']%2)+1, $data['whiteurl']);

        if (isset($acconf['replacement']) && count($acconf['replacement'])) {
            foreach($acconf['replacement'] as $rk => $rv){
                $data['whiteurl'] = str_replace($rk, $rv, $data['whiteurl']);
                $data['url'] = str_replace($rk, $rv, $data['url']);
            }
        }

        return $data;

	}

    // 初始化地域
    static function initLocation($platcache, $aCities, $redis){

        $maps = [
            '北京' => ["北京直辖市", ""],
        ];
        $aCities = self::keepcities();
        foreach($aCities as $sCityName){        
            $row1 = $maps[$sCityName];
            $redis->hSet($platcache, $sCityName, json_encode($row1));
        }

    }
}