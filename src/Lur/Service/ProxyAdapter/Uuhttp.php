<?php

class Uuhttp{
	/*
    该供应商为固定一个用户名, 他们在后台开不同城市的线路
    */
    // 一个账号X个ADR共用的数量
    const ACCGROUPSTEP=8;

    // 只允许拨号的城市
    static function cities(){
        // 南京,,,苏州,,,大连,北京,天津,济南  合肥,    杭州   厦门, 武汉 , 合肥
        // return ["苏州","大连","北京","天津","济南","南京"];

        // return ["成都","昆明","沈阳","石家庄","徐州","长沙","郑州"];
        // return ["北京","重庆","广州","杭州","徐州","大连","苏州","天津","济南"];
        return ["北京","重庆","广州","徐州","大连","苏州","天津","济南"];
        // 济南,南京,天津
  
    }

    // 常驻的城市
    static function keepcities(){
        // 南京,,,苏州,,,大连,北京,天津,济南  合肥,    杭州   厦门, 武汉 , 合肥
        // return ["苏州","大连","北京","天津","济南","南京"];

        // return ["成都","昆明","沈阳","石家庄","徐州","长沙","郑州"];
        // return ["北京","重庆","广州","杭州","徐州","大连","苏州","天津","济南"];
        return ["北京","重庆","广州","徐州","大连","苏州","天津","济南"];
        // 济南,南京,天津
    }

    // 处理不同的code, 判断是否需要更新
    static function monitorcodes($rowcode){
        $ok = false;
        // if (count($rowcode)) {
        //     foreach($rowcode as $code=>$count){
        //         switch(strtoupper($code)){
        //             case "9999": //无该地区ip
        //                 if($count > 50){
        //                     $ok = true;
        //                 }
        //             break;
        //         }
        //     }
        // }
        return $ok;
    }

    // 绑定配置
	static function band( $city, $acconf, $locationconf){

        $data = [
            "whiteurl" => "",
            // "url" => "http://api7.uuhttp.com:39007/index/api/return_data?area=&mode=http&count=400&b_time=120&return_type=2&line_break=6&ttl=1&balance=0&secert=MTg1MTk4OTUwNjE6ODNiMDZiZWE3M2FjZmFkNTAwZGY4NWFjNzczNGQ0ZTg=",
            "url" => "http://api5.uuhttp.com:39005/index/api/return_data?area=&mode=http&count=5&b_time=30&return_type=2&line_break=6&ttl=1&balance=0&secert=[secert]",

            // http://api5.uuhttp.com:39005/index/api/return_data?area=12&mode=http&count=13&b_time=30&return_type=2&line_break=6&balance=0&secert=MTU2MTg2MTYxMTM6MjAwODIwZTMyMjc4MTVlZDE3NTZhNmI1MzFlN2UwZDI=
        ];
        if (isset($acconf['replacement']) && count($acconf['replacement'])) {
            foreach($acconf['replacement'] as $rk => $rv){
                $data['whiteurl'] = str_replace($rk, $rv, $data['whiteurl']);
                $data['url'] = str_replace($rk, $rv, $data['url']);
            }
        }
        $data['url'] = str_replace('area=&', "area=".intval($locationconf[1])."&", $data['url']);

        return $data;

	}

    // 初始化地域
    static function initLocation($platcache, $aCities, $redis){
        // $mapss = file_get_contents(__DIR__."/uuhttp.json");

        $url = 'http://wapi.http.cnapi.cc/index/api/get_all_area_map';
        $i = 0;
        $ageo2id = [];
        $aresut = [];
        $contents = file_get_contents($url);
        $a1 = json_decode($contents, 1);
        if (is_array($a1) && isset($a1['code']) && $a1['code']==1 && isset($a1['ret_data']) && count($a1['ret_data'])){
            // print_r($a1);
            foreach($a1['ret_data'] as $row1){
                if(isset($row1['date']) && count($row1['date'])){
                    foreach($row1['date'] as $row2){
                        $key1 = $row1['name'].'-'.$row2['name'];
                        if(!isset($ageo2id[$key1])){
                            $ageo2id[$key1] = [$row1['id'], $row2['id']];
                        }
                    }
                }
            }
        }

        // 列表中没有的, 手动增加城市
        $ageo2id["徐州"] = ["3200", "3203"];
        $ageo2id["天津"] = ["12", "12"];

        foreach($ageo2id as $location => $row1){
            foreach ($aCities as $sCityName) {
                $sCityName = trim($sCityName);
                if (empty($sCityName)  || strlen($sCityName)<=3) {
                    continue;
                }
                    // echo "@@@".$sCityName."#".$location."\n";
                if ( preg_match("/$sCityName/i", $location)) {

                    if(preg_match("/北京/i", $sCityName)){
                        $row1 = ["11", "11"];
                    }elseif (preg_match("/天津/i", $sCityName)) {
                        $row1 = ["12", "12"];
                    }elseif (preg_match("/重庆/i", $sCityName)) {
                        $row1 = ["50", "50"];
                    }elseif (preg_match("/大连/i", $sCityName)) {
                        $row1 = ["210000", "210200"];
                    }elseif (preg_match("/广州/i", $sCityName)) {
                        $row1 = ["440000", "440100"];
                    }elseif (preg_match("/杭州/i", $sCityName)) {
                        $row1 = ["3300", "3301"];
                    }elseif (preg_match("/徐州/i", $sCityName)) {
                        $row1 = ["3200", "3203"];
                    }elseif (preg_match("/苏州/i", $sCityName)) {
                        $row1 = ["3200", "3205"];
                    }elseif (preg_match("/济南/i", $sCityName)) {
                        $row1 = ["3700", "3701"];
                    }

        // return ["北京","重庆","广州","深圳",  徐州, "杭州","","大连","苏州","天津","济南"];


                    $redis->hSet($platcache, $sCityName, json_encode($row1)); 
                }

            }

        }
    }


//         $datas = json_decode(trim($mapss), 1);

//         $aCities = [];
//         foreach($datas as $crow){
//             $aCities[$crow['name']] = [
//                 0 => "",
//                 1 => $crow['city'],
//             ];

//         }

//         // $aCities = [
//         //     '长沙' => [19, 351],
//         //     '合肥' => [19, 351],
//         // ];

//         foreach ($aCities as $sCityName => $data) {
//             $redis->hSet($platcache, $sCityName, json_encode($data)); 
//         }
//         return $aCities;
// // , , 湖南长沙

//     }
}