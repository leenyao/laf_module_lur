<?php

class Zdopen{
    /**
     * 该供应商 每条线路只允许两个白名单 
     */

    // 一个账号X个ADR共用的数量
    const ACCGROUPSTEP=2;

    static function keepcities(){
        return [
            // "湖南省", "浙江省"
            "上海", "北京"
            , "广州", "重庆"
        ];
    }
    static function cities(){
        return self::keepcities();
    }

    // 处理不同的code, 判断是否需要更新
    static function monitorcodes($rowcode){
        $ok = LC_PROXY_MONITORCODE_FALSE;
        if (count($rowcode)) {
            // var_dump($rowcode);
            foreach($rowcode as $code=>$count){
                switch(strtoupper($code)){
                    case "12009": //无该地区ip
                        if($count > 50){
                            $ok = LC_PROXY_MONITORCODE_NEEDUPDATE;
                        }
                    break;
                    case "121": //您的该套餐已经过期了！  
                    case "116": //该套餐今天已经消耗完毕
                        $ok = LC_PROXY_MONITORCODE_EXPIRED;
                    break;
                    case "114": //账户金额消耗完毕
                    break;
                }
            }
        }
        return $ok;
    }
// 202108050104384336,7216bcf0f19b653f
    // 绑定配置
	static function band( $city, $acconf, $locationconf){
        $data = [
            "whiteurl" => "http://www.zdopen.com/ShortProxy/BindIP/?api=[api]&akey=[akey]&i=[i]",
            "url" => "http://www.zdopen.com/ShortProxy/GetIP/?api=[api]&akey=[akey]&count=5&adr=[adr]&timespan=2&type=3",

        ];

        $data['url'] = str_replace('[adr]', urlencode($locationconf[0]), $data['url']);

        // 供应商只允许一个账号放2个白名单, 所以将i取模
        $data['whiteurl'] = str_replace('[i]', ($acconf['id']%2)+1, $data['whiteurl']);

        if (isset($acconf['replacement']) && count($acconf['replacement'])) {
            foreach($acconf['replacement'] as $rk => $rv){
                $data['whiteurl'] = str_replace($rk, $rv, $data['whiteurl']);
                $data['url'] = str_replace($rk, $rv, $data['url']);

            }
        }

        return $data;

	}

    // 初始化地域
    static function initLocation($platcache, $aCities, $redis){
        // $aCities = self::keepcities();
        foreach($aCities as $sCityName){        
            $row1 = [$sCityName,$sCityName];
            $redis->hSet($platcache, $sCityName, json_encode($row1));
        }

    } 

}