<?php
namespace Lur\Form;
class Adschedule extends \Application\Form\Common{

    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);
		$sTableSource = "adschedule";

        // $aUserId = \Application\Model\Common::getResourceList($this->getServiceManager(), array('source'=>'adcustomer', 'pkfield'=>'id', 'labelfield'=>'name'), array('id'=>array('$gt'=>1000)));

                // var_dump($sm->get('zfcuser_auth_service')->getIdentity());

        $aUserId = \Application\Model\Common::getResourceAllDataKV($sm, 'adcustomer', array("user_id"=>array('$in'=>array(0, $sm->get('zfcuser_auth_service')->getIdentity()->getId()))), array('id','name'));



        $aM1007 = \Application\Model\Common::getResourceMetadaList($this->getServiceManager(), 1007);
        $aM1008 = \Application\Model\Common::getResourceMetadaList($this->getServiceManager(), 1008);
        $aM1009 = \Application\Model\Common::getResourceMetadaList($this->getServiceManager(), 1009);
        $aM1014 = \Application\Model\Common::getResourceMetadaList($this->getServiceManager(), 1014);
        $aM1015 = \Application\Model\Common::getResourceMetadaList($this->getServiceManager(), 1015);
        // var_dump($aM1014);
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
            "user_id" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab'),
				'options' => array(
                    'value_options' => $aUserId,
				),
			),
			"code" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"uv2ndurl" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),

			'm1014_id' => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab'),
				'options' => array(
                    'value_options' => $aM1014,
				),
			),
            "sspctr" => array( 
                'type' => 'text',
                'attributes' => array('type'=>'text'),
                'options' => array('tips'=>""),
            ),
			"stable_cookie_percent" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"domaincookie_delete_percent" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"code_label" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
			"referer" => array( 
				'type' => 'textarea',
				'attributes' => array('rows'=>'10'),
				'options' => array('tips'=>""),
			),
            "relation___1007" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab', 'multiple'=>1),
				'options' => array(
                    'value_options' => $aM1007,
				),
			),
            "relation___1008" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab', 'multiple'=>1),
				'options' => array(
                    'value_options' => $aM1008,
				),
			),
            "relation___1009" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab', 'multiple'=>1),
				'options' => array(
                    'value_options' => $aM1009,
				),
			),
			'relation___1015' => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab', 'multiple'=>1),
				'options' => array(
                    'value_options' => $aM1015,
				),
			),
			"schedule" => array( 
				'type' => 'textarea',
				'attributes' => array('rows'=>'10'),
				'options' => array('tips'=>""),
			),

		);
        $this->aFormElement['Submitcancel'] = $this->fmtSubmitButton('adcustomer');
        
		$this->_fmtFormElements(__CLASS__);
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
		if(isset($aData['id'])){
			$aRelation1007 = \Application\Model\Common::getRelationList($this->getServiceManager(),1007, $aData['id']);
			$aRelation1008 = \Application\Model\Common::getRelationList($this->getServiceManager(),1008, $aData['id']);
			$aRelation1009 = \Application\Model\Common::getRelationList($this->getServiceManager(),1009, $aData['id'], false, true);
			$aRelation1015 = \Application\Model\Common::getRelationList($this->getServiceManager(),1015, $aData['id']);
            $aFormElement['relation___1007']['attributes']['value'] = $aRelation1007;
            $aFormElement['relation___1008']['attributes']['value'] = $aRelation1008;
            $aFormElement['relation___1009']['attributes']['value'] = $aRelation1009;
            $aFormElement['relation___1015']['attributes']['value'] = $aRelation1015;
        }
        return $aFormElement;
    }
}