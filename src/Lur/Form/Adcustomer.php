<?php
namespace Lur\Form;
class Adcustomer extends \Application\Form\Common{

    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);
    	/*
    	
    	$aM1201 = \Application\Model\Common::getResourceMetadaList($sm, 1201);
    	
    	 */
		$this->aFormElement = array(
			"id" => array( 
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
			"name" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
            /*

            "user_id" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab'),
				'options' => array(
                    'value_options' => $aUserId,
				),
			),
			"start" => array( 
				'type' => 'date',
				 'attributes' => array('class'=>'date-picker'),
				'options' => array('table'=>$sTableSource, 'format' => 'Y-m-d',),
			),
			"content" => array( 
                'type' => 'textarea',
                'attributes' => array( 'class'=>'summernote','required' => 'required'),
                'options' => array('helpname'=>'wysiwyg'),
			),
			"code" => array( 
				'type' => 'text',
				'attributes' => array('type'=>'text'),
				'options' => array('tips'=>""),
			),
            "relation___1007" => array( //1:n 关系数据模式
				'type' => 'select',
				'attributes' => array('class'=>'select-tab', 'multiple'=>1),
				'options' => array(
                    'value_options' => $aM1007,
				),
			),    
			"in_file" => array( 
				'type' => 'file',
				'params' => array('id'=>'in_file'),
				'attributes' => array('data-filetype'=>'csv'),
				'options' => array('helpname'=>'fileupload'),
			),        
            */

		);
        $this->aFormElement['Submitcancel'] = $this->fmtSubmitButton('adcustomer');
		$this->_fmtFormElements(__CLASS__);
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }
	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
		if(isset($aData['id'])){//特殊处理逻辑
        }
        return $aFormElement;
    }
}