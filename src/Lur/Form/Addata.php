<?php
namespace Lur\Form;
class Addata extends \Application\Form\Common{

    public function _getTaskType($sm){
		$aTaskType = \Application\Model\Common::getResourceMetadaList($sm, 110);
		return $aTaskType;
	}

    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);
		$sTableSource = "addata";

//		var_dump($aLocations);
        $aM1005 = \Application\Model\Common::getResourceMetadaList($this->getServiceManager(), 1005);
		$this->aFormElement = array(
			"id" => array(
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('tips'=>""),
			),
            "user_id" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab'),
				'options' => array(
                    'value_options' => $aM1005,
				),
			),
            "m1005_id" => array( //所属项目
				'type' => 'select',
				'attributes' => array('class'=>'select-tab'),
				'options' => array(
                    'value_options' => $aM1005,
				),
			),
			"datapath" => array( 
				'type' => 'file',
				'params' => array('id'=>'file'),
				'attributes' => array('data-filetype'=>'csv'),
				'options' => array('table'=>$sTableSource, 'helpname'=>'fileupload'),
			),
		    "Submitcancel" => array(
		        'type' => 'button',
		        'options' => array(
		            'table'=>$sTableSource,
		            'helpname'=>'Submitcancel',
		        ),
		    ),
		);
		$this->_fmtFormElements(__CLASS__);

        // we want to ignore the name passed
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }

	public function _fmtElement4Data($aData=array(), $aFormElement){
		$aFormElement = parent::_fmtElement4Data($aData, $aFormElement);
        if(isset($aData['id'])){
            $aM1007 = \Application\Model\Common::getResourceMetadaList($this->getServiceManager(), 1007, $aData['id'], 1, true);
            if(count($aM1007)){
                $aFormElement['metadata_1007']['attributes']['data-selected'] = json_encode($aM1007[$aData['id']]);
            }
        }
		return $aFormElement;
	}
}