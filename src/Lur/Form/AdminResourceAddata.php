<?php

namespace Lur\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

use Zend\Form\Form;
use Zend\Form\Element;

class AdminResourceAddata extends \Application\Form\Common
{
    public function _getTaskType($sm){
		$aTaskType = \Application\Model\Common::getResourceMetadaList($sm, 110);
		return $aTaskType;
	}

    public function __construct($oController)
    {
		$sm = $oController->getServiceLocator();
		$this->setController($oController,__CLASS__);
		$this->setServiceManager($sm);
		$sTableSource = "addata";

//		var_dump($aLocations);
		$this->aFormElement = array(
			"id" => array(
				'type' => 'hidden',
				'attributes' => array('type'=>'hidden'),
				'options' => array('table'=>$sTableSource,'tips'=>""),
			),
//            "user_id" => array( //所属项目
//				'type' => 'select',
//				'attributes' => array('class'=>'select-tab'),
//				'options' => array('table'=>$sTableSource,
//                'value_options' => \Application\Model\Common::getResourceList($sm,array('source'=>'user','pkfield'=>'id','labelfield'=>'username'), array('id'=>array('$gte'=>1000))),
//				),
//			),
			"datapath" => array( 
				'type' => 'file',
				'params' => array('id'=>'file'),
				'attributes' => array('data-filetype'=>'csv'),
				'options' => array('table'=>$sTableSource, 'helpname'=>'fileupload'),
			),
		    "Submitcancel" => array(
		        'type' => 'button',
		        'options' => array(
		            'table'=>$sTableSource,
		            'helpname'=>'Submitcancel',
		        ),
		    ),
		);
		$this->_fmtFormElements(__CLASS__);

        // we want to ignore the name passed
        parent::__construct(str_replace("\\", "_", __CLASS__));
        $this->setAttribute('method', 'post');
    }


}