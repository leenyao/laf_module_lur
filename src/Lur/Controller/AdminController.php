<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Lur\Controller;



use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminController extends \Application\Controller\LafController
{
	public function vpnacceditAction(){
		$aViewParams = array();

		if (isset($_POST) && count($_POST)) {

			if (isset($_REQUEST['vpnstore'])) {
				if ($_REQUEST['vpnstore'] == 'xx') {
				  $_REQUEST['vpnstore'] = 'xx2';
				}
				if ($_REQUEST['vpnstore'] == 'jk') {
				  $_REQUEST['vpnstore'] = 'xx3';
				}
				
			}



			$oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave
			if (isset($_REQUEST['id'])) {
				$sSql = "update vpn_accounts set status=:status, platform=:platform, vpnstore=:vpnstore, account=:account, password=:password, disabledate=:disabledate, memo=:memo where id=:id";
				
				$oSth = $oPdo->prepare($sSql);
				$oSth->execute(array(
				    ":platform" => $_REQUEST['platform'],
				    ":status" => $_REQUEST['status'],
				    ":vpnstore" => $_REQUEST['vpnstore'],
				    ":account" => $_REQUEST['account'],
				    ":password" => $_REQUEST['password'],
				    ":disabledate" => $_REQUEST['disabledate'],
				    ":memo" => $_REQUEST['memo'],
				    ":id" => $_REQUEST['id'],
				));

			}else{
				// $sSql = "insert into vpn_accounts set status=:status, vpnstore=:vpnstore, account=:account, password=:password, disabledate=:disabledate, memo=:memo ";
				$_REQUEST['account'] = preg_replace("/\s+/", "", $_REQUEST['account']);
				$aRowTmp4 = [];
				if (preg_match("/\[\d+-\d+\]/i", $_REQUEST['account'])) {
					preg_match_all("/^([^\[]*)\[(\d+)-(\d+)\]/i", $_REQUEST['account'], $matches);
					for ($i=$matches[2][0];$i <= $matches[3][0] ; $i++) { 
						$num=str_pad($i, $_POST['addzero'] ,"0",STR_PAD_LEFT);   
						$aRowTmp4[] = $matches[1][0].$num;

					}
				}else{
					$aRowTmp4[] = $_REQUEST['account'];
				}

				foreach ($aRowTmp4 as $account) {

					$sSql = "replace into vpn_accounts set status=:status, platform=:platform, vpnstore=:vpnstore, account=:account, password=:password, disabledate=:disabledate, memo=:memo ";
					
					$oSth = $oPdo->prepare($sSql);
					$oSth->execute(array(
					    ":platform" => $_REQUEST['platform'],
					    ":status" => $_REQUEST['status'],
					    ":vpnstore" => $_REQUEST['vpnstore'],
					    ":account" => $account,
					    ":password" => $_REQUEST['password'],
					    ":disabledate" => $_REQUEST['disabledate'],
					    ":memo" => $_REQUEST['memo'],
					));
				}

			}

			
// exit;
			$this->redirect()->toRoute('lur/default', array('controller'=>'admin', 'action'=>'vpnacc'));
		}
		$oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave

		$sSql = "select distinct vpnstore from vpn_hosts ";
		$oSth = $oPdo->prepare($sSql);
		$oSth->execute(array(
		));
		$aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
		$aViewParams['vpnstore'] = $aResultTmp;

		if (isset($_REQUEST['id'])) {
			$aViewParams['vpn_accounts'] = \Application\Model\Common::getResourceById($this->serviceManager, 'vpn_accounts', $_REQUEST['id']);
			unset($aViewParams['vpn_accounts']['id']);
			unset($aViewParams['vpn_accounts']['created_time']);
			unset($aViewParams['vpn_accounts']['modified']);
			
		}else{
			$aViewParams['vpn_accounts'] = array(
				'platform' => '',
				'status' => '',
				'vpnstore' => '',
				'account' => '',
				'password' => '',
				'disabledate' => '',
				'memo' => '',
			);
		}

		return new ViewModel($aViewParams);

	}

	
	public function extendbatchAction(){

		$oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave
		$sSql = "select distinct vpnstore from vpn_hosts ";
		$oSth = $oPdo->prepare($sSql);
		$oSth->execute(array(
		));
		$aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
		$aViewParams['vpnstore'] = $aResultTmp;

		$accounts = [];
		$aViewParams['targetacc'] = [];
		if ($_POST && isset($_POST['account'])) {

			if (isset($_REQUEST['vpnstore'])) {
				if ($_REQUEST['vpnstore'] == 'xx') {
				  $_REQUEST['vpnstore'] = 'xx2';
				}
				if ($_REQUEST['vpnstore'] == 'jk') {
				  $_REQUEST['vpnstore'] = 'xx3';
				}
				
			}

			$extendday = $_POST['extendday'];
			if(intval($_POST['extendday']) == 0)
				$extendday = $_POST['extendday_text'];

			// extendday
			// extendday_text

			$_REQUEST['account'] = preg_replace("/\s+/", "", $_REQUEST['account']);
			$aRowTmp4 = [];
			if (preg_match("/\[\d+-\d+\]/i", $_REQUEST['account'])) {
				preg_match_all("/^([^\[]*)\[(\d+)-(\d+)\]/i", $_REQUEST['account'], $matches);
				for ($i=$matches[2][0];$i <= $matches[3][0] ; $i++) { 
					$num=str_pad($i, $_POST['addzero'] ,"0",STR_PAD_LEFT);   
					$aRowTmp4[] = $matches[1][0].$num;
				}
				// var_dump($_POST['addzero']);
				// var_dump($aRowTmp4);
				// exit;

				foreach ($aRowTmp4 as $account) {
					$sSql = "select * from vpn_accounts where account=:account and vpnstore=:vpnstore";
					$oSth = $oPdo->prepare($sSql);
					$oSth->execute(array(
						':account' => $account,
						':vpnstore' => $_REQUEST['vpnstore'],
					));
					$tmp = $oSth->fetch(\PDO::FETCH_ASSOC);

					if ($tmp) {
						$accounts[] = $tmp;

						// 更新
						if(isset($_POST['confirm'])){

							$sNewDate = date('Y-m-d', strtotime("+".$extendday." day", strtotime($tmp['disabledate'])));

							if (strtotime($sNewDate) >= time()) {
								$sSql = "update vpn_accounts set status=1, disabledate=:disabledate where id=:id";
								// var_dump($sNewDate, $tmp['id']);
								$oSth = $oPdo->prepare($sSql);
								$oSth->execute(array(
								    ":disabledate" => $sNewDate,
								    ":id" => $tmp['id']
								));
								// exit;
							}
						}
					}
				}
				if (count($accounts)) {
					$aViewParams['targetacc'] = $accounts;
				}
			}
		}

		if (isset($_POST['confirm'])) {
			$this->redirect()->toRoute('lur/default', array('controller'=>'admin', 'action'=>'vpnacc'));
		}

		return new ViewModel($aViewParams);
	}

	function fmtMgplanGrep($grep='')
	{
		$return = [];
		$return2 = [];
		if (!empty($grep)) {
			$greps = explode("\r\n", $grep);
			// $setups = [];
			if (count($greps)) {
				foreach ($greps as $line) {
					$line = trim($line);
					$line = strtolower($line);
					if (empty($line)) {
						continue;
					}
					$vars = explode("|", $line);
					$line2 = [];
					foreach ($vars as $var) {
						list($v1, $v2) = explode("=", $var);
						$v1 = trim($v1);
						$v2 = trim($v2);
						$line2[$v1] = $v2;
					}
					$return[] = $line2;
				}
			}
			if (count($return)) {
				foreach ($return as $k=> $row) {
					$row3= [];
					if (isset($row['p'])
							&&is_numeric($row['p'])
							&&intval($row['p']) >=0
							&&intval($row['p']) <=100
						) {
						$row3['p'] = intval($row['p']);
					}else{
						throw new \Exception('参数p错误, 必须存在且介于0-100之间');
					}
					if (isset($row['u']))  {
						$row3['u'] = [];
						$us = explode(",", $row['u']);
						foreach ($us as $ua) {
							$row3['u'][] = $ua = trim($ua);
							if (!in_array($ua, ['pc', 'phone', 'pad'])){
								throw new \Exception('参数u错误, 必须为 pc,phone,pad');
							}
						}

					}
					if (isset($row['d']))  {
						$dates2 = $this->_chkDateFormat($row['d']);
						if ($dates2===false){
							throw new \Exception('参数d错误, 必须为正确的日期模式');
						}
					}
					if (!empty($dates2)) {
						$row3['d'] = $dates2;
					}
					$return2[$k] = $row3;
				}

			}


		}
		return $return2;
	}

	function fmtDatereplace($grep='')
	{
		$return = [];
		if (!empty($grep)) {
			$greps = explode("\r\n", $grep);
			// $setups = [];
			if (count($greps)) {
				foreach ($greps as $line) {
					$line = trim($line);
					$line = strtolower($line);
					if (empty($line)) {
						continue;
					}
					if (preg_match("/^\d{4}-\d{2}-\d{2}\s+\d{2}-\d{2}$/i", $line)) {
						$return[] = $line;
					}else{
						throw new \Exception('日期格式错误');
					}

				}
			}
		}
		return $return;
	}
	public function _chkDateFormat($sDate)
	{
			if (empty($sDate)) {
				return "";
			}
	    $sNewDate = $sDate;
	    $sNewDate = trim($sNewDate);
	    $dates = explode(",", $sNewDate);
	    foreach ($dates as $sDateTmp2) {
	    	if(strtotime($sDateTmp2) === false) {
	    		return false;
	    	}
	    }
	    return count($dates)==1?[$dates[0], $dates[0]] : $dates;

	}      

	public function mgplaneditAction($plan=null, $percent=null, $status=0, $grep=null, $datereplace=null){
		$newplan = 0;
    if (strtolower(PHP_SAPI) == 'cli') {
    }else{
    	
		$aConfig =  $this->getServiceLocator()->get('config');
	    $op_user_id = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();
	    if (!in_array($op_user_id, $aConfig['mgplanpuers'])) {
	      throw new \Exception('inavailable action');
	    }

			if (isset($_GET['plan'])) {
				$plan = $_GET['plan'];
			}
			if ($_POST && isset($_POST['percent'])) {
				$percent = trim($_POST['percent']);
			}
			if ($_POST && isset($_POST['grep'])) {
				$grep = trim($_POST['grep']);
			}
			if ($_POST && isset($_POST['datereplace'])) {
				$datereplace = trim($_POST['datereplace']);
			}


	    $lastop =  $this->getServiceLocator()->get('Lur\Service\Common')->getLastHisOp($plan);
			$status = isset($lastop['status'])?$lastop['status']:0;
			$newplan = 1;

	    // 手动调整的单子
    }
		$redis = \Application\Model\Common::getCacheObject($this->serviceManager);

		$msg = "";
		try {

			if (!is_null($percent) && !is_null($percent)) {
				if (is_numeric($percent) && $percent>=0 && $percent<=100) {
				}else{
					throw new \Exception('错误的百分比:'.$percent);
				}
				$grep_configs = $this->fmtMgplanGrep($grep);
				$datereplace = $this->fmtDatereplace($datereplace);

				if ($plan == 202037) {
					// var_dump($grep_configs);
					// exit;
				}



				$redis->hSet(LAF_CACHE_HNSCHEDULEAPI_OPHIS_210.$op_user_id.$plan, time(), json_encode([
					'percent' => $percent,
					'status' => $status,
					'grep_configs' => $grep_configs,
					'grep' => $grep,
					'datereplace' => $datereplace,
					'applytime' => date("Y-m-d H:i:s"),
				]));


				if ($newplan==1) {
					if (!$redis->hExists(LAF_CACHE_HNSCHEDULEAPI_NEW_207.$op_user_id, $plan)) {
						// 若不是新增
						$newplan = 0;
					}
				}

				$redis->hDel(LAF_CACHE_HNSCHEDULEAPI_NEW_207.$op_user_id, $plan);
				$redis->hDel(LAF_CACHE_HNSCHEDULEAPI_STOP_208.$op_user_id, $plan);

				// if ($plan == 202037) {
					$this->saveplan2db($op_user_id, $plan, $percent, $grep_configs, $newplan, $status, $datereplace);
				// }

				$msg = "下单成功! 请到<批量导入排期>检查执行结果, 然后更新缓存";

			}

			$aViewParams = array(
				'lastpercent' => !is_null($percent) ? $percent:0,
				'lastgrep' => !is_null($grep) ? $grep:"",
				'datereplace' => !is_null($datereplace) ? $datereplace:"",
				'msg' => $msg,
				'ophis' => $redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_OPHIS_210.$op_user_id.$plan),
			);
			return new ViewModel($aViewParams);

		} catch (Exception $e) {
			
				echo " error.".$e->getMessage();
				exit();
		}
	}
	public function mgplanAction(){

	    $op_user_id = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();
		$aConfig =  $this->getServiceLocator()->get('config');
	    if (!in_array($op_user_id, $aConfig['mgplanpuers'])) {
	      throw new \Exception('inavailable action');
	    }

		$redis = \Application\Model\Common::getCacheObject($this->serviceManager);
		$aViewParams = array(
			'op_user_id' => $op_user_id,
			'data_his' => $redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_LIST_206.$op_user_id),
			'data_add' => $redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_NEW_207.$op_user_id),
			'data_stop' => $redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_STOP_208.$op_user_id),

		);
		return new ViewModel($aViewParams);
	}

	public function mgdetailAction(){
	    $op_user_id = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();
		$aConfig =  $this->getServiceLocator()->get('config');
	    if (!in_array($op_user_id, $aConfig['mgplanpuers'])) {
	      throw new \Exception('inavailable action');
	    }

		$plan = $_GET['plan'];
		$type = $_GET['type'];
		// mgdetail
		$detail = [];
		try {
			switch ($type) {
				case 'mg':
				default:
					$detail =  $this->chgDetail2Csv($op_user_id, 0, $plan, $type);
					break;
				case 'me':
					$lastop =  $this->serviceManager->get('Lur\Service\Common')->getLastHisOp($plan);
					$datereplace = [];
					if (isset($lastop['datereplace'])) {
						$datereplace = $lastop['datereplace'];
					}
					$detail =  $this->chgDetail2Csv($op_user_id, 0, $plan, $type, (isset($lastop['percent'])?($lastop['percent']):100), $lastop['grep_configs'], 0, $datereplace);
					break;
				case 'down':
					$lastop =  $this->serviceManager->get('Lur\Service\Common')->getLastHisOp($plan);
					$datereplace = [];
					if (isset($lastop['datereplace'])) {
						$datereplace = $lastop['datereplace'];
					}
					$detail =  $this->chgDetail2Csv($op_user_id, 0, $plan, $type, (isset($lastop['percent'])?$lastop['percent']:100), $lastop['grep_configs'], 0, $datereplace);
					break;
			}
			$aViewParams = array(
				'detail' => $detail,
			);
			return new ViewModel($aViewParams);

		} catch (Exception $e) {
			
				echo "plan error.".$e->getMessage();
				exit();
		}

	}

	function saveplan2db($op_user_id, $plan, $percent=100, $grep_configs=[], $newplan=0, $status=0, $datereplace=[]){


		$redis = \Application\Model\Common::getCacheObject($this->serviceManager);


		$todaykey = $plan."-".date("Ymd");
		// 当天的小时逻辑
		$todayhourkey = LAF_CACHE_HNSCHEDULEAPI_TODAYHOUT_212.$op_user_id;
		if ($newplan==1 && !$redis->hExists($todayhourkey, $todaykey)) {
			$redis->hSet($todayhourkey, $todaykey, date("H")."-23");
		}


		$details = $this->chgDetail2Csv($op_user_id, 1, $plan, "me", $percent, $grep_configs, $status, $datereplace);
		$aFiledata = [];
		foreach ($details as $row) {
			$aFiledata[] = '"'.join('","', $row).'"';
		}
		$dbfile = "/tmpupload/mg_".$plan."_".date("YmdHis").".csv";
		$savefile = "/app/public".$dbfile;
		file_put_contents($savefile, join("\n", $aFiledata));


    	$oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave

		// if ($plan != 202037) {
			$sSql = "insert into system_batch_jobs set m158_id=1, in_file='".$dbfile."', memo='".$plan."(API下单)', op_user_id=".$op_user_id;
			$oSth = $oPdo->prepare($sSql);
			$oSth->execute();
    // }
	}
// 

	function chgDetail2Csv($op_user_id, $saveflag, $plan, $type, $percent=100, $grep_configs=[], $status=0, $datereplace=[])
	{

		$aConfig =  $this->serviceManager->get('config');
		$mgconfig = $aConfig['mgplanconfig'][$op_user_id];

		$cncode2label = $aConfig['cncode2label'];

		if (in_array($type, ['mg'])) {
			$percent = 100;
		}elseif (in_array($type, ['me'])) {
			$percent = 100-$percent;
		}else{
			$percent = $percent;
		}

		$stop = $status==1 || (empty($percent)&&empty($grep_configs)) ? 1 : 0;

			$urls = [
			    'list' => 'http://cx.da.mgtv.com/planList?cxid='.$mgconfig['cxid'].'&key='.$mgconfig['key'].'',
			    'detail' => 'http://cx.da.mgtv.com/planDetail?cxid='.$mgconfig['cxid'].'&key='.$mgconfig['key'].'&plan=',
			];

	    $province2cities = $this->serviceManager->get('Lur\Service\Common')->province2cities();

	    foreach ($province2cities as $provincename => $row) {
	        $province2cities[$provincename."市"] = $row;
	    }
        $aIaCprovicesRes = $this->getServiceLocator()->get('\Lur\Service\Common')->getIacProvince();


	    $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave
	    $sSql = "select code, label, mapresid from `laf_lur`.`location_iaccode` group by code, label";
	    $oSth = $oPdo->prepare($sSql);
	    $oSth->execute([]);
	    $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
	    $aCityId2Resid = array();
	    $aCityId2Label = array();
	    foreach ($aResultTmp as $aRowTmp) {
	        $aCityId2Resid[$aRowTmp['code']] = $aRowTmp['mapresid'];
	        $aCityId2Label[$aRowTmp['code']] = $aRowTmp['label'];
	        foreach ($cncode2label as $cncode => $cnlabel) {
		        if ($cnlabel == $aRowTmp['label'] or $cnlabel == $aRowTmp['label']."市" ) {
			        $aCityId2Resid[$cncode] = $aRowTmp['mapresid'];
			        $aCityId2Label[$cncode] = $aRowTmp['label'];
		        }
	        }
	    }
	    
	    // print_r($aIaCprovicesRes);
	    // exit;
	    $contents_detail = file_get_contents($urls['detail'].$plan);
	    $c2_json = json_decode($contents_detail, 1);
	    if (isset($c2_json['status'])
	        && isset($c2_json['data'])
	        && isset($c2_json['data']['cxtype'])
	        && isset($c2_json['data']['task'])
	        && count($c2_json['data']['task'])
	    ) {
	        $aCsv = [];
	        $aDates = [];
	        foreach ($c2_json['data']['task'] as $row) {
	            foreach ($row['list'] as $row2) {

	                $resids = [];
	                $citylabel = "全国";
	                if (!empty($row2['ccode']) && in_array($row2['ccode'], [1156000000])) {
	                    
	                }else if(!isset($aCityId2Resid[$row2['ccode']]) || empty($aCityId2Resid[$row2['ccode']])) {

	                    if (isset($aCityId2Label[$row2['ccode']])
	                        && isset($province2cities[$aCityId2Label[$row2['ccode']]])
	                    ) {
	                    	// 存在该城市ip
	                        $cities = $province2cities[$aCityId2Label[$row2['ccode']]];

	                        $resids = array_keys($cities);
	                        $citylabel = $aCityId2Label[$row2['ccode']];
	                         // print_r($cities);

	                        
	                    }else{
	                    	if(isset($aCityId2Label[$row2['ccode']])){
		                    	if ( !isset($province2cities[$aCityId2Label[$row2['ccode']]])) {
		                    		// $resids[] = "没有该城市IP".$aCityId2Label[$row2['ccode']];
		                    		$newCityResid = $this->getServiceLocator()->get('\Lur\Service\Common')->saveIacMapping(intval($row2['ccode']));
		                    		if (empty($newCityResid)) {
			                    		$resids[] = intval($row2['ccode']);
			                    		// cncode2label
		                        	// throw new \Exception('bad ccode1:'.$row2['ccode'].$percent);
		                    		}else{
			                    		$resids[] = $newCityResid;
		                    		}
		                    	}else{
		                        // throw new \Exception('bad ccode2:'.$row2['ccode']);
			                        if ($saveflag==1) {
					                    	$resids[] = intval($row2['ccode']);
			                        }else{
					                    	$resids[] = $cncode2label[$row2['ccode']];
			                        }
		                    	}
	                    	}else{
							    $pmatched = false;
							    foreach($aIaCprovicesRes as $row13){

							    	if (preg_match('/'.$row13['label'].'/i', $cncode2label[$row2['ccode']]) || $row2['ccode']== $row13['code']){
								    	// print_r($row13);
								    	$pmatched = true;
								    	break;

							    	}
							    }
							    if($pmatched){
					                $resids[] = intval($row13['mapresid']);

							    }else{

			                        if ($saveflag==1) {
					                    	$resids[] = intval($row2['ccode']);
			                        }else{
					                    	$resids[] = $cncode2label[$row2['ccode']];
			                        }

							    }
	                    	}
                    	}
	                }else{
					    // $pmatched = false;
					    // print_r($row2['ccode']);
					    // print_r($cncode2label[$row2['ccode']]);
					    // foreach($aIaCprovicesRes as $row13){
					    // 	if (preg_match('/'.$row13['label'].'/i', $cncode2label[$row2['ccode']] || $row2['ccode']== $row13['code'])){
						   //  	print_r($row13);
						   //  	$pmatched = true;

					    // 	}
					    // }
					    // if($pmatched)
					    // exit;
	                    $resids[] = intval($aCityId2Resid[$row2['ccode']]);
	                    $citylabel = $aCityId2Label[$row2['ccode']];

	                }





	                $key = $citylabel."|-|".$plan."|-|".$row2['plt']."|-|".$row2['ccode']."|-|".json_encode($resids);
	                if (!isset($aCsv[$key])) {
	                    $aCsv[$key] = [];
	                }

	                if (!isset($aCsv[$key][$row['date']])) {
	                    // $aCsv[$key][$row['date']] =  $stop ? intval($row2['num']) : intval($row2['num']*$percent/100);
// var_dump($percent);
	                	$aCsv[$key][$row['date']] = $this->getNumByGrep($type, intval($row2['num']), $percent, $stop, $row['date'], strtolower($row2['plt']), $grep_configs);
	                    $aDates[] = $row['date'];

	                    // var_dump('--------');
	                    // var_dump([intval($row2['num']), $percent, $stop, $row['date'], strtolower($row2['plt']), $grep_configs]);
	                    // var_dump($aCsv[$key][$row['date']]);
	                }
	            }
	        }
	        $debug = 0;
	        return $this->_fmt2csv($mgconfig, $saveflag, $plan, $aCsv, $aDates, $stop, $debug, $c2_json['data']['note'], $datereplace);
	    }else{
	        throw new \Exception('bad detail contents:'.$contents_detail);
	    }
	}

	function getNumByGrep($type, $num, $defaultpercent, $stop, $date, $ua, $grep_configs){
		if (in_array($type, ['mg'])) {
			return intval($num);
		}
		// if ($stop) {
		// 	return intval($num);
		// }
		if (empty($grep_configs)) {
			return intval($num*$defaultpercent/100);
		}else{
			foreach ($grep_configs as $row) {
				$matcheds = [];
				$matcheds['d'] = 1; //日期判断
				$matcheds['u'] = 1; //ua判断

				if (
					isset($row['d']) 
					&& count($row['d'])
					&& (strtotime($date) < strtotime($row['d'][0]) ||  strtotime($date) > strtotime($row['d'][1]))
				) {
					$matcheds['d'] = 0;
				}

				if (
					isset($row['u']) 
					&& count($row['u'])
					&& !in_array($ua, $row['u'])
				) {
					$matcheds['u'] = 0;
				}
				// var_dump($row, $matcheds);
				if (array_sum($matcheds) == 2) {//满足条件
					$matchpercent = 0;
					if (in_array($type, ['me'])) {
						$matchpercent = (100-$row['p']);
					}elseif (in_array($type, ['down'])) {
						$matchpercent = $row['p'];
					}

					return intval($num*$matchpercent/100);
				}
			}
			// 没有命中任何条件, 走默认
			return intval($num*$defaultpercent/100);
		}
	}


	function _fmt2csv($mgconfig, $saveflag, $plan, $aCsv, $aDates, $stop, $debug, $note, $datereplace=[])
	{
	    $aDates = array_unique($aDates);
	    asort($aDates);
	    $aFiledata = [];
			$redis = \Application\Model\Common::getCacheObject($this->serviceManager);


	    // print_r($aDates);
	    // print_r($aCsv);

	    $rowHead = [];
	    $rowHead[0] = 'userid';
	    $rowHead[1] = 'codelabel';
	    $rowHead[2] = 'plan';
	    $rowHead[3] = '';
	    $rowHead[4] = '';
	    $rowHead[5] = 'UA';
	    $rowHead[6] = 'cities';
	    $rowHead[7] = '';
	    $rowHead[8] = '';
	    $rowHead[9] = 'cookie-frequence';
	    $rowHead[10] = 'show_activity';
	    $rowHead[11] = '';
	    $rowHead[12] = '';
	    $rowHead[13] = 'sourcetype';
	    $rowHead[14] = '';
	    $rowHead[15] = '';
	    $rowHead[16] = '';
	    $rowHead[17] = '';
	    $rowHead[18] = '';
	    $rowHead[19] = '';
	    $rowHead[20] = 'ssp-ctr';
	    $rowHead[21] = '';
	    $rowHead[22] = '';
	    $rowHead[23] = 'hunan-cxid';
	    $rowHead[24] = '';
	    $rowHead[25] = '';
	    $rowHead[26] = 'date-start-column';



	    foreach ($aDates as $date) {

    			$todaykey = $plan."-".date("Ymd", strtotime($date));
      		$todaystring = "";
    	    $todayhourkey = LAF_CACHE_HNSCHEDULEAPI_TODAYHOUT_212.$mgconfig['op_user_id'];
    	    if ($redis->hExists($todayhourkey, $todaykey)) {
    	    	$todaystring = $redis->hGet($todayhourkey, $todaykey);
    	    }

    	    $bmatch = false;
    	    foreach ($datereplace as $matchdate) {
    	    	if (preg_match("/^".$date."/", $matchdate)) {
    	    		// var_dump($matchdate,$date );
    	    		$bmatch = true;
			        $rowHead[] = $matchdate;


    	    	}
    	    }
    	    if ($bmatch == false) {
	    		// 当天的, 加上小时
	    		if (!empty($todaystring)) {
		        	$rowHead[] = $date." ".$todaystring;
	    		}else{
		        	$rowHead[] = $date;
	    		}
    	    }

	    }
	    // $aFiledata[0] = '"'.join('","', $rowHead).'"';
	    $aFiledata[0] = $rowHead;
	    foreach ($aCsv as $key => $row3) {
	        list($citylabel, $plan,$ua,$cityid,$city) = explode("|-|", $key);
	        if ($saveflag==0) {
	        	$aaa = json_decode($city, 1);
	        	$city = join(", ", $aaa)."/".$citylabel;
	        }
	        $newrow = [];
	        $ctr = \Application\Model\Common::getClickRateByMeo($note, strtolower($ua));
	        $uaid = "";
	        switch (strtolower($ua)) {
	            case 'pc':
	                // $ctr = 0.01;
	                $uaid = '[1]';
	                break;
	            case 'phone':
	                // $ctr = 0.035;
	                $uaid = '[2,3]';

	                break;
	            case 'pad':
	                // $ctr = 0.025;
	                $uaid = '[4]';
	                break;
	            case 'ott':
	                // $ctr = 0.025;
	                $uaid = '[23]';
	                break;
	        }

	        $newrow[0] = $mgconfig['user_id'];
	        // $newrow[1] = $citylabel.'-'.$ua;
	        $newrow[1] = strtolower($ua);
	        $newrow[2] = $plan;
	        $newrow[3] = '';
	        $newrow[4] = '';
	        $newrow[5] = $uaid;
	        $newrow[6] = $city=='[]' ? '' : $city;
	        $newrow[7] = '';
	        $newrow[8] = '';
	        $newrow[9] = 10;
	        $newrow[10] = $stop==1?2:1; // 是否激活
	        $newrow[11] = '';
	        $newrow[12] = '';
	        $newrow[13] = 5;
	        $newrow[14] = '';
	        $newrow[15] = '';
	        $newrow[16] = '';
	        $newrow[17] = '';
	        $newrow[18] = '';
	        $newrow[19] = '';
	        $newrow[20] = $ctr;
	        $newrow[21] = '';
	        $newrow[22] = '';
	        $newrow[23] = $mgconfig['cxid'];
	        $newrow[24] = '';
	        $newrow[25] = '';
	        $newrow[26] = ''; //date-start-column

	        
	        foreach ($aDates as $date) {
	            $count = isset($row3[$date]) ? $row3[$date] : 0;
	            $newrow[] = $count;
	        }
	        // $aFiledata[] = '"'.join('","', $newrow).'"';
	        $aFiledata[] = $newrow;
	    }

	    return $aFiledata;
	    // if ($debug) {
	    //     print_r($aFiledata);
	    // }
	    // $dbfile = "/tmpupload/mg_".$plan."_".date("YmdHis").".csv";
	    // $savefile = "/app/public".$dbfile;
	    // file_put_contents($savefile, join("\n", $aFiledata));
	    // echoMsg('['.__CLASS__.'] save mg file: '.$savefile);





	    
	    
	}



	public function vpnaccAction(){
		
		$sWhere = '';
		if (isset($_REQUEST['action']) && isset($_REQUEST['id']) && isset($_POST['count'])) {

			$oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave

			switch ($_REQUEST['action']) {
				case 'edit':

					$aViewParams = array(
					);
					return new ViewModel($aViewParams);
				break;
				case 'extend':

					$sSql = "select * from vpn_accounts where id=:id";
					$oSth = $oPdo->prepare($sSql);
					$oSth->execute(array(
					    ":id" => $_REQUEST['id'],
					));
					$aResultTmp = $oSth->fetch(\PDO::FETCH_ASSOC);
					if ($aResultTmp && count($aResultTmp)) {
						$sNewDate = date('Y-m-d', strtotime("+".$_POST['count']." day", strtotime($aResultTmp['disabledate'])));

						if (strtotime($sNewDate) >= time()) {
							$sSql = "update vpn_accounts set status=1, disabledate=:disabledate where id=:id";
							$oSth = $oPdo->prepare($sSql);
							$oSth->execute(array(
							    ":disabledate" => $sNewDate,
							    ":id" => $_REQUEST['id']
							));

							echo json_encode(array(
								'id' => $_REQUEST['id'],
								'disabledate' => $sNewDate,
							));
							exit;

						}
						
					}
				break;


				
				default:
					# code...
					break;
			}


			header('HTTP/1.1 500 Internal Server Error');
			echo '更新失败';
			exit;
		}

		if (isset($_REQUEST['action'])) {
			switch ($_REQUEST['action']) {
				case 'all':
				case 'fo100':
					$sWhere = '';
				break;
				case 'active':
					$sWhere = ' where status=1';
				break;
				case 'e3':
					$sWhere = " where status=1 and disabledate<='".date("Y-m-d",strtotime("+3 day"))."'";
				break;
				case 'e7':
					$sWhere = " where status=1 and disabledate<='".date("Y-m-d",strtotime("+7 day"))."'";
				break;
				case 'e15':
					$sWhere = " where status=1 and disabledate<='".date("Y-m-d",strtotime("+15 day"))."'";
				break;
				case 'unactive':
					$sWhere = ' where status<>1';
				break;


				
				default:
					# code...
					break;
			}
		}

		$oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave
		$sSql = "select * from vpn_accounts".$sWhere;
		$oSth = $oPdo->prepare($sSql);
		$oSth->execute(array(
		    // ":date" => date("Y-m-d", strtotime($sDate)),
		));
		$aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
		$aVpnaccounts = array();
		foreach ($aResultTmp as $aRowTmp) {
			$sKey = $aRowTmp['vpnstore']."-".$aRowTmp['status']."-".$aRowTmp['disabledate']."-".$aRowTmp['account']."-".$aRowTmp['id'];
		    $aVpnaccounts[$sKey] = $aRowTmp;
		}
		ksort($aVpnaccounts);
		// var_dump($aVpnaccounts);

		
        $aAdrSummary = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrSummary();
		
		$aViewParams = array(
			'aAdrSummary' => $aAdrSummary,
			'aVpnaccounts' => $aVpnaccounts,
		);
		return new ViewModel($aViewParams);
	}

	public function _getReportVarsFromCache($sm, $sReportDate){


		$aDateCode2Count = $aTmp2 = $aTmp3 = $aTmp4 = $aTmp5 = array();
		$aTmp =  array(array(), array(), array(), array(), array());

		$sCacheKey = LAF_CACHE_DASHBOARD_REPOT_321.$sReportDate;
		$aCacheData = \YcheukfCommon\Lib\Functions::getCache($sm, $sCacheKey);
		if (is_array($aCacheData)) {
			// return $aCacheData;
		}

		$aRowTmp2 = $sm->get('\Lur\Service\Common')->getReportDataFromCache($sReportDate, $sReportDate);;
		if (count($aRowTmp2)) {
			foreach($aRowTmp2 as $sKey2=>$sRowTmp3) {
				$result = [];
				$aRowTmp3 = json_decode($sRowTmp3, 1);
				$result['date'] = $sReportDate;
				list($result['adrserver_id'], $result['code_id'], $result['customerid'], $result['op_user_id']) = explode("/", $sKey2);
				$result['total'] = intval($aRowTmp3['c']);
				$result['total_sspad'] = isset($aRowTmp3['c_sad']) ? intval($aRowTmp3['c_sad']) : 0;
				$sServerLable = $result['adrserver_id'];
				$sCustomerLabel = $aRowTmp3['cl'];
				$sOpuserLabel = $aRowTmp3['ul'];


				if (!isset($aTmp[2][$sServerLable])) {
					$aTmp[2][$sServerLable] = array();
				}
				if (!isset($aTmp[2][$sServerLable][$sReportDate])) {
					$aTmp[2][$sServerLable][$sReportDate] = 0;
				}
				$aTmp[2][$sServerLable][$sReportDate] += $result['total'];

				if (!isset($aTmp[3][$sCustomerLabel])) {
					$aTmp[3][$sCustomerLabel] = array();
				}
				if (!isset($aTmp[3][$sCustomerLabel][$sReportDate])) {
					$aTmp[3][$sCustomerLabel][$sReportDate] = 0;
				}
				$aTmp[3][$sCustomerLabel][$sReportDate] += $result['total'];

				if (!isset($aTmp[4][$sOpuserLabel][$sReportDate])) {
					$aTmp[4][$sOpuserLabel][$sReportDate] = 0;
				}
				$aTmp[4][$sOpuserLabel][$sReportDate] += $result['total'];


				if (!isset($aDateCode2Count[$sReportDate])) {
					$aDateCode2Count[$sReportDate] = array();
				}
				$sCodeLabel = $sOpuserLabel.", ".$sCustomerLabel.", ".$result['code_id'];
				if (!isset($aDateCode2Count[$sReportDate][$sCodeLabel])) {
					$aDateCode2Count[$sReportDate][$sCodeLabel] = [
						'total' => 0,
						'total_sspad' => 0,
					];
				}
				$aDateCode2Count[$sReportDate][$sCodeLabel]['total'] += $result['total'];
				$aDateCode2Count[$sReportDate][$sCodeLabel]['total_sspad'] += $result['total_sspad'];
				

				$aTmp2[$sServerLable] = 0;
				$aTmp3[$sReportDate] = 0;
				$aTmp4[$sCustomerLabel] = 0;
				$aTmp5[$sOpuserLabel] = 0;

			}
		}
		$aReturn  = [$aTmp, $aDateCode2Count, $aTmp2, $aTmp3, $aTmp4, $aTmp5];
		if (date("Ymd") != $sReportDate) {
			\YcheukfCommon\Lib\Functions::saveCache($sm, $sCacheKey, $aReturn, 86400*7);
		}
		return $aReturn;
	}


	public function dashboardAction()
	{

// ... code ...

		$oPdo = (\Application\Model\Common::getPDOObejct($this->getServiceLocator(), 'db_slave'));
		$sDate1 = date("Y-m-d", strtotime("-7 day"));
		$eDate1 = date("Y-m-d");


		$aSspClickPercentData[LAF_ADSOURCETYPE_SSP_HUNAN] = $this->getServiceLocator()->get('\Lur\Service\Report')->clickPercentReport(LAF_ADSOURCETYPE_SSP_HUNAN, date("Y-m-d", strtotime("-3 day")), $eDate1, true);
		// print_r($aSspClickPercentData);
		// exit;




		$this->getServiceLocator()->get('\Lur\Service\Common')->flushCacheCounter2Db();
		//获取过去七天数据



		/**
		 * #######closure function start#######
		 */
		$fClosureRecentDayCounterFunc = function($oThis, $sDate1, $eDate1) use ($oPdo)
		{
			$sm = $oThis->getServiceLocator();
        	$aDates = $sm->get('\Lur\Service\Common')->getDateFromRange($sDate1, $eDate1);

			$aReturn = $aTmp2 = $aTmp3 = $aTmp4 = $aTmp5 = $aDateCode2Count = array();
			$aTmp = array(array(), array(), array(), array(), array());
			// foreach($oPDOStatement as $result) {
			foreach($aDates as $sReportDate) {

				list($t1, $t2, $t3, $t4, $t5, $t6) = $oThis->_getReportVarsFromCache($sm, $sReportDate);

				for ($ii=2; $ii <= 4 ; $ii++) { 
					if ($t1[$ii]) {
						foreach ($t1[$ii] as $k1 => $r1) {
							if (!isset($aTmp[$ii][$k1])) {
								$aTmp[$ii][$k1] = [];
							}
							foreach ($r1 as $d1 => $v1) {
								$aTmp[$ii][$k1][$d1] = $v1;
							}
						}
					}
				}

				$aDateCode2Count = $t2 + $aDateCode2Count;
				$aTmp2 = $aTmp2 + $t3;
				$aTmp3 = $aTmp3 + $t4;
				$aTmp4 = $aTmp4 + $t5;
				$aTmp5 = $aTmp5 + $t6;
			}

			/**
			 * #######组织成报表需要的数据#######
			 */
			$fClosureFunc = function($axAxisData, $aLegned, $axAxisData2) 
			{
				$aReturn2 = array();
				$aReturn2['xAxisData'] = array_keys($axAxisData);
				sort($aReturn2['xAxisData']);
				$aReturn2['legend'] = array_keys($aLegned);
				sort($aReturn2['legend']);
				foreach ($aReturn2['legend'] as $sTmp1) {
					foreach ($aReturn2['xAxisData'] as $sDate) {
						$axAxisData2[$sTmp1][$sDate] = isset($axAxisData2[$sTmp1][$sDate]) ? $axAxisData2[$sTmp1][$sDate] : 0;
						ksort($axAxisData2[$sTmp1]);
					}
				}
			// var_dump($axAxisData2);

				$aReturn2['series'] = array();


				foreach ($aReturn2['legend'] as $sTmp2) {

					$aReturn2['series'][] = array(
						"name" => $sTmp2,
						"type" => "bar",
						"stack" => "one",
						"data" => array_values($axAxisData2[$sTmp2]),
						'itemStyle' => array(),
						'label' => array(
							"normal" => array(
								// 'show' => true,
								'position' => "insideRight",
						)),
					);
				}
				    // var_dump($aReturn2['xAxisData']);
				    // var_dump($aReturn2);
				    // var_dump($aTmp);

				return $aReturn2;
			};

			$aReturn = array();
			$aReturn["server"] = $fClosureFunc($aTmp3, $aTmp2, $aTmp[2]);
			$aReturn["customer"] = $fClosureFunc($aTmp3, $aTmp4, $aTmp[3]);
			$aReturn["user"] = $fClosureFunc($aTmp3, $aTmp5, $aTmp[4]);

			$aReturn["datecode"] = $aDateCode2Count;

			unset($fClosureFunc);
			// $aReturn['ssphunan'] = $this->getServiceLocator()->get('\Lur\Service\Report')->getSspHunanData();


			return $aReturn;

		};


		$aViewParams = array();

		$aRecentData = $fClosureRecentDayCounterFunc($this, $sDate1, $eDate1);

		foreach ($aRecentData as $sKey => $aTmp6) {
			if ($sKey == 'datecode') {
				$aViewParams["ec_".$sKey] =  $aTmp6;
				krsort($aViewParams["ec_".$sKey]);
			}else{
				$aViewParams["ec_".$sKey] =  $this->_fmtEchartOption("RecentdayCounter", $sKey, $aTmp6);
			}
		}



		//小时分布图
		/**
		 * #######closure function start#######
		 */
		
		/**
		 * #######closure function start#######
		 */
		$fClosureFunc3 = function($sm) use ($oPdo)
		{
			$aHourDatas = $aHours = array(
			);

			$sDate1 = date("Ymd", strtotime("-1 day"));
			$sDate2 = date("Ymd");


			for ($i=0; $i < 24; $i++) { 
                $sHourTmp = strval($i<10 ? "0".$i : $i);
			    $aHours[$sHourTmp] = ($sHourTmp);
			}
	    	$aHourDatas = $this->getServiceLocator()->get('\Lur\Service\Common')->getLatestDateHourReport();
		    	// print_r($aHourDatas);




			 /**
			  * #######closure function start#######
			  */
			 $fClosureFunc2 = function($axAxisData, $aLegned, $axAxisData2, $flashType='bar') 
			 {
			 	$aReturn2 = array();
			 	$aReturn2['xAxisData'] = array_keys($axAxisData);
			 	sort($aReturn2['xAxisData']);
			 	$aReturn2['legend'] = ($aLegned);
			 	sort($aReturn2['legend']);
			 	foreach ($aReturn2['legend'] as $sTmp1) {
			 		foreach ($aReturn2['xAxisData'] as $sDate) {
			 			$axAxisData2[$sTmp1][$sDate] = isset($axAxisData2[$sTmp1][$sDate]) ? $axAxisData2[$sTmp1][$sDate] : 0;
			 			ksort($axAxisData2[$sTmp1]);
			 		}
			 	}
			 // var_dump($axAxisData2);

			 	$aReturn2['series'] = array();


			 	foreach ($aReturn2['legend'] as  $sTmp2) {

			 		$aReturn2['series'][] = array(
			 			"name" => $sTmp2,
			 			"type" => $flashType,
			 			"stack" => $sTmp2,
			 			"data" => @array_values($axAxisData2[$sTmp2]),
			 			'itemStyle' => array(),
			 			'label' => array(
			 				"normal" => array(
			 					// 'show' => true,
			 					'position' => "insideRight",
			 			)),
			 		);
			 	}

			 	return $aReturn2;
			 };

			 $aReturn = array();
			 $aReturn["hour"] = $fClosureFunc2($aHours, array($sDate1, $sDate2), $aHourDatas);


			 // ssp 的点击率报告
			 $aData4SspClickRate = [
			 	$sDate2 => []
			 ];
			$aSspClickPercentData4Realtime = $sm->get('\Lur\Service\Report')->clickPercentReportRealTime( LAF_ADSOURCETYPE_SSP_HUNAN, $sDate2);
			if (count($aSspClickPercentData4Realtime)) {
				foreach ($aSspClickPercentData4Realtime as $sMinuteKey => $sTmp3) {
					$aTmp8 = json_decode($sTmp3, 1);
					$nImp = $nCt = 0;
					if (count($aTmp8)) {
						foreach ($aTmp8 as $aTmp9) {
							$nImp += $aTmp9['imp'];
							$nCt += isset($aTmp9['c-t']) ? $aTmp9['c-t'] : 0;
						}
					}
					$aData4SspClickRate[$sDate2][$sMinuteKey] = empty($nImp) ? 0 :round($nCt/$nImp*100, 2);

				}
			}
			$aReturn["sspcrhour"] = $fClosureFunc2($aData4SspClickRate[$sDate2], array( $sDate2), $aData4SspClickRate, 'line');

		// var_dump($aReturn["sspcrhour"]);
		// var_dump($aData4SspClickRate);

			 // var_dump($aHours, $aHourDatas);
			return $aReturn;
		};

		$aHourData = $fClosureFunc3($this->getServiceLocator());
		unset($fClosureFunc3);

		foreach ($aHourData as $sKey => $aTmp6) {
			$aViewParams["ec_".$sKey] =  $this->_fmtEchartOption("RecentdayCounter", $sKey, $aTmp6);
		}


		$aViewParams["thirdapistatus"] = $this->_get3rdscheduleStatus();

		$aViewParams['sspclickpercent'] = $aSspClickPercentData;
		// $aViewParams['sspclickpercent4realtime'] = $aSspClickPercentData4Realtime;


		// var_dump(number_format(($debug_t2-$debug_t1), 4).'s');



// var_dump($aViewParams);
       return new ViewModel($aViewParams);

	}

	/**
	 * 第三方api调用数据
	 * 
	 */
	public function _get3rdscheduleStatus(){
	    $oRedis = $this->getServiceLocator()->get('\Lur\Service\Common')->getThirdScheduleRedis();
	    $aApiCounterKeys  = array(
	        LAF_CACHE_3RDSCHEDULE_APICALLCOUNTER_PRE.date("Ymd", strtotime("-2 day"))."/*",
	        LAF_CACHE_3RDSCHEDULE_APICALLCOUNTER_PRE.date("Ymd", strtotime("-1 day"))."/*",
	        LAF_CACHE_3RDSCHEDULE_APICALLCOUNTER_PRE.date("Ymd")."/*"
	    );
	    $aReturn = array();
	    foreach ($aApiCounterKeys as $sCacheTmp) {
	    	$aKeys = $oRedis->keys($sCacheTmp);
	    	if (count($aKeys)) {
	    		foreach ($aKeys as $sCacheTmp2) {
			    	$aReturn[$sCacheTmp2] = $oRedis->hGetAll($sCacheTmp2);
	    		}
	    	}
	    }
	    krsort($aReturn);
	    return $aReturn;

	}

	public function _fmtEchartOption($sType, $sKey, $aData)
	{
		$aReturn = array();
		switch ($sType) {
			case 'RecentdayCounter':
				$aReturn = array(
					// "title" => array(
					// 	"text" => $sKey,
					// ),
					"tooltip" => array(
						"trigger" => "axis",
					),
					"legend" => array(
						"data" => $aData['legend'],
					),
					"grid" => array(
						"left" => "3%",
						"right" => "4%",
						"bottom" => "3%",
						"containLabel" => true,
					),
					"xAxis" => array(
						"type" => "category",
						"data" => $aData['xAxisData'],
					),
					"yAxis" => array(
						"type" => "value",
					),
					"series" =>  $aData['series'],
				);
				break;
			
			default:
				# code...
				break;
		}
		return $aReturn;
	}



	public function indexAction()
	{
		var_dump(1);
	}
}