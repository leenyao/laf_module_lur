<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Lur\Controller;



use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends \Application\Controller\LafController
{


    public function showAdrScanQrCodeAction(){
        $sHtml  = <<<OUTPUT

        <!DOCTYPE html><html>
            <head>
                <meta charset="utf-8">
                <title>LUR导入模板描述文件</title>

                <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
                 <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
                 <script src="http://apps.bdimg.com/libs/bootstrap/3.0.3/js/bootstrap.min.js"></script>
            </head>
        <body>
        <table class="table table-bordered table-hover">
            <tr>
OUTPUT;
        for ($i=72; $i < 198 ; $i++) { 
            $sImg = "http://lur.local/tmpupload/pi-qrcode/adrpi-scan.jpg";
            $sTrHtml = "";
            if ($i%6==0) {
                $sTrHtml = "</tr><tr>";
            }
            $sHtml  .= <<<OUTPUT
            {$sTrHtml}
            <td><img src="{$sImg}" class="img-responsive" alt="Image"></td>
OUTPUT;
        }
        $sHtml  .= <<<OUTPUT
                    </tr>
                </table>
            </body>
OUTPUT;
        echo $sHtml;
        exit;
    }
    public function showAdrQrCodeAction(){


        $sHtml  = <<<OUTPUT

        <!DOCTYPE html><html>
            <head>
                <meta charset="utf-8">
                <title>LUR导入模板描述文件</title>

                <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
                 <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
                 <script src="http://apps.bdimg.com/libs/bootstrap/3.0.3/js/bootstrap.min.js"></script>
            </head>
        <body>
        <table class="table table-bordered table-hover">
            <tr>
OUTPUT;
        for ($i=72; $i < 198 ; $i++) { 
            $sImg = "http://lur.local/tmpupload/pi-qrcode/".$i.".jpg";
            $sTrHtml = "";
            if ($i%6==0) {
                $sTrHtml = "</tr><tr>";
            }
            $sHtml  .= <<<OUTPUT
            {$sTrHtml}
            <td><img src="{$sImg}" class="img-responsive" alt="Image"></td>
OUTPUT;


        }

        $sHtml  .= <<<OUTPUT
                    </tr>
                </table>
            </body>
OUTPUT;
        echo $sHtml;
        exit;
    }
    
    public function genAdrQrCodeAction(){


        for ($i=1; $i < 200 ; $i++) { 
            $qrCode = new \Endroid\QrCode\QrCode();
            $sUrl = "http://".LAF_WEBHOST."/module/lur/piwifi.php?i={$i}&ei=".\YcheukfCommon\Lib\Functions::encryptBySlat($i);
            $qrCode
                ->setText($sUrl)
                ->setSize(130)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel("adrpi-".$i)
                ->setLabelFontSize(12)
                ->render(BASE_INDEX_PATH."/tmpupload/pi-qrcode/".$i.".jpg", "jpg")
            ;
        }
    }
    
    public function genAdrWifiScanQrCodeAction(){


        $qrCode = new \Endroid\QrCode\QrCode();
        $sUrl = "http://".LAF_WEBHOST."/module/lur/piwifi.lanscan.php";
        $qrCode
            ->setText($sUrl)
            ->setSize(130)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
            ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
            ->setLabel("adrpi-scan")
            ->setLabelFontSize(12)
            ->render(BASE_INDEX_PATH."/tmpupload/pi-qrcode/adrpi-scan.jpg", "jpg")
        ;
    }
    public function debugAction(){
        $aConfig = $this->getServiceLocator()->get('config');
        var_dump($aConfig['adrhashredis']);
        var_dump($aConfig['ssp_platforms']);


        exit;
        $aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), LAF_METADATATYPE_LOCATION);
        $aCityLabel2ResId = array_flip($aCityId2Label);
        var_dump($aCityLabel2ResId);

        exit;

        $aRelation1102 = array(1,2,3,4,5,6);
        // $aSocialcontent2 = \Application\Model\Common::getResourceList($this->getServiceLocator(), array('source'=>'b_adschedule', 'pkfield'=>'id', 'labelfield'=>'code'), array("id" => array('$in' => $aRelation1102) ));
        $aSocialcontent2 = \Application\Model\Common::getResourceList($this->getServiceLocator(), array('source'=>'b_adschedule', 'pkfield'=>'id', 'labelfield'=>'code'), array("id" => 1));
        var_dump($aSocialcontent2);

        exit;
        for ($i=1; $i < 200 ; $i++) { 
            $qrCode = new \Endroid\QrCode\QrCode();
            $sUrl = "http://".LAF_WEBHOST."/module/lur/piwifi.php?i={$i}&ei=".\YcheukfCommon\Lib\Functions::encryptBySlat($i);
            $qrCode
                ->setText($sUrl)
                ->setSize(130)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel("adr-".$i)
                ->setLabelFontSize(12)
                ->render(BASE_INDEX_PATH."/tmpupload/pi-qrcode/".$i.".jpg", "jpg")
            ;
        }
        exit;
        $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrReportDataForCounterTable(array(25987, 23616, 26066), "2017-04-07", "2017-04-08");
        exit;
    
    }
    public function indexAction(){
       return new ViewModel();
    }

    public function clearapicacheAction()
    {

        $this->getServiceLocator()->get('\Lur\Service\Common')->updateScheduleCache();

        // \YcheukfCommon\Lib\Functions::delCache($this->getServiceLocator(), 'restapi_adrconfig', true);
        
        $this->redirect()->toRoute("zfcadmin/contentlist", array('type' => 'system_batch_jobs', 'cid' => 1));
    }


    public function downloadsampleinfoAction()
    {

        $aConfig = $this->getServiceLocator()->get('config');
        $sCxids = join(",", $aConfig['hunan_cxids']);
        
        echo <<<__EOF

        <!DOCTYPE html><html>
            <head>
                <meta charset="utf-8">
                <title>LUR导入模板描述文件</title>
            </head>
        <body>
            <pre>

        字段说明:

        第1列[userid]:         企业ID
        第2列[codelabel]:         该条代码的别名
        第3列[show_code]:         show代码. 
        <!--  
            pptv2. 填写公式 [ta,platform,pos]. 样例: 27096,232414,36
            le. 填写公式 [dealid,oid,广告位ID,平台ID]. 样例: 27096,232414,36,7
            sh. 填写公式 [平台,对应id,vrs分类|vrs分类,al参数,vid参数,duration,ct]. 样例: oad,63123,100|102|103,9056052,2785403,15,2
            keep. 填写公式 [imp.id,deal_type,deal_id,宽,高]. 样例: 1000,1,rs_open_20200119,640,960

            -->
        第4列[click_code]:         click代码
        第5列[servers]:         [已弃用, 由lur服务自动选择]本条代码所使用的服务器
        第6列[UA]:         本条代码所使用的UA
        第7列[cities]:         所投城市
        第8列[refferer]:         refferer
        第9列[ip frequency]:         ip频次, 一个ip会被使用多少次
        第10列[cookie frequency]:         cookie频次, <click代码>的cookie频次会在后台被强制转换成1
        第11列[show_activity]:         是否激活show代码/视频播放代码, 1=>激活,2=>禁止, 默认1
        第12列[click_activity]:          是否激活click代码, 1=>激活,2=>禁止 默认1
        第13列[stable cookie percent]:         stable cookie百分比
        第14列[sourcetype]:         广告类型, 1=>普通PC广告, 2=>sdk广告, 4=>视频播放量, 5=>SSP-HUNAN
        第15列[apps]:         在 sourcetype=2 时有效, 所定向的app, 在设置 <apps广告>时不能为空
        第16列[domaincookie_delete_percent]:         从domain cookie 池子删除该cookie的概率, 在设置<跨媒体频次1>类型广告时必须为100
        <!--  
        第16列[reach_percent]:    [1-100] 用我们自己的设备号时，3+reach 客户要求频控为3次的时候，正好看过3次的比例在70%以上 或者其他比例
            -->

        第17列[uv code]:         UV配置信息, json.
        第18列[uv 2nd code]:         废弃
        第19列[uv 2nd percent]:         废弃
        第20列[uv_activity]:         是否激活UV代码, 1=>激活, 默认1
        第21列[ssp-ctr]:         ssp的点击率, 范围是0-1的浮点数, 例如0.04(即代表4%的点击率)
        第22列[ssp-媒资id]:         ssp的媒体资源ID,ssp格式时必填, 例: [111,22,33]
        第23列[TA id]:         TA库的ID, 见 映射表
        第24列[cxid]:   
        <!--  
              
           mango. 湖南ssp的cxid, 只允许的值:[{$sCxids}].  可不填,默认为6778_ry_0, 可多选(用逗号分隔, 如  6778_ry_0,6778_ry_2)
           sh. identifyid. 只允许的值[1,5],不填默认1
        第25列[useiacip]:         是否只使用与iac一致的地域[1=>是,其他=>否(默认)]
            -->
        <!--  

        第25列[useiac]:   是否严格使用iac的ip. 1=>是, 默认0(选择开启该参数有可能会导致完成率下降)
        第26列[vid]:   sh的vid
        第27列[useapiuuid]:   sh的使用apiuuid, [1=>是,其他=>否(默认)]
            -->

        </pre>
        </body>
        </html>
__EOF;
        exit;
    }

    public function _fmtDownfile($sFileNmae)
    {

        //将日期替换成今天的逻辑
        $sContents = file_get_contents($sFileNmae);
        $aTmp = explode("\r", $sContents);
        preg_match_all("/(\d{4}\-\d{2}-\d{2})+?/i", $aTmp[0], $aMatchs);
        if (count($aMatchs[1])) {
            $aUniqueDates = array_unique($aMatchs[1]);
            $nIndex = 0;
            foreach ($aUniqueDates as $sDate) {
                $aTmp[0] = str_replace($sDate, date("Y-m-d", strtotime("+".$nIndex." day")), $aTmp[0]);
                $nIndex++;
            }
        }
        $sContents = join("\r", $aTmp);
        return $sContents;
        # code...
    }

    function downloadsampleAction($value='')
    {

        $sSource = isset($_GET['type']) ? $_GET['type'] : null;
        $sFileNmae = BASE_INDEX_PATH."/../module/Lur/config/sample-".strtolower($sSource)."-code.csv";
        if (!file_exists($sFileNmae)) {
            echo "file not exists:".$sFileNmae;
            exit;
        }

        $len = filesize($sFileNmae);
        $sContents = $this->_fmtDownfile($sFileNmae);
        // var_dump($sSource);


        // $aHeader = explode(",", $aTmp[0]);
        // $bStart = false;
        // foreach ($aHeader as $key => $value) {
        //     if ("date-start-column" == $value) {
        //         $bStart = true;
        //     }
        //     if (!$bStart) {
        //         continue;
        //     }
        //     if (preg_match("//i", subject)) {
        //         # code...
        //     }
        // var_dump($value);


        // }
        // var_dump($aHeader);


        // exit;
        //Begin writing headers;
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public"); 
        header("Content-Description: File Transfer");
        
        //Use the switch-generated Content-Type
        header("Content-Type:csv");

        $header='Content-Disposition: attachment; filename="'.basename($sFileNmae);
        header($header);
        header("Content-Transfer-Encoding:binary");
        header("Content-Length: ".$len);
        echo $sContents;
        // @readfile($sFileNmae);
        exit;

    }
    public function viewmappinglistAction(){

        \Application\Model\Common::setUseLafCacheFlag(false);

        $aM1007 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1007);
        $aM1008 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1008);
        // $aM1009 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1009);
        $aM1014 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1014);
        $aM1015 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1015);
        $aM1023 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1023);


        $aM1025 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), LAF_MTYPE_SHVID_1025);


        $aSspPlatform = $this->getServiceLocator()->get('\Lur\Service\Common')->getViewSspPlatform();

        $aM1014New = [];
        foreach ($aM1014 as $key => $value) {
            if (preg_match('/^ssp-/i', $value)) {
                if (!in_array($key, $aSspPlatform)) {
                    unset($aM1014[$key]);
                    # code...
                }
            }
        }




        // echo "<pre>";
        // print_r($aM1007);
        // print_r($aM1008);
        // // print_r($aM1009);
        // print_r($aM1014);
        // print_r($aM1015);
        // echo "</pre>";
        krsort($aM1007);
        krsort($aM1008);
        krsort($aM1014);
        krsort($aM1015);
        krsort($aM1025);

        $aprovicesRes = $this->getServiceLocator()->get('\Lur\Service\Common')->province2cities();

        $aIaCprovicesRes = $this->getServiceLocator()->get('\Lur\Service\Common')->getIacProvince();


        // var_dump($aIaCprovicesRes);
        // var_dump($aprovicesRes);
        $aViewParams = array(
            'aM1007' => $aM1007,
            'aM1008' => $aM1008,
            'aM1014' => $aM1014,
            'aM1015' => $aM1015,
            'aM1023' => $aM1023,
            'aM1025' => $aM1025,
            'aprovicesRes' => $aprovicesRes,
            'aIaCprovicesRes' => $aIaCprovicesRes,
        );
        return new ViewModel($aViewParams);
    }


    public function viewlackingcitiesAction(){

        $sDate = isset($_GET['d']) ? $_GET['d'] : date("Y-m-d");
        $sShortDate = date("Ymd", strtotime($sDate));
        $sShortDate2 = date("Ymd", strtotime("-1 day", strtotime($sShortDate)));
        $sShortDate3 = date("Ymd", strtotime("-2 day", strtotime($sShortDate)));
         $sSql = "select * from `b_adrstats` where id in(select  max(id) from `b_adrstats` where type=1 and   status=1 and created_time between '".$sDate." 00:00:00' and '".$sDate." 23:59:59' group by host)";


        $aConfig = $this->getServiceLocator()->get('config');
        $oPdo = (\Application\Model\Common::getPDOObejct($this->getServiceLocator()));

        $oQuery = $oPdo->query($sSql);

        $aTmp = array();
        $aLackingCities = array();
        $aM1007 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1007);
        while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){
            $aTmp = json_decode($aResult['data'], 1);
            if (isset($aTmp['ARK_122/'.$sShortDate]) && count($aTmp['ARK_122/'.$sShortDate])) {
                foreach ($aTmp['ARK_122/'.$sShortDate] as $nCityId => $nCount) {
                    // $sLabel = $nCityId."/".(isset($aM1007[$nCityId])?$aM1007[$nCityId]:"");
                    $nCount = intval($nCount);
                    if (!isset($aLackingCities[$nCityId])) {
                        $aLackingCities[$nCityId] = 0;
                    }
                    $aLackingCities[$nCityId] += $nCount;
                }
            }
         }

        $aCity2Count = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getAllCityCounter($sShortDate);
        $aCity2Count2 = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getAllCityCounter($sShortDate2);
        $aCity2Count3 = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getAllCityCounter($sShortDate3);
        // print_r($aCity2Count);
         arsort($aCity2Count);
        // print_r($aCity2Count);

        $sLackCityCacheKey = LAF_CACHE_LACKVPNACCCITIES_108.$sShortDate;
        $aLackVpnCities = ($this->getServiceLocator()->get('\Lur\Service\Common')->getAddtionalRedis()->hGetAll($sLackCityCacheKey));
        $aCurrentVpnRelation = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getCurrentVpnRelation(true);
        $aIproxyAllcities = $this->getServiceLocator()->get('\Lur\Service\Common')->getIproxyAllCity();

        $aVpnCity2Count = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getVpnCity2Count(array_values($aM1007));
        $aAdrSummary = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrSummary();

        $aViewParams = array(
            'aIproxyAllcities' => $aIproxyAllcities,
            'aCurrentVpnRelation' => $aCurrentVpnRelation,
            'aLackVpnCities' => $aLackVpnCities,
            'aLackingCities' => $aLackingCities,
            'aCity2Count' => $aCity2Count,
            'aCity2Count2' => $aCity2Count2,
            'aCity2Count3' => $aCity2Count3,
            'aVpnCity2Count' => $aVpnCity2Count,
            'aM1007' => $aM1007,
            'aAdrSummary' => $aAdrSummary,
        );
        return new ViewModel($aViewParams);
    }

    public function sspscheduleAction($value='')
    {


        $oAuthService = ($this->serviceManager->get('zfcuser_auth_service'));
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($this->serviceManager, $oAuthService->getIdentity());


        if (in_array("superadmin", $aRoles)) {
            $sGrantUserWhere = '';
        }else{
            $nOpUserId = $this->serviceManager->get('zfcuser_auth_service')->getIdentity()->getId();
            $sGrantUserWhere = ' and op_user_id='.$nOpUserId.'';
        }

        $aSspPlatform = $this->getServiceLocator()->get('\Lur\Service\Common')->getViewSspPlatform();




        $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave
        $sSql = "select * from `laf_lur`.`b_adschedule` where `m1014_id` in (".join(',',$aSspPlatform).") and m102_id=1 ".$sGrantUserWhere." order by code_label";

        
        // var_dump($sSql);


        $oSth = $oPdo->prepare($sSql);
        $oSth->execute(array(
            // ":date" => date("Y-m-d", strtotime($sDate)),
        ));
        $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
        $aCounterRanking = array();
        $aRelations = array();
        $aMetadataList = array();
        $aMetadataList[1007] = \Application\Model\Common::getResourceMetadaList($this->serviceManager, 1007);
        foreach ($aResultTmp as $aRowTmp) {
            $aCityIds = \Application\Model\Common::getRelationList($this->serviceManager,1007, $aRowTmp['id']);
            $aRowTmp['cities'] = array();

            
            if (is_array($aCityIds) && count($aCityIds)) {
                foreach ($aCityIds as $sKtmp1 => $nIdTmp) {
                    $aRowTmp['cities'][$nIdTmp] = $aMetadataList[1007][$nIdTmp];
                }
            }
            
            $aCounterRanking[$aRowTmp['id']] = $aRowTmp;

        }
        $aViewParams = array(
            "aCounterRanking" => $aCounterRanking,
            // "aRelations" => $aRelations,
            // "aMetadataList" => $aMetadataList,
        );

        return new ViewModel($aViewParams);
    }
    

    public function _sspTypeId2Lable($nId){

        $aMetadataList = array();
        $aMetadataList[1014] = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1014);
        return str_replace('ssp-', "", strtolower($aMetadataList[1014][$nId]));
    }

    public function _fmtSspParams($get)
    {

        if (!isset($get['sdate'])) {
            $get['sdate'] = date("Y-m-d", strtotime("-7 day"));
        }
        if (!isset($get['edate'])) {
            $get['edate'] = date("Y-m-d");
        }
        $nType=1;
        if (isset($get['type'])) {
            $nType = $get['type'];
        }
        $get['type'] = $nType;

        $sspType = 'hunan';
        if (isset($get['st'])) {
            $sspType = $get['st'];
        }
        $get['st'] = $sspType;
        return $get;
    }
    public function sspreportAction(){
        $_GET = $this->_fmtSspParams($_GET);

        $aOutput = $this->getServiceLocator()->get('\Lur\Service\Report')->getSspHunanDataByDateCode($_GET['sdate'], $_GET['edate'], $_GET['type'], $_GET['st']);
        // $aReturn = array(array(
        //     "date",
        //     "code-id",
        //     "city",
        //     "imp-count",
        //     "click-count",
        // ));
        // var_dump($aOutput);
        // exit;
        $aReturn = array();
        $aTableTitle = [];
        if (is_array($aOutput) && count($aOutput)) {
            $aM1007 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1007);

            foreach ($aOutput as $aRow) {
                switch ($_GET['type']) {
                    case 3:
                        $aReturn[] = array(
                            $aRow['date'],
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                        );
                        $aTableTitle = ['date', 'imp', 'click', 'click-rate'];
                        break;
                    case 9:
                        $aReturn[] = array(
                            $aRow['date'],
                            $aRow['hunan_cxids'],
                            $aRow['sspid'],
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                        );
                        $aTableTitle = ['date', 'cxids', 'plan', 'imp', 'click', 'click-rate'];
                        break;
                    case 8:
                    // var_dump($aRow);
                        $sCountLabel = $this->getServiceLocator()->get('\Lur\Service\Common')->getScheduleFinishedInfo($aRow['id'], $aRow['date'], 0, intval($aRow['impcount']));
                        $aSplit2 = explode(',', $sCountLabel);
                        $sCountLabel2 = $aSplit2[0];
                        list($finish1, $p2) = explode("/", $sCountLabel);
                        list($total1, $p3) = explode("(", $p2);

                        $aCityData = $this->getServiceLocator()->get('\Lur\Service\Common')->getCitysByCodeId($aRow['id'], false);
                        $aReturn[] = array(
                            $aRow['date'],
                            $aRow['sspid'],
                            $aRow['hunan_cxids'],
                            // $aRow['id'],
                            // $aRow['code_label'],
                            // "<a target=blank href='".$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$aRow['id']."&sdate=".$aRow['date']."'>".$aRow['id']."/".$aRow['code_label']."</a>",
                            "<a target=blank href='".$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$aRow['id']."&sdate=".$aRow['date']."'>".str_replace("-show", "", $aRow['code_label'])."</a>",
                            $aCityData['label'],
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                            $sCountLabel2,
                            $total1.",".strip_tags($finish1)
                        );
                        $aTableTitle = ['date', 'plan', 'cxids','code',  'citys', 'imp', 'click','click-rate', 'finish-rate', 'total,finish'];

                        break;
                    case 2:
                        $aReturn[] = array(
                            $aRow['date'],
                            (isset($aM1007[$aRow['city_id']])?$aM1007[$aRow['city_id']]:"全国"),
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                        );
                        break;
                    case 7:
                        $aReturn[] = array(
                            $aRow['date'],
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                        );
                        break;
                    case 5:
                        $aReturn[] = array(
                            $aRow['date'],
                            $aRow['code_lid'],
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                        );
                        break;
                    case 6:
                        $aReturn[] = array(
                            $aRow['date'],
                            $aRow['code_id'],
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                        );
                        break;
                    case 4:
                        $aReturn[] = array(
                            $aRow['date'],
                            $aRow['code_id'],
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                        );
                        break;
                    
                    default:
                        $aReturn[] = array(
                            $aRow['date'],
                            $aRow['code_id'],
                            (isset($aM1007[$aRow['city_id']])?$aM1007[$aRow['city_id']]:"全国"),
                            $aRow['impcount'],
                            $aRow['ccount'],
                            round($aRow['ccount']/$aRow['impcount'], 4),
                        );
                        break;
                }
            }
        }
        // var_dump($aReturn);

        echo $this->_htmloutput($aReturn, "SSP-HUNAN report", 1, $aTableTitle, true);
        exit;
    }
    public function realtimereportAction(){
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $debug = isset($_GET['debug']) ? $_GET['debug'] : 0;
        $sdate = isset($_GET['sdate']) ? $_GET['sdate'] : date("Y-m-d");
        $sIdKey = isset($_GET['idkey']) ? $_GET['idkey'] : "system_batch_jobs___id";//adschedule___id,system_batch_jobs___id

        if (in_array($id, [30378])){
            $id = 14162;
        }elseif (in_array($id, [78772])){
            $id = 14292;
        }else{
            $id = \YcheukfCommon\Lib\Functions::decodeLafResouceId($id, str_replace("___id", "", $sIdKey));

        }

        if (is_null($id)) {
            echo "need param id";
            exit;
        }



        switch ($sIdKey) {
            case 'adschedule___id':

                $result = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'adschedule', $id);
                if (count($result) < 1) {
                    echo "no such id";
                    exit;
                }
                $aJson2 = array(
                    "show" => array(
                        array("id"=>$result['id'], "url"=>$result['code']),
                    ),
                    "click" => array(
                        array(),
                    ),
                );
                break;
            
            default:
                $result = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'system_batch_jobs', $id);


                if (empty($result['out_json']) || is_null($result['out_json'])) {
                    echo "error out_json";
                    exit;
                }
                $aJson2 = json_decode($result['out_json'], 1);
            break;
        }


        $aUrlids = array();
        if(!is_null($aJson2)){
            foreach ($aJson2 as $k1 => $aTmp1) {
                if (count($aTmp1)) {
                    foreach ($aTmp1 as $k2 => $aTmp2) {
                        if (isset($aTmp2['id'])) {
                            $aUrlids[] = $aTmp2['id'];
                        }
                    }

                }
            }
        }
        $sToday = $sdate;
        $sUserId = $result['user_id'];
        $aConfig = $this->getServiceLocator()->get('config');


        $this->getServiceLocator()->get('\Lur\Service\Common')->flushCacheCounter2Db();

        $nTypeTmp = $debug==3? 3 : ($debug==1?2:0);
        list($bFalg, $aReportData) = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrReportDataForCounterTable( $aUrlids, $sToday, $sToday, $nTypeTmp);

// var_dump($aReportData);

        if ($bFalg == false) {
            echo $aReportData;
            exit;
        }
        // var_dump($aReportData);
        $aOutput = array();
        foreach ($aJson2['show'] as $k1 => $aTmp1) {
            if (count($aTmp1)) {
                if (isset($aTmp1['id']) && isset($aTmp1['id'])) {
                    $sShowIdTmp = isset($aJson2['show'][$k1]['id']) ? $aJson2['show'][$k1]['id'] : "";
                    ;

                    $sShowUrlTmp = isset($aJson2['show'][$k1]['url']) ? "<a href='".$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$aJson2['show'][$k1]['id']."'>".(preg_match('/www\.baidu\.com/', $aJson2['show'][$k1]['url'])?urldecode($aJson2['show'][$k1]['url']):$aJson2['show'][$k1]['url'])."</a>" : "";
                    $sClickIdTmp = isset($aJson2['click'][$k1]['id']) ? $aJson2['click'][$k1]['id'] : "";
                    $sClickUrlTmp = isset($aJson2['click'][$k1]['url']) ? "<a href='".$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$aJson2['click'][$k1]['id']."'>".$aJson2['click'][$k1]['url']."</a>" : "";

                    if (!isset($nShowCount)) {
                        $nShowCount = 0;
                        $nClickCount = 0;
                    }


                    // $aConfig = $this->getServiceLocator()->get('config');
                    // $oPdo =  new \PDO($aConfig['db_master']['dsn'], $aConfig['db_master']['username'], $aConfig['db_master']['password'], array(\PDO::ATTR_PERSISTENT => true));
                    // $oQuery = $oPdo->query($sSql);
                    // $aTmp = array();
                    // $aM1009 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1009);
                    // while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){
                    //     $aTmp[$aResult['host']] = json_decode($aResult['data'], 1);


                    //     // $aTmp[$aResult['host']] = $aResult['id'];
                    //  }



                    //yestoday data
                    $nShowCount = isset($aReportData[$sToday][$sShowIdTmp]) ? $aReportData[$sToday][$sShowIdTmp] : 0;
                    $nClickCount = isset($aReportData[$sToday][$sClickIdTmp]) ? $aReportData[$sToday][$sClickIdTmp] : 0;

                     if (!$debug) {
                         $aOutput[] = array(
                             $nShowCount,
                             $nClickCount,
                         );
                    }else{
                        $aOutput[] = array(
                             $nShowCount,
                             $nClickCount,
                             $sShowUrlTmp,
                             $sShowIdTmp,
                             $sClickUrlTmp,
                             $sClickIdTmp,
                        );
                    }

                }

            }
        }
        echo $this->_htmloutput($aOutput, "real time report", $nTypeTmp);
        exit;

        // $aReportData = $this->_importTable($aConfig, $result['user_id'], $aUrlids, $sYestoday);

    }

    private function _htmloutput($aOutput, $sTitle, $nTypeTmp=1, $aTableTitle=[], $bSspFlag=false)
    {

        $unfinish = 0;
        $total3 = 0;
           $sHtml = "<div class=\"table-responsive\"><table class=\"table table-bordered\">";

           if (count($aTableTitle)) {
            $sHtml .= "<thead>";

            foreach ($aTableTitle as $sTHeader) {
                $sHtml .= "<th width=200px>".$sTHeader."</th>";
            }
            $sHtml .= "</thead>";

           }
           if ($nTypeTmp === 3) {
            // var_dump($aOutput);
            // exit;
               foreach ($aOutput as $k1 => $aRow1) {
                    if (isset($aRow1[0]) && count($aRow1[0])) {
                       foreach ($aRow1[0] as $nAdrIdTmp=>$nCountTmp) {
                           $sHtml .=  "<tr><td>".$nCountTmp."</td><td>".$nAdrIdTmp."</td><td>".$aRow1[3]."</td><td>".$aRow1[2]."</td></tr>";
                       }
                    }
               }
           }else {
               foreach ($aOutput as $aRow) {
                    if ($nTypeTmp == 1) {
                        if (isset($aRow[9])) {
                            list($total2, $finised) = explode(",", $aRow[9]);
                            $unfinish +=  $finised;
                            $total3 +=  $total2;
                        }
                    }
                   $sHtml .=  "<tr><td>".implode("</td><td>", $aRow)."</td></tr>";
               }
           }
           $sHtml .= "</table></div>";

           if (isset($aRow[9])) {
           $sHtml .= "<h5>总下单量:".$total3.", 完成量:".$unfinish.", 未完成量:".($total3-$unfinish).", 未完成率:".(number_format(($total3-$unfinish)*100/$total3, 4))."%</h5>";
               
           }

           

           $aConfig = $this->getServiceLocator()->get('config');

            $_GET = $this->_fmtSspParams($_GET);

           $aMetadataList = array();
           $aMetadataList[1014] = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1014);
           $sSspPlatFormHtml = '';
           if ($bSspFlag) {
                $nOpUserId = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();

                if ($nOpUserId == 1) {
                    $aConfig['userid2ssp_platforms'][$nOpUserId] =  $aConfig['ssp_platforms'];
                }
                if(isset($aConfig['userid2ssp_platforms'][$nOpUserId])){
                   foreach ($aConfig['userid2ssp_platforms'][$nOpUserId] as $n1) {
                        $sSspType = $this->_sspTypeId2Lable($n1);
                        $sSelectedStyle = ($sSspType==$_GET['st'])? 'style="font-weight: bold;background-color: burlywood;"' : '';
                        $sSspPlatFormHtml .= '<li '.$sSelectedStyle.'> <a href="'.$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'sspreport'))."?sdate=".$_GET['sdate']."&edate=".$_GET['edate'].'&type='.(isset($_GET['type'])?$_GET['type']:1).'&st='.$sSspType.'">'.$aMetadataList[1014][$n1]."</a></li>";
                   }

                }
               
               $sSspPlatFormHtml = '<ul>'.$sSspPlatFormHtml."</ul>";
               
           }
           $sReturn = <<<_EOF

           <html>
           <style>
           th,h4{background-color:#ffff99}
           </style>
           <title>$sTitle</title>
           <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
            <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
            <script src="http://apps.bdimg.com/libs/bootstrap/3.0.3/js/bootstrap.min.js"></script>

           </head>

           <body>
           {$sSspPlatFormHtml}
            $sHtml
            </body>
_EOF;
        return $sReturn;
        # code...
    }

    public function histAdScheduleAction(){
        if (!isset($_GET['id'])) {

            echo "need id";
            exit;
        }

        $nId = \YcheukfCommon\Lib\Functions::decodeLafResouceId($_GET['id'], "adschedule");
        $edate = date("Ymd");
        $sdate = date("Ymd", strtotime("-1 day", strtotime($edate)));
        $sdate = isset($_GET['sdate']) ? $_GET['sdate'] : $sdate;
        $edate = isset($_GET['edate']) ? $_GET['edate'] : $edate;

        // var_dump($sdate);

        // 完成数据, 按天/服务器
        $nTypeTmp =7;
        list($bFalg, $aReportData) = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrReportDataForCounterTable( array($nId), $sdate, $edate, $nTypeTmp);

        // 完成数据, 按天
        $nTypeTmp =6;
        list($bFalg, $aReportData2) = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrReportDataForCounterTable( array($nId), $sdate, $edate, $nTypeTmp);


        $aParams = array(
            'op'=> 'get',
            'resource'=> 'system_trigger_log',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->getServiceLocator()),
            'params' => array(
                'select' => array (

                    "where" => array('type'=>"b_adschedule", "resid"=>$nId),

                    'order' => 'id asc',
                ),

            ),
        );

        $aDatas = \YcheukfCommon\Lib\Functions::getResourceList2($this->getServiceLocator(), $aParams);

        if ($aDatas['count']) {
            foreach ($aDatas['dataset'] as $row) {
                $aTmp = ($row['memo']);
                $sPrint = print_r($aTmp, 1);
                echo <<<__EOF
                <h3>ID: {$row['resid']}  于  {$row['modified']} 更新的内容</h3>
                操作类型: {$row['optype']}
                操作内容: <pre>{$sPrint}</pre>
                <hr/>

__EOF;
            }
            $result = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'adschedule', $nId);
            $sPrint = print_r($result, 1);
            $sReportData = print_r($aReportData, 1);
            $sReportData2 = print_r($aReportData2, 1);

            $sShowLinkHtml = $sClickLinkHtml = "";
            if (empty($result['cookie_code_id'])) { // 显示代码
                $result2 = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'adschedule', $nId, 'cookie_code_id');
                if ($result2 && count($result2)) {
                    $sClickLinkHtml = "<a target='blank' href='".$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$result2['id']."'>对应点击代码</a>,".$result2['id'] ;
                }
            }else{//点击代码
                $sShowLinkHtml = "<a target='blank' href='".$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$result['cookie_code_id']."'>对应显示代码</a>,".$result['cookie_code_id'] ;
            }
            

            $aM1007 = \Application\Model\Common::getRelationList($this->getServiceLocator(),1007, $nId);
            $aM1008 = \Application\Model\Common::getRelationList($this->getServiceLocator(),1008, $nId);
            $aM1009 = \Application\Model\Common::getRelationList($this->getServiceLocator(),1009, $nId, false, true);
            $aM1015 = \Application\Model\Common::getRelationList($this->getServiceLocator(),1015, $nId);
            $sUas = count($aM1008) ? join(" / ", \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1008, $aM1008)) : "无定向";

            $aClick2Uv = \Application\Model\Common::getRelationList($this->getServiceLocator(),LAF_RELATIONTYPE_UV2CLICK, $nId);
            $aUvids = array_values($aClick2Uv);
            $sUvids = join("/", $aUvids);


            
            $aServers = count($aM1009) ? \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1009, $aM1009) : array();
            $sSourceType = \Application\Model\Common::getResourceMetaDataLabel($this->getServiceLocator(), 1014, $result['m1014_id']);
            // var_dump($aServers);
            $sCities = "无定向";
            if (count($aM1007)) {
                $aCities = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1007, $aM1007);

                $sCities = "";
                foreach ($aCities as $keyTmp2 => $sValTmp2) {
                    $sCities .= $keyTmp2."/".$sValTmp2.";";
                }
            }
            $sServers = "无定向";
            if (count($aServers)) {
                $sServers = "";
                foreach ($aServers as $keyTmp2 => $sValTmp2) {
                    $sServers .= $keyTmp2."/".$sValTmp2.";";
                }
            }

            $sApps = "无定向";
            if (count($aM1015)) {
                $aApps = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1015, $aM1015);
                $sApps = "";
                foreach ($aApps as $keyTmp2 => $sValTmp2) {
                    $sApps .= $keyTmp2."/".$sValTmp2.";";
                }
            }

            // list($aScheduleEntity, $nScheduleTotal) = $this->getServiceLocator()->get('\Lur\Service\Common')->getScheduleCountByDate($nId, $sdate);
            // var_dump($aScheduleEntity);
            // var_dump($nScheduleTotal);
            // list($aScheduleEntity, $nScheduleTotal) = $this->oFrameworker->sm->get('\Lur\Service\Common')->getScheduleFinishedInfo($nId, $edate);


            $sHtml = "<ul style='word-break: break-all;max-width: 900px;white-space: normal;'>
                <li>servers: {$sServers} 
                <li>城市: {$sCities}
                <li>广告类型: {$sSourceType} 
                <ul>
                    <li>定向APP: {$sApps}
                    <li>UA: {$sUas}
                    </ul>
                </ul>
            ";

            $sToday1 = date('Ymd');
            $sYesterday1 = date('Ymd', strtotime('-1 day'));
            $aAddtionalCounter[$sToday1] = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getJobCountById($nId, $sToday1);
            $aAddtionalCounter[$sYesterday1] = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getJobCountById($nId, $sYesterday1);
            $sAddtionalCounter = print_r($aAddtionalCounter, 1);

            $sActiveLable = $result['m102_id']==1 ? '': '(<font color=red>已停止</font>)';

            $sScheuleCache = 'id='.$nId;

            $sTmp2 = $this->getServiceLocator()->get('\Lur\Service\Common')->getScheduleByCodeId($nId);
            if (!empty($sTmp2)) {
                $sScheuleCache .= print_r(json_decode($sTmp2, 1), 1);
            }
            
            echo <<<__EOF

            <!DOCTYPE html><html>
                <head>
                    <meta charset="utf-8">
                    <title>{$nId} 排期历史数据</title>
                     <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
                </head>
            <body>

            <h3>ID: {$row['resid']} {$sActiveLable} 当前内容, {$sShowLinkHtml}{$sClickLinkHtml}</h3>
            操作内容: <pre>
                {$sPrint}
                {$sHtml}
            </pre>
            当前CACHE: <pre>
            {$sScheuleCache}
            </pre>
            <hr/>
            <h5>补量账户</h5>
            <pre>{$sAddtionalCounter}</pre>
            <h5>完成数据</h5>
            <pre>{$sReportData2}</pre>
            <pre>{$sReportData}</pre>
            </body>

             <script type="text/javascript">

            \$(".copy_adr_attach").click(function(event) {
              // console.log(\$(this).data('dhost'));
              var oInput = \$("<input value='~/Documents/script/adr-login.sh "+\$(this).data('hid')+" "+\$(this).data('dhost')+" "+\$(this).data('aid')+" "+\$(this).data('cid')+" '>");
                oInput.appendTo('body');
                oInput.select();
                document.execCommand("Copy"); // 执行浏览器复制命令
                oInput.remove()
            });
             </script>
            </html>

__EOF;

        }
        exit; 
    }
    public function histreportAction(){
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $sdate = isset($_GET['sdate']) ? $_GET['sdate'] : null;
        $edate = isset($_GET['edate']) ? $_GET['edate'] : null;
        $sIdKey = isset($_GET['idkey']) ? $_GET['idkey'] : "system_batch_jobs___id";//adschedule___id,system_batch_jobs___id


        // 3222
        if (in_array($id, [30378])){
            $id = 14162;
        }elseif (in_array($id, [78772])){
            $id = 14292;
        }else{
            $id = \YcheukfCommon\Lib\Functions::decodeLafResouceId($id, str_replace("___id", "", $sIdKey));

        }
        // var_dump(\YcheukfCommon\Lib\Functions::encodeLafResouceId(3222, "system_batch_jobs"));
        // var_dump($sIdKey);
        // var_dump($id);


        $debug = isset($_GET['debug']) ? $_GET['debug'] : 0;
        if (is_null($id)) {
            echo "need param id";
            exit;
        }
        if (is_null($sdate)) {
            echo "need param sdate";
            exit;
        }
        if (is_null($edate)) {
            echo "need param edate";
            exit;
        }

        switch ($sIdKey) {
            case 'adschedule___id':

                $result = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'adschedule', $id);
                if (count($result) < 1) {
                    echo "no such id";
                    exit;
                }
                $aJson2 = array(
                    "show" => array(
                        array("id"=>$result['id'], "url"=>$result['code']),
                    ),
                    "click" => array(
                        array(),
                    ),
                );
                break;
            
            default:
                $result = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'system_batch_jobs', $id);

// var_dump($result);
                if (empty($result['out_json']) || is_null($result['out_json'])) {
                    echo "error out_json";
                    exit;
                }
                $aJson2 = json_decode($result['out_json'], 1);
            break;
        }

        if(!is_null($aJson2)){
            foreach ($aJson2['show'] as $k1 => $aTmp2) {
                if (isset($aTmp2['id'])) {
                    $aUrlids[] = $aTmp2['id'];
                }
            }
            if (isset($aJson2['click']) && count($aJson2['click'])) {
                foreach ($aJson2['click'] as $k1 => $aTmp2) {
                    if (isset($aTmp2['id'])) {
                        $aUrlids[] = $aTmp2['id'];
                    }
                }
            }

            $sSql = "select * from b_report_e where de1 in (".implode(",", $aUrlids).") and date>='".$sdate."' and date<='".$edate."' ORDER BY FIELD(de1,".implode(",", $aUrlids)."),date";
            // echo($sSql);

            if ($debug==3) {
                list($bFalg, $aReportDataTmp2) = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrReportDataForCounterTable( $aUrlids, $sdate, $edate, 3);
                echo "<pre>";
                print_r($aReportDataTmp2);
                echo "</pre>";
            }

           $aConfig = $this->getServiceLocator()->get('config');
            $oPdo = (\Application\Model\Common::getPDOObejct($this->getServiceLocator()));
           $oQuery = $oPdo->query($sSql);
           $aOutput = $aId2Data = $aDates = array();
           if ($oQuery) {
               while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){
                    if (!isset($aId2Data[$aResult['de1']][$aResult['date']])) {
                        $aId2Data[$aResult['de1']][$aResult['date']] = 0;
                    }
                    $aId2Data[$aResult['de1']][$aResult['date']] += $aResult['me1'];
                    $aDates[$aResult['date']] = $aResult['date'];
                }
           }
            // var_dump($aJson2);
            foreach ($aDates as $sDateTmp) {
                foreach ($aJson2['show'] as $k2 => $aTmp3) {
                    $nClickId = isset($aJson2['click'][$k2]['id']) ? ($aJson2['click'][$k2]['id']) : "";
                    $nClickUrl = isset($aJson2['click'][$k2]['url']) ? ($aJson2['click'][$k2]['url']) : "";
                    $nShow = isset($aJson2['show'][$k2]['id']) && isset($aId2Data[$aJson2['show'][$k2]['id']]) && isset($aId2Data[$aJson2['show'][$k2]['id']][$sDateTmp]) ? $aId2Data[$aJson2['show'][$k2]['id']][$sDateTmp] : 0;
                    $nClick = isset($aJson2['click'][$k2]['id']) && isset($aId2Data[$aJson2['click'][$k2]['id']])  && isset($aId2Data[$aJson2['click'][$k2]['id']][$sDateTmp]) ? $aId2Data[$aJson2['click'][$k2]['id']][$sDateTmp] : 0;

                    $sShowLink = "<a href='".$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$aJson2['show'][$k2]['id']."'>".$aJson2['show'][$k2]['url']."</a>";
                    $sClickLink = "<a href='".$this->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'histadschedule'))."?id=".$nClickId."'>".$nClickUrl."</a>";
                    if ($debug) {
                        $aOutput[] = array(
                            $nShow,
                            $nClick,
                            $sDateTmp,
                            $sShowLink,
                            $aJson2['show'][$k2]['id'],
                            $sClickLink,
                            $nClickId,
                        );
                   }else{
                        $aOutput[] = array(
                            $sDateTmp,
                            $nShow,
                            $nClick,
                        );
                    }
                }
            }


            echo $this->_htmloutput($aOutput, "history report");
            exit;

        }else{
            echoMsg('['.__CLASS__.'] wrong json format '.$result['out_json']);
        }
        exit;



    }


    public function viewVpnFaildHisAction(){
        $aHis = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getVpnFaildHistoryByAccId($_GET['id']);

            echo <<<__EOF
            <table border=1>
            <tr>
                <th>时间</th>
                <th>VPN账号</th>
                <th>VPN 拨号地域</th>
                <th>VPN 拨号ADR机器</th>
            </tr>
__EOF;
        $aMetadataList[LAF_METADATATYPE_ADRSERVER] = \Application\Model\Common::getResourceMetadaList($this->serviceManager, LAF_METADATATYPE_ADRSERVER);

        foreach ($aHis as $s) {
            $aTmp = json_decode($s, 1);
            $sServerLable = $aMetadataList[LAF_METADATATYPE_ADRSERVER][$aTmp['s']];
            echo <<<__EOF
            <tr>
                <td>{$aTmp['t']}</td>
                <td>{$aTmp['a']['id']} / {$aTmp['a']['vpnstore']} / {$aTmp['a']['account']}</td>
                <td>{$aTmp['h']['host']} / {$aTmp['h']['city']}</td>
                <td>{$aTmp['s']} / {$sServerLable}</td>
            </tr>
__EOF;
        }
        echo '</table>';

        // print_r($aHis);
        exit;
    }
    
    public function viewacrossmappingAction(){
        $aTagList = $this->getServiceLocator()->get('\Lur\Service\Across')->getTaglist();

        // $aTmp = $this->getServiceLocator()->get('\Lur\Service\Across')->getRtbDataByTagId(99314);
        // var_dump($aTmp);
        $aViewParams = array(
            "aTagList" => $aTagList,
        );
        return new ViewModel($aViewParams);

    }
    public function hunanreportAction(){

        $daylyconfig = require '/app/module/Lur/config/adr.shell.script.dayli.php';

        $currentdate  = isset($_GET['date']) ? $_GET['date'] : date("Ymd", strtotime("-1 day"));

        $date_sub = date("Ymd", strtotime("-1 day", strtotime($currentdate)));
        $dates = [$currentdate, $date_sub];
        $index = 1;
        $batchResult = [];
        $hourdatas = [];
        foreach ($daylyconfig as $key1 => $shell) {
            foreach ($dates as $date) {
                if (!isset($hourdatas[$date])) {
                    $hourdatas[$date] = [];
                }
                if (!isset($batchResult[$date])) {
                    $batchResult[$date] = [];
                }
                list($crontime, $duration, $taskafter) = explode(",", $key1);
                preg_match_all('/batch\/(.*.script)/i', $shell, $matches);

                $shell1 = "";
                if ($matches
                    && isset($matches[1])
                    && isset($matches[1][0])
                    && !empty($matches[1][0])
                ) {
                    $shell1 = "php /app/module/Lur/public/adrscript/batch/".str_replace( '.script', '.result.php', $matches[1][0])." ".date("Ymd", strtotime("+1 day", strtotime($date))).$taskafter;
                }
                unset($matches);

                if (!empty($shell1)) {
                    exec($shell1, $output);
                    $output2 = join("\n", $output);
                    unset($output);

                    preg_match_all('/\[result\-s\](.*)\[result\-e\]/is', $output2, $matches);
                    if ($matches
                        && isset($matches[1])
                        && isset($matches[1][0])
                        && !empty($matches[1][0])
                    ) {
                        $batchResult[$date][$taskafter] = $matches[1][0];
                        unset($matches);

                    }


                    // 处理小时数据, 小时错误数据
                    $labels = ['errorhourdata', 'hourdata'];

                    foreach ($labels as $label) {
                        if (!isset($hourdatas[$date][$label])) {
                            $hourdatas[$date][$label] = [];
                        }

                        preg_match_all('/\['.$label.'\-s\](.*)\['.$label.'\-e\]/is', $output2, $matches);
                        if ($matches
                            && isset($matches[1])
                            && isset($matches[1][0])
                            && !empty($matches[1][0])
                        ) {
                            $tmp1 = explode("\n", trim($matches[1][0]));
                            foreach ($tmp1 as $line1) {
                                preg_match_all('/planid=(.*);hourdata=(.*)/is', $line1, $matches2);

                                if ($matches2
                                    && isset($matches2[1])
                                    && isset($matches2[1][0])
                                    && !empty($matches2[1][0])
                                ) {
                                    $hourdatas[$date][$label][$matches2[1][0]] = json_decode($matches2[2][0], 1);
                                }
                            }

                        }
                        unset($matches);
                        
                    }

                }
            }
        }


        /**
         * #######closure function start#######
         */
        $fErrorCompare = function($hourdatas=null) 
        {
            $aReturn = array();
            $maps = ['status-201', 'statusmsg-253'];
            foreach ($hourdatas as $date =>$row2) {
                $aReturn[$date] = [];
                foreach ($row2['errorhourdata'] as $k => $row) {
                    foreach ($maps as $k2) {
                        if (preg_match("/".$k."/", $k2)) {
                            $aReturn[$date][$k] = [
                                'data' => $row,
                                'type' => $k2=='status-201' ? 'line' : 'bar',
                                'stack' => $k2,
                            ];
                        }
                    }
                }
            }
        
            return $aReturn;
        };
        $aErrorCompare = $fErrorCompare($hourdatas);
// print_r($aErrorCompare);
//         exit;
        //unset($fClosureFunc);
        
        $aViewParams = array(
            "aErrorCompare" => $aErrorCompare,
            "batchResult" => $batchResult,
            "hourdatas" => $hourdatas,
            // "hourerrorchartdata" => $hourdatas['errorhourdata'],
            // "hourchartdata" => $hourdatas['hourdata'],
            "hourchartdata_t" => [2.90,1.70,0.98,0.66,0.53,0.68,1.40,2.28,3.22,3.94,4.55,5.76,8.54,5.42,4.34,4.18,4.32,5.04,6.37,6.82,7.16,7.46,6.80,4.90],
            "date" => $currentdate,
            // "a1" => $a1,
            // "a2" => $a2,
            // "aAliveMachines" => $aAliveMachines,
            // "lastActiveTime" => $lastActiveTime,
        );
        return new ViewModel($aViewParams);
    }

    public function viewadrscriptsAction(){

        $oReids = \Application\Model\Common::getCacheObject($this->getServiceLocator());

        $taskid = $oReids->get(LAF_CACHE_CURRENT_SHELLID_137);
        $sShellKeySucc = LAF_CACHE_ADRSHELLLIST_OK_138."/".$taskid;
        $a1 = $oReids->hGetAll($sShellKeySucc);
        $sShellKeyLock = LAF_CACHE_ADRSHELLLIST_136."/".$taskid;
        $a2 = $oReids->hGetAll($sShellKeyLock);
        $aAliveMachines = $this->serviceManager->get("\Lur\Service\Addtional")->getAliveList();
        // print_r($aAliveMachines);
        $aAdrSummary = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrSummary();
        $lastActiveTime = $oReids->get(LAF_CACHE_CURRENT_SHELLID_LASTACTIVE_139);


        $aViewParams = array(
            "taskid" => $taskid,
            "aAdrSummary" => $aAdrSummary,
            "a1" => $a1,
            "a2" => $a2,
            "aAliveMachines" => $aAliveMachines,
            "lastActiveTime" => $lastActiveTime,
        );
        return new ViewModel($aViewParams);

    }

    public function pvforecastAction(){
        if (isset($_GET['date'])) {
            $date = $_GET['date'];
        }else{
            $date = date("Ymd", strtotime("-1 day"));

        }
        $aprovicesRes = $this->getServiceLocator()->get('\Lur\Service\Common')->province2cities();

        list($city2ips) = $this->getServiceLocator()->get('\Lur\Service\Common')->getCity2IpHis($date);

        $aSerarchIpResult = [];
        if (isset($_POST['ips'])) {
        }

        // var_dump($city2ips);

        $aViewParams = array(
            // 'aUsVpnCityStatus' => $aUsVpnCityStatus,
            // 'aIacIpInfo' => $aIacIpInfo,
            // 'aIpTotals' => $aIpTotals,
            // 'aVpnIpHis' => $aVpnIpHis,
            // 'nBlackIpCounter' => $nBlackIpCounter,
            'aprovicesRes' => $aprovicesRes,
            'city2ips' => $city2ips,

        );
        return new ViewModel($aViewParams);

    }

    public function viewvpniphisAction(){
        list($aVpnIpHis, $nBlackIpCounter, $aIp2Iac) = $this->getServiceLocator()->get('\Lur\Service\Common')->getVpnIpHis($_GET['date']);

        $aSerarchIpResult = [];
        if (isset($_POST['ips'])) {
            $sPostIp = preg_replace('/\r\n/', ";;;", $_POST['ips']);
            $aLines = explode(";;;", $sPostIp);


            // 
            $aUsVpnIp2City = $this->getServiceLocator()->get('\Lur\Service\Common')->getVpnIp2City($_GET['date']);
            // print_r($aLines);

            foreach ($aLines as $sIp) {
                $sIp = trim($sIp);
                if (isset($aVpnIpHis[$sIp])) {
            // print_r($aVpnIpHis[$sIp]);

                    $aSerarchIpResult[] = $sIp.' => '.print_r($aVpnIpHis[$sIp], 1).'; '.(isset($aUsVpnIp2City[$sIp])?print_r($aUsVpnIp2City[$sIp], 1): "");
                }
            }
            // print_r($aSerarchIpResult);

        }

        list($aUsVpnCityStatus, $aIpTotals) = $this->getServiceLocator()->get('\Lur\Service\Common')->getVpnCityStats($_GET['date']);
        $aIacIpInfo = $this->getServiceLocator()->get('\Lur\Service\Common')->getIacIpInfo();


        $aViewParams = array(
            'aSerarchIpResult' => $aSerarchIpResult,
            'aUsVpnCityStatus' => $aUsVpnCityStatus,
            'aIacIpInfo' => $aIacIpInfo,
            'aIpTotals' => $aIpTotals,
            'aVpnIpHis' => $aVpnIpHis,
            'nBlackIpCounter' => $nBlackIpCounter,
            'aIp2Iac' => $aIp2Iac,
            

        );
        return new ViewModel($aViewParams);

    }
    
    public function isblackipAction(){

        $aBlackIpResult = [];
        if (isset($_POST['ips'])) {
            $sPostIp = preg_replace('/\r\n/', ";;;", $_POST['ips']);
            $aLines = explode(";;;", $sPostIp);

            foreach ($aLines as $sRow) {
                preg_match_all('/(\d*\.\d+\.\d+\.\d+).*/', $sRow, $aMatches);

                if (count($aMatches) 
                    && isset($aMatches[1])
                    && isset($aMatches[1][0])
                ) {
                    $sIpTmp = $aMatches[1][0];
                    $bFlag = $this->getServiceLocator()->get('\Lur\Service\Common')->isInBlackIpList($aMatches[1][0]);

                    if ($bFlag) {
                        $aBlackIpResult[$aMatches[1][0]] = $sRow;
                    }
                }

            }
        }
        $aViewParams = array(
            'aBlackIpResult' => $aBlackIpResult,
        );
        return new ViewModel($aViewParams);

    }

    public function viewadrserversAction(){

        $aTmp = array();
        $aHistoryAlivesAdrServers = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getAliveList();
        foreach ($aHistoryAlivesAdrServers as $nAdrId) {
            $aTmp[$nAdrId] = ($this->getServiceLocator()->get('\Lur\Service\Common')->getAdrServerLastStats($nAdrId));
        }

        $aM1009 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1009);

        $aAdrSummary = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrSummary();
        
        list($aUsVpnCityStatus, $aIpTotals) = $this->getServiceLocator()->get('\Lur\Service\Common')->getVpnCityStats(date("Ymd", strtotime("-0 day")));

        $aBlackIpInfo = $this->getServiceLocator()->get('\Lur\Service\Common')->getBlackIpInfo();

        $aIacIpInfo = $this->getServiceLocator()->get('\Lur\Service\Common')->getIacIpInfo();

        $sCacheKey4 = LAF_CACHE_CODERUNTIME_REPORT_352."/".date("Ymd");
        $aCodeMonitor = $this->getServiceLocator()->get('\Lur\Service\Common')->getAddtionalRedis()->hGetAll($sCacheKey4);

        $aViewParams = array(
            'aAdrStats' => $aTmp,
            'aM1009' => $aM1009,
            'aAdrSummary' => $aAdrSummary,
            'aCodeMonitor' => $aCodeMonitor,
            'aUsStats' => $this->getServiceLocator()->get('\Lur\Service\Common')->getUsLastData(),
            'aUsVpnCityStatus' => $aUsVpnCityStatus,
            'aIpTotals' => $aIpTotals,
            'aBlackIpInfo' => $aBlackIpInfo,
            'aIacIpInfo' => $aIacIpInfo,
        );

        // var_dump($this->getServiceLocator()->get('\Lur\Service\Common')->getAllRunninngCityIds());
// exit();
        return new ViewModel($aViewParams);
        // return $this->response;

    }


    
    public function chg2vpnAction(){
        $nAdrId = isset($_GET['nAdrId']) ? $_GET['nAdrId'] : null;
        $sLabel1 = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_ADRSERVER, $nAdrId);
        $sLabel2 = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021, $nAdrId);

        if (empty($sLabel2)) {
            \Application\Model\Common::saveMetadata($this->serviceManager, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021, $nAdrId, $sLabel1);
            echo 'DONE';
            
        }else{
            echo 'change alredy';
        }

        // var_dump($sLabel1);
        // var_dump($sLabel2);
        exit;
        
    }
    public function viewadrserverinfoAction(){
        $nAdrId = isset($_GET['nAdrId']) ? $_GET['nAdrId'] : null;
        $aAdrSummary = $this->getServiceLocator()->get('\Lur\Service\Common')->getAdrSummary();

        $aLastStats = ($this->getServiceLocator()->get('\Lur\Service\Common')->getAdrServerLastStats($nAdrId));


        $aViewParams = array(
            "nAdrId" => $nAdrId,
            "aLastStats" => $aLastStats,
            'aAdrSummary' => $aAdrSummary,
        );

        // var_dump($aAllVpnCities);


        return new ViewModel($aViewParams);
    }

    public function viewlackingcityinfoAction(){
        $nCityId = isset($_GET['nCityId']) ? $_GET['nCityId'] : null;
        $sLabel1007 = \Application\Model\Common::getResourceMetaDataLabel($this->serviceManager, 1007, $nCityId);


        // $configs = $this->getServiceLocator()->get('config');
        // var_dump($configs['adr_report_url']);

        $aViewParams = array(
            "nCityId" => $nCityId,
            "sLabel1007" => $sLabel1007,
        );

        // var_dump($aAllVpnCities);


        return new ViewModel($aViewParams);
    }

    public function viewbadcodeAction()
    {
        $sCacheKey4 = LAF_CACHE_CODERUNTIME_REPORT_352."/".date("Ymd");
        $aCodeMonitor = $this->getServiceLocator()->get('\Lur\Service\Common')->getAddtionalRedis()->hGetAll($sCacheKey4);

        $return1 = [];
        $nOpUserId = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();

        foreach ($aCodeMonitor as $codeId=>$row1) {
            $row2 = json_decode($row1, 1);
            $batchids = \YcheukfCommon\Lib\Functions::getRelationList($this->getServiceLocator(), 1016, $codeId, 1);
            arsort($batchids);
            $batchid = current($batchids);
            $batchdata = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'system_batch_jobs', $batchid);


            $userdata = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'b_adcustomer', $batchdata['user_id']);
            $userdata2 = \Application\Model\Common::getResourceById($this->getServiceLocator(), 'system_users', $batchdata['op_user_id']);



            
            // $key2 = $userdata2['id']."-".$userdata['id']."-".$batchdata['id']."-".$row2[4]."-".$key2;
            $key2 = $userdata2['id']."-".$userdata['id']."-".$batchdata['id']."-".$row2[4];
            // print_r($codeId);
            // print_r($batchdata);

            if ($nOpUserId==1) {
                $return1[($key2)] = [
                    $userdata2['id']."/".$userdata2['username'],
                    $userdata['id']."/".$userdata['name'],
                    $codeId,
                    $row2[1],
                    $batchdata['id'],
                    $batchdata['memo'],
                    $batchdata['modified'],
                    $row2[4],
                ];
            }else{
                if ($nOpUserId==$userdata2['id']) {
                    $return1[($key2)] = [
                        $userdata2['id']."/".$userdata2['username'],
                        $userdata['id']."/".$userdata['name'],
                        $codeId,
                        $row2[1],
                        $batchdata['id'],
                        $batchdata['memo'],
                        $batchdata['modified'],
                        $row2[4],
                    ];
                }
            }

        }
        krsort($return1);
        //     print_r($return1);

        // exit;


        $aViewParams = array(
            'aCodeMonitor' => $return1,
            'nOpUserId' => $nOpUserId,
        );
        return new ViewModel($aViewParams);
    }
    public function viewvpnstroestatsAction(){
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->getServiceLocator());
        $keys = $oReids->keys(LAF_CACHE_VPNIPSTORES_143."*");

        $datas = [];
        foreach ($keys as $key) {
            $datas[str_replace(LAF_CACHE_VPNIPSTORES_143, "", $key)] = json_decode($oReids->get($key), 1);
        }
        $getAllVpnAccs = $this->getServiceLocator()->get('\Lur\Service\Common')->getAllVpnAccs();
        $aStroe2Accs = [];
        foreach($getAllVpnAccs as $row){
            if (!isset($aStroe2Accs[$row['vpnstore']])) {
                $aStroe2Accs[$row['vpnstore']] = 0;
                
            }
            $aStroe2Accs[$row['vpnstore']]++;
        }
        // print_r($getAllVpnAccs);
        $aViewParams = array(
            "datas" => $datas,
            "aStroe2Accs" => $aStroe2Accs,
        );
        // print_r($aViewParams);

        return new ViewModel($aViewParams);

    }

    public function viewvpncitiesAction()
    {
        $aAllVpnCities = $this->getServiceLocator()->get('\Lur\Service\Common')->getAllVpnCities();

        $aLackingCounts = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getLackingCityCount("label");

        $aLackingCounts_yesterday = $this->getServiceLocator()->get('\Lur\Service\Addtional')->getLackingCityCount("label", date("Ymd", strtotime("-1 day")));

        $findcitysresult = [];
        if (isset($_POST) && isset($_POST['findcitys'])) {
            
            $sTmp3 = preg_replace('/\r\n/', ";;;", $_POST['findcitys']);
            $aLines = explode(";;;", $sTmp3);

            // \Application\Model\Common::setUseLafCacheFlag(false);

            $aM1007 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1007);
            // var_dump($aM1007);
            foreach ($aLines as $sCity) {
                $sCity = trim($sCity);
                if (empty($sCity)) {
                    continue;                    
                }
                if (isset($aAllVpnCities[$sCity])) {
                    $nCityTotal = 0;
                    if (in_array($sCity, $aM1007)) {
                        foreach ($aAllVpnCities[$sCity] as $sVpnStroy => $aRow2) {
                          $nCityTotal += $aRow2['acccount'];
                        }
                        if ($nCityTotal==0) {
                            $findcitysresult[$sCity] = "[EMPTY]存在城市, 但没有账号 ";
                        }elseif ($nCityTotal <= 20) {
                            $findcitysresult[$sCity] = "[LACK]账号少,账号数量 ".$nCityTotal;
                        }else{
                            $findcitysresult[$sCity] = "[OK]可正常拨号,账号数量 ".$nCityTotal;
                        }
                    }else{
                        $findcitysresult[$sCity] = "[NONE]系统MAPPING表无该城市, 请添加后再尝试";
                    }

                }else{
                    $findcitysresult[$sCity] = "[NONE]VPN无此城市";
                }
            }

        }

        $aViewParams = array(
            "aLackingCounts" => $aLackingCounts,
            "aLackingCounts_yesterday" => $aLackingCounts_yesterday,
            "aAllVpnCities" => $aAllVpnCities,
            "findcitys" => $findcitysresult,
        );

        // var_dump($aAllVpnCities);


        return new ViewModel($aViewParams);
    }
    /**
     * 查看各ADR机器上的状态
     */
    /**
     * 查看各机器上的排期
     */
    public function viewscheduleAction(){

        $aM1009 = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), 1009);
        $aConfig = $this->getServiceLocator()->get('config');
        $aTmp = array();
        foreach($aM1009 as $k=>$v){
            $sTmp = $this->getServiceLocator()->get('\Lur\Service\Common')->getScheduleByAdrId($k);
            $aTmp2 = json_decode($sTmp, 1);
            $aTmp3 = array();
                    // var_dump($aTmp2['dataset']);

            if (isset($aTmp2['dataset']) && is_array($aTmp2['dataset']) && count($aTmp2['dataset'])) {
                foreach ($aTmp2['dataset'] as $k2 => $v2) {
                    $aTmp3[$v2['id']] = array(
                        'url' => $v2['url'],
                    );
                    # code...
                }
            }
            $aTmp[$k] = array(
                'label' => $v,
                'values' => $aTmp3
            );
            // var_dump($aTmp);
            // if(!is_null($aTmp)){
            //     echo "<hr>";
            //     echo $k."/".$v."/".$aTmp['count'];
            //     echo "<hr>";
            //     echo "<pre>";
            //     print_r($aTmp['dataset']);
            //     echo "</pre>";
            // }
        }

        $aViewParams = array(
            'aSchedule' => $aTmp,
        );
        return new ViewModel($aViewParams);
        // return $this->response;

    }
}