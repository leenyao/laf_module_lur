<?php

namespace Lur\Controller;



use \Lur\Service\HashRedis;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ProxyipController extends \Application\Controller\LafController
{

	public function testAction(){
		$date = date("Ymd");
		$plat = "uuhttp";
		$cities = [18, 19, 48];
		$data = [];
		$datamerger = [];
		foreach($cities as $city){
	        $cachekey2 = ADRKEY_IPHIS_CITY2IPS_371.$date."/".$plat."/".$city;
			// var_dump($cachekey2);

			$data[$city] = HashRedis::sMembers($cachekey2);
			$datamerger = array_merge($datamerger, HashRedis::sMembers($cachekey2));

		}
		var_dump($data);
		var_dump(count($datamerger));
		$datamerger = array_unique($datamerger);
		var_dump(count($datamerger));

		foreach($data[18] as $ip1){
			if (in_array($ip1, $data[19])) {
				var_dump($ip1);
			}
		}

		exit();
	}
	public function cityipsAction(){
		$cities = ['南京','合肥','厦门','昆明','南宁','成都','沈阳', '长沙','天津','济南', '北京', '上海', '武汉', '宁波', '重庆', '广州','苏州'];

		$data = [];
		$aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), LAF_METADATATYPE_LOCATION);
		foreach($aCityId2Label as $cityid => $label){
			if (in_array($label, $cities)) {

				$data[$cityid] = [
					'id' => $cityid,
					'label' => $label,
					'sum' => 0,
					'row' => [],
					'supply' => [],
				];
			}
		}


		$date = date("Ymd");
		$citystats = [];
		$city2sum = [];
		$plats = $this->getServiceLocator()->get('Lur\Service\Proxyip')->getPlats($date);
		foreach($plats as $plat){
			$row = $this->getServiceLocator()->get('Lur\Service\Proxyip')->getCityStatsByPlat($plat, $date);
			if (count($row)) {
				$citystats[$plat] = $row;
				foreach($row as $cityid => $count){
					$citylabel = $aCityId2Label[$cityid];
					if (!isset($city2sum[$citylabel])) {
						$city2sum[$citylabel] = 0;
					}
					$city2sum[$citylabel] += $count;
					if (isset($data[$cityid])) {
						$data[$cityid]['sum'] += $count;
						$data[$cityid]['supply'][] = $plat;
					}
				}
			}
		}

		arsort($city2sum);

		print_r($city2sum);

		// var_dump("citystats", $citystats);
		var_dump("data", $data);
		echo "<pre>";
		$sCsv = "城市\t ip数\t cpm\t 供应商\n";
		foreach($data as $row){
			$sCsv .= $row['label']."\t".$row['sum']."\t".($row['sum']/100)."\t".(join(",", $row['supply']))."\n";

		}
		echo $sCsv;
		echo "</pre>";

		exit();

	}

	public function statsAction(){

		$date = date("Ymd");
		if (isset($_GET['date'])) {
			$date = $_GET['date'];
		}
		// 
		$adrids = $this->getServiceLocator()->get('Lur\Service\Proxyip')->getAdrIdPool($date);
		$plats = $this->getServiceLocator()->get('Lur\Service\Proxyip')->getPlats($date);
		// $plats = ['jiguang'];

		$aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->getServiceLocator(), LAF_METADATATYPE_LOCATION);
		//var_dump("aCityId2Label", $aCityId2Label);



		$province2cities = $this->serviceManager->get('Lur\Service\Common')->province2cities();
		//var_dump("province2cities", $province2cities);


		//var_dump("date", $date);
		// var_dump("adrids", $adrids);
		//var_dump("plats", $plats);

		$summary = $this->getServiceLocator()->get('Lur\Service\Proxyip')->getSummary($date);
		//var_dump("summary", $summary);

		$stats = [];
		foreach($plats as $plat){
			$stats[$plat] = $this->getServiceLocator()->get('Lur\Service\Proxyip')->getIpstatsByPlat($plat, $date);
		}
		//var_dump("stats", $stats);

		$codes = [];
		foreach($plats as $plat){
			$row = $this->getServiceLocator()->get('Lur\Service\Proxyip')->getCodesByPlat($plat, $date);
			if (count($row)) {
				$codes[$plat] = $row;
			}
		}
		//var_dump("codes", $codes);

		
		$citystats = [];
		foreach($plats as $plat){
			$row = $this->getServiceLocator()->get('Lur\Service\Proxyip')->getCityStatsByPlat($plat, $date);
			if (count($row)) {
				$citystats[$plat] = $row;
			}
		}

        $aLackingCity2Count = $this->getServiceLocator()->get('Lur\Service\Addtional')->getAllCityCounter($date);

        list($sp2cities, $citiesbanded) = $this->getServiceLocator()->get('Lur\Service\Addtional')->sp2cities();

		$aViewParams = [
            "citystats" =>$citystats,
            "stats" => $stats,
            "province2cities" => $province2cities,
            "aLackingCity2Count" => $aLackingCity2Count,
            "sp2cities" => $sp2cities,

        ];
        // print_r($aViewParams['sp2cities']);
        // exit();
		return new ViewModel($aViewParams);

	}





}