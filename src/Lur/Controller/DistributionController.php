<?php
/**
 
sh与销售的流量分配

 */

namespace Lur\Controller;



use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DistributionController extends \Application\Controller\LafController
{

	public function testAction(){
		var_dump("OK");
		exit();
	}
	public function updcacheAction(){
		$redis = \Application\Model\Common::getCacheObject($this->serviceManager);
		$t = date('Y-m-d H:i:s');
		$redis->set(LAF_CACHE_DISTRICBUTIONAPI_APICACHE_816.'sohu', $t);

		echo $t;
		exit();
	}


	public function getCsvFile($id){
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"));
        $sSql = "select * from `system_batch_jobs` where id= ".$id;
        $sth = $oPdo->prepare($sSql);
        $sth->execute();
        $row = $sth->fetch(\PDO::FETCH_ASSOC);
        $file = "/app/public".$row['in_file'];
        $return = \Application\Model\Common::getCsvData($file);
        if (isset($return[0])){
        	$iii = 0;
        	foreach($return[0] as $i=>$title){
        		if ($title == 'date-start-column'){
        			$iii = $i;
        		}
        		$title = trim($title);
        		$split = explode(" ", $title);
        		if ($i>$iii && !empty($iii) && strtotime(trim($split[0])) !== false){
	        		$date = trim($split[0]);
	        		$date2 = date("Y-m-d", strtotime($date));
	        		$split[0] = $date2;
	        		$title = join(" ", $split);
	        		$return[0][$i] = $title;
        		}
        	}
        }

        return $return;
        // return $content;

	}

	public function detailAction(){

		// $id = $_GET['id'];
		$plan = $_GET['plan'];
		$type = $_GET['type'];
		// mgdetail
		$detail = [];
		try {
			$planrow = $this->getByPlanno($plan);
			$id = $planrow['id'];
			switch ($type) {
				case 'shmain':
					$detail = $this->getCsvFile($id);
					// exit();
					break;
				default:
				case 'shdown':
					$detail = $this->getCsvFile($id);
					$lastop =  $this->serviceManager->get('Lur\Service\Common')->getLastHisOp2('shmain', $plan);
					list($show_activity, $pplanno, $csvcontent, $detail) = $this->_ftmDetail($plan, $type, $detail, $lastop['percent'], $lastop['grep_configs'], $lastop['datereplace']);
					break;
				case 'shself':
					$detail = $this->getCsvFile($id);
					$lastop =  $this->serviceManager->get('Lur\Service\Common')->getLastHisOp2('shmain', $plan);
					list($show_activity, $pplanno, $csvcontent, $detail) = $this->_ftmDetail($plan, $type, $detail, 100-$lastop['percent'], $lastop['grep_configs'], $lastop['datereplace']);

					break;
			}
			$aViewParams = array(
				'plan' => $plan,
				'detail' => $detail,
			);
			$oViewModel = new ViewModel($aViewParams);
            $oViewModel->setTemplate("lur/admin/mgdetail");
			return $oViewModel; 

		} catch (Exception $e) {
			
				echo "plan error.".$e->getMessage();
				exit();
		}
	}
	
	public function planeditAction($type='shmain' , $plan=null, $percent=null, $status=0, $grep=null, $datereplace=null){

		$newplan = 0;
		if(isset($_GET['type']))
			$type = $_GET['type'];
	    if (strtolower(PHP_SAPI) == 'cli') {
	    }else{
		    $sUserId = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();
		    if (!in_array($sUserId, [1, 2003])) {
		      throw new \Exception('inavailable action');
		    }

			if (isset($_GET['plan'])) {
				$plan = $_GET['plan'];
			}
			$percent = null;
			if ($_POST && isset($_POST['percent'])) {
				$percent = trim($_POST['percent']);
			}
			$grep = "";
			if ($_POST && isset($_POST['grep'])) {
				$grep = trim($_POST['grep']);
			}
			$datereplace = "";
			if ($_POST && isset($_POST['datereplace'])) {
				$datereplace = trim($_POST['datereplace']);
			}

		    $lastop =  $this->getServiceLocator()->get('Lur\Service\Common')->getLastHisOp2($type, $plan);
			$status = isset($lastop['status'])?$lastop['status']:0;
			$newplan = 1;

		    // 手动调整的单子
	    }
		$redis = \Application\Model\Common::getCacheObject($this->serviceManager);
		$msg = "";
		try {
			$cachekey1 = LAF_CACHE_DISTRICBUTIONAPI_OPHIS_810.$type."/".$plan;

			if (!is_null($percent) && !is_null($percent)) {
				if (is_numeric($percent) && $percent>=0 && $percent<=100) {
				}else{
					throw new \Exception('错误的百分比:'.$percent);
				}
				$grep_configs = $this->fmtMgplanGrep($grep);
				$datereplace = $this->fmtDatereplace($datereplace);

				if ($plan == 202037) {
					// var_dump($grep_configs);
					// exit;
				}


				$redis->hSet($cachekey1, time(), json_encode([
					'percent' => $percent,
					'status' => $status,
					'grep_configs' => $grep_configs,
					'grep' => $grep,
					'datereplace' => $datereplace,
					'applytime' => date("Y-m-d H:i:s"),
				]));

            	$redis->setTimeout($cachekey1, 100*86400);



				$cachekey2 = LAF_CACHE_DISTRICBUTIONAPI_NEW_807."/".$type;
				$cachekey3 = LAF_CACHE_DISTRICBUTIONAPI_STOP_808."/".$type;

				if ($newplan==1) {
					if (!$redis->hExists($cachekey2, $plan)) {
						// 若不是新增
						$newplan = 0;
					}
				}

				$redis->hDel($cachekey2, $plan);
				$redis->hDel($cachekey3, $plan);

				$dbdatas = [];
				if ($type == 'shmain'){
					$dbdatas[] = [
						'type' =>'shself',
						'op_user_id' =>2003,
						'percent' =>100-intval($percent),
						'memo' =>"(API-分单)",
						// 'savefalg' =>1,
					];
				}
				foreach($dbdatas as $row){
					$this->saveplan2db($row['op_user_id'], $row['type'], $plan, $row['percent'], $grep_configs, $newplan, $status, $datereplace, $row['memo']);

				}
				// }

				$msg = "下单成功! 请到<批量导入排期>检查执行结果, 然后更新缓存";

			}

			$aViewParams = array(
				'lastpercent' => !is_null($percent) ? $percent:0,
				'lastgrep' => !is_null($grep) ? $grep:"",
				'datereplace' => !is_null($datereplace) ? $datereplace:"",
				'msg' => $msg,
				'ophis' => $redis->hGetAll($cachekey1),
			);
			$oViewModel = new ViewModel($aViewParams);
            $oViewModel->setTemplate("lur/admin/mgplanedit");
			return $oViewModel;

		} catch (Exception $e) {
			
				echo " error.".$e->getMessage();
				exit();
		}
	}

	function _ftmDetail($planno, $type, $detail, $defaultpercent, $grep_configs, $datereplace=[]){
		$datereplace = is_array($datereplace) ? $datereplace : [];
		$return = $detail;
		$pplannos = [];
		$uaindex = 0;
		$sspidindex = 0;
		$saindex = 0;
		$show_activity = false;

		foreach($detail[0] as $i1 => $title){


			if(empty($sspidindex) && strtolower($title) == 'sspid'){
				$sspidindex = $i1;
			}

			if(empty($uaindex) && strtolower($title) == 'ua'){
				$uaindex = $i1;
			}
			if(empty($saindex) && strtolower($title) == 'show_activity'){
				$saindex = $i1;
			}

			if(preg_match('/^\d{4}-\d{2}-\d{2}/i', $title)){
				list($date) = explode(" ", $title);

	    	    $bmatch = false;
	    	    foreach ($datereplace as $matchdate) {
	    	    	if (preg_match("/^".$date."/", $matchdate)) {
	    	    		// var_dump($matchdate,$date );
				        $return[0][$i1] = $matchdate;
	    	    	}
	    	    }




				foreach($detail as $i2 => $row){
					// var_dump($saindex, $row[$saindex]);
					if($i2==0)continue;

					if(isset($row[$saindex]) && (empty($row[$saindex]) || intval($row[$saindex])==1)){
						$show_activity = true;
					}
		    	    $ua = 'pc';

		    	    $uas = json_decode($row[$uaindex], 1);
		    	    $uas = is_null($uas)?[intval($uas)]:$uas;


		    	    foreach($uas as $ua1){
		    	    	if(in_array($ua1, [2,3])){
				    	    $ua = 'phone';
		    	    	}elseif(in_array($ua1, [4])){
				    	    $ua = 'pad';
		    	    	}elseif(in_array($ua1, [1])){
				    	    $ua = 'pc';
		    	    	}elseif(in_array($ua1, [23])){
				    	    $ua = 'ott';
		    	    	}
		    	    }
		    	    // switch(strtolower())

					$num = $this->getNumByGrep($type, $row[$i1], $defaultpercent, 0, $date, $ua, $grep_configs);
					// var_dump($i1, $title, $i2, $i1, $row[$i1], $return[$i2][$i1], $num, $defaultpercent);
					$return[$i2][$i1] = $num;

					// sh向销售分配单子, 销售的单子需要打上adrid
					if($type=='shdown'){
						if(!preg_match('/\[adrid=/', $return[$i2][$sspidindex])){
							$return[$i2][$sspidindex] =  $return[$i2][$sspidindex]."[adrid=".str_replace("-", "", $planno)."]";
						}
					}

				}

			}

		}
		$content = "";
		// print_r($return);
		foreach($return as $i1 => $row){
			$content .= "\"".join("\",\"", $row)."\"\n";
		}
		return [$show_activity, $planno, $content, $return];
	}

	function saveplan2db($op_user_id, $type, $plan, $percent=100, $grep_configs=[], $newplan=0, $status=0, $datereplace=[], $memo=""){

		$redis = \Application\Model\Common::getCacheObject($this->serviceManager);
		$todaykey = $plan."-".date("Ymd");
		// 当天的小时逻辑
		$todayhourkey = LAF_CACHE_DISTRICBUTIONAPI_PLANDETAILHIS_813.$type;
		if ($newplan==1 && !$redis->hExists($todayhourkey, $todaykey)) {
			$redis->hSet($todayhourkey, $todaykey, date("H")."-23");
		}
		$planrow = $this->getByPlanno($plan);
		$detail = $this->getCsvFile($planrow['id']);

		// var_dump($plan);
		// var_dump($planrow);
		list($show_activity, $pplanno, $csvcontent, $detail2) = $this->_ftmDetail($plan, $type, $detail, $percent, $grep_configs, $datereplace);


		$dbfile = "/tmpupload/".$type."_".$plan."_".date("YmdHis").".csv";
		$savefile = "/app/public".$dbfile;
		file_put_contents($savefile, $csvcontent);

		// var_dump($savefile);

    	$oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave


		$sSql = "insert into system_batch_jobs set pplanno='".$plan."', planno='".$plan."', m158_id=1, in_file='".$dbfile."', memo='".$planrow['memo'].$memo."', op_user_id='".$op_user_id."'";
		// var_dump($sSql);

		$oSth = $oPdo->prepare($sSql);
		$oSth->execute();


		// 保存自己的量和配置
		list($show_activity, $pplanno2, $csvcontent2, $detail2) = $this->_ftmDetail($plan, 'shself', $detail, $percent, $grep_configs, $datereplace);
		$cachekey4 = LAF_CACHE_DISTRICBUTIONAPI_LASTDETAIL_814.'shself';
		$redis->hSet($cachekey4, $plan, json_encode($detail2));



		// 保存下游的量和配置
		list($show_activity, $pplanno2, $csvcontent2, $detail2) = $this->_ftmDetail($plan, 'shdown', $detail, 100-$percent, $grep_configs, $datereplace);
		$cachekey4 = LAF_CACHE_DISTRICBUTIONAPI_LASTDETAIL_814.'shdown';
		$redis->hSet($cachekey4, $plan, json_encode($detail2));

		// var_dump("show_activity", $show_activity, $percent, $grep_configs, $datereplace);
		// 保存列表
		if($show_activity && (100-$percent)==0 && empty($grep_configs) && empty($datereplace)){
			$show_activity = false;
		}
		if(!is_null($plan)){
			$cachekey5 = LAF_CACHE_DISTRICBUTIONAPI_LIST_815.'sohu';
			$redis->hSet($cachekey5, $plan, json_encode([
				'plan' => $plan,
				'code' => $show_activity?0:1,
				'sign' => md5(json_encode([$show_activity,$detail2])),
				'applytime' => date("Y-m-d H:i:s"),
			]));

		}
		
	}

	// 搜狐主账号
	public function shmainplanAction(){

		// 代理或者main
	    $sUserId = $this->getServiceLocator()->get('zfcuser_auth_service')->getIdentity()->getId();
	    if (!in_array($sUserId, [1, 2003])) {
	      throw new \Exception('inavailable action');
	    }


		$redis = \Application\Model\Common::getCacheObject($this->serviceManager);
		$aViewParams = array(
			'orders' => $this->getOrders(),
			'type' => 'shmain',
			// 'data_add' => $redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_NEW_207),
			// 'data_stop' => $redis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_STOP_208),

		);
		// print_r($aViewParams);
		return new ViewModel($aViewParams);
	}


	public function getByPlanno($planno)
	{
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"));
        $sql = "select id,memo,created,in_file,planno,pplanno from system_batch_jobs where m158_id=10 and planno='".$planno."' and pplanno='' order by created desc limit 1";

        $sth = $oPdo->prepare($sql);
        $sth->execute();
        $aTmp = $sth->fetch(\PDO::FETCH_ASSOC);

        return $aTmp;
	}

	public function getOrders($conf=['op_user_id'=>2003])
	{
        $oPdo = (\Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"));
        $sql = "select id,memo,created,planno from system_batch_jobs where m158_id=10 and planno<>'' and pplanno='' and op_user_id=".$conf['op_user_id']." order by created desc ";
        // echo $sql;
        $sth = $oPdo->prepare($sql);
        $sth->execute();
        $aTmp = $sth->fetchAll(\PDO::FETCH_ASSOC);
        $aReturn = [];
        foreach ($aTmp as $aRow){ 
        	$aReturn[$aRow['planno']] = [
        		'id' => $aRow['id'],
        		'memo' => $aRow['memo'],
        		'created' => $aRow['created'],
        		'planno' => $aRow['planno'],
        	];
        }

        
        // print_r($sql);
        // print_r($aReturn);
        return($aReturn);
	}


	function fmtMgplanGrep($grep='')
	{
		$return = [];
		$return2 = [];
		if (!empty($grep)) {
			$greps = explode("\r\n", $grep);
			// $setups = [];
			if (count($greps)) {
				foreach ($greps as $line) {
					$line = trim($line);
					$line = strtolower($line);
					if (empty($line)) {
						continue;
					}
					$vars = explode("|", $line);
					$line2 = [];
					foreach ($vars as $var) {
						list($v1, $v2) = explode("=", $var);
						$v1 = trim($v1);
						$v2 = trim($v2);
						$line2[$v1] = $v2;
					}
					$return[] = $line2;
				}
			}
			if (count($return)) {
				foreach ($return as $k=> $row) {
					$row3= [];
					if (isset($row['p'])
							&&is_numeric($row['p'])
							&&intval($row['p']) >=0
							&&intval($row['p']) <=100
						) {
						$row3['p'] = intval($row['p']);
					}else{
						throw new \Exception('参数p错误, 必须存在且介于0-100之间');
					}
					if (isset($row['u']))  {
						$row3['u'] = [];
						$us = explode(",", $row['u']);
						foreach ($us as $ua) {
							$row3['u'][] = $ua = trim($ua);
							if (!in_array($ua, ['pc', 'phone', 'pad'])){
								throw new \Exception('参数u错误, 必须为 pc,phone,pad');
							}
						}

					}
					if (isset($row['d']))  {
						$dates2 = $this->_chkDateFormat($row['d']);
						if ($dates2===false){
							throw new \Exception('参数d错误, 必须为正确的日期模式');
						}
					}
					if (!empty($dates2)) {
						$row3['d'] = $dates2;
					}
					$return2[$k] = $row3;
				}

			}


		}
		return $return2;
	}

	function fmtDatereplace($grep='')
	{
		$return = [];
		if (!empty($grep)) {
			$greps = explode("\r\n", $grep);
			// $setups = [];
			if (count($greps)) {
				foreach ($greps as $line) {
					$line = trim($line);
					$line = strtolower($line);
					if (empty($line)) {
						continue;
					}
					if (preg_match("/^\d{4}-\d{2}-\d{2}\s+\d{2}-\d{2}$/i", $line)) {
						$return[] = $line;
					}else{
						throw new \Exception('日期格式错误');
					}

				}
			}
		}
		return $return;
	}
	public function _chkDateFormat($sDate)
	{
			if (empty($sDate)) {
				return "";
			}
	    $sNewDate = $sDate;
	    $sNewDate = trim($sNewDate);
	    $dates = explode(",", $sNewDate);
	    foreach ($dates as $sDateTmp2) {
	    	if(strtotime($sDateTmp2) === false) {
	    		return false;
	    	}
	    }
	    return count($dates)==1?[$dates[0], $dates[0]] : $dates;

	}  

	// 根据配置调整每天的量
	function getNumByGrep($type, $num, $defaultpercent, $stop, $date, $ua, $grep_configs){
		if (in_array($type, ['shmain'])) {
			return intval($num);
		}

		if (empty($grep_configs)) {
			return intval($num*$defaultpercent/100);
		}else{
			foreach ($grep_configs as $row) {
				$matcheds = [];
				$matcheds['d'] = 1; //日期判断
				$matcheds['u'] = 1; //ua判断

				if (
					isset($row['d']) 
					&& count($row['d'])
					&& (strtotime($date) < strtotime($row['d'][0]) ||  strtotime($date) > strtotime($row['d'][1]))
				) {
					$matcheds['d'] = 0;
				}

				if (
					isset($row['u']) 
					&& count($row['u'])
					&& !in_array($ua, $row['u'])
				) {
					$matcheds['u'] = 0;
				}
				if (array_sum($matcheds) == 2) {//满足条件
					$matchpercent = 0;
					if (in_array($type, ['shself'])) {
						$matchpercent = (100-$row['p']);
					}elseif (in_array($type, ['shdown'])) {
						$matchpercent = $row['p'];
					}

					return intval($num*$matchpercent/100);
				}
			}
			// 没有命中任何条件, 走默认
			return intval($num*$defaultpercent/100);
		}
	}    
}