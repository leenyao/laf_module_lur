<?php
include_once(__DIR__."/../../../../../../bin/script/config_inc.php");

// 旗讯
// http://47.92.116.14:2222/api/areaPool.html
// xunyou
// http://xunyou.ippptp.com:2222/api/areaPool.html
// 先锋
// Request URL: http://routeline.pptp.biz/
// 贝壳
// Request URL: http://ry.cncntc.com/bkdz.php
// 迅捷
// Request URL: http://xjip.hlapi.com/area
// 91ip
// Request URL: http://dl.91ip.vip/areas
// http://routeline.pptp.biz/

$aEnableAccount = array(
	'eagle_a',
	'eagle_b',
	'xianfeng',
	'qiangzi',
	// 'zk5_100',
	// 'zekou5_c',
	'kun658_80',
	// 'vpn1_dongguan',
	// 'vpn1_qingdao',
	// 'zekou5_d',
	// 'k41',
	// 'yhtip_custom1',
	'xx1', // 星星1
	'xx2', // 星星2 => xx
	'xx3', // 星星3 => jk
	'xunyou', // 迅游
	'xunjie', // 迅捷
	'qixun', // qixun
	'beike', // qixun
	'91ip', // qixun
	
	// 'xx4', // 星星4
	// 'xianfeng2', // 先锋2
	'laoying_c', // 迅游
	'youzi', // 迅游
	'xigua', // 迅游
	'radius', // 蜗牛
	'shenlong', // shenlong
	'yundongtai', // 运动太
	'diy1', //独享vpn
	'2288', //独享vpn
	'yoyoip', //独享vpn

	// K41,eagle_c,vpn1_dongguan,vpn1_qingdao,xianfeng2,xx4,yhtip_custom1,zekou5_c,zk5_100
	// 改名：xx2--》xx，xx3--》jk
);
$sDebugStoreName="yoyoip";

$bDebug = false;
// $bDebug = true;


function getProvinceName($sCityName, $sProviceNameTmp){
	$list = ["广东","浙江","江苏","山东","河南","辽宁","四川","河北","湖北","湖南","福建","江西","安徽","黑龙江","山西","陕西","吉林","广西","新疆","云南","内蒙古","内蒙","贵州","甘肃","海南","宁夏","青海","西藏","台湾","香港","澳门"];
	$matched = [];
	foreach($list as $name){
		if (preg_match("/".$name."/i", $sProviceNameTmp)) {
			if(in_array($name, ["内蒙古","内蒙"]))$name='内蒙古';
			$matched[$name] = $name;
		}
	}
	if (count($matched)>1){
		var_dump("----匹配错误----", $sCityName, $sProviceNameTmp, $matched);
		exit();
	}elseif(count($matched)==1){
		return current($matched);
	}else{
		return '全国';
	}
}

try {
		

	$sDir = __DIR__."/accounts";
	$aFiles = scandir($sDir, 1);
	$aAccounts = array();
	foreach ($aEnableAccount as $sStoreName) {
		// $sStoreName = str_replace(".account", "", $sFile);

		if ($bDebug && $sStoreName!=$sDebugStoreName) {
			continue;
		}
		if (!in_array($sStoreName, $aEnableAccount)) {
			continue;
		}


		// $sAccContents = file_get_contents($sDir."/".$sFile);
		// $aLinesTmp = explode("\n", $sAccContents);
		// $bBreakFlag = true;
		// foreach ($aLinesTmp as $vTmp1) {
		// 	$vTmp1 = trim($vTmp1);
		// 	if (empty($vTmp1)) {
		// 		continue;
		// 	}
		// 	$vTmp1 = preg_replace("/\s+/", " ", $vTmp1);
		// 	$aSplit = explode(" ", $vTmp1);
		// 	$bEnable = isset($aSplit[2]) ? $aSplit[2] : 1;
		// 	if ($bEnable == 1) {
		// 		$bBreakFlag = false;
		// 		break;
		// 	}
		// }
		// if ($bBreakFlag) {
		// 	continue;
		// }
	// var_dump($sStoreName, $bEnable, $bBreakFlag);



		$aAccounts[] = $sStoreName;
		switch ($sStoreName) {
			case 'vpn1_dongguan'://debug 时关闭
				$sContents = <<<__EOF
				61.145.162.93 东莞
__EOF;
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;
						
			case 'vpn1_qingdao'://debug 时关闭
				$sContents = <<<__EOF
				58.56.131.250 青岛
__EOF;
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;
									
			case 'yhtip_custom1'://debug 时关闭
				$sContents = <<<__EOF
				yhtip.com 不限
__EOF;
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;

			case 'kun658_80'://debug 时关闭
				// $sContents = file_get_contents("http://www.kun658.com/pptp/user.asp?action=areas&bmnchi=tfxy11");
				$sContents = file_get_contents("http://99.kun658.com/pptp/user.asp?action=areas");
				// $sContents = file_get_contents("http://www.kun658.com/style/info/80pptp.asp");
				// 
				// var_dump($sContents);
				$aJsonData = json_decode($sContents, 1);
				// var_dump($aJsonData);
				// exit;
				$sContent4Write = "";
				foreach ($aJsonData as $aRow) {
					foreach ($aRow['areas'] as $aRow2) {
						$sContent4Write .= $aRow2["name"]." ".$aRow2["address"]."\n";
					}
				}
				// $sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContent4Write);
				break;
			case 'k41'://debug 时关闭
				$sContents = file_get_contents("http://vps.kun658.com/pptp/41.txt");
				$sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;

			case 'zekou5'://debug 时关闭
				$sContents = file_get_contents("http://glpptp.zhekou5.com/vpsadm/pptplb.asp");
				$sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;
			case 'zekou5_b'://debug 时关闭
				$sContents = file_get_contents("http://vps.yangquan588.com/pptp/pptp2.txt");
				$sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;
			case 'zekou5_c'://debug 时关闭
				// $sContents = file_get_contents(__DIR__."/accounts/".$sStoreName.".ips");
				$sContents = file_get_contents("http://vps.kun658.com/pptp/pptp4.txt");

				// $sContents = iconv("GBK", "UTF-8", $sContents);
				// file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;


			case 'radius'://debug 时关闭
				$sContents = file_get_contents(__DIR__."/accounts/".$sStoreName.".ips1");

				preg_match_all("/(\d+)\s*,.*/i", $sContents, $aMatches);
				$sContents2 = "";
				if (isset($aMatches[1]) && count($aMatches[1])) {
					foreach ($aMatches[1] as $nIndex => $nVpnCityId) {
						$sContents2 .= $nVpnCityId.".1.s77.cn \t".$aMatches[0][$nIndex]."\n";
					}
				}

				// $sContents = strip_tags($sContents);
				// $sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents2);
				break;

			case 'zekou5_d'://debug 时关闭
				$sContents = file_get_contents("http://pptp.84684.net/user/ip_api.php?vpnname=a448482&vpnpassword=123456&idc=0");
				$sContents = str_replace("<li>", "\n<li>", $sContents);
				// print_r($sContents);

				preg_match_all("/<li>.*<input.*qiehuandiqu.*value=[\"]?(\d+)[\"]?>(.*)/i", $sContents, $aMatches);
				// print_r($aMatches);
				$sContents2 = "";
				if (isset($aMatches[1]) && count($aMatches[1])) {
					foreach ($aMatches[1] as $nIndex => $nVpnCityId) {
						$sContents2 .= $nVpnCityId.".ros2.chengshu.com \t".$aMatches[2][$nIndex]."\n";
					}
				}

				// $sContents = strip_tags($sContents);
				// $sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents2);
				break;


			// case 'shenlong'://debug 时关闭
			// 	http://1.yunipip.cn/%E7%A5%9E%E9%BE%99IP%E7%BA%BF%E8%B7%AF%E8%A1%A8/1.html
			// 	$sch = "curl 'http://routeline.hunanguangsu.com/'   -X 'POST'   -H 'Connection: keep-alive'   -H 'Content-Length: 0'   -H 'Pragma: no-cache'   -H 'Cache-Control: no-cache'   -H 'Accept: application/json, text/javascript, */*; q=0.01'   -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36'   -H 'X-Requested-With: XMLHttpRequest'   -H 'Origin: http://routeline.hunanguangsu.com'   -H 'Referer: http://routeline.hunanguangsu.com/'   -H 'Accept-Language: zh-CN,zh;q=0.9' ";
			// 	exec($sch, $output);

			// 	$aJsonData = json_decode($output[0], 1);
			// 	// print_r($aJsonData);
			// 	$sContent4Write = "";
			// 	foreach ($aJsonData['data'] as $sCityName => $aRow) {
			// 		foreach ($aRow as $aRow2) {
			// 			$sContent4Write .= $sCityName." ".$aRow2["nasname"]."\n";
			// 		}
			// 	}
			// 	// print_r($$sContent4Write);
			// 	file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContent4Write);

			// break;

			// case 'shenlong'://debug 时关闭
			// 	$sch = "curl 'http://routeline.hunanguangsu.com/'   -X 'POST'   -H 'Connection: keep-alive'   -H 'Content-Length: 0'   -H 'Pragma: no-cache'   -H 'Cache-Control: no-cache'   -H 'Accept: application/json, text/javascript, */*; q=0.01'   -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36'   -H 'X-Requested-With: XMLHttpRequest'   -H 'Origin: http://routeline.hunanguangsu.com'   -H 'Referer: http://routeline.hunanguangsu.com/'   -H 'Accept-Language: zh-CN,zh;q=0.9' ";
			// 	exec($sch, $output);

			// 	$aJsonData = json_decode($output[0], 1);
			// 	// print_r($aJsonData);
			// 	$sContent4Write = "";
			// 	foreach ($aJsonData['data'] as $sCityName => $aRow) {
			// 		foreach ($aRow as $aRow2) {
			// 			$sContent4Write .= $sCityName." ".$aRow2["nasname"]."\n";
			// 		}
			// 	}
			// 	// print_r($$sContent4Write);
			// 	file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContent4Write);

			// break;
			case 'laoying_c'://debug 时关闭

				$sContents = file_get_contents("http://dl.91ip.vip/areas");
				$sContents = preg_replace("/<\/td>\s*/", "<\/td>", $sContents);
				$sContents = str_replace("</tr>", "<\/tr>\n", $sContents);
				$sContents = str_replace("<table>", "\n<table>", $sContents);
				// print_r($sContents);

				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;

				

			case 'yundongtai'://debug 时关闭

				$sContents = file_get_contents("http://web.ipsoft168.com:8000/ip.php");
				$sContents = iconv("GBK", "UTF-8", $sContents);
				
				// print_r($sContents);

				$sContents = preg_replace("/<\/td>\s*/", "<\/td>", $sContents);
				$sContents = str_replace("</tr>", "<\/tr>\n", $sContents);
				$sContents = str_replace("<table>", "\n<table>", $sContents);
				// print_r($sContents);

				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;



			case 'yoyoip'://debug 时关闭

				$sContents = file_get_contents("https://user.yoyoip.com/line");
				// print_r($sContents);

				$sContents = preg_replace("/<\/td>\s*/", "<\/td>", $sContents);
				$sContents = str_replace("</tr>", "<\/tr>\n", $sContents);
				$sContents = str_replace("<table>", "\n<table>", $sContents);
				// print_r($sContents);

				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);

				break;

			case 'youzi'://debug 时关闭

				$sContents = file_get_contents("http://www.yzip.vip/");
				// print_r($sContents);

				$sContents = preg_replace("/<\/td>\s*/", "<\/td>", $sContents);
				$sContents = str_replace("</tr>", "<\/tr>\n", $sContents);
				$sContents = str_replace("<table>", "\n<table>", $sContents);
				// print_r($sContents);

				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;

			case 'xianfeng'://debug 时关闭
				if($sStoreName == "xianfeng"){
					$sUrl = "http://routeline.hunanguangsu.com/";
				}

				$sch = "curl 'http://routeline.hunanguangsu.com/'   -X 'POST'   -H 'Connection: keep-alive'   -H 'Content-Length: 0'   -H 'Pragma: no-cache'   -H 'Cache-Control: no-cache'   -H 'Accept: application/json, text/javascript, */*; q=0.01'   -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36'   -H 'X-Requested-With: XMLHttpRequest'   -H 'Origin: http://routeline.hunanguangsu.com'   -H 'Referer: http://routeline.hunanguangsu.com/'   -H 'Accept-Language: zh-CN,zh;q=0.9' ";
				exec($sch, $output);

				// print_r($output);
				$aJsonData = json_decode($output[0], 1);
				// $sContents = myFileGetContents($sUrl);
				// print_r($aJsonData);
				// $aJsonData = json_decode($sContents, 1);
				// print_r($aJsonData);
				$sContent4Write = "";
				foreach ($aJsonData['data'] as $sCityName => $aRow) {
					foreach ($aRow as $aRow2) {
						$sContent4Write .= $sCityName." ".$aRow2["nasname"]."\n";
					}
				}
				// print_r($sContent4Write);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContent4Write);

				break;
			case 'xunyou'://debug 时关闭
			case 'qixun'://debug 时关闭
			case 'beike'://debug 时关闭
			case '91ip'://debug 时关闭
			case 'xunjie'://debug 时关闭
			case 'shenlong':
			// case 'xianfeng'://debug 时关闭
				if($sStoreName == "xunyou"){
					// $sUrl = "http://qiangziyun.com/index/table2/table.html";
					$sUrl = "http://118.31.36.187:2222/";
				}elseif($sStoreName == "qixun"){
					// $sUrl = "http://qiangziyun.com/index/table2/table.html";
					$sUrl = "http://47.92.116.14:2222/api/areaPool.html";
				}elseif($sStoreName == "beike"){
					// $sUrl = "http://qiangziyun.com/index/table2/table.html";
					$sUrl = "http://ry.cncntc.com/bkdz.php";
				}elseif($sStoreName == "xunjie"){
					// $sUrl = "http://qiangziyun.com/index/table2/table.html";
					$sUrl = "http://xjip.hlapi.com/area";
				}elseif($sStoreName == "91ip"){
					// $sUrl = "http://qiangziyun.com/index/table2/table.html";
					$sUrl = "http://dl.91ip.vip/areas";
				}elseif($sStoreName  == 'shenlong'){
					$sUrl = 'http://1.yunipip.cn/%E7%A5%9E%E9%BE%99IP%E7%BA%BF%E8%B7%AF%E8%A1%A8/1.html';
				}

				$sContents = myFileGetContents($sUrl);
				// print_r($sContents);
				$sContents = preg_replace("/\s*/", "", $sContents);
				// $sContents = preg_replace("/<\/td>\s*/", "<\/td>", $sContents);
				$sContents = str_replace("</tr>", "<\/tr>\n", $sContents);
				// print_r($sContents);
				// exit;
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;

			case 'zk5_100'://debug 时关闭
				$sContents = file_get_contents("http://vps.kun658.com/pptp/pptp.txt");
				$sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;
				
			
			case 'spptp'://debug 时关闭
				$sContents = file_get_contents("http://www.9jsq.com/iplist.html");
				// $sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				break;
			case 'gpptp'://debug 时关闭
				$sContents = file_get_contents("http://www.gpptp.com/iplist.html");
				$sContents = preg_replace("/<\/td>\s+/i", "<\/td>", $sContents);
				// $sContents = iconv("GBK", "UTF-8", $sContents);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);
				// exit;
				break;
			case 'qiangzi'://
			case 'eagle_a'://
			case 'eagle_b'://
			case 'xx2'://
			case 'xx3'://
			case 'xigua'://
			
			//http://php-api.juip.com/script/linedata/display.php?product=1 
				// $sContents = file_get_contents(__DIR__."/accounts/".$sStoreName.".ips");
				// var_dump($sContents);
				// $sUrl = "http://d568466b.wiz03.com/share/api/shares/3lq4pH194ArU2A0x-Y2k09LZ0N3wlq3jpkMd2i4jtN1wiaNh?_=1536904780748";

			// http://www.juip.com/linelist/index?ProductId=1
				if ($sStoreName == "eagle_a") {
					$sUrl = "http://php-api.juip.com/script/linedata/display.php?product=4";
				}else if($sStoreName == "eagle_b"){
					$sUrl = "http://laoyingip.com/index/vps/table.html";
				}else if($sStoreName == "xianfeng1"){
					$sUrl = "http://php-api.juip.com/script/linedata/display.php?product=6";
				}else if($sStoreName == "qiangzi"){
					$sUrl = "http://php-api.juip.com/script/linedata/display.php?product=1";
				}else if($sStoreName == "xx2"){
					$sUrl = "http://php-api.juip.com/script/linedata/display.php?product=8";
				}else if($sStoreName == "xx3"){
					$sUrl = "http://php-api.juip.com/script/linedata/display.php?product=13";
				}else if($sStoreName == "xigua"){
					$sUrl = "http://php-api.juip.com/script/linedata/display.php?product=17";
				}else{
					$sUrl = "http://qiangziyun.com/index/index/table.html";
				}






				// echo $sUrl;

				$sContents = myFileGetContents($sUrl);
				$aJsonData2 = json_decode($sContents, 1);
				$sContents = "";
				foreach ($aJsonData2['data'] as $row) {
					$sContents .= $row['city']."; ".$row['nasname']."\n";
				}
				// var_dump($sContents);
				// $aJsonData2 = json_decode($sContents, 1);
				// $sContents = $aJsonData2['doc']['html'];
				// $sTmp = strip_tags($sTmp);
				// $sContents = preg_replace("/(<tr><td colspan=\"7\".*<\/tr>)/i", "xxxxxxxx", $sContents);
				// $sContents = html_entity_decode($sContents);
				// echo $sContents;


				// preg_match_all("/<tr\s*style=\"\">\s*<td.*>(.*)<\/td>\s*<td.*>(.*)<\/td>\s*<td.*>(.*)<\/td>\s*<td.*>(.*)<\/td>/i", $sContents, $aMatches);
				// // var_dump($aMatches);

				// $aJsonData3 = [];
				// if (isset($aMatches[2]) && count($aMatches[2])) {
				// 	foreach ($aMatches[2] as $k => $v) {
				// 		$aJsonData3[] = $v." ; ".$aMatches[4][$k];
				// 	}
				// }
				// print_r($aJsonData3);
				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sContents);

				// var_dump($sContents);

				break;
			// case 'xianfeng1'://
			case 'xx1'://
			// case 'xx3'://
			case 'xx4'://
			case 'xianfeng2'://
			// case 'xunyou'://
			

			
				if ($sStoreName == "eagle_a") {
					$sUrl = "http://note.youdao.com/yws/public/note/22f07552fbdb6856783f60201a20e151?editorType=0";
					$sContents = file_get_contents($sUrl);
					$aJsonData2 = json_decode($sContents, 1);
					$sTmp = $aJsonData2['content'];
					$sTmp = preg_replace("/<.+?>/i", "\n", $sTmp);
				}elseif ($sStoreName == "eagle_b") {
					$sUrl = "http://note.youdao.com/yws/public/note/604da8d355edf7435cac6bd08685fefd?editorType=0";
					$sContents = file_get_contents($sUrl);
					$aJsonData2 = json_decode($sContents, 1);
					$sTmp = $aJsonData2['content'];
					$sTmp = preg_replace("/<\/tr>/i", "<\/tr>\n", $sTmp);
					$sTmp = preg_replace("/<.+?>/i", ", ", $sTmp);
					// var_dump($sTmp);
				}elseif ($sStoreName == "xianfeng1") {
					$sUrl = "http://note.youdao.com/yws/public/note/ed544b24ac88a84c1291af5354c5c976?editorType=0";
					$sUrl = "http://xianfengip.com/index/index/table.html";

					$sContents = file_get_contents($sUrl);
					$aJsonData2 = json_decode($sContents, 1);
					$sTmp = $aJsonData2['content'];
					$sTmp = preg_replace("/<\/tr>/i", "<\/tr>\n", $sTmp);
					$sTmp = preg_replace("/<\/span>/i", "<\/span>\n", $sTmp);
					$sTmp = preg_replace("/<.+?>/i", ", ", $sTmp);
					// var_dump($sTmp);
				}elseif ($sStoreName == "xx1") {
					$sUrl = "http://note.youdao.com/yws/public/note/6950ce5f61059308291ac97aad8b9ee0?editorType=0";
					$sContents = file_get_contents($sUrl);
					$aJsonData2 = json_decode($sContents, 1);
					$sTmp = $aJsonData2['content'];
					$sTmp = preg_replace("/<\/tr>/i", "<\/tr>\n", $sTmp);
					# $sTmp = preg_replace("/<\/span>/i", "<\/span>\n", $sTmp);
					$sTmp = preg_replace("/<.+?>/i", ", ", $sTmp);
					// echo($sTmp);
				}elseif ($sStoreName == "xx3") {
					$sUrl = "http://note.youdao.com/yws/public/note/bb6e6c94c0351de04a2efc6d8698f80b?editorType=0";
					$sContents = file_get_contents($sUrl);
					$aJsonData2 = json_decode($sContents, 1);
					$sTmp = $aJsonData2['content'];
					// echo $sTmp;
					$sTmp = preg_replace("/<table/i", "\n<table", $sTmp);
					$sTmp = preg_replace("/<\/tr>/i", "<\/tr>\n", $sTmp);
					# $sTmp = preg_replace("/<\/span>/i", "<\/span>\n", $sTmp);
					$sTmp = preg_replace("/<.+?>/i", ", ", $sTmp);
					// echo($sTmp);
				}elseif ($sStoreName == "xx4") {
					$sUrl = "http://note.youdao.com/yws/public/note/f043d4e6e1566455777eca114ef9bf0c?editorType=0";
					$sContents = file_get_contents($sUrl);
					$aJsonData2 = json_decode($sContents, 1);
					$sTmp = $aJsonData2['content'];
					// echo $sTmp;
					$sTmp = preg_replace("/<table/i", "\n<table", $sTmp);
					$sTmp = preg_replace("/<\/div>/i", "<\/div>\n", $sTmp);
					# $sTmp = preg_replace("/<\/span>/i", "<\/span>\n", $sTmp);
					$sTmp = preg_replace("/<.+?>/i", ", ", $sTmp);
					// echo($sTmp);
				}elseif ($sStoreName == "xianfeng2") {
					$sUrl = "http://note.youdao.com/yws/public/note/a14ead731d11a66270db0c23d3b5d578?editorType=0";
					$sContents = file_get_contents($sUrl);
					$aJsonData2 = json_decode($sContents, 1);
					$sTmp = $aJsonData2['content'];
					// echo $sTmp;
					$sTmp = preg_replace("/<table/i", "\n<table", $sTmp);
					$sTmp = preg_replace("/<\/div>/i", "<\/div>\n", $sTmp);
					# $sTmp = preg_replace("/<\/span>/i", "<\/span>\n", $sTmp);
					$sTmp = preg_replace("/<.+?>/i", ", ", $sTmp);
					// echo($sTmp);
				}
				

				file_put_contents(__DIR__."/accounts/".$sStoreName.".ips", $sTmp);
				break;


			default:
				# code...
				break;
		}
	}

	$aConfig = $oGlobalFramework->sm->get('config');
	$oPdo = \Application\Model\Common::getPDOObejct($oGlobalFramework->sm);
	// select * from `laf_lur`.`location_city`
	$aCities = array();
	$aProvinceId2Lable = array();
	$aCitieName2ProvinceId = array();

	$sSql = "select id,name from `laf_lur`.`location_province`";
	$oQuery = $oPdo->query($sSql);
	while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){
		$aResult['name'] = preg_replace("/市$/i", "", $aResult['name']);
		$aResult['name'] = preg_replace("/省$/i", "", $aResult['name']);
		$aResult['name'] = preg_replace("/自治区$/i", "", $aResult['name']);
		$aResult['name'] = preg_replace("/壮族/i", "", $aResult['name']);
		$aResult['name'] = preg_replace("/回族/i", "", $aResult['name']);
		$aResult['name'] = preg_replace("/维吾尔/i", "", $aResult['name']);
		$aResult['name'] = preg_replace("/内蒙古/i", "内蒙", $aResult['name']);


		$aResult['name'] = preg_replace("/混播/i", "全国混拨", $aResult['name']);
		$aResult['name'] = preg_replace("/混拨/i", "全国混拨", $aResult['name']);
		$aResult['name'] = preg_replace("/全国/i", "全国混拨", $aResult['name']);
		$aResult['name'] = preg_replace("/混合/i", "全国混拨", $aResult['name']);
		

		$aProvinceId2Lable[$aResult['id']] = $aResult['name'];
	}

	$sSql = "select name,province_id from `laf_lur`.`location_district`";
	$oQuery = $oPdo->query($sSql);
	while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){

		$sCityName = $aResult['name'];
	 	$sCityName = trim($sCityName);
		if (preg_match("/^.*市$/i", $sCityName)) {
		    $sCityName = preg_replace("/^(.*)市$/i", '\1', $sCityName);
		}elseif(preg_match("/^.*县$/i", $sCityName)){
	 	    $sCityName = preg_replace("/^(.*)县$/i", '\1', $sCityName);
	 	}
	 	$sCityName = trim($sCityName);
	    $aCities[] = $sCityName;
	    $aCitieName2ProvinceId[$sCityName] = $aResult['province_id'];
	 }
	 $aCities[] = '全国混拨';

	 $sSql = "select name,province_id from `laf_lur`.`location_city` order by province_id, city_index asc";
	 $oQuery = $oPdo->query($sSql);
	 while($aResult = $oQuery->fetch(\PDO::FETCH_ASSOC)){

	 	$sCityName = $aResult['name'];
	 	$sCityName = trim($sCityName);
	 	if (preg_match("/^.*市$/i", $sCityName)) {
	 	    $sCityName = preg_replace("/^(.*)市$/i", '\1', $sCityName);
	 	}elseif(preg_match("/^.*县$/i", $sCityName)){
	 	    $sCityName = preg_replace("/^(.*)县$/i", '\1', $sCityName);
	 	}
	 	$sCityName = trim($sCityName);
	 	// if (preg_match("/忻州/i", $sCityName)) {
	 	// 	var_dump($sCityName, $aResult['province_id']);
	 	// 	# code...
	 	// }
	     $aCities[] = $sCityName;
	    $aCitieName2ProvinceId[$sCityName] = $aResult['province_id'];
	  }

	// 重新调整程序

	/**
	 * #######closure function start#######
	 */
	$fUpdCitySort = function($aCities=null) 
	{
	    $aReturn = array();
	    foreach ($aCities as $k=>$sCity) {
	    	if (in_array($sCity, ['马鞍山', '鞍山'])) {
	    		unset($aCities[$k]);
	    	}
	    }
	    $aReturn = array_merge(['马鞍山', '鞍山'], $aCities);
	    return $aReturn;
	};
	$aCities = $fUpdCitySort($aCities);
	//unset($fUpdCitySort);


	print_r($aAccounts);
	// print_r($aCities);

	// exit;

	$aData4Sql = array();
	foreach ($aAccounts as $sStoreName) {
		if ($bDebug && $sStoreName!=$sDebugStoreName) {
			continue;
		}
		// if ($sStoreName != "zekou5") {
		// 	continue;
		// }
		if (!isset($aData4Sql[$sStoreName])) {
			$aData4Sql[$sStoreName] = array();
		}
		if (!isset($aData4Sql[$sStoreName]['accounts'])) {
			$aData4Sql[$sStoreName]['accounts'] = array();
		}
		if (!isset($aData4Sql[$sStoreName]['ips'])) {
			$aData4Sql[$sStoreName]['ips'] = array();
		}
		$sAccFile = $sDir."/".$sStoreName.".account";
		$sIpsFile = $sDir."/".$sStoreName.".ips";

		// if (!file_exists($sAccFile) || !file_exists($sIpsFile)) {
		// 	continue;
		// }
		// $sAccContents = file_get_contents($sAccFile);
		// $aLinesTmp = explode("\n", $sAccContents);
		// foreach ($aLinesTmp as $vTmp1) {
		// 	$vTmp1 = trim($vTmp1);
		// 	if (empty($vTmp1)) {
		// 		continue;
		// 	}
		// 	$vTmp1 = preg_replace("/\s+/", " ", $vTmp1);
		// 	$aSplit = explode(" ", $vTmp1);
		// 	$bEnable = isset($aSplit[2]) ? $aSplit[2] : 1;
		// 	$sdisabledate = isset($aSplit[3]) ? $aSplit[3] : date("Y-m-d", strtotime("+1 month"));
		// 	if ($bEnable != 1) {
		// 		continue;
		// 	}
		// 	$aData4Sql[$sStoreName]['accounts'][$aSplit[0]] = array(
		// 		"username" => $aSplit[0],
		// 		"password" => $aSplit[1],
		// 		'disabledate' => $sdisabledate,
		// 	);
		// }
		// if (count($aData4Sql[$sStoreName]['accounts']) < 1) {
		// 	unset($aData4Sql[$sStoreName]);
		// 	continue;
		// }
		echo "\n============".$sStoreName."==============\n";


		$sContentsTmp = file_get_contents($sIpsFile);
		$aLinesTmp = explode("\n", $sContentsTmp);
		// print_r($aLinesTmp);
		foreach ($aLinesTmp as $vTmp1) {
			if (preg_match("/地区/i", $vTmp1))continue;

			$vTmp1 = preg_replace("/混播/i", "全国混拨", $vTmp1);
			$vTmp1 = preg_replace("/混拨/i", "全国混拨", $vTmp1);
			$vTmp1 = preg_replace("/全国/i", "全国混拨", $vTmp1);
			$vTmp1 = preg_replace("/混合/i", "全国混拨", $vTmp1);
			$vTmp1 = preg_replace("/松源/i", "松原", $vTmp1);


			
			// if (preg_match("/全国/i", $vTmp1))continue;

						// echo "###".$vTmp1."\n";
			
			if (
				preg_match("/(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/i", $vTmp1)
				|| preg_match("/([a-z0-9]+([a-z0-9-]*(?:[a-z0-9]+))?\.)?[a-z0-9]+([a-z0-9-]*(?:[a-z0-9]+))?(\.us|\.tv|\.org\.cn|\.org|\.net\.cn|\.net|\.mobi|\.vip|\.me|\.vc|\.la|\.info|\.hk|\.gov\.cn|\.pro|\.edu|\.com\.cn|\.com|\.co\.jp|\.co|\.cn|\.cc|\.biz|\.vin)/i", $vTmp1)

				
				) {
						// echo "###".$vTmp1."\n";

				foreach ($aCities as $sCityName) {
					$sCityName = trim($sCityName);
					if (empty($sCityName) || strlen($sCityName)<=3) {
						continue;
					}
						// echo "@@@".$sCityName."\n";
					if (preg_match("/$sCityName/i", $vTmp1)) {
						if ($sCityName == '全国混拨') {
							$vTmp1 = preg_replace('/\d+-+/i', "", $vTmp1);
							$vTmp1 = preg_replace('/\w+-+/i', "", $vTmp1);
							$vTmp1 = preg_replace('/-+/i', "", $vTmp1);
						}
						$sHost = "";

						// 判断ip
						preg_match_all("/(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/i", $vTmp1, $aMatches);
						if (count($aMatches[0]) ) {
							$sHost = $aMatches[0][0];
						}else{
							// 判断域名
							preg_match_all("/([a-z0-9]+([a-z0-9-]*(?:[a-z0-9]+))?\.)*[a-z0-9]+([a-z0-9-]*(?:[a-z0-9]+))?(\.us|\.tv|\.org\.cn|\.org|\.net\.cn|\.net|\.mobi|\.vip|\.me|\.vc|\.la|\.info|\.hk|\.gov\.cn|\.pro|\.edu|\.com\.cn|\.com|\.co\.jp|\.co|\.cn|\.cc|\.biz|\.vin)/i", $vTmp1, $aMatches);
							// var_dump($vTmp1);
							if (count($aMatches[0]) ) {
								$sHost = $aMatches[0][0];
								
							}else{
								throw new \Exception($vTmp1, 1);
							}
						}
						// print_r($sCityName.$sHost."\n");
						$sProviceNameTmp = strip_tags($vTmp1);

						// print_r($aProvinceId2Lable);
						// print_r($aCitieName2ProvinceId);
						if (isset($aCitieName2ProvinceId[$sCityName])) {
							$sProviceNameTmp = $aProvinceId2Lable[$aCitieName2ProvinceId[$sCityName]];
						}

						$sProvinceName = getProvinceName($sCityName, $sProviceNameTmp);
						$aData4Sql[$sStoreName]['ips'][$sHost] = array(
							'province' => $sProvinceName,
							'city' => $sCityName,
							'host' => $sHost,
							'memo' => $sProviceNameTmp,
						);

						if ($sCityName=='全国混拨') {
							echo $sCityName." => ".$sHost."/".$sProviceNameTmp."\n";
						}
						break;
					}

				}

			}
			// $vTmp1 = preg_replace("/\s+/", " ", $vTmp1);
			// $aSplit = explode(" ", $vTmp1);
			// $aData4Sql[$sStoreName]['accounts'][$aSplit[0]] = array(
			// 	"username" => $aSplit[0],
			// 	"password" => $aSplit[1],
			// );
		}

	}

	echo "\n\n";

	if ($bDebug) {
		print_r($aData4Sql);
		// exit;
	}

	$aUsingAcc = [];
	$sSql3 = "select count(id) total,vpnstore from `vpn_accounts` where status=1 group by vpnstore";
	
	$oQuery3 = $oPdo->query($sSql3);
	while($aResult3 = $oQuery3->fetch(\PDO::FETCH_ASSOC)){
		$aUsingAcc[$aResult3['vpnstore']] = $aResult3['total'];
	}
	echo "\n===vpn accounts====\n";
	print_r($aUsingAcc);
	echo "\n===vpn hosts====\n";
	$nAccountTotal = 0;
	foreach ($aData4Sql as $key => $value) {
		$nAccountTotal += count($value['ips']);
		// echo $key." accounts => ".count($value['accounts'])."\n";
		$swarring =  '';
		if (isset($aUsingAcc[$key])&&$aUsingAcc[$key]>0 && $value['ips']<1) {
			$swarring = '!!! using vpnstore without any host!!!';
		}

		echo $key." ips => ".count($value['ips'])." ;".$swarring."\n";
	}
	echo "\n\n";
	echo "total ips:".$nAccountTotal."\n";

// 	$sSqlContents = <<<_EOF

// 	DROP table if exists `vpn_accounts`;
// 	CREATE TABLE `vpn_accounts` (
// 	  `id` int(11) NOT NULL AUTO_INCREMENT,
// 	  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;2=>删除',
// 	  `vpnstore` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn 供应商名称',
// 	  `account` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn acc',
// 	  `password` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn password',
// 	  `disabledate` varchar(255) NOT NULL DEFAULT '' COMMENT '过期日期',

// 	  `memo` varchar(255) NOT NULL DEFAULT '',
// 	  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
// 	  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
// 	  PRIMARY KEY (`id`),
// 	  UNIQUE KEY `account` (`vpnstore`,`account`)
// 	) ENGINE=InnoDB DEFAULT CHARSET=utf8;


// 	DROP table if exists `vpn_hosts`;
// 	CREATE TABLE `vpn_hosts` (
// 	  `id` int(11) NOT NULL AUTO_INCREMENT,
// 	  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;2=>删除',
// 	  `vpnstore` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn 供应商名称',
// 	  `host` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn host',
// 	  `city` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn city',
// 	  `memo` varchar(255) NOT NULL DEFAULT '',
// 	  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
// 	  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
// 	  PRIMARY KEY (`id`),
// 	  UNIQUE KEY `account` (`vpnstore`,`host`)
// 	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

// _EOF;

	$sSqlContents = <<<_EOF

	DROP table if exists `vpn_hosts`;
	CREATE TABLE `vpn_hosts` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;2=>删除',
	  `vpnstore` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn 供应商名称',
	  `host` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn host',
	  `province` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn province',
	  `city` varchar(255) NOT NULL DEFAULT '' COMMENT 'vpn city',
	  `memo` varchar(255) NOT NULL DEFAULT '',
	  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `account` (`vpnstore`,`host`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

_EOF;


	

	$aSqlTmp2 = array();
	foreach ($aData4Sql as $sStoreName => $aTmp) {
		foreach ($aTmp['accounts'] as $aRows) {
			// $aSqlTmp2[] = "insert into vpn_accounts set vpnstore='".$sStoreName."',account='".$aRows['username']."',disabledate='".$aRows['disabledate']."', password='".$aRows['password']."';";
			// $sSqlContents .= $sSql."\n";
		}
		foreach ($aTmp['ips'] as $aRows) {
			$aSqlTmp2[] = "insert into vpn_hosts set vpnstore='".$sStoreName."',host='".$aRows['host']."',memo='".$aRows['memo']."', city='".$aRows['city']."', province='".$aRows['province']."';";
			// print_r($sSql);
			// $sSqlContents .= $sSql."\n";
		}
		# code...
	}

	// shuffle($aSqlTmp2);
	\YcheukfCommon\Lib\Functions::delCache($oGlobalFramework->sm, LAF_CACHE_VPNIPCHANGE_328);


	$sSqlContents .= join("\n", $aSqlTmp2);

	echo "SAVE 2 SQL FILE " .__DIR__."/vpn.sql \n";
	file_put_contents(__DIR__."/vpn.sql", $sSqlContents);

	// 检查 vpn 是否已经更新
	if (file_exists(__DIR__."/vpn.current.md5")) {
		$currentMd5 = file_get_contents(__DIR__."/vpn.current.md5");
		if ($currentMd5 != md5($sSqlContents)) {
			\YcheukfCommon\Lib\Functions::saveCache($oGlobalFramework->sm, LAF_CACHE_VPNIPCHANGE_328, 1, 86400*7);
			file_put_contents(__DIR__."/vpn.current.md5", md5($sSqlContents));
			echo "\n\n NEED UPDATE VPNIP ".md5($sSqlContents)." vs ".$currentMd5." \n\n";

		}
	}else{
		file_put_contents(__DIR__."/vpn.current.md5", md5($sSqlContents));
			\YcheukfCommon\Lib\Functions::saveCache($oGlobalFramework->sm, LAF_CACHE_VPNIPCHANGE_328, 1, 86400*7);
		
	}

	echo "import into db  \n";

	$aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($aConfig['db_master']['dsn']);
	$sShell = "mysql -u {$aConfig['db_master']['username']} -p{$aConfig['db_master']['password']} --default-character-set=utf8 --port={$aDsn['port']} -h{$aDsn['host']} {$aDsn['dbname']} < ".__DIR__."/vpn.sql";

	echo $sShell."\n";
	// exec($sShell);

	echo "DONE\n\n";

} catch (\Exception $e) {
	print_r($e->getMessage());
}
// print_r($aData4Sql);
