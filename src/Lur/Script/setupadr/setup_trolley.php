<?php
/**
 * TROLLEY 代码 自动部署/自动更新
 */
set_time_limit(0);
define('RUN_LOCK_FILE', '/tmp/setup_trolley_script.lock');
defined('LAF_LOCALDOMAIN') || define('LAF_LOCALDOMAIN', "web.adrlocal.com");


$aConfig = array(
	"debug" => 0,
);

$nParamIndex = 1;
$sInstallDir = null;
if (isset($_SERVER['argv'][$nParamIndex])) {
	$sInstallDir = $_SERVER['argv'][$nParamIndex];
	if (!file_exists($sInstallDir)) {
	    echo ("file not exists: ".$sInstallDir."\n");
	    exit;
	}
}else{
    echo ("wrong param[".$nParamIndex."]\n");
    exit;
}

$aRunableDirs = add2installList($sInstallDir);
$aFileInfo = pathinfo($sInstallDir);

if ($aFileInfo['basename'] != 'adrtrolley1') {
    exit;
}

sleep(2);

if (getJobsNums()>1) {
	// echoDaemonTips("ANOTHER job is running");
	exit;
}else{
	$sVersion = isset($argv[2]) ? $argv[2] : "release";

	while (1) {
		$sUrl = "http://".LAF_LOCALDOMAIN."/module/lur/adr_trolley_release.php?act=chkmd5&version=".$sVersion."&md5flag=1&".genDynamicToken4LurApiParams("ADRTROLLEY");
		$aRunableDirs = add2installList($sInstallDir);
		list($sOnlineVersion, $aCurlInfo) = myFileGetContents($sUrl);

		foreach ($aRunableDirs as $sTmp1) {
			$sInstallDir2 = str_replace("_SPLIT_", "/", $sTmp1);

			if ((isset($aConfig['debug']) && $aConfig['debug'] == 1)) {
				echoDaemonTips("PROCESSING:". $sInstallDir2);

				
			}


			if (preg_match('/^adr_trolley_release.*/i', $sOnlineVersion)) {
				$sDataDir = $sInstallDir2."/data";
				exec("mkdir -p ".$sDataDir);
				$sVersonFile = $sDataDir."/ADRTROLLEYVERSION";
				if (!file_exists($sVersonFile)) {
					exec("touch ".$sVersonFile);
				}
				list($sCurrentVersion, $aCurlInfo) = myFileGetContents($sVersonFile);

				$nAdrId = '';
				if (file_exists($sDataDir."/source/volume/code/ADRSERVERID")) {
					$nAdrId = trim(file_get_contents($sDataDir."/source/volume/code/ADRSERVERID"));
				}

				if ($aConfig['debug']==1 && $nAdrId==42) {
					$sCurrentVersion = "";
				}
				//校验线上线下版本
				if ($sOnlineVersion != $sCurrentVersion) {

					// 检查是否需要升级. 有些版本truck不需要升级
					$sUrl = "http://".LAF_LOCALDOMAIN."/module/lur/adr_trolley_release.php?act=runable&adrid=".$nAdrId."&version=".$sVersion."&".genDynamicToken4LurApiParams("ADRTROLLEY");
					list($isRunable, $aCurlInfo) = myFileGetContents($sUrl);
					if ($isRunable == 0) {
						continue;
					}


					// 获取下载版本MD5
					$sUrl = "http://".LAF_LOCALDOMAIN."/module/lur/adr_trolley_release.php?act=ask4update&adrid=".$nAdrId."&version=".$sVersion."&".genDynamicToken4LurApiParams("ADRTROLLEY");
					list($sOnlineVersionMd5, $aCurlInfo) = myFileGetContents($sUrl);
					if (preg_match("/\w{32}/i", $sOnlineVersionMd5)) {
						

						if ($aConfig['debug']==1 && $nAdrId==42) {
							$nRunningJobs = 0;
						}else{
							$nRunningJobs = getRunningJobsNums($sInstallDir2);
						}
						if ($nRunningJobs > 1) { // 当前主机已有另外升级进程在执行
							$sScript = "ps aux | grep \"".$_SERVER['SCRIPT_FILENAME']." $sInstallDir2\" |  grep -v \"grep\" |  grep -v \"/bin/sh -c\" ";
							exec($sScript, $aRunningjobsOutput);
							echoDaemonTips("DONE-DOWNLOAD FAILD, ANOTHER DOWNLOAD IS WORKING.",  $aRunningjobsOutput);
							continue;
						}

						// 下载最新版本
						$sUrl = "http://".LAF_LOCALDOMAIN."/module/lur/adr_trolley_release.php?act=download&version=".$sVersion."&".genDynamicToken4LurApiParams("ADRTROLLEY");
						$sAppDir = $sDataDir."/source/";

						exec("mkdir -p ".$sAppDir);
						exec("rm -f ".$sAppDir.$sOnlineVersion);
						$sWgetShell = "wget --timeout=60 -4c -O ".$sAppDir.$sOnlineVersion." '".$sUrl."' > /dev/null 2>&1";
						exec($sWgetShell);


						$bDownloadFlag = false;
						$bBreakFlag = false;
						$bWhileRetryTimes = 5;
						while (1) {
						    if ($bWhileRetryTimes-- < 1)$bBreakFlag = true;
							sleep(5);

						    // 校验下载文件MD5
						    if (md5_file($sAppDir.$sOnlineVersion) === $sOnlineVersionMd5) {
						        $bDownloadFlag = $bBreakFlag = true;
						    }
						    
						    if($bBreakFlag)break;
						}

						if ($bDownloadFlag === false) {
							echoDaemonTips("DONE-DOWNLOAD FAILD, BAD FILE.", $sAppDir.$sOnlineVersion);
							continue;

						}

						if (is_numeric($nAdrId)) {
							exec("sudo docker exec -it adrequest${nAdrId} sh /app/stop.sh 2>/dev/null");
							exec("sudo docker exec -it adrequest${nAdrId} redis-cli -h 127.0.0.1 -p 6379 shutdown SAVE 2>/dev/null");
						}
// exit;
						// 下载成功, 解压
						exec("cd ".$sAppDir." && tar -zxvf ".$sOnlineVersion." && rm -f ".$sAppDir.$sOnlineVersion);

						if (!file_exists($sAppDir."docker_start.sh")) {
							echoDaemonTips("DONE-UNTAR FAILD, FILE NOT EXIST.", $sAppDir."docker_start.sh");
							continue;
						}


						// 执行命令启动容器
						exec("sh $sAppDir/docker_start.sh");

						// 成功, 记录最新版本
						file_put_contents($sVersonFile, $sOnlineVersion);
						file_put_contents($sAppDir."/volume/code/config/ADRTROLLEYVERSION", $sOnlineVersion);
						echoDaemonTips("DONE-OK", $sOnlineVersion." is upgrade 2 ".$sAppDir);

					}else{
						echoDaemonTips("DONE-ASK4UPDATE FAILD, LUR UPDATE LIMIT.", $sOnlineVersionMd5);
					}

				}else{
					if (isset($aConfig['debug']) && $aConfig['debug'] == 1) {
						echoDaemonTips("DONE-OK", $sOnlineVersion." is already the newest version");
					}
				}
			}
		}

		if ($aConfig['debug']==1) {
			break;
		}

		sleep(30);
	}
}



function startJob()
{
	if (file_exists(RUN_LOCK_FILE)) {
		$fTime = filectime(RUN_LOCK_FILE);
		if (time()-$fTime > 60*5) {
			echoDaemonTips("lock file too old");
			quitJob();
		}else{
			echoDaemonTips("ANOTHER SCRIPT IS RUNNING");
			exit;
		}
	}
	echoDaemonTips("START");
	exec('touch '.RUN_LOCK_FILE);
}
function quitJob()
{
	exec('rm -f '.RUN_LOCK_FILE);
	exit;
}
function getRunningJobsNums($sInstallDir){
	// var_dump($_SERVER['SCRIPT_FILENAME']);
	$sScript = "ps aux | grep \"".$_SERVER['SCRIPT_FILENAME']." $sInstallDir\" |  grep -v \"grep\" |  grep -v \"/bin/sh -c\" | wc -l";
	// var_dump($sScript);
    exec($sScript, $aRunningjobs);
    $nReturn = intval($aRunningjobs[0]);
    return $nReturn;
}
function echoDaemonTips($k, $sMemo="")
{

	$sMemoString = is_array($sMemo) ? json_encode($sMemo) : $sMemo;
    $sToday = date("Ymd");
    $s = array(
        't'=>date("Y-m-d H:i:s"),
        's'=>$k,
        'm'=>$sMemo,
    );
    // echo is_array($s) ? print_r($s, 1) : $s;
    // echo "\n";
}

function myFileGetContents($sUrl){
	if (filter_var($sUrl, FILTER_VALIDATE_URL) === false){
		return array(file_get_contents($sUrl), array());
	}else{
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $sUrl);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		//设置curl默认访问为IPv4
		if(defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')){
		    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		}
		//设置curl请求连接时的最长秒数，如果设置为0，则无限
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0);
		//设置curl总执行动作的最长秒数，如果设置为0，则无限
		curl_setopt ($ch, CURLOPT_TIMEOUT, 0);
		$file_contents = curl_exec($ch);
        $info = curl_getinfo($ch); 

		curl_close($ch);
		return array($file_contents, $info);
	}
}
/**
 * 为访问LUR API生成token参数
 */
function genDynamicToken4LurApiParams($sSlat="ADRTROLLEY")
{
	list($sTokenT, $sTokenV) = getLyDynamicToken($sSlat);
	return "token_t=".$sTokenT."&token_v=".$sTokenV."&token_suid=".getConsoleUniqId(__FILE__)."&argv=".join(";",$_SERVER['argv']);
} 




/**
 * 获取命令行执行的唯一ID    
*/
function getConsoleUniqId($sScriptFile)
{
    $aParam = $_SERVER['argv'];
    if (isset($_SERVER['HOSTNAME'])) {
	    $aParam[] = $_SERVER['HOSTNAME'];
    }
    $aParam[] = filemtime($sScriptFile);
    $aParam[] = filectime($sScriptFile);
    return md5(json_encode($aParam));
}


 /**
  * 动态token, 每分钟更新
  * @param  string $sSlat 混淆码
  * @return [type]        [description]
  */
function getLyDynamicToken($sSlat="ADRTROLLEY")
{
	$sTimeSpan = time();//基数
	$sToken = md5(strrev($sTimeSpan.$sSlat));
	return array($sTimeSpan,  $sToken);
} 


function add2installList($sInstallDir)
{
	$sInstallDirListDir = __DIR__."/adr_trolley_list";
	$sInstallDirFileName = $sInstallDirListDir."/".str_replace("/", "_SPLIT_", $sInstallDir);
	// var_dump($sInstallDirLogName);
	exec("mkdir -p ".$sInstallDirListDir);
	exec("sudo chmod -cR 777 ".$sInstallDirListDir);
	exec("touch ".$sInstallDirFileName);

  	$aRunableDirs = array();

	if ($dHandle = opendir($sInstallDirListDir)) 
	{
	  while (($file = readdir($dHandle)) !== false)
	  {
	
	    if(preg_match("/adr.*\d+$/i", $file))
	    {
	    	if (time()-filemtime($sInstallDirListDir."/".$file) > 5*50) {
				exec("rm -f ".$sInstallDirListDir."/".$file);
	    	}else{
		        $aRunableDirs[] = $file;
	    		
	    	}
	    }
	  }
	}
	closedir($dHandle);
	return $aRunableDirs;
}


function getJobsNums(){
	$aFileInfo = pathinfo(__FILE__);
    // exec("ps aux | grep \"".__FILE__."\" ", $tmp);
    // var_dump($tmp);
    exec("ps aux | grep \"".$aFileInfo['basename']."\" |  grep -v \"grep\" |  grep -v \"sudo \" | grep \"/bin/sh -c\" | wc -l", $aRunningjobs);
    // echo "ps aux | grep \"".__FILE__."\" |  grep -v \"grep\" | wc -l";
    $nReturn = intval($aRunningjobs[0]);
    return $nReturn;
}
