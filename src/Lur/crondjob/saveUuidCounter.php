<?php
/*
将 UUID 的计数器数据存入数据库  

**/
class saveUuidCounter extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $oAddRedis = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis();


        $oPdo = \Application\Model\Common::getPDOObejct($this->oFrameworker->sm, "db_master"); //db_master|db_slave

        $aGenPool = [];
        $aKeys = $oAddRedis->keys(LAF_CACHE_UUID_GENPOOL_313."*");
        $nTotalFinished = 0;
        echoMsg('['.__CLASS__.'] start 2 save uuid counter  : '.count($aKeys));

        foreach ($aKeys as $sKey) {
            list($t1, $nPlatform, $sIp) = explode('/', $sKey);
            $aJsonData = $oAddRedis->hGetAll($sKey);
            krsort($aJsonData);
            $sJsonData = json_encode($aJsonData);


            $aDbData = \Application\Model\Common::getResourceById($this->oFrameworker->sm, 'b_sdkip_counter', $this->fmtMd5Key($sIp, $nPlatform), 'md5key');
            if ($aDbData 
                && isset($aDbData['data'])
                && $aDbData['data'] == $sJsonData
            ) {
                $oAddRedis->del($sKey);
                // var_dump('skip');
                continue;
            }
            


            $sSql = "REPLACE INTO b_sdkip_counter (`ip`, `platform`, `data`, `city_id`, `city_label`, `md5key`, `maxindex`) VALUES (:ip, :platform, :data,  :city_id, :city_label, :md5key, :maxindex)";

            if (!filter_var($sIp, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                continue;
            }
            

            $aIpConfig = $this->oFrameworker->sm->get('\Lur\Service\Common')->getIacConfigByIp($sIp, true);

            $oSth = $oPdo->prepare($sSql);
            $oSth->execute(array(
                ":ip"=> $sIp,
                ":platform"=> $nPlatform,
                ":data"=> $sJsonData,
                ":city_id"=> $aIpConfig['data']['mapresid'],
                ":city_label"=> $aIpConfig['data']['region'],
                ":md5key"=> $this->fmtMd5Key($sIp, $nPlatform),
                ":maxindex"=> current($aJsonData),

            ));
            $nTotalFinished++;
        }

        echoMsg('['.__CLASS__.'] save uuid counter  save/all : '.$nTotalFinished.'/'.count($aKeys));

    }

    public function fmtMd5Key($sIp='', $nPlatform='')
    {
        return md5($sIp."/".$nPlatform);
    }
}

