<?php
/*
芒果检查排期的api

**/
class Mgscheduleapi extends \YcheukfCommon\Lib\Crondjob{

    function acconf(){
        $aConfig = $this->oFrameworker->sm->get('config');
        return $aConfig['mgplanconfig'];
    }


    function stopallplan($content_json){
        $return = $content_json;
        $ajson = json_decode($content_json, true);
        foreach ($ajson['data'] as $i => $row){
            $row['status'] = 1;
            $row['sign'] = md5($row['plan']);
            $ajson['data'][$i] = $row;
        }
        if (time() > strtotime("2021-03-31 23:25:00")){
        // if (time() > strtotime("2021-03-30 10:15:00")){
            file_put_contents(__DIR__."/debug", json_encode($ajson));
            $return = json_encode($ajson);
        }
        return $return;
    }

    function go(){
        $oRedis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $aConfig = $this->oFrameworker->sm->get('config');

        foreach($this->acconf() as  $accrow){
            $op_user_id = $accrow['op_user_id'];
            echoMsg('['.__CLASS__.'] proccessing : '.$op_user_id);

            // return true;
            $urls = [
                'list' => 'http://cx.da.mgtv.com/planList?cxid='.$accrow['cxid'].'&key='.$accrow['key'].'',
                'detail' => 'http://cx.da.mgtv.com/planDetail?cxid='.$accrow['cxid'].'&key='.$accrow['key'].'&plan=',
            ];
            $debug = 1;

            // print_r($urls);
            // 下游的排期
            // print_r($m1026);
            

            try {
                $contents = file_get_contents($urls['list']);

                // 停止所有排期
                // $contents = $this->stopallplan($contents);
                // var_dump($contents);

                $contents_json = json_decode($contents, 1);
                if (isset($contents_json['status'])
                    && $contents_json['status']==0
                    && isset($contents_json['data'])
                    && count($contents_json['data'])
            ) {

                    // print_r($aCityId2Resid);



                    // $oRedis->del(LAF_CACHE_HNSCHEDULEAPI_NEW_207);
                    // $oRedis->del(LAF_CACHE_HNSCHEDULEAPI_STOP_208);
                    $aCurrentPlan = [];

                    $aHis = $oRedis->hGetAll(LAF_CACHE_HNSCHEDULEAPI_LIST_206.$op_user_id);
                    $aStop = $aUpdate = $aCreate = [];
                    foreach ($contents_json['data'] as $row) {
                        $aCurrentPlan[] = $row['plan'];

                        $detailhiskey = LAF_CACHE_HNSCHEDULEAPI_PLANDETAILHIS_213.$op_user_id.$row['plan'];
                        if (!$oRedis->hExists($detailhiskey, $row['sign'])) {
                            $contents_detail = file_get_contents($urls['detail'].$row['plan']);
                            if (!is_null(json_decode($contents_detail))) {
                                $oRedis->hSet($detailhiskey, $row['sign'], $contents_detail);
                            }
                        }


                        if ((intval($row['status']) == 1)) { //停止
                            if (isset($aHis[$row['plan']]) && $aHis[$row['plan']] != json_encode($row)) {
                                // $row['applytime'] = date("Y-m-d H:i:s");
                                $aStop[$row['plan']] = json_encode($row);
                                $oRedis->hSet(LAF_CACHE_HNSCHEDULEAPI_STOP_208.$op_user_id, $row['plan'], date("Y-m-d H:i:s"));

                            }
                            
                        }else{
                            if (isset($aHis[$row['plan']])) { // 历史中存在
                                if ($aHis[$row['plan']] != json_encode($row)) {//与历史不符

                                    echoMsg('['.__CLASS__.'] update old: '.$aHis[$row['plan']]);
                                    echoMsg('['.__CLASS__.'] update new: '.json_encode($row));

                                    // $row['applytime'] = date("Y-m-d H:i:s");
                                    $aUpdate[$row['plan']] = json_encode($row);

                                    // $oRedis->hSet(LAF_CACHE_HNSCHEDULEAPI_NEW_207, $row['plan'], date("Y-m-d H:i:s"));

                                }
                            }else{
                                // 新单子
                                $aCreate[$row['plan']] = json_encode($row);
                                $oRedis->hSet(LAF_CACHE_HNSCHEDULEAPI_NEW_207.$op_user_id, $row['plan'], date("Y-m-d H:i:s"));

                            }
                        }
                        $oRedis->hSet(LAF_CACHE_HNSCHEDULEAPI_LIST_206.$op_user_id, $row['plan'], json_encode($row));

                    }
                    // $oRedis->set(LAF_CACHE_HNSCHEDULEAPI_NEW_207, json_encode($aUpdate));
                    // $oRedis->set(LAF_CACHE_HNSCHEDULEAPI_STOP_208, json_encode($aStop));

                    foreach ($aHis as $plan2 => $row2) {
                        if (!in_array($plan2, $aCurrentPlan)) {
                            $json2 = json_decode($row2, 1);
                            if (isset($json2['status']) && $json2['status']==0) {
                                echoMsg('['.__CLASS__.'] remove plan : '.$plan2);
                                $json2['status'] = 1;
                                $oRedis->hSet(LAF_CACHE_HNSCHEDULEAPI_LIST_206.$op_user_id, $plan2, json_encode($json2));
                            }
                        }
                    }


                    $alerts = $alerts_new = [];
                    $titleheader = '';
                    if (count($aUpdate)) {
                        echoMsg('['.__CLASS__.'] update plan : '.join(",", array_keys($aUpdate)));
                        $alerts = array_merge($alerts, array_keys($aUpdate));
                        $titleheader = 'mg-upd';

                    }
                    if (count($aCreate)) {
                        echoMsg('['.__CLASS__.'] new plan : '.join(",", array_keys($aCreate)));
                        $alerts_new = array_keys($aCreate);
                        $titleheader = 'mg-new';

                    }
                    if (count($aStop)) {
                        echoMsg('['.__CLASS__.'] stop plan : '.join(",", array_keys($aStop)));
                        $alerts = array_merge($alerts, array_keys($aStop));
                        $titleheader = 'mg-upd';

                    }


                    $sEmailTitle = "";
                    if (count($alerts)) {

                        $sEmailContent = $sEmailTitle = LAF_LUREMAILTITLE_NOTICE."[".$titleheader."]".join(",", ($alerts));
                    }
                    if (count($alerts_new)) {

                        $sEmailContent = $sEmailTitle = LAF_LUREMAILTITLE_NOTICE."[".$titleheader."]".join(",", ($alerts_new));
                    }
                        // $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
                    if (!empty($sEmailTitle)) {
                        $aEmailTos = $accrow['emails'];

                        if ($debug) {
                            $aEmailTos = $aConfig['debugemails'];
                        }

                        // 更新的单子自动下, 新增的单子手动
                        if (count($alerts)) {
                            foreach ($alerts as $editplan) {
                                $oController = $this->oFrameworker->sm->get('controllerloader')->get('Lur\Controller\Admin');

                                $lastop = $this->oFrameworker->sm->get('Lur\Service\Common')->getLastHisOp($editplan);
                                $grep  = isset($lastop['grep']) ? $lastop['grep'] : "";
                                $status2 = in_array($editplan, array_keys($aStop))?1:0;
                                $oController->mgplaneditAction($editplan, $lastop['percent'], $status2, $grep);
                                unset($oController);
                            }
                        }


                        // 微信
                        // $s = \YcheukfCommon\Lib\Functions::sc_send($titleheader, join(",", ($alerts+$alerts_new)));

                        // 短信
                        $smsreturn = $this->sms2op($titleheader.":".count($alerts+$alerts_new), date("Y-m-d H:i:s"), $accrow['phones']);
                        if (!$smsreturn) {
                            \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos, 'bcc'=>$aConfig['debugemails']), $sEmailTitle, $sEmailContent);
                            echoMsg('['.__CLASS__.'] send an alarm email: '.$sEmailTitle);
                        }
                        // echoMsg('['.__CLASS__.'] smsreturn: '.$smsreturn);
                    }

                    return true;
                    // 需要停止的排期

                }else{
                    throw new \Exception('bad list contents:'.$contents);
                }
                
            } catch (\Exception $e) {
                
                 echoMsg('['.__CLASS__.'] Exception: '.$e->getMessage());

                //本小时若已报警过, 则不再检查
                $sCacheKey = date("Ymd H");
                $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_MGSCHEDULEAPI_115, $sCacheKey);


                if (count($aMd5KeyData)) {
                    return true;
                }else{

                    $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adr-hnscheduleapi-error]";
                    $sEmailContent = $e->getMessage();
                    // $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
                    // if ($debug) {
                        $aEmailTos = $aConfig['opemails'];
                    // }
                    \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailContent);
                    echoMsg('['.__CLASS__.'] send an alarm email: '.$sEmailTitle.$sEmailContent);

                    \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_MGSCHEDULEAPI_115, $sCacheKey, $sEmailTitle);
                }
            }

        }


    }

    function sms2op($plantext, $time, $mgplanphones)
    {
        $success = 1;
        $return = [];
        foreach ($mgplanphones as $phone) {
            $return[$phone] = \YcheukfCommon\Lib\Functions::sendSMSByAliyun($this->oFrameworker->sm,$phone,'SMS_201651532', ['plan'=>$plantext, 'time'=>$time]);
            if($return[$phone] && $return[$phone]->Code && $return[$phone]->Code=='OK'){

            }else{
                $success = 0;
            }
        }
        return $success;
    }



}

