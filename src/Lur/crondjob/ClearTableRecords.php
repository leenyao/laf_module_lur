<?php
/*
定期清理过大的表  


**/
class ClearTableRecords extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $bDebug = true;
        $aConfig = $this->oFrameworker->sm->get('config');
        $aDsn = \YcheukfCommon\Lib\Functions::getParamFromDSN($aConfig['db_master']['dsn']);
        $aDsnSlave = \YcheukfCommon\Lib\Functions::getParamFromDSN($aConfig['db_slave']['dsn']);
        $oPdo = \Application\Model\Common::getPDOObejct($this->oFrameworker->sm, "db_master"); //db_master|db_slave
        $sBackUpPath = '/app/data/dbtablerecoreds';

        

        // print_r($aConfig['intervalClearTables']);
        foreach ($aConfig['intervalClearTables'] as $aRow) {
            $sDate = date('Y-m-d', strtotime('-'.$aRow['activeDays'].' day', time()));
            $sDir = $sBackUpPath."/".$aRow['table'];
            $sBackFile = $sBackUpPath."/".$aRow['table']."/".str_replace("-", "", $sDate).".sql";




            if ($bDebug) {
                // echoMsg('['.__CLASS__.']activeDays: '.$aRow['activeDays']);
                // echoMsg('['.__CLASS__.']sBackFile: '.$sBackFile);
            }
            if (file_exists($sBackFile)) {
                continue;
            }


            $sSql = "select count(*) as total from laf_lur.{$aRow['table']} where DATE_FORMAT({$aRow['datefiled']}, \"%Y-%m-%d\")=:date";
            $oSth = $oPdo->prepare($sSql);
            $oSth->execute(array(
                ":date" => $sDate,
            ));
            $aResultTmp = $oSth->fetch(\PDO::FETCH_ASSOC);

            if (!($aResultTmp && isset($aResultTmp['total']) && $aResultTmp['total'] >0)) {
                continue;
            }

            exec("mkdir -p ".$sDir);

            $sScript = "mysqldump -u{$aConfig['db_slave']['username']} -p{$aConfig['db_slave']['password']} --port={$aDsnSlave['port']} -h{$aDsnSlave['host']}   --default-character-set=utf8  --skip-lock-table  --skip-add-drop-table --skip-add-locks -n -t laf_lur {$aRow['table']} --where=\" DATE_FORMAT({$aRow['datefiled']}, '%Y-%m-%d')='{$sDate}'\" > ".$sBackFile;

            exec($sScript);
            if ($bDebug) {
                echoMsg('['.__CLASS__.']dump-sql: '.$sScript);
            }

            echoMsg('['.__CLASS__.']backfile: '.$sBackFile);


            $sSql = "delete from laf_lur.{$aRow['table']} where DATE_FORMAT({$aRow['datefiled']}, \"%Y-%m-%d\")=:date";
         

             if ($bDebug) {
                 echoMsg('['.__CLASS__.']delete-sql: '.$sSql);
             }   
            $oSth = $oPdo->prepare($sSql);
            $oSth->execute(array(
                ":date" => $sDate,
            ));

        }


        return true;
    }
}

