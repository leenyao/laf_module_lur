<?php
/*
问题代码计数监视器
**/
class AdcounterMonitor extends \YcheukfCommon\Lib\Crondjob{

    var $code2count=null;
    function go(){

        if (date("H")<=8) {
            return true;
        }

        // $d = $this->getCodeCurrentCounter(118111);
        // var_dump($d);
        // $d = $this->getCodeCurrentCounter(120759);

        // var_dump($d);
        // exit;        

        $bFireAlarm = 0;
        $sCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".date("Ymd");
        $sCacheKey2 = LAF_CACHE_CODERUNTIME_350."/".date("Ymd");
        $sCacheKey3 = LAF_CACHE_CODERUNTIME_IGNORE_351."/".date("Ymd");
        $sCacheKey4 = LAF_CACHE_CODERUNTIME_REPORT_352."/".date("Ymd");
        $oRedis = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis();

        // $oRedis->del($sCacheKey2);

        $aOffsetData = $oRedis->hGetAll($sCacheKey);
        foreach ($aOffsetData as $jobkeytmp2 => $count2) {
            list($nCodeId) = explode("-", $jobkeytmp2);
            if(!$oRedis->hExists($sCacheKey2, $nCodeId)){
                $oRedis->hSet($sCacheKey2, $nCodeId, time());
            }
        }
        $code2start = $oRedis->hGetAll($sCacheKey2);
        $aMails = [];

        foreach ($code2start as $nCodeId => $ts) {
            if(empty($nCodeId))continue;

            if(time()-$ts >= 900
                && !$oRedis->hExists($sCacheKey3, $nCodeId)
            ){
                list($aScheduleEntity, $nScheduleTotal) = $this->oFrameworker->sm->get('\Lur\Service\Common')->getScheduleCountByDate($nCodeId, date("Y-m-d"));

                if ($aScheduleEntity['split_index']>0
                    || $aScheduleEntity['cookie_code_id']>0
                ) {
                    $oRedis->hDel($sCacheKey4, $nCodeId);

                    continue;
                }

                $count = $this->getCodeCurrentCounter($nCodeId);

                // echoMsg('['.__CLASS__.'] STH WRONG2');

                if ($count > 0) {
                    $oRedis->hSet($sCacheKey3, $nCodeId, $count);
                    $oRedis->hDel($sCacheKey4, $nCodeId);

                }else{
                    if(!$oRedis->hExists($sCacheKey4, $nCodeId)){


                        $oRedis->hSet($sCacheKey4, $nCodeId, json_encode([$aScheduleEntity['code'],$aScheduleEntity['code_label'],$aScheduleEntity['user_id'],$aScheduleEntity['op_user_id'], time()]
                        ));
                        $bFireAlarm = 1; // 存在新增
                        $aMails[$aScheduleEntity['op_user_id']] = $aScheduleEntity['op_user_id'];
                    }
                }
            }
        }


        if ($bFireAlarm==1) {
            
            $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adr-code-notrun]出现问题代码,请到系统查看";
            // $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADMINEMAILS);

            $aEmailToBccs = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_MTYPE_TECHOPMAIL_1024);

            foreach ($aMails as $nOpUserId=>$v2) {
                
                $aTmp5 = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'user', $nOpUserId);

                \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>[$aTmp5['email']]), $sEmailTitle, $sEmailTitle);
                echoMsg('['.__CLASS__.'] send an adr-code-notrun email :'.$aTmp5['email']);
            }



        }
        // print_r($aMails);
        // print_r($sCacheKey2);
        $oRedis->setTimeout($sCacheKey2, 86400*3);
        $oRedis->setTimeout($sCacheKey3, 86400*3);
        $oRedis->setTimeout($sCacheKey4, 86400*3);
        return true;
    }


    public function getCodeCurrentCounter($sId){
        $sDate = date("Y-m-d");
        list($aScheduleEntity, $nScheduleTotal) = $this->oFrameworker->sm->get('\Lur\Service\Common')->getScheduleCountByDate($sId, $sDate);

        if (!isset($aScheduleEntity['id']) || !isset($aScheduleEntity['user_id']) || !isset($aScheduleEntity['op_user_id'])) {
            echoMsg('['.__CLASS__.'] STH WRONG :id='.$sId.json_encode($aScheduleEntity));
            return 0;
        }

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $aAdrSerVerAll = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEY_1018);

        $aConfig = $this->oFrameworker->sm->get('config');
        $return = 0;
        $sDate = date("Ymd", strtotime($sDate));
        // ssp
        if (1) {

            foreach ($aAdrSerVerAll as $nServerId=>$s) {
                $sKey2 = $nServerId."/".$aScheduleEntity['id']."/".$aScheduleEntity['user_id']."/".$aScheduleEntity['op_user_id'];
                $stmp3 = $oReids->hGet(LAF_CACHE_COUNTER_DAYSET4SHOW_408.$sDate, $sKey2);
                // var_dump($oReids->hExists(LAF_CACHE_COUNTER_DAYSET4SHOW_408.$sDate, $sKey2));
                // var_dump($stmp3);
                $stmp3_json = json_decode($stmp3, 1);


                if (is_array($stmp3_json)) {
                    if (isset($aScheduleEntity['m1014_id']) && 
            in_array($aScheduleEntity['m1014_id'], $aConfig['ssp_platforms'])) {
                        $return += intval($stmp3_json['c_sad']);
                    }else{
                        $return += intval($stmp3_json['c']);
                    }
                }

            }
            return $return;
        }

    }
}

