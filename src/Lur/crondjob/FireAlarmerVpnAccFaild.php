<?php
/*
报警器 , 
检测 VPN拨号状态

**/
class FireAlarmerVpnAccFaild extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $aConfig = $this->oFrameworker->sm->get('config');
        $nAlarmMax =30;

        //本小时若已报警过, 则不再检查
        $sCacheKey = date("Ymd H");
        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_VPNACCFAILD_108, $sCacheKey);
        if (count($aMd5KeyData)) {
            return true;
        }



        $aNeedToAlarm = array();
        $aFaildToomanyAcc = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getFaildTooManyAcc();
        if (count($aFaildToomanyAcc)) {

            $aVpnAccs = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAllVpnAccs();
            foreach ($aFaildToomanyAcc as $accid => $count) {
                if ($count>$nAlarmMax) {

                    $aAcc = 
                    $aNeedToAlarm[$accid] = array(
                        'acc' => $aVpnAccs[$accid],
                        'count' => $count,
                    );
                }
            }
        }
        // var_dump($aNeedToAlarm);

        if (count($aNeedToAlarm)) {

            $sEmailTitle = LAF_LUREMAILTITLE_WARRING."[vpnacc-faild-toomany] ".count($aNeedToAlarm)." account";
            $sTmp = print_r($aNeedToAlarm, 1);
            $sEmailContent = <<<OUTPUT
            the account below faild > {$nAlarmMax} times in two hours.
        {$sTmp}
OUTPUT;

            $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
            \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailContent);
            echoMsg('['.__CLASS__.'] send an alarm email: '.$sEmailTitle);

            \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_VPNACCFAILD_108, $sCacheKey, $sEmailTitle);
        }


        return true;
    }



}

