<?php
/*
历史数据进表. 数据处理的两种方式



**/
class ImportAdrData extends \YcheukfCommon\Lib\Crondjob{


    var $bForceFlag = false;//是否为强制指定倒库行为
    function go(){


        $aConfig = $this->oFrameworker->sm->get('config');

        //多余的
        $this->oFrameworker->sm->get('\Lur\Service\Common')->updateScheduleCache();
        
        $nTotalProcess = $this->oFrameworker->sm->get('\Lur\Service\Common')->flushCacheCounter2Db();


        //允许接受命令行参数
        if (isset($_SERVER['argv'][2])) {
            $sYestoday = $_SERVER['argv'][2];
            if (!preg_match("/^\d{4}-\d{2}-\d{2}$/i", $sYestoday)) {
                echoMsg('['.__CLASS__.'] wrong date format:'.$sYestoday);

            }
            $this->bForceFlag = true;
        }else{
            $sYestoday = date("Y-m-d", strtotime("-1 day"));
        }

        $aUrlids =  $aUrlids2 = array();

        echoMsg('['.__CLASS__.'] start porcessing adrreport '.$sYestoday);

        $sSql = "select distinct code_id from b_adcounter where date='".$sYestoday."'";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
        $aTmp5 = array();
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
                list($aScheduleEntity, $nScheduleTotal) = $this->oFrameworker->sm->get('\Lur\Service\Common')->getScheduleCountByDate($result['code_id'], $sYestoday);

                if (empty($aScheduleEntity)) {
                    echoMsg('['.__CLASS__.'] no such code_id=>'.$result['code_id']);
                    continue;
                }

                $sUserId = intval($aScheduleEntity['user_id']);
                $sShowUrlTmp = $aScheduleEntity['code'];
                $aTmp5[] = $sShowIdTmp = intval($result['code_id']);


                // 通过实时表去取数据
                list($bFalg, $aReportData) = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAdrReportDataForCounterTable(array($sShowIdTmp), $sYestoday, $sYestoday, 2);

                /**
                 * 若是被人为禁止的排期, 不进行数据请求
                 */
                if ($this->bForceFlag===false && intval($aScheduleEntity['m102_id']) !== 1 ) {
                    $sDate1 = date("Y-m-d", strtotime($aScheduleEntity['modified']));//最后禁止时间
                    $sDate2 = date("Y-m-d", strtotime("-1 day"));//昨天的时间
                    if (strtotime($sDate1) < strtotime($sDate2)) {
                        echoMsg('['.__CLASS__.'] skip forbiddened code_id=>'.$result['code_id']);
                        continue;
                    }
                }


                $nShowCount = isset($aReportData[$sYestoday][$sShowIdTmp]) ? $aReportData[$sYestoday][$sShowIdTmp] : 0;

                $sSql = "delete from b_report_e where date='".$sYestoday."' and user_id='".$sUserId."'  and de1='".$sShowIdTmp."'";
                $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
                if (empty($nShowCount)) {
                    echoMsg('['.__CLASS__.'] skip zero-counter code_id=>'.$result['code_id']);
                    continue;
                }

                $sSql = "insert into b_report_e set date='".$sYestoday."', user_id='".intval($sUserId)."', de1='".$sShowIdTmp."', de2='".$sShowUrlTmp."', me1=".$nShowCount.", me2=".$nScheduleTotal;
                $oPDOStatement = $this->oFrameworker->queryPdo($sSql);

            }

            echoMsg('['.__CLASS__.'] urlids=>'.json_encode($aTmp5));
        }



        //本小时若已报警过, 则不再检查
        $sCacheKey = $sYestoday;
        $sLog = \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_ADRDATAFINISHED, $sCacheKey, "");

        echoMsg("[".__CLASS__."] DEBUG", json_encode($sLog));

        echoMsg("[".__CLASS__."] batchdataimport-finished ".$sYestoday.",".count($aTmp5));

        //发信通知运营完成
        // $sEmailTitle = LAF_LUREMAILTITLE_NOTICE."[batchdataimport-finished]".$sYestoday.",".count($aTmp5);
        // $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
        // \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, 
        //     $sEmailTitle);
        // echoMsg('['.__CLASS__.'] send an alarm email: '.$sEmailTitle. "; emails:".json_encode($aEmailTos));

        return true;
    }


}

