<?php
/*
导入第三方排期, letv

**/
include_once(__DIR__."/ImportAdschedule.php");
class ImportAdschedule3rd extends \ImportAdschedule{


    function go(){
        $aLocationMap = array(

            "1156310000" => "1",    //"上海",
            "1156330100" => "14",   //"杭州",
            "1156110000" => "15",   //"北京",
            "1156440100" => "16",   //"广州",
            "1156440300" => "17",   //"深圳",
            "1156120000" => "18",   //"天津",
            "1156320100" => "19",   //"南京",
            "1156500000" => "20",   //"重庆",
            "1156370200" => "21",   //"青岛",
            "1156210200" => "22",   //"大连",
            "1156510100" => "23",   //"成都",
            "1156420100" => "24",   //"武汉",
            "1156610100" => "25",   //"西安",
            "1156430100" => "26",   //"长沙",
            "1156410100" => "27",   //"郑州",
            "1156441900" => "28",   //"东莞",
            "1156140100" => "29",   //"太原",
            "1156530100" => "30",   //"昆明",
            "1156210100" => "31",   //"沈阳",
            "1156330200" => "32",   //"宁波",
            "1156320200" => "33",   //"无锡",
            "1156410100" => "34",   //"郑州",
            "1156230100" => "35",   //"哈尔滨",
            "1156360100" => "36",   //"南昌",
            "1156440600" => "37",   //"佛山",
            "1156450100" => "38",   //"南宁",
            "1156330400" => "39",   //"嘉兴",
            "1156650100" => "40",   //"乌鲁木齐",
            "1156321000" => "41",   //"扬州",
            "1156331000" => "42",   //"台州",
            "1156450900" => "43",   //"玉林",
            "1156330300" => "44",   //"温州",
            "1156340100" => "45",   //"合肥",
            "1156340200" => "46",   //"芜湖",
            "1156320500" => "47",   //"苏州",
            "1156370100" => "48",   //"济南",
            "1156350200" => "49",   //"厦门",
            "1156130100" => "50",   //"石家庄",
            "1156350100" => "51",   //"福州",
            "1156220100" => "52",   //"长春",
            "1156350500" => "53",   //"泉州",
            "1156320600" => "54",   //"南通",
            "1156321100" => "55",   //"镇江",
            "1156320300" => "56",   //"徐州",
            "1156330900" => "57",   //"舟山",
            "1156330500" => "58",   //"湖州",
            "1156321200" => "59",   //"泰州",
            "1156320900" => "60",   //"盐城",
            "1156331100" => "61",   //"丽水",
            "1156330800" => "62",   //"衢州",
            "1156370800" => "63",   //"济宁",
            "1156370600" => "64",   //"烟台",
            "1156370700" => "65",   //"潍坊",
            "1156411300" => "66",   //"南阳",
            "1156320700" => "67",   //"连云港",
            "1156320800" => "68",   //"淮安",
            "1156371400" => "69",   //"德州",
            "1156430200" => "70",   //"株洲",
            "1156430600" => "71",   //"岳阳",
            "1156350600" => "72",   //"漳州",
            "1156420800" => "73",   //"荆门",
            "1156370300" => "74",   //"淄博",
            "1156340300" => "75",   //"蚌埠",
            "1156410300" => "76",   //"洛阳",
            "1156340800" => "77",   //"安庆",
            "1156420500" => "78",   //"宜昌",
            "1156330700" => "79",   //"金华",
            "1156371000" => "80",   //"威海",
            "1156430400" => "81",   //"衡阳",
            "1344000000" => "82",   //"香港"

        );

        $aUaMap = array(
            "ipad" => array(4),
            "iphone" => array(2),
            "aphone" => array(3),
            "pcmobile" => array(1,2,3),
        );


        $sSql = "select * from b_schedule3rd where status in (1,2,3,4,5) order by id asc";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
                // {\"adverName\":\"ip+ipad\",\"adverPlan\":{\"2016-07-06\":1,\"2016-07-05\":1},\"clickMonitors\":[\"http://ark.letv.com/k?_oid=2242&_rtname=click&_rt=2&_u=111\"],\"completeMonitors\":[],\"coopAdverCode\":\"68\",\"directStrategy\":{\"isAreaNegative\":0,\"putAreaId\":[1156000000],\"putHourPeriodId\":[1,2,5,6,7],\"clickRate\":33.33,\"platform\":\"iphone\"},\"endDate\":\"2016-07-06 23:59:59\",\"frequencyPeriod\":0,\"landingPage\":\"\",\"materialDuration\":0,\"materialURL\":\"\",\"maxFrequency\":0,\"minFrequency\":0,\"startDate\":\"2016-07-05 00:00:00\",\"viewMonitors\":[\"http://ark.letv.com/k?_oid=2242&_rtname=Impression&_rt=1&_u=111\"],\"memo\":\"频控2\"}
                // https://mail.qq.com/cgi-bin/readmail?sid=-ClGmJqKJzeyfX4p&mailid=ZL0114-LPk0S0mb3s6RQkEW5gM~H66&nocheckframe=true&t=attachpreviewer&select=1&selectfile=&seq=
                // var_dump($result['json_data']);
                $sMsg = "OK";
                $bFlag = true;
                $aJsonData = json_decode($result['json_data'], 1);
                // var_dump($aJsonData);
                // exit;
                $aUas = $aUaMap['pcmobile'];
                $aLocations = array();
                $aServers = array(5,6);
                $aApps = array(1);
                if (is_null($aJsonData)) {
                    $sMsg = "unavailable json";
                    $bFlag = false;
                }

                if(count($aJsonData['directStrategy']['putAreaId'])){
                    if ($aJsonData['directStrategy']['putAreaId'] == array("1156000000")) {
                        $aJsonData['directStrategy']['putAreaId'] = array("1156310000", "1156330100", "1156110000", "1156440300", "1156120000");

                    }
                    foreach ($aJsonData['directStrategy']['putAreaId'] as $sLocationTmp) {
                        if (!isset($aLocationMap[$sLocationTmp])) {
                            $sMsg = "unavailable location:".$sLocationTmp;
                            $bFlag = false;
                        }else{
                            $aLocations[] = $aLocationMap[$sLocationTmp];
                        }
                    }
                    unset($sLocationTmp);

                    //反向地域, 选择5个
                    if ($bFlag && isset($aJsonData['directStrategy']['isAreaNegative']) && $aJsonData['directStrategy']['isAreaNegative']==1) {
                        $aLocations = array();
                        foreach ($aLocationMap as $sLocationTmp=>$sTmp4) {
                            if (!in_array($sLocationTmp, $aJsonData['directStrategy']['putAreaId'])) {
                                $aLocations[] = $sTmp4;
                                if (count($aLocations) >=5) {
                                    break;
                                }
                            }
                        }
                        unset($sLocationTmp);
                        unset($sLocationTmp2);
                        unset($sTmp4);
                    }
                }
                if(!empty($aJsonData['platform'])){
                    if (isset($aUaMap[$aJsonData['platform']])) {
                        $aUas = $aUaMap[$aJsonData['platform']];
                    }else{
                        $sMsg = "unavailable uakey:".$aJsonData['platform'];
                        $bFlag = false;
                    }
                }

                if(empty($aJsonData['viewMonitors'])){
                    $sMsg = "empty viewMonitors";
                    $bFlag = false;
                }
                if(empty($aJsonData['adverPlan'])){
                    $sMsg = "empty adverPlan";
                    $bFlag = false;
                }
                if ($bFlag) {
                    $oContentController = $this->oFrameworker->sm->get('controllerloader')->get('Application\Controller\AdminContentController');

                    $sHours = "";
                    $aDbData = array();
                    if (count($aJsonData['directStrategy']['putHourPeriodId'])) {
                        $sHours = join(",", $aJsonData['directStrategy']['putHourPeriodId']);
                    }
                    foreach ($aJsonData['viewMonitors'] as $sUrlTmp) {
                        $aCodes = $this->splitRequestUrl($sUrlTmp);
                        $aShow4Click = array();
                    }
                    $aScheduleShow = array();
                    foreach ($aJsonData['adverPlan'] as $sDateTmp3 => $nCpm) {
                        $aScheduleShow[] = array(
                            empty($sHours) ? $sDateTmp3 : $sDateTmp3." ".$sHours,
                            (isset($aJsonData['maxFrequency']) && !empty($aJsonData['maxFrequency'])) ? $aJsonData['maxFrequency'] : 1,
                            100,
                            $nCpm*1000,
                        );
                    }
                    foreach ($aCodes as $split_index => $sCodeTmp) {
                        $nM102Id = 1;
                        if ($split_index > 0) {//split-url 检查
                             $bIgnoreFlag = $this->getDomainIgnoreStatus($sCodeTmp);
                            if ($bIgnoreFlag) {
                                $nM102Id = 2;
                            }
                        }
                        $aEntity = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', trim($sCodeTmp), 'code');
                        $sEntityId = isset($aEntity['id']) ? $aEntity['id'] : "";


                        $aTmpRow = array(
                            'id' => $sEntityId,
                            'aShow4Click' => $aShow4Click,//嵌套关联的显示代码
                            'relation___1007' => empty($aLocations) ? array() : $aLocations,//city
                            'relation___1008' => empty($aUas) ? array() : $aUas,//ua
                            'relation___1009' => empty($aServers) ? array() : $aServers,//server
                            'relation___1015' => empty($aApps) ? array() : $aApps,//apps
                            'user_id' => 1,
                            'code' => $sCodeTmp,
                            'split_father_url' => $sUrlTmp, 
                            'split_index' => $split_index,
                            'code_label' => $aJsonData['adverName']."-show",
                            'referer' => "",
                            'm102_id' => $nM102Id,
                            'm1014_id' => 2,//source type
                            'stable_cookie_percent' => 0,
                            'schedule' => json_encode($aScheduleShow),
                        );
                        if (empty($sEntityId)) {
                            $aTmpRow['created_time'] = date('Y-m-d H:i:s');
                        }
                        $aDbData['show'][] = $aTmpRow;
                        $aShow4Click[] = array(
                            'cookie_domain' => $this->oFrameworker->sm->get('\Lur\Service\Common')->getCookieDomain($sCodeTmp),
                            'code' => $sCodeTmp,
                        );
                    }
                    

                    if (count($aJsonData['clickMonitors'])) {

                        foreach ($aJsonData['clickMonitors'] as $sUrlTmp) {
                            $aCodes = $this->splitRequestUrl($sUrlTmp);
                        }
                        $aScheduleShow = array();
                        foreach ($aJsonData['adverPlan'] as $sDateTmp3 => $nCpm) {
                            $aScheduleShow[] = array(
                                empty($sHours) ? $sDateTmp3 : $sDateTmp3." ".$sHours,
                                1,
                                100,
                                $nCpm*1000*$aJsonData['directStrategy']['clickRate']/100,
                            );
                        }
                        foreach ($aCodes as $split_index => $sCodeTmp) {
                            $nM102Id = 1;
                            if ($split_index > 0) {//split-url 检查
                                 $bIgnoreFlag = $this->getDomainIgnoreStatus($sCodeTmp);
                                if ($bIgnoreFlag) {
                                    $nM102Id = 2;
                                }
                            }
                            $aEntity = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', trim($sCodeTmp), 'code');
                            $sEntityId = isset($aEntity['id']) ? $aEntity['id'] : "";


                            $aTmpRow = array(
                                'id' => $sEntityId,
                                'aShow4Click' => $aShow4Click,//嵌套关联的显示代码
                                'relation___1007' => empty($aLocations) ? array() : $aLocations,//city
                                'relation___1008' => empty($aUas) ? array() : $aUas,//ua
                                'relation___1009' => empty($aServers) ? array() : $aServers,//server
                                'relation___1015' => empty($aApps) ? array() : $aApps,//apps
                                'user_id' => 1,
                                'code' => $sCodeTmp,
                                'split_father_url' => $sUrlTmp, 
                                'split_index' => $split_index,
                                'code_label' => $aJsonData['adverName']."-click",
                                'referer' => "",
                                'm102_id' => $nM102Id,
                                'm1014_id' => 2,//source type
                                'stable_cookie_percent' => 0,
                                'schedule' => json_encode($aScheduleShow),
                            );
                            if (empty($sEntityId)) {
                                $aTmpRow['created_time'] = date('Y-m-d H:i:s');
                            }
                            $aDbData['click'][] = $aTmpRow;
                        }
                    }

// var_dump($aDbData);
                    $aExists = $aDbIds = array();
                    foreach ($aDbData as $k3 => $aTmp4) {
                        if (count($aTmp4)) {
                            foreach ($aTmp4 as $key => $a3) {
                                if (count($a3) < 1) {
                                    continue;
                                }
                                $sTmp3 = strtoupper($a3['code']);
                                $sMd5Tmp = md5($sTmp3);
                                if (isset($aExists[$sMd5Tmp])) {
                                    return $this->doErrorMsg($key, "mutiple code", $a3['code']);
                                }
                                $aExists[$sMd5Tmp] = true;
                            }
                            foreach ($aTmp4 as $key => $a3) {

                                if (count($a3) < 1) {
                                    $aDbIds[$k3][] = new stdClass();
                                }else{

                                    //将点击的urlid与显示的url id关联, 只关联同一个域名下的
                                    if (isset($a3['aShow4Click']) && !empty($a3['aShow4Click'])) {
                                        $nCookieCodeId = 0;
                                        // var_dump($a3['aShow4Click']);
                                        foreach ($a3['aShow4Click'] as $aTmpRow2) {
                                            if ($aTmpRow2['cookie_domain'] === $this->oFrameworker->sm->get('\Lur\Service\Common')->getCookieDomain($a3['code'])) {
                                                $aCookieCodeId = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', trim($aTmpRow2['code']), 'code');
                                                $nCookieCodeId = isset($aCookieCodeId['id']) ? $aCookieCodeId['id'] : "";
                                                break;
                                            }
                                        }

                                        $a3['cookie_code_id'] = $nCookieCodeId;
                                        unset($a3['aShow4Click']);
                                    }
                                    if (isset($a3['split_father_url']) && !empty($a3['split_father_url'])) {
                                        // var_dump($a3['split_father_url'],$a3['code']);
                                        $aSplitFather = ($a3['split_father_url']===$a3['code']) ? array() : \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', trim($a3['split_father_url']), 'code');
                                        $nTmpId = isset($aSplitFather['id']) ? $aSplitFather['id'] : 0;
                                        // var_dump($nTmpId);

                                        $a3['split_fid'] = $nTmpId;
                                        unset($a3['split_father_url']);
                                    }
                                    
                                    $aPost = array('params' => json_encode($a3));
                                    $sActionReturn = $oContentController->contenteditAction($a3['id'], "", "", "adschedule", $aPost);
                                    $aTmp3 = array();
                                    $aTmp3['sActionReturn'] = $sActionReturn;
                                    $aTmp3['postParams'] = $a3;
                                    if ($a3['split_index'] == 0) {
                                        $aDbIds[$k3][] = array(
                                            'id' => $sActionReturn,
                                            'url' => $a3['code'],
                                        );
                                    }

                                }
                            }
                        }
                    }
                }



// var_dump($bFlag, $sMsg);
                // $this->updateTable(5, "Processing, it will take a little long time when you post a new url...wait, plz", $result['id']);
                // $t1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat(    );
                // @list($bFlag, $sResult, $nUserId, $sOutJson) = $this->doImport($result['in_file']);
                // $t2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat(    );
                // exit;
                if ($bFlag) {
                    $this->updateTable(10, $result['id'], $sMsg);
                }else{
                    $this->updateTable(20, $result['id'], $sMsg);
                }
            }
        }

        return true;
    }


    public function updateTable($nStatus, $sId, $sMemo="")
    {

        $sSql = "update b_schedule3rd set status=?, memo=? where id in (?)";
        $sth = $this->oFrameworker->pdo->prepare($sSql);
        $sth->execute(array($nStatus, $sMemo, $sId));
        echoMsg('['.__CLASS__.'] '.$sId."/".$nStatus);
    }

}

