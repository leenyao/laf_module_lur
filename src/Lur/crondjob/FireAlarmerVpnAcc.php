<?php
/*
检查vpn账号是否将要过期

**/
class FireAlarmerVpnAcc extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $aConfig = $this->oFrameworker->sm->get('config');

        $this->chkVpnAccount($aConfig);


        return true;
    }



    /**
     * 检查adr心跳
     * 
     */
    public function chkVpnAccount($aConfig)
    {

        $sCacheKey = date("Ymd");

        //已报警过, 则不再检查
        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_USFIREALARM, $sCacheKey);
        if (count($aMd5KeyData)) {
            return true;
        }
        // echoMsg('['.__CLASS__.'] start checking alarm email');

        $aDisableRows = array();
        $aDisableIds = array();

        $sSql = "select * from `laf_lur`.`vpn_accounts` where status=1 and disabledate<='".date("Y-m-d",strtotime("+3 day"))."'";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
        $aData = array();
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
                if (date("Ymd", strtotime($result['disabledate'])) <= $sCacheKey) {
                    // var_dump($result);
                    $aDisableRows[] = $result;
                    $aDisableIds[] = $result['id'];
                }else{
                    //若心跳在, 则不需要报警
                    $aData[] = $result;
                }
            }
        }
        // var_dump($aDisableIds);

        $sEmailTitle = "";
        if (count($aDisableRows)) {

            $sSql = "update `laf_lur`.`vpn_accounts` set status=2 where id in (".join(",",$aDisableIds).")";
            $oPDOStatement = $this->oFrameworker->queryPdo($sSql);

            $sEmailTitle = LAF_LUREMAILTITLE_WARRING."[adrvpn-account-disabled] ".count($aDisableRows)." 个VPN账号已被禁止";
            $sEmailContents = json_encode($aDisableRows);
            $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
            \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailContents);
            echoMsg('['.__CLASS__.'] send an alarm email :'.$sEmailTitle. " to ".json_encode($aEmailTos));
            \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_USFIREALARM, $sCacheKey, $sEmailTitle);
        }

        $sEmailTitle = "";
        if (count($aData)) {
            $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adrvpn-account-expired] ".count($aData)." 个VPN账号即将于3天后过期";
            $sEmailContents = json_encode($aData);
            $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
            \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailContents);
            echoMsg('['.__CLASS__.'] send an alarm email :'.$sEmailTitle. " to ".json_encode($aEmailTos));



            $this->oFrameworker->sm->get('Lur\Service\Common')->smsSos($sEmailTitle);
            echoMsg('['.__CLASS__.'] send SMS ');

            \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_USFIREALARM, $sCacheKey, $sEmailTitle);
        }




    }

}

