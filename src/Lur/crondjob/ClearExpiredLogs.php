<?php
/*
清理过期日志

**/
class ClearExpiredLogs extends \YcheukfCommon\Lib\Crondjob{



    function go(){


        $bDebug = false;
        // $bDebug = true;
        if ($bDebug) {
            $aLogClearConfig = [
                // '/app/data/addtionallogs_adridhist_send' => [
                //     "nActiveDay" => 5,      
                //     "nCompressTime" => 10,  
                // ],
                // '/app/data/adrresult' => [
                //     "nActiveDay" => 5,      
                //     "nCompressTime" => 10,  
                // ],
                '/app/public/tmpupload/' => [
                    "nActiveDay" => 200,      
                    "nCompressTime" => 10,  
                ],
                // '/app/data/dbtablerecoreds/b_report_e' => [
                //     "nActiveDay" => 5,      
                //     "nCompressTime" => 10,  
                // ],
                // '/app/data/dbtablerecoreds/system_trigger_log' => [
                //     "nActiveDay" => 5,      
                //     "nCompressTime" => 10,  
                // ],
            ];

            $aRedisCacheClearConfig = [
              // LAF_CACHE_BLACKIPS_HIS_124 =>[
              //   'format' => '[key][date|Ymd]',
              //   'type' => 'hash',
              //   'nActiveDay' => 2,
              // ],
              // LAF_CACHE_VPNFAILD_HISTORY_120 =>[
              //   'format' => '[key][date|Ymd]',
              //   'type' => 'list',
              //   'nActiveDay' => 2,
              // ],
              // LAF_CACHE_COUNTER_SSP_DAYSET4SHOW_409 =>[
              //   'format' => '[key][date|Ymd]',
              //   'type' => 'hash',
              //   'nActiveDay' => 5,
              // ],
              // LAF_CACHE_COUNTER_DAYSET4SHOW_408 =>[
              //   'format' => '[key][date|Ymd]',
              //   'type' => 'hash',
              //   'nActiveDay' => 5,
              // ],
              // LAF_CACHE_VPNIPS_HIS_125 =>[
              //   'format' => '[key][date|Ymd]',
              //   'type' => 'hash',
              //   'nActiveDay' => 5,
              // ],
              // LAF_CACHE_ADDTIONAL_DATEINIT_315 =>[
              //   'format' => '[key][date|Ymd]',
              //   'type' => 'set',
              //   'nActiveDay' => 5,
              // ],
            ];

        }else{
            $aConfig = $this->oFrameworker->sm->get('config');

            $aLogClearConfig = $aConfig['logClearConfig'];
            $aRedisCacheClearConfig = $aConfig['redisCacheClearConfig'];
        }
        $oObject = \Application\Model\Common::getCacheObject($this->oFrameworker->sm);




        // foreach ($aRedisCacheClearConfig as $key => $row) {
          
        //     $aMatchKeys = $oObject->keys($key."*");
        //     foreach ($aMatchKeys as $s1) {

        //       if ($oObject->ttl($s1) === -1) {
        //         $oObject->setTimeOut($s1, 86400*5);
        //         var_dump($s1);
        //       }
        //     }
        // }
        // exit;

        $nCurrentTime = time();
        $sCacheHisDir = '/app/data/cachehistory';
        foreach ($aRedisCacheClearConfig as $key => $row) {
            $sCacheKey = '';
            $sExpiredTimeSpan = $nCurrentTime-(86400*$row['nActiveDay']);
            preg_match_all('/\[date\|(.*)\]/', $row['format'], $aMatcheDate);
            if (count($aMatcheDate) && isset($aMatcheDate[1][0])) {
              $sExpiredDate = date($aMatcheDate[1][0], $sExpiredTimeSpan);
              $sCacheKey = $row['format'];
              $sCacheKey = str_replace('[key]', $key, $sCacheKey);
              $sCacheKey = preg_replace('/\[date\|.*\]/', $sExpiredDate, $sCacheKey);

            }

            if (!empty($sCacheKey)) {
              $aMatchKeys = $oObject->keys($sCacheKey."*");

              if ( $aMatchKeys && count($aMatchKeys)) {
                $sDirTmp1 = $sCacheHisDir."/".str_replace("/", "", $key);


                $aLogClearConfig[$sDirTmp1] = [
                  "nActiveDay" => 2,      
                  "nCompressTime" => 200,  
                ];

                exec("mkdir -p ".$sDirTmp1);
                foreach ($aMatchKeys as $sCacheKey2) {
                  $sHisFileName = $sDirTmp1."/".date("Ymd", $sExpiredTimeSpan).".".str_replace("/", "_", $sCacheKey2);
                  $aData1 = null;
                  switch ($row['type']) {
                    case 'hash':
                      $aData1 = json_encode($oObject->hGetAll($sCacheKey2));
                      break;
                    
                    case 'list':
                      $aData1 = json_encode($oObject->lRange($sCacheKey2, 0, -1));
                      break;
                    case 'set':
                      $aData1 = json_encode($oObject->sMembers($sCacheKey2));
                      break;
                    default:
                      break;
                  }

                  if (!is_null($aData1)) {
                    file_put_contents($sHisFileName, json_encode($aData1));
                    $oObject->del($sCacheKey2);
                    echoMsg('['.__CLASS__.'] flush 2 disk:'.$sHisFileName);
                  }

                }
              }
            }
        }
        // print_r($aLogClearConfig);

        // exit;

        foreach ($aLogClearConfig as $key => $config_tmp) {
            $this->doClear($key, $config_tmp['nActiveDay'], $config_tmp['nCompressTime'], $bDebug);//保留 20*(5+1)
        }
        echoMsg('['.__CLASS__.'] DONE');
        return true;
    }


    function moveFile2DateDir($dir, $sLogFileDate, $sFilePath)
    {
      if (!is_null($sLogFileDate)) {
        $sFilePathTmp = null;
        if (file_exists($sFilePath)) {
          $sFilePathTmp = $sFilePath;
        }elseif(file_exists($sFilePath.".bz2")){
          $sFilePathTmp = $sFilePath.".bz2";
        }
        if (!is_null($sFilePathTmp)) {
          exec("mkdir -p $dir/backup-$sLogFileDate");
          exec("mv -f ".$sFilePathTmp." ".$dir."/backup-".$sLogFileDate."/");
        }
      }
    }

    /**
     * 日志 nActiveDay天内不做处理
     * 日志 > nActiveDay && < nActiveDay*$nCompressTime 压缩
     * 日志 大于 nActiveDay*$nCompressTime 删除
     * 即日志保留时间为 nActiveDay*(1+$nCompressTime+$nCompressTime) 天
     * @param  [type] $dir             [description]
     * @param  [type] $nActiveDay [description]
     * @return [type]                  [description]
     */
    function doClear($dir, $nActiveDay, $nCompressTime=3, $bDebug=false)
    {
      $nCurrentTime = time();
      if(is_dir($dir))
        {
          if ($dh = opendir($dir)) 
          {
            $aBackupFiles = array();
              while (($file = readdir($dh)) !== false)
            {
              if($file!="." && $file!="..")
              {
                $bZipFileFlag = preg_match("/\.bz2$/i", $file) ? true : false;
                $sFilePath = $dir."/".$file;

                if (preg_match("/backup-20\d{6}/i", $sFilePath)) {
                  $aBackupFiles[] = $sFilePath;
                }


                
                $sLogFileDate = null;
                $nRealmTime = $nFilemTime = filectime($sFilePath);

                preg_match_all("/[-|.]?(20\d{6})/i", $sFilePath, $aMatches);
                if (count($aMatches) 
                    // && $bZipFileFlag==true
                    && isset($aMatches[1]) 
                    && count($aMatches[1])
                    && isset($aMatches[1][0]) 
                    && strtotime($aMatches[1][0]) !== false
                ) {// 真实的创建时间
                    $sLogFileDate = $aMatches[1][0];
                    // $nRealmTime = strtotime($sLogFileDate)+(86400*$nCompressTime);
                    $nRealmTime = strtotime($sLogFileDate);

                }
                $nFilemTime = $nFilemTime<$nRealmTime ? $nFilemTime : $nRealmTime;

                if ($nCurrentTime-(86400*$nActiveDay) < $nFilemTime) {//x天内日志不做处理
                  // if($bDebug)echo "FRESH FILE: ".date("Ymd", filectime($sFilePath))." \t ".$sFilePath."\n";

                  # code...
                }elseif ($nCurrentTime-(86400*$nActiveDay*$nCompressTime) < $nFilemTime) {// 3x 天内日志压缩 && 归档

                  if (!in_array($sFilePath, $aBackupFiles)) { // 非备份目录
                    if($bDebug)echo date("Ymd", filectime($sFilePath))." \t ".$sFilePath."\n";

                    if ($bZipFileFlag) {//已经被压缩过则不再压缩
                      if($bDebug)echo "NOT COMPRESS\t\t".$sFilePath."\n";
                      
                    }else{
                      if($bDebug)echo "COMPRESS\t\t".$sFilePath."\n";
                      $this->doCompress($sFilePath);
                    }
                    
                    $this->moveFile2DateDir($dir, $sLogFileDate, $sFilePath);
                  }
                }else{// 3x天之外的日志删除, 
                  if($bDebug)echo "DELETE \t\t".$sFilePath."\n";
                  // $this->rmFile($sFilePath);
                }

                // filemtime($sFilePath);
              }
            }
              closedir($dh);
          }

          // 每天的日志压缩成一个文件
          if ($dh = opendir($dir)) 
          {
            $aBackupFiles = array();
            while (($file = readdir($dh)) !== false)
            {

              if(preg_match("/^backup-20\d{6}$/i", $file))
              {
                if($bDebug)echo "COMPRESS\t\t".$dir."/".$file."\n";
                $this->doCompressFast($dir."/".$file);
              }
            }
            closedir($dh);
          }
        }
    }

    function rmFile($sPath)
    {
      exec("rm -fR ".$sPath);
    }
    function doCompress($sPath)
    {
      exec("bzip2 -9 -s ".$sPath." > /dev/null 2>&1");
    }
    function doCompressFast($sPath)
    {
      exec("cd $sPath && tar cvf ".$sPath.".tar  . > /dev/null 2>&1");
      if (file_exists($sPath.".tar")) {
        exec("rm -fR ".$sPath);
      }
    }
    function doDeCompress($sPath)
    {
      exec("bzip2 -d ".$sPath);
    }

}

