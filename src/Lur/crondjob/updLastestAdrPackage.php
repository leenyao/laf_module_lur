<?php
/*
将最新的ADR 包更新进版本文件  

**/
class updLastestAdrPackage extends \YcheukfCommon\Lib\Crondjob{



    function go(){


        $sDir = BASE_INDEX_PATH."/../data/adr_trolley_release/package/";


        /**
         * #######closure function start#######
         */
        $fGetLastestPackage = function($sDir=null) 
        {

            $sLastestFile = "";
            $aFiles = scandir($sDir, 1);
            arsort($aFiles);
            $bLastest = false;
            foreach ($aFiles as $sFile) {
                if (in_array($sFile, array('.', '..'))) {
                    continue;
                }
                if ($bLastest==false && preg_match("/^adr_trolley_release.*/i", $sFile)) {
                    $sLastestFile = $sFile;
                    $bLastest = true;
                }else{
                    exec('mv '.$sDir.$sFile.' '.BASE_INDEX_PATH."/../data/adr_trolley_release/package_history/");
                }
            }
            return $sLastestFile;
        };
        $sLastestFile = $fGetLastestPackage($sDir);
        $sCurrentLastestFile = BASE_INDEX_PATH."/../data/adr_trolley_release/adr-trolley.lastest.package";
        if (!file_exists($sCurrentLastestFile)) {
            exec('touch '.$sCurrentLastestFile);
        }
        $sDiskLastestVersion = file_get_contents($sCurrentLastestFile);

        // 版本文件有变动
        if ($sLastestFile != $sDiskLastestVersion) {
            // exec('rm -f '.$sCurrentLastestFile);
            file_put_contents($sCurrentLastestFile, $sLastestFile);
            // exec('ln -s '.$sDir.$sLastestFile.' '.$sCurrentLastestFile);
            echoMsg('['.__CLASS__.'] lastest adr-package:'.($sDir.$sLastestFile));


            \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_ADRRELEASING_320, 1, 3600);

            
        }


        // var_dump($sLastestFile);

        return true;
    }
}

