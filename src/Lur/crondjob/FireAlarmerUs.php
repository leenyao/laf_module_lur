<?php
/*
报警器 , 
检测 US心跳
检测 us脚本

**/
class FireAlarmerUs extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $aConfig = $this->oFrameworker->sm->get('config');

        $this->chkUsHeartbeats($aConfig);


        return true;
    }



    /**
     * 检查adr心跳
     * 
     */
    public function chkUsHeartbeats($aConfig)
    {

    	$sCacheKey = date("Ymd H");

    	//本小时若已报警过, 则不再检查
        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_USFIREALARM, $sCacheKey);
        if (count($aMd5KeyData)) {
            return true;
        }
        // echoMsg('['.__CLASS__.'] start checking alarm email');

        $bHeartBeatFlag = true;
        $sSql = "select * from b_usstats  order by id desc limit 1";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
        $aJsonData = array();
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
            	//若心跳在, 则不需要报警
            	if ((time() - strtotime($result['created_time']) < $aConfig['us_heartbeat_interaltime'])) {
                    $bHeartBeatFlag = false;
            	}
                $aJsonData = json_decode($result['data'], 1);
        	}
        }
        $sEmailTitle = "";
        if ($bHeartBeatFlag) {
            $sEmailTitle = LAF_LUREMAILTITLE_SOS."[us-no-heartbeat] ";
        }

        if (count($aJsonData)) {
            foreach ($aJsonData as $sRedisClient => $aTmp1) {
                $aTmp2 = array("ios", "android", "web");
                foreach ($aTmp2 as $sPlatfrom) {
                    if (isset($aTmp1[$sPlatfrom])
                        && isset($aTmp1[$sPlatfrom]['mqhead'])
                        && count($aTmp1[$sPlatfrom]['mqhead'])
                        && isset($aTmp1[$sPlatfrom]['mqhead']['t'])
                    ) {
                        if (intval(time() - $aTmp1[$sPlatfrom]['mqhead']['t']) > 96000) {
                            $sEmailTitle = LAF_LUREMAILTITLE_SOS."[us-gencookiescript-stop] ";
                        }
                    }
                }
            }
        }


        if (!empty($sEmailTitle)) {
            $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
            \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailTitle);
            echoMsg('['.__CLASS__.'] send an alarm email :'.$sEmailTitle. " to ".json_encode($aEmailTos));


            // $this->oFrameworker->sm->get('Lur\Service\Common')->smsSos($sEmailTitle);
            // echoMsg('['.__CLASS__.'] send SMS ');

            
            \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_USFIREALARM, $sCacheKey, $sEmailTitle);

        }

    }

}

