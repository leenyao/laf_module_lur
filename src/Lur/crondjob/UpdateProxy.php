<?php
/*
清理过期的排期设置  

**/
defined('LC_PROXY_EMPTYIPCITS_835') || define('LC_PROXY_EMPTYIPCITS_835', "LC_835/");
defined('LC_PROXY_CITYLIST_838') || define('LC_PROXY_CITYLIST_838', "LC_838/");
defined('LC_PROXY_FORBIDDENACC_839') || define('LC_PROXY_FORBIDDENACC_839', "LC_839/");


defined('LC_PROXY_MONITORCODE_FALSE') || define('LC_PROXY_MONITORCODE_FALSE', 0);
defined('LC_PROXY_MONITORCODE_NEEDUPDATE') || define('LC_PROXY_MONITORCODE_NEEDUPDATE', 1);
defined('LC_PROXY_MONITORCODE_EXPIRED') || define('LC_PROXY_MONITORCODE_EXPIRED', 2);


class UpdateProxy extends \YcheukfCommon\Lib\Crondjob{

    var $aCityId2Label=[];
    var $keepCities=[];


    function isBatchUpdateAble(){
        $bLock = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LC_PROXY_BATCHUPDATE_LOCK_834);
        return $bLock ? 0:1;
    }   
    function freshBatchUpdateAble(){
        \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LC_PROXY_BATCHUPDATE_LOCK_834, 1, 3600);
    }
    function getUnbandAdrid(){
        $all = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getAliveAdrTrolleyVpnList2();

        $used = array_keys($this->oFrameworker->sm->get('\Lur\Service\Common')->getAdr2VpnAcc());

        $notuse = (array_diff($all, $used));
        $notuse2 = \YcheukfCommon\Lib\Functions::shuffle_assoc($notuse);

        print_r($notuse2);
        var_dump(count($all));
        var_dump(count($used));
        var_dump(count($notuse2));
    }

    function manaulband(){
        // LY-111/347
        $config = $this->manaulconfig();
        foreach($config as $row){
            $this->bandingAccount2Adr($row['adrid'], $row['acckey']);
            $this->bandCity($row['adrid']);

        }
    }

    function test(){
        // $plat = 'uuhttp';
        // $this->fmtPlatGeo($plat);


        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $aCities = $this->oFrameworker->sm->get('\Lur\Service\Location')->getDbCities();
        $plat = "zdopen";
        $platcache = LC_PROXY_PLATCITY_832.$plat;
        $adapter = $this->getAdapter($plat);
        var_dump($aCities);
        $aCities = $adapter::initLocation($platcache, $aCities, $redis);


        return true;

        // 绑定测试的机器和账号
        // $this->manaulband();
        $accounts = $this->getPlatAccounts();
        print_r($accounts);

        return true;
        $plat = 'songzi';
        $i = 0;
        while($i++< 10){
            $city = $this->get_next_lackingcity($plat);

        }

        // $adrs = [1004,1030,1031,1033,1038,1043,1052,1053,1060,1068,1079,1080,1083,1087,111,114,119,122,125,126,131,138,139,169,178,179,180,181,184,186,191,192,194,198,199,207,238,240,244,254,256,271,272,300,305,311,341,347,372,377,379,397,40,404,411,412,416,419,42,424,428,443,447,448,450,451,474,48,482,484,487,489,496,497,500,501,509,512,516,519,520,526,528,530,533,536,538,541,542,547,551,559,564,569,576,586,587,591,595,608,617,626,638,642,645,650,669,670,685,690,692,696,794,798,801,802,807,815,816,835,838,854,871,874,877,905,907,914,927,928,932,935,942,949,950,953,979,989,991,992];
        // asort($adrs);
        // print(join(",", $adrs));
        // $this->getUnbandAdrid();

        // $aAliveNotUsedVpnMachine = 

        return true;

        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);

    }


    public function getUsingPlats()
    {
        return ["jiguang", "vps91", "zhima",  "zdopen", "songzi", "uuhttp", "jinglin"];
    }

    function go(){
        // $adrnotuse = $this->get_adrnotuse();
        $bDebug = $this->isDebug();
        // $this->test();
        // return true;

        // 更新空ip的城市列表, 动量
        $this->updEmptyipcity();



        // $plat = "jiguang";
        // $date = date("Ymd");
        // $cities = $this->getEmptyipcity($plat, $date);
        // print_r($cities);
        // }

        if ($this->isBatchUpdateAble()==0 && $bDebug==0) {//被锁定, 未到执行时间
            return true;
        }else{//批量


            $plats = $this->getUsingPlats();
            foreach($plats as $plat){
                // 将供应商的地域列表存起来
                $this->fmtPlatGeo($plat);
                // 将所有的常驻地域初始化
                $this->addkeepcity2list($plat);

            }

            // $this->fmtPlatGeo("CUSTOM0730");

            // 根据供应商获取地域列表
            // $citys = $this->getPlatGeo();
            // print_r($citys);

            // 解绑所有 adr2acc
            $this->clearBandADR2ACC();

            // 绑定测试的机器和账号
            $this->manaulband();


            // 绑定的adr与账号
            $this->bandAdr2Acc();

            // 绑定的adr与城市
            $this->bandCity();

            // 绑定的debug adr与城市
            $this->bandManualCity();

            // 刷新批量更新锁
            $this->freshBatchUpdateAble();

        }




        // 获取所有可用来做proxy adr的列表
        // $proxyadrs = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getProxyAdrs();
        // print_r($proxyadrs);

        // 是否 proxy adr
        // $isProxyAdr = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->isProxyAdr(42);
        // var_dump($isProxyAdr);

        // 
        // $ProxyConfigs = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getBindedProxyConfig();
        // var_dump($ProxyConfigs);


        return true;
    }


    function get_lackingcities(){

        list($citiesnoband, $toplacking) = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->citiesnoband();

        cronjobMsg([__FUNCTION__, "citiesnoband", join(",", $citiesnoband)], __CLASS__);
        cronjobMsg([__FUNCTION__, "toplacking", join(",", $toplacking)], __CLASS__);


        $cities = $citiesnoband+$toplacking;

        $cities = [
            "宁波","青岛","汕头"

            ,"北京","上海","广州"

            ,"南京","合肥","厦门","苏州","武汉","杭州","大连","天津","济南"
            ,"苏州","武汉","大连"
            ,"舟山","温州","台州","嘉兴"
            ,"徐州", "郑州"
        ];
        return $cities;
    }

// 绑定测试账号
    // function bandDebug(){
    //     $adrid = $this->getDebugAdr();
    //     $acckey = 'uuhttp/5';
    //     $this->bandingAccount2Adr($adrid, $acckey);

    // }

    // 手动绑定一些机器
    function manaulconfig(){
        $config = [];
        // $config[] = [
        //     'adrid' => 311,
        //     'acckey' => 'uuhttp/11',
        //     'city' => ["北京", ["110000","110000"], 15],
        // ];
        // $config[] = [
        //     'adrid' => 347,
        //     'acckey' => 'uuhttp/12',
        //     'city' => ["北京", ["110000","110000"], 15],
        // ];
        // $config[] = [
        //     'adrid' => 272,
        //     'acckey' => 'songzi/10',
        //     'city' => ["北京", ["110000","110000"], 15],
        // ];
        // $config[] = [
        //     'adrid' => 1,
        //     'acckey' => 'jinglin/1000',
        //     'city' => ["青岛", ["青岛","青岛"], 21],
        // ];
        // $config[] = [
        //     'adrid' => 372,
        //     'acckey' => 'zhima/1',
        //     'city' => ["杭州", ["330000","330100"], 14],
        // ];
        // $config[] = [
        //     'adrid' => 377,
        //     'acckey' => 'zhima/2',
        //     'city' => ["杭州", ["330000","330100"], 14],
        // ];



        return $config;
    }



    function bandManualCity(){
        $config = $this->manaulconfig();
        foreach($config as $row){
            $this->bandCity($row['adrid']);

        }
    }

    // 绑定测试地域
    function bandManualCityInfo($adrid){
        $config = $this->manaulconfig();

        foreach($config as $row){
            if($row['adrid'] == $adrid){
                return $row['city'];
            }

        }
    }
    // 绑定测试地域
    function isManulAdr($adrid){
        $config = $this->manaulconfig();
        $adrids = [];
        foreach($config as $row){
            $adrids[] = $row['adrid'];

        }
        $adrids[] = $this->getDebugAdr();

        return in_array($adrid, $adrids)?1:0;
    }
    function getDebugAdr(){
        return 1;
    }

    function getNoBandingAdrs(){
        $adrs = [40,42,48,111,114,119,122,125,126,131,138,139,169,178,179,180,181,184,186,191,192,194,198,199,207,238,240,244,254,256,271,272,300,305,311,341,347,372,377,379,397,404,411,412,416,419,424,428,443,447,448,450,451,474,482,484,487,489,496,497,500,501,509,512,516,519,520,526,528,530,533,536,538,541,542,547,551,559,564,569,576,586,587,591,595,608,617,626,638,642,645,650,669,670,685,690,692,696,794,798,801,802,807,815,816,835,838,854,871,874,877,905,907,914,927,928,932,935,942,949,950,953,979,989,991,992,1004,1030,1031,1033,1038,1043,1052,1053,1060,1068,1079,1080,1083,1087];
        foreach($adrs as $adrid){
            $this->add2proxyadrs($adrid);
        }
        return $adrs;
    }
    // 获取平台的账号
    function getPlatAccounts(){
        $accounts = [];



        $replacements = [
            [
                '[pack]' => '39933','[count]' => 360,
            ],
            [
                '[pack]' => '40217','[count]' => 1440,
            ],
            [
                '[pack]' => '40141','[count]' => 360,
            ]
        ];
        $accounts['jiguang'] = [];
        for($i=0; $i<count($replacements); $i++){

            $accounts['jiguang'][] = [
                'id' => ($i+1)*1000,
                'replacement' => $replacements[$i],

            ];
        }



        $replacements = [
            [
                '[secert]'=>'MTU2MTg2MTYxMTM6MjAwODIwZTMyMjc4MTVlZDE3NTZhNmI1MzFlN2UwZDI=',
            ]
        ];
        $accounts['uuhttp'] = [];
        for($i=0; $i<count($replacements); $i++){

            $accounts['uuhttp'][] = [
                'id' => ($i+1)*1000,
                'replacement' => $replacements[$i],
            ];
        }


        $replacements = [
            [
                '[api]'=>'202108050104384336', '[akey]'=>'7216bcf0f19b653f', '[expired]' => '2021-09-05',
            ],
            [
                '[api]'=>'202108152022596799', '[akey]'=>'05f233f8c2b8e2e6', '[expired]' => '2022-08-15'
            ],
            [
                '[api]'=>'202108170001545675', '[akey]'=>'00ed5be9a8d0b4d0', '[expired]' => '2022-08-17'
            ],
            [
                '[api]'=>'202108220355442696', '[akey]'=>'f811494651cc5df6', '[expired]' => '2021-09-21'
            ]


        ];
        $accounts['zdopen'] = [];
        for($i=0; $i<count($replacements); $i++){
            $accounts['zdopen'][] = [
                'id' => ($i+1)*1000,
                'replacement' => $replacements[$i],
            ];
        }


        $accounts['zhima'] = [];
        $replacements = [
            [
                '[pack]'=>'172084',
            ]
        ];
        for($i=0; $i<count($replacements); $i++){

            $accounts['zhima'][] = [
                'id' => ($i+1)*1000,
                'replacement' => $replacements[$i],
            ];
        }


        $accounts['jinglin'] = [];
        $replacements = [
            [
                '[packid]'=>'1',
                '[appid]'=>'19354',
                '[appkey]'=>'c625d188764def36c36bff19d546b7e1',
            ]
        ];
        for($i=0; $i<count($replacements); $i++){

            $accounts['jinglin'][] = [
                'id' => ($i+1)*1000,
                'proxyacc' => 'dylangogo',
                'proxypsw' => 'qwe123',
                'replacement' => $replacements[$i],
            ];
        }


        // $accounts['xingsuyun'] = [];
        // $replacements = [
        //     [
        //         '[orderNumber]'=>'CN2021081915294828','[uid]'=>'1428253609507201025',
        //     ]
        // ];
        // for($i=0; $i<count($replacements); $i++){

        //     $accounts['xingsuyun'][] = [
        //         'id' => ($i+1)*1000,
        //         'replacement' => $replacements[$i],
        //     ];
        // }







        $accountsreturn = [];
        $forbiddenaccs = $this->getForbiddenacc();
        foreach($accounts as $plat => $acclist){
            foreach($acclist as $accrow){

                // 将每个账号扩展N倍
                $nTimes = $this->getAccMutiTimes($plat);
                for ($i=0; $i < $nTimes; $i++) { 
                    $newaccrow = $accrow;
                    $newaccrow['id'] += $i;
                    $acckey = $plat."/".$newaccrow['id'];

                    if (!in_array($acckey, $forbiddenaccs)) {
                        $accountsreturn[$acckey] = $newaccrow;
                    }
                }
            }
        }

        return $accountsreturn;

    }

    function getAdapter($type){
        $classname = ucfirst(strtolower($type));
        $file = "/app/module/Lur/src/Lur/Service/ProxyAdapter/".ucfirst($classname).".php";
        if (file_exists($file)) {
            include_once($file);
        }
        return $classname;

    }


    // 保存该地域到排除列表里
    function _updEmptyipcity($plat, $date, $adrid){
        // 每N个小时做一个缓存
        $hourkey = intval(date("H")/3);
        $cachekey = LC_PROXY_EMPTYIPCITS_835.$plat."/".$date."/".$hourkey;
        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);

        $row = $redis->hGet(LC_PROXYADR_ID2CONFIG_831, $adrid);
        $bandcity = json_decode($row, 1);
        if (is_array($bandcity) && count($bandcity) && isset($bandcity['clabel'])) {
            $redis->sAdd($cachekey, $bandcity['clabel']);
            $redis->setTimeout($cachekey, 86400*3);

            echoMsg('['.__CLASS__.'] '."_updEmptyipcity :".$bandcity['clabel']." at ".$cachekey);

        }
    }

    // 更新没有ip的地域
    function updEmptyipcity(){
        $date = date("Ymd");
        $plats = $this->oFrameworker->sm->get('Lur\Service\Proxyip')->getPlats($date);
        foreach($plats as $plat){

            $adr2code = $this->oFrameworker->sm->get('Lur\Service\Proxyip')->getCodesByPlat($plat, $date);

            $adapter = $this->getAdapter($plat);

            // if ($plat=='zdopen') {
            //     $adr2code[139] = [12009=>5000];
            // }
            if (count($adr2code)) {
                foreach($adr2code as $adrid=>$rowcode){
                    $monitorcode = $adapter::monitorcodes($rowcode);
                    switch($monitorcode){
                        // 更新地域
                        case LC_PROXY_MONITORCODE_NEEDUPDATE:
                            $this->_updEmptyipcity($plat, $date, $adrid);
                            $this->bandCity($adrid);
                        break;
                        case LC_PROXY_MONITORCODE_EXPIRED:
                            $this->add2Forbiddenacc($adrid);
                        break;

                        default:
                        case LC_PROXY_MONITORCODE_FALSE:
                        break;
                    }
                }
            }


        }
    }
    // 获取没有ip的地域
    function getEmptyipcity($plat, $date=null){

        if(is_null($date)) $date = date("Ymd");
        $cachekey = LC_PROXY_EMPTYIPCITS_835.$plat."/".$date."/".date("H");
        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        return $redis->sMembers($cachekey);
    }


    function addkeepcity2list($plat){
        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $cachelist = LC_PROXY_CITYLIST_838.$plat;

        // 优先绑定常驻城市
        $keepcites = $this->getPlatKeepCity($plat);

        if (count($keepcites)) {
            $redis->del($cachelist);
            foreach($keepcites as $city){
                $redis->rPush($cachelist, $city);
                $this->remPlatKeepCity($plat, $city);
                cronjobMsg([__FUNCTION__, $plat, $city], __CLASS__);
            }
        }
    }


    function get_next_lackingcity($plat){
        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);



        $cachelist = LC_PROXY_CITYLIST_838.$plat;
        if ($redis->lLen($cachelist) > 0) {
            $cities = $redis->lRange($cachelist, 0, -1);

        }else{
            // 绑定缺量城市
            $cities = $this->get_lackingcities();

            // 获取该平台在的空ip城市
            $emptyipcities = $this->getEmptyipcity($plat);
            // 获取该平台的指定拨号城市
            $bandcities = $this->getPlatBandCity($plat);

            foreach($cities as $index => $city){

                if (in_array($city, $emptyipcities)) { //曾经为空ip, 排除
                    unset($cities[$index]);
                }elseif (!empty($bandcities) && !in_array($city, $bandcities)) {
                    // 指定了城市, 且不在其中, 排除
                    unset($cities[$index]);

                }else{
                    $redis->rPush($cachelist, $city);

                }
            }
        }

        $city = $redis->lPop($cachelist);

        // if ($plat == 'songzi') {
            // var_dump("###", $cachelist, $city, $cities);
        // }

        return $city;
    }


    public function getCityRow($city='')
    {
        $resid = 0;
        if(empty($this->aCityId2Label)){
            $this->aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_LOCATION);

        }
        foreach($this->aCityId2Label as $id => $label){
            if ($label == $city) {
                $resid = $id;
                break;
            }
        }
        return [
            'c' => 1,
            'id' => $resid,
        ];
    }

    
    // 获取需要绑定的地域
    function _getBandLoction($plat, $adrid){

        // 测试机器手动指定
        if ($this->isManulAdr($adrid)) {
            return $this->bandManualCityInfo($adrid);
        }

        $i = 0;
        $locationconf = [];
        while($i++< 100){
            $city = $this->get_next_lackingcity($plat);

            // 当前用户已经绑定
            if($this->oFrameworker->sm->get('\Lur\Service\Addtional')->iscitband($plat, $city)){
                // var_dump($plat, $city);
                continue;
            }

            if (is_null($city))break;

            // unset($lackcitys[$city]);
            $cityrow = $this->getCityRow($city);

            $acconfig_s = $this->getPlatConfByCity($plat, $city);
            // var_dump($city, $acconfig_s);

            if($acconfig_s){
                $locationconf = json_decode($acconfig_s, 1);
                break;
            }
        }

        return [$city, $locationconf, $cityrow['id']];
    }


    function clearBandADR2ACC(){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $bandcache = LC_PROXY_BANDADR_833;
        $oCacheObject->del($bandcache);

        $bandcache = LC_PROXYADR_ID2CONFIG_831;
        $oCacheObject->del($bandcache);
        
    }

    // 绑定 账号/城市
    function bandCity($customadrid=null){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $bandcache = LC_PROXY_BANDADR_833;
        $adrid2acckey = $oCacheObject->hGetAll($bandcache);

        foreach($adrid2acckey as $adrid => $acckey){

            // debug 机器不要占用资源
            if (is_null($customadrid) && $this->isManulAdr($adrid)) {
                continue;
            }

            // 指定只更新某台机器
            if(!is_null($customadrid) && $customadrid!=$adrid)continue;

            list($plat, $id) = explode('/', $acckey);


            list($city, $locationconf, $cityresid) = $this->_getBandLoction($plat, $adrid);
            $acconf = $this->getPlatAccountById($plat, $id);

            $adapter = $this->getAdapter($plat);
            $data = $adapter::band($city, $acconf, $locationconf);


            $data['p'] = $plat;
            $data['clid'] = $cityresid;
            $data['accid'] = $acconf['id'];
            $data['unikey'] = isset($acconf['replacement']) ? md5(json_encode($acconf['replacement'])) : md5($acconf['id']) ;

            $data['clabel'] = $city;
            $data['pa'] = isset($acconf['proxyacc'])&&!empty($acconf['proxyacc']) ? $acconf['proxyacc'] : "" ;
            $data['pp'] = isset($acconf['proxypsw'])&&!empty($acconf['proxypsw']) ? $acconf['proxypsw'] : "" ;



            $oCacheObject->hSet(LC_PROXYADR_ID2CONFIG_831, $adrid, json_encode($data));
            $this->updadr2city($adrid, $cityresid);
            echoMsg('['.__CLASS__.'] '."bandcity :".$city.json_encode([$adrid,  $acckey, $locationconf, $acconf, $data]));

        }
    }
     public function add2Forbiddenacc($adrid){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $accconfig = $oCacheObject->hGet(LC_PROXYADR_ID2CONFIG_831, $adrid);

        $acc = json_decode($accconfig, 1);
        if (is_array($acc) && count($acc) && isset($acc['p'])) {
            $accstring = $acc['p']."/".$acc['accid'];
            $cacheKey = LC_PROXY_FORBIDDENACC_839.date("Ymd");
            $oCacheObject->sAdd($cacheKey, $accstring);
            cronjobMsg([__FUNCTION__, $adrid, $cacheKey, $accstring], __CLASS__);
        }


    }
     public function getForbiddenacc(){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);

        $cacheKey = LC_PROXY_FORBIDDENACC_839.date("Ymd");
        $data = $oCacheObject->sMembers($cacheKey);
        return $data;
    }


    public function updadr2city($adrid, $cityid)
    {
        $sCacheKey = LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306.$adrid;
        $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->set($sCacheKey, $cityid);
    }

    // 绑定机器/账号
    function bandAdr2Acc(){
        // 获取没有被绑定的账号
        $nobandaccs = $this->getNoBandingPlatAccounts();
        // 获取没有被绑定的机器
        $adrs = $this->getNoBandingAdrs();
        // var_dump($nobandaccs);
        // var_dump($adrs);

        foreach ($nobandaccs as $acckey => $row){
            if (count($adrs)){
                foreach ($adrs as $i=>$adrid){
                    $this->bandingAccount2Adr($adrid, $acckey);
                    unset($adrs[$i]);
                    break;
                }

            }
        }

    }

    // 获取还没有被绑定的账号
    function getNoBandingPlatAccounts(){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $bandcache = LC_PROXY_BANDADR_833;
        $bandlist = $oCacheObject->hGetAll($bandcache);
        unset($bandlist[$this->getDebugAdr()]);

        $accounts =  $this->getPlatAccounts();
        foreach($accounts as $acckey => $row){
            if (in_array($acckey, $bandlist)){
                unset($accounts[$acckey]);
            }
        }
        return $accounts;

    }
    function add2proxyadrs($adrid){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $oCacheObject->sAdd(LC_PROXYADRLIST_830, $adrid);
    }


    // 获取还没有被绑定的账号
    function bandingAccount2Adr($adrid, $acckey){
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $bandcache = LC_PROXY_BANDADR_833;
        $oCacheObject->hSet($bandcache, $adrid, $acckey);
    }

    // 获取平台的账号
    function getPlatAccountById($plat, $id){
        $accs = $this->getPlatAccounts();
        return $accs[$plat."/".$id];
    }


    function getPlatConfByCity($plat, $city){
        $platcache = LC_PROXY_PLATCITY_832.$plat;
        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        return $redis->hGet($platcache, $city);
    }

    // 获取供应商的地域
    function getPlatGeo($plat){
        $platcache = LC_PROXY_PLATCITY_832.$plat;
        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $a = $redis->hGetAll($platcache);
        return array_keys($a);
    }

    // 格式化供应商的地域
    function fmtPlatGeo($plat){
        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);

        $aCities = $this->oFrameworker->sm->get('\Lur\Service\Location')->getDbCities();
        // var_dump($aCities);
        $platcache = LC_PROXY_PLATCITY_832.$plat;

        $adapter = $this->getAdapter($plat);
        $redis->del($platcache);
        $adapter::initLocation($platcache, $aCities, $redis);
        echoMsg('['.__CLASS__.'] '."fmtPlatGeo :".$plat);
    }

// 获取一个账号被重复复制的次数
    function getAccMutiTimes($plat){
        $adapter = $this->getAdapter($plat);
        $return = 1;
        if (defined("$adapter::ACCGROUPSTEP")) {
            $return = $adapter::ACCGROUPSTEP;
        }
        return $return;
    }
    function getPlatBandCity($plat){
        $cities = $this->getPlatGeo($plat);
        // $adapter = $this->getAdapter($plat);
        // $cities = $adapter::cities();
        // echoMsg('['.__CLASS__.'] '.$plat." cities :".json_encode($cities));
        return $cities;
    }
    function getPlatKeepCity($plat){
        if (!isset($this->keepCities[$plat])) {
            $adapter = $this->getAdapter($plat);
            $this->keepCities[$plat] = $adapter::keepcities();
            // code...
        }
        // echoMsg('['.__CLASS__.'] '.$plat." cities :".json_encode($cities));
        return $this->keepCities[$plat];
    }
    function remPlatKeepCity($plat, $city){
        if (in_array($city, $this->keepCities[$plat])) {
            unset($this->keepCities[$plat][$city]);
        }
    }




}

