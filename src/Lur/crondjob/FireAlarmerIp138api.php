<?php
/*
IAC API 报警器  

**/
class FireAlarmerIp138api extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $aConfig = $this->oFrameworker->sm->get('config');

        $this->chkIacAPI($aConfig);


        return true;
    }


    /**
     * 检查IAC API
     */
    public function chkIacAPI($aConfig){
        $sIproxyContents = (file_get_contents($aConfig['ipproxy_url']));
        $strtmp=json_decode($sIproxyContents,true);
        $iacApi = "https://api.ip138.com/query/?ip=114.114.114.114&datatype=jsonp&token=09404676078860407d9d8cccef31d6e2";
            // var_dump($strtmp);

        //本小时若已报警过, 则不再检查
        $sCacheKey = date("Ymd H");
        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, 103, $sCacheKey);
        if (count($aMd5KeyData)) {
            return true;
        }

        $aUsedProxyIps = $result="";
        if (!is_null($strtmp)) {
            $a=$strtmp['internal'];
            for($i=0;$i<10;$i++){//尝试十次, 无效后报警
                $rand=rand(0,10);

                $sProxyIp= ($i==0) ? "" : $a[$rand]['ip'].":".$a[$rand]['port'];
                $aUsedProxyIps .= "/".(empty($sProxyIp)?"本机":$sProxyIp);
                $result= \YcheukfCommon\Lib\Functions::curl_core_func($iacApi, $sProxyIp);
                $aTmp = json_decode($result, 1);
                if (!is_null($aTmp) && isset($aTmp['ret']) && $aTmp['ret']=='ok') {//正常
                    return true;                    
                }
            }

        }


        $sEmailTitle = LAF_LUREMAILTITLE_SOS."[ip138-api-not-work] ";
        $sEmailContent = <<<OUTPUT

        api_url: {$iacApi} <hr/>
        api_content: {$result} <hr/>
        api_usedproxyip: {$aUsedProxyIps} <hr/>
        ipproxy_url: {$aConfig['ipproxy_url']} <hr/>
        ipproxy_content: {$sIproxyContents} <hr/>
OUTPUT;

        $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
        \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailContent);
        echoMsg('['.__CLASS__.'] send an alarm email: '.$sEmailTitle);

        \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, 103, $sCacheKey, $sEmailTitle);

    }


}

