<?php
/*
清理过期的排期设置  

**/
class UpdateCache extends \YcheukfCommon\Lib\Crondjob{



    public function _updShowClickRelation()
    {


        $aCacheData = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_SHOWCLICK_RELATION_126);
        if (!$aCacheData) {
            // 更新 show-click 对应关系
            $oPdo = \Application\Model\Common::getPDOObejct($this->oFrameworker->sm, "db_slave"); //db_master|db_slave
            $sSql = "select id, cookie_code_id from b_adschedule where m102_id=1 and cookie_code_id<>0";
            
            $oSth = $oPdo->prepare($sSql);
            $oSth->execute(array(
            ));
            $aResultTmp = $oSth->fetchAll(\PDO::FETCH_ASSOC);
            // print_r();

            $aCounterRanking = array();
            foreach ($aResultTmp as $aRowTmp) {
                $aCounterRanking[$aRowTmp['cookie_code_id']] = $aRowTmp['id'];
            }

            \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_SHOWCLICK_RELATION_126, $aCounterRanking, 1800);
            echoMsg('['.__CLASS__.'] '."update show-click relation:".count($aCounterRanking));
        }


    }

    // 更新下游api
    public function _updDownPlan()
    {

        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $keys = [LAF_CACHE_DISTRICBUTIONAPI_LIST_815.'sohu'];
        $difftime = 86400*3;

        foreach($keys as $key){
            $lists = $redis->hGetAll($key);
            foreach($lists as $plan => $s){
                $detail = json_decode($s, 1);

                if (isset($detail['code']) && isset($detail['applytime']) && $detail['code']!=0){
                    if (time()-strtotime($detail['applytime']) > $difftime){
                        $redis->hDel($key, $plan);
                        echoMsg('['.__CLASS__.'] '."remove :".$plan);
                    }
                }
            }
        }
        
    }

    
    
    // 根据芒果的排期调整, 更新缓存
    public function _updApiCacheByMgApi()
    {
        
        $updtime = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_MGAPI_RESTART_144);

        if (is_numeric($updtime) && (time()-$updtime)>180) {

            \YcheukfCommon\Lib\Functions::delCache($this->oFrameworker->sm, LAF_CACHE_MGAPI_RESTART_144);
            $this->oFrameworker->sm->get('\Lur\Service\Common')->updateScheduleCache();
        }
        
    }



    // 更新省份id到城市id的关系
    public function _updProvinceid2cityid()
    {
        
        $redis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);

        if ($redis->ttl(LAF_CACHE_PROVINCEID2CITYID_148) < 3600) {

            $return = [];
            $province2cities = $this->oFrameworker->sm->get('\Lur\Service\Common')->province2cities();
            $iacProvince = $this->oFrameworker->sm->get('\Lur\Service\Common')->getIacProvince();
            foreach($iacProvince as $row){
                $matched = 0;
                foreach($province2cities as $plabel => $row2){
                    if (preg_match('/^'.$row['label'].'/', $plabel)){
                        if (!isset($return[$row['mapresid']]))$return[$row['mapresid']] = [];
                        $matched = 1;

                        $return[$row['mapresid']] = $row2;
                    }else{
                    }
                }
                if($matched ==0 ){
                    // print_r($row);

                }

            }
            // print_r($province2cities);
            // print_r($iacProvince);
            // print_r($return);
            // var_dump(count($return));
            // var_dump(count($iacProvince));
        
            $redis->set(LAF_CACHE_PROVINCEID2CITYID_148, json_encode($return));
            $redis->setTimeout(LAF_CACHE_PROVINCEID2CITYID_148,12*3600);
        }
        
    }


    // 将truckADRid更新进缓存
    public function _updTruckIds()
    {
        
        $aAdrTruck = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009);
        \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_ONLINE_ADRTRUCKS_316, $aAdrTruck, 100*1800);

        
    }


    // 更新 uvpool 所使用的vpn账号
    public function _updUvVpnAccs()
    {
        $oCacheObject = ($this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis());


        $aAllVpnCities = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAllVpnCities();

        if ($oCacheObject->ttl(LAF_CACHE_UVPOOL_VPNCITY_324) > 3600*10) {
            return;
        }

        $oCacheObject->set(LAF_CACHE_UVPOOL_VPNCITY_324, json_encode($aAllVpnCities));
        $oCacheObject->setTimeout(LAF_CACHE_UVPOOL_VPNCITY_324,48*3600);


        if ($oCacheObject->ttl(LAF_CACHE_UVPOOL_VPNACC_323) > 3600*10) {
            return;
        }
        

        $vpnaccs = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAllVpnAccs('uv');

        $data = [];
        foreach ($vpnaccs as $id => $row) {
            if (!isset($data[$row['vpnstore']])) {
                $data[$row['vpnstore']] = [];
            }
            $data[$row['vpnstore']][$id] = $row;
        }
        $oCacheObject->set(LAF_CACHE_UVPOOL_VPNACC_323, json_encode($data));
        $oCacheObject->setTimeout(LAF_CACHE_UVPOOL_VPNACC_323,48*3600);
    }
    function go(){


        $this->_updDownPlan();
        $this->_updTruckIds();
        $this->_updUvVpnAccs();
        $this->_updShowClickRelation();
        $this->_updProvinceid2cityid();


        $aCacheKeys = array(
            LAF_CACHE_IACCITY2IP, 
            LAF_CACHE_PROXYCOMMONCONFIG
        );
        foreach ($aCacheKeys as $sCacheKey) {
            $aCommonConfig = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, $sCacheKey);
            if (is_null($aCommonConfig) || $aCommonConfig===false) {
            // if (true) {//debug
                $this->oFrameworker->sm->get('\Lur\Service\Common')->updateCache($sCacheKey);
                echoMsg('['.__CLASS__.'] '."update cache:".$sCacheKey);

            }

            // \YcheukfCommon\Lib\Functions::sendMQ($this->oFrameworker->sm, date("Y-m-d"), "adschedule_update");
            // $sLastUdateTime = \YcheukfCommon\Lib\Functions::getMQ($this->oFrameworker->sm, "adschedule_update");
            // var_dump($sLastUdateTime);
            

            // \YcheukfCommon\Lib\Functions::saveCache($this->getServiceLocator(), "lur_adschedule_lastupdate_time", date("Y-m-d H:i:s"), null);
            // \YcheukfCommon\Lib\Functions::delCache($this->getServiceLocator(), 'restapi_adrconfig', true);

        }

        // 更新adr summary
        // $this->oFrameworker->sm->get('\Lur\Service\Common')->getAdrSummary(true);

        return true;
    }

}

