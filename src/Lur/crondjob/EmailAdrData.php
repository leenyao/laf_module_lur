<?php
/*
清理过期的排期设置  

**/
class EmailAdrData extends \YcheukfCommon\Lib\Crondjob{



    function go(){
        $aConfig = $this->oFrameworker->sm->get('config');
        $sToday = date("Y-m-d");
        $sYestoday = date("Y-m-d", strtotime("-1 day"));
        $aUrlids = array();

        $sSql = "select distinct user_id, id, out_json from system_batch_jobs where type=1 and m158_id in (10)  order by id desc";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
// echo BASE_INDEX_PATH;
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
                if (empty($result['out_json']) || is_null($result['out_json'])) {
                    continue;
                }
                $aJson2 = json_decode($result['out_json'], 1);

                if(!is_null($aJson2)){
                    foreach ($aJson2 ['show'] as $k1 => $aTmp2) {
                        if (isset($aTmp2['id'])) {
                            $aUrlids[] = $aTmp2['id'];
                        }
                    }
                    $sSql = "select * from b_report_e where de1 in (".implode(",", $aUrlids).") and date='".$sYestoday."' ORDER BY FIELD(de1,".implode(",", $aUrlids).")";

			        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
			// echo BASE_INDEX_PATH;
			        if($oPDOStatement && $oPDOStatement->rowCount()){
			            foreach($oPDOStatement as $result) {
			                var_dump($result);
			            }
			        }
                exit;

                }else{
                    echoMsg('['.__CLASS__.'] wrong json format '.$result['out_json']);
                }
            }
        }


        return true;
    }

    function _importTable($aConfig, $sUserId, $aUrlids, $sQueryToday){

        $aPostField = array(
            "uid" => strval($sUserId),
            "urlid" => strval(implode(",", $aUrlids)),
            "sdate" => strval($sQueryToday),
            "edate" => strval($sQueryToday),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            $aConfig['adr_report_url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     json_encode($aPostField)); 

        $result=curl_exec ($ch);

        if(!curl_errno($ch)){ 
            $info = curl_getinfo($ch); 

            if(in_array($info['http_code'], array(200))){
                $aJson = json_decode($result, 1);
                if (!is_null($aJson) || count($aJson) < 1) {
                    return $aJson;
                }else{
                    echoMsg('['.__CLASS__.'] wrong json format:'.$result);
                }
            }else{
                echoMsg('['.__CLASS__.'] wrong responce:'.json_encode($info));
            }
            curl_close($ch);  
        }else {
            echoMsg('['.__CLASS__.'] '.curl_error($ch));
            curl_close($ch);  
            return false;
        } 

    }

}

