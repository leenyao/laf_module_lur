<?php
include_once(__DIR__."/../../../../../bin/script/config_inc.php");

$aM1007 = \YcheukfCommon\Lib\Functions::getResourceMetadaList($oGlobalFramework->sm, 1007);
$aM1011 = \YcheukfCommon\Lib\Functions::getResourceMetadaList($oGlobalFramework->sm, 1011);
$aConfig = $oGlobalFramework->sm->get('config');


$sKey = intval(isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : "");

if(!isset($aM1011[$sKey])){
    echo "error proxykey:".$sKey."\n";
    exit;
}



$nIntervalTime = isset($aConfig['nProxyipIntervalTime']) ? $aConfig['nProxyipIntervalTime'] : 1;
// $nIntervalTime = 0;
$sProxyUrl = $aM1011[$sKey];
if (in_array($sKey,  $aConfig['leenyao_proxyip_mid'])) {
    $nIntervalTime = isset($aConfig['nProxyipIntervalTime']) ? $aConfig['nProxyipIntervalTime'] : 0.5;
}
$sCityEncodeFunc = 'urlencode';
// $nIntervalTime = 0.1;

$nIndex = 0;
while(1){

    foreach($aM1007 as $nCityId=>$sCity){
        // sleep(rand($nIntervalTime, $nIntervalTime+2));//api 频率过快， 随机等待
        if($nIndex !== 0)sleep($nIntervalTime);//api 频率过快， 随机等待

        if ($sKey == 6) {//该代理ip商需要用gbk编码
            $sCity = iconv('UTF-8','GB2312',  $sCity);
        }

        $sUrl = str_replace("[cityurlencode]", $sCityEncodeFunc($sCity), $sProxyUrl);
        $s = @file_get_contents($sUrl);
        if (!in_array($sKey,  $aConfig['leenyao_proxyip_mid'])) {
            $nRetry = 2;
            while($s==false || !(preg_match('/^\d+\.\d+\.\d+\.\d+:/i', $s))){
                sleep(rand($nIntervalTime, $nIntervalTime+2));//api 频率过快， 随机等待
                $s = @file_get_contents($sUrl);
                if($nRetry--<1)break;
            }
        }
        $sCacheKey = "iproxy_".$sKey."/".$nCityId;
        if(
            ($s!=false && preg_match('/^\d+\.\d+\.\d+\.\d+:/i', $s))
            && ($s!=false && !preg_match('/127\.0\.0\.1/i', $s))
        ){

            $s = \YcheukfCommon\Lib\Functions::br2nl($s);
            $aTmp = explode("\n", $s);
            $aTmp = array_unique($aTmp);
        	\YcheukfCommon\Lib\Functions::saveCache($oGlobalFramework->sm, $sCacheKey, join("\n", $aTmp));
            echo('['.date("Y-m-d H:i:s").']generate_ip:'. $sCacheKey."\n");
        }else{
            echo('['.date("Y-m-d H:i:s").']generate_ip error:'. $sCacheKey."/".$sUrl."/".$s."\n");
        }
        $nIndex++;
    }

}