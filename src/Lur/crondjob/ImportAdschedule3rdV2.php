<?php

/*
导入第三方排期, API模式

**/
include_once(__DIR__."/ImportAdschedule.php");

class ImportAdschedule3rdV2 extends \ImportAdschedule{

	function go(){
		$oRedis = $this->oFrameworker->sm->get('\Lur\Service\Common')->getThirdScheduleRedis();
		$aConfig = $this->oFrameworker->sm->get('config');

		//每个文件最多一次多少条记录
		$nCountPerFile = 0;
		$aData4Csv = array();
		while ($nCountPerFile < 100) {
			$aCsvData = $aImportResult = array();

			//检查对头是否为空, 为空则退出
			$s = $oRedis->lGet(LAF_CACHE_3RDSCHEDULE_MQ, 0);
			if ($s==false || is_null($s)) {
				break;
			}

			//
			$aJsonData = json_decode($s, 1);
			if (!is_null($aJsonData) && count($aJsonData)) {
				$nCusTomerIdTmp = intval($aJsonData['cid']);

				

				//demo数据不要处理, 出队
				if ((!isset($aConfig['debugFlag']) || $aConfig['debugFlag']!=1 ) && $nCusTomerIdTmp === intval(LAF_CACHE_3RDSCHEDULE_DEMOCUSTOMERID)) {
					$oRedis->lPop(LAF_CACHE_3RDSCHEDULE_MQ);
			        echoMsg('['.__CLASS__.'] ignore demo customerid :'.$nCusTomerIdTmp);
					continue;
				}
				if (isset($aJsonData['data'])) {
					/**
					 * 
* col0 userid
* col1 codelabel
* col2 show code
* col3 click code
* col4 servers
* col5 UA
* col6 cities
* col7 refferer
* col8 ip frequency
* col9 cookie frequency
* col10 show_activity
* col11 click_activity
* col12 stable cookie percent
* col13 sourcetype
* col14 apps
* col15 domaincookie_delete_percent
* col16 uv 1st code
* col17 uv 2nd code
* col18 uv1_activity
* col19 uv2 percent
					 */
					$aTmp = array();
					$aTmp[0] = $nCusTomerIdTmp;
					$aTmp[1] = isset($aJsonData['data']['showcode']['label']) ? $aJsonData['data']['showcode']['label'] : (isset($aJsonData['data']['clickcode']['label']) ? $aJsonData['data']['clickcode']['label'] : "");
					$aTmp[1] = iconv("UTF8", "GB2312", $aTmp[1]);
					$aTmp[2] = isset($aJsonData['data']['showcode']['url']) ? $aJsonData['data']['showcode']['url'] : "";
					$aTmp[3] = isset($aJsonData['data']['clickcode']['url']) ? $aJsonData['data']['clickcode']['url'] : "";
					$aTmp[4] = "";
					$aTmp[5] = json_encode(array($aJsonData['data']['uatype']));
					
					//地域转换

					/**
					 * #######closure function start#######
					 */
					$fClosureFunc = function($aCities) 
					{
						$aReturn = array();
						foreach ($aCities as $nCityCode) {
							$sql = "select mapresid from location_iaccode where code=".$nCityCode;
							$aLocationCode = $this->oFrameworker->queryPdo($sql)->fetch(\PDO::FETCH_ASSOC); 
							$aReturn[] = $aLocationCode["mapresid"];
						}
						return json_encode($aReturn);
					};
					$aTmp[6] = $fClosureFunc($aJsonData['data']['cities']);
					unset($fClosureFunc);


					$aTmp[7] = json_encode($aJsonData['data']['referrer']);
					$aTmp[8] = 100;
					$aTmp[9] = isset($aJsonData['data']['showcode']['cookefreq']) ? $aJsonData['data']['showcode']['cookefreq'] : 1;
					$aTmp[10] = isset($aJsonData['data']['showcode']['enable']) ? $aJsonData['data']['showcode']['enable'] : 1;
					$aTmp[11] = isset($aJsonData['data']['clickcode']['enable']) ? $aJsonData['data']['clickcode']['enable'] : 1;
					$aTmp[12] = $aJsonData['data']['stb_percent'];
					$aTmp[13] = $aJsonData['data']['adtype'];
					$aTmp[14] = json_encode(isset($aJsonData['data']['apps']) ? $aJsonData['data']['apps'] : array());
					$aTmp[15] = "";
					$aTmp[16] = "";
					$aTmp[17] = "";
					$aTmp[18] = "";
					$aTmp[19] = "";

					$aTmpHeader = array();
					for ($i=0; $i < count($aTmp); $i++) { 
						$aTmpHeader[] = "";
					}
					$aTmpHeader[] = $aTmp[] = "date-start-column";
					if (isset($aJsonData['data']['showcode']) && isset($aJsonData['data']['showcode']['data']) && count($aJsonData['data']['showcode']['data'])) {
						foreach ($aJsonData['data']['showcode']['data'] as $sScheduleTmp => $nCountTmp) {
							$aTmpHeader[] = $sScheduleTmp."|show";
							$aTmp[] = $nCountTmp;
						}
					}
					if (isset($aJsonData['data']['clickcode']) && isset($aJsonData['data']['clickcode']['data']) && count($aJsonData['data']['clickcode']['data'])) {
						foreach ($aJsonData['data']['clickcode']['data'] as $sScheduleTmp => $nCountTmp) {
							$aTmpHeader[] = $sScheduleTmp."|click";
							$aTmp[] = $nCountTmp;
						}
					}

					$aCsvData = array($aTmpHeader, $aTmp);
                    $aEntity = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'b_adcustomer', $nCusTomerIdTmp);
                    
                    $aImportResult = $this->doImport($aCsvData, $aEntity['user_id']);

                    // print_r($aJsonData['data']['showcode']['data']);
					$oRedis->lPop(LAF_CACHE_3RDSCHEDULE_MQ);

				}else{
			        echoMsg('['.__CLASS__.'] no data-key:data, ignore unavailable json :'.$s);

					$oRedis->lPop(LAF_CACHE_3RDSCHEDULE_MQ);
				}


			}else{
				$oRedis->lPop(LAF_CACHE_3RDSCHEDULE_MQ);
		        echoMsg('['.__CLASS__.'] ignore unavailable json :'.$s);
			}


			$sSql = "insert into b_schedule3rd_log set 
				atime='".(isset($aJsonData['atime'])?date("Y-m-d H:i:s", $aJsonData['atime']) : "")."'".
				", ptime='".date("Y-m-d H:i:s")."'".
				", cid=".$aJsonData['cid'].
				", p_in='".json_encode($aJsonData)."'".
				", p_in2='".json_encode($aCsvData)."'".
				", p_result='".json_encode($aImportResult)."'";
			$this->oFrameworker->queryPdo($sSql);

			$nCountPerFile++;
		}
		if (!empty($nCountPerFile)) {
	        echoMsg('['.__CLASS__.'] process records :'.$nCountPerFile);
	        $this->oFrameworker->sm->get('\Lur\Service\Common')->updateScheduleCache();
		}

		return true;
	}
}