<?php
/*
定期检查IAC与LUR的地域mapping情况  

**/
class MapIacLocation extends \YcheukfCommon\Lib\Crondjob{



    function go(){


        //first param
        $nParamIndex = 2;
        $bReloadCache = 0;
        // var_dump($_SERVER['argv']);
        if (isset($_SERVER['argv'][$nParamIndex])) {
            $bReloadCache = $_SERVER['argv'][$nParamIndex];
        }else{
            $bReloadCache = 0;
        }
        echoMsg('['.__CLASS__.'] bReloadCache: '.$bReloadCache);
        

        $aConfig = $this->oFrameworker->sm->get('config');


        $this->oFrameworker->sm->get('\Lur\Service\Common')->loadIacMapping($bReloadCache);

        if ($bReloadCache==1) {
            $oRedis3 = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis();
            $aKeys = $oRedis3->keys(LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306."*");

            if (count($aKeys)) {
                foreach ($aKeys as $sCacheTmp2) {
                    $oRedis3->del($sCacheTmp2);
                }
            }
        }

        // var_dump($this->oFrameworker->sm->get('\Lur\Service\Common')->getIacConfigByIp2("115.57.127.129"));

        $aLurCities = array();
        $aIacCities = array();
        $sSql = "select * from `system_metadata` where `type` = 1007 and status=1";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
                $aLurCities[$result['resid']] = $result['label'];
            }
        }


        $sSql = "select * from `location_iaccode` where `mapresid` <> '' ";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
                $aIacCities[] = $result['mapresid'];
            }
        }

        $aDiff = array_diff(array_keys($aLurCities), $aIacCities);
        // print_r($aDiff);

        if (count($aDiff)) {
            foreach ($aDiff as $nKeyIndex => $sIdTmp1) {
                // 吉林省特殊处理
                if($sIdTmp1 == 244){
                    $sSql = "update `location_iaccode` set mapresid=".$sIdTmp1." where `code` = '156021000000' ";
                    $this->oFrameworker->queryPdo($sSql);

                }


                $sCityLabel = $aLurCities[$sIdTmp1];
                // print_r($sCityLabel);

                $sSql = "select * from `location_iaccode` where `label` = '".$sCityLabel."' ";
                $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
                if($oPDOStatement && $oPDOStatement->rowCount() == 1){

                    $result = $oPDOStatement->fetch(PDO::FETCH_ASSOC);
                    $sSql = "update `location_iaccode` set mapresid=".$sIdTmp1." where `id` = '".$result['id']."' ";
                    $this->oFrameworker->queryPdo($sSql);

                    unset($aDiff[$nKeyIndex]);
                }elseif($oPDOStatement && $oPDOStatement->rowCount() < 1){

                    $sSql = "select * from `location_iaccode` where `label` = '".$sCityLabel."市' ";
                    $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
                    if($oPDOStatement && $oPDOStatement->rowCount() == 1){
                        $result = $oPDOStatement->fetch(PDO::FETCH_ASSOC);
                        $sSql = "update `location_iaccode` set mapresid=".$sIdTmp1." where `id` = '".$result['id']."' ";
                        $this->oFrameworker->queryPdo($sSql);
                        unset($aDiff[$nKeyIndex]);
                    }elseif($oPDOStatement && $oPDOStatement->rowCount() < 1){
                        if (preg_match("/省$/i", $sCityLabel) || preg_match("/市$/i", $sCityLabel)){
                            // var_dump($sCityLabel);
                            // var_dump(mb_strlen($sCityLabel,'utf-8'));
                            $sCityLabel = mb_substr($sCityLabel, 0, mb_strlen($sCityLabel,'utf-8')-1,"utf-8");
                            // var_dump($sCityLabel);

                            $sSql = "select * from `location_iaccode` where `label` = '".$sCityLabel."' ";
                            $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
                            if($oPDOStatement && $oPDOStatement->rowCount() == 1){
                                $result = $oPDOStatement->fetch(PDO::FETCH_ASSOC);
                                $sSql = "update `location_iaccode` set mapresid=".$sIdTmp1." where `id` = '".$result['id']."' ";
                                $this->oFrameworker->queryPdo($sSql);
                                unset($aDiff[$nKeyIndex]);
                            }
                        }
                    }
                }
            }
        }

        //无法匹配, 发送邮件
        if (count($aDiff)) {


            //今天若已报警过, 则不再检查
            $sCacheKey = date("Ymd");
            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_IACLURLOCATION_MAPTAG, $sCacheKey);
            if (count($aMd5KeyData)) {
                return true;
            }

            $sEmailTitle = LAF_LUREMAILTITLE_SOS."[iac-lur-locationmap-faild]";
            $sIds = join(",", $aDiff);
            $sEmailContent = <<<OUTPUT
            unmap ids: {$sIds} 
OUTPUT;
            echoMsg('['.__CLASS__.'] un mapped iacids: '.$sEmailContent);

            return true;

            $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);
            \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailContent);
            echoMsg('['.__CLASS__.'] send an alarm email: '.$sEmailTitle. " to ".join(",", $aEmailTos));


            $this->oFrameworker->sm->get('Lur\Service\Common')->smsSos($sEmailTitle);
            echoMsg('['.__CLASS__.'] send SMS ');
            

            \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_IACLURLOCATION_MAPTAG, $sCacheKey);

        }

        return true;
    }

}

