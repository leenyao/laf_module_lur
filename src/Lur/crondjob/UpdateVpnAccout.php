<?php
/*
根据缺量城市情况分配VPN账号  

**/
class UpdateVpnAccout extends \YcheukfCommon\Lib\Crondjob{

    var $aVpnHostsArrays;
    var $aLocationsArrays;

    public function getKeepCity2Count()
    {
        $aReturn = $aReturn2 = $aReturn3 = [];
        $aConfig = $this->oFrameworker->sm->get("config");
        foreach ($aConfig['keepVpnCity'] as $index => $row) {
            if($index > 10)break;
            $aReturn[$row['label']] = isset($row['count']) ? $row['count'] : 1;
            $aReturn2[$row['label']] = isset($row['selected_store']) ? $row['selected_store'] : [];
            $aReturn3[$row['label']] = isset($row['adrids']) ? $row['adrids'] : [];
        }
        return [$aReturn, $aReturn2, $aReturn3];
    }

    public function go()
    {
        $bDebug = true;
        $sToday = $bDebug?date("Ymd"):date("Ymd");
        $aConfig = $this->oFrameworker->sm->get("config");

        // if ($bDebug) {
        //     $aLackingCity2Count2 = $this->getKeepCity2Count();
        //     print_r($aLackingCity2Count2);
        //     exit;
        // }
        $bDebug = false;
        list($aCityId2Label, $aCityLabel2ResId) = $this->getLocationsArrays();
        // print_r($aCityId2Label);
        // print_r($aCityLabel2ResId);

        // $a = $this->getVpnHostsArrays();
        // $aaaa = print_r($a[1], 1);
        // file_put_contents("/tmp/feng2", $aaaa);

        $aData4Sql = $aVpnAccId2VpnConfig = array();
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $aForbbidenAdrids = $oReids->sMembers(LAF_CACHE_VPNFORBBIDENADRID_114);

        if ($aForbbidenAdrids && count($aForbbidenAdrids)) {

            $this->doForbbidenAdrid($sToday, $aForbbidenAdrids);
        }

        $bLock = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_UPDATEVPNACC_LOCK_111);
        if ($bLock && $bLock==1 && $bDebug==false) {//被锁定, 未到执行时间
            return true;
        }


        $oPdo = (\Application\Model\Common::getPDOObejct($this->oFrameworker->sm));
        // $sToday = $bDebug?"20170731":date("Ymd");


        if ($bDebug == true) {
            echo <<<OUTPUT
#################################            
            caution!!!! DEBUGING
            sToday=$sToday
#################################  

OUTPUT;
        }
        

        // 所有的vpn账号
        $aVpnAccountsStore2Acc = array();
        $aVpnAccounts = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAllVpnAccs();
        foreach ($aVpnAccounts as $aRow) {
            $aVpnAccounts[$aRow['id']] = $aRow;
            if (!isset($aVpnAccountsStore2Acc[$aRow['vpnstore']])) {
                $aVpnAccountsStore2Acc[$aRow['vpnstore']] = array();
            }
            $aVpnAccountsStore2Acc[$aRow['vpnstore']][$aRow['id']] = $aRow;

        }
        $nMaxVpnAccount = count($aVpnAccounts);
        // VPN账号数量上限
        $aVpnAccountsMaxCount = count($aVpnAccounts);



        // 使用中VPN账号
        $aUsingVpnAcc = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAdr2VpnAcc();
        $aUsingVpnAcc2AdrId = array_flip($aUsingVpnAcc);
        //所有拨号中的adrid
        $aUsingVpnAdrId = array_keys($aUsingVpnAcc);

        // 使用中VPN城市
        $aUsingVpnCity = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAdr2VpnHost();

        // 所有的城市
        list($aCityId2Label, $aCityLabel2ResId) = $this->getLocationsArrays();

        //缺量城市
        $aLackingCity2Count2 = $this->getLackingCityCount($sToday, $aVpnAccountsMaxCount*3);


        $aIgnoreVpnRelations = $this->oFrameworker->sm->get('\Lur\Service\Common')->getIgnoreVpnHostIds();
        if ($bDebug) {
            echoMsg('['.__CLASS__.'] ignore vphhostid'.json_encode($aIgnoreVpnRelations));
        }

        // 所有的vpn城市
        list($aVpnHosts, $aVpnHostsLable2Row, $aVpnHostsStore2Row, $aVpnHostsStoreCity2Row) = $this->getVpnHostsArrays();


        // 使用中的ADR [VPN城市][ADRID] => vpninfo
        // print_r($aUsingVpnAcc);
        $aUsingVpnCity2AdrId = array();
        foreach ($aUsingVpnCity as $nAdrId => $nVpnHostId) {
            if (isset($aVpnHosts[$nVpnHostId])) {
                $aUsingVpnCity2AdrId[$aVpnHosts[$nVpnHostId]['city']][$nAdrId] = array(
                    'nVpnHostId' => $nVpnHostId,
                    'nVpnAccountId' => isset($aUsingVpnAcc[$nAdrId]) ? $aUsingVpnAcc[$nAdrId] : "",
                );


                // [$nAdrId][$aVpnHosts[$nVpnHostId]['vpnstore']][$aVpnAccounts[$aUsingVpnAcc[$nAdrId]]['account']] = 1;
                // array(
                //     "host" => $aVpnHosts[$nVpnHostId],
                //     "account" => $aVpnAccounts[$aUsingVpnAcc[$nAdrId]],
                // );
            }
        }
            // var_dump($aLackingCity2Count, $aCityId2Label);

        // 所有活着的adr-trolley-vpn
        $aAliveNotUsedVpnMachine = $aAliveAdrVpnMachine = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getAliveAdrTrolleyVpnList2();




        // aUsingVpnAdrId

        // print_r($aAliveNotUsedVpnMachine);
        echoMsg('['.__CLASS__.']all adr-server: '.join(", ", $aAliveAdrVpnMachine));
        echoMsg('['.__CLASS__.']all aUsingVpnAdrId: '.join(", ", $aUsingVpnAdrId));
        

        // 若缺量特别严重则多分配一台机器, 取得 城市=>vpn账号数量关系

        list($aLackingCity2Count3,$aLackingCity2SelectedStore,$aLackingCity2AdrId) = $this->getKeepCity2Count();
        if (count($aLackingCity2Count2)) {
            $nAverage = array_sum($aLackingCity2Count2)/count($aLackingCity2Count2);
            foreach ($aLackingCity2Count2 as $sCityLable => $nCount) {
                if ($nCount<1000 && $bDebug==false) {//量太少不切换账号折腾 
                    continue;
                }
                //量大到一定程度部署两个机器
                $nVpnAccCount = $nCount>5000&&$nCount>=$nAverage*1.8 ? ($nCount>=30000?($nCount>=500000?4:3):2) : 1;
                if (!isset($aLackingCity2Count3[$sCityLable])) {
                    $aLackingCity2Count3[$sCityLable] = $nVpnAccCount;
                }
            }
        }


        // $aLackingCity2Count4 = array();
        // if (count($aLackingCity2Count3)) {
        //     foreach ($aLackingCity2Count3 as $sCityLable => $nVpnAccCount) {
        //         if (isset(var)) {
        //             # code...
        //         }
        //     }
        // }
        // $aCityIdsByCounterRanking = $this->oFrameworker->sm->get('\Lur\Service\Common')->getCityIdsByCounterRanking();
        // var_dump($aCityIdsByCounterRanking);
        // exit;

        if ($bDebug) {
            $aLackingCity2Count3 = [];
            $aLackingCity2Count3['广东省'] = 2;
            $aLackingCity2Count3['吉林省'] = 2;
            echoMsg('['.__CLASS__.']aLackingCity2Count2: '.print_r($aLackingCity2Count2, 1));
        }
        echoMsg('['.__CLASS__.']need 2 supply city: '.print_r($aLackingCity2Count3, 1));
        // 分配账号
        // 被使用的vpn账号
        $aUsedVpnAccoutIds = $aVpnAccounts;
        $aNotMacthedCities = array();//匹配不上的城市
        $sLackCityCacheKey = LAF_CACHE_LACKVPNACCCITIES_108.$sToday;
        $oAddtionalRedis = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis();
        // print_r($aVpnAccountsStore2Acc);

        if (count($aLackingCity2Count3)) {
            foreach ($aLackingCity2Count3 as $sCityLable => $nVpnAccCount) {
                for ($nIndexTmp4=0; $nIndexTmp4 < $nVpnAccCount; $nIndexTmp4++) { 

                    // 若之前已经有服务器配置过, 则继续使用
                    // 只允许全国混拨的机器使用历史, 完全随机
                    // if (false) {
                    if ( 
                        // '全国混拨' == $sCityLable
                        // && 
                        isset($aUsingVpnCity2AdrId[$sCityLable]) 
                        && count($aUsingVpnCity2AdrId[$sCityLable])
                    ) {

                        foreach ($aUsingVpnCity2AdrId[$sCityLable] as $nAdrIdTmp1 => $aRowTmp) {

                            //可能已经被加进屏蔽列表中|下线, 则不再使用, 并从记录中删除
                            if (!in_array($nAdrIdTmp1, $aAliveNotUsedVpnMachine) || !isset($aVpnHosts[$aRowTmp["nVpnHostId"]])) {
                                $this->oFrameworker->sm->get('\Lur\Service\Common')->delAdr2VpnAcc($nAdrIdTmp1);
                                $this->oFrameworker->sm->get('\Lur\Service\Common')->delAdr2VpnHost($nAdrIdTmp1);


                                // echoMsg('['.__CLASS__.'] forbidden city-vpnhost or offline adr:'.$sCityLable."-".$nAdrIdTmp1);

                                // need 2 supply city
                                continue;
                            }

                            if (isset($aVpnAccountsStore2Acc[$aVpnHosts[$aRowTmp["nVpnHostId"]]["vpnstore"]]) && isset($aVpnAccountsStore2Acc[$aVpnHosts[$aRowTmp["nVpnHostId"]]["vpnstore"]][$aRowTmp["nVpnAccountId"]])) {//库存中还有账号

                                //已经被屏蔽
                                if ($this->isIgnoreVpnRelation($aIgnoreVpnRelations, $aRowTmp["nVpnHostId"], $aRowTmp["nVpnAccountId"])) {
                                    continue;
                                }

                                $aVpnAccId2VpnConfig[$aRowTmp["nVpnAccountId"]] = array(
                                // $aData4Sql[$nAdrIdTmp1] =  array(
                                    "bAppendType" => "history",
                                    "sCityLable" => $sCityLable,
                                    "nVpnHostId" => $aRowTmp["nVpnHostId"],
                                    "nVpnAccountId" => $aRowTmp["nVpnAccountId"],
                                    "nIndex" => count($aVpnAccId2VpnConfig)+1,
                                    "bandAdrId" => $nAdrIdTmp1,
                                );
                                foreach ($aAliveNotUsedVpnMachine as $nKeyTmp => $nAdrIdTmp2) {
                                    if ($nAdrIdTmp2 == $nAdrIdTmp1) {
                                        unset($aAliveNotUsedVpnMachine[$nKeyTmp]);
                                    }
                                }
                                unset($aUsingVpnCity2AdrId[$sCityLable][$nAdrIdTmp1]);
                                unset($aVpnAccountsStore2Acc[$aVpnHosts[$aRowTmp["nVpnHostId"]]["vpnstore"]][$aRowTmp["nVpnAccountId"]]);

                                break;
                            }
                        }
                    }else{//到vpn库中查找
                        // aLackingCity2SelectedStore
                        if (isset($aVpnHostsLable2Row[$sCityLable])) {//找到
                            $oAddtionalRedis->hDel($sLackCityCacheKey, $sCityLable);

                            // 若存在指定的供应商拨号
                            if (isset($aLackingCity2SelectedStore[$sCityLable]) && count($aLackingCity2SelectedStore[$sCityLable])) {
                                foreach ($aLackingCity2SelectedStore[$sCityLable] as $sStoreTmp) {
                                    if (isset($aVpnAccountsStore2Acc[$sStoreTmp]) && count($aVpnAccountsStore2Acc[$sStoreTmp])) {//库存中还有账号

                                        if (isset($aVpnHostsStoreCity2Row[$sStoreTmp])
                                            && isset($aVpnHostsStoreCity2Row[$sStoreTmp][$sCityLable])
                                            && count($aVpnHostsStoreCity2Row[$sStoreTmp][$sCityLable])) {
                                            $aVpnHostTmp2 = current($aVpnHostsStoreCity2Row[$sStoreTmp][$sCityLable]);
                                            if ($sCityLable == '深圳') {
                                                // \YcheukfCommon\Lib\Functions::onlinedebug('sz1', [$aVpnHostTmp2['id'], $sCityLable]);
                                            }

                                            $bBandFlag = $this->bandVpn2Adr("new-custom", $aLackingCity2AdrId, $aVpnAccId2VpnConfig, $aUsedVpnAccoutIds, $aUsingVpnCity2AdrId, $aAliveNotUsedVpnMachine, $aVpnAccountsStore2Acc,$aIgnoreVpnRelations, $sStoreTmp, $aVpnHostTmp2['id'], $sCityLable);
                                            
                                            if ($bBandFlag == 1) {
                                                break;
                                            }
                                        }

                                    }
                                }
                            }

                            // 乱序
                            $aVpnHostsLable2Row[$sCityLable] = \YcheukfCommon\Lib\Functions::shuffle_assoc($aVpnHostsLable2Row[$sCityLable]);
                            foreach ($aVpnHostsLable2Row[$sCityLable] as $aRowTmp2) {
                                if (isset($aVpnAccountsStore2Acc[$aRowTmp2['vpnstore']]) && count($aVpnAccountsStore2Acc[$aRowTmp2['vpnstore']])) {//库存中还有账号
                                    // print_r($aVpnAccountsStore2Acc[$aRowTmp2['vpnstore']]);



                                    $bBandFlag = $this->bandVpn2Adr("new-radmon",$aLackingCity2AdrId, $aVpnAccId2VpnConfig, $aUsedVpnAccoutIds, $aUsingVpnCity2AdrId, $aAliveNotUsedVpnMachine, $aVpnAccountsStore2Acc,$aIgnoreVpnRelations, $aRowTmp2['vpnstore'], $aRowTmp2["id"], $sCityLable);
                                    if ($bBandFlag == 1) {
                                        break;
                                    }


                                }else{//库存用光
                                    if ($bDebug) {
                                        echoMsg('['.__CLASS__.'] lack vpnacc:'.$sCityLable);
                                    }
                                }
                            }

                            // print_r($aVpnHostsLable2Row[$sCityLable]);
                        }else{//找不到
                            echoMsg('['.__CLASS__.'] lacking city:'.$sCityLable);
                            $oAddtionalRedis->hSet($sLackCityCacheKey, $sCityLable, isset($aLackingCity2Count2[$sCityLable]) ? $aLackingCity2Count2[$sCityLable] : 9999);

                            if ($oAddtionalRedis->ttl($sLackCityCacheKey)<86400) {
                                $oAddtionalRedis->setTimeout($sLackCityCacheKey, 7*86400);
                            }
                        }
                    }
                }

                //所有账号使用完毕
                if (count($aVpnAccId2VpnConfig) >= $aVpnAccountsMaxCount) {
                    break;
                }
            }
        }

        // if ($bDebug) {
            // echoMsg('['.__CLASS__.']aVpnAccountsStore2Acc '.json_encode($aVpnAccountsStore2Acc).", total=".count($aVpnAccountsStore2Acc));
            echoMsg('['.__CLASS__.']aAliveNotUsedVpnMachine '.json_encode($aAliveNotUsedVpnMachine));
        // }
        /**
         * 尚有剩余的机器 && 尚有剩余的VPN账号
         * 进行给缺量城市随机分配
         */

        $aCityIdsByCounterRanking = $this->oFrameworker->sm->get('\Lur\Service\Common')->getCityIdsByCounterRanking();
        $aLackingCity2Count4 = $aLackingCity2Count3;
        $aLackingCity2Exists = array();

        if (count($aVpnAccountsStore2Acc)) {
            foreach ($aVpnAccountsStore2Acc as $vpnstore => $aRowTmp5) {
                if (count($aAliveNotUsedVpnMachine)< 1) {
                    break;
                }
                echoMsg('['.__CLASS__.']vpnstore=>account total: '.$vpnstore.'=>'.count($aRowTmp5));

                if (count($aRowTmp5)) {
                    foreach ($aRowTmp5 as $key3 => $aVpnAccountTmp) {


                        $bAppendType = "randmon";
                        $nAdrIdTmp5 = "";
                        $aVpnHostTmpId = "";
                        $aVpnAccTmpId = "";
                        $sCityLable ="";

                        /*
                        // 找出历史用过的关系
                        if (isset($aUsingVpnAcc2AdrId[$aVpnAccountTmp["id"]])) {
                            $nAdrIdTmp5 = ($aUsingVpnAcc2AdrId[$aVpnAccountTmp["id"]]);
                            $aVpnHostTmpId = $aUsingVpnCity[$nAdrIdTmp5];
                        }
                        // 若历史关系还在 && 不在屏蔽列表中, 继续沿用
                        if (!empty($nAdrIdTmp5) && isset($aVpnHosts[$aVpnHostTmpId])) {
                            $bAppendType = "r_history";
                            $sCityLable = $aVpnHosts[$aVpnHostTmpId]['city'];
                        }else{
                            $nAdrIdTmp5 = "";
                            $aVpnHostTmpId ="";
                        }

                        if(empty($nAdrIdTmp5)){//DO重新分配
                            $nAdrIdTmp5 = array_shift($aAliveNotUsedVpnMachine);
                        }
                        */

                        $nAdrIdTmp5 = array_shift($aAliveNotUsedVpnMachine);

                        $bBreakFlag = false;
                        $bWhileRetryTimes = 10;
                        while (1) {
                            if ($bWhileRetryTimes-- < 1)$bBreakFlag = true;

                            // 对缺量城市随机分配, 每个城市最多分配多一个账号
                            $bBreakFlag3 = false;
                            $aLackingCity2Count4 = \YcheukfCommon\Lib\Functions::shuffle_assoc($aLackingCity2Count4);

                            foreach ($aLackingCity2Count4 as $sCityLable3 => $nVpnAccCount2) {

                                if (isset($aVpnHostsStoreCity2Row[$vpnstore][$sCityLable3]) && !isset($aLackingCity2Exists[$sCityLable3])) {
                                    $bAppendType = "r_lack";

                                    $bBreakFlag3 = true;
                                    $aVpnHostTmp = current($aVpnHostsStoreCity2Row[$vpnstore][$sCityLable3]);
                                    $sCityLable = $sCityLable3;
                                    $aVpnHostTmpId = $aVpnHostTmp['id'];
                                    $aLackingCity2Exists[$sCityLable3] = 1;

                                    unset($aVpnHostsStoreCity2Row[$vpnstore][$sCityLable3]);
                                    // unset($aVpnHostsStore2Row[$vpnstore][$sCityLable3][$aVpnHostTmp['id']]);
                                    break;
                                }
                            }
                            if ($bBreakFlag3) {
                                break;
                            }

                            // 随机选择当天投放量最多的城市进行分配
                            if (is_array($aCityIdsByCounterRanking) &&  count($aCityIdsByCounterRanking)) {
                                $bBreakFlag3 = false;
                                $aCityIdsByCounterRanking = \YcheukfCommon\Lib\Functions::shuffle_assoc($aCityIdsByCounterRanking);
                                // var_dump($aCityIdsByCounterRanking);

                                    // var_dump($aCityIdsByCounterRanking);
                                if (count($aCityIdsByCounterRanking)) {
                                // echoMsg('['.__CLASS__.']aCityIdsByCounterRanking', print_r($aCityIdsByCounterRanking, 1));
                                // echoMsg('['.__CLASS__.']aCityIdsByCounterRanking json', json_encode($aCityIdsByCounterRanking));

                                    foreach ($aCityIdsByCounterRanking as $aRowCity1) {
                                        $sCityLable3 = $aRowCity1['label'];
                                        if (isset($aVpnHostsStoreCity2Row[$vpnstore][$sCityLable3])) {
                                            $bAppendType = "r_rank";

                                            $bBreakFlag3 = true;
                                            $aVpnHostTmp = current($aVpnHostsStoreCity2Row[$vpnstore][$sCityLable3]);
                                            $sCityLable = $sCityLable3;
                                            $aVpnHostTmpId = $aVpnHostTmp['id'];

                                            unset($aVpnHostsStoreCity2Row[$vpnstore][$sCityLable3]);
                                            // unset($aVpnHostsStore2Row[$vpnstore][$sCityLable3][$aVpnHostTmp['id']]);
                                            break;
                                        }
                                    }
                                }
                                if ($bBreakFlag3) {
                                    break;
                                }
                            }

                            // if ($vpnstore == 'vpn1_dongguan') {
                            //     var_dump("================");
                            //     var_dump($aVpnAccountTmp,$aAliveNotUsedVpnMachine);
                            //     var_dump($aVpnAccountTmp,$aAliveNotUsedVpnMachine);

                            // }
                            // 随机选择一个城市进行分配
                            if(empty($aVpnHostTmpId) && count($aAliveNotUsedVpnMachine)){
                                if (isset($aVpnHostsStore2Row[$aVpnAccountTmp['vpnstore']])) {
                                    $bAppendType = "randmon";
                                    $aTmp7 = \YcheukfCommon\Lib\Functions::shuffle_assoc($aVpnHostsStore2Row[$aVpnAccountTmp['vpnstore']]);
                                    $aVpnHostTmp = current($aTmp7);
                                    $aVpnHostTmpId = $aVpnHostTmp['id'];
                                    $sCityLable = $aVpnHostTmp['city'];
                                }else{
                                    if ($bDebug == true) {
                                        echoMsg('['.__CLASS__.']empty random vpnhost');
                                    }
                                }
                            }


                            // 是否在屏蔽列表中, 不在则通过
                            if (
                                $this->isIgnoreVpnRelation($aIgnoreVpnRelations, $aVpnHostTmpId, $aVpnAccountTmp['id']) == false
                            ){
                                $bBreakFlag = true;
                            }else{//在则重做
                                echoMsg('['.__CLASS__.']ignore forbidden '.$aVpnAccountTmp['id']."/".$aVpnHostTmpId);
                                $aVpnHostTmpId = "";
                            }
                            
                            if($bBreakFlag)break;
                        }


                        if (!empty($nAdrIdTmp5) && !empty($aVpnHostTmpId)) {

                            
                            $aVpnAccId2VpnConfig[$aVpnAccountTmp['id']] = array(
                                "bAppendType" => $bAppendType,
                                "sCityLable" => $sCityLable,
                                "nVpnHostId" => $aVpnHostTmpId,
                                "nVpnAccountId" => $aVpnAccountTmp['id'],
                                "nIndex" => count($aVpnAccId2VpnConfig)+1,
                                "bandAdrId" => $nAdrIdTmp5, 
                            );
                            unset($aVpnAccountsStore2Acc[$vpnstore][$aVpnAccountTmp['id']]);
                            if (count($aAliveNotUsedVpnMachine)) {
                                foreach ($aAliveNotUsedVpnMachine as $key2 => $value2) {
                                    if ($value2 == $nAdrIdTmp5) {
                                        unset($aAliveNotUsedVpnMachine[$key2]);
                                    }
                                }
                            }
                            continue;
                        }
                    }
                }
            }
        }



        foreach ($aVpnAccId2VpnConfig as $row2) {
            if (isset($row2['bandAdrId'])) {
                $aData4Sql[$row2['bandAdrId']] = $row2;
            }else{
                if (count($aAliveNotUsedVpnMachine)) {
                   $nNewAdrId2 = current($aAliveNotUsedVpnMachine);
                    $aData4Sql[$nNewAdrId2] = $row2;

                }
            }
        }



        // $oReids->sAdd(LAF_CACHE_VPNFORBBIDENADRID_114, $nServerId);

        if ($bDebug == true) {
            echoMsg('['.__CLASS__.']not use adr-server: '.json_encode($aAliveNotUsedVpnMachine));
            echoMsg('['.__CLASS__.']aData4Sql: '.json_encode($aData4Sql));
            // echoMsg('['.__CLASS__.']not use vpn '.print_r($aVpnAccountsStore2Acc, 1));

            // $sLogTmp = join("\t", array_values($aData4Sql))."\n";

            // var_dump(join("\t", array_keys(current(($aData4Sql)))));
            // exit;
            // print_r($aUsingVpnAcc);
            // print_r($aUsingVpnAcc2AdrId);
            // print_r($aUsingVpnCity);
            echo "count=>".count($aData4Sql)."\n";
            return true;
        }
            // print_r($aData4Sql);


        // 保存 ADRSERVERID与CITYID关系
        foreach ($aData4Sql as $nAdrIdTmp3 => $aRowTmp6) {
            $aRowTmp6['nCityResId'] = isset($aCityLabel2ResId[$aRowTmp6['sCityLable']]) ? $aCityLabel2ResId[$aRowTmp6['sCityLable']] : 0;
            $aData4Sql[$nAdrIdTmp3] = $aRowTmp6;


        }



        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);


        $aUsingVpnAcc4Del = $this->saveData($aUsingVpnAcc, $aData4Sql);


        // 清除历史旧绑定关系, 有多余的账号, 则保留继续拨号
        if (count($aUsingVpnAcc4Del)) {
            foreach ($aUsingVpnAcc4Del as $nAdrIdTmp4 => $nVpnAccountIdTmp2) {
                foreach ($aVpnAccountsStore2Acc as $vpnstore => $aRowTmp4) {
                    if (isset($aRowTmp4[$nVpnAccountIdTmp2])) {
                        unset($aUsingVpnAcc4Del[$nAdrIdTmp4]);
                    }
                }
            }
        }

        //清除历史绑定账号
        if (count($aUsingVpnAcc4Del)) {
            foreach ($aUsingVpnAcc4Del as $nAdrIdTmp4 => $nVpnAccountIdTmp2) {

                $this->oFrameworker->sm->get('\Lur\Service\Common')->delAdr2VpnAcc($nAdrIdTmp4);
                $this->oFrameworker->sm->get('\Lur\Service\Common')->delAdr2VpnHost($nAdrIdTmp4);
            }
        }

        
        \YcheukfCommon\Lib\Functions::delCache($this->oFrameworker->sm, LAF_CACHE_VPNACCPRE_107, true);
        foreach ($aAliveAdrVpnMachine as $nAdrIdTmp8) {
            $this->oFrameworker->sm->get('\Lur\Service\Common')->getVpnData($nAdrIdTmp8, 1);
        }

        echoMsg('['.__CLASS__.']remove binding '.json_encode($aUsingVpnAcc4Del));
        // print_r($aVpnAccountsStore2Acc);
        // print_r($aUsingVpnAcc4Del);


        $this->_echoResult1($aData4Sql);
        \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_UPDATEVPNACC_LOCK_111, 1, 1200);


        return true;
    
    }


    // aLackingCity2AdrId
    public function bandVpn2Adr($bAppendType="new", $aLackingCity2AdrId, &$aVpnAccId2VpnConfig, &$aUsedVpnAccoutIds, &$aUsingVpnCity2AdrId, &$aAliveNotUsedVpnMachine, &$aVpnAccountsStore2Acc,$aIgnoreVpnRelations, $sVpnStore, $sHostId, $sCityLable){

        if (count($aAliveNotUsedVpnMachine)) {// 还有尚未使用的adr机器, 进行绑定!!!

            // 指定的供应商中还有账号
            if (isset($aVpnAccountsStore2Acc[$sVpnStore]) && count($aVpnAccountsStore2Acc[$sVpnStore])) {
            }else{
                return -3;
            }


            $aKeysTmp = array_keys($aVpnAccountsStore2Acc[$sVpnStore]);

            $nVpnAccountIdTmp = $aVpnAccountsStore2Acc[$sVpnStore][$aKeysTmp[0]]['id'];

            //已经被屏蔽
            if ($this->isIgnoreVpnRelation($aIgnoreVpnRelations, $sHostId, $nVpnAccountIdTmp)) {
                return -2;
            }

            $nNewAdrId = null;

            // 存在需要绑定的adrid
            if (is_array($aLackingCity2AdrId) 
                && isset($aLackingCity2AdrId[$sCityLable])
                && count($aLackingCity2AdrId[$sCityLable])
            ) {
                foreach ($aLackingCity2AdrId[$sCityLable] as $adrid1) {
                    if (in_array($adrid1, $aAliveNotUsedVpnMachine)) {
                        $nNewAdrId = $adrid1;
                        break;
                    }
                }
            }
            if (is_null($nNewAdrId)) {
                $nNewAdrId = array_shift($aAliveNotUsedVpnMachine);
            }

            $aVpnAccId2VpnConfig[$nVpnAccountIdTmp] = array(
                "bAppendType" => $bAppendType,
                "sCityLable" => $sCityLable,
                "nVpnHostId" => $sHostId,
                "nVpnAccountId" => $nVpnAccountIdTmp,
                "nIndex" => count($aVpnAccId2VpnConfig)+1,
                "bandAdrId" => $nNewAdrId,
            );

            unset($aUsedVpnAccoutIds[$nVpnAccountIdTmp]);
            unset($aUsingVpnCity2AdrId[$sCityLable][$nVpnAccountIdTmp]);
            unset($aVpnAccountsStore2Acc[$sVpnStore][$nVpnAccountIdTmp]);
            foreach ($aAliveNotUsedVpnMachine as $key4 => $value4) {
                if ($value4 == $nNewAdrId) {
                    unset($aAliveNotUsedVpnMachine[$key4]);
                }
            }
            return 1;
        }else{

        }
        return 0;

    }


    public function getLackingCityCount($sToday, $nMaxCity=10)
    {
        
        list($aCityId2Label, $aCityLabel2ResId) = $this->getLocationsArrays();
        list($aVpnHosts, $aVpnHostsLable2Row, $aVpnHostsStore2Row, $aVpnHostsStoreCity2Row) = $this->getVpnHostsArrays();

        // print_r($aVpnHostsStoreCity2Row);
        
        $aLackingCity2Count = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getAllCityCounter($sToday);


        $bDebug = false;
        if($bDebug == true){
            // 广东,吉林
            $aLackingCity2Count = [];
            $aLackingCity2Count[1366] = 5000000;
            $aLackingCity2Count[244] = 5000000;
        }

        if (count($aLackingCity2Count)) {
            unset($aLackingCity2Count[LAF_CACHE_ADDTIONAL_ALLTICIES_9999]);
        }
        // echoMsg('['.__CLASS__.']aLackingCity2Count: '.print_r($aLackingCity2Count, 1));

        // 只给最缺量的几个城市分配VPN, 取得 城市=>缺量 关系
        $aLackingCity2Count2 = array();
        $nIndexTmp = 0;
        if (count($aLackingCity2Count)) {
            foreach ($aLackingCity2Count as $nCity => $nCount) {
                // echoMsg('['.__CLASS__.']nCity: '.$nCity);

                if ($nIndexTmp >= $nMaxCity) {
                    break;
                }
                if (!isset($aVpnHostsLable2Row[$aCityId2Label[$nCity]])) {
                    // echoMsg('['.__CLASS__.'] lacking vpn city:'.$aCityId2Label[$nCity]);
                    continue;
                }
                $sCityLable = $aCityId2Label[$nCity];
                $aLackingCity2Count2[$sCityLable] = $nCount;
                $nIndexTmp++;
            }
        }
        // echoMsg('['.__CLASS__.']aLackingCity2Count222: '.print_r($aLackingCity2Count2, 1));

        return $aLackingCity2Count2;
    }

    public function getLocationsArrays()
    {
        if (isset($this->aLocationsArrays) && count($this->aLocationsArrays)) {
            return $this->aLocationsArrays;
        }
        // 所有的城市
        $aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_LOCATION);
        $aCityLabel2ResId = array_flip($aCityId2Label);
        $this->aLocationsArrays = array($aCityId2Label, $aCityLabel2ResId);
        return $this->aLocationsArrays;
    }


    public function getVpnHostsArrays()
    {
        if (isset($this->aVpnHostsArrays) && count($this->aVpnHostsArrays)) {
            return $this->aVpnHostsArrays;
        }
        $oPdo = (\Application\Model\Common::getPDOObejct($this->oFrameworker->sm));

        // 所有的vpn城市
        $aVpnHosts = $aVpnHostsLable2Row = $aVpnHostsStore2Row = $aVpnHostsStoreCity2Row = array();
        $sSql = "select * from `vpn_hosts` where status=1";
        // var_dump($sSql);
        // return false;
        $sth = $oPdo->prepare($sSql);
        $sth->execute();
        $aTmp = $sth->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($aTmp as $aRow) {
            $sprovince = $aRow['province'];
            if(!preg_match("/省$/i", $sprovince))$sprovince = $sprovince."省";

            $aVpnHosts[$aRow['id']] = $aRow;
            $aVpnHostsLable2Row[$aRow['city']][$aRow['id']] = $aRow;
            $aVpnHostsLable2Row[$sprovince][$aRow['id']] = $aRow;

            $aVpnHostsStore2Row[$aRow['vpnstore']][$aRow['id']] = $aRow;
            $aVpnHostsStoreCity2Row[$aRow['vpnstore']][$aRow['city']][$aRow['id']] = $aRow;

            $aVpnHostsStoreCity2Row[$aRow['vpnstore']][$sprovince][$aRow['id']] = $aRow;
        }
        if (count($aVpnHosts)) {
            \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_VPNHOST_147, $aVpnHosts, 360*86400);

        }
        $this->aVpnHostsArrays = array($aVpnHosts, $aVpnHostsLable2Row, $aVpnHostsStore2Row, $aVpnHostsStoreCity2Row);
        return $this->aVpnHostsArrays;

    }


    public function saveData($aUsingVpnAcc, $aData4Sql)
    {
        
        list($aCityId2Label, $aCityLabel2ResId) = $this->getLocationsArrays();
        $oAddtionalRedis = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis();


        $aUsingVpnAcc4Del = $aUsingVpnAcc;
        foreach ($aData4Sql as $nAdrIdTmp3  => $aRowTmp3) {

            $aRowTmp3['nCityResId'] = isset($aCityLabel2ResId[$aRowTmp3['sCityLable']]) ? $aCityLabel2ResId[$aRowTmp3['sCityLable']] : LAF_CACHE_ADDTIONAL_ALLTICIES_9999;

            $oAddtionalRedis->set(LAF_CACHE_ADDTIONAL_ADRSERVER2CITYID_PRE_306.$nAdrIdTmp3, $aRowTmp3['nCityResId'], 3600);


            $this->oFrameworker->sm->get('\Lur\Service\Common')->delAdr2VpnAcc($nAdrIdTmp3);
            $this->oFrameworker->sm->get('\Lur\Service\Common')->delAdr2VpnHost($nAdrIdTmp3);



            // echoMsg('['.__CLASS__.']debugfeng '.LAF_METADATATYPE_ADRSERVER_VPNACC_1019."##".$nAdrIdTmp3."##".$aRowTmp3['nVpnAccountId']);


            $this->oFrameworker->sm->get('\Lur\Service\Common')->saveAdr2VpnAcc($nAdrIdTmp3, $aRowTmp3['nVpnAccountId']);
            $this->oFrameworker->sm->get('\Lur\Service\Common')->saveAdr2VpnHost($nAdrIdTmp3, $aRowTmp3['nVpnHostId']);

            unset($aUsingVpnAcc4Del[$nAdrIdTmp3]);
            if (in_array($aRowTmp3['nVpnAccountId'], $aUsingVpnAcc4Del)) {
                unset($aUsingVpnAcc4Del[$nAdrIdTmp3]);
            }

            // 删除adrid-vpnaccid 的关系
            \YcheukfCommon\Lib\Functions::delCache($this->oFrameworker->sm, LAF_CACHE_VPNACCPRE_107.$nAdrIdTmp3);
            $this->oFrameworker->sm->get('\Lur\Service\Common')->getVpnData($nAdrIdTmp3, 1);

        }
        return $aUsingVpnAcc4Del;
    }

    public function _echoResult1($aData4Sql)
    {


        list($aVpnHosts, $aVpnHostsLable2Row, $aVpnHostsStore2Row, $aVpnHostsStoreCity2Row) = $this->getVpnHostsArrays();
        $aVpnAccounts = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAllVpnAccs();

        $sLogTmp = "total:".count($aData4Sql)."\n";
        $init=0;
        foreach ($aData4Sql as $sKeyTmp1 => $aRow1) {
            if ($init==0) {
                // $sLogTmp .= "\nADRID\t".join("\t\t\t", array_keys($aRow1))."\n";
                $init=1;
            }
            $aRow4Print = [];
            $aRow4Print[] = "vpn_host=>"
                .$aRow1['sCityLable']
                ."/".$aRow1['nVpnHostId']
                ."/".$aVpnHosts[$aRow1['nVpnHostId']]['vpnstore']
                ."/".$aVpnHosts[$aRow1['nVpnHostId']]['host']
                ;
            $aRow4Print[] = "vpn_acc=>".$aRow1['nVpnAccountId']
                ."/".$aVpnAccounts[$aRow1['nVpnAccountId']]['vpnstore']
                ."/".$aVpnAccounts[$aRow1['nVpnAccountId']]['account']
                ."/".$aVpnAccounts[$aRow1['nVpnAccountId']]['password']
                ."/".$aVpnAccounts[$aRow1['nVpnAccountId']]['disabledate']
                ;
            $aRow4Print[] = "index=>".(isset($aRow1['nIndex'])?$aRow1['nIndex']:'');
            // $aRow4Print[] = "nCityResId=>".(isset($aRow1['nCityResId'])?$aRow1['nCityResId']:'');

            $sLogTmp .= "adrid=>".$sKeyTmp1.", ".$aRow1['bAppendType']."\t".join("\t\t", $aRow4Print)."\n";
        }
        // echoMsg('['.__CLASS__.']binding '.$sLogTmp);


    }

    /**
     * 单独处理失效的adrid
     */
    public function doForbbidenAdrid($sToday, $aForbbidenAdrids)
    {
        $aData4Sql = array();
        $oPdo = (\Application\Model\Common::getPDOObejct($this->oFrameworker->sm));
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);


        // 使用中VPN账号
        $aUsingVpnAcc = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAdr2VpnAcc();
        $aUsingVpnAcc2AdrId = array_flip($aUsingVpnAcc);
        //所有拨号中的adrid
        $aUsingVpnAdrId = array_keys($aUsingVpnAcc);

        // 使用中VPN城市
        $aUsingVpnCity = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAdr2VpnHost();

        // 所有的城市
        $aCityId2Label = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_LOCATION);
        $aCityLabel2ResId = array_flip($aCityId2Label);


        // 所有的vpn账号
        $aVpnAccountsStore2Acc = array();
        $aVpnAccounts = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAllVpnAccs();



        // 所有的vpn城市
        list($aVpnHosts, $aVpnHostsLable2Row, $aVpnHostsStore2Row, $aVpnHostsStoreCity2Row) = $this->getVpnHostsArrays();
        $aIgnoreVpnRelations = $this->oFrameworker->sm->get('\Lur\Service\Common')->getIgnoreVpnHostIds();

            // var_dump($aUsingVpnAcc);

        list($citiesnoband, $toplacking) = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->citiesnoband();
        $lackingcitys = $citiesnoband + $toplacking;
        foreach($lackingcitys as $citylabel){
            $aLackingCity2Count2[$citylabel] = 1;
        }
        // $aLackingCity2Count2 = $this->getLackingCityCount($sToday, 10);


        list($aCityId2Label, $aCityLabel2ResId) = $this->getLocationsArrays();

        foreach ($aForbbidenAdrids as $nAdrIdTmp7) {
            if (!isset($aUsingVpnAcc[$nAdrIdTmp7]) && !isset($aUsingVpnCity[$nAdrIdTmp7])) {
                $oReids->sRem(LAF_CACHE_VPNFORBBIDENADRID_114, $nAdrIdTmp7);

                continue;
            }
            $nAccoutId = $aUsingVpnAcc[$nAdrIdTmp7];
            $nHostId = $aUsingVpnCity[$nAdrIdTmp7];

            // 该账号已经失效
            if (!isset($aVpnAccounts[$nAccoutId])) {

                continue;
            }

            $sVpnstore = $aVpnAccounts[$nAccoutId]['vpnstore'];
            $sCityLable = $aVpnHosts[$nHostId]['city'];
            // var_dump($nAccoutId, $nHostId, $sVpnstore, $sCityLable);
            // 剔除已经在屏蔽列表中的host
            if (isset($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable][$nHostId])) {
                unset($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable][$nHostId]);
            }

            //已经被屏蔽
            if ($this->isIgnoreVpnRelation($aIgnoreVpnRelations, $nHostId, $nAccoutId)) {
                unset($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable][$nHostId]);
            }

            // 该城市仍能找到备用方案, 禁止, 因为导致反复循环
            // if(false){
            if (isset($aVpnHostsStoreCity2Row[$sVpnstore])
                && isset($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable])
                && count($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable])) {
                $aVpnHostTmp2 = current($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable]);
                $aData4Sql[$nAdrIdTmp7] = array(

                    "bAppendType" => "sf_stock",
                    "sCityLable" => $aVpnHostTmp2['city'],
                    "nVpnHostId" => $aVpnHostTmp2['id'],
                    "nVpnAccountId" => $nAccoutId,
                );

            }else{//随机分配另外的一个城市

                $bBreakFlag2 = false;
                $bWhileRetryTimes2 = 10;
                $aVpnHostTmp2 = array();
                $bAppendTypeTmp = "sf_randomtop";
                // echoMsg('['.__CLASS__.']aLackingCity2Count2 '.print_r($aLackingCity2Count2, 1));

                // 优先分配给缺量城市
                while (1) {
                    if ($bWhileRetryTimes2-- < 1)$bBreakFlag2 = true;

                    
                    if($bBreakFlag2){
                        break;
                    }else{
                        // 随机取城市
                        // $aLackingCity2Count2 = \YcheukfCommon\Lib\Functions::shuffle_assoc($aLackingCity2Count2);
                        $sCityLable2 = key($aLackingCity2Count2);
                        unset($aLackingCity2Count2[$sCityLable2]);
                        // echoMsg('['.__CLASS__.']sCityLable2 '.$sCityLable2);


                        // 随机取host
                        if (isset($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable2])) {
                            $bBreakFlag2 = true;
                            $aTmp9 = \YcheukfCommon\Lib\Functions::shuffle_assoc($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable2]);

                            $aVpnHostTmp2 = current($aTmp9);
                        }
                    }
                }

                // 缺量城市分配失败, 则分给随机城市
                if (empty($aVpnHostTmp2) && isset($aVpnHostsStore2Row[$sVpnstore]) && count($aVpnHostsStore2Row[$sVpnstore])) {
                    $aTmp8 = \YcheukfCommon\Lib\Functions::shuffle_assoc($aVpnHostsStore2Row[$sVpnstore]);
                    $aVpnHostTmp2 = current($aTmp8);
                    $bAppendTypeTmp = "sf_random";

                }
                $aData4Sql[$nAdrIdTmp7] = array(

                    "bAppendType" => $bAppendTypeTmp,
                    "sCityLable" => $aVpnHostTmp2['city'],
                    "nVpnHostId" => $aVpnHostTmp2['id'],
                    "nVpnAccountId" => $nAccoutId,
                );
                unset($aVpnHostsStoreCity2Row[$sVpnstore][$sCityLable][$aVpnHostTmp2['id']]);

            }
            $oReids->sRem(LAF_CACHE_VPNFORBBIDENADRID_114, $nAdrIdTmp7);

        }

        $aUsingVpnAcc4Del = $this->saveData($aUsingVpnAcc, $aData4Sql);



        $this->_echoResult1($aData4Sql);


    }


    public function isIgnoreVpnRelation($aIgnoreVpnRelations, $nHostId, $nAccoutId)
    {
        $bReturn = false;
        if (is_array($aIgnoreVpnRelations) 
            && count($aIgnoreVpnRelations) 
            && count($aIgnoreVpnRelations['aRelation']) ) {
            if (isset($aIgnoreVpnRelations['aRelation'][$nAccoutId."/".$nHostId])) {
                $bReturn = true;
            }
        }
        return $bReturn;
    }
}

