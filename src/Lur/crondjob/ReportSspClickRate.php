<?php
/*
ssp 报表的定时点击率报告

**/
class ReportSspClickRate extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $aConfig = $this->oFrameworker->sm->get('config');

        $this->oFrameworker->sm->get('\Lur\Service\Common')->flushCacheCounter2Db();

        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);

        $sToday = date("Ymd");
        $sCurrentMinute = intval(date("i")/10)."0";
        $sCacheKey = LAF_CACHE_COUNTER_SSP_CLICKRATE_410.$sToday."/".LAF_ADSOURCETYPE_SSP_HUNAN;
        $sHashKey = date("H").":".$sCurrentMinute;
        // var_dump($sCacheKey);
        // var_dump($sHashKey);

        $aSspClickPercentData[LAF_ADSOURCETYPE_SSP_HUNAN] = $this->oFrameworker->sm->get('\Lur\Service\Report')->getSspDataByDate2($sToday, LAF_ADSOURCETYPE_SSP_HUNAN);

// print_r($aSspClickPercentData[LAF_ADSOURCETYPE_SSP_HUNAN]);
        // $this->chkUsHeartbeats($aConfig);


        $oReids->hSet($sCacheKey, $sHashKey, json_encode($aSspClickPercentData[LAF_ADSOURCETYPE_SSP_HUNAN]));

        // sCacheKey
        $oReids->setTimeout($sCacheKey, 86400*10);


        echoMsg('['.__CLASS__.'] clickrate: '.json_encode($aSspClickPercentData[LAF_ADSOURCETYPE_SSP_HUNAN]));

        // print_r($aClickRate);

        return true;
    }



}

