<?php
/*
报警器 

检测 ADR心跳
维护 ALIVE ADR SERVER

**/
class FireAlarmer extends \YcheukfCommon\Lib\Crondjob{



    function go(){


        $this->chkAdrHeartbeats();

        $this->chkAdrLogCounter();

        
        $this->checkAdrTruckActive();

        $this->checkRemoteIpApi();


        $this->checkVpnIpUpdate();



        $this->checkCronjob();

        $this->dailyReport();

        return true;
    }
    public function dailyReport(){

        if(date("H") == '09'){
            $sCacheKey = date("Ymd");
            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_DAYLIREPORT_116, $sCacheKey);
            if (!$aMd5KeyData) {
            // if (true) {
                $total= $machine= $vpn= $mg ="";
                $sYesterday1 = date('Ymd', strtotime('-1 day'));
                $aAdrSummary = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAdrSummary();


                $machine = count($aAdrSummary['aLiveAdrServerType2Id']['TROLLEY-VPN']) ."/". count($aAdrSummary['aAllAdrServer']['aServerType2Id']['TROLLEY-VPN']);
                $vpn = count($aAdrSummary['aUsingVpnAcc']) ."/". count($aAdrSummary['aAllVpnAccs']);


                foreach ($aAdrSummary['aAllAdrServer']['aServerType2Id']['TROLLEY-VPN'] as $nAdrServerIdTmp) {
                  $aAdrServerInfoTmp = $aAdrSummary['aAdrServerInfo'][$nAdrServerIdTmp];

                  $total += $aAdrServerInfoTmp['daycounter_yesterday'];
                }



                $hndata = $this->oFrameworker->sm->get('\Lur\Service\Report')->getSspHunanDataByDateCode($sYesterday1, $sYesterday1, 3);

                // print_r($hndata);
                $total = round($total/1000,0);
                $mg = round($hndata[0]['impcount']/1000,0);
                $this->sms2op($total, $machine, $vpn, $mg);

                \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_DAYLIREPORT_116, $sCacheKey, $sCacheKey);
                echoMsg('['.__CLASS__.']  '."$total, $machine, $vpn, $mg");
            }



        }
    }


    function sms2op($total, $machine, $vpn, $mg)
    {
        $success = 1;
        $return = [];
        $aConfig = $this->oFrameworker->sm->get('config');
        foreach ($aConfig['daybphone'] as $phone) {
            $return[$phone] = \YcheukfCommon\Lib\Functions::sendSMSByAliyun($this->oFrameworker->sm,$phone,'SMS_205396599', ['total'=>$total, 'machine'=>$machine, 'vpn'=>$vpn, 'mg'=>$mg]);
            if($return[$phone] && $return[$phone]->Code && $return[$phone]->Code=='OK'){

            }else{
                $success = 0;
            }
        }
        return $success;
    }


    public function checkCronjob(){
        if (intval(date("H")) >= 9) {

            // 每天定时执行vpn供应商的状态报告
            $sCacheKey = 'viewvpnstroestats';
            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_CRONDJOB_114, $sCacheKey);
            if (!$aMd5KeyData) {
                exec('  php /app/module/Lur/public/adrscript/batch/vipin_info.result.php '.date("Ymd").'_VPNIPINFO > /dev/null ', $ou);
                \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_CRONDJOB_114, $sCacheKey, $sCacheKey);
                echoMsg('['.__CLASS__.']  '.$sCacheKey);
            }

        }
    }


    // 检查 vpn ip 是否发生了变化
    public function checkVpnIpUpdate(){

        $sCacheKey = date("Y-W");
        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_VPNHOSTCHK_113, $sCacheKey);

// var_dump($aMd5KeyData);
        if (!$aMd5KeyData) {

            exec(' php /app/module/Lur/src/Lur/Script/importvpnacc/gensql.php ', $ou);

            $flag = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_VPNIPCHANGE_328);
            if ($flag) {
                $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adr-vpnhost-needupdate]";
                $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADMINEMAILS);
                \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailTitle);
                echoMsg('['.__CLASS__.'] send an vpnhost-alarm email :'.$sEmailTitle);

            }
            echoMsg('['.__CLASS__.'] checkVpnIpUpdate ');
            \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_VPNHOSTCHK_113, $sCacheKey, $sCacheKey);
        }

    }

    
    public function checkRemoteIpApi(){

        
        $sCacheKey = date("YmdH");
        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_REMOTEIP_112, $sCacheKey);

        if (!$aMd5KeyData) {
            $aConfig = $this->oFrameworker->sm->get('config');

            $nRetryTimes = 3;
            while ($nRetryTimes-- > 0) {
              $bBreakFlag = false;
            

              $sIp = @file_get_contents($aConfig['adr_remoteip_api']);
              if(\YcheukfCommon\Lib\Functions::isIpv4Address($sIp)){
                \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LC_REMOTE_LURIP_836, $sIp, 86400*3650);
                
                $bBreakFlag = true; // 满足条件
              }
              
            
              if ($bBreakFlag) { 
                break;
              }else{
                sleep(5);
              }
            }
            
            if ($bBreakFlag) {
            }else{
                $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adr-remoteipapi] FAILD!";
                $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADMINEMAILS);

                \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailTitle);
                \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_REMOTEIP_112, $sCacheKey, $sEmailTitle);
                echoMsg('['.__CLASS__.'] send an alarm email :'.$sEmailTitle);

            }
        }
    }


    // 选出唯一的一个激活adr truck
    public function checkAdrTruckActive()
    {
        $bReSelect = false;
        $nActivceTruckItem = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_ONLINE_ADRTRUCK_ACTIVE_317);
        if (!$nActivceTruckItem) {
            $bReSelect = true;
        }else{
            $bReSelect = $this->oFrameworker->sm->get('Lur\Service\Addtional')->isAdrAlive($nActivceTruckItem['id']) ? false : true;
        }
        // 重选ativce truck
        if ($bReSelect) {
            $aAdrTruck = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009);
            $bSuccess = false;

            $aAdrTruck = \YcheukfCommon\Lib\Functions::shuffle_assoc($aAdrTruck);
            if (is_array($aAdrTruck) && count($aAdrTruck)) {

                $aItem = [
                    'id'=>key($aAdrTruck),
                    'date'=>date("Y-m-d H:i:s"),
                ];

                \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_ONLINE_ADRTRUCK_ACTIVE_317, $aItem, 86400*3650);

                echoMsg('['.__CLASS__.'] reselect active-adr-truck :'.json_encode($aItem));
                $bSuccess = true;

            }


            if ($bSuccess == false) {
                $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adr-truck-reselect] FAILD!";
            }else{
                $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adr-truck-reselect] SUCCESS";
            }

            $sCacheKey = date("Ymd H");
            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_FIREALARM_102, $sCacheKey);
            // var_dump(LAF_MD5KV_FIREALARM_102, $sCacheKey, $aMd5KeyData);
            if (!$aMd5KeyData) {
                $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADMINEMAILS);
                \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailTitle);
            }



        }
    }
    // 检查日志计数, 对异常日志报警
    public function chkAdrLogCounter()
    {

        $aLogerCounter = [];
        $aLogerAdrid2Counter = [];

        $aHistoryAlivesAdrServers = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getAliveList();
        foreach ($aHistoryAlivesAdrServers as $nAdrId) {
            $aTmp = ($this->oFrameworker->sm->get('\Lur\Service\Common')->getAdrServerLastStats($nAdrId));
            $sKey = "monitor_logs/".date("Ymd");
            if (isset($aTmp[$sKey]) && count($aTmp[$sKey])) {
                foreach ($aTmp[$sKey] as $logcode => $nTotal) {
                    if (!isset($aLogerCounter[$logcode])) {
                        $aLogerCounter[$logcode] = 0;
                    }
                    if (!isset($aLogerAdrid2Counter[$logcode])) {
                        $aLogerAdrid2Counter[$logcode] = [];
                    }
                    if (!isset($aLogerAdrid2Counter[$logcode][$nAdrId])) {
                        $aLogerAdrid2Counter[$logcode][$nAdrId] = 0;
                    }
                    $aLogerCounter[$logcode] += $nTotal;
                    $aLogerAdrid2Counter[$logcode][$nAdrId] += $nTotal;
                }
            }
        }

        $aFirealarmKeys = [
            ADREQUEST_247_LOG_HUNANSSP_ERROR => 150000,
            ADREQUEST_411_ERROR_CANNOT_LOAD_ONLINE_ADRCONFIG => 1000,
            ADREQUEST_412_ERROR_CANNOT_LOAD_LOCALADRCONFIG => 1000,
            ADREQUEST_403_ERROR_DAEMON_START => 1000,
            ADREQUEST_425_ERROR_HUNAN_UA => 1000,
            ADREQUEST_426_ERROR_HUNAN_PLATFORM => 1000,
            ADRJOB_EXCEPTION_SSPHUNAN_PARAMERROR => 1000,
        ];

        $aNeedAlarmCodes = [];
        foreach ($aFirealarmKeys as $code => $target) {

            if (isset($aLogerCounter[$code]) && $aLogerCounter[$code]>$target) {
                $aNeedAlarmCodes[$code] = $aLogerCounter[$code];
            }
        }


        $sCacheKey = date("Ymd");


        //本小时若未报警过, 则发报警email
        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_FIREALARM_LOGCOUNTER_110, $sCacheKey);
        if (!$aMd5KeyData && count($aNeedAlarmCodes)) {

            $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adr-logcode-too-high] ".join("/", array_keys($aNeedAlarmCodes));
            $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADMINEMAILS);
            $sText = print_r($aNeedAlarmCodes, 1);
            $sText .= "\n\n".print_r($aLogerAdrid2Counter, 1);
            \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sText);
            echoMsg('['.__CLASS__.'] send an alarm email :'.$sEmailTitle);
            echoMsg('['.__CLASS__.'] send an alarm email to :'.json_encode($aEmailTos));


            $this->oFrameworker->sm->get('Lur\Service\Common')->smsSos($sEmailTitle);
            echoMsg('['.__CLASS__.'] send SMS ');




            \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_FIREALARM_LOGCOUNTER_110, $sCacheKey, $sEmailTitle);


            // print_r($aLogerCounter);
        }

    }


    /**
     * 检查adr心跳
     * 
     */
    public function chkAdrHeartbeats()
    {

        $aAdrAllResource = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADRSERVER);
    	$aAlarmServers = array();
        $aAdrTruck = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009);// 只有 ADR-TRUCK 机器需要报警处理
    	$sCacheKey = date("Ymd H");
        $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_ADMINEMAILS);
        $aAliveServers = array();
       $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        

        // echoMsg('['.__CLASS__.'] start checking alarm email');
    	// adr_server_heartbeat_interaltime

    	if (count($aAdrAllResource)) {

            $aAdrId2Host = [];
            foreach ($aAdrAllResource as $nServerId => $label) {
                $aAdrId2Host[$nServerId] = substr($label, 0, strpos($label, "/"));
            }
            \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_ADRID2HOST_135, $aAdrId2Host, rand(86400,86400*7));



    		foreach ($aAdrAllResource as $nServerId => $aTmp) {
                $sLastStatsCacheKey = LAF_CACHE_ADRLASTSTATS_121."1/".$nServerId;
                $sLastStatsData = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, $sLastStatsCacheKey);

                // 全部
                $sLastStatsCacheKey2 = LAF_CACHE_ADRLASTSTATS_121."2/".$nServerId;
                $sLastStatsData2 = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, $sLastStatsCacheKey2);

                if (!$sLastStatsData) {//该机器最近没有上报状态, 报警
                    if (isset($aAdrTruck[$nServerId])) {
                        $aAlarmServers[$nServerId] = $aTmp;
                    }
                    continue;
                }

                //本id已经被处理过, 则不再处理
                $sCacheKey2 = LAF_CACHE_PROCESSADRID2STATSID_119.$nServerId;
                if (\YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, $sCacheKey2)==md5(json_encode($sLastStatsData))) {
                    continue;
                }
                \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, $sCacheKey2, md5(json_encode($sLastStatsData)), 1200);


                // echoMsg('['.__CLASS__.'] deal adrid=>'.$nServerId);

                // 加入在线列表
                $aAliveServers[$nServerId] = $nServerId;


		        // $sSql = "select * from b_adrstats where id=".$sLastStatsId;
		        // $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
		        // if($oPDOStatement && $oPDOStatement->rowCount()){
		        //     foreach($oPDOStatement as $result) {

                        // 处理各机器的本地ip列表
                        if (isset($sLastStatsData2['data'])) {
                            $aJsonData = json_decode($sLastStatsData2['data'], 1);
                            if (isset($aJsonData['ENV'])
                                && isset($aJsonData['ENV']['system'])
                                && isset($aJsonData['ENV']['system'])
                                && isset($aJsonData['ENV']['system']['localmachine_ip'])
                            ){
                                $oReids->sAdd(LAF_CACHE_ADRLOCALHOSTS_142, $aJsonData['ENV']['system']['localmachine_ip']);
                                
                            }
                        }


                        /**
                         * 处理拨号失败的VPN
                         */
                        if (isset($sLastStatsData['data'])) {
                            $aJsonData = json_decode($sLastStatsData['data'], 1);


                            // VPN拨号失败标识
                            if (isset($aJsonData['ADRKEY_CALLVPNFAILD_804']) && $aJsonData['ADRKEY_CALLVPNFAILD_804']==1) {

                                $nUsedVpnAccId = $this->oFrameworker->sm->get('Lur\Service\Common')->getAdr2VpnAcc($nServerId);

                                $nUsedVpnHostId = $this->oFrameworker->sm->get('Lur\Service\Common')->getAdr2VpnHost($nServerId);

                                // var_dump($nUsedVpnAccId, $nUsedVpnHostId);
                                if ($nUsedVpnAccId && $nUsedVpnHostId) {


                                    $sSql = "select * from vpn_accounts where id=$nUsedVpnAccId";
                                    $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
                                    $sAdrVpnAcc = $oPDOStatement->fetch(\PDO::FETCH_ASSOC);

                                    $sSql = "select * from vpn_hosts where id=".$nUsedVpnHostId;
                                    $oPDOStatement2 = $this->oFrameworker->queryPdo($sSql);
                                    $aVpnHost = $oPDOStatement2->fetch(\PDO::FETCH_ASSOC);


                                    $aAdrLocalVpnAcc = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_VPNLOCALACC_116.$nServerId);
                                    $aAdrVpnSets = explode(" ", $aAdrLocalVpnAcc);

                                    $bVpnRecallFlag = false;
                                    // 已经在拨号, 且拨号账号与当前分配一致
                                    if($aAdrLocalVpnAcc 
                                        && !empty($aAdrLocalVpnAcc) 
                                        && count($aAdrVpnSets) 
                                        && $aAdrVpnSets[0]==$aVpnHost['host'] 
                                        && $aAdrVpnSets[1]==$sAdrVpnAcc['account']
                                    ){
                                        $bVpnRecallFlag = true; 
                                    }
                                    if ($aAdrLocalVpnAcc==false) {//没有当前拨号账号传过来
                                        $bVpnRecallFlag = true; 
                                    }

                                    if ($bVpnRecallFlag) {//触发重新拨号
                                        $this->oFrameworker->sm->get('\Lur\Service\Addtional')->updVpnFaildHistory($sAdrVpnAcc, $aVpnHost, $nServerId);

                                        $sCacheKeyTmp = LAF_CACHE_IGNOREVPNHOST_PRE_110.$nUsedVpnHostId."/".$nUsedVpnAccId;
                                       if (!$oReids->exists($sCacheKeyTmp)) {

                                            // echoMsg('['.__CLASS__.'] ignore adrid=>vpnhostid :'.$nServerId."=>".$nUsedVpnHostId);

                                            \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, $sCacheKeyTmp, array(
                                                "hostid" => $nUsedVpnHostId,
                                                "accid" => $nUsedVpnAccId,
                                            ), 1200);
                                       }


                                        // 将分配给改机器的账号存起, 避免重复拨号
                                        \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_VPNLOCALACC_116.$nServerId, trim($aVpnHost['host']." ".$sAdrVpnAcc['account']), 300);

                                       // 触发重新分配账号
                                        $oReids->sAdd(LAF_CACHE_VPNFORBBIDENADRID_114, $nServerId);
                                        // echoMsg('['.__CLASS__.'] fengdebug recall adrid=>'.$nServerId);

                                       // \YcheukfCommon\Lib\Functions::delCache($this->oFrameworker->sm, LAF_CACHE_UPDATEVPNACC_LOCK_111);
                                    }
                               }
                            }

                        }
		        // 	}
		        // }
    		}
    	}


        // 某机器下线, 从在线列表中移除
        $aHistoryAlivesAdrServers = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getAliveList();
        if (count($aHistoryAlivesAdrServers)) {
            foreach ($aHistoryAlivesAdrServers as $nAdrServerId) {

                $bAlive = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_ADDTIONAL_ADRALIVE_PRE_310.$nAdrServerId);
                if (!$bAlive) {
                    echoMsg('['.__CLASS__.']  offline :'.$nAdrServerId);
                    $this->oFrameworker->sm->get('\Lur\Service\Addtional')->remove4AliveList($nAdrServerId);
                }
            }
        }
        // echo "aAlarmServersaAlarmServers";
        // print_r($aAlarmServers);
        
        // adr-truck 机器不在线, 报警
        if(count($aAlarmServers)){
    	// if(false){
    		$aTmp2 = array();
    		foreach ($aAdrAllResource as $nServerId => $sLabel) {
    			if (isset($aAlarmServers[$nServerId]) && isset($aAdrTruck[$nServerId])) {

    				$aTmp2[] = $nServerId." => ".$sLabel;
    			}
			}
			$sTmp = join(";\n", $aTmp2);

			$sText = <<<_EOF
	these server did not heartbeat !!

{$sTmp}

_EOF;

			//debug email
			// $aEmailTos = array(debugemails);


            //本小时若未报警过, 则发报警email
            // var_dump("#################");

            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_FIREALARM_102, $sCacheKey);
            // var_dump(LAF_MD5KV_FIREALARM_102, $sCacheKey, $aMd5KeyData);

            if (!$aMd5KeyData) {
                $sEmailTitle = LAF_LUREMAILTITLE_SOS."[adr-no-heartbeat] ".join("/", $aTmp2);
                \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sText);
                echoMsg('['.__CLASS__.'] send an alarm email :'.$sEmailTitle);
                echoMsg('['.__CLASS__.'] send an alarm email to :'.json_encode($aEmailTos));


                $this->oFrameworker->sm->get('Lur\Service\Common')->smsSos($sEmailTitle);
                echoMsg('['.__CLASS__.'] send SMS ');
                

                \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_FIREALARM_102, $sCacheKey, $sEmailTitle);
                return true;
            }
    	}else{

            //本小时若已报警过 && 换=>好, 发通知email
            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_FIREALARM_102, $sCacheKey);
            if ($aMd5KeyData) {

                $sEmailTitle = LAF_LUREMAILTITLE_NOTICE."[adr-no-heartbeat] BECOME OK ";
                \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailTitle);

                echoMsg('['.__CLASS__.'] send an alarm email :'.$sEmailTitle);
                echoMsg('['.__CLASS__.'] send an alarm email to :'.json_encode($aEmailTos));
                \YcheukfCommon\Lib\Functions::delLafCacheData($this->oFrameworker->sm, LAF_MD5KV_FIREALARM_102, $sCacheKey, $sEmailTitle);
                return true;
            }

        }

    }


}

