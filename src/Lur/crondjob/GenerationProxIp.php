<?php
/*
定期检查 gen_ips 脚本  

**/
class GenerationProxIp extends \YcheukfCommon\Lib\Crondjob{


	function __construct($oFrameworker){
		parent::__construct($oFrameworker);
    	$aPids = \Application\Model\Common::getJobsId("daemon_lur_gen_ips.php ");
    	if ($aPids!=false) {
            foreach ($aPids as $pid) {
                exec("kill -9 ".$pid." &"); 
            }
    	}

	}


    function go(){

    	// $aProxyConfig = array(1=>'a', 2=>'b', 4=>'e');
    	$aProxyConfig = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1011);

    	foreach($aProxyConfig as $kName => $sProxyUrl){

	    	$aPids = \Application\Model\Common::getJobsId("daemon_lur_gen_ips.php ".$kName);
	    	if ($aPids!=false) {
	    	    continue;
	    	}
		    $sScript = "php ".__DIR__."/daemon_lur_gen_ips.php ".$kName."  >> /dev/null &";
		    echo $sScript."\n";
		    exec($sScript); 
		}
    	


        return true;
    }

}

