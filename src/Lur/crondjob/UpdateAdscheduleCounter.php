<?php
/*
	将 counter key更新进数据库
  

**/
class UpdateAdscheduleCounter extends \YcheukfCommon\Lib\Crondjob{

    var $aCounterDb;
    var $aScheduleInfo=array();
    var $aCustomerInfo=array();
    var $aOpUserInfo=array();

    function go(){


        $nParamIndex = 2;
        $bForceFlag = null;
        if (isset($_SERVER['argv'][$nParamIndex])) {
            $bForceFlag = $_SERVER['argv'][$nParamIndex];
            if (!preg_match("/^\d{1}$/i", $bForceFlag)) {
                echo ("wrong param[".$nParamIndex."]:".$bForceFlag."\n");
                exit;
            }
        }else{
            $bForceFlag = 0;
        }


        $nRound = 1;
        $sCacheKey = null;
        $sCacheKey2 = null;
        // 普通ADR 计数
        // \YcheukfCommon\Lib\Functions::sendMQ($this->oFrameworker->sm, date("Y-m-d"), "adschedule_update");
        $oReids = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
        $aConfig = $this->oFrameworker->sm->get('config');




        // SSP计数
        $aMapSspMq2Field = array(
            "b_adcounter_ssp_mango" => "hunan",
        );
        $sSspMqName = "b_adcounter_ssp_mango";
        while ($sCounterStrings = \YcheukfCommon\Lib\Functions::getMQ($this->oFrameworker->sm, $sSspMqName)){
            list($nServerId, $sDate, $sCodeMix, $nCount) = explode("/", $sCounterStrings);
            if ($nCount < 1) {
                continue;
            }

            $sCacheKey = LAF_CACHE_COUNTERS_SSP_DAYSET_406.$sDate;
            $sCacheKey2 = LAF_CACHE_COUNTER_SSP_DAYSET4SHOW_409.$sDate;
            
            $sHashKey = $nServerId."/".$sCodeMix."/".$aMapSspMq2Field[$sSspMqName];
            $oReids->hSet($sCacheKey, $sHashKey, $nCount);
            $oReids->hSet($sCacheKey2, $sHashKey, json_encode(array(
                'c' => $nCount,
                'sl' => $nServerId,
            )));
            $nRound++;
            if ($nRound>30000) {
                break;
            }
        }
        echoMsg('['.__CLASS__.'] '."while nRound :".$nRound);
        $nRound =1;

        if ($sCacheKey) {
            $oReids->setTimeout($sCacheKey, 10*86400);
        }
        if ($sCacheKey2) {
            $oReids->setTimeout($sCacheKey2, 10*86400);
        }

        // 清理缓存
        $this->oFrameworker->sm->get('\Lur\Service\Common')->aSspData=null;

        while ($sCounterStrings = \YcheukfCommon\Lib\Functions::getMQ($this->oFrameworker->sm, "adschedule_counter")){
        	list($nServerId, $sDate, $nCodeId, $nCount) = explode("/", $sCounterStrings);
        	if ($nCount < 1) {
        		continue;
        	}

            // 处理uv
            if (!is_numeric($nCodeId)) {

                $this->saveUvdata($oReids, $nServerId, $sDate, $nCodeId, $nCount);
                continue;
            }else{
            }
            // if ($this->chkCounterDb($nServerId, $sDate, $nCodeId, $nCount)) {
            //     continue;
            // }

            $sCacheKey = LAF_CACHE_COUNTER_DAYSET_401.$sDate;
            $sCacheKey2 = LAF_CACHE_COUNTER_DAYSET4SHOW_408.$sDate;
            $sHashKey = $nServerId."/".$nCodeId;
            $oReids->hSet($sCacheKey, $sHashKey, $nCount);


            $aCodeInfo = $this->getAdInfo($nCodeId);
            $aCodeInfo2 = $this->getCustomerInfo($aCodeInfo['customer_id']);
            $aCodeInfo3 = $this->getOpUserInfo($aCodeInfo['op_user_id']);
            $sHashKey = $nServerId."/".$nCodeId."/".$aCodeInfo['customer_id']."/".$aCodeInfo['op_user_id'];


            $nSspAdCount = 0;
            $isSsp = false;
            if (!isset($aCodeInfo['m1014_id'])) {
                echoMsg('['.__CLASS__.'] '."nCodeId :".$nCodeId);
                echoMsg('['.__CLASS__.'] '."aCodeInfo :".json_encode($aCodeInfo));
            }
            // 特殊逻辑!! 若是SSP数据, 则将ssp的下的ad的统计当做这个代码的统计
            if (in_array($aCodeInfo['m1014_id'], $aConfig['ssp_platforms'])
                && !empty($aCodeInfo['sspid'])
            ) {
                if (strtotime($sDate) > strtotime('2018-05-13 23:59:59')) {
                    $isSsp = true;
                    $nSspAdCount = $this->oFrameworker->sm->get('\Lur\Service\Common')->getSspDataBySspId($sDate, $nCodeId, $nServerId);
                }
                // var_dump($nCodeId, $nCount);
            }

            $nUvAdCount = $this->oFrameworker->sm->get('\Lur\Service\Common')->getUvAdCount($sDate, $nCodeId, $nServerId);
            if ($nUvAdCount>0) {
                // echoMsg('['.__CLASS__.'] '."nUvAdCount :".$sHashKey."=>".$nUvAdCount);
            }
            $oReids->hSet($sCacheKey2, $sHashKey, json_encode(array(
                'c' => $nCount,
                'c_sad' => $nSspAdCount,
                'c_uv' => $nUvAdCount,
                'isssp' => $isSsp,
                'cl' => $aCodeInfo2['label'],
                'ul' => $aCodeInfo3['label'],
            )));


            
/**
 * 
            $sSql = "REPLACE INTO b_adcounter (`adrserver_id`, `date`, `code_id`, `count`) VALUES (:adrserver_id, :date, :code_id, :count)";
            $sth = $oPdo->prepare($sSql);
            $sth->execute(array(
                ":adrserver_id" => $nServerId,
                ":date" => date("Y-m-d", strtotime($sDate)),
                ":code_id"=> $nCodeId,
                ":count"=>$nCount,
            ));
 */
            // $this->add2CounterDb($nServerId, $sDate, $nCodeId, $nCount);
            $nRound++;
            if ($nRound>30000) {
                break;
            }
        }

        if ($sCacheKey) {
            $oReids->setTimeout($sCacheKey, 10*86400);
        }
        if ($sCacheKey2) {
            $oReids->setTimeout($sCacheKey2, 10*86400);
        }

        // echoMsg('['.__CLASS__.'] '."while nRound :".$nRound);

// exit;


// exit;
// 刷进DB

        $bFlushFlag = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_COUNTER_FLUSH2DBFLAG_402);
        if ($bForceFlag==1 || $bFlushFlag !== 1) {
            \YcheukfCommon\Lib\Functions::saveCache($this->oFrameworker->sm, LAF_CACHE_COUNTER_FLUSH2DBFLAG_402, 1, 3600);
            $nTotalProcess = $this->oFrameworker->sm->get('\Lur\Service\Common')->flushCacheCounter2Db();
            if ($nTotalProcess > 0) {
                echoMsg('['.__CLASS__.'] '."update counter total :".$nTotalProcess);
            }
            $nTotalProcess = $this->oFrameworker->sm->get('\Lur\Service\Common')->flushCacheCounterSsp2Db();
            if ($nTotalProcess > 0) {
                echoMsg('['.__CLASS__.'] '."update counter-ssp total :".$nTotalProcess);
            }
        }
    }

    public function getCustomerInfo($id='')
    {
        if (isset($this->aCustomerInfo[$id])) {
        }else{
            $aData = \Application\Model\Common::getResourceById($this->oFrameworker->sm, 'adcustomer', $id);
            $this->aCustomerInfo[$id] = array(
                'label' => $aData['id'].','.$aData['name'],
            );

        }
        return $this->aCustomerInfo[$id];
    }

    public function saveUvdata($oReids, $nServerId, $sDate, $nCodeId, $nCount)
    {
        
        $sCacheKey = LAF_CACHE_COUNTER_DAYSET4SHOW_UV_411.$sDate;
        $sHashKey = $nServerId."/".str_replace("uv", "", $nCodeId);
        $oReids->hSet($sCacheKey, $sHashKey, $nCount);
    }

    
    public function getOpUserInfo($id='')
    {
        if (isset($this->aOpUserInfo[$id])) {
        }else{
            $aData = \Application\Model\Common::getResourceById($this->oFrameworker->sm, 'user', $id);
            $this->aOpUserInfo[$id] = array(
                'label' => $aData['id'].",".$aData['username'],
            );

        }
        return $this->aOpUserInfo[$id];
    }


    public function getAdInfo($id='')
    {
        if (isset($this->aScheduleInfo[$id])) {
        }else{
            $aData = \Application\Model\Common::getResourceById($this->oFrameworker->sm, 'b_adschedule', $id);
            $this->aScheduleInfo[$id] = $aData;
            $this->aScheduleInfo[$id]['customer_id'] = $aData['user_id'];

        }
        return $this->aScheduleInfo[$id];
    }

    public function chkCounterDb($nServerId, $sDate, $nCodeId, $nCount)
    {
        $sTmpKey = $nServerId."/".$sDate."/".$nCodeId;
        if (isset($this->aCounterDb) && is_array($this->aCounterDb) && isset($this->aCounterDb[$sTmpKey])) {
            if ($this->aCounterDb[$sTmpKey]==$nCount) {
                return true;
            }
        }
        return false;
    }
    public function add2CounterDb($nServerId, $sDate, $nCodeId, $nCount)
    {
        $sTmpKey = $nServerId."/".$sDate."/".$nCodeId;
        if (!is_array($this->aCounterDb)) {
            $this->aCounterDb = array();
        }
        $this->aCounterDb[$sTmpKey] = $nCount;
    }



}

