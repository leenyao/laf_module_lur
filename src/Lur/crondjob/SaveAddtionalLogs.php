<?php
/*
将 补量的log存储到文件  

**/
class SaveAddtionalLogs extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        //first param
        $nParamIndex = 2;
        $sYestoday = null;
        // var_dump($_SERVER['argv']);
        if (isset($_SERVER['argv'][$nParamIndex])) {
            $sYestoday = $_SERVER['argv'][$nParamIndex];
            if (!preg_match("/^\d{8}$/i", $sYestoday)) {
                echo ("wrong param[".$nParamIndex."]:".$sYestoday."\n");
                exit;
            }
        }else{
        
                //echo ("need param[".$nParamIndex."]\n");
                //exit;
            $sYestoday = date("Ymd", strtotime("-1 day"));
        }

        echoMsg('['.__CLASS__.'] sYestoday: '.$sYestoday);
        

        // $sYestoday = date("Ymd", strtotime("-1 day"));
        // $sYestoday = date("Ymd");

        exec("mkdir -p ".BASE_INDEX_PATH."/../data/addtionallogs_get/");
        exec("mkdir -p ".BASE_INDEX_PATH."/../data/addtionallogs_send/");
        exec("mkdir -p ".BASE_INDEX_PATH."/../data/addtionallogs_db/");
        exec("mkdir -p ".BASE_INDEX_PATH."/../data/addtionallogs_adridhist_send/");
        exec("mkdir -p ".BASE_INDEX_PATH."/../data/addtionallogs_adridhist_get/");

        $sFile1 = BASE_INDEX_PATH."/../data/addtionallogs_get/".$sYestoday;
        $sFile2 = BASE_INDEX_PATH."/../data/addtionallogs_send/".$sYestoday;
        $sFile3 = BASE_INDEX_PATH."/../data/addtionallogs_db/".$sYestoday;
        $sDir4 = BASE_INDEX_PATH."/../data/addtionallogs_adridhist_send/";
        $sDir5 = BASE_INDEX_PATH."/../data/addtionallogs_adridhist_get/";


        // var_dump($sFile1);
        // var_dump(date("Ymd", $nRealmTime));


        // 获取
        if (
            !file_exists($sFile1)   //文件不存在
            || (file_exists($sFile1) && $sYestoday >= date("Ymd", filectime($sFile1))) //文件存在但不是隔天生成
        ) {
            $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_TRADENO_GET_303."/".$sYestoday;
            $aAllLogs = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->hGetAll($sTradeNoCacheKey);
            $hFile = fopen($sFile1, "w");

            $aData4File = array();
            if (count($aAllLogs)) {
                foreach ($aAllLogs as $sTmp) {
                    $aJsonData = json_decode($sTmp, 1);
                    $aData4File[] = json_encode(array(
                        "t" => $aJsonData[1],
                        "i" => $aJsonData[2]['i'],
                        "d" => $aJsonData[0],
                    ));
                }
            }
            ksort($aData4File);


            fwrite($hFile, join("\n", $aData4File));
            fclose($hFile);
            // $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->del($sTradeNoCacheKey);
            echoMsg('['.__CLASS__.'] save addtional logs: '.$sFile1);

        }

        // 上报
        if (!file_exists($sFile2)
            || (file_exists($sFile2) && $sYestoday >= date("Ymd", filectime($sFile2)))) //文件存在但不是隔天生成 
        {
            $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_TRADENO_SEND_302."/".$sYestoday;
            $aAllLogs = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->hGetAll($sTradeNoCacheKey);
            $hFile = fopen($sFile2, "w");


            $aData4File = array();
            if (count($aAllLogs)) {
                foreach ($aAllLogs as $sTmp) {
                    $aJsonData = json_decode($sTmp, 1);
                    $aData4File[] = json_encode(array(
                        "t" => $aJsonData[1],
                        "i" => $aJsonData[2]['i'],
                        "d" => $aJsonData[0],
                    ));
                }
            }
            ksort($aData4File);

            
            fwrite($hFile, join("\n", $aData4File));
            fclose($hFile);
            echoMsg('['.__CLASS__.'] save addtional logs: '.$sFile2);
            // $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->del($sTradeNoCacheKey);

        }

        // 池子
        if (!file_exists($sFile3) 
            || (file_exists($sFile3) && $sYestoday >= date("Ymd", filectime($sFile3)))) //文件存在但不是隔天生成 
        {
            $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".$sYestoday;
            $aAllLogs = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->hGetAll($sTradeNoCacheKey);
            $hFile = fopen($sFile3, "w");


            $aData4File = array();
            if (count($aAllLogs)) {
                ksort($aAllLogs);
                foreach ($aAllLogs as $sKey => $sTmp) {
                    $aData4File[] = $sKey." ".$sTmp;
                }
            }
            
            fwrite($hFile, join("\n", $aData4File));
            fclose($hFile);
            echoMsg('['.__CLASS__.'] save addtional logs: '.$sFile3);
            // $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->del($sTradeNoCacheKey);

        }

        // 操作记录 send
        $sHistoryKey = LAF_CACHE_ADDTIONAL_SENDLOG_PRE_311.$sYestoday;
        $aKeys = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->keys($sHistoryKey."*");
        foreach ($aKeys as $sKeyTmp) {
            $sFileTmp = $sDir4.str_replace("/", "_", $sKeyTmp);

            if (!file_exists($sFileTmp) 
                || (file_exists($sFileTmp) && $sYestoday >= date("Ymd", filectime($sFileTmp)))) //文件存在但不是隔天生成 
            {

                if ($this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->exists($sKeyTmp)) {
                    $aData4File = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->lRange($sKeyTmp, 0, -1);
                    $hFile = fopen($sFileTmp, "w");
                    fwrite($hFile, join("\n", $aData4File));
                    fclose($hFile);
                    echoMsg('['.__CLASS__.'] save addtional logs: '.$sFileTmp);
                }
            }
        }

        // 操作记录 get
        $sHistoryKey = LAF_CACHE_ADDTIONAL_GETLOG_PRE_312.$sYestoday;
        $aKeys = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->keys($sHistoryKey."*");
        foreach ($aKeys as $sKeyTmp) {
            $sFileTmp = $sDir5.str_replace("/", "_", $sKeyTmp);
            if (!file_exists($sFileTmp) 
                || (file_exists($sFileTmp) && $sYestoday >= date("Ymd", filectime($sFileTmp)))) //文件存在但不是隔天生成 
            {
                if ($this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->exists($sKeyTmp)) {
                    $aData4File = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->lRange($sKeyTmp, 0, -1);
                    $hFile = fopen($sFileTmp, "w");
                    fwrite($hFile, join("\n", $aData4File));
                    fclose($hFile);
                    echoMsg('['.__CLASS__.'] save addtional logs: '.$sFileTmp);
                }
            }
        }
        
        // $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->del($sTradeNoCacheKey);


        return true;
    }

}

