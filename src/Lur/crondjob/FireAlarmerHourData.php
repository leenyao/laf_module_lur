<?php
/*
每个小时检查数据, 若数据太少可能是平台出现问题, 报警

**/
class FireAlarmerHourData extends \YcheukfCommon\Lib\Crondjob{



    function go(){
        if (date("Ymd")=='20200404') {
            return true;
            
        }

        $aConfig = $this->oFrameworker->sm->get('config');
        echoMsg('['.__CLASS__.'] start ');

        $this->chk($aConfig);


        return true;
    }



    /**
     * 检查小时报表
     * 
     */
    public function chk($aConfig)
    {

        $sToday = date("Ymd");
        $sCurrentHour = date("H");


        $aLastHourData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_HOURDATA_111, LAF_MD5KV_HOURDATA_111);
        $aCurrentHourDatas = $this->oFrameworker->sm->get('\Lur\Service\Common')->getLatestDateHourReport();

        if ($aLastHourData 
            && isset($aLastHourData['data'])
            && isset($aLastHourData['data'][$sToday])
        ) {

            $nLastTotal = array_sum($aLastHourData['data'][$sToday]);
            $nCurrentTotal = array_sum($aCurrentHourDatas[$sToday]);
            $nDiff = $nCurrentTotal - $nLastTotal;

            // print_r($nCurrentTotal);

            if (\YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_ADRRELEASING_320) != 1) {
                if ($nDiff < 3000) {

                    echoMsg("nLastTotal=".$nLastTotal);
                    echoMsg("nCurrentTotal=".$nCurrentTotal);
                    echoMsg("nDiff=".$nDiff);
                    $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1012);

                    $sEmailTitle = LAF_LUREMAILTITLE_SOS."[hourdata-grow-slowly] 小时数据增长缓慢 ";
                    \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailTitle);
                    echoMsg('['.__CLASS__.'] send an alarm email :'.$sEmailTitle. " to ".json_encode($aEmailTos));

                }
            }else{
                // 升级中
            }
        }

        \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, LAF_MD5KV_HOURDATA_111, LAF_MD5KV_HOURDATA_111, $aCurrentHourDatas);


        return true;



    }

}

