<?php
/*
报警器  

**/
class FireAlarmerScheduleFinished extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $aConfig = $this->oFrameworker->sm->get('config');

        //允许接受命令行参数
        if (isset($_SERVER['argv'][2])) {
            $sYestoday = $_SERVER['argv'][2];
            if (!preg_match("/^\d{4}-\d{2}-\d{2}$/i", $sYestoday)) {
                echoMsg('['.__CLASS__.'] wrong date format:'.$sYestoday);

            }
        }else{
            $sYestoday = date("Y-m-d", strtotime("-1 day"));
        }

        echoMsg('['.__CLASS__.'] start ');

        $this->chkAdrScheduleFinished($aConfig, $sYestoday);


        return true;
    }


    /**
     * 检查排期是否完成
     */
    public function chkAdrScheduleFinished($aConfig, $sYestoday){

        //该天若已经检查过, 则不再检查
        $sCacheKey = $sYestoday;
        $nAlarmFinishPercent = 90;//完成率低于该百分比的则报警


        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, LAF_MD5KV_ADRDATAFINISHED, $sCacheKey);
        if (count($aMd5KeyData) < 1) {//昨天没有倒库, 报警
            $aEmailTos = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_MTYPE_TECHOPMAIL_1024);
            $sEmailTitle = LAF_LUREMAILTITLE_SOS."[batchdataimport-not-finishedt] ".$sYestoday;
            \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos), $sEmailTitle, $sEmailTitle);
            echoMsg('['.__CLASS__.'] send an SOS email: '.$sEmailTitle. "; emails:".json_encode($aEmailTos));


            $this->oFrameworker->sm->get('Lur\Service\Common')->smsSos($sEmailTitle);
            echoMsg('['.__CLASS__.'] send SMS ');
            
            return true;
        }

        $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, 103, $sCacheKey);
        if (count($aMd5KeyData)) {//暂时屏蔽, 允许重复发送报警email
            // return true;
        }




        $aAlarmData = array();
        $sSql = "select *,floor(100*me1/me2) as finishpercent from b_report_e where date='".$sYestoday."' and me2<>0 and floor(100*me1/me2)<".$nAlarmFinishPercent ." order by user_id";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
                $nCustomerId = $result['user_id'];

                /*若是转移了用户, 则以最后一个为准*/
                $aTmp6 = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', $result['de1']);
                if (count($aTmp6) < 1 || $nCustomerId != $aTmp6['user_id']) {
                    continue;
                }


                $nOpUserId = 0;//运营人员账号
                $aTmp4 = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adcustomer', $nCustomerId);
                if (count($aTmp4) && isset($aTmp4['user_id'])) {
                    $nOpUserId = $aTmp4['user_id'];
                }

                if (!empty($nOpUserId)) {
                    $aTmp3 = array(
                        "pecent" => $result['finishpercent']."%",
                        "finished" => $result['me1'],
                        "plan" => $result['me2'],
                        "user_id" => $nCustomerId,
                        "user_label" => $aTmp4['name'],
                        "code_id" => $result['de1'],
                        "code_url" => $result['de2'],
                    );
                    if (!isset($aAlarmData[$nOpUserId])) {
                        $aAlarmData[$nOpUserId] = array();
                    }
                    $aAlarmData[$nOpUserId][] = '"'.join("\"\t\"", $aTmp3).'"';
                }
            }
        }

        if (count($aAlarmData)) {

            //技术运营人员
            $aEmailToBccs = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_MTYPE_TECHOPMAIL_1024);

            foreach ($aAlarmData as $nOpUserId => $aScheduleData) {
                $aTmp5 = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'user', $nOpUserId);
                if (\YcheukfCommon\Lib\Functions::checkEmail($aTmp5['email']) === false) {
                    echoMsg('['.__CLASS__.'] can not send email, unavailable email: '.$aTmp5['email']);
                    continue;
                }
                //运营人员邮箱
                $aEmailTos = array($aTmp5['email']);
                            

                $sEmailTitle = LAF_LUREMAILTITLE_NOTICE."[schedule-didnot-finished] ".$aTmp5["username"].",".$sYestoday.",".count($aScheduleData);
                $sFileName = "tmpupload/snf_".md5($sEmailTitle).".csv";
                $sWriteFilePath = BASE_INDEX_PATH."/".$sFileName;
                $sFileUrl = $aConfig['webhost_path']."/".$sFileName;
                $fHandle = fopen($sWriteFilePath, "w");
                fwrite($fHandle, chr(255).chr(254).iconv("UTF-8", "UTF-16LE", "finished percent\tfinished\tschedule\tuser id\tuser name\tcode id\tcode url\n".join("\n",$aScheduleData)));
                fclose($fHandle);

                $sEmailContent = <<<OUTPUT
                download csv :

                $sFileUrl
OUTPUT;
                \YcheukfCommon\Lib\Functions::sendEmail($this->oFrameworker->sm, array('to'=>$aEmailTos, 'bcc'=>$aEmailToBccs), $sEmailTitle, 
                    $sEmailContent);


                echoMsg('['.__CLASS__.'] send an alarm email: '.$sEmailTitle. "; emails:".json_encode($aEmailTos));
                echoMsg('['.__CLASS__.'] download url : '.$sWriteFilePath);

                //记录发送记录
                \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, 103, $sCacheKey, "");


            }

            
        }

    }


}

