<?php
/*
处理单代码多地域的投放, 动态平衡量
**/
class AddtionalDbBalance extends \YcheukfCommon\Lib\Crondjob{



    function go(){
        $aAlljobs = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getAllJobs();

        $aCodes = [];
        // print_r($aAlljobs);

        if (count($aAlljobs)) {
            foreach ($aAlljobs as $nCityId => $aRow1) {
                foreach ($aRow1 as $sJobId => $nCount) {
                    if ($nCount < 5000) {
                        continue;
                    }
                    list($nCodeId, $t1, $t2, $t3) = explode("-", $sJobId);



                    $aM1007 = \Application\Model\Common::getRelationList($this->oFrameworker->sm,1007, $nCodeId);
                    if ($aM1007 && count($aM1007)>1) {

                        $aId2Counts = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getJobCountById($nCodeId);

                        foreach ($aM1007 as $nCityIdTmp) {
                            $sJobIdTmp = $nCodeId."-".$t1."-".$t2."-".$nCityIdTmp;
                            if (!isset($aCodes[$nCodeId][$sJobIdTmp])) {
                                if (!isset($aCodes[$nCodeId])) {
                                    $aCodes[$nCodeId] = $aId2Counts;
                                }else{
                                    $aCodes[$nCodeId] = array_merge($aCodes[$nCodeId], $aId2Counts);

                                }
                                $aCodes[$nCodeId][$sJobIdTmp] = 0;


                            }
                        }

                    }



                }
            }
        }
        // print_r($aCodes);

        if (count($aCodes)) {
            // 动态平衡量, 将缺量最大的城市折半分给缺量为0的城市
            foreach ($aCodes as $codeId => $aTmp1) {
                $aBplan = [];
                $nMaxCountJobkey = null;
                $nMaxCount = 0;
                foreach ($aTmp1 as $jobkey => $count) {
                    if ($count < 200) {
                        $aBplan[] = $jobkey;
                    }
                    if ($count > $nMaxCount) {
                        $nMaxCount = $count;
                        $nMaxCountJobkey = $jobkey;
                    }
                }

                $aOffsetData = [];
                if (count($aBplan)) {
                    // var_dump($nMaxCount);
                    // var_dump($aBplan);
                    // var_dump($nMaxCountJobkey);

                    // 最大jobkey需要减掉的量
                    $nOffsetCount = intval($nMaxCount/(count($aBplan)+1));

                    $aOffsetData[$nMaxCountJobkey] = -($nOffsetCount*count($aBplan));

                    foreach ($aBplan as $jobkeytmp) {
                        $aOffsetData[$jobkeytmp] = ($nOffsetCount);
                    }

                }

                if (count($aOffsetData)) {
                    $sCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".date("Ymd");
                    foreach ($aOffsetData as $jobkeytmp2 => $count2) {
                        $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->hIncrBy($sCacheKey, $jobkeytmp2, $count2);
                    }

            // print_r($aOffsetData);



                   echoMsg(json_encode($aOffsetData),'['.__CLASS__.'] addtional blance');
                }


            }
        }


        return true;
    }
}

