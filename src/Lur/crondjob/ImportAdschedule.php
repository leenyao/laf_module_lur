<?php
/*
导入排期文件  

**/
class ImportAdschedule extends \YcheukfCommon\Lib\Crondjob{


    var $aConfig;
    var $aAppConfig;
    var $aDefaultAdrServerId;
    var $aCodeType;//代码类型
    var $bLoginFlag = false;

    function __construct($oFrameworker){
        parent::__construct($oFrameworker);

        // 顺序不能改, 处理有先后
        $this->aCodeType = array("show", "click", "uv", 'vpc');
        $this->aDefaultAdrServerId = null;

        $aM1010 = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, 1010);
        $aM1022 = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->oFrameworker->sm, LAF_METADATATYPE_IGNOREURL_1022);

        $this->aConfig = array(
            'debug' => 1,
            'adserver_ignore_domains' => $aM1010,
            'adserver_ignore_urls' => $aM1022,
            'nFlowRedirectLevel' => 3,
            'nCurlSlowTime' => 10,
        );

        $this->aAppConfig = $this->oFrameworker->sm->get('config');

    }
    function go(){

        $sSql = "select * from system_batch_jobs where type=1 and m158_id in (1,2,3,4,5) order by id asc";
        $oPDOStatement = $this->oFrameworker->queryPdo($sSql);
        $oRedis = \YcheukfCommon\Lib\Functions::getCacheObject($this->oFrameworker->sm);
// echo BASE_INDEX_PATH;
        if($oPDOStatement && $oPDOStatement->rowCount()){
            foreach($oPDOStatement as $result) {
                
                

                // 检查是否存在该用户, 存在的帮助登陆
                $aUser = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'user', $result['op_user_id']);
                if (count($aUser)) {
                    \YcheukfCommon\Lib\Functions::saveAuthIdentity($this->oFrameworker->sm, $result['op_user_id']);
                    $this->bLoginFlag = true;
                }else{
                    $this->updateTable(20, "no such userid:".$result['op_user_id'], $result['id']);
                    continue;
                }



                $this->updateTable(5, "Processing, it will take a little long time when you post a new url...wait, plz", $result['id']);
                $t1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat(    );
                @list($bFlag, $aCsvData) = $this->_getCsvData($result);

                if ($bFlag) {
                    @list($bFlag, $sResult, $nUserId, $sOutJson, $aScheduleIds, $planno) = $this->doImport($aCsvData, $result['op_user_id']);
                    $t2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat(    );
                    if ($bFlag) {
                        $this->updateTable(10, $sResult, $result['id'], ($t2-$t1), $nUserId, $sOutJson, $aScheduleIds, $planno);
                        $ischild = !empty($result['pplanno']) && !empty($planno) ? true : false;

                        if ($ischild == false){
                            $platforms = ['sohu'=>'shmain'];
                            foreach($platforms as $platform=>$opkey){
                                $key1 = LAF_CACHE_DISTRICBUTIONAPI_MAINPLANLIST_817.$platform;
                                $oRedis->sAdd($key1, $result['id']);
                            }
                        }

                    }else{
                        $this->updateTable(20, $sResult, $result['id'], ($t2-$t1), $nUserId, null, $aScheduleIds, $planno);
                    }
                }else{
                    $this->updateTable(20, $sResult, $result['id'], ($t2-$t1), $nUserId, null, $aScheduleIds);
                }
            }
        }

        return true;
    }

    public function _getCsvData($result)
    {
        $sFile = $result['in_file'];

        $sFile = BASE_INDEX_PATH.$sFile;
        if (!file_exists($sFile)) {
            return $this->doErrorMsg(0, 'file not exists:'.$sFile);
        }

        $sEncoding = \YcheukfCommon\Lib\Functions::getFileEncoding($sFile);

        $handle = fopen($sFile,"r");
        $aReturn = array();
        if ($handle) {
            while (!feof($handle)) {
                $aData = fgetcsv($handle);
                if (!is_array($aData) || count($aData) < 1) {
                    continue;
                }
                if (strtoupper($sEncoding) !== 'UTF-8') {
                    foreach ($aData as $key => $value) {
                        $aData[$key] = iconv($sEncoding, "UTF8", $value);
                    }
                }
                $aReturn[] = $aData;
            }
            fclose($handle);
        }
        return array(1, $aReturn);
    }



    // 处理曾经错乱的ID
    public function _getMapCityId($nId)
    {
        $aMap = array(
            34=>27,
            220=>50,
            225=>62,
            196=>168,
            232=>143,
            224=>81,
            235=>183,
        );
        return isset($aMap[$nId]) ? $aMap[$nId] : $nId;
    }


    function isOttua($uas)
    {
        $uas = is_numeric($uas) ? [$uas] : json_decode($uas, 1);
        foreach ($uas as $ua) {
            if (in_array($ua, $this->aAppConfig['ottuas'])) {
                return true;
            }
        }
        return false;
    }
/**
 * 描述见 IndexController.downloadsampleinfoAction
 */
    public function doImport($aCsvData, $sUserOpId)
    {

        if ($this->bLoginFlag == false) {
            \YcheukfCommon\Lib\Functions::saveAuthIdentity($this->oFrameworker->sm, $sUserOpId);
            $this->bLoginFlag = true;
        }


        
        $aSspTypes = $this->aAppConfig['ssp_platforms'];

        //控制器
        $nProccedCount = 0;
        $nProccedCount_active = 0;
        $nProccedCount_inactive = 0;
        $eparams = $aScheduleIds = $aCustomerList = $aScheduleDateHeader = $aDbIds = $aDbData = $aUrlExists = $aFinishedCode = array();
        foreach ($this->aCodeType as $sCodeName) {
            $aDbIds[$sCodeName] = $aDbData[$sCodeName] = array();
        }
        $nLineIndex = 0;
        $nUserIdRecord = 0;
        $bUvTmpltureFlag = false;
        $oContentController = $this->oFrameworker->sm->get('controllerloader')->get('Application\Controller\AdminContentController');
        $aSspIds = [];
        $sohusspvail = [];
        $aWarrings = [];
        $planno = "";
        if (count($aCsvData)) {
            foreach ($aCsvData as $aData) {
                $nLineIndex++;
                if (!is_array($aData) || count($aData) < 1) {
                    continue;
                }
                foreach ($aData as $keyTmp2 => $vTmp2) {
                    $aData[$keyTmp2] = trim($vTmp2);
                }
                if ($nLineIndex == 1) {//表头

                    //通过 date-start-column 定位动态分割线
                    $nDateIndex = 0;
                    foreach ($aData as $k2 => $v2) {
                        if ($v2 == 'date-start-column') {
                            $nDateIndex = $k2+1;
                            break;
                        }
                    }
                    if ($nDateIndex == 0) {                     
                        return $this->doErrorMsg($nLineIndex, "illegal csv. can not find column 'date-start-column", $aData);
                    }

                    //循环日期部分表头, 检查合法性
                    while (1) { 
                        if (isset($aData[$nDateIndex])) {
                            if(empty($aData[$nDateIndex])){
                            }else{
                                //检查日期格式是否正确
                                $sErrorMsg = $this->_chkDateFormat($nLineIndex, $aData[$nDateIndex]);
                                if ($sErrorMsg !== true) return $sErrorMsg;

                                //并兼容不同excel版本, 将其日期格式统一切换成Y-m-d格式
                                $aScheduleDateHeader[$nDateIndex] = $this->_fmtDateFormat($aData[$nDateIndex]);
                            }
                            $nDateIndex++;
                        } else {
                            break;//
                        }
                    }
                }else{//内容

                    if(count($aData) != $nDateIndex){
                        return $this->doErrorMsg($nLineIndex, "column not match", count($aData)." vs ".$nDateIndex);
                    }
                    $bEmptyFalg = true;
                    foreach ($aData as $sColumnValue) {
                        $sColumnValue = trim($sColumnValue);
                        if (!empty($sColumnValue)) {
                            $bEmptyFalg = false;
                            break;
                        }
                    }
                    if ($bEmptyFalg) {
                        continue;
                    }
                        // print_r($aData);

                    $aData[1] = \YcheukfCommon\Lib\Functions::iconv2Utf8($aData[1]);
                    foreach ($aData as $nDateIndex2 => $sTmp4) {
                        $aData[$nDateIndex2] = trim($sTmp4);
                    }



                    $aScheduleShowDate2Count = $aAdrServerIds = $aScheduleData = array();
                    foreach ($this->aCodeType as $sCodeName) {
                        $aScheduleData[$sCodeName] = array();
                    }



                    if (!empty($aData[5]) && is_null(json_decode($aData[5]))) {
                        return $this->doErrorMsg($nLineIndex, "unavailable json", $aData, 6);
                    }
                    $bFlagTmp =  $this->chkListAvailable($nLineIndex, 1008, $aData[5]);
                    if (!$bFlagTmp) {
                        return $this->doErrorMsg($nLineIndex, "no such ua in database", $aData[5], 6);
                    }

                    if (LAF_ADSOURCETYPE_SSP_SOHU==$aData[13] && is_array($aData[5]) && count($aData[5])>=2) {
                        return $this->doErrorMsg($nLineIndex, "被指定的UA不能超过1个", $aData[5], 6);
                    }

                    $isOtt = $this->isOttua($aData[5]);
                    // 若是ott, 限制ip,cookie 频次
                    if ($isOtt) {
                        $aData[8] = 50;
                        $aData[9] = 1;
                    }

                    // var_dump("isott");
                    // var_dump($isOtt);
                    // var_dump($aData[5]);
                    // var_dump($aData[8]);
                    // var_dump($aData[9]);


                    foreach ($aScheduleDateHeader as $k1 => $v1) {
                        $aTmp6 = array();
                        if (preg_match("/\|\s*click/i", $v1)) {//点击代码
                            $v1 = preg_replace("/\|\s*click/i", "", $v1);
                            $v1 = trim($v1);
                            $aTmp6[0] = $v1;

                            $aTmp6[1] = 1;//hard code: when proc click-code
                            // $aTmp6[1] = intval($aData[9]);//cookie fq
                            $aTmp6[2] = intval($aData[8]);//ip fq
                            $aTmp6[3] = intval(str_replace(",", "", $aData[$k1]));//count

                            if (empty($aTmp6[3])) {
                                $aTmp6[3] = 0;
                            }
                            
                            $aScheduleData['click'][] = $aTmp6;
                        }elseif (preg_match("/\|\s*uv/i", $v1)) {//uv
                            $v1 = preg_replace("/\|\s*uv/i", "", $v1);
                            $v1 = trim($v1);
                            $aTmp6[0] = $v1;

                            $aTmp6[1] = 1;//hard code: when proc click-code
                            $aTmp6[2] = intval($aData[8]);//ip fq
                            $aTmp6[3] = intval(str_replace(",", "", $aData[$k1]));//count
                            if (empty($aTmp6[3])) {
                                $aTmp6[3] = 0;
                            }
                            $aScheduleData['uv'][] = $aTmp6;

                        }elseif (preg_match("/\|\s*vpc/i", $v1)) {//uv
                            $v1 = preg_replace("/\|\s*vpc/i", "", $v1);
                            $v1 = trim($v1);
                            $aTmp6[0] = $v1;

                            $aTmp6[1] = intval($aData[9]);//cookie fq
                            $aTmp6[2] = intval($aData[8]);//ip fq
                            $aTmp6[3] = intval(str_replace(",", "", $aData[$k1]));//count

                            if (empty($aTmp6[3])) {
                                $aTmp6[3] = 0;
                            }
                            $aScheduleData['vpc'][] = $aTmp6;

                        }else {//显示代码
                            $v1 = preg_replace("/\|\s*show/i", "", $v1);
                            $v1 = trim($v1);
                            $aTmp6[0] = $v1;

                            $aTmp6[1] = !empty($aData[9]) ? intval($aData[9]) : 1;//cookie fq
                            $aTmp6[2] = !empty($aData[8]) ? intval($aData[8]) : 1;//ip fq
                            $aTmp6[3] = intval(str_replace(",", "", $aData[$k1]));//count
                            if (empty($aTmp6[3])) {
                                $aTmp6[3] = 0;
                            }
                            $aScheduleData['show'][] = $aTmp6;
                            // var_dump($aTmp6);
                            list($sDateTmp3) = explode(" ", $aTmp6[0]);
                            $aScheduleShowDate2Count[$sDateTmp3] = $aTmp6[3];
                        }
                        unset($aTmp6);
                    }


                    // 默认为web投放, 若传递了appid, 则强制转换成 sdk投放
                    $aData[13] = !empty($aData[13]) ? $aData[13] : LAF_ADSOURCETYPE_WEB;
                    if (!in_array($aData[13], [LAF_ADSOURCETYPE_UV])) {
                        $aData[13] = !empty($aData[14]) ? LAF_ADSOURCETYPE_SDK : $aData[13];
                    }
                    $aMetadataList = array();
                    $aMetadataList[1014] = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, 1014);
                    if (!in_array(intval($aData[13]), array_keys($aMetadataList[1014]))) {
                        return $this->doErrorMsg($nLineIndex, "wrong sourcetype ", $aData[13], 14);
                    }


                    // 判断taid是否正确
                    $aData[22] = trim($aData[22]);
                    if (!empty($aData[22])) {
                        $aMetadataList = array();
                        $aMetadataList[1023] = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, 1023);
                        if (!isset($aMetadataList[1023][$aData[22]])) {
                            return $this->doErrorMsg($nLineIndex, "wrong ta-id ", $aData[22], 23);
                        }
                        
                    }else{
                        $aData[22] = 0;
                    }



                    // 额外的媒体参数 
                    if (isset($aData[21]) && !empty($aData[21])) {
                        if (preg_match("/mzta=/i", $aData[21])) {
                            $s1 = str_replace("mzta=", "", $aData[21]);
                            $eparams[] = $aData[21];
                            $a1 = explode(",", $s1);
                            foreach ($a1 as $s2) {
                                // $s2 = trim($s2);
                            }
                        }else{
                            return $this->doErrorMsg($nLineIndex, "wrong mzta ", $aData[21], 22);
                        }
                    }

                    if (!isset($aData[20]) || empty($aData[20])) {
                        $aData[20] = 0;
                    }

                    $aRtbData = [];

                    // 媒资id
                    $sSspId = '';
                    $aSspMediaIds = array();
                    if (in_array($aData[13], $aSspTypes)) {//ssp 
                        if (empty($aData[2])) {
                            return $this->doErrorMsg($nLineIndex, "unavailable sspid", $aData[2], 3);
                        }else{
                            switch ($aData[13]) {
                                case LAF_ADSOURCETYPE_SSP_HUNAN:
                                case LAF_ADSOURCETYPE_SSP_LINGJI:
                                    if (!preg_match('/^\d+$/i', $aData[2])) {
                                        return $this->doErrorMsg($nLineIndex, "sspid should be  numbers like '201824'", $aData[2], 3);
                                    }
                                    $sSspId=$aData[2];
                                    
                                    if (empty($planno)){
                                        $planno = $aData[2];
                                    }
                                    $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("|", "_", $aData[2])."|".$aData[1]."|".$aData[6]);
                                break;
                                case LAF_ADSOURCETYPE_SSP_LETV:
                                    if (preg_match('/^\s*\d+\s*,\s*\d{1,2}\s*,\s*\d{5,}\s*,\s*\d{5,}\s*,\s*\d+,\s*\d+$/i', $aData[2])) { // [广告位ID,平台ID,dealid,oid,duration,ct]
                                        $sSspId=$aData[2];

                                    }elseif (preg_match('/^\s*\d{5,}\s*,\s*\d{5,}\s*,\s*\d+\s*,\s*\d{1,2}/i', $aData[2])) { //[dealid,oid,广告位ID,平台ID,duration,ct]

                                        $fClosureFmtSspId = function($sSspId) 
                                        {
                                            $aSplit = explode(',', $sSspId);
                                            return $aSplit[2].",".$aSplit[3].",".$aSplit[0].",".$aSplit[1].",".$aSplit[4].",".$aSplit[5];
                                        };
                        // echoMsg('['.__CLASS__.'] [b sSspId]: '.$aData[2]);

                                        $sSspId = $fClosureFmtSspId($aData[2]);
                                        unset($fClosureFmtSspId);
                        // echoMsg('['.__CLASS__.'] [a sSspId]: '.$sSspId);
                                        
                                    }else{
                                        return $this->doErrorMsg($nLineIndex, "sspid should be string like [dealid,oid,广告位ID,平台ID,duration,CT] '27096,232414,36,7,15,2'", $aData[2], 3);

                                    }



                                    $fClosureChkPlatForm = function($sSspId) 
                                    {
                                        $aReturn = false;
                                        $aSplit = explode(',', $sSspId);
                                        if (isset($aSplit[1]) && in_array($aSplit[1], [1,2,3,4,5,6,7,8,9,10,19])) {
                                            $aReturn = true;
                                        }
                                    
                                        return $aReturn;
                                    };
                                    if ($fClosureChkPlatForm($sSspId) == false) {
                                        return $this->doErrorMsg($nLineIndex, " unavailable 平台ID($sSspId) ", $aData[2], 3);
                                    };
                                    unset($fClosureChkPlatForm);
                                    

                                    

                                    $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("|", "_", $aData[2])."|".$aData[1]."|".$aData[6]);
                                break;
                                case LAF_ADSOURCETYPE_SSP_SOHU:
                                    $aData[23] = !isset($aData[23]) || empty($aData[23]) ? 1 : $aData[23];
                                    // 指定的sohu id
                                    $sSohuIdStr = $aData[23]==1?'':'|'.$aData[23];
                                    $sSspId=str_replace("|", "#", $aData[2]);
                                    $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$sSohuIdStr.$aData[13]."|".str_replace("#", "_", $sSspId)."|".$aData[1]."|".$aData[6]);
                                break;
                                case LAF_ADSOURCETYPE_SSP_KEEP:
                                    // 指定的sohu id
                                    $sSspId=str_replace("|", "#", $aData[2]);
                                    if (preg_match('/^\s*\d+\s*,\s*\d+\s*,\s*[\w_]+\s*,\s*\d+\s*,\s*\d+\s*$/i', $aData[2])) { // [广告位ID,平台ID,dealid,oid,duration,ct]
                                        $sSspId=$aData[2];
                                        $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("#", "_", $sSspId)."|".$aData[1]."|".$aData[6]);
// var_dump($aData);
                                    }else{
                                        return $this->doErrorMsg($nLineIndex, "sspid should be string like [imp.id,deal_type,deal_id,宽,高] '1000,1,rs_open_20200119,640,960'", $aData[2], 3);

                                    }

                                break;
                                case LAF_ADSOURCETYPE_SSP_REACHMAX:
                                    if (preg_match('/^\s*\d+\s*$/i', \YcheukfCommon\Lib\Functions::replaceAdrTag($aData[2]))) { // [dealid]
                                        $sSspId=$aData[2];
                                        $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("#", "_", $sSspId)."|".$aData[1]);
// var_dump($aData);
                                    }else{
                                        return $this->doErrorMsg($nLineIndex, "sspid should be string like [deal_id] '1234567'", $aData[2], 3);
                                    }

                                break;
                                case LAF_ADSOURCETYPE_SSP_VOICEADS:
                                    if (preg_match('/^\w*(,\w*){3}$/i', \YcheukfCommon\Lib\Functions::replaceAdrTag($aData[2]))){
                                        $sSspId=$aData[2];
                                        $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("#", "_", $sSspId)."|".$aData[1]);
// var_dump($aData);
                                    }else{
                                        return $this->doErrorMsg($nLineIndex, "sspid should be string like [广告位ID,deal_id,宽,高] 'aaa,bbb,100,50'", $aData[2], 3);
                                    }

                                break;
                                
                                case LAF_ADSOURCETYPE_SSP_MIGU:
                                    list($did1) = explode(",", \YcheukfCommon\Lib\Functions::replaceAdrTag($aData[2]));
                                    if (preg_match('/^\s*\w+\s*$/i', $did1)) { // [dealid]
                                        $sSspId=$aData[2];
                                        $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("#", "_", $sSspId)."|".$aData[1]);
// var_dump($aData);
                                    }else{
                                        return $this->doErrorMsg($nLineIndex, "sspid should be string like [deal_id] '1234567'", $aData[2], 3);
                                    }

                                break;


                                case LAF_ADSOURCETYPE_SSP_PPTV:
                                    $sSspId=$aData[2];
                                    $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("|", "_", $aData[2])."|".$aData[1]."|".$aData[6]);
                                break;
                                case LAF_ADSOURCETYPE_SSP_PPTV2:
                                    $sSspId=$aData[2];
                                    $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("|", "_", $aData[2])."|".$aData[1]."|".$aData[6]);
                                break;
                                case LAF_ADSOURCETYPE_SSP_ACROSS:
                                    $sSspId=trim($aData[2]);
                                    if (!preg_match('/^\d+$/i', $sSspId)) {
                                        return $this->doErrorMsg($nLineIndex, "sspid should be  numbers like '123456'", $aData[2], 3);
                                    }
                                    $aRtbData = $this->oFrameworker->sm->get('\Lur\Service\Across')->getRtbDataByTagId($sSspId);
                                    if (empty($aRtbData)) {
                                        return $this->doErrorMsg($nLineIndex, "no such tagid ", $aData[2], 3);
                                    }

                                    $aData[2] = "http://www.baidu.com/s?wd=".urlencode("ssp|".$aData[13]."|".str_replace("|", "_", $sSspId)."|".$aData[6]);
                                break;


                                default:
                                    return $this->doErrorMsg($nLineIndex, "unavailable ssp type ", $aData[13], 14);
                                    break;
                            }
                            if (!($aData[20]>=0 && $aData[20]<=1)) {
                                return $this->doErrorMsg($nLineIndex, "unavailable ctr", $aData[20], 21);
                            }
                        }



                        // 判断hunan cxid是否正确
                        $sFieldCxid = '';
                        $aData[23] = trim($aData[23]);

                        if (LAF_ADSOURCETYPE_SSP_SOHU==$aData[13]){
                            // 
                            $sFieldCxid = $aData[23];
                            if (!isset($sohusspvail['cxid']) && !empty($sFieldCxid)){
                                $sohusspvail['cxid'] = $sFieldCxid;
                            }
                            if (isset($sohusspvail['cxid']) && $sohusspvail['cxid']!=$sFieldCxid){
                                return $this->doErrorMsg($nLineIndex, "一个排期只允许一个cxid", $aData[22], 23);
                            }



                            list($t1, $t2) = explode(",", $sSspId);
                            if(time()>strtotime("2021-04-08 19:00:00")){
                                $t2 = trim($t2);
                                $oldlist = ["3069-407",
                                    "12-329",
                                    "5-930",
                                    "11-408",
                                    "11-406",
                                    "11-046",
                                    "3069-404",
                                    "11-407",
                                    "11-498",
                                    "9-331",
                                    "3069-408",
                                    "2048-408"];
                                $plannoold = $sohusspvail['cxid']."-".substr(trim($t2), 0, 3);

                                if(in_array($plannoold, $oldlist)){
                                    $planid = substr(trim($t2), 0, 3);
                                }else{
                                    if(!preg_match("/^\d{13}$/i", $t2)){
                                        return $this->doErrorMsg($nLineIndex, "代码id应为13个数字,如2021040810001", $t2, 2);
                                    }
                                    $planid = substr(trim($t2), 0, 10);
                                }

                            }else{
                                $planid = substr(trim($t2), 0, 3);
                            }
                            if (!isset($sohusspvail['planid']) && !empty($sSspId)){
                                $sohusspvail['planid'] = $planid;
                            }

                            if (isset($sohusspvail['planid']) && $sohusspvail['planid']!=$planid){
                                return $this->doErrorMsg($nLineIndex, "一个排期只允许一个排期", $sSspId, 2);
                            }
                            $planno = $sohusspvail['cxid']."-".$planid;


                        }elseif (LAF_ADSOURCETYPE_SSP_HUNAN==$aData[13]){
                            if(!empty($aData[23])) {
                                $sCxidTmp = $aData[23];
                                $sCxidTmp = \YcheukfCommon\Lib\Functions::fmtSplitString($sCxidTmp);
                                $aCxidTmp = explode(',', $sCxidTmp);
                                foreach ($aCxidTmp as $sTmp5) {
                                    if (!in_array($sTmp5, $this->aAppConfig['hunan_cxids'])) {
                                        // var_dump($this->aAppConfig['hunan_cxids']);
                                        // var_dump($sTmp5);
                                        return $this->doErrorMsg($nLineIndex, "wrong cxid ", $aData[23], 24);
                                    }
                                    
                                }
                                $sFieldCxid = $sCxidTmp;

                            }else{
                                $sFieldCxid = "6778_ry_0";
                            }
                            $aCxidTmp2 = explode(',', $sFieldCxid);
                            foreach ($aCxidTmp2 as $sTmp6) {
                            
                                $sParamsUrls = 'http://cx.da.mgtv.com/cx?cxid='.$sTmp6.'&plan='.$sSspId;
                                $sParamContents = file_get_contents($sParamsUrls);
                                if (preg_match('/param plan is unregistered/i', $sParamContents)) {
                                    $sTmp7 = "plan(".$sSspId.") is not enable with cxids ".$sTmp6;
                                    $aWarrings[md5($sTmp7)] = $sTmp7;

                                }
                                
                            }


                        }else{
                            $sFieldCxid = "";
                        }

                        $aData[8] = 200;
                        // $aData[9] = 1;
                        if (empty($aData[10])) {
                            $aData[10] = 1;
                        }

                        // 是否使用iac ip
                        $aData[24] = ($aData[24]==1) ? 1:0;


                        // sohu 的 vid
                        // sohu 的 use api uuid
                        $aData[25] = isset($aData[25]) ? trim($aData[25]) : "";
                        $aData[26] = isset($aData[26]) ? trim($aData[26]) : "";
                        if (LAF_ADSOURCETYPE_SSP_SOHU==$aData[13]){
                            if (!empty($aData[25]) ) {
                                $aMetadataList = array();
                                $aMetadataList[1025] = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, 1025);
                                $shvids = explode(",", $aData[25]);
                                foreach ($shvids as $vidtmp) {
                                    $vidtmp = trim($vidtmp);
                                    if (!isset($aMetadataList[1025][$vidtmp])) {
                                        return $this->doErrorMsg($nLineIndex, "wrong shvid ", $vidtmp, 25);
                                    }
                                }
                            }
                            if (!empty($aData[26]) ) {

                                if (!in_array($aData[26], [1,2, 3])) {
                                    // var_dump($this->aAppConfig['hunan_cxids']);
                                    // var_dump($sTmp5);
                                    return $this->doErrorMsg($nLineIndex, "wrong apiuuid ", $aData[26], 27);
                                }
                            }
                        }

                        /* 不再使用媒资id 2018-05-23 10:16:46
                        $aData[21] = trim($aData[21]);
                        if (!empty($aData[21])) {
                            if (preg_match('/^\[/i', $aData[21])) {
                                $aSspMediaIds = json_decode($aData[21]);
                            }else{
                                $aSspMediaIds = array($aData[21]);
                            }
                            if (is_null($aSspMediaIds) || count($aSspMediaIds)<1) {        
                               return $this->doErrorMsg($nLineIndex, "unavailable mzid json-format", $aData[21]);
                            }
                            if (is_array($aSspMediaIds) && count($aSspMediaIds)) {
                                foreach ($aSspMediaIds as $aRowTmp1) {
                                    if (!preg_match("/\d+/i", $aRowTmp1)) {
                                       return $this->doErrorMsg($nLineIndex, "unavailable mzid", $aRowTmp1);
                                    }
                                }
                            }
                        }
                        if (empty($aSspMediaIds)) {
                            // 忘记这是干什么的了, 随便写几个数字
                            $aSspMediaIds=array('4375199', '4358204');
                           // return $this->doErrorMsg($nLineIndex, "mzid can not be empty", $aData[21]);
                        }
                        */


                        //     return $this->doErrorMsg($nLineIndex, "unavailable mzid", $aData[21]);
                        // }else{

                        $aSspIds[$sSspId] = $sSspId.(!empty($sFieldCxid)?";cxid=".$sFieldCxid:"").(!empty($aData[21])?";".$aData[21]:"");
                    }

                    // if (empty($aData[2])) {//show为空, 则补全代码
                    //     $aData[2] = "http://www.baidu.com/s?wd=".uniqid();
                    //     if (empty($aData[10])) {
                    //         $aData[10] = 1;
                    //     }
                    //     $aScheduleData['show'] = array(array(
                    //         date("Y-m-d"),
                    //         1,
                    //         10,
                    //         1
                    //     ));
                    //     // return $this->doErrorMsg($nLineIndex, "show code can not be empty", $aData[2]);
                    // }



                    // 屏蔽旧逻辑, 使用随机指定的adr truck
                    // $aAdrServerIds = ($this->getRandAdrServerIds($aScheduleShowDate2Count));
                    // if (count($aAdrServerIds) <= 1) {
                    //     $aAdrServerIds = $this->getDefaultAdrServerIds();
                    // }

                    $aAdrServerIds = $this->getActiveAdrServerIds();
                    if (empty($aAdrServerIds)) {
                        return $this->doErrorMsg($nLineIndex, "can not match any adrserver:".json_encode($aScheduleShowDate2Count));
                    }


                    //show click uv 同时为空, 不做处理
                    if (empty($aData[2]) && empty($aData[3])  && empty($aData[16])) {
                        return $this->doErrorMsg($nLineIndex, "either show/click/uv was empty");
                    }
                    //check available
                    if ($aData[10]==1 && empty($aScheduleData['vpc']) && empty($aScheduleData['show']) && empty($aScheduleData['click']) && empty($aScheduleData['uv']) ) {
                        // return $this->doErrorMsg($nLineIndex, "empty schedule", $aData);
                        $aData[10]=2;
                    }
                    if ((intval($aData[0])) < 1) {
                        return $this->doErrorMsg($nLineIndex, "empty customer", $aData);
                    }

                    $sSql3 = "select * from b_adcustomer where id=".intval($aData[0]);
                    $oPDOStatement3 = $this->oFrameworker->queryPdo($sSql3);
                    if($oPDOStatement3 && $oPDOStatement3->rowCount()){
                        foreach($oPDOStatement3 as $result3) {

                            //判断该客户是否有操作权限
                            if ($result3['user_id'] != $sUserOpId) {
                                return $this->doErrorMsg($nLineIndex, "illegal action, no such customer ".$result3['name']);
                                // return $this->doErrorMsg($nLineIndex, "illegal action, this customer ".$result3['name']." did not belong to the user ".$sUserOpId);
                            }
                            $aCustomerList[$result3['id']] = $result3['id']."/".$result3['name'];
                        }
                    }else{
                        return $this->doErrorMsg($nLineIndex, "no such customer", $aData[0], 1);
                    }



                    if (!in_array($aData[13], $aSspTypes) && (intval($aData[8])) < 1) {
                        return $this->doErrorMsg($nLineIndex, "empty ip frequence", $aData);
                    }
                    if (!in_array($aData[13], $aSspTypes) && (intval($aData[9])) < 1) {
                        return $this->doErrorMsg($nLineIndex, "empty cookie frequence", $aData, 10);
                    }
                    // if (!empty($aData[4]) && is_null(json_decode($aData[4]))) {
                    //     return $this->doErrorMsg($nLineIndex, "unavailable json", $aData);
                    // }
                    // if (empty($aData[4])) {
                    //     return $this->doErrorMsg($nLineIndex, "unavailable service", $aData[4]);
                    // }
                    if (!empty($aData[6]) && is_null(json_decode($aData[6]))) {
                        return $this->doErrorMsg($nLineIndex, "unavailable json", $aData, 7);
                    }
                    if (!empty($aData[2]) && filter_var(\YcheukfCommon\Lib\Functions::replaceAdrTag($aData[2]), FILTER_VALIDATE_URL) === FALSE) {
                        return $this->doErrorMsg($nLineIndex, "unavailable show code", \YcheukfCommon\Lib\Functions::replaceAdrTag($aData[2]));
                    }           
                    if (!empty($aData[3]) && filter_var(\YcheukfCommon\Lib\Functions::replaceAdrTag($aData[3]), FILTER_VALIDATE_URL) === FALSE) {
                        return $this->doErrorMsg($nLineIndex, "unavailable click code", \YcheukfCommon\Lib\Functions::replaceAdrTag($aData[3], 4));
                    }
                    if (!empty($aData[16])) {
                        if (is_null(json_decode($aData[16]))) {
                            return $this->doErrorMsg($nLineIndex, "unavailable uv json", \YcheukfCommon\Lib\Functions::replaceAdrTag($aData[16], 17));
                        }
                    }



                    if (!empty($aData[10]) && !in_array(intval($aData[10]), array(1,2,3))) {
                        return $this->doErrorMsg($nLineIndex, "unavailable show_activity", $aData[10], 11);
                    }     
                    if (!empty($aData[11]) && !in_array(intval($aData[11]), array(1,2,3))) {
                        return $this->doErrorMsg($nLineIndex, "unavailable click_activity", $aData[11], 12);
                    }
                    if (!empty($aData[12]) && (intval($aData[12])<0 || intval($aData[12])>100)) {
                        return $this->doErrorMsg($nLineIndex, "stable cookie percent should between 0 to 100", $aData[12], 13);
                    }

                    if (!empty($aData[14]) && is_null(json_decode($aData[14]))) {
                        return $this->doErrorMsg($nLineIndex, "unavailable json", $aData[14], 15);
                    }


                    // show/click 代码所使用的投放方式, 必须是 web/sdk的两者之一
                    $nShowClickType = in_array($aData[13], array_merge(array(LAF_ADSOURCETYPE_WEB, LAF_ADSOURCETYPE_SDK, LAF_ADSOURCETYPE_UV), $aSspTypes)) ? $aData[13] : LAF_ADSOURCETYPE_WEB;
                    

                    if (!empty($aData[3]) && empty($aScheduleData['click'])) {
                        return $this->doErrorMsg($nLineIndex, "click code without any click-schedule", "", 4);
                    }     
                    if ($aData[10]==1 && !empty($aData[2]) && (empty($aScheduleData['show']) && empty($aScheduleData['vpc']) && empty($aScheduleData['uv']))) {
                        return $this->doErrorMsg($nLineIndex, "show code without any show-schedule", "", 3);
                    }     
                    if (!empty($aData[16]) && empty($aScheduleData['uv'])) {
                        return $this->doErrorMsg($nLineIndex, "uv code without any uv-schedule", "", 17);
                    }     

                    //带UV的导入模板
                    $bUvTmpltureFlag = !empty($aData[16]) ? true : false;
                    if ($bUvTmpltureFlag && !empty($aData[19]) && !in_array(intval($aData[19]), array(1,2,3))) {
                        return $this->doErrorMsg($nLineIndex, "unavailable uv_activity", $aData[19], 20);
                    }

                    if (!empty($aData[6])) {

                        $aIdsTmp = empty($aData[6]) ? array() : json_decode($aData[6], 1);
                        $aIdsTmp = is_array($aIdsTmp) ? $aIdsTmp : array($aIdsTmp);

                        $aNewTmp = array();
                        if(count($aIdsTmp)) {
                            foreach ($aIdsTmp as $sIdTmp2) {
                                $aNewTmp[] = $this->_getMapCityId($sIdTmp2);
                            }                            
                        }
                        $aData[6] = json_encode($aNewTmp);
                    }
                    $bFlagTmp = $this->chkListAvailable($nLineIndex, 1007, $aData[6]);
                    if ($aData[10]==1 && !$bFlagTmp) {
                        return $this->doErrorMsg($nLineIndex, "no such city in database", $aData[6], 7);
                    }
                    if (LAF_ADSOURCETYPE_SSP_SOHU==$aData[13] && is_array($aData[6]) && count($aData[6])>=2) {
                        return $this->doErrorMsg($nLineIndex, "被指定的城市不能超过1个", $aData[6], 7);
                    }




                    $bFlagTmp =  $this->chkListAvailable($nLineIndex, 1015, $aData[14]);
                    if (!$bFlagTmp) {
                        $atmp2 = $this->doErrorMsg($nLineIndex, "no such app in database", $aData[14], 15);
                        return $atmp2;
                    }

                    //referer check
                    $aReferfer = $aData[7];
                    if (empty($aData[7])) {
                        $aReferfer = array();
                    }else{
                        if (preg_match('/^\[/i', $aData[7])) {
                            $aReferfer = json_decode($aData[7]);
                            if (is_null($aReferfer)) {        
                               return $this->doErrorMsg($nLineIndex, "unavailable referer json-format", $aData[7], 8);
                            }
                            foreach ($aReferfer as $v3) {
                                if (filter_var($v3, FILTER_VALIDATE_URL) === FALSE) {
                                    return $this->doErrorMsg($nLineIndex, "unavailable referer url-format", $aData[7]);
                                }
                            }

                        }else{
                            if (filter_var($aData[7], FILTER_VALIDATE_URL) === FALSE) {
                                return $this->doErrorMsg($nLineIndex, "unavailable referer url-format", $aData[7]);
                            }
                        }
                    }

                    $nM102Id = empty($aData[10]) ? 1 : $aData[10];
                    $nProccedCount++;
                    if (intval($nM102Id) === 1)  $nProccedCount_active++;
                    else $nProccedCount_inactive++;
                    $aShow4Click = array();

                    if (!empty($aData[2]) && !empty($aScheduleData['show'])) {
                        $aCodes = $this->splitRequestUrl($aData[2], $aData[13]);
                        foreach ($aCodes as $split_index => $sCodeTmp) {
                            if ($split_index > 0) {//split-url 检查
                                 $bIgnoreFlag = $this->getDomainIgnoreStatus($sCodeTmp);
                                if ($bIgnoreFlag) {
                                    $nM102Id = 2;
                                }
                            }
                            $aEntity = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', trim($sCodeTmp), 'code');
                            $sEntityId = isset($aEntity['id']) ? $aEntity['id'] : "";
                            $nUserIdRecord = $aData[0];


                            $aTmpRow = array(
                                'id' => $sEntityId,
                                'aCodeRelations' => $aShow4Click,//嵌套关联的显示代码
                                'relation___1007' => empty($aData[6]) ? array() : json_decode($aData[6]),//city
                                'relation___1008' => empty($aData[5]) ? array() : json_decode($aData[5]),//ua
                                'relation___1015' => empty($aData[14]) ? array() : json_decode($aData[14]),//apps
                                'user_id' => $aData[0],
                                'op_user_id' => $sUserOpId,
                                'code' => $sCodeTmp,
                                'split_father_url' => $aData[2], 
                                'split_index' => $split_index,
                                'code_label' => preg_match("/show/i", $aData[1]) ? $aData[1] : $aData[1]."-show",
                                'referer' => json_encode($aReferfer),
                                'm102_id' => $nM102Id,
                                'm1014_id' =>  $nShowClickType,
                                'm1023_id' =>  $aData[22],
                                'sspctr' =>  $aData[20],
                                'sspmzid' =>  $aData[21],

                                'stable_cookie_percent' => empty($aData[12]) ? 0 : intval($aData[12]),
                                'domaincookie_delete_percent' => empty($aData[15]) ? 0 : intval($aData[15]),
                                'schedule' => json_encode($aScheduleData['show']),
                                'sspid' => $sSspId,
                                'hunan_cxids' => $sFieldCxid,
                                'rtbdata' => json_encode($aRtbData),
                                'useiac' => $aData[24],
                                'sh_vid' => $aData[25],
                                'sh_useapiuuid' => $aData[26],

                                'uv2ndurl' => $aData[17],
                                'uv2_percent' => $aData[18],


                            );
                            if (empty($sEntityId)) {
                                $aTmpRow['created_time'] = date('Y-m-d H:i:s');
                                $aTmpRow['relation___1009'] = $aAdrServerIds;//server
                            }else{
                                // $aM1009 = \Application\Model\Common::getRelationList($this->oFrameworker->sm,1009, $sEntityId, false, true);
                                // if (empty($aM1009) ||  count($aAdrServerIds) > 1) { //没有选择服务器 || 大于1个server时才需要在更新的时候重新选择服务器
                                //     $aTmpRow['relation___1009'] = $aAdrServerIds;//server
                                // }
                            }
                            $aDbData['show'][] = $aTmpRow;
                            $aShow4Click[] = array(
                                'cookie_domain' => $this->oFrameworker->sm->get('\Lur\Service\Common')->getCookieDomain($sCodeTmp),
                                'code' => $sCodeTmp,
                            );
                        }
                    }
                    
                    //click
                    $aClick4Uv = array();
                    if (!empty($aData[3]) && !empty($aScheduleData['click'])) {

                        $nM102IdClick = empty($aData[11]) ? 1 : $aData[11];
                        $nProccedCount++;

                        if (intval($nM102IdClick) === 1)  $nProccedCount_active++;
                        else $nProccedCount_inactive++;

                        $aCodes = $this->splitRequestUrl($aData[3], $aData[13]);
                        foreach ($aCodes as $split_index => $sCodeTmp) {
                            if ($split_index > 0) {//split-url 检查
                                 $bIgnoreFlag = $this->getDomainIgnoreStatus($sCodeTmp);
                                if ($bIgnoreFlag) {
                                    $nM102IdClick = 2;
                                }
                            }
                            $aEntity = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', $sCodeTmp, 'code');
                            $sEntityId = isset($aEntity['id']) ? $aEntity['id'] : "";  


                            $aTmpRow = array(
                                'id' => $sEntityId,
                                // 'cookie_code_url' => $aData[2],//关联显示代码
                                'aCodeRelations' => $aShow4Click,//嵌套关联的显示代码
                                'relation___1007' => empty($aData[6]) ? array() : json_decode($aData[6]),//city
                                'relation___1008' => empty($aData[5]) ? array() : json_decode($aData[5]),//ua
                                'relation___1015' => empty($aData[14]) ? array() : json_decode($aData[14]),//apps
                                'user_id' => $aData[0],
                                'op_user_id' => $sUserOpId,
                                'code' => $sCodeTmp,
                                'split_father_url' => $aData[3], 
                                'split_index' => $split_index,
                                'code_label' => preg_match("/click/i", $aData[1]) ? $aData[1] :$aData[1]."-click",
                                'referer' => json_encode($aReferfer),
                                'm102_id' => $nM102IdClick,
                                'm1014_id' => $nShowClickType,
                                'm1023_id' =>  $aData[22],
                                'stable_cookie_percent' => empty($aData[12]) ? 0 : intval($aData[12]),
                                'domaincookie_delete_percent' => empty($aData[15]) ? 0 : intval($aData[15]),
                                'schedule' => json_encode($aScheduleData['click']),
                                'sspid' => $sSspId,
                                'hunan_cxids' => $sFieldCxid,
                                'rtbdata' => json_encode($aRtbData),

                                'useiac' => $aData[24],
                                'sh_vid' => $aData[25],
                                'sh_useapiuuid' => $aData[26],

                            );
                            if (empty($sEntityId)) {
                                $aTmpRow['created_time'] = date('Y-m-d H:i:s');
                                $aTmpRow['relation___1009'] = $aAdrServerIds;//server
                            }else{
                                // $aM1009 = \Application\Model\Common::getRelationList($this->oFrameworker->sm,1009, $sEntityId, false, true);
                                
                                // if (empty($aM1009) ||  count($aAdrServerIds) > 1) { //大于1个server时才需要在更新的时候重新选择服务器
                                //     $aTmpRow['relation___1009'] = $aAdrServerIds;//server
                                // }
                            }
                            
                            $aDbData['click'][] = $aTmpRow;

                            $aClick4Uv[] = array(
                                'cookie_domain' => $this->oFrameworker->sm->get('\Lur\Service\Common')->getCookieDomain($sCodeTmp),
                                'code' => $sCodeTmp,
                            );
                        }
                    }else{
                        $aDbData['click'][] = array();
                    }
                    
                    //uv 的导入
                    if ($bUvTmpltureFlag && !empty($aData[16]) && !empty($aScheduleData['uv'])) {

                        $nM102IdUV1 = empty($aData[19]) ? 1 : $aData[19];
                        $nProccedCount++;

                        if (intval($nM102IdUV1) === 1)  $nProccedCount_active++;
                        else $nProccedCount_inactive++;

                        $aCodes = array($aData[16]);
                        foreach ($aCodes as $split_index => $sCodeTmp) {
                            $aEntity = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', $sCodeTmp, 'code');
                            $sEntityId = isset($aEntity['id']) ? $aEntity['id'] : "";  


                            $aTmpRow = array(
                                'id' => $sEntityId,
                                // 'cookie_code_url' => $aData[2],//关联显示代码
                                'aCodeRelations' => $aClick4Uv,//嵌套关联的显示代码
                                'relation___1007' => empty($aData[6]) ? array() : json_decode($aData[6]),//city
                                'relation___1008' => empty($aData[5]) ? array() : json_decode($aData[5]),//ua
                                'relation___1015' => empty($aData[14]) ? array() : json_decode($aData[14]),//apps
                                'user_id' => $aData[0],
                                'op_user_id' => $sUserOpId,
                                'code' => $sCodeTmp,
                                'split_father_url' => $sCodeTmp, 
                                'split_index' => $split_index,
                                'code_label' => preg_match("/uv/i", $aData[1]) ? $aData[1] :$aData[1]."-uv",
                                'referer' => json_encode($aReferfer),
                                'm102_id' => $nM102IdUV1,
                                'm1014_id' => LAF_ADSOURCETYPE_UV,//uv类型
                                'm1023_id' =>  $aData[22],
                                'stable_cookie_percent' => empty($aData[12]) ? 0 : intval($aData[12]),
                                'domaincookie_delete_percent' => empty($aData[15]) ? 0 : intval($aData[15]),
                                'schedule' => json_encode($aScheduleData['uv']),
                                'sspid' => $sSspId,
                                'hunan_cxids' => $sFieldCxid,
                                'rtbdata' => json_encode($aRtbData),

                                'useiac' => $aData[24],
                                'sh_vid' => $aData[25],
                                'sh_useapiuuid' => $aData[26],
                                'uv2ndurl' => $aData[17],
                                'uv2_percent' => $aData[18],

                            );
                            if (empty($sEntityId)) {
                                $aTmpRow['created_time'] = date('Y-m-d H:i:s');
                                $aTmpRow['relation___1009'] = $aAdrServerIds;//server
                            }else{
                                // $aM1009 = \Application\Model\Common::getRelationList($this->oFrameworker->sm,1009, $sEntityId, false, true);
                                
                                // if (empty($aM1009) ||  count($aAdrServerIds) > 1) { //大于1个server时才需要在更新的时候重新选择服务器
                                //     $aTmpRow['relation___1009'] = $aAdrServerIds;//server
                                // }
                            }
                            
                            $aDbData['uv'][] = $aTmpRow;
                        }
                    }else{
                        $aDbData['uv'][] = array();
                    }


                }
            }
            // var_dump($aDbData);
            // exit;
            
            $aExists = array();
            foreach ($aDbData as $k3 => $aTmp4) {

                //uv2与uv统一处理
                if ($k3 === 'uv2') {
                    continue;
                }

                if (count($aTmp4)) {

                    //检查代码中是否有重复
                    foreach ($aTmp4 as $key => $a3) {
                        if (count($a3) < 1) {
                            continue;
                        }
                        $sMd5Tmp = md5(strtoupper($a3['code']));
                        if (isset($aExists[$sMd5Tmp])) {
                            return $this->doErrorMsg($key, "mutiple code", $a3['code']);
                        }
                        $aExists[$sMd5Tmp] = true;
                    }


                    foreach ($aTmp4 as $key => $a3) {

                        if (count($a3) < 1) {
                            $aDbIds[$k3][] = new stdClass();
                        }else{


                            $a3['cookie_code_id'] = 0;
                            //将点击的urlid与显示的url id关联, 只关联同一个域名下的, 为了解决嵌套代码的关联问题, show/click 只关联一个code id
                            if (in_array($k3, array("show", "click")) && isset($a3['aCodeRelations']) && !empty($a3['aCodeRelations'])) {
                                $nCookieCodeId = 0;
                                // var_dump($a3['aCodeRelations']);
                                foreach ($a3['aCodeRelations'] as $aTmpRow2) {
                                    if ($aTmpRow2['cookie_domain'] === $this->oFrameworker->sm->get('\Lur\Service\Common')->getCookieDomain($a3['code'])) {
                                        $aCookieCodeId = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', trim($aTmpRow2['code']), 'code');
                                        $nCookieCodeId = isset($aCookieCodeId['id']) ? $aCookieCodeId['id'] : "";
                                        break;
                                    }
                                }
                                $a3['cookie_code_id'] = empty($nCookieCodeId)?0:$nCookieCodeId;
                                unset($a3['aCodeRelations']);
                            }elseif (in_array($k3, array("uv")) && isset($a3['aCodeRelations']) && !empty($a3['aCodeRelations'])) {// 将uv代码与 click代码关联, 一个uv代码关联多个click代码

                                $nCookieCodeId = 0;
                                // var_dump($a3['aCodeRelations']);
                                if (!isset($a3['relation___'.LAF_RELATIONTYPE_UV2CLICK])) {
                                    $a3['relation___'.LAF_RELATIONTYPE_UV2CLICK] = array();
                                }
                                foreach ($a3['aCodeRelations'] as $aTmpRow2) {
                                    $aCookieCodeId = \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', trim($aTmpRow2['code']), 'code');
                                    $a3['relation___'.LAF_RELATIONTYPE_UV2CLICK][] = isset($aCookieCodeId['id']) ? $aCookieCodeId['id'] : "";
                                }

                                unset($a3['aCodeRelations']);
                            }



                            if (isset($a3['split_father_url']) && !empty($a3['split_father_url'])) {
                                // var_dump($a3['split_father_url'],$a3['code']);
                                $aSplitFather = ($a3['split_father_url']===$a3['code']) ? array() : \YcheukfCommon\Lib\Functions::getResourceById($this->oFrameworker->sm, 'adschedule', trim($a3['split_father_url']), 'code');
                                $nTmpId = isset($aSplitFather['id']) ? $aSplitFather['id'] : 0;
                                // var_dump($nTmpId);

                                $a3['split_fid'] = $nTmpId;
                                unset($a3['split_father_url']);
                            }
                            
                            $aPost = array('params' => json_encode($a3));


                            $sActionReturn = $oContentController->contenteditAction($a3['id'], "", "", "adschedule", $aPost);
// var_dump($sActionReturn);

                            $aScheduleIds[] = $sActionReturn;
                            $aTmp3 = array();
                            $aTmp3['sActionReturn'] = $sActionReturn;
                            $aTmp3['postParams'] = $a3;
                            if ($a3['split_index'] == 0) {
                                $aDbIds[$k3][] = array(
                                    'id' => $sActionReturn,
                                    'url' => $a3['code'],
                                );
                            }

                        }
                        // echoMsg('['.__CLASS__.'] [post check params]: '.json_encode($aPost));
                        // echoMsg('['.__CLASS__.'] [post check return]: '.json_encode($aTmp3));
                    }
                }
            }
        }

        $sInactiveHTML = empty($nProccedCount_inactive) ? $nProccedCount_inactive : "<font color=red>".$nProccedCount_inactive."</font>";

        $sSspIdHtml = empty($aSspIds) ? "" : "\n SSP排期:".print_r(array_values($aSspIds), 1);

        $sWarringMsg = '';
        if (count($aWarrings)) {
            $sWarringMsg = "\n<font color=red>warring: ".join(",", $aWarrings)."</font>";
        }


        $eparams = array_unique($eparams);
        return array(true, 
            'filetype:'.$aMetadataList[1014][$nShowClickType]
            ."\n".'finished/enable/disable:'.$nProccedCount."/".$nProccedCount_active."/".$sInactiveHTML
            ."\n".'users:'.implode(";", $aCustomerList)
            ."\n".'eparams:'.implode(";", $eparams)
            ."\n".'planno:'.$planno
            .$sSspIdHtml
            .$sWarringMsg
            ."\n"
            , $nUserIdRecord
            , json_encode($aDbIds)
            , $aScheduleIds
            , $planno
        );
    }
   function doErrorMsg($nLineIndex, $sMsg, $data=null, $nDateIndex=null){
        $sMessage = "line:".$nLineIndex.(!is_null($nDateIndex) ? "\ncolumn:".$nDateIndex: "")."\nerror:".$sMsg.":\n".(is_array($data) ? json_encode($data) : $data);
        echoMsg($sMessage);
        return array(false, \YcheukfCommon\Lib\Functions::iconv2Utf8($sMessage));
   } 


   function getrandomindex()
   {
       return uniqid();
   }

    public function getDomainIgnoreStatus($url='')
    {
         $aUrlInfo = parse_url($url);
         if (!isset($aUrlInfo['host'])) {
             return $this->doErrorMsg(__FUNCTION__, "wrong url", $aUrlInfo);

             return false;
         }

         $bIgnoreFlag = false;
        $aIgnoreDomains = isset($this->aConfig['adserver_ignore_domains']) ? $this->aConfig['adserver_ignore_domains'] : array();
        $aIgnoreUrls = isset($this->aConfig['adserver_ignore_urls']) ? $this->aConfig['adserver_ignore_urls'] : array();


        if (count($aIgnoreDomains)) {
            foreach ($aIgnoreDomains as $key => $value) {
                if (preg_match('/'.preg_quote($value, "/").'/i', $aUrlInfo['host'])) {
                    $bIgnoreFlag = true;
                    break;
                }
            }
        }
        if ($bIgnoreFlag === false && count($aIgnoreUrls)) {
            foreach ($aIgnoreUrls as $key => $value) {
                if (preg_match('/^'.preg_quote($value, "/").'/i', $url)) {
                    $bIgnoreFlag = true;
                    break;
                }
            }
        }
        return $bIgnoreFlag;
   }

   public function splitRequestUrl($sUrl, $sSourceType)   
   {   
        $aSspTypes = $this->aAppConfig['ssp_platforms'];
        if (in_array($sSourceType, $aSspTypes)) {
            $aReturn[] = $sUrl;
        }else{
            $aMd5KeyData = \YcheukfCommon\Lib\Functions::getLafCacheData($this->oFrameworker->sm, 101, trim($sUrl));
            if (count($aMd5KeyData)) {
                return $aMd5KeyData['data'];
            }
           $aReturn = array();
           $aReturn[] = $sUrl;
           $bSaveFlag = true;
           foreach($aReturn as $sUrlTmp){
               $aInfo = $this->curl_core_func($sUrlTmp);
               if ($aInfo === false) {
                    $bSaveFlag = false;
                   continue;
               } else {    
                    while (in_array($aInfo['http_code'], array(301,302))) {
                       if (!empty($aInfo['redirect_url'])) {
                            $bIgnoreFlag = $this->getDomainIgnoreStatus($aInfo['redirect_url']);
                           if ($bIgnoreFlag) {//是不需要跳转的域名
                                echoMsg("ignore url:".$aInfo['redirect_url']);
                               break;
                           }
                           if ($this->aConfig['nFlowRedirectLevel'] < count($aReturn)) {//到达层级深度
                               break;
                           } 
                           
                           $aReturn[] = $aInfo['redirect_url']."[adrsplitid=".($this->getrandomindex())."]";
                           $aInfo = $this->curl_core_func($aInfo['redirect_url']);
                           if ($aInfo === false) {
                               break;
                           }
                           
                       }else{
                           break;
                       }
                   }
               }            
           }
           if ($bSaveFlag) {
               \YcheukfCommon\Lib\Functions::saveLafCacheData($this->oFrameworker->sm, 101, trim($sUrl), $aReturn);
           }

        }
       return $aReturn;


   }

   /**
    * 请求代码函数
    * @param  string $value [description]
    * @return [type]        [description]
    */
   public function curl_core_func($sUrl, $sUa="", $sProxyIp="", $sReffer="", $sCookieReadPath="", $sCookieWritePath="", $bContentFlag=0)
   {
       // $sProxyIp="";
       if ($this->aConfig['debug'] == 1) {
           $nStartTime = time();   
       }        
       $ch = curl_init();
       $sUrl = \YcheukfCommon\Lib\Functions::replaceAdrTag($sUrl);
       curl_setopt($ch, CURLOPT_URL, $sUrl);
       if(!empty($sUa))
           curl_setopt($ch, CURLOPT_USERAGENT, $sUa); 
       if(!empty($sProxyIp))
           curl_setopt($ch, CURLOPT_PROXY, $sProxyIp);
       if(!empty($sReffer)){
           if (is_array($sReffer)) {//若为referer数组, 随机一个
               $sReffer = $sReffer[rand(0, count($sReffer)-1)];
           }
           curl_setopt ($ch,CURLOPT_REFERER, $sReffer);
       }
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
       if(!empty($sCookieReadPath)){

           // echoMsg(array('sCookieReadPath'=>$sCookieReadPath, 'sCookieWritePath'=>$sCookieWritePath));
           curl_setopt($ch, CURLOPT_COOKIEFILE, $sCookieReadPath);
           curl_setopt($ch, CURLOPT_COOKIEJAR, $sCookieWritePath);   
       }
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_HEADER, 1);
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
       curl_setopt($ch, CURLOPT_TIMEOUT, 10);
       // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
       // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
       $sContent = curl_exec($ch);
       if(!curl_errno($ch)){ 
           $info = curl_getinfo($ch); 
           if ($bContentFlag) {
               $info['content'] = $sContent;
           }
           // print_r($info);
           curl_close($ch);  

           if ($this->aConfig['debug'] == 1) {
               $nExecTime = (time()-$nStartTime);
               if (($this->aConfig['nCurlSlowTime'] <= $nExecTime)) {
                   echoMsg(array('exec_second'=>$nExecTime, 'ip'=>$sProxyIp));
               }
           }
           return $info;      
       }else {
           if ($this->aConfig['debug'] == 1) {
               echoMsg('url:'.$sUrl.';Curl error: ' . curl_error($ch));
           }
           curl_close($ch);  
           return false;
       } 
   }


    public function updateTable($nStatus, $sResult, $sId, $processed_time=null,$nUserId=0, $sOutJson="", $aScheduleIds=array(), $planno='')
    {



        if (!is_null($processed_time)) {


            $aParams = array(
                'op'=> 'save',
                'resource'=> 'system_batch_jobs',
                'resource_type'=> 'common',
                'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->oFrameworker->sm),
                'params' => array(
                    'dataset'=>array(array(
                        'processed_time'=> intval($processed_time),
                        'out_text'=> $sResult,
                        'm158_id'=> $nStatus,
                        'user_id'=> (empty($nUserId)? 0 :$nUserId),
                        'out_json'=> $sOutJson,
                        'planno'=> $planno,
                    )),
                    'where' => array(
                        "id"=>$sId,
                    ),
                ),
            );

            \YcheukfCommon\Lib\Functions::saveRealtion($this->oFrameworker->sm, 1016, $sId, $aScheduleIds);

        }else{

            $aParams = array(
                'op'=> 'save',
                'resource'=> 'system_batch_jobs',
                'resource_type'=> 'common',
                'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->oFrameworker->sm),
                'params' => array(
                    'dataset'=>array(array(
                        'out_text'=> $sResult,
                        'planno'=> $planno,
                        'm158_id'=> $nStatus,
                        'user_id'=> (empty($nUserId)? 0 :$nUserId),
                    )),
                    'where' => array(
                        "id"=>$sId,
                    ),
                ),
            );

        }
        // var_dump($aParams);
        $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->oFrameworker->sm, $aParams);


        echoMsg('['.__CLASS__.'] jobid/status :'.$sId."/".$nStatus);
    }


    public function getActiveAdrServerIds(){
        $nActivceTruckItem = \YcheukfCommon\Lib\Functions::getCache($this->oFrameworker->sm, LAF_CACHE_ONLINE_ADRTRUCK_ACTIVE_317);
        if ($nActivceTruckItem && isset($nActivceTruckItem['id'])) {
            return [$nActivceTruckItem['id']];
        }
        return [];
    }

    /**
     * 随机选择ADR服务器
     * 计算最近的N天(包含今天)内的每天平均投放量, 若该量大于每天平均投放的指导量, 则增加服务器
     *
     * @return array server ids
     * 
     */
    public function getRandAdrServerIds($aScheduleShowDate2Count)
    {


        // 计算动态分配ADR SERVER的因子
        // 用于计算 大于等于今天且最近N天的天数
        $nGuideLineDayCount = 3;
        // 每天平均投放量指导量
        $nGuideLineAdCountPerDay = 500000;
        $aReturn = array();
        ksort($aScheduleShowDate2Count);
        if (count($aScheduleShowDate2Count) < 1) {
            return array();
        }

        $aTmp5 = array();
        $nStartDateTimeSpan = strtotime(date("Y-m-d"));
        foreach ($aScheduleShowDate2Count as $sDateTmp => $nCount) {
            if (strtotime($sDateTmp)>=$nStartDateTimeSpan) {
                $aTmp5[$sDateTmp] = $nCount;
            }
            if (count($aTmp5) >= $nGuideLineDayCount) {
                break;
            }
        }
        if (count($aTmp5) < 1) {
            return array();
        }

        // n天内总量
        $nTotalTmp = array_sum($aTmp5);
        $nAverageCount = $nTotalTmp/count($aTmp5);
        $nServerCount = intval($nAverageCount / $nGuideLineAdCountPerDay) + 1;


        $aServerList = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, 1009);
        $nServerCount = $nServerCount >= count($aServerList) ? count($aServerList) : $nServerCount;
        $aTmp = array_rand($aServerList, $nServerCount);
        if (is_array($aTmp)) {
            $aReturn = $aTmp;
        }else{
            $aReturn = array($aTmp);
        }

        return $aReturn;
    }
    public function getDefaultAdrServerIds($nServerCount=1)
    {
        if (is_null($this->aDefaultAdrServerId)) {
            $aServerList = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, 1009);
            $aTmp = array_rand($aServerList, $nServerCount);
            if (is_array($aTmp)) {
                $aReturn = $aTmp;
            }else{
                $aReturn = array($aTmp);
            }
            $this->aDefaultAdrServerId = $aReturn;
        }
        return $this->aDefaultAdrServerId;
    }
    public function chkListAvailable($nLineIndex, $nType, $sIds)
    {
        $aIds = empty($sIds) ? array() : json_decode($sIds, 1);
        if(count($aIds)) {
            $aResource = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, $nType);
            foreach ($aIds as $sIdTmp) {
                if (!isset($aResource[$sIdTmp])) {
                    return false;
                }
            }                            
        }
        return true;
    }

    public function _chkDateFormat($nLineIndex, $sDate)
    {
        $sNewDate = $sDate;
        foreach ($this->aCodeType as $sCodeName) {
            $sNewDate = preg_replace("/\|\s*".$sCodeName."/i", "", $sNewDate);
        }
        $sNewDate = trim($sNewDate);
        list($sDateTmp2) = explode(" ", $sNewDate);
        // if (!preg_match("/^\d{4}-\d{2}-\d{2}/i", $sDateTmp2)) {
        //     return $this->doErrorMsg($nLineIndex, " 正确的日期格式为: 2020-10-10", $sDate." # ".$sDateTmp2);
        // }

        if(strtotime($sDateTmp2) !== false) {
        }else{
            return $this->doErrorMsg($nLineIndex, "unavailable date format", $sDate." # ".$sDateTmp2);
        }
        return true;

    }                                
    public function _fmtDateFormat($sDate)
    {
        list($sTmp1) = explode("|", $sDate);
        $sTmp1 = trim($sTmp1);
        list($sDateTmp) = explode(" ", $sTmp1   );
        return str_replace($sDateTmp, date("Y-m-d", strtotime($sDateTmp)), $sDate);
    }
}

