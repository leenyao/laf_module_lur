<?php
/*
从队列中读出更新缓存信号
清理过期的排期设置  

**/
class UpdateAdschedule extends \YcheukfCommon\Lib\Crondjob{


    // if ($k == 84992) { // 测试
    //     $oCacheObject->sAdd(LAF_CACHE_USEIACIP_ADSET_130, $k);
    // }else{
    //     $oCacheObject->sRem(LAF_CACHE_USEIACIP_ADSET_130, $k);
    // }

    function isbatchdone(){
        $return = false;
        $sql = 'select count(*) as total from `laf_lur`.`system_batch_jobs` where m158_id not in (10,20)';
        $oPDOStatement3 = $this->oFrameworker->queryPdo($sql);
        if($oPDOStatement3 && $oPDOStatement3->rowCount()>0){
            $return = true;
        }
        return $return;
    }

    function updateadrconfig(){

        $sUrl = \Application\Model\Common::fmtUrlWithDynamicToken('http://127.0.0.1/restapi.php/adrconfig?updatecacheflag=1', 42);

        $sReturn = file_get_contents($sUrl);
        echoMsg('['.__CLASS__.'] updateadrconfig:'.$sUrl);

    }

    function go(){

        if($this->isbatchdone() == false){
            echoMsg('['.__CLASS__.'] waiting system_batch_jobs');
            return true;
        }


        // \YcheukfCommon\Lib\Functions::sendMQ($this->oFrameworker->sm, date("Y-m-d"), "adschedule_update");
        $sLastUdateTime = \YcheukfCommon\Lib\Functions::getMQ($this->oFrameworker->sm, "adschedule_update");

            // echoMsg('['.__CLASS__.'] '."checking adschedule_update:".$sLastUdateTime);
        // $sLastUdateTime = true;

        

        if ($sLastUdateTime === false) {
        }else{
            \YcheukfCommon\Lib\Functions::delCache($this->oFrameworker->sm, LAF_CACHE_RESAPI_AD_SINGLE_132, true);

            $this->updateadrconfig();
            // \YcheukfCommon\Lib\Functions::delCache($this->oFrameworker->sm, 'restapi_adrconfig', true);

            $aM1009 = \Application\Model\Common::getResourceMetadaList($this->oFrameworker->sm, 1009);
            $aConfig = $this->oFrameworker->sm->get('config');
            $aTmp = array();
            $t1 = time();
            foreach($aM1009 as $k=>$v){
                $sTmp = $this->oFrameworker->sm->get('\Lur\Service\Common')->getScheduleByAdrId($k);
            }
            $t2 = time();
            echoMsg('['.__CLASS__.'] '."update adschedule: count=".count($aM1009).' '.$sLastUdateTime."(".($t2-$t1)."s)");
            \YcheukfCommon\Lib\Functions::saveCustomerlog($this->oFrameworker->sm, "更新缓存", 11);

            //清空队列中的所有信号
            while ($sLastUdateTime = \YcheukfCommon\Lib\Functions::getMQ($this->oFrameworker->sm, "adschedule_update")) {
                echoMsg('['.__CLASS__.'] '."clear adschedule:".$sLastUdateTime);
            }
        }


        // \YcheukfCommon\Lib\Functions::saveCache($this->getServiceLocator(), "lur_adschedule_lastupdate_time", date("Y-m-d H:i:s"), null);

        return true;
    }

}

