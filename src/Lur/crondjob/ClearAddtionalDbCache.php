<?php
/*
清理过期的排期设置  
清理过期的adr状态数据

**/
class ClearAddtionalDbCache extends \YcheukfCommon\Lib\Crondjob{



    function go(){

        $nParamIndex = 2;
        $sDate = null;
        if (isset($_SERVER['argv'][$nParamIndex])) {
            $sDate = $_SERVER['argv'][$nParamIndex];
            if (!preg_match("/^\d{8}$/i", $sDate)) {
                echo ("wrong param[".$nParamIndex."]:".$sDate."\n");
                exit;
            }
        }else{
            $sDate = date("Ymd");
        }
        $sAddtionalDbCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".$sDate;



        $aAlljobs = $this->oFrameworker->sm->get('\Lur\Service\Addtional')->getAllJobs();
        $oPdo = (\Application\Model\Common::getPDOObejct($this->oFrameworker->sm, "db_slave"));
        // print_r($aAlljobs);
        $aExists = array();
        if (count($aAlljobs)) {
            foreach ($aAlljobs as $nCityId => $aRow1) {
                foreach ($aRow1 as $sJobId => $nCount) {
                    if ($nCount > 100) {
                        list($nCodeId, $t1, $t2, $t3) = explode("-", $sJobId);
                        if (!isset($aExists[$nCodeId])) {
                            $aExists[$nCodeId] = $nCodeId;

                            $sSql = "select m102_id from b_adschedule where id=:id";
                            $sth = $oPdo->prepare($sSql);
                            $sth->execute(array(":id"=>$nCodeId));
                            $aFirstRow = $sth->fetch(\PDO::FETCH_ASSOC);
                            if (is_array($aFirstRow) && count($aFirstRow) && $aFirstRow['m102_id']!=1) {

                                $bFlag = $this->oFrameworker->sm->get('\Lur\Service\Common')->getAddtionalRedis()->hDel($sAddtionalDbCacheKey, $sJobId);

                               echoMsg('['.__CLASS__.'] id='.$nCodeId.",nCount=".$nCount);
                            }

                        }else{
                        }
                    }
                }
            }
        }

        return true;
    }
}

