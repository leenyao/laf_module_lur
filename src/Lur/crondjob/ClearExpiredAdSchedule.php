<?php
/*
清理过期的排期设置  
清理过期的adr状态数据

**/
class ClearExpiredAdSchedule extends \YcheukfCommon\Lib\Crondjob{



    function go(){


        $this->oFrameworker->sm->get('\Lur\Service\Report')->clickPercentReport(LAF_ADSOURCETYPE_SSP_HUNAN, date("Y-m-d", strtotime("-3 day")), date("Y-m-d"));

        $sSql = "select * from b_adschedule where m102_id=1";
		$oPDOStatement = $this->oFrameworker->queryPdo($sSql);

		if($oPDOStatement && $oPDOStatement->rowCount()){
			foreach($oPDOStatement as $result) {
				$aSchedule = json_decode(($result['schedule']), 1);
				if (is_null($aSchedule)) {
					$this->updateTable($result['id']);
				} else {
					if(strtotime($this->getLastDateFromSchedule($aSchedule)) < strtotime("-1 day")){
						$this->updateTable($result['id']);
					}
				}
			}
		}

        // echoMsg('['.__CLASS__.'] '.$sSql);


        /**
         * 删除过期的adr server 状态, 保留 X天数据
         */
        // $sSql = "select max(id) as mid from `b_adrstats`";
        // $oPDOStatement = $this->oFrameworker->queryPdo($sSql);

        // $nMaxAdrStatsId = 0;
        // if($oPDOStatement && $oPDOStatement->rowCount()){
        //     foreach($oPDOStatement as $result) {
        //         $nMaxAdrStatsId = $result['mid'];
        //     }
        // }
        // if (!empty($nMaxAdrStatsId)) {
            // $sSql = "delete from `b_adrstats` where created_time<'".date("Y-m-d", strtotime("-2 day"))." 00:00:00'";
            // $this->oFrameworker->queryPdo($sSql);
        // }



        /**
         * 删除过期的b_usstats 状态, 保留 10000条数据
         */
        // $sSql = "select max(id) as mid from `b_usstats`";
        // $oPDOStatement = $this->oFrameworker->queryPdo($sSql);

        // $nMaxAdrStatsId = 0;
        // if($oPDOStatement && $oPDOStatement->rowCount()){
        //     foreach($oPDOStatement as $result) {
        //         $nMaxAdrStatsId = $result['mid'];
        //     }
        // }
        // if (!empty($nMaxAdrStatsId)) {
            $sSql = "delete from `b_usstats` where created_time<'".date("Y-m-d", strtotime("-10 day"))." 00:00:00'";
        echoMsg('['.__CLASS__.'] '.$sSql);
            
            $this->oFrameworker->queryPdo($sSql);
        // }

        // echoMsg('['.__CLASS__.'] '.$sSql);

        return true;
    }
    public function getLastDateFromSchedule($aSchedule)
    {

    	$sReturn = "1970-01-01";
        foreach($aSchedule as $kTmp => $aRow2){
            $aTmp4 = explode(" ", $aRow2[0]);

            //扔掉过期的排期
            if(preg_match("/\,/", $aTmp4[0])){
                list($a123,$sLastScheduleDate) = explode(",", $aTmp4[0]);
            }else{
                $sLastScheduleDate = $aTmp4[0];
            }
            $sReturn = strtotime($sLastScheduleDate) > strtotime($sReturn) ? $sLastScheduleDate : $sReturn;
        }
		return $sReturn;    	
    }

    public function updateTable($sId)
    {

  //       $sSql = "update b_adschedule set m102_id=2 where m102_id=1 and id in (".$sId.")";
		// $oPDOStatement = $this->oFrameworker->queryPdo($sSql);

        $aParams = array(
            'op'=> 'save',
            'resource'=> 'adschedule',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->oFrameworker->sm),
            'params' => array(
                'dataset'=>array(array(
                    'm102_id'=> 2,
                )),
                'where' => array(
                    // "m102_id"=>1,
                    "id"=>$sId,
                ),
            ),
        );

        $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->oFrameworker->sm, $aParams);
		// echoMsg('['.__CLASS__.'] '.$sSql);
        

        //关掉代码-adr服务器关系

        $aParams = array(
            'op'=> 'save',
            'resource'=> 'relation',
            'resource_type'=> 'common',
            'access_token' => \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->oFrameworker->sm),
            'params' => array(
                'dataset'=>array(array(
                    'status'=> 2,
                )),
                'where' => array(
                    "type"=>1009,
                    "fid"=>$sId,
                ),
            ),
        );
        $aResult = \YcheukfCommon\Lib\Functions::myUpdate($this->oFrameworker->sm, $aParams);

        echoMsg('['.__CLASS__.'] '.$sId);
    }

}

