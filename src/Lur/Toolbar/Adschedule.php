<?php
namespace Lur\Toolbar;
class Adschedule extends \Application\Toolbar\Toolbar{
    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
        $aReturn = array();


        $nTypeTmp = 102;
        $aStatus = \Application\Model\Common::getResourceMetadaList($sm, $nTypeTmp);

        $oAuthService = ($sm->get('zfcuser_auth_service'));
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($sm, $oAuthService->getIdentity());
        //新增按钮
        if (in_array("user-member", $aRoles)) {
            $aReturn[] = $this->_getAddButton($oController, $sSource, $nId);

            $aReturn[] = array(
                'type' => 'text',
                'position' => 'left',
                'params' => array(
                    'label' => $oController->translate('状态操作:'),
                ),
            );
            foreach($aStatus as $nStatus=>$nLabel){
                $aReturn[] = $this->_getPublishButton($sm, $oController, $nTypeTmp, $sSource, $nStatus, null, 'm'.$nTypeTmp.'_id');
            }
            $aReturn[] = array(
                'type' => 'pipe',
            );
        }

        //下载
        $aReturn[] = array(
            'type' => 'detail-button-download',
            'cls' => 'download',
            'iconCls' => 'licon-download',
            'position' => 'left',
            'params' => array(
                'request' => array('returnType'=>'csv_all'),
                'label' => $oController->translate('download'),
            ),
        );
/*
        $aReturn[] = array(
            'type' => 'pipe',
        );
        //更新缓存
        $aReturn[] = array(
            'type' => 'ajax-button-action',
            'cls' => 'clear_adschedule_cache',
            'confirm' => $sm->get('translator')->translate('clear_adschedule_cache_confirmtext'),
            'position' => 'left',
//            'iconCls' => 'licon-publish_'.$nTypeTmp."_".$nStatus,
            'params' => array(
                'label' => '更新排期缓存',
                'action-url' => $oController->url()->fromRoute('zfcadmin/clearcache', array('cache_key'=>'restapi_adschedule_get_')),
            ),
            'permission' => array(),
        );
        //查看排期
        $aReturn[] = array(
            'type' => 'link',
            'position' => 'left',
//            'iconCls' => 'licon-publish_'.$nTypeTmp."_".$nStatus,
            'params' => array(
                'label' => '查看排期',
                'attributes' => array('target'=>'_blank'),
                'link' => $oController->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'viewschedule')),
            ),
            'permission' => array(),
        );
*/
        $aReturn[] = array(
            'type' => 'detail-br',
        );


        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('状态:'),
            ),
        );
        $aStatus =  $aStatus + array(0 =>$oController->translate("all"));
        // var_dump($aStatus);
        $aReturn[] = array(
            'type' => 'detail-select-action',
            'cls' => 'filter',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('过滤状态'),
                'value' => 1,
                'object_name' => 'm102_id',
                'options' => $aStatus,
            ),
        );

        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('url层级:'),
            ),
        );
        $aStatus =  array(0,1,2,3);
        // var_dump($aStatus);
        $aReturn[] = array(
            'type' => 'detail-select-action',
            'cls' => 'filter',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('过滤状态'),
                'value' => 0,
                'object_name' => 'split_index',
                'options' => $aStatus,
            ),
        );

        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('客户:'),
            ),
        );

        $aUserList = array();
        if (in_array("user-member", $aRoles)) {

            $aUserList = \Application\Model\Common::getResourceAllDataKV($sm, 'adcustomer', array("user_id"=>array('$in'=>array(0, $sm->get('zfcuser_auth_service')->getIdentity()->getId()))), array('id','name'));//        var_dump($aUserList);
        }elseif (in_array("superadmin", $aRoles)) {
            $aUserList = \Application\Model\Common::getResourceAllDataKV($sm, 'adcustomer', array(), array('id','name'));
        }
        krsort($aUserList);
        $aUserList =  array(0 =>$oController->translate("all")) + $aUserList;
        $aReturn[] = array(
            'type' => 'detail-select-action',
            'cls' => 'filter',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('客户'),
                'object_name' => 'user_id',
                'options' => $aUserList,
            ),
        );

        $aReturn[] = array(
            'type' => 'detail-searchbar',
            'position' => 'right',
            'params' => array(
                'label' => $oController->translate('搜索'),
                'tips' => 'fill chart plz',
                'object_name' => 'q_keyword',
            ),
        );

        $aReturn = $this->_fmtPermission($sm, $aReturn);
        return $aReturn;

    }
}