<?php
namespace Lur\Toolbar;

class Addata extends \Application\Toolbar\Toolbar{
    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
		$aReturn = array();

		//新增按钮
		$aReturn[] = $this->_getAddButton($oController, $sSource, $nId);


		$aReturn = $this->_fmtPermission($sm, $aReturn);
		return $aReturn;

	}	
}