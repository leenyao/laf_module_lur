<?php
namespace Lur\Toolbar;
class SystemBatchJobs extends \Application\Toolbar\Toolbar{
    public function getConfig($oController, $sm, $sSource, $nId, $nFid)
    {
        $aReturn = array();


        $oAuthService = ($sm->get('zfcuser_auth_service'));
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($sm, $oAuthService->getIdentity());
        //新增按钮
        if (in_array("user-member", $aRoles)) {
            $aReturn[] = $this->_getAddButton($oController, $sSource, $nId);
        }

        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('客户:'),
            ),
        );
        $aUserList = array();
        $nUserId = $sm->get('zfcuser_auth_service')->getIdentity()->getId();
        if (in_array("user-member", $aRoles)) {
            $aUserList = \Application\Model\Common::getResourceAllDataKV($sm, 'adcustomer', array("user_id"=>array('$in'=>array(0, $nUserId))), array('id','name'));//        var_dump($aUserList);
        }elseif (in_array("superadmin", $aRoles)) {
            $aUserList = \Application\Model\Common::getResourceAllDataKV($sm, 'adcustomer', array(), array('id','name'));
        }
        krsort($aUserList);
        $aUserList =  array("0" =>$oController->translate("all")) + $aUserList;

        $aReturn[] = array(
            'type' => 'detail-select-action',
            'cls' => 'filter',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('客户'),
                'object_name' => 'user_id',
                'options' => $aUserList,
            ),
        );

        if ((in_array("superadmin", $aRoles))) {
            $aConfig = $sm->get('config');

            // var_dump($aSspPlatform);
            $aAdminUserList = \Application\Model\Common::getResourceAllDataKV($sm, 'user', array('id'=>array('$in'=>array_keys($aConfig['userid2ssp_platforms']))), array('id','username'));

            $aAdminUserList =  array("0" =>$oController->translate("all")) + $aAdminUserList;
            $aReturn[] = array(
                'type' => 'detail-select-action',
                'cls' => 'filter',
                'position' => 'left',
                'params' => array(
                    'label' => $oController->translate('操作账号'),
                    'object_name' => 'op_user_id',
                    'options' => $aAdminUserList,
                ),
            );
        }
        
        //查看排期
        $aReturn[] = array(
            'type' => 'link',
            'position' => 'left',
//            'iconCls' => 'licon-publish_'.$nTypeTmp."_".$nStatus,
            'params' => array(
                'label' => '多列代码模板',
                'attributes' => array('target'=>'_blank'),
                'link' => $oController->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'downloadsample'))."?type=double",
            ),
            'permission' => array(),
        );
        $aReturn[] = array(
            'type' => 'pipe',
        );
        //查看排期
        $aReturn[] = array(
            'type' => 'link',
            'position' => 'left',
//            'iconCls' => 'licon-publish_'.$nTypeTmp."_".$nStatus,
            'params' => array(
                'label' => '单列代码模板',
                'attributes' => array('target'=>'_blank'),
                'link' => $oController->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'downloadsample'))."?type=single",
            ),
            'permission' => array(),
        );


        $aSspPlatform = $sm->get('\Lur\Service\Common')->getViewSspPlatformLabels();
        foreach ($aSspPlatform as $sspId => $sspLabel) {
            $sspLabel2 = strtolower(str_replace("SSP-", "", $sspLabel));

            $aReturn[] = array(
                'type' => 'pipe',
            );
            $aReturn[] = array(
                'type' => 'link',
                'position' => 'left',
                'params' => array(
                    'label' => $sspLabel.'模板',
                    'attributes' => array('target'=>'_blank'),
                    'link' => $oController->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'downloadsample'))."?type=".$sspLabel2,
                ),
                'permission' => array(),
            );
        }

        // 暂时隐藏, 此类模板废弃 2018-11-01
        if (false) {
         
                 $aReturn[] = array(
                     'type' => 'pipe',
                 );
                 $aReturn[] = array(
                     'type' => 'link',
                     'position' => 'left',
         //            'iconCls' => 'licon-publish_'.$nTypeTmp."_".$nStatus,
                     'params' => array(
                         'label' => 'UV代码模板',
                         'attributes' => array('target'=>'_blank'),
                         'link' => $oController->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'downloadsample'))."?type=uv",
                     ),
                     'permission' => array(),
                 );
                 $aReturn[] = array(
                     'type' => 'pipe',
                 );
                 $aReturn[] = array(
                     'type' => 'link',
                     'position' => 'left',
         //            'iconCls' => 'licon-publish_'.$nTypeTmp."_".$nStatus,
                     'params' => array(
                         'label' => 'VV模板',
                         'attributes' => array('target'=>'_blank'),
                         'link' => $oController->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'downloadsample'))."?type=vpc",
                     ),
                     'permission' => array(),
                 );
        }



        $aReturn[] = array(
            'type' => 'pipe',
        );
        //查看排期
        $aReturn[] = array(
            'type' => 'link',
            'position' => 'left',
//            'iconCls' => 'licon-publish_'.$nTypeTmp."_".$nStatus,
            'params' => array(
                'label' => '模板说明',
                'attributes' => array('target'=>'_blank'),
                'link' => $oController->url()->fromRoute('lur/default', array('controller'=>'index', 'action'=>'downloadsampleinfo')),
            ),
            'permission' => array(),
        );
/*
        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('状态操作:'),
            ),
        );
        $nTypeTmp = 102;
        $aStatus = \Application\Model\Common::getResourceMetadaList($sm, $nTypeTmp);
        foreach($aStatus as $nStatus=>$nLabel){
            $aReturn[] = $this->_getPublishButton($sm, $oController, $nTypeTmp, $sSource, $nStatus, null, 'm'.$nTypeTmp.'_id');
        }
        $aReturn[] = array(
            'type' => 'pipe',
        );
        //下载
        $aReturn[] = array(
            'type' => 'detail-button-download',
            'cls' => 'download',
            'iconCls' => 'licon-download',
            'position' => 'left',
            'params' => array(
                'request' => array('returnType'=>'csv_all'),
                'label' => $oController->translate('download'),
            ),
        );

        $aReturn[] = array(
            'type' => 'detail-br',
        );



        $aReturn[] = array(
            'type' => 'text',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('状态:'),
            ),
        );
        $aStatus =  array(0 =>$oController->translate("all")) + $aStatus;
        $sObjectName = \Application\Model\Common::getFieldBySource('status', $sSource);
        $aReturn[] = array(
            'type' => 'detail-select-action',
            'cls' => 'filter',
            'position' => 'left',
            'params' => array(
                'label' => $oController->translate('过滤状态'),
                'object_name' => $sObjectName,
                'options' => $aStatus,
            ),
        );
*/  


        $aReturn[] = array(
            'type' => 'detail-searchbar',
            'position' => 'right',
            'params' => array(
                'label' => $oController->translate('搜索'),
                'tips' => 'fill chart plz',
                'object_name' => 'q_keyword',
            ),
        );
        $aReturn = $this->_fmtPermission($sm, $aReturn);
        return $aReturn;

    }
}