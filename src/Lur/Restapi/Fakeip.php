<?php
namespace Lur\Restapi;
/*
* 通过地域ID造ip
* 
*/
class Fakeip extends \YcheukfCommon\Lib\Restapi{

    function get() {
        global $oGlobalFramework;
        header('Content-type: application/json'); 
        $nCityId = isset($_GET["ci"]) ? intval($_GET["ci"]) : "";

        $aReturn = array("success"=>false, "data"=>array(), "msg"=>"", "cacheflag"=>0);
        if (!empty($nCityId)) {
	        $sCacheKey = LAF_CACHE_FAKEIPPRE.$nCityId;

	        $aCacheData = \YcheukfCommon\Lib\Functions::getCache($this->sm, $sCacheKey);

	        if(!is_null($aCacheData) && !empty($aCacheData)){//找到缓存
	            $aReturn['data'] = $aCacheData;
	            $aReturn['cacheflag'] = 1;
	            $aReturn['success'] = true;

	        }else{
		        $aCacheData = $this->sm->get('\Lur\Service\Common')->getIacCity2IpListByCityId($nCityId);
	            \YcheukfCommon\Lib\Functions::saveCache($this->sm, $sCacheKey, $aCacheData, 86400);

	            $aReturn['data'] = $aCacheData;
	            $aReturn['cacheflag'] = 0;
	            $aReturn['success'] = true;
	        }
        }else{
            $aReturn['msg'] = "unavailable ci";
        }
	        // print_r($aReturn);

        return $this->responce(200, json_encode($aReturn));
    }


}