<?php
namespace Lur\Restapi;
/*
* IAC IP
*/
class Iacip extends \YcheukfCommon\Lib\Restapi{

    function get() {
        global $oGlobalFramework;
        header('Content-type: application/json'); 
        	if (!isset($_GET["ip"])) {
	            return $this->responce(500, json_encode($_GET));
        	}
            $ip=$_GET["ip"];


            if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
	            return $this->responce(500, json_encode($_GET));
            }

            $aReturn = $this->sm->get('\Lur\Service\Common')->getIacConfigByIp($ip);
            return $this->responce(200, json_encode($aReturn));
    }


}