<?php
namespace Lur\Restapi;
/*
* adcount 的队列, 收集adr的代码计数
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Adcountermq extends \YcheukfCommon\Lib\Restapi{

    /*
    * post/put 调用本函数
    */
    function _postConfig($sResource=null, $sId="", $aPostData) {
        // var_dump($aPostData);


        
        // $filename = '/tmp/feng1.'.$aPostData['adrserverid'];
        // if (!file_exists($filename)) {
        //     $handle = fopen($filename, "w");
        //     fwrite($handle, print_r($aPostData, 1));
        //     fclose($handle);
        // }
        

        // 计数器进入哪个队列
        $aMqTypeMaps = array("adschedule_counter", "b_adcounter_ssp_mango");// 默认adr, ssp芒果
        $nMqType = isset($aPostData['type']) && in_array($aPostData['type'], $aMqTypeMaps)? $aPostData['type'] : $aMqTypeMaps[0];
        if (count($aPostData) && isset($aPostData['data'])  && isset($aPostData['adrserverid']) && count($aPostData['data'])) {
            foreach ($aPostData['data'] as $sDate => $aRowTmp) {
                foreach ($aRowTmp as $sId => $nCount) {
                    $aTmp = array(
                        $aPostData['adrserverid'],
                        $sDate,
                        $sId,
                        $nCount,
                    );
                    $sKeyString = join("/", $aTmp);
                    // var_dump($sKeyString);
                    \YcheukfCommon\Lib\Functions::sendMQ($this->sm, $sKeyString, $nMqType);
                }
            }
        }
        return $this->responce(200, true);
    }
    function post($sResource=null, $sId="") {
        return $this->doPost($sResource, $sId);

    }
    function put($sResource=null, $sId="") {
        return $this->doPut($sResource, $sId);
    }

}