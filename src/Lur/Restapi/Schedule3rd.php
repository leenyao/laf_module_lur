<?php
namespace Lur\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Schedule3rd extends \YcheukfCommon\Lib\Restapi{
    var $aCoopIds;

    function __construct($sm=null){
        parent::__construct($sm);
        $this->aCoopIds = array(
            'letv' => 'a607ab1b7e863dfff894ada6b2af56db',
        );
    }
    function get($sResource=null, $sId="") {
        $this->_init($sResource, 0);//第二个参数 1=>验证token
        $aDocsConf = array(
            $sResource => array(//apidocs键值
                'resource' => $sResource,//apidocs键值对应资源名
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' => array()
                ),
//                'child' => array(//子元素
//                    'items' => array(//子元素的apidocs键值
//                        'resource' => 'hcoffee_items',//子元素的对应资源名
//                        'select' => array('where' =>array('m1004_id'=>1, 'category_id'=>'[=field=]id')),
//                        'child' => array(
//                            'attrs' => array(
//                                'resource' => 'view_item_option_1001',
//                                'select' => array('where' =>array('status'=>1, 'resid'=>'[=field=]id')),
//                            ),
//                        ),
//                    ),
//                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf[$sResource]['select']['where']['id'] = $sId;
        }
        return $this->returnGet($aDocsConf, $sId, array('cachekey'=>__FUNCTION__.'_GET_'.$sId, 'expiredtime'=>3600));
    }

    public function _chkPostFieldExists($aPostField, $aPostFieldMap)
    {
        if (count($aPostField) && count($aPostFieldMap)) {
            foreach ($aPostField as $k => $v) {
                if (!isset($aPostFieldMap[$k])) {
                    return false;
                }else{
                    if (isset($aPostFieldMap[$k]['chkfunction'])) {
                        foreach ($aPostFieldMap[$k]['chkfunction'] as $sFunc) {
                            switch ($sFunc) {
                                case 'notempty':
                                    if (strlen($aPostField[$k]) < 1) {
                                        return false;
                                    }
                                    break;
                                case 'json':
                                    if (json_decode($aPostField[$k]) == null) {
                                        return false;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        # code...
                    }
                }
            }
        }else{
            return false;
        }
        return true;
    }

    function post($sResource=null, $sId="") {
        $this->_init($sResource, 1);//第二个参数 1=>验证token

        $aPostFieldMap = array(
            "token" => array(
                "chkfunction" => array("notempty")
            ), 
            "coopId" => array(
                "chkfunction" => array("notempty")
            ),  
            "adver" => array(
                "chkfunction" => array("json", "notempty")
            ), 
        );
        $bFlag = $this->_chkPostFieldExists($_POST, $aPostFieldMap);
        if ($bFlag === false) {

            $aReturnMsg = array(
                'errCode' => 102,
                'errMsg' => "error json",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }

        if (isset($this->aCoopIds[$_POST['coopId']]) && $this->aCoopIds[$_POST['coopId']] === $_POST['token']) {
        }else{
            $aReturnMsg = array(
                'errCode' => 101,
                'errMsg' => "error token ",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }

        $aAdver = json_decode($_POST['adver'], 1);


        if (!isset($aAdver['adverName']) || strlen($aAdver['adverName']) < 1) {

            $aReturnMsg = array(
                'errCode' => 103,
                'errMsg' => "adverName can not be empty ",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }

        if (!isset($aAdver['startDate']) || strlen($aAdver['startDate']) < 1) {

            $aReturnMsg = array(
                'errCode' => 104,
                'errMsg' => "startDate can not be empty ",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }
        if (!isset($aAdver['endDate']) || strlen($aAdver['endDate']) < 1) {

            $aReturnMsg = array(
                'errCode' => 104,
                'errMsg' => "endDate can not be empty ",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }
        if ($this->validateDate($aAdver['startDate']) === false) {
            $aReturnMsg = array(
                'errCode' => 104,
                'errMsg' => "startDate can not be empty ",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }
        if ($this->validateDate($aAdver['endDate']) === false) {
            $aReturnMsg = array(
                'errCode' => 104,
                'errMsg' => "endDate can not be empty ",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }

        if (!isset($aAdver['directStrategy']) || count($aAdver['directStrategy']) < 1) {

            $aReturnMsg = array(
                'errCode' => 105,
                'errMsg' => "directStrategy can not be empty ",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }
        if (!isset($aAdver['adverPlan']) || count($aAdver['adverPlan']) < 1) {

            $aReturnMsg = array(
                'errCode' => 106,
                'errMsg' => "adverPlan can not be empty ",
                'adver' => $_POST['adver'],
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }


        $aParams = array(
            'op'=> 'save',
            'resource'=> $sResource,
            'resource_type'=> 'common',
            'access_token' => $this->dbAccessToken,
            'params' => array(
                'dataset' => array (
                    array(
                        'token' => $_POST['token'],
                        'json_data' => $_POST['adver'],
                        // 'user_id' => $_POST['coopId'],
                        'created_time' => date("Y-m-d H:i:s"),
                    )
                ),
            ),
        );
        $aReturn = \YcheukfCommon\Lib\Functions::getDataFromApi($this->sm, $aParams, 'function');
        if($aReturn['status'] == 1){
            $aAdver['coopAdverCode'] = isset($aAdver['coopAdverCode']) && !empty($aAdver['coopAdverCode'])? $aAdver['coopAdverCode'] : $aReturn['data'];
            $aReturnMsg = array(
                'errCode' => 0,
                'errMsg' => "",
                'adver' => ($aAdver),
            );
            return $this->responce(200, json_encode($aReturnMsg));
        }else{
            return $this->responce(500, json_encode($aReturn));
        }

    }

    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}