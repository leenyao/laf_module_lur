<?php
namespace Lur\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Adschedule extends \YcheukfCommon\Lib\Restapi{

    function get($sResource=null, $sId=0) {
        $this->_init($sResource, true, true);//第二个参数 验证token, 验证ADRID合法性


        $aConfig = $this->sm->get('config');

        $bUpdateCacheflag = isset($_GET['updatecacheflag']) ? $_GET['updatecacheflag'] : 0;

        // 查单条排期
        $nCodeId = 0;
        if(isset($_GET['id']) && !empty($_GET['id'])){
                $nCodeId = $_GET['id'];
        }else{//查全量排期
            $nServerId = isset($_GET['server_id']) ? $_GET['server_id'] :(isset($_GET['i'])?$_GET['i']:1);

            $aActiveAdr = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_ONLINE_ADRTRUCK_ACTIVE_317);
            if ($aActiveAdr
                && $nServerId
                && is_numeric($nServerId)
                && isset($aActiveAdr["id"])
                && ($aActiveAdr["id"]==$nServerId)
            ) {
                // var_dump($aActiveAdr);
            }else{
                return $this->responce(200, json_encode(array(
                    'dataset'=>[], 
                    'md5'=>md5(json_encode([])), 
                    'count'=>0, 
                    'laf-cachekey'=>"not-active-adr-truck", 
                    'laf-cacheflag'=>0, 
                    'laf-cachetime'=>date("YmdHis"),
                )));

            }
            // $aM1009Fid = \Application\Model\Common::getRelationList($this->sm,1009, $nServerId, true);
        }

        $this->nPnPerPage = 50000;
        $aDocsConf = array(
            $sResource => array(//apidocs键值
                'resource' => $sResource,//apidocs键值对应资源名
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    // 'where' => array('id'=>array('$in'=>$aM1009Fid))
                ),
                'child' => array(//子元素
                    'city' => array(//子元素的apidocs键值
                        'select' => array('where' =>array( 'fid'=>'[=field=]id')),
                    ),
                    'ua_type' => array(//子元素的apidocs键值
                        'select' => array('where' =>array( 'fid'=>'[=field=]id')),
                    ),
                    // 'servers' => array(//子元素的apidocs键值
                    //     'select' => array('where' =>array( 'fid'=>'[=field=]id')),
                    // ),
                    'apps' => array(//子元素的apidocs键值
                        'select' => array('where' =>array( 'fid'=>'[=field=]id')),
                    ),
                    'uvcodeid' => array(//子元素的apidocs键值
                        'select' => array('where' =>array( 'fid'=>'[=field=]id')),
                    ),
                ),
            ),
        );

        // 单个排期
        if(!empty($nCodeId)){
            $sCacheKey = LAF_CACHE_RESAPI_AD_SINGLE_132.$nCodeId;
            $aDocsConf[$sResource]['select']['where']['id'] = $nCodeId;

        }else{//全部排期
            $aDocsConf[$sResource]['select']['where']['m102_id'] = 1;
            // $sCacheKey = "restapi_adschedule_get_".md5(json_encode($aM1009Fid));
            $sCacheKey = LAF_CACHE_RESAPI_AD_ACTIVE_131;
        }
        $bMd5Flag = (isset($_GET['md5flag']) && $_GET['md5flag']==1)  ?  1 : 0;
        $aTmp = $this->returnGet($aDocsConf, "", array('cachekey'=>$sCacheKey, 'expiredtime'=>rand(76400,86400)), $bMd5Flag, $bUpdateCacheflag);
        // $aTmp = $this->returnGet($aDocsConf, $sId);
    }

    protected function __trigger_fmtReturnValue($a='')
    {
        $oCacheObject = \YcheukfCommon\Lib\Functions::getCacheObject($this->serviceManager);

        $nYestodayDateTimeWaterLine = strtotime("-1 day", time());//只加载三天数据
        $nDateTimeWaterLine = strtotime("+2 day", time());//只加载三天数据
        // var_dump(date("Y-m-d", $nDateTimeWaterLine));
        $aReturn = array();
        if (count($a)) {
            foreach ($a as $k1 => $a1) {

                // mg从2021-03-31起屏蔽
                $mgbroke = false;
                if ($a1['user_id'] ==2036) {
                    $mgbroke = true;
                    // \Application\Model\Common::onlinedebug('a', json_encode($a));
                }
                // 将指定使用
                if (isset($a1['useiac']) && $a1['useiac']==1) { // 测试
                    $oCacheObject->sAdd(LAF_CACHE_USEIACIP_ADSET_130, $a1['id']);
                }else{
                    $oCacheObject->sRem(LAF_CACHE_USEIACIP_ADSET_130, $a1['id']);
                }


                $aNewA1 = $a1;
                $Adschedule = array();
                foreach ($a1['schedule'] as $k2 => $a2) {
                    $a2[0] = preg_replace("/\s+/i", " ", trim($a2[0]));
                    if (preg_match("/,/i", $a2[0])) {
                    }else{
                        $a3 = explode(" ", $a2[0]);
                        if (strtotime($a3[0]) > $nDateTimeWaterLine) {
                            continue;
                        }
                        if (strtotime($a3[0]) < $nYestodayDateTimeWaterLine) {
                            continue;
                        }

                        // mg从2021-03-31起屏蔽
                        if($mgbroke && strtotime($a3[0]) > strtotime("2021-03-31")){
                            continue;
                        }
                    }

                    $Adschedule[$k2] = $a2;
                }
                $aNewA1['schedule'] = $Adschedule;
                $aReturn[$k1] = $aNewA1;
            }
        }
        return $aReturn;
    }
}