<?php
namespace Lur\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Adrconfig extends \YcheukfCommon\Lib\Restapi{
    function get($sResource=null, $sId="") {
        // var_dump($sResource);
        $this->_init($sResource, 0);//第二个参数 1=>验证token

        $aConfig = $this->sm->get('config');
        
        $aReturn = array();
        // var_dump($aReturn);

        $bUpdateCacheflag = isset($_GET['updatecacheflag']) ? $_GET['updatecacheflag'] : 0;


        $sCacheKey = "restapi_adrconfig";
        $bCache = false;
        $aCacheData = \YcheukfCommon\Lib\Functions::getCache($this->sm, $sCacheKey);



        if($bUpdateCacheflag==0 && (!is_null($aCacheData) && !empty($aCacheData))){//找到缓存
        // if(false){//找到缓存
            $aReturn = $aCacheData;
            $bCache = true;
        }else{

            // $aReturn['iaccity2ip'] = $this->sm->get('\Lur\Service\Common')->getIacCity2IpList();
            $aReturn['citymaps'] = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->sm, 1007);
            // $aReturn['adserver_domains'] = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->sm, 1010);
            // $aReturn['ipproxyconfig'] = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->sm, 1011);
            $aReturn['warning_receiver'] = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->sm, 1012);
            $aTmp = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->sm, 1015);
            $aTmp2 = array();   
            foreach ($aTmp as $key => $value) {
                $aTmp2[$key] = json_decode($value, 1);
            }
            $aReturn['appinfos'] = $aTmp2;
            $aReturn['usredisconf'] = $this->sm->get('\Lur\Service\Common')->getUsRedisConf();
            $aReturn['usconf'] = $this->sm->get('\Lur\Service\Common')->getUsConf();
            $aReturn['getAllRunninngCityIds'] = $this->sm->get('\Lur\Service\Common')->getAllRunninngCityIds();


            
            // $aReturn['adserver_ignore_domains'] = \YcheukfCommon\Lib\Functions::getResourceMetadaList($this->sm, 1013);
            // var_dump($aReturn);
            \YcheukfCommon\Lib\Functions::saveCache($this->sm, $sCacheKey, $aReturn, 86400*10);
        }

        return $this->responce(200, json_encode($aReturn));

        // return $this->returnGet($aDocsConf, $sId, array('cachekey'=>__FUNCTION__.'_GET_'.$sId, 'expiredtime'=>3600));
    }

}