<?php
namespace Lur\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Adrserver extends \YcheukfCommon\Lib\Restapi{
    function get($sResource=null, $sId="") {
        // var_dump($sResource);
        $this->_init($sResource, 1);//第二个参数 1=>验证token

        if (!isset($_GET['pkey'])) {
            return $this->responce(500, "need pkey");
        }
        if (($_GET['pkey']) !== $this->_encrypt($sId)) {
            return $this->responce(500, "invalable pkey: ".$sId."=>".$this->_encrypt($sId));
        }
        \YcheukfCommon\Lib\Functions::setUseLafCacheFlag(true);
        $aData = \YcheukfCommon\Lib\Functions::getResourceMetaData($this->sm, LAF_METADATATYPE_ADRSERVER, $sId);
        if ($aData['count'] < 1) {
            return $this->responce(500, "no such adrserver");
        }

        return $this->responce(200, 1);

        // return $this->returnGet($aDocsConf, $sId, array('cachekey'=>__FUNCTION__.'_GET_'.$sId, 'expiredtime'=>3600));
    }



    function _encrypt($nId, $sSlat="leenyao")
    {
    	return \YcheukfCommon\Lib\Functions::encryptBySlat($nId);
    } 

    function post($sResource=null, $sId="") {


        // 测试代码
        if ($_SERVER['REMOTE_ADDR']=="172.17.0.1") {
            $sAdrServerIp = "222.67.215.54";//上海IP, 1
        }elseif (preg_match("/^172\.[0-9]+\.0\.[0-9]+$/i", $_SERVER['REMOTE_ADDR'])) {
            $sAdrServerIp = "180.97.80.75";//上海IP, 1
        }
        else{
            $sAdrServerIp = \YcheukfCommon\Lib\Functions::getRemoteClientIp();
            
        }
        

    	// var_dump($_SERVER);
        $sMaxId = \Application\Model\Common::getMetadataMaxResId($this->sm, LAF_METADATATYPE_ADRSERVER);
        $nNewId = $sMaxId+1;

        $aIpConfig = $this->sm->get('\Lur\Service\Common')->getIacConfigByIp($sAdrServerIp, true);
        $sIpTmp = (count($aIpConfig) ? ($aIpConfig['data']['ip']) : $sAdrServerIp);
        $sLabelName = $sIpTmp;
        $aConfig = $this->sm->get('config');
        // var_dump($aConfig['aAdrRegistrationNameMapping']);
        $sLabelName = isset($aConfig['aAdrRegistrationNameMapping'][$sLabelName]) ? $aConfig['aAdrRegistrationNameMapping'][$sLabelName] : $sLabelName;

        \YcheukfCommon\Lib\Functions::saveMetadata($this->sm, LAF_METADATATYPE_ADRSERVER, $nNewId,  $sLabelName."/".$nNewId);
        \YcheukfCommon\Lib\Functions::saveMetadata($this->sm, LAF_METADATATYPE_ADRSERVERTYPE_TROLLEYVPN_1021, $nNewId,  $sLabelName."/".$nNewId);

        
        return $this->responce(200, $nNewId.",".$this->_encrypt($nNewId));

    	// _postConfig
     //    $this->doPost($sResource, $sId);
    }
}


