<?php
namespace Lur\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Adrstats extends \YcheukfCommon\Lib\Restapi{
    var $bIsBlackIp=false;
    /*
    * post/put 调用本函数
    */
    function _postConfig($sResource=null, $sId="", $aPostData) {
        $sApidocsKey = 'adrstats';
        $sResourceKey = $sResource;
        $aPostData = $this->_fmtPostData($this->aConfig['apiservice']['customer'], $aPostData, $sApidocsKey);

        /*
        $aDocsConf = array(
            $sApidocsKey => array(
                'resource' => $sResourceKey,
            ),
        );
        if(!empty($sId)){
            $aDocsConf[$sApidocsKey]['select']['where']['id'] = $sId;
        }else{
            $aDocsConf[$sApidocsKey]['select']['dataset']['created_time'] = date('Y-m-d H:i:s');
        }
        */

        $this->bIsBlackIp = $this->sm->get('\Lur\Service\Common')->updBlackIpCounter($this->nAdrServerId, $aPostData);

        // if ($this->nAdrServerId==343) {
        //     exec("echo '".print_r($aPostData, 1)."' >> /tmp/feng.".$this->nAdrServerId);
        $this->_saveLastData($aPostData);

        // }
        return $this->responce(200, json_encode($aPostData));
        // return $this->doSave($aPostData, $aDocsConf);
    }

    public function _saveLastData($aData)
    {
        
        if (is_array($aData) 
            && isset($aData['host']) 
            && isset($aData['type']) 
            && preg_match("/^\d+$/i", $aData['host']) 
            && preg_match("/^\d+$/i", $aData['type'])
        ) {

            // $aConfig = $this->serviceManager->get('config');

            $aTypes = [$aData['type']];
            if ($aData['type'] == 2) {
                // 1状态包含了2状态
                $aTypes[] = 1;
            }

            $nExpiredTime =87000*3;
            foreach ($aTypes as $nType) {
                $sCacheKey = LAF_CACHE_ADRLASTSTATS_121.$nType."/".$aData['host'];
                \YcheukfCommon\Lib\Functions::saveCache($this->sm, $sCacheKey, $aData, $nExpiredTime);
            }
        }
    }

    function get($sResource=null, $sId="") {
        $this->_init($sResource, 1, 1);//第二个参数 1=>验证token
        $aDocsConf = array(
            $sResource => array(//apidocs键值
                'resource' => $sResource,//apidocs键值对应资源名
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' => array()
                ),
//                'child' => array(//子元素
//                    'items' => array(//子元素的apidocs键值
//                        'resource' => 'hcoffee_items',//子元素的对应资源名
//                        'select' => array('where' =>array('m1004_id'=>1, 'category_id'=>'[=field=]id')),
//                        'child' => array(
//                            'attrs' => array(
//                                'resource' => 'view_item_option_1001',
//                                'select' => array('where' =>array('status'=>1, 'resid'=>'[=field=]id')),
//                            ),
//                        ),
//                    ),
//                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf[$sResource]['select']['where']['id'] = $sId;
        }
        return $this->returnGet($aDocsConf, $sId, array('cachekey'=>__FUNCTION__.'_GET_'.$sId, 'expiredtime'=>3600));
    }
    function post($sResource=null, $sId="") {
        \YcheukfCommon\Lib\Functions::setUseLafCacheFlag(true);
        
        $this->sRunType = 'function';// 不会直接response
        list($nCode, $sResponce) = $this->doPost($sResource, $sId, true);
        if($nCode !== 200){ //失败
            \ToroHook::fire($nCode, $sResponce);
        }else{
            $aReturn = array();
            // adr 机器补量满载指标
            // $aReturn["addtional_loadpercent"] = $this->sm->get('\Lur\Service\Addtional')->getAllCitiesLoadPercent();
            // 不再需要
            // $aReturn["addtional_servercount"] = $this->sm->get('\Lur\Service\Addtional')->getAllCitiesLoadPercent();

            // adr 机器补量城市计数器
            // $aReturn["addtional_citycount"] = $this->sm->get('\Lur\Service\Addtional')->getAllCityCounter();

            // adr 机器补量城市计数器
            $aReturn["addtional_adrcityid"] = $this->sm->get('\Lur\Service\Addtional')->getCityidByAdrid($this->nAdrServerId);

            $aReturn["adrserver_type"] =  $this->sm->get('\Lur\Service\Common')->getAdrServerType($this->nAdrServerId);

            $aReturn["is_blackip"] =  $this->bIsBlackIp===true?1:0;

            

            // $sAdrServerIp = \YcheukfCommon\Lib\Functions::getRemoteClientIp();



            // adr 机器VPN账号
            // \YcheukfCommon\Lib\Functions::setUseLafCacheFlag(false);
            $sVPNData = $this->sm->get('\Lur\Service\Common')->getVpnData($this->nAdrServerId);
            // var_dump($this->nAdrServerId, $aData);
            $aReturn["vpnacc"] = ($sVPNData) && !empty($sVPNData) ? $sVPNData : "";

            // $aReturn["shellscript"] = $this->sm->get('\Lur\Service\Common')->getShellScript($this->nAdrServerId);


            // 批量执行的脚本, 分成N批执行
            $adrHost = $this->sm->get('\Lur\Service\Common')->getAdrId2Host($this->nAdrServerId);
            $sShellscript = require '/app/module/Lur/config/adr.shell.script.php';
                // \Application\Model\Common::onlinedebug('filemtime(filename)', filemtime('/app/module/Lur/config/adr.shell.script.php'));
            
            if ($sShellscript 
                && !is_null($sShellscript)
                && !is_null($adrHost)
                && $this->nAdrServerId != 42
            ) {
                preg_match_all('/t=(\w+);.*/', $sShellscript, $matches);
                if (isset($matches[1]) && isset($matches[1][0])) {
                    $oReids = \Application\Model\Common::getCacheObject($this->sm);
                    $taskid = $matches[1][0];
                    $oReids->set(LAF_CACHE_CURRENT_SHELLID_137, $taskid);

                    $sShellKeyLock = LAF_CACHE_ADRSHELLLIST_136."/".$taskid;
                    $sShellKeySucc = LAF_CACHE_ADRSHELLLIST_OK_138."/".$taskid;
                    
                    // 若已经成功提交了结果, 则跳过
                    if (!$oReids->hExists($sShellKeySucc, $this->nAdrServerId)) {

                        // 是否当前机器已经有被某adrid锁定, 若无则自己锁定
                        if (!$oReids->hExists($sShellKeyLock, $adrHost)) {


                            $oReids->hSet($sShellKeyLock, $adrHost, json_encode([
                                "t" => date("Y-m-d H:i:s"),
                                "id" => $this->nAdrServerId,
                            ]));
                            $oReids->setTimeout($sShellKeyLock,100*3600);
                            $aReturn["shellscript"] = $sShellscript;

                        }else{
                            
                            $tmp = $oReids->hGet($sShellKeyLock, $adrHost);
                            $json = json_decode($tmp, 1);

                            // 超时没有完成, 执行新的任务
                            if (time()-strtotime($json['t']) > 50) {
                                $aReturn["shellscript"] = "rand=".time().";".$sShellscript;
                                $oReids->hSet($sShellKeyLock, $adrHost, json_encode([
                                    "t" => date("Y-m-d H:i:s"),
                                    "id" => $this->nAdrServerId,
                                ]));
                            }

                        }
                    }

                }

            }
            // 
            \ToroHook::fire($nCode, json_encode($aReturn));

        }
    }
    function put($sResource=null, $sId="") {
        $this->post($sResource, $sId);
    }

}