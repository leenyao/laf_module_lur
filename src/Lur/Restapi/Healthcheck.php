<?php
namespace Lur\Restapi;
/*
* lur 健康检查
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Healthcheck extends \YcheukfCommon\Lib\Restapi{

    function get($sResource=null) {
        // $this->_init($sResource, true, false);//第二个参数 验证token, 验证ADRID合法性

    	$sRemoteIp = \Application\Model\Common::getRemoteClientIp();
      $action = isset($_GET['action']) ? $_GET['action'] : "all"; 
    	// var_dump($_SERVER);

    	$aReturn = [
    		"status" => 1,
    		"data" => [],
    		"msg" => [],
    	];
    	$bCheckOk = true; 
    	try {

        switch ($action) {
          case "adr_online":
            
            $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
            $sTitle = 'adr_online';
            $aAdrSummary = $this->serviceManager->get('\Lur\Service\Common')->getAdrSummary();
            $aReturn['data'][$sTitle] = count($aAdrSummary['aLiveAdrServerType2Id']['TROLLEY-VPN']) ."/". count($aAdrSummary['aAllAdrServer']['aServerType2Id']['TROLLEY-VPN']);
            $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
            $aReturn['data'][$sTitle."-time"] = ($nTime2-$nTime1);
            // $bCheckOk=true;
            return $this->responce(200, json_encode($aReturn));

            
            break;
          
          default:
            
            break;
        }


            $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
    		$sTitle = 'redis_default';
    		$oCacheObject = \Application\Model\Common::getCacheObject($this->serviceManager);
		    $aInfo = $oCacheObject->info();
		    $bFlag = ($aInfo && count($aInfo)) ? 1: 0;
            $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
            $aReturn['data'][$sTitle] = $bFlag?1:0;
		    $aReturn['data'][$sTitle."-time"] = ($nTime2-$nTime1);
		    if(!$bFlag)$bCheckOk=false;

		    // 锁
		    if ($oCacheObject->setNx(LAF_CACHE_HEALTHYCHEKCK_LOCK_129, 1)) {
		    	$oCacheObject->setTimeout(LAF_CACHE_HEALTHYCHEKCK_LOCK_129,60);
		    }else{
		    	$aReturn['msg'] = 'cache';
		    	return $this->responce(200, json_encode($aReturn));
		    }
		    





            $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
    		$sTitle = 'db_master';
		    $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_master"); //db_master|db_slave
		    $sSql = "select 1";
		    $oSth = $oPdo->prepare($sSql);
		    $bFlag = $oSth->execute();
		    if(!$bFlag)$bCheckOk=false;
            $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
		    $aReturn['data'][$sTitle] = $bFlag?1:0;
            $aReturn['data'][$sTitle."-time"] = ($nTime2-$nTime1);



            $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
    		$sTitle = 'db_slave';
		    $oPdo = \Application\Model\Common::getPDOObejct($this->serviceManager, "db_slave"); //db_master|db_slave
		    $sSql = "select 1";
		    $oSth = $oPdo->prepare($sSql);
		    $bFlag = $oSth->execute();
		    // $bFlag = false;
            $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
		    if(!$bFlag)$bCheckOk=false;
		    $aReturn['data'][$sTitle] = $bFlag?1:0;
            $aReturn['data'][$sTitle."-time"] = ($nTime2-$nTime1);



            $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
    		$sTitle = 'redis_addtional';
		    $aInfo = $this->serviceManager->get('\Lur\Service\Common')->getAddtionalRedis()->info();
		    $bFlag = ($aInfo && count($aInfo)) ? 1: 0;
            $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
		    $aReturn['data'][$sTitle] = $bFlag?1:0;
            $aReturn['data'][$sTitle."-time"] = ($nTime2-$nTime1);
		    if(!$bFlag)$bCheckOk=false;





            
            $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
            $nActivceTruckItem = \YcheukfCommon\Lib\Functions::getCache($this->serviceManager, LAF_CACHE_ONLINE_ADRTRUCK_ACTIVE_317);
        	$aAdrServerInfo = $this->serviceManager->get('\Lur\Service\Common')->getAdrSerInfoById($nActivceTruckItem['id']);

    		$sTitle = 'adr_truck';
        	if (
        		// truck 存在
        		count($aAdrServerInfo) 
        		// 激活的truck存在上传数据
        		&& isset($aAdrServerInfo)
        		&& isset($aAdrServerInfo['tds_counter'])
        		// 激活的truck在当前小时存在上传数据
        		&& isset($aAdrServerInfo['tds_counter'][date('H')])
        		&& intval($aAdrServerInfo['tds_counter'][date('H')])>0

        	) {
        		$bFlag = true;
	        	// print_r($aAdrServerInfo['tds_counter'][date('H')]);
        	}else{
			    $bFlag = $bCheckOk=false;

        	}
            $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
		    $aReturn['data'][$sTitle] = $bFlag?1:0;
            $aReturn['data'][$sTitle."-time"] = ($nTime2-$nTime1);



      //       $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
    		// $sTitle = 'adr_trolley';
      //   	if (
      //   		// trolley 存在
      //   		count($aAdrSummary['aLiveAdrServerType2Id']['TROLLEY-VPN']) 
      //   		&& count($aAdrSummary['aAllAdrServer']['aServerType2Id']['TROLLEY-VPN']) 

      //   		// 在线率不到50%
      //   		&& count($aAdrSummary['aLiveAdrServerType2Id']['TROLLEY-VPN']) / count($aAdrSummary['aAllAdrServer']['aServerType2Id']['TROLLEY-VPN']) > 0.5

      //   		// 存在vpn账号, 且在线率打到50
      //   		&& count($aAdrSummary['aUsingVpnAcc']) 
      //   		&& count($aAdrSummary['aAllVpnAccs']) 
      //   		&& (count($aAdrSummary['aUsingVpnAcc'])/count($aAdrSummary['aAllVpnAccs'])>0.5 )


      //   	) {
      //   		$bFlag = true;
      //   	}else{
			   //  $bFlag = $bCheckOk=false;
      //   	}
      //       $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
		    // $aReturn['data'][$sTitle] = $bFlag?1:0;
      //       $aReturn['data'][$sTitle."-time"] = ($nTime2-$nTime1);


		    // 当前小时有统计数据
    		$sTitle = 'adr_hourdata';
            $nTime1 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
	    	$aHourDatas = $this->serviceManager->get('\Lur\Service\Common')->getLatestDateHourReport();
	    	if (
	    		isset($aHourDatas[date("Ymd")])
	    		&& isset($aHourDatas[date("Ymd")][date("H")])
	    		&& intval($aHourDatas[date("Ymd")][date("H")]) > 0
	    	) {
        		$bFlag = true;
	    	}else{
			    $bFlag = $bCheckOk=false;
	    	}
            $nTime2 = \YcheukfCommon\Lib\Functions::getMicrotimeFloat();//计时
		    $aReturn['data'][$sTitle] = $bFlag?1:0;
            $aReturn['data'][$sTitle."-time"] = ($nTime2-$nTime1);





		    if ($bCheckOk==false) {
		    	throw new \Exception("Error Processing Request", 1);
		    	
		    }

		    return $this->responce(200, json_encode($aReturn));
    	} catch (\Exception $e) {
    		$aReturn['status'] = 500;
		    return $this->responce(500, json_encode($aReturn));
    		
    	}


    }

}