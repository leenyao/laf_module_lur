<?php
namespace Lur\Restapi;
/*
* restful 方法 get/post/put/delete, 如不需要, 注释掉即可
*/
class Usstats extends \YcheukfCommon\Lib\Restapi{

    /*
    * post/put 调用本函数
    */
    function _postConfig($sResource=null, $sId="", $aPostData) {
        $sApidocsKey = 'usstats';
        $sResourceKey = $sResource;
        $aPostData = $this->_fmtPostData($this->aConfig['apiservice']['customer'], $aPostData, $sApidocsKey);
        $aDocsConf = array(
            $sApidocsKey => array(
                'resource' => $sResourceKey,
            ),
        );
        if(!empty($sId)){
            $aDocsConf[$sApidocsKey]['select']['where']['id'] = $sId;
        }else{
            $aDocsConf[$sApidocsKey]['select']['dataset']['created_time'] = date('Y-m-d H:i:s');
        }
        return $this->doSave($aPostData, $aDocsConf);
    }
    function get($sResource=null, $sId="") {
        $this->_init($sResource, 1);//第二个参数 1=>验证token
        $aDocsConf = array(
            $sResource => array(//apidocs键值
                'resource' => $sResource,//apidocs键值对应资源名
                'select' => array(
                    "columns" => "*",
                    'offset' => ($this->nPnPage-1)*$this->nPnPerPage,
                    'limit' => $this->nPnPerPage,
                    'order' => empty($this->nPnOrder) ? null : $this->nPnOrder,
                    'where' => array()
                ),
//                'child' => array(//子元素
//                    'items' => array(//子元素的apidocs键值
//                        'resource' => 'hcoffee_items',//子元素的对应资源名
//                        'select' => array('where' =>array('m1004_id'=>1, 'category_id'=>'[=field=]id')),
//                        'child' => array(
//                            'attrs' => array(
//                                'resource' => 'view_item_option_1001',
//                                'select' => array('where' =>array('status'=>1, 'resid'=>'[=field=]id')),
//                            ),
//                        ),
//                    ),
//                ),
            ),
        );
        if(!empty($sId)){
            $aDocsConf[$sResource]['select']['where']['id'] = $sId;
        }
        return $this->returnGet($aDocsConf, $sId, array('cachekey'=>__FUNCTION__.'_GET_'.$sId, 'expiredtime'=>3600));
    }
    function post($sResource=null, $sId="") {
        $this->doPost($sResource, $sId);
    }
    function put($sResource=null, $sId="") {
        $this->doPut($sResource, $sId);
    }

}