<?php
namespace Lur\Restapi;
/*
* POST 补量上报
* GET 补量获取
*/
class Addtional extends \YcheukfCommon\Lib\Restapi{

    /*
    * post/put 调用本函数
    */
    function _postConfig($sResource=null, $sId="", $aPostData) {

        $sAction = isset($aPostData['action']) ? $aPostData['action'] : "get";
        $sAction = in_array($sAction, array("get", "send")) ? $sAction : "get";

        switch ($sAction) {
            case 'get'://获取补量
                $aReturn = $this->_doGet($aPostData);
                // var_dump($aReturn);
                $this->responce(200, json_encode($aReturn));
                break;
            case 'send'://发送补量
                $this->_doSend($aPostData);
                return $this->responce(200, 1);
                break;
            default:
                break;
        }
    }

    public function _doGet($aPostData=array())
    {
        // 直连pi3 ,屏蔽
        if (in_array($this->nAdrServerId, [9,10,11,17,22,23,24,26,31,33,37,38,39,41,43,44,45,46,47,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94])) {
            return [];
        }
        $aConfig = $this->sm->get('config');
        $aIgnorecoids =isset($aPostData['ignorecoids']) ? $aPostData['ignorecoids'] : array();
        $sVpnremoteip =isset($aPostData['sVpnremoteip']) ? $aPostData['sVpnremoteip'] : "";
        $sLocalremoteip =isset($aPostData['sLocalremoteip']) ? $aPostData['sLocalremoteip'] : "";
        $sPostVpnAcc =isset($aPostData['sVpnAcc']) ? $aPostData['sVpnAcc'] : "";
        $bVpnStat = LAF_CALLVPNSTAT_BAD_602;
        $oAddtionalRedis = $this->sm->get('\Lur\Service\Common')->getAddtionalRedis();

        $aReturn = array();
        if (isset($_GET['tradeno'])) {
            $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_TRADENO_GET_303."/".date("Ymd");
            $aCacheTmp = $oAddtionalRedis->hGet($sTradeNoCacheKey, $_GET['tradeno']);
            if ($aCacheTmp) {
                $aJson = json_decode($aCacheTmp, 1);
                $aReturn = $aJson[0];
            }else{


                if ($_SERVER['REMOTE_ADDR']=="172.17.0.1") {//测试用
                    $sAdrServerIp = "222.67.215.54";//上海IP, 1
                }else{
                    $sAdrServerIp = \YcheukfCommon\Lib\Functions::getRemoteClientIp();
                }
                // if (preg_match("/^172\.[0-9]{2}\.0\.[0-9]+$/i", $sAdrServerIp)) {
                //     $sAdrServerIp = "180.97.80.75";//上海IP, 1
                // }


                $sCurrentVpnAcc = $this->sm->get('\Lur\Service\Common')->getVpnData($this->nAdrServerId);


                // 判断是否直连模式
                // 未分配VPN
                if (empty($sCurrentVpnAcc) && empty($sPostVpnAcc)) {
                    // 存在本地IP 且 本地IP==请求IP
                    
                    $bVpnStat = LAF_CALLVPNSTAT_DERECT_603;
                    return [];
                }

                // 上传的VPN IP 与 lur实际读到的IP 一致 (避免adr vpn 中途失效的情况)
                // if (!empty($sVpnremoteip) && $sVpnremoteip==$sAdrServerIp) {
                // 预设vpn信息 与 上传的 vpn信息一致


                // if ($this->nAdrServerId==343) {
                //     exec("echo 'sVpnremoteip=".$sVpnremoteip."' >> /tmp/feng.".$this->nAdrServerId);
                //     exec("echo 'sCurrentVpnAcc=".$sCurrentVpnAcc."' >> /tmp/feng.".$this->nAdrServerId);
                //     exec("echo 'sPostVpnAcc=".$sPostVpnAcc."' >> /tmp/feng.".$this->nAdrServerId);
                    

                // }
                if (!empty($sCurrentVpnAcc) && !empty($sPostVpnAcc) && $sPostVpnAcc==$sCurrentVpnAcc) {
                    if (!empty($sVpnremoteip) ) {
                        $sAdrServerIp = $sVpnremoteip;
                        $bVpnStat = LAF_CALLVPNSTAT_OK_601;
                    }
                }

                // 存储上报上来的 vpnacc, 以供重播使用
                if (!empty($sPostVpnAcc)) {
                    \YcheukfCommon\Lib\Functions::saveCache($this->sm, LAF_CACHE_VPNLOCALACC_116.$this->nAdrServerId, trim($sPostVpnAcc), 300);
                }
                // 



                
                $aReturn = $this->sm->get('\Lur\Service\Addtional')->getJobsByType($bVpnStat,$this->nAdrServerId, $this->sAdrServerType, $sAdrServerIp, $aIgnorecoids);

                if (count($aReturn)) {
                    $aLogData = json_encode(array($aReturn, date("Y-m-d H:i:s"), $_GET));
                    $oAddtionalRedis->hSet($sTradeNoCacheKey, $_GET['tradeno'], $aLogData);
                    $oAddtionalRedis->set(LAF_CACHE_ADDTIONAL_LASTRECORD_GET_PRE_308.$this->nAdrServerId, $aLogData);

                    // 记录历史交易
                    $aLogData = json_encode(array($aReturn, date("Y-m-d H:i:s")));
                    $sHisTradeCacheKey = LAF_CACHE_ADDTIONAL_GETLOG_PRE_312.date("Ymd")."/".$this->nAdrServerId;
                    $oAddtionalRedis->rpush($sHisTradeCacheKey, $aLogData);
                    if ($oAddtionalRedis->ttl($sHisTradeCacheKey)<3600) {
                        $oAddtionalRedis->setTimeout($sHisTradeCacheKey, 86400*1.5);
                    }
                }
            }

            if ($oAddtionalRedis->ttl($sTradeNoCacheKey)<3600) {
                $oAddtionalRedis->setTimeout($sTradeNoCacheKey, 86400);
            }

        }
        return $aReturn;
    }

    public function _doSend($aPostData)
    {

        // 直连pi3 ,屏蔽
        if (in_array($this->nAdrServerId, [9,10,11,17,22,23,24,26,31,33,37,38,39,41,43,44,45,46,47,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94])) {
            return [];
        }
        // 最后时刻不再请求任务, 避免跨天
        // $oTimeDiff = (\YcheukfCommon\Lib\Functions::getTimeSecondDiff(date("H:i:s"), "23:59:59"));
        // if ($oTimeDiff && $oTimeDiff->interval<5) {

        //     return true;
        // }


        $oAddtionalRedis = $this->sm->get('\Lur\Service\Common')->getAddtionalRedis();
        
        if (count($aPostData) && isset($aPostData['data'])  && isset($_GET['tradeno'])  && count($aPostData['data'])) {

            $sCurrentDateTime = date("Y-m-d H:i:s");
            $sCurrentDate = isset($aPostData['date']) ? $aPostData['date'] :  date("Ymd");


            $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_TRADENO_SEND_302."/".$sCurrentDate;
            $sHisTradeCacheKey = LAF_CACHE_ADDTIONAL_SENDLOG_PRE_311.$sCurrentDate."/".$this->nAdrServerId;
            
            if (!$oAddtionalRedis->hExists($sTradeNoCacheKey, $_GET['tradeno'])) {

                $sCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".$sCurrentDate;
                foreach ($aPostData['data'] as $sJobkey => $nCount) {
                    list($p1, $p2, $p3, $p4) = explode("-", $sJobkey);
                    $sJobkey = $p1."-".$p2."-9999-".$p4;

                    // truck
                    if ($this->sAdrServerType == LAF_METADATATYPE_ADRSERVERTYPE_TRUCK_1009) {
                        $oAddtionalRedis->hIncrBy($sCacheKey, $sJobkey, $nCount);

                        $this->sm->get('\Lur\Service\Adrserver')->incTruckSendHourCounter($this->nAdrServerId, $sCurrentDate, $nCount);


                    }else{// 非truck
                        $bGetInitFlag = $this->sm->get('\Lur\Service\Addtional')->getGetInitFlag($this->nAdrServerId, $p1);
                        if ($bGetInitFlag) { // 先有get
                            # code...
                          $oAddtionalRedis->hIncrBy($sCacheKey, $sJobkey, $nCount);
                        }else{
                            // 跨天的提交, 都归到昨天的池子

                            $sCacheKey = LAF_CACHE_ADDTIONAL_DB_301."/".date("Ymd", strtotime("-1 day"));
                            $sTradeNoCacheKey = LAF_CACHE_ADDTIONAL_TRADENO_SEND_302."/".date("Ymd", strtotime("-1 day"));
                            $sHisTradeCacheKey = LAF_CACHE_ADDTIONAL_SENDLOG_PRE_311.date("Ymd", strtotime("-1 day"))."/".$this->nAdrServerId;

                            $oAddtionalRedis->hIncrBy($sCacheKey, $sJobkey, $nCount);
                            // exec("echo '".date("Ymd H:i:s").",".$this->nAdrServerId.','.$sJobkey.','.$nCount."' >> /tmp/error.addtional.send.".date("Ymd"));
                        }
                    }


                }
                if ($oAddtionalRedis->ttl($sCacheKey)<3600) {
                    $oAddtionalRedis->setTimeout($sCacheKey, 86400*3);
                }

                // 记录某ADR最后一次交易, 按trade no
                $aLogData = json_encode(array($aPostData['data'], $sCurrentDateTime, $_GET));
                $oAddtionalRedis->hSet($sTradeNoCacheKey, $_GET['tradeno'], $aLogData);
                $oAddtionalRedis->set(LAF_CACHE_ADDTIONAL_LASTRECORD_SEND_PRE_307.$this->nAdrServerId, $aLogData);

                // 记录某ADR最后一次交易, 按 时间顺序
                $aLogData = json_encode(array($aPostData['data'], $sCurrentDateTime));
                $oAddtionalRedis->rpush($sHisTradeCacheKey, $aLogData);
                if ($oAddtionalRedis->ttl($sHisTradeCacheKey)<3600) {
                    $oAddtionalRedis->setTimeout($sHisTradeCacheKey, 86400*1.5);
                }
                
            }

            if ($oAddtionalRedis->ttl($sTradeNoCacheKey)<3600) {
                $oAddtionalRedis->setTimeout($sTradeNoCacheKey, 86400*3);
            }

        }
    }


    function post($sResource=null, $sId="") {
        return $this->doPost($sResource, $sId, true);

    }
    function put($sResource=null, $sId="") {
        return $this->doPut($sResource, $sId, true);
    }
    function get($sResource=null, $sId="") {
        $this->_init($sResource, 1, 1);//第二个参数 1=>验证token
        $aReturn = $this->_doGet();
        return $this->responce(200, json_encode($aReturn));

    }
}