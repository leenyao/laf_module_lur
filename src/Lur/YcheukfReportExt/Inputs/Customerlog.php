<?php
namespace Lur\YcheukfReportExt\Inputs;
class Customerlog extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
                // array(
                //     'key' => 'status', //操作符
                //     'op' => 'in', //操作符=,in,like
                //     'value' => "(1)"//过滤的值
                // ),
			),
			'input'=>array(
				'detail'=>array(
					'is_editable' => false,
					'is_viewable' => false,
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								'id',
								'user_id',
								'm159_id',
								'memo',
								'modified',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
            ),
		);
		$aInput = $this->_fmtDetailFilters(
            $sSource, 
            $aInput, 
            $this->getReportParams(), 
            array('m159_id', 'user_id'),  //filterable field
            array('memo', 'm159_id')
        );
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}