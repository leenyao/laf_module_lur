<?php
namespace Lur\YcheukfReportExt\Inputs;
class Adschedule extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();

				
		$oIdentity = $this->sM->get('zfcuser_auth_service')->getIdentity();
		$sUserId = $oIdentity->getId();
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($this->sM, $oIdentity);
        
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件.
                // array(
                //     'key' => 'user_id', //操作符
                //     'op' => 'in', //操作符=,in,like
                //     'value' => "(0, ".$sUserId.")"//过滤的值
                // ), 
			),
			'input'=>array(
				'detail'=>array(
					'type' =>'table',
					// 'is_editable' => in_array("user-member", $aRoles) ? true : false,
					'is_editable' => false,
					'is_viewable' => false,
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								$sSource.'___id',
								array(
									'key' => 'op_user_id',
									'group' => false,
								),
								'adschedule___split___user_id',
								array(
									'key' => 'code',
									'group' => false,
								),
								array(
									'key' => 'user_id',
									'group' => false,
								),
								'code_label',
								'm102_id',
								'split_index',
								array(
									'key' => 'id',
									'group' => false,
								),
								array(
									'key' => 'referer',
									'group' => false,
								),
								'modified',
                                array(
    								'key'=>'___subtr___op___id',
                                ),
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
				'subtr' => true,
				'subtr_function' => function($nId) use ($sSource){
                    $aSourceData = \Application\Model\Common::getResourceById($this->sM, $sSource, $nId);
                    // var_dump($nId);
                    // var_dump($aSourceData);
                    $aM1007 = \Application\Model\Common::getRelationList($this->sM,1007, $nId);
                    $aM1008 = \Application\Model\Common::getRelationList($this->sM,1008, $nId);
                    $aM1009 = \Application\Model\Common::getRelationList($this->sM,1009, $nId, false, true);
                    $aM1015 = \Application\Model\Common::getRelationList($this->sM,1015, $nId);
                    $sUas = count($aM1008) ? join(" / ", \Application\Model\Common::getResourceMetadaList($this->sM, 1008, $aM1008)) : "无定向";

                    $aClick2Uv = \Application\Model\Common::getRelationList($this->sM,LAF_RELATIONTYPE_UV2CLICK, $nId);
                    $aUvids = array_values($aClick2Uv);
                    $sUvids = join("/", $aUvids);


                    
                    $aServers = count($aM1009) ? \Application\Model\Common::getResourceMetadaList($this->sM, 1009, $aM1009) : array();
                    $sSourceType = \Application\Model\Common::getResourceMetaDataLabel($this->sM, 1014, $aSourceData['m1014_id']);
                    // var_dump($aServers);
                    $sCities = "无定向";
                    if (count($aM1007)) {
                    	$aCities = \Application\Model\Common::getResourceMetadaList($this->sM, 1007, $aM1007);

	                    $sCities = "";
                    	foreach ($aCities as $keyTmp2 => $sValTmp2) {
                    		$sCities .= $keyTmp2."/".$sValTmp2.";";
                    	}
                    }
                    $sServers = "无定向";
                    if (count($aServers)) {
	                    $sServers = "";
                    	foreach ($aServers as $keyTmp2 => $sValTmp2) {
                    		$sServers .= $keyTmp2."/".$sValTmp2.";";
                    	}
                    }

                    $sApps = "无定向";
	                if (count($aM1015)) {
	                    $aApps = \Application\Model\Common::getResourceMetadaList($this->sM, 1015, $aM1015);
	                    $sApps = "";
	                	foreach ($aApps as $keyTmp2 => $sValTmp2) {
	                		$sApps .= $keyTmp2."/".$sValTmp2.";";
	                	}
	                }

                    $aTmp = json_decode($aSourceData['schedule'], 1);
                    $sSchedule = is_null($aTmp) ? 'invalbe json format' : '<pre>'.json_encode($aTmp).'</pre>';
                    $sHtml = "<ul style='word-break: break-all;max-width: 900px;white-space: normal;'>
                        <li>url: {$aSourceData['code']}
                        <li>referer: {$aSourceData['referer']}
                        <li>split_fid: {$aSourceData['split_fid']}
                        <li>cookie_code_id: {$aSourceData['cookie_code_id']}
                        <li>ssp ctr: {$aSourceData['sspctr']}
                        <li>UV关联code id: {$sUvids}
                        <li>UV二跳概率: {$aSourceData['uv2_percent']}%
                        <li>split_index: {$aSourceData['split_index']}
                        <li>stable_cookie_percent: {$aSourceData['stable_cookie_percent']}%
                        <li>domaincookie_delete_percent: {$aSourceData['domaincookie_delete_percent']}%
                        <li>servers: {$sServers} 
                        <li>城市: {$sCities}
                        <li>广告类型: {$sSourceType} 
                        <ul>
                        	<li>定向APP: {$sApps}
                        	<li>UA: {$sUas}
                        	</ul>
                        <li>排期: {$sSchedule}
                        </ul>
                    ";
                    return $sHtml;
                }
            ),
		);



		//平台用户, 只能看到自己的数据
		if (in_array("user-member", $aRoles)) {

			$aInput['filters'][] = array(
			    'key' => 'op_user_id', //操作符
			    'op' => 'in', //操作符=,in,like
			    'value' => "(0,".$sUserId.")"//过滤的值
			);
		}
		// 超管账号, 允许看到具体用户
		if (in_array("superadmin", $aRoles)) {
			$aInput['input']['detail']['table'][$sSource]['dimen'][1]['group'] = true;
		}


//        if(m102_id)
       $aReportParams = ($this->getReportParams());
       // var_dump($aReportParams['params']);
       // 		$aFilters = isset($aReportParams['params']) ? json_decode($aReportParams['params'], 1) : array();

		$aInput = $this->_fmtDetailFilters($sSource, $aInput, $this->getReportParams(), array('m102_id', 'user_id', 'split_index'), 
			array($sSource.'___id', 'code', 'referer', 'code_label')
			// array('code_label')
			, array('m102_id' => array('op' => 'in', 'value' => "1")));
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}