<?php
namespace Lur\YcheukfReportExt\Inputs;
class Reportd extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
        $sSource = $this->getResource();
        $aReportParams = $this->getReportParams();
        $nCid = $this->getCid();
        $oSm = $this->getSm();
        $sReturnType = $this->getReturnType();
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($oSm, $oSm->get('zfcuser_auth_service')->getIdentity());

        $aInput = array(
            'filters' => array( //选传. 默认为空, 过滤条件. 
//                array(
//                    'key' => 'user_id', //操作符
//                    'op' => '>=', //操作符=,in,like
//                    'value' => 1000//过滤的值
//                ),
            ),
            'input'=>array(
                'flash'=>array(
                    'type' =>'Multmetric',
                    'typeStyle' => 'bar', 
                    'table' => array(
                        $sSource => array(
                            'dimen' => array(
                                    array(
                                        'key' => 'dd1', //必传. 某个维度的键值, 可直接用表中的字段名
                                        'group'=>true,
                                    ),
                                    array(
                                        'key' => 'user_id', //必传. 某个维度的键值, 可直接用表中的字段名
                                        'group'=>false,
                                    ),
                            ),
                            'metric' => array(
//                                'm1',
                                'md4',
                            ),
                        ),
                    ),
                ),
                'detail'=>array(
                    'type' =>'table',
                    'orderby' =>'dd1 desc',
                    'table' => array(
                        $sSource => array(
                            'dimen' => array(
                                'dd1',
                                'dd2',
                            ),
                            'metric' => array(
                                'md1',
                                'md2',
                                'mdx1_mixfield',
                                'md3',
                                'md4',
                                'mdx2_mixfield',
                            ),
                        ),
                    ),
                ),
            ),
            'output' => array(
                'format' => $sReturnType,
            ),
            'custom' => array(
                'detail_format' => 'table',//table|ul
                'detail_source' => $sSource,
            ),
        );
//var_dump($oSm->get('zfcuser_auth_service')->getIdentity()->getId());
        if(in_array("user-member", $aRoles)){
             array_unshift($aInput['input']['detail']['table'][$sSource]['dimen'], array("key"=>"user_id","group"=>false));
           $aInput['filters'] = array( //选传. 默认为空, 过滤条件. 
                array(
                    'key' => 'user_id', //操作符
                    'op' => '=', //操作符=,in,like
                    'value' => $oSm->get('zfcuser_auth_service')->getIdentity()->getId()//过滤的值
                ),
            );
//                var_dump($aInput['filters']);
        }else{
//            array_unshift($aInput['input']['detail']['table'][$sSource]['dimen'], "user_id");
       }

//        $aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array(), array());
        $aInput = $this->_formatOutput($aInput);

        return $aInput;
    }
}