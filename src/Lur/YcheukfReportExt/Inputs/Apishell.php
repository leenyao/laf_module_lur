<?php
namespace Lur\YcheukfReportExt\Inputs;
class Apishell extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. status为删除状态, 数据库应该有此字段1=>正常,0=>删除
                array(
                    'key' => 'status', //操作符
                    'op' => 'in', //操作符=,in,like
                    'value' => "(1)"//过滤的值
                ),
			),
			'input'=>array(
				'detail'=>array(
					'is_editable' => false,
					'is_viewable' => false,
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								$sSource.'___id',
								array(
                                    'key' => 'status',
                                    'group' => false,
                                ),
								array(
                                    'key' => 'host',
                                    'group' => true,
                                ),
								array(
                                    'key' => 'ssh2_tunnel',
                                    'group' => true,
                                ),
                                'modified',
								'___subtr___op___id',
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
                'subtr' => true,
                'subtr_function' => function($nId) use ($sSource){
                    $aSourceData = \Application\Model\Common::getResourceById($this->sM, $sSource, $nId);
                    // var_dump($nId);
                    // var_dump($aSourceData);
                    $sPort = $aSourceData['host']%10000 + 10000;
                    $sData = $aSourceData['data'];
                    $sHtml = <<<__EOF
                    <h5>脚本命令</h5>
                    {$sData}

                    <h5>ssh反向隧道</h5>
                    <p>30分钟内, 登上目标机器 ly75, 切换至feng的账号
                    <p>ssh root@localhost -p {$sPort} -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no
__EOF;
                    return $sHtml;
                }
            ),
		);
		$aInput = $this->_fmtDetailFilters(
            $sSource, 
            $aInput, 
            $this->getReportParams(), 
            array('status'),  //filterable field
            array('modified'), //fulltext search field
            array('m102_id' => array('op' => 'in', 'value' => "1")) // default filters
        );
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}