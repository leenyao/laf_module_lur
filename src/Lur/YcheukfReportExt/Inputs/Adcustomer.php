<?php
namespace Lur\YcheukfReportExt\Inputs;
class Adcustomer extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
		$sSource = $this->getResource();
		
		$oIdentity = $this->sM->get('zfcuser_auth_service')->getIdentity();
		$sUserId = $oIdentity->getId();
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($this->sM, $oIdentity);

		// var_dump($sUserId);


		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. m101_id为删除状态, 数据库应该有此字段1=>正常,0=>删除
                array(
                    'key' => 'm101_id', //操作符
                    'op' => 'in', //操作符=,in,like
                    'value' => "(1)"//过滤的值
                ),
			),
			'input'=>array(
				'detail'=>array(
					'is_editable' => true,
					'is_viewable' => false,
					'type' =>'table',
					'orderby' =>'modified desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								$sSource.'___id',
								'name',
								array(
                                    'key' => 'm101_id',
                                    'group' => false,
                                ),
								array(
                                    'key' => 'user_id',
                                    'group' => false,
                                ),
								'modified',
            //                     array(
    								// 'key'=>'___subtr___op___id',
            //                     ),
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
				// 'subtr' => true,
            ),
		);


		if (in_array("user-member", $aRoles)) {

			$aInput['filters'][] = array(
			    'key' => 'user_id', //操作符
			    'op' => 'in', //操作符=,in,like
			    'value' => "(0,".$sUserId.")"//过滤的值
			);
		}
		if (in_array("superadmin", $aRoles)) {
			$aInput['input']['detail']['table'][$sSource]['dimen'][] = "user_id";
		}



		$aInput = $this->_fmtDetailFilters(
            $sSource, 
            $aInput, 
            $this->getReportParams(), 
            array('m101_id'),  //filterable field
            array('name'), //fulltext search field
            array('m102_id' => array('op' => 'in', 'value' => "1")) // default filters
        );
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}