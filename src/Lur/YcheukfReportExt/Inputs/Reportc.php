<?php
namespace Lur\YcheukfReportExt\Inputs;
class Reportc extends \Application\YcheukfReportExt\Inputs\Inputs{

	function getInput(){
        $sSource = $this->getResource();
        $aReportParams = $this->getReportParams();
        $nCid = $this->getCid();
        $oSm = $this->getSm();
        $sReturnType = $this->getReturnType();
        $aRoles = \YcheukfCommon\Lib\Functions::getUserRoles($oSm, $oSm->get('zfcuser_auth_service')->getIdentity());

        $aInput = array(
            'filters' => array( //选传. 默认为空, 过滤条件. 
//                array(
//                    'key' => 'user_id', //操作符
//                    'op' => '>=', //操作符=,in,like
//                    'value' => 1000//过滤的值
//                ),
            ),
            'input'=>array(
                'flash'=>array(
                    'type' =>'Multmetric',
                    'typeStyle' => 'bar', 
                    'table' => array(
                        $sSource => array(
                            'dimen' => array(
                                    array(
                                        'key' => 'date', //必传. 某个维度的键值, 可直接用表中的字段名
                                        'group'=>true,
                                    ),
                            ),
                            'metric' => array(
//                                'm1',
                                'mc1',
                            ),
                        ),
                    ),
                ),
                'detail'=>array(
                    'type' =>'table',
                    'orderby' =>'date desc',
                    'table' => array(
                        $sSource => array(
                            'dimen' => array(
                                'date',
                                'dc1',
                                'dc2',
                                'dc3',
                                'dc4',
                            ),
                            'metric' => array(
                                'mc1',
                                'mc2',
                                'mc3',
                            ),
                        ),
                    ),
                ),
            ),
            'output' => array(
                'format' => $sReturnType,
            ),
            'custom' => array(
                'detail_format' => 'table',//table|ul
                'detail_source' => $sSource,
            ),
        );
//var_dump($oSm->get('zfcuser_auth_service')->getIdentity()->getId());
        if(in_array("user-member", $aRoles)){
             array_unshift($aInput['input']['detail']['table'][$sSource]['dimen'], array("key"=>"user_id","group"=>false));
           $aInput['filters'] = array( //选传. 默认为空, 过滤条件. 
                array(
                    'key' => 'user_id', //操作符
                    'op' => '=', //操作符=,in,like
                    'value' => $oSm->get('zfcuser_auth_service')->getIdentity()->getId()//过滤的值
                ),
            );
//                var_dump($aInput['filters']);
        }else{
//            array_unshift($aInput['input']['detail']['table'][$sSource]['dimen'], "user_id");
       }

//        $aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array(), array());
        $aInput = $this->_formatOutput($aInput);

        return $aInput;
    }
}