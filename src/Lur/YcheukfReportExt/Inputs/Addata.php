<?php
namespace Lur\YcheukfReportExt\Inputs;

class Addata extends Inputs{
    function getInput(){
        $sSource = $this->getResource();
        $aReportParams = $this->getReportParams();
        $nCid = $this->getCid();
        $oSm = $this->getSm();
        $sReturnType = $this->getReturnType();


        $aInput = array(
            'filters' => array( //选传. 默认为空, 过滤条件. 
                array(
                    'key' => 'user_id', //操作符
                    'op' => '>=', //操作符=,in,like
                    'value' => 1000//过滤的值
                ),
            ),
            'input'=>array(
                'detail'=>array(
                    'type' =>'table',
                    'orderby' =>'modified desc',
                    'table' => array(
                        $sSource => array(
                            'dimen' => array(
                                array(
                                     'key' => $sSource.'___id', //必传. 某个维度的键值, 可直接用表中的字段名
                                    'group'=>true,
                                ),
//                                'user_id',
                                'm1005_id',
                                'memo',
                                'datapath',
                                'modified',
//                                array(
//                                     'key' => 'user_id_credit', 
//                                ),
//                                array(
//                                     'key' => 'status', //必传. 某个维度的键值, 可直接用表中的字段名
//                                    'thclass'=>array(),
//                                ),
//                                'logo',
                            ),
                        ),
                    ),
                ),
            ),
            'output' => array(
                'format' => $sReturnType,
            ),
            'custom' => array(
                'detail_format' => 'table',//table|ul
                'detail_source' => $sSource,
            ),
        );
        $aInput = $this->_fmtDetailFilters($sSource, $aInput, $aReportParams, array('status','m157_id','m154_id'), array( 'user_id','username'));
        $aInput = $this->_formatOutput($aInput);


        return $aInput;
        
    }

}