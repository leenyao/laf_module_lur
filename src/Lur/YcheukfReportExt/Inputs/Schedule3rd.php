<?php
namespace Lur\YcheukfReportExt\Inputs;
class Schedule3rd extends \Application\YcheukfReportExt\Inputs\Inputs{
	// CREATE TABLE `b_schedule3rd` (
	//   `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	//   `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id. system_user.id',
	//   `token` varchar(250) DEFAULT NULL COMMENT 'token',
	//   `json_data` longtext COMMENT 'json 数据',
	//   `memo` text COMMENT '描述',
	//   `status` tinyint(1) DEFAULT '1' COMMENT '状态 1=>正常;0=>删除',
	//   `created_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
	//   `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	//   PRIMARY KEY (`id`),
	//   KEY `user_id` (`user_id`)
	// ) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COMMENT='第三方排期';

	function getInput(){
		$sSource = $this->getResource();
		$aInput = array(
			'filters' => array( //选传. 默认为空, 过滤条件. 
                // array(
                //     'key' => 'status', //操作符
                //     'op' => 'in', //操作符=,in,like
                //     'value' => "(1)"//过滤的值
                // ),
			),
			'input'=>array(
				'detail'=>array(
					'is_editable' => false,
					'is_viewable' => false,
					'type' =>'table',
					'orderby' =>'created_time desc,  id desc',
					'table' => array(
						$sSource => array(
							'dimen' => array(
								'id',
								array(
                                    'key' => 'status',
                                    'group' => true,
                                ),
								'memo',
								'created_time',
                                array(
    								'key'=>'___subtr___op___id',
                                ),
							),
						),
					),
				),
			),
			'output' => array(
				'format' => $this->getReturnType(),
			),
			'custom' => array(
				'subtr' => true,
				'subtr_function' => function($nId) use ($sSource){
                    $access_token = \YcheukfCommon\Lib\OauthClient::getDbProxyAccessToken($this->sM);
                    $aSourceData = \Application\Model\Common::getResourceById($this->sM, $sSource, $nId);
                    $aOut = json_decode(($aSourceData['json_data']), 1);
                    $sOut = print_r($aOut, 1);
                    $sHtml = "<p/><pre>
                    	{$sOut}
                    	</pre>
                    ";
                    return $sHtml;
                }
            ),
		);
		$aInput = $this->_fmtDetailFilters(
            $sSource, 
            $aInput, 
            $this->getReportParams(), 
            array('status'),  //filterable field
            array('modified'), //fulltext search field
            array('m102_id' => array('op' => 'in', 'value' => "1")) // default filters
        );
		$aInput = $this->_formatOutput($aInput);

		return $aInput;
		
    }
}