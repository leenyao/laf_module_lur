<?php
namespace Lur\YcheukfReportExt\Report\Dictionary;


/**
 * id转label类
 *
 * 负责将db类中获得id转变成相应的label
 *
 * @author   ycheukf@gmail.com
 * @package  Dictionary
 * @access   public
 */
class Id2label extends \YcheukfReportExt\Report\Dictionary\Id2label
{

	public function ALYSchgId2Label($aDimenkey2selected=array(), $aryIdSet=array(), $aConfig=array()){
		$this->addDimonFunc();
		return parent::ALYSchgId2Label($aDimenkey2selected, $aryIdSet, $aConfig);
	}

	/**
	 * 增加字典
	 */
	private function addDimonFunc()
	{
		$sDimonKey = "adschedule___split___user_id";
		$this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
		{
			$aryReturn = array();
			$aryReturn = $this->_getDataFromResource(
                                $sm,
                                $aIds,
                                'adcustomer',
                                'id',
                                'name'
                            );
            return $aryReturn;
		});

        $sDimonKey = "metadata___id";

        $this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
        {
            $aInput = \YcheukfReport\Lib\ALYS\Report\Start::getInput();
            
            $aDataKey = array();
            $bEditable = isset($aInput['input']['detail']['is_editable']) ? $aInput['input']['detail']['is_editable'] : true;
            $bViewable = isset($aInput['input']['detail']['is_viewable']) ? $aInput['input']['detail']['is_viewable'] : true;
            if(preg_match('/___id/i', $sDimonKey))
                $sSource = str_replace('___id', '', $sDimonKey);
            else
                $sSource = $aInput['custom']['source'];
            $nTmpCid = isset($aInput['custom']['cid']) ? $aInput['custom']['cid'] : 0;
            foreach ($aIds as $key => $v) {

                //混淆ID
                $sHashV = $v;
                // $sHashV = \YcheukfCommon\Lib\Functions::encodeLafResouceId($v, $sSource);
                if($bEditable){
                    $aDataKey['edit'] = array(
                        'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
                            'edit'
                        ),
                        'data' => $this->_geturl(
                            'zfcadmin/contentedit',
                            array(
                                'cid' => $nTmpCid,
                                'id' => $sHashV,
                                'type' => $sSource
                            )
                        ),
                        'attributes' =>  array('target' => '_blank'),
                        'type' => 'url',
                        'permission' => array(
                            'zfcadmin/contentedit/' . $sSource,
                        ),
                    );
                };

                // links

                if($bViewable){
                    $aDataKey['view'] = array(
                        'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
                            'view'
                        ),
                        'data' => $this->_geturl(
                            'zfcadmin/contentview',
                            array(
                                'cid' => $nTmpCid,
                                'id' => $sHashV,
                                'type' => $sSource
                            )
                        ),
                        'attributes' => array("target" => "_blank",),
                        'type' => 'url',
                    );
                }
                // var_dump($nTmpCid);
                // var_dump($sHashV);
                if (intval($nTmpCid) == 1018) {//所有的服务器
                    $mRMetadata = \Application\Model\Common::getResourceById($sm, 'metadata', $v);

                    $aDataKey['viewpi3wifi'] = array(
                        'label' => \YcheukfReport\Lib\ALYS\ALYSLang::_(
                            'viewpi3wifi'
                        ),
                        'data' => "/module/lur/piwifi.php?i=".$mRMetadata['resid']."&ei=".\YcheukfCommon\Lib\Functions::encryptBySlat($mRMetadata['resid']),
                        'attributes' => array("target" => "_blank",),
                        'type' => 'url',
                    );
                }

                $sDataKey = $this
                ->_fmtDataCommandByPermission(
                    $sm,
                    $aDataKey
                );

                $aryReturn[$v] = '<span data-command=\''
                    . $sDataKey . '\'>' . $v . '</span>';
            }
            return $aryReturn;

        });


		$sDimonKey = "op_user_id";
		$this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
		{
			$aryReturn = array();
			$aryReturn = $this->_getDataFromResource(
                                $sm,
                                $aIds,
                                'user',
                                'id',
                                'display_name'
                            );
            return $aryReturn;
		});

        
        $sDimonKey = "host";
        $this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
        {
            $aReturn = array();
            foreach ($aIds as $sId) {
                $sLabel = \Application\Model\Common::getResourceMetaDataLabel($sm, LAF_METADATATYPE_ADRSERVER, $sId);
                if ($sLabel)
                    $aReturn[$sId] = $sId."/".\YcheukfReport\Lib\ALYS\ALYSLang::_($sLabel).",".(10000+$sId%10000);
                else
                    $aReturn[$sId] = empty($sId) ? "" : $sId;
            }
                // var_dump($aReturn);

            return $aReturn;
        });
		$sDimonKey = "systembatchjobs___split___user_id";
		$this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
		{
			$aryReturn = array();
			$aryReturn = $this->_getDataFromResource(
                                $sm,
                                $aIds,
                                'adcustomer',
                                'id',
                                'name'
                            );
            return $aryReturn;
		});
		$sDimonKey = "user_id";
		$this->addDimenTrigger($sDimonKey, function ($aIds, $sm) use ($sDimonKey)
		{
			// var_dump($aIds);
			$aryReturn = array();
			$aryReturn = $this->_getDataFromResource(
                                $sm,
                                $aIds,
                                'user',
                                'id',
                                'display_name'
                            );
            return $aryReturn;
		});
	}
}
