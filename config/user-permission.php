<?php
/*
权限配置列表, 配置后需要执行 /app/bin/script/reset_user_permission.php 以重置现有配置
 */

return array(
	"user-member" => array(
		"resources" => array(
			"adschedule",
			"system_batch_jobs",
			"adcustomer",
		), 
		"custom" => array(
			"lur/default",
		), 
	),

);