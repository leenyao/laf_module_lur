<?php

include_once __DIR__."/defined.php";
include_once "/app/config/autoload/local.php";
$a = array(
    'dashboardroute_role2action' => array(
        'guest' => array("home"),
        'user-member' => array("zfcadmin/contentlist", array('type' => 'system_batch_jobs', 'cid' => 1)),
        'superadmin' => array("zfcadmin/contentlist", array('type' => 'adschedule', 'cid' => 0)),
    ),
    'user-permission' => require(__DIR__."/user-permission.php"),


    'ottuas' => [35,34,33,32,31,30,29,26,25,24,23,22,21,20,19,18,14,13,12,11,10,9,8,7],


    'translator' => array(
        'locale' => 'zh_CN',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(__DIR__ . '/../view'),
        'template_map' => array(
            'admin/login/layout' => __DIR__ . '/../view/layout/layout.phtml',
        ),
    ),
    "adr_server_heartbeat_interaltime" => 1200,//adr心跳报警时间
    "us_heartbeat_interaltime" => 1800,//us心跳报警时间
    'adr_report_url' => 'http://'.LAF_WEBHOST.'/base',
    "ipproxy_url" => "http://".LAF_WEBHOST."/api/ip?token=13962a512b88f0801b368b54dddc0509&debug=1",
    'leenyao_proxyip_mid' => array(4, 5),//
    // 'adr_acesstoken' => array('480024f3496966c64f9f8c98efc657fa'),
    "apiservice" => array(
        "customer" =>array(
            

            'healthcheck' => array(
                "class"=>"\Lur\Restapi\Healthcheck", 
                "restfulflag"=>1,  
                'memo'=>'healthcheck',
                "d:entity"=>array(
                ),
            ),
            'timespan' => array(
                "class"=>"\Lur\Restapi\Timespan", 
                "restfulflag"=>1,  
                'memo'=>'timespan',
                "d:entity"=>array(
                    "timespan" => array(
                        "d:type"=>"int",
                        "d:memo"=>"city",
                    ),
                ),
            ),
            'adrserver' => array(
                "class"=>"\Lur\Restapi\Adrserver", 
                "restfulflag"=>1,  
                'memo'=>'adrserver',
                "d:entity"=>array(
                    "timespan" => array(
                        "d:type"=>"int",
                        "d:memo"=>"city",
                    ),
                ),
            ),
            'fakeip' => array(
                "class"=>"\Lur\Restapi\Fakeip", 
                "restfulflag"=>1,  
                'memo'=>'timespan',
                "d:entity"=>array(
                    "timespan" => array(
                        "d:type"=>"int",
                        "d:memo"=>"city",
                    ),
                ),
            ),
            'iacip' => array(
                "class"=>"\Lur\Restapi\Iacip", 
                "restfulflag"=>1,  
                'memo'=>'timespan',
                "d:entity"=>array(
                    "timespan" => array(
                        "d:type"=>"int",
                        "d:memo"=>"city",
                    ),
                ),
            ),
            'proxyip' => array(
                "class"=>"\Lur\Restapi\Proxyip", 
                "restfulflag"=>1,  
                'memo'=>'proxyip',
                "d:entity"=>array(
                    "m1007" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"city",
                        "d:resource"=>"metadata",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                            ),
                            'label' => array(
                                "d:type"=>"string",
                            ),
                        ),

                    ),
                ),
            ),
            'adcountermq' => array(
                "class"=>"\Lur\Restapi\Adcountermq", 
                "restfulflag"=>1,  
                'memo'=>'Adcountermq',
                "d:entity"=>array(
                    "data" => array(
                        "d:type"=>"list-entity",
                        "d:entity" => array(
                            'key' => array(
                                "d:type"=>"string",
                            ),
                            'count' => array(
                                "d:type"=>"int",
                            ),
                        ),

                    ),
                ),
            ),
            'addtional' => array(
                "class"=>"\Lur\Restapi\Addtional", 
                "restfulflag"=>1,  
                'memo'=>'addtional',
                "d:entity"=>array(
                    "data" => array(
                        "d:type"=>"list-entity",
                        "d:entity" => array(
                            'key' => array(
                                "d:type"=>"string",
                            ),
                            'count' => array(
                                "d:type"=>"int",
                            ),
                        ),

                    ),
                ),
            ),
            'schedule3rd' => array(
                "class"=>"\Lur\Restapi\Schedule3rd", 
                "restfulflag"=>1,  
                'memo'=>'Schedule3rd',
                "d:entity"=>array(
                    "coopId" => array(
                        "d:type"=>"string",
                        "d:memo"=>"coopId",
                        "d:resource"=>"schedule3rd",
                        "d:alias"=>"user_id",
                    ),
                    "token" => array(
                        "d:type"=>"string",
                        "d:memo"=>"token",
                        "d:resource"=>"schedule3rd",
                    ),
                    "adver" => array(
                        "d:type"=>"string",
                        "d:memo"=>"adver",
                        "d:resource"=>"schedule3rd",
                        "d:alias"=>"json_data",
                    ),
                ),
            ),
            'adrconfig' => array(
                "class"=>"\Lur\Restapi\Adrconfig", 
                "restfulflag"=>1,  
                'memo'=>'adrconfig',
                "d:entity"=>array(
                    "m1007" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"city",
                        "d:resource"=>"metadata",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                            ),
                            'label' => array(
                                "d:type"=>"string",
                            ),
                        ),

                    ),
                ),
            ),
            'usstats' => array(
                "class"=>"\Lur\Restapi\Usstats", 
                "restfulflag"=>1,  
                'memo'=>'usstats',
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"int",
                        "d:memo"=>"id",
                        "d:resource"=>"usstats",
                    ),
                    "data" => array(
                        "d:type"=>"string",
                        "d:post-required" => true,
                        "d:resource"=>"usstats",
                        "d:memo"=>"stats json data",
                    ),
                ),
            ),
            'adrstats' => array(
                "class"=>"\Lur\Restapi\Adrstats", 
                "restfulflag"=>1,  
                'memo'=>'adrstats',
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"int",
                        "d:memo"=>"id",
                        "d:resource"=>"adrstats",
                    ),
                    "host" => array(
                        "d:type"=>"string",
                        "d:post-required" => true,
                        "d:memo"=>"host",
                        "d:resource"=>"adrstats",
                    ),
                    "type" => array(
                        "d:type"=>"int",
                        "d:post-required" => true,
                        "d:memo"=>"type",
                        "d:resource"=>"adrstats",
                    ),
                    "data" => array(
                        "d:type"=>"string",
                        "d:post-required" => true,
                        "d:resource"=>"adrstats",
                        "d:memo"=>"stats json data",
                    ),
                ),
            ),
            'adschedule' => array(
                "class"=>"\Lur\Restapi\Adschedule", 
                "restfulflag"=>1,  
                'memo'=>'adschedule',
                "d:entity"=>array(
                    "id" => array(
                        "d:type"=>"int",
                        "d:memo"=>"id",
                    ),
                    "cookie_code_id" => array(
                        "d:type"=>"int",
                        "d:memo"=>"cookie_code_id",
                    ),
                    "m1023_id" => array(
                        "d:type"=>"int",
                        "d:memo"=>"m1023_id",
                    ),
                    "cookie_domain" => array(
                        "d:type"=>"string",
                        "d:memo"=>"cookie_domain",
                    ),
                    "user_id" => array(
                        "d:type"=>"int",
                        "d:memo"=>"user_id",
                    ),
                    "sourcetype" => array(
                        "d:type"=>"int",
                        "d:memo"=>"",
                        "d:alias"=>"m1014_id",
                    ),
                    "ctr" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                        "d:alias"=>"sspctr",
                    ),
                    "sspmzid" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                    ),

//                    "city" => array(
//                        "d:type"=>"string",
//                        "d:memo"=>"city",
//                    ),
                    "city" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"city",
                        "d:resource"=>"system_relation_1007",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                                "d:alias"=>"cid",
                            ),
                            // 'label' => array(
                            //     "d:type"=>"string",
                            //     "d:alias"=>"clabel",
                            // ),
                        ),

                    ),
                    "apps" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"apps",
                        "d:resource"=>"system_relation_1015",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                                "d:alias"=>"cid",
                            ),
                            // 'label' => array(
                            //     "d:type"=>"string",
                            //     "d:alias"=>"clabel",
                            // ),
                        ),

                    ),
                    // "servers" => array(
                    //     "d:type"=>"list-entity",
                    //     "d:memo"=>"ua",
                    //     "d:resource"=>"system_relation_1009",
                    //     "d:entity" => array(
                    //         'id' => array(
                    //             "d:type"=>"int",
                    //             "d:alias"=>"cid",
                    //         ),
                    //         // 'label' => array(
                    //         //     "d:type"=>"string",
                    //         //     "d:alias"=>"clabel",
                    //         // ),
                    //     ),

                    // ),
                    "ua_type" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"ua",
                        "d:resource"=>"system_relation_1008",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                                "d:alias"=>"cid",
                            ),
                            // 'label' => array(
                            //     "d:type"=>"string",
                            //     "d:alias"=>"clabel",
                            // ),
                        ),

                    ),
                    "code_label" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                        "d:alias"=>"code_label",
                    ),
                    "url" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                        "d:alias"=>"code",
                    ),
                    
                    "uv2ndurl" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                        "d:alias"=>"uv2ndurl",
                    ),
                    "uv2_percent" => array(
                        "d:type"=>"int",
                        "d:memo"=>"",
                    ),
                    "uvcodeid" => array(
                        "d:type"=>"list-entity",
                        "d:memo"=>"ua",
                        "d:resource"=>"system_relation_1017",
                        "d:entity" => array(
                            'id' => array(
                                "d:type"=>"int",
                                "d:alias"=>"cid",
                            ),
                            // 'label' => array(
                            //     "d:type"=>"string",
                            //     "d:alias"=>"clabel",
                            // ),
                        ),

                    ),
                    "stable_cookie_percent" => array(
                        "d:type"=>"int",
                        "d:memo"=>"",
                        "d:alias"=>"stable_cookie_percent",
                    ),
                    "domaincookie_delete_percent" => array(
                        "d:type"=>"int",
                        "d:memo"=>"",
                        "d:alias"=>"domaincookie_delete_percent",
                    ),
                    "schedule_md5" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                        "d:alias"=>"schedule_md5",
                    ),
                    "schedule" => array(
                        "d:type"=>"json_string",
                        "d:memo"=>"",
                    ),
                    "rtbdata" => array(
                        "d:type"=>"json_string",
                        "d:memo"=>"",
                    ),
                    "referer" => array(
                        "d:type"=>"json_string",
                        "d:memo"=>"",
                    ),
                    "split_index" => array(
                        "d:type"=>"int",
                        "d:memo"=>"",
                    ),
                    "sspid" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                    ),
                    "useiac" => array(
                        "d:type"=>"int",
                        "d:memo"=>"",
                    ),
                    "sh_vid" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                    ),
                    "sh_useapiuuid" => array(
                        "d:type"=>"int",
                        "d:memo"=>"",
                    ),
                    "hunan_cxids" => array(
                        "d:type"=>"string",
                        "d:memo"=>"",
                    ),
                ),
            ),
        ),
        "resources" =>array(
            "reporta" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"b_report_a", 'memo'=>''),//\n\
            "reportb" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"b_report_b", 'memo'=>''),//\n\
            "reportc" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"b_report_c", 'memo'=>''),//\n\
            "reportd" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"b_report_d", 'memo'=>''),//\n\
            "reporte" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"b_report_e", 'memo'=>''),//\n\
            "addata" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"b_addata", 'memo'=>''),//\n\
            "adcustomer" => array("driver"=>"mysql", "restfulflag"=>1, "table"=>"b_adcustomer", 'memo'=>''),//\n\

            "adrstats" => array("driver"=>"mysql", "table"=>"b_adrstats", 'memo'=>''),//\n\
            "usstats" => array("driver"=>"mysql", "table"=>"b_usstats", 'memo'=>''),//\n\
            "adschedule" => array("driver"=>"mysql", "table"=>"b_adschedule", 'memo'=>''),//\n\
            "schedule3rd" => array("driver"=>"mysql", "table"=>"b_schedule3rd", 'memo'=>''),//\n\
        ),        
    ),
    "report_field_mapper" => array(
        'reporta' => array(
            'user_id' => 0,
            'date' => 1,
            'd1' => 2,
            'd2' => 3,
            'm1' => 4,
            'm2' => 5,
            'm3' => 6,
        ),
        'reportb' => array(
            'user_id' => 0,
            'date' => 1,
            'd1' => 2,
            'd2' => 3,
            'm1' => 4,
            'm2' => 5,
            'm3' => 6,
        ),
        'reportd' => array(
            'user_id' => 0,
            'date' => 1,
            'dd1' => 2,
            'dd2' => 3,
            'md1' => 4,
            'md2' => 5,
            'md3' => 6,
            'md4' => 7,
        ),
        'reporte' => array(
            'user_id' => 0,
            'date' => 1,
            'de1' => 2,
            'de2' => 3,
            'me1' => 4,
            'me2' => 5,
            'me3' => 6,
            'me4' => 7,
        ),
    ),
    //路由与layout映射关系
    'viewtemplate' => array(
    ),

    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /demo/:controller/:action
            'lur' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/lur',
                    'defaults' => array(
                        'controller'    => 'Lur\Controller',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller][/:action]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Lur\Controller',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Lur\Controller\Index' => 'Lur\Controller\IndexController',
            'Lur\Controller\Admin' => 'Lur\Controller\AdminController',
            'Lur\Controller\Distribution' => 'Lur\Controller\DistributionController',
            'Lur\Controller\Proxyip' => 'Lur\Controller\ProxyipController',
            'lf_admincontent' => 'Lur\Service\AdminContent',
        ),
    ),




    //第三方排期消息队列, 已经废弃
    '3rdschedule' => array(
        "redismq" => array(
            "host"=>'10.30.200.3',
            "port"=> 9000,
            "db"=> 0
        ),
        "errorcode" => array(
            100001 => "redis error",
            100002 => "unkonw error",
            100003 => "token unavailable",
            100004 => "unavailable appid",
            100005 => "unavailable post-action",
            100006 => "unavailable post-data",
            100007 => "unavailable query 'action'",
            100008 => "too many urls",
            100009 => "unavailable post-data",

        ),
    ),

    
    'redisCacheClearConfig' => [
      LAF_CACHE_BLACKIPS_HIS_124 =>[
        'format' => '[key][date|Ymd]',
        'type' => 'hash',
        'nActiveDay' => 5,
      ],
      LAF_CACHE_VPNFAILD_HISTORY_120 =>[
        'format' => '[key][date|Ymd]',
        'type' => 'list',
        'nActiveDay' => 5,
      ],
      LAF_CACHE_COUNTER_SSP_DAYSET4SHOW_409 =>[
        'format' => '[key][date|Ymd]',
        'type' => 'hash',
        'nActiveDay' => 5,
      ],
      LAF_CACHE_COUNTER_DAYSET4SHOW_408 =>[
        'format' => '[key][date|Ymd]',
        'type' => 'hash',
        'nActiveDay' => 5,
      ],
      LAF_CACHE_VPNIPS_HIS_125 =>[
        'format' => '[key][date|Ymd]',
        'type' => 'hash',
        'nActiveDay' => 5,
      ],
      LAF_CACHE_ADDTIONAL_DATEINIT_315 =>[
        'format' => '[key][date|Ymd]',
        'type' => 'set',
        'nActiveDay' => 5,
      ],
    ],
    'logClearConfig' =>array(
        '/app/data/addtionallogs_db' => [
            "nActiveDay" => 5,      
            "nCompressTime" => 20,  
        ],
        '/app/data/addtionallogs_adridhist_get' => [
            "nActiveDay" => 5,      
            "nCompressTime" => 20,  
        ],
        '/app/data/addtionallogs_adridhist_send' => [
            "nActiveDay" => 5,      
            "nCompressTime" => 20,  
        ],
        '/app/data/adrresult' => [
            "nActiveDay" => 5,      
            "nCompressTime" => 10,  
        ],
        '/app/public/tmpupload' => [
            "nActiveDay" => 200,      
            "nCompressTime" => 30,  
        ],
        '/app/data/addtionallogs_get' => [
            "nActiveDay" => 5,      
            "nCompressTime" => 20,  
        ],
        '/app/data/addtionallogs_send' => [
            "nActiveDay" => 5,      
            "nCompressTime" => 20,  
        ],
        '/app/data/dbtablerecoreds/b_adcounter' => [
            "nActiveDay" => 2,      
            "nCompressTime" => 200,  
        ],
        '/app/data/dbtablerecoreds/b_adcounter_ssp' => [
            "nActiveDay" => 2,      
            "nCompressTime" => 200,  
        ],
        '/app/data/dbtablerecoreds/b_report_e' => [
            "nActiveDay" => 2,      
            "nCompressTime" => 200,  
        ],
        '/app/data/dbtablerecoreds/system_trigger_log' => [
            "nActiveDay" => 2,      
            "nCompressTime" => 200,  
        ],
    ),




    // 需要定时清理的表
    'intervalClearTables' => [
        ['table'=> 'b_report_e', 'datefiled'=>'date', 'activeDays'=>170],
        ['table'=> 'b_adcounter', 'datefiled'=>'date', 'activeDays'=>200],
        ['table'=> 'system_trigger_log', 'datefiled'=>'crt_timestamp', 'activeDays'=>200],
        ['table'=> 'b_adcounter_ssp', 'datefiled'=>'date', 'activeDays'=>200],

        
    ],

    // adr机器中活着, 但不分配vpn账号的adrid
    // bf 下架的adr
    'adridAliveNoCallVpn' => [
        50,51,52,53,54,55,56,57,58,59,96,97,60,123,124,127,129,135,143,153,154,155,156,157,158,159,160,161,162,227,228,229,230,231,322,323,324,325,328,329,330,331,332,333,334,337,338,340,367,368,369,518,98,99,100,101,102,103,104,105,106,107,698,699,700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722
    ],


//  宝鸡,汉中,铜川,榆林,渭南,安康,咸阳,商洛,延安,西安
    // 南京,合肥,厦门,苏州,武汉,杭州,大连,北京,天津,济南
    // 常驻需要拨号的vpn城市, 只有前十个有效
    'keepVpnCity' => array(
        array( 'label'=> "青岛", 'count'=>3),
        array( 'label'=> "宁波", 'count'=>3),
        array( 'label'=> "汕头", 'count'=>3),

        array( 'label'=> "北京", 'count'=>3),   
        array( 'label'=> "海口", 'count'=>3),
        array( 'label'=> "常州", 'count'=>3),
        array( 'label'=> "济宁", 'count'=>3),
        array( 'label'=> "廊坊", 'count'=>3),
        array( 'label'=> "南充", 'count'=>3),
        array( 'label'=> "乌鲁木齐", 'count'=>3),


        // -----------------分割线以下的不会被排到------------------------------

        array( 'label'=> "成都", 'count'=>3),
        array( 'label'=> "昆明", 'count'=>3),
        array( 'label'=> "沈阳", 'count'=>3),

        array( 'label'=> "石家庄", 'count'=>3),
        array( 'label'=> "徐州", 'count'=>3),
        array( 'label'=> "长沙", 'count'=>3),
        array( 'label'=> "郑州", 'count'=>3),
        array( 'label'=> "杭州", 'count'=>3),
        array( 'label'=> "厦门", 'count'=>3),


        array( 'label'=> "武汉", 'count'=>6),
        array( 'label'=> "合肥", 'count'=>6),
        array( 'label'=> "三亚", 'count'=>3),

        array( 'label'=> "南京", 'count'=>23),
        array('label'=> "苏州", 'count'=>2),
        array( 'label'=> "大连", 'count'=>2),
        array( 'label'=> "天津", 'count'=>2),
        array( 'label'=> "济南", 'count'=>2),

        array( 'label'=> "上海", 'count'=>5),
        array( 'label'=> "重庆", 'count'=>5),

        array( 'label'=> "南宁", 'count'=>3),

        array( 'label'=> "绍兴", 'count'=>3, 'selected_store'=>['xianfeng']),
        array( 'label'=> "金华", 'count'=>3, 'selected_store'=>['xianfeng']),
        array( 'label'=> "衢州", 'count'=>3, 'selected_store'=>['xianfeng']),
        array( 'label'=> "丽水", 'count'=>3, 'selected_store'=>['xianfeng']),
        array( 'label'=> "湖州", 'count'=>3, 'selected_store'=>['xianfeng']),
        array( 'label'=> "舟山", 'count'=>3),
        array( 'label'=> "温州", 'count'=>3),
        array( 'label'=> "台州", 'count'=>3, 'selected_store'=>['xianfeng']),
        array( 'label'=> "嘉兴", 'count'=>3, 'selected_store'=>['xianfeng']),


        array( 'label'=> "淮安", 'count'=>3),

        array( 'label'=> "贵阳", 'count'=>5),
        array( 'label'=> "长春", 'count'=>5, 'selected_store'=>['qiangzi']),
        array( 'label'=> "西安", 'count'=>8),
        
        array( 'label'=> "太原", 'count'=>5),

        array( 'label'=> "宝鸡", 'count'=>3),
        array( 'label'=> "汉中", 'count'=>3),
        array( 'label'=> "铜川", 'count'=>3),
        array( 'label'=> "榆林", 'count'=>3),
        array( 'label'=> "渭南", 'count'=>3),
        array( 'label'=> "安康", 'count'=>3),
        array( 'label'=> "咸阳", 'count'=>3),
        array( 'label'=> "商洛", 'count'=>3),
        array( 'label'=> "延安", 'count'=>3),

        array( 'label'=> "绥化", 'count'=>3),
        array( 'label'=> "哈尔滨", 'count'=>3),
        array( 'label'=> "佳木斯", 'count'=>3),
        array( 'label'=> "牡丹江", 'count'=>3),
        array( 'label'=> "温州", 'count'=>3),

        array( 'label'=> "无锡", 'count'=>5),

        array( 'label'=> "贵港", 'count'=>3),
        array( 'label'=> "达州", 'count'=>3),
        array( 'label'=> "桂林", 'count'=>5),
        
        array( 'label'=> "广东省", 'count'=>5),
        array( 'label'=> "承德", 'count'=>3),
        array( 'label'=> "聊城", 'count'=>2),
        array( 'label'=> "潍坊", 'count'=>5),

        array( 'label'=> "大理", 'count'=>5, 'selected_store'=>['xianfeng1']),
        array( 'label'=> "西双版纳", 'count'=>5, 'selected_store'=>['xianfeng1']),
        array( 'label'=> "楚雄", 'count'=>3, 'selected_store'=>['xianfeng1']),
        array( 'label'=> "昭通", 'count'=>3, 'selected_store'=>['xianfeng1']),
        array( 'label'=> "保山", 'count'=>3, 'selected_store'=>['xianfeng1']),
        array( 'label'=> "临沧", 'count'=>3, 'selected_store'=>['xianfeng1']),
        array( 'label'=> "玉溪", 'count'=>3, 'selected_store'=>['xianfeng1']),
        array( 'label'=> "曲靖", 'count'=>3, 'selected_store'=>['xianfeng1']),
        array( 'label'=> "丽江", 'count'=>3, 'selected_store'=>['xianfeng1']),

        
        array( 'label'=> "深圳", 'count'=>10),


        array( 'label'=> "江门", 'count'=>3),




        array( 'label'=> "菏泽", 'count'=>5),
        array( 'label'=> "枣庄", 'count'=>5),
        array( 'label'=> "泰安", 'count'=>5),
        array( 'label'=> "日照", 'count'=>5),
        array( 'label'=> "滨州", 'count'=>5),
        array( 'label'=> "莱芜", 'count'=>5),
        array( 'label'=> "威海", 'count'=>5),
        array( 'label'=> "淄博", 'count'=>5),
        array( 'label'=> "德州", 'count'=>5),
        array( 'label'=> "烟台", 'count'=>6),
        array( 'label'=> "东营", 'count'=>5),





        
        
        array( 'label'=> "兰州", 'count'=>5),

        array( 'label'=> "潮州", 'count'=>2),
        array( 'label'=> "佛山", 'count'=>6, 'selected_store'=>['kun658_80', 'xianfeng1']),


        array( 'label'=> "广州", 'count'=>10, 'selected_store'=>['kun658_80']),

        array( 'label'=> "台州", 'count'=>5),

        

        

        array( 'label'=> "中山", 'count'=>6),

        
        array( 'label'=> "泉州", 'count'=>5),

        array( 'label'=> "东莞", 'count'=>6, 'selected_store'=>['kun658_80', 'xianfeng1']),
        array( 'label'=> "福州", 'count'=>5),

        
        array( 'label'=> "南昌", 'count'=>5),


        



        array( 'label'=> "株洲", 'count'=>4),

        array( 'label'=> "怀化", 'count'=>4),
        array( 'label'=> "郴州", 'count'=>5),
        array( 'label'=> "张家界", 'count'=>5),
        array( 'label'=> "邵阳", 'count'=>5),
        array( 'label'=> "德阳", 'count'=>5),
        array( 'label'=> "攀枝花", 'count'=>5),
        array( 'label'=> "益阳", 'count'=>5),
        array( 'label'=> "广安", 'count'=>5),
        array( 'label'=> "遂宁", 'count'=>5),
        array( 'label'=> "内江", 'count'=>5),
        array( 'label'=> "资阳", 'count'=>5),
        array( 'label'=> "自贡", 'count'=>5),
        array( 'label'=> "绵阳", 'count'=>5),
        array( 'label'=> "泸州", 'count'=>5),
        array( 'label'=> "雅安", 'count'=>3),
        array( 'label'=> "湘潭", 'count'=>2),


        
        array( 'label'=> "衡阳", 'count'=>4),
        array( 'label'=> "常德", 'count'=>4),

        array( 'label'=> "岳阳", 'count'=>4),

        array( 'label'=> "巴中", 'count'=>3),
        array( 'label'=> "广元", 'count'=>3),
        array( 'label'=> "眉山", 'count'=>3),
        array( 'label'=> "宜宾", 'count'=>3),
        array( 'label'=> "乐山", 'count'=>3),

        array( 'label'=> "临沂", 'count'=>3),
        array( 'label'=> "锦州", 'count'=>5),
        array( 'label'=> "荆州", 'count'=>2),
        array( 'label'=> "镇江", 'count'=>2),
        array( 'label'=> "龙岩", 'count'=>2),


        array( 'label'=> "惠州", 'count'=>2),
        array( 'label'=> "邯郸", 'count'=>2),

        

        array('label'=> "珠海", 'count'=>2),
        array( 'label'=> "揭阳", 'count'=>2),


        array( 'label'=> "石家庄", 'count'=>2),
        array( 'label'=> "南通", 'count'=>2),
        array( 'label'=> "南阳", 'count'=>2),
        array( 'label'=> "景德镇", 'count'=>2),
        array( 'label'=> "盐城", 'count'=>2),
        array( 'label'=> "许昌", 'count'=>2),
        array( 'label'=> "柳州", 'count'=>2),
        array( 'label'=> "咸阳", 'count'=>2),
        array( 'label'=> "湖州", 'count'=>2),
        
        
        array( 'label'=> "全国混拨", 'count'=>20, 'adrids'=>[42], 'selected_store'=>['xx3']),

        
    ),

    // adr-regertion-mapping
    'aAdrRegistrationNameMapping' => array(
        "苏州市/180.97.70.13" => "BF-13",
        "180.97.70.13" => "BF-13",
        "苏州市/180.97.81.4" => "BF-4",
        "180.97.81.26" => "BF-26",
        "10.30.200.26" => "BF-26",
        "全球/172.20.0.3" => "LY-75",
        "172.20.0.3" => "LY-75",
        "172.24.0.2" => "LY-75",
        "180.97.80.75" => "LY-75", 
        "180.97.81.191" => "LY-191",
        "10.30.200.191" => "LY-191",
        "180.97.81.5" => "LY-191",
        "10.30.200.40" => "BF-40",
        "180.97.81.40" => "BF-40",
        "180.97.80.12" => "LY-12",
        "10.30.200.108" => "LY-12",

        "10.30.200.32" => "LY-32",
        "10.30.200.33" => "LY-33",
        "10.30.200.34" => "LY-34",
        "10.30.200.35" => "LY-35",
        "10.30.200.53" => "LY-53",
        "10.30.200.69" => "LY-69",
        
        "10.30.200.76" => "LY-76",
        "10.30.200.77" => "LY-77",
        "10.30.200.80" => "LY-80",
        "10.30.200.72" => "LY-72",
        "10.30.200.73" => "LY-73",
        "10.30.200.111" => "BF-111",
        "10.30.200.18" => "BF-18",


        "10.30.200.86" => "LY-86",
        "10.30.200.99" => "LY-99",
        "10.30.200.110" => "LY-110",
        "10.30.200.79" => "LY-111",

        "10.30.200.128" => "LY-128",
        "10.30.200.117" => "LY-117",

        
        "10.30.200.4" => "LY-4",
        "180.97.81.4" => "LY-4",
        "10.30.200.8" => "LY-8",
        "180.97.81.8" => "LY-8",
        "10.30.200.3" => "LY-3",
        "180.97.81.3" => "LY-3",

        "10.30.200.13" => "LY-13",
        "180.97.81.13" => "LY-13",
        
        "10.30.200.114" => "LY-114",
        "10.30.200.94" => "LY-94",

    ),


    'adr_remoteip_api' => 'http://121.41.98.149/ip.php',
    
    

    // 补量池子
    'addtionalredis' => array(
        "host"=>'10.30.200.3',
        "port"=> 9000,
        "db"=> 0
    ),


    // adr-truck 自动升级状态
    'adr-truck-releasable' => 0,

    //定时脚本
    'crondjob' => array(

        /*
            为每个脚本创建一个计时器, 当执行时间到达时间戳时才会执行脚本, 秒
            array(
                '脚本文件名1' => 时间戳,
                '脚本文件名2' => 时间戳,
            )
        */      
        'aGlobalScriptIndex' => array(
            
            // "GenerationProxIp" => array(__DIR__.'/../src/Lur/crondjob/GenerationProxIp.php', 10, "生成IP列表"),//批量更新排期脚本
            "firealarm" => array(__DIR__.'/../src/Lur/crondjob/FireAlarmer.php', 10, "报警器-检查ADR心跳"),//消防报警器

            "sohusspplan" => array(__DIR__.'/../src/Lur/crondjob/Sohusspplan.php', 10, "SH排期自动更新"),//

            "UpdateProxy" => array(__DIR__.'/../src/Lur/crondjob/UpdateProxy.php', 120, "UpdateProxy"),//更新代理配置

            "Mgscheduleapi" => array(__DIR__.'/../src/Lur/crondjob/Mgscheduleapi.php', 900, "芒果排期自动更新api"),//消防报警器

            "FireAlarmerVpnAccFaild" => array(__DIR__.'/../src/Lur/crondjob/FireAlarmerVpnAccFaild.php', 3600*8, "报警器-检查VPN拨号状态"),//消防报警器
            "FireAlarmerUs" => array(__DIR__.'/../src/Lur/crondjob/FireAlarmerUs.php', 10, "报警器-检查US状态"),//消防报警器
            "FireAlarmerHourData" => array(__DIR__.'/../src/Lur/crondjob/FireAlarmerHourData.php', 1800, "报警器-检查小时数据是否有增加"),//消防报警器


            "UpdateCache" => array(__DIR__.'/../src/Lur/crondjob/UpdateCache.php', 2, "定期更新缓存"),//定期更新缓存
            "ClearExpiredAdSchedule" => array(__DIR__.'/../src/Lur/crondjob/ClearExpiredAdSchedule.php', 3600, "清理过期排期"),//每天清理一遍过期排期
            "importads" => array(__DIR__.'/../src/Lur/crondjob/ImportAdschedule.php', 2, "导入排期"),//批量更新排期脚本
            "ImportAdschedule3rdV2" => array(__DIR__.'/../src/Lur/crondjob/ImportAdschedule3rdV2.php', 2, "将第三方上传的api存入排期"),//批量更新排期脚本
            "UpdateAdschedule" => array(__DIR__.'/../src/Lur/crondjob/UpdateAdschedule.php', 300, "更新排期缓存"),//定期排期缓存
            "UpdateVpnAccout" => array(__DIR__.'/../src/Lur/crondjob/UpdateVpnAccout.php', 10, "根据缺量城市分配VPN账号"),

            // "importads3rd" => array(__DIR__.'/../src/Lur/crondjob/ImportAdschedule3rd.php', 2, "导入第三方排期"),//批量更新排期脚本
            "importadrreports" => array(__DIR__.'/../src/Lur/crondjob/ImportAdrData.php', "H6", "导入adr报表数据"),//每天清理一遍过期排期 && 重新更新缓存

            "ClearExpiredLogs" => array(__DIR__.'/../src/Lur/crondjob/ClearExpiredLogs.php', "H3", "打包过期日志"),//每天清理一遍过期排期 && 重新更新缓存

            "ClearTableRecords" => array(__DIR__.'/../src/Lur/crondjob/ClearTableRecords.php', "H4", "将过期的日志数据刷进磁盘"),//每天清理一遍大表

            
            "saveUuidCounter" => array(__DIR__.'/../src/Lur/crondjob/saveUuidCounter.php', 3600, "将uuid counter 数据存进表"),//每天清理一遍过期排期 && 重新更新缓存

            
            "firealarm_adschedule" => array(__DIR__.'/../src/Lur/crondjob/FireAlarmerScheduleFinished.php', "H7", "报警器-检查排期是否完成"),//消防报警器
            "FireAlarmerVpnAcc" => array(__DIR__.'/../src/Lur/crondjob/FireAlarmerVpnAcc.php', "H9", "报警器-检查VPN账号是否即将到期"),//消防报警器
            // "firealarm_iac" => array(__DIR__.'/../src/Lur/crondjob/FireAlarmerIacapi.php', 600, "消防报警器-IAC的API是否正常"),//消防报警器
            // "firealarm_ip138" => array(__DIR__.'/../src/Lur/crondjob/FireAlarmerIp138api.php', 600, "消防报警器-IP138的API是否正常"),//消防报警器
            "updadcounter" => array(__DIR__.'/../src/Lur/crondjob/UpdateAdscheduleCounter.php', 5, "将counter队列更新进DB"),//批量更新排期脚本
            "MapIacLocation" => array(__DIR__.'/../src/Lur/crondjob/MapIacLocation.php', 300, "定期检查iac与lur的mapping情况"),//批量更新排期脚本
            "SaveAddtionalLogs" => array(__DIR__.'/../src/Lur/crondjob/SaveAddtionalLogs.php', 3600, "定期将昨天的补量log刷进磁盘"),//批量更新排期脚本
            "ClearAddtionalDbCache" => array(__DIR__.'/../src/Lur/crondjob/ClearAddtionalDbCache.php', 300, "定时将当天被停止的排期从db中去掉"),//批量更新排期脚本
            "updLastestAdrPackage" => array(__DIR__.'/../src/Lur/crondjob/updLastestAdrPackage.php', 5, "更新最新的ADR TROLLEY PACKAGE"),//批量更新排期脚本

            // "ReportSspClickRate" => array(__DIR__.'/../src/Lur/crondjob/ReportSspClickRate.php', 300, "定时将SSP点击率计算进缓存"),//批量更新排期脚本

            "AddtionalDbBalance" => array(__DIR__.'/../src/Lur/crondjob/AddtionalDbBalance.php', 600, "定时将单代码多地域的量平衡化"),//批量更新排期脚本

            
            "AdcounterMonitor" => array(__DIR__.'/../src/Lur/crondjob/AdcounterMonitor.php', 600, "问题代码计数监视器"),//消防报警器

            "Test" => array(__DIR__.'/../src/Lur/crondjob/Test.php', 360000, "清理过期排期"),//测试

            
            
            // array(__DIR__.'/../src/Lur/crondjob/EmailAdrData.php', "H0"),//定期发送数据email
        ),
    ),
);

if (defined('APPLICATION_MODEL') && strtolower(APPLICATION_MODEL) !== "console") {
    $a['navigation'] = require(__DIR__."/generation.php");
}

$a['adr_across'] = require(__DIR__."/across.adr.php");

if (file_exists(__DIR__."/defined.php")) {
    require_once(__DIR__."/defined.php");
}

return $a;