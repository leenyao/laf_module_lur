<?php
// 每天执行
// 启动时间-最少执行时长(分钟)
$aDaylyConfig = [
	// "02:00:00,30,_HN_TE,TE转化率" => 'sudo curl http://'.LAF_LOCALDOMAIN.'/module/lur/adrscript/batch/count_hnssp_te.script | php -- 1 &',
	// "03:00:00,30,_HN_TE_TIMEOUT,超时率" => 'sudo curl http://'.LAF_LOCALDOMAIN.'/module/lur/adrscript/batch/count_hnssp_te_timeout.script | php -- 1 &',
	// "04:00:00,60,_HN_ERRORS,小时错误数据" => 'sudo curl http://'.LAF_LOCALDOMAIN.'/module/lur/adrscript/batch/hnssp_error_check.script | php -- 1 &',
	// "05:00:00,60,_HN_HOUR,小时数据" => 'sudo curl http://'.LAF_LOCALDOMAIN.'/module/lur/adrscript/batch/count_hnssp_hour.script | php -- 1 &',
	// "06:00:00,60,_VPNIPINFO,小时数据" => 'sudo curl http://'.LAF_LOCALDOMAIN.'/module/lur/adrscript/batch/vipin_info.script | php -- 1 &',
];

return $aDaylyConfig;

