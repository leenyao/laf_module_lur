<?php

$sLabelPre = "menu_";
return array(


    'user-member' => array(
        array(
            'name' => 'adschedule',
            'label' => $sLabelPre . 'adschedule',
            'route' => 'zfcadmin/contentlist',
            'params' => array('type' => 'system_batch_jobs', 'cid' => 1),
           'pages' => array(
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('adcustomer'),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('adschedule'),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('system_batch_jobs', 1),
                
            ),
        ),
    ),
    'user-admin' => array(
        array(
            'name' => 'user',
            'label' => $sLabelPre . 'user',
            'route' => 'zfcadmin/contentlist',
            'params' => array('type' => 'user', 'cid' => 0),
           'pages' => array(
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('user'),
            ),
        ),
        // array(
        //     'name' => 'addata',
        //     'label' => $sLabelPre . 'addata',
        //     'route' => 'zfcadmin/contentlist',
        //     'params' => array('type' => 'addata', 'cid' => 0),
        //    'pages' => array(
        //         \YcheukfCommon\Lib\Functions::genNormalMenuConfig('addata'),
        //     ),
        // ),
        array(
            'name' => 'adschedule',
            'label' => $sLabelPre . 'adschedule',
            'route' => 'zfcadmin/contentlist',
            'params' => array('type' => 'adschedule', 'cid' => 0),
           'pages' => array(
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('adcustomer'),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('adschedule'),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('system_batch_jobs', 1),
                // \YcheukfCommon\Lib\Functions::genNormalMenuConfig('schedule3rd', 1),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1007),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1008),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1009),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1018),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1021),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1010),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1022),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1011),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1012),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1024),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1015),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('apishell'),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('customerlog'),
                \YcheukfCommon\Lib\Functions::genNormalMenuConfig('metadata', 1023),



            ),
        ),
//         array(
//             'name' => 'report',
//             'label' => $sLabelPre . 'report',
//             'route' => 'zfcadmin/report',
//             'params' => array('type' => 'reporta'),
//            'pages' => array(
// // //           zfcadmin/report
// //                 \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reporta', 0, 'menu_report_', 'zfcadmin/report'),
// //                 \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reportb', 0, 'menu_report_', 'zfcadmin/report'),
// //                 \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reportc', 0, 'menu_report_', 'zfcadmin/report'),
// //                 \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reportd', 0, 'menu_report_', 'zfcadmin/report'),
// //                 \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reporte', 0, 'menu_report_', 'zfcadmin/report'),
//                 // \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reporta', 0, 'menu_report_'),
//                 // \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reportb', 0, 'menu_report_'),
//                 // \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reportc', 0, 'menu_report_'),
//                 // \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reportd', 0, 'menu_report_'),
//                 // \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reporte', 0, 'menu_report_'),
//                 \YcheukfCommon\Lib\Functions::genNormalMenuConfig('reportf', 0, 'menu_report_'),
                
//             ),
//         ),
    ),
    'listeners' => array()
);